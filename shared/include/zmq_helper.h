/*
 * Copyright (c) 2016 <copyright holder> <email>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 */

#ifndef ZMQ_HELPER_H
#define ZMQ_HELPER_H

#include <iostream>
#include <thread>
#include <chrono>
#include <string>
#include <memory>
#include <zmq.hpp>
#include <zmq_addon.hpp>

using std::this_thread::sleep_for;
using std::chrono::nanoseconds;

class ZMQPublisher {
    bool connected_;
    zmq::context_t *context_;
    zmq::socket_t *socket_;
public:
    size_t send_count;
    ZMQPublisher();
    ~ZMQPublisher();
    void Connect(const std::string&);
    template <typename T>
    void Send(T*);
    template <typename T>
    void Send(T*, size_t);
};


template <typename T>
void ZMQPublisher::Send(T *data) {
    if (!connected_) {
        return;
    }
    size_t size = sizeof(T);
    zmq::message_t msg(size);
    memcpy(msg.data(), data, size);
    socket_->send(msg);
    ++send_count;
    // sleep for a bit so we don't fuck ourselves
    sleep_for(nanoseconds(250));
    return;
}


template <typename T>
void ZMQPublisher::Send(T *data, size_t alloced) {
    if (!connected_) {
        return;
    }
    size_t size = alloced * sizeof(T);
    zmq::message_t msg(size);
    memcpy(msg.data(), data, size);
    socket_->send(msg);
    ++send_count;
    // sleep for a bit so we don't fuck ourselves
    sleep_for(nanoseconds(50));
    return;
}


template <class T>
class DataPacker {
    bool packable_;
    size_t i_, length_, alloced_, dat_size_;
public:
    char *data_array;
    DataPacker() {
        packable_ = false;
        data_array = nullptr;
    }
    ~DataPacker() {
        if (data_array != nullptr) {
            delete data_array;
        }
    }
    void Init(size_t length) {
        packable_ = true;
        length_ = length;
        dat_size_ = sizeof(T);
        alloced_ = length_ * dat_size_;
        i_ = 0;
        if (alloced_ > 0) {
            data_array = new char[alloced_];
        } else {
            data_array = nullptr;
        }
    }
    bool Pack(const T &dat) {
        if (!packable_) {
            return false;
        }
        memcpy(data_array + dat_size_ * i_, &dat, dat_size_);
        i_ = (i_ + 1) % length_;
        return i_ == 0;
    }
    void PackRemainingZeros() {
        if (!packable_) {
            return;
        }
        while (i_ < alloced_) {
            *(data_array + i_) = '\0';
            ++i_;
        }
    }
    size_t GetAlloced() {
        return alloced_;
    }
};

#endif // ZMQ_HELPER_H
