import sys
import psutil
import datetime as dt

from .lib.config_helper import init_configs

if __name__ == '__main__':
    target = sys.argv[1]
    cfg, today, yday = init_configs(sys.argv[1:], dt.date.today(), suppress_output=True)
    pythonpath = cfg['correct_python_path']
    for proc in psutil.process_iter():
        cmdline = ' '.join(proc.cmdline())
        exists = (pythonpath in cmdline) & (sys.argv[0] not in cmdline)
        for arg in sys.argv[1:]:
            if not exists:
                break
            exists &= (arg in cmdline)
        if exists:
            print('killing', proc.pid, cmdline)
            proc.kill()
