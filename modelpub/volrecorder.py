import sys
import datetime as dt
import numpy as np
from .lib.config_helper import init_configs
from .lib.listening import ZMQListener


vols_dtype = [
    ('secid', np.int64),
    ('ts', np.int64),
    ('seqnum', np.int64),
    ('data', np.float64)
]


if __name__ == '__main__':
    cfg, today, yday = init_configs(sys.argv, dt.date.today())
    vols = ZMQListener(cfg['vol_publisher_chan'], recv_pyobj=True)
    
    vols_filename = cfg['vols_filename']
    vols_filename = vols_filename.replace('REPLACEDATE', today.strftime('%Y%m%d'))

    dat = np.zeros(1, dtype=vols_dtype)

    print(dt.datetime.now(), 'writing vols to', vols_filename)
    try:
        with open(vols_filename, 'a') as f:
            while 1:
                dat[:] = vols.recv()
                dat.tofile(f)
    except KeyboardInterrupt:
        print(dt.datetime.now(), 'quitting')
        
