import sys
import zmq
import datetime as dt
import numpy as np

from time import sleep
from .lib.config_helper import init_configs
from .volrecorder import vols_dtype


class ZMQPublisher:
    def __init__(self, addr, use_pyobj=True):
        context = zmq.Context()
        sock = context.socket(zmq.PUB)
        sock.bind(addr)
        self.sock = sock
        self.context = context
        if use_pyobj:
            self.send = self.sock.send_pyobj
        else:
            self.send = self.sock.send
        sleep(0.25)

    def kill(self):
        self.sock.close()
        self.context.destroy()


if __name__ == '__main__':
    today = dt.date.today()
    cfg, today, yday = init_configs(sys.argv, dt.date.today())

    filename = sys.argv[4]

    with open(filename, 'r') as f:
        data = np.fromfile(f, vols_dtype)
    print(dt.datetime.now(), 'reading from', filename)

    addr = cfg['vol_publisher_chan']
    publisher = ZMQPublisher(addr, use_pyobj=True)

    for i in range(data.shape[0]):
        publisher.send(tuple(data[i]))

    publisher.kill()
