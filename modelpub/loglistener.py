import sys
import datetime as dt

from zmq import ZMQError
from time import sleep
from collections import deque
from threading import Thread
from .lib.listening import ZMQListener
from .lib.config_helper import init_configs


class LogDumper(ZMQListener):
    def __init__(self, shared_q, addr, recv_pyobj):
        ZMQListener.__init__(self, addr, recv_pyobj=recv_pyobj)
        self.shared_q = shared_q
        self.should_kill = False

    def run(self):
        while 1:
            if self.should_kill:
                break
            try:
                self.shared_q.append(self.recv())
            except ZMQError:
                print('ZMQError: a listener died!')
                break

    def kill_all(self):
        self.should_kill = True
        self.kill()


if __name__ == '__main__':
    cfg, today, yday = init_configs(sys.argv, dt.date.today(), suppress_output=True)

    q = deque()

    vols = LogDumper(q, cfg['vol_log_chan'], recv_pyobj=False)
    vols_thread = Thread(target=vols.run)
    vols_thread.start()

    opm = LogDumper(q, cfg['opm_log_chan'], recv_pyobj=False)
    opm_thread = Thread(target=opm.run)
    opm_thread.start()

    print(dt.datetime.now(), 'starting')

    while 1:
        try:
            while q:
                print(q.popleft())
            sleep(0.1)
        except KeyboardInterrupt:
            print(dt.datetime.now(), '\nstopping')
            break

    vols.kill()
    opm.kill_all()

    vols_thread.join()
    opm_thread.join()
