import sys
import zmq
import datetime as dt
import numpy as np
import pandas as pd

from glt.tools import now
from .lib.config_helper import init_configs
from .lib.publishing import (
    OPMDataStruct,
    PUB_MSGTYPE,
    MSGTYPE_FULL,
    MSGTYPE_ADJ
)


if __name__ == '__main__':
    today = dt.date.today()
    cfg, today, yday = init_configs(sys.argv, today, suppress_output=True)
    model = cfg['model']
    listen_addr = cfg['opm_publisher_chan']

    context = zmq.Context()
    listener = context.socket(zmq.SUB)
    listener.connect(listen_addr)
    listener.setsockopt(zmq.SUBSCRIBE, 'MOD')

    pubdata = OPMDataStruct()

    relevant_msgtypes = set([MSGTYPE_FULL, MSGTYPE_ADJ])

    opm_filename = cfg['opm_filename']
    opm_filename = opm_filename.replace('REPLACEDATE', today.strftime('%Y%m%d'))

    try:
        with open(opm_filename, 'a') as f:
            while 1:
                data = listener.recv()
                ts = now()
                if 'MOD,' in data:
                    msg, head, body = pubdata.unpack(data[4:])
                    if head[PUB_MSGTYPE] in relevant_msgtypes:
                        f.write(data[4:] + '\n')
    except KeyboardInterrupt:
        print(dt.datetime.now(), 'KeyboardInterrupt! quitting.')

    listener.close()
    context.destroy()

