import sys
import zmq
import datetime as dt
import numpy as np
import pandas as pd

from glt.tools import now
from .lib.config_helper import init_configs
from .lib.publishing import OPMDataStruct


if __name__ == '__main__':
    today = dt.date.today()
    cfg, today, yday = init_configs(sys.argv, today, suppress_output=True)
    model = cfg['model']
    listen_addr = cfg['opm_publisher_chan']

    context = zmq.Context()
    listener = context.socket(zmq.SUB)
    listener.connect(listen_addr)
    listener.setsockopt(zmq.SUBSCRIBE, 'MOD')

    pubdata = OPMDataStruct()

    while 1:
        try:
            data = listener.recv()
            ts = now()
            if 'MOD,' in data:
                msg, head, body = pubdata.unpack(data[4:])
                print(dt.datetime.now(), ts, len(data)-head[1], head)
                #if head[-3] == 1:
                #    print dt.datetime.now(), ts, head
                #    dat = pd.DataFrame(body.reshape(-1, 3), columns=['secid', 'vol', 'adj'])
                #    dat['secid'] = dat['secid'].astype(int)
                #    print dat.to_string()
        except ValueError as inst:
            print('ValueError?', inst)
        except KeyboardInterrupt:
            print('\nKeyboardInterrupt! breaking.')
            break

    listener.close()
    context.destroy()

