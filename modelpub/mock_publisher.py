import zmq
import json
import numpy as np

from struct import Struct
from glt.tools import now


PUB_START = 0
PUB_NBYTES = 1
PUB_SERVERID = 2
PUB_SEQNUM = 3
PUB_TIMESTAMP = 4
PUB_MICROSECONDS = 5
PUB_SPOT_PRICE = 6
PUB_SPOT_LAST_CURVE_UPDATE = 7
PUB_SPREAD_OFFSET = 8
PUB_SYNTHETIC = 9
PUB_SYNTHETIC_OFFSET = 10
PUB_ENTRIES = 11
PUB_MSGTYPE = 12
PUB_PED_IDX = 13
PUB_RANDOM = 14

MSGTYPE_FULL = 1
MSGTYPE_ADJ = 2


class ValuesPublisher:
    def __init__(self, addr):
        context = zmq.Context()
        sock = context.socket(zmq.PUB)
        sock.bind(addr)
        
        self.sock = sock
        self.context = context

    def send(self, data):
        self.sock.send(data)

    def kill(self):
        self.sock.close()
        self.context.destroy()


class PublishData:
    def __init__(self):
        self.hstruct = Struct('<BIIIQqdddddHBBI')
        self.hsize = self.hstruct.size
        self.full_pattern = 'Idd'
        self.adj_pattern = 'Id'
        
    def unpack(self, bytes):
        header = self.hstruct.unpack(bytes[:self.hsize])
        n = header[PUB_ENTRIES]
        msg = header[PUB_MSGTYPE]
        if msg == MSGTYPE_FULL:
            bstruct = Struct(n*self.full_pattern)
        elif msg == MSGTYPE_ADJ:
            bstruct = Struct(n*self.adj_pattern)
        else:
            raise ValueError('unknown msgtype: %d' % msg)
        return header, np.array(bstruct.unpack(bytes[self.hsize:]))


if __name__ == '__main__':
    with open('modelpub.prod.json', 'r') as f:
        cfg = json.load(f)

    listen_addr = cfg['publishChannel']

    context = zmq.Context()
    listener = context.socket(zmq.SUB)
    listener.connect(listen_addr)
    listener.setsockopt(zmq.SUBSCRIBE, 'MOD')

    pubdata = PublishData()

    pub_addr = 'ipc:///home/deio/karl/mock_pub.ipc'
    publisher = ValuesPublisher(pub_addr)

    while 1:
        try:
            data = listener.recv()
            publisher.send(data)
        except KeyboardInterrupt:
            print('\nKeyboardInterrupt! breaking.')
            break

    publisher.kill()
    listener.close()
    context.destroy()

