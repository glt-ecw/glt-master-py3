import sys
import os
import time
import json
import subprocess
import signal
from numpy import isnan

from glt.listening.md import DeioMDReceiver
from glt.log import create_logger, DEBUG, INFO, filter_file_handlers

from .lib.fill_handling import MDRepubFillHandler
from .lib.deio_publishing import OPMDataPublisher, ZMQListener

import numpy as np
np.set_printoptions(linewidth=300)

required_env_vars = [
    'RESEARCH_TOOLS',
]


def find_request_channel(pub_channel):
    epgm, addr, port = pub_channel.split(':')
    return '%s:%s:%d' % (epgm, addr, int(port)+1)


class ConsoleCommand:
    def __init__(self, publisher, logger):
        self.publisher = publisher
        self.logger = logger
        self.delim = ' -- '
        self.fake_position_orderid = 1
        self.fake_fills = []
        self.reload_config = False

    def process(self, request):
        _, cmd = request.split('CONSOLE:')
        self.logger.info('ConsoleCommand processing: %s', cmd)
        if self.delim not in cmd:
            self.logger.info('unrecognized command: %s', cmd)
            return
        cmd_type, cmd_action = cmd.split(' -- ')
        if cmd_type == 'setLogLevel':
            if cmd_action == 'DEBUG':
                self.logger.setLevel(DEBUG)
                self.logger.warning('changing logger level to DEBUG')
            else:
                self.logger.setLevel(INFO)
                self.logger.warning('changing logger level to INFO')
        elif cmd_type == 'addPosition':
            self.logger.warning('ConsoleCommand adding fake fill: %s', cmd_action)
            secid, side, price, qty = cmd_action.split(',')
            fill = {
                'id': self.fake_position_orderid,
                'secid': int(secid),
                'side': int(side),
                'price': float(price),
                'qty': float(qty),
                'exchtradeid': ''
            }
            self.fake_fills.append(fill)
            self.logger.warning('ConsoleCommand queueing up fill: %s', fill)
            self.fake_position_orderid += 1
        elif cmd_type == 'reloadConfig':
            #does nothing
            self.logger.warning('ConsoleCommand reloading config')
            self.reload_config = True
        elif cmd_type == 'reloadAdjustments':
            self.logger.info('ConsoleCommand reloading adjustments')
            new_params = eval(cmd_action)
            publisher.update_adjustment_parameters(
                new_params['maxAdjustment'],
                new_params['widthFactor'],
                new_params['wingFactor'],
                new_params['timeFactor'],
                new_params['vegaCapPercentage']
            )
            publisher.create_adjustments()
        elif cmd_type == 'reloadModel':
            self.logger.info('ConsoleCommand reloading model cfg')
            new_params = eval(cmd_action)
            publisher.update_model_parameters(new_params)
        elif cmd_type == 'dumpPos':
            self.logger.info('ConsoleCommand dumping position')
            # columns: 'POS, VOL, TTE, M, VEGA, ADJ, K, ISSM, CPos, PPOS, CDEL, PDEL, CADJ, PADJ, VP'
            idx = [0,6,1,2,4,5,12,13,14]
            # to output this to the log file, uncomment logger lines below
            header ='POS, K, VOL, TTE, VEGA, ADJ, CADJ, PADJ, VP'
            print(header)
            #self.logger.info(header)
            for key in list(publisher.shared_arrays.keys()):
                for row in publisher.shared_arrays[key]:
                    print(row[idx])
                    #self.logger.info(row[idx])
        else:
            self.logger.warning('unrecognized command: %s', cmd)

    def should_reload_config(self):
        if self.reload_config:
            self.reload_config = False
            return True
        return False

    def latest_fake_fills(self):
        while self.fake_fills:
            yield self.fake_fills.pop()


class DummyModel:
    def __init__(self, *args, **kwargs):
        pass


def init_model(model_cfg, ts_start, logger):
    Model = DummyModel
    try:
        logger.info('model_cfg: %s', model_cfg)
        import_cmd = (
            'from {module} import {obj} as Model'
            ''.format(
                module=model_cfg['module'], 
                obj=model_cfg['object']
            )
        )
        logger.info(import_cmd)
        exec(import_cmd)
        param = model_cfg['parameters']
        # add logger to param
        param['logger'] = logger
        logger.info('calling %s.__init__ with ts_start=%s', 
            model_cfg['object'], ts_start)
        model = Model(ts_start=ts_start, **param)
        return model
    except:
        logger.exception('failed initializing model')
        raise        


class ConfigHandler:
    def __init__(self):
        pass

    def add_logger(self, logger):
        self.logger = logger

    def read(self, filename):
        with open(filename, 'r') as f:
            cfg = json.load(f)

        self.flatfile = cfg['flatFiles']
        self.models = cfg['models']
        self.tv_adjust = cfg['tvAdjust']
        self.listen = cfg['listening']
        self.pub = cfg['publishing']
        self.debug = cfg['debug']
        self.process_name = cfg['processName']

        # and for the heck of it, save the whole config
        self.whole = cfg


def init_fill_handler(cfg, logger):
    fill_srcs_cfg = cfg.tv_adjust['fillSources']
    adjusting_cfg = cfg.tv_adjust['adjusting']
    fillfwd_cfg = cfg.listen['fillFwd']
    fill_handler = MDRepubFillHandler(fill_srcs_cfg['servers'], fill_srcs_cfg['users'], 
        tdate, fill_srcs_cfg['table'], fill_srcs_cfg['instance'], fill_srcs_cfg['load_pos'],
        fillfwd_cfg['interface'], fillfwd_cfg['channel'], logger)
    return fill_handler


def init_publisher(cfg, price_multiplier, models, logger):
    pub_channel = cfg.pub['0MQChannel']
    adjusting_cfg = cfg.tv_adjust['adjusting']
    return OPMDataPublisher(cfg.pub['modelID'], price_multiplier, 
        pub_channel, models, cfg.pub['intervalInSeconds'], adjusting_cfg, logger)


class ScriptKiller:
    kill = False
    def __init__(self):
        signal.signal(signal.SIGINT, self.exit)
        signal.signal(signal.SIGTERM, self.exit)

    def exit(self, signum, frame):
        self.kill = True


if __name__ == '__main__':
    cfg_filename, ts_start = sys.argv[1:]
    timeval = int(time.time())

    # check environment variables
    if not all(var in os.environ for var in required_env_vars):
        error_msg = (
            'need to define: {vars}. wrap run_opm.py with shell script.'
            ''.format(vars=required_env_vars)
        )
        raise RuntimeError(error_msg)

    cfg = ConfigHandler()
    cfg.read(cfg_filename)    

    log_filename = cfg.flatfile['logFilename'].format(datetime=timeval)
    logger = create_logger(
        log_name=cfg.process_name, 
        log_filename=log_filename,
        debug=cfg.debug
    )
    logger.info('logging to %s', log_filename)
    logger.info('ts_start=%s', ts_start)
    cfg.add_logger(logger)
    logger.info('read config:\n%s', json.dumps(cfg.whole, indent=4, separators=(',', ': ')))

    if not cfg.models:
        logger.error('no models configured. quitting')
        sys.exit(1)

    # market data from deio
    price_multiplier = 10000.0
    try:
        mdfwd_cfg = cfg.listen['mdFwd']
        mdfwd_addr = mdfwd_cfg['channel']
        mdfwd_iface = mdfwd_cfg['interface']
        logger.info(
            'creating DeioMDReceiver on %s;%s', mdfwd_iface, mdfwd_addr
        )
        md_receiver = DeioMDReceiver(mdfwd_iface, mdfwd_addr, price_multiplier=price_multiplier)
        md_receiver.listen()
        logger.info('done')

        # replay command
        mdfwd_replay_command = os.path.expandvars(cfg.listen['replayCommand'])
        already_replayed = False

        pub_channel = cfg.pub['0MQChannel']
        request_channel = find_request_channel(pub_channel)

        # init models
        models = [init_model(m, ts_start, logger) for m in cfg.models]
        tdate = models[0].tdate()
        logger.info('tdate=%s', tdate)

        # init publisher
        logger.info('initializing publisher')
        publisher = init_publisher(cfg, price_multiplier, models, logger)

        # init fill processing
        logger.info('initializing fill_handler')
        fill_handler = init_fill_handler(cfg, logger)
        fill_handler.start()

        logger.info('initializing request listener')
        request_listener = ZMQListener(request_channel, '', logger)
        request_listener.start()

        console_cmd = ConsoleCommand(publisher, logger)

    except:
        logger.exception('initialization failed. quitting')
    else:
        logger.info('initialization complete! start processing')
        logger.info('removing logging from stdout. please refer to log file above')

    # remove logging to stdout
    logger.handlers = filter_file_handlers(logger)

    killer = ScriptKiller()
    while True:
        if killer.kill: #killer kill yo!
            logger.info('script killed.  Going down...')
            break 
        if not already_replayed:
            logger.info('requesting known md using script: %s', mdfwd_replay_command)
            try:
                replay_out = subprocess.check_output(mdfwd_replay_command)
            except OSError:
                logger.exception('no such script. check replayCommand in your config')
                break
            except:
                logger.exception('failed to replay known md. quitting')
                break
            else:
                logger.info('done. script output:\n%s', replay_out)
                already_replayed = True
                publisher.start()
        else:
            try:
                # any requests?
                for request in request_listener.latest_msgs():
                    if request.startswith('CONSOLE:'):
                        logger.warning('received CONSOLE command: %s', request)
                        console_cmd.process(request)
                    else:
                        logger.warning('giving request (%s) to publisher', request)
                        publisher.add_request(request)
                if console_cmd.should_reload_config():
                    fill_handler.stop()
                    publisher.stop()

                    logger.warning('reloading configs')
                    cfg.read(cfg_filename)

                    # init models
                    models = [init_model(m, ts_start, logger) for m in cfg.models]
                    tdate = models[0].tdate()
                    logger.info('tdate=%s', tdate)

                    # init fill processing
                    logger.info('initializing fill_handler')
                    fill_handler = init_fill_handler(cfg, logger)
                    fill_handler.start()

                    logger.info('initializing publisher')
                    publisher = init_publisher(cfg, price_multiplier, models, logger)
                    already_replayed = False
                    continue
                # now update the publisher
                # fake fills?
                for fill in console_cmd.latest_fake_fills():
                    publisher.update_position(fill)
                    do_fill = True
                # first fills
                for fill in fill_handler.latest_fills():
                    publisher.update_position(fill)
                    do_fill = True
                # now md
                for pymd in md_receiver.latest_msgs():
                    #logger.debug('received md=%s', pymd)
                    publisher.update_models(pymd)
                # tv adjust if fills
                #if publisher.has_new_fills:
                if do_fill:
                    publisher.create_adjustments()
                    do_fill = False

            except:
                logger.error('error! check exception stack', exc_info=True)
                break

    logger.warning('killing other threads')
    # store vols in a pickle here
    publisher.dump_data()
    publisher.stop()
