import sys
import os
import glob
import pickle
import subprocess
import datetime as dt

from numpy import fromfile
from pandas import DataFrame
from threading import Thread
#from glt.tools import now
from glt.listening.md import DeioMDReceiver

from .lib.config_helper import init_configs
#from lib.listening import (
#    open_multicast_socket, 
#    RequestHandler,
#    MarketDataUnpacker, 
#    StructError
#)
from .lib.publishing import RawVolPublisher
from .lib.modeling import SecurityLoader
from .lib.logging import LogPublisher
from .volrecorder import vols_dtype


"""
 // This stuff is used for MD forwarding.
    struct FwdEvent {
        struct Header {
            unsigned int        serverId;
            unsigned int        securityId;
            unsigned long       exchSeqNum;

            // Pack all the char fields together to save space.
            unsigned char       eventType;
            unsigned char       status;
            unsigned char       lastTradeAggressorSide;
            unsigned char       numLevels;

            // Trade specific fields.
            double              lastTradePrice;
            double              lastTradeQty;
            double              accumVol;

            // Price change specific fields;
            double              priceChangeBidPrice;
            double              priceChangeOfferPrice;
            double              indicativePrice;
            bool                replay;
        };

        struct BookLevel {
            double          bidPrice;
            double          bidQty;
            double          offerPrice;
            double          offerQty;
        };

        const static size_t MaxLevels = 10;

        Header                header;
        BookLevel            levels[MaxLevels]; // There will only be header.numLevels of these.

        typedef boost::shared_ptr<FwdEvent> pointer;
        void reset() { memset(this, 0, sizeof(*this)); }
        unsigned int length() const { return sizeof(Header) + header.numLevels * sizeof(BookLevel); }
    };
"""


if __name__ == '__main__':
    today = dt.date.today()
    cfg, today, yday = init_configs(sys.argv, today)
    model_cfg = cfg['model']

    debug_mode = model_cfg['volPublisher']['debug']
    logname = '%s: volpublisher.py' % model_cfg['name']
    logger = LogPublisher(cfg['vol_log_chan'], logname, debug_mode=debug_mode)

    secs = SecurityLoader(logger=logger)
    secs.build_objects(cfg, today)

    # write to pickle
    with open(os.path.join(cfg['securities_filename']), 'wb') as f:
        pickle.dump(secs.as_dict(), f)

    # test opening secs
    with open(os.path.join(cfg['securities_filename']), 'rb') as f:
        secs.rebuild_objects(pickle.load(f))

    # let's get the model going
    publisher = RawVolPublisher(
        cfg['vol_publisher_chan'], secs, model_cfg, logger=logger, trade_log_chan=cfg['trade_log_chan']
    )

    # what's happening with the rest of the world
    logger.write('creating mdfwd listener', 'info')
    md_receiver = DeioMDReceiver(cfg['mdfwd_iface'], cfg['mdfwd_addr'])
    md_receiver.listen()
    #struct_fmt = '<IIQBBBBiddddddQ', '4d2I'
    #md = MarketDataUnpacker(struct_fmt, header=80, max_levels=10)

    #sock = open_multicast_socket(cfg['mdfwd_addr'], cfg['host'])
    replay_command = cfg['mdfwd_replay']
    already_replayed = False
    logger.write('done.', 'info')

    logger.write('opening request listener', 'info')
    requests = RequestHandler(
        cfg['vol_request_chan'], publisher, logger=logger, replay_messages=['REPLAY']
    )
    reqs_thread = Thread(target=requests.run)
    #reqs_thread.daemon = True
    reqs_thread.start()
    logger.write('done', 'info')

    filenames = glob.glob(cfg['vols_filename'].replace('REPLACEDATE', '*'))
    filenames.sort()
    filenames = list(filter(os.path.getsize, filenames))
    if not filenames:
        logger.write('no filenames found.', 'warn')
    else:
        filename = filenames[-1]
        logger.write('reading previous volpublisher data from %s' % filename, 'info')
        with open(filename, 'r') as f:
            rdata = DataFrame(fromfile(f, vols_dtype))
        rdata = rdata.drop_duplicates('secid', keep='last')
        publisher.update_from_arrays(rdata['secid'].values, rdata['seqnum'], rdata['data'])
        logger.write('done', 'info')

    while 1:
        try:
            if not already_replayed:
                logger.write('requesting known md.', 'info')
                exit_code = subprocess.call(replay_command)
                if exit_code != 0:
                    logger.write(
                        ('failed to replay known md. quitting. '
                         'exit code: %d') % exit_code, 'error'
                    )
                    raise KeyboardInterrupt
                logger.write('done.', 'info')
                already_replayed = True
            #msg = sock.recv(1000)
            #ts = now()
            #sz, dat = md.unpack(msg)
            #publisher.update_md(ts, dat, sz)
            for md in md_receiver.latest_msgs():
                publisher.update_md(md)
        except KeyboardInterrupt:
            logger.write('KeyboardInterrupt! exiting', 'error')
            break
        except StructError:
            pass

    # kill run_forever loops and close connections
    requests.kill()
    publisher.kill()
    sock.close()

    reqs_thread.join()
