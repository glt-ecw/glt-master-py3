import glt.db

from collections import deque, namedtuple
from time import time
from pandas import to_datetime, Timedelta
from glt.net import MulticastSocket
from threading import Thread 


def query_users(users, logger):
    if not users:
        return []
    cmd = """
SELECT userid
FROM DEIO_META.users
WHERE username in ({users})
""".format(users=','.join(["'%s'"%x for x in users]))
    logger.info('querying mysql:\n%s', cmd)
    return glt.db.autoclose_query(cmd)['userid'].tolist()
    

def query_pos_fills(tdate, db_table, logger):
    for i in range(1, 10):
        query = '''
        SELECT securityId as secid, position as qty
        FROM {table}.positions
        WHERE tradedate="{tdate}";
        '''.format(tdate=to_datetime(tdate).date() - Timedelta('%sD'%i),
            table=db_table)
        st = time()
        pos = glt.db.autoclose_query(query)
        # once you come to a query with more than 0 values, break
        # this should handle multiple holidays in a row
        if len(pos) > 0:
            logger.debug('found position fills:\n%s', query)
            break
    et = time()
    logger.debug('query finished in %0.3f seconds', et-st)
    #doctor up the df a bit
    # need to add id, userid, serverid, price, side, exchtradeid
    pos.loc[:, 'id'] = -1
    pos.loc[:, 'userid'] = -1
    pos.loc[:, 'serverid'] = -1
    pos.loc[:, 'price'] = 0
    pos.loc[:, 'exchtradeid'] = -1
    pos.loc[:, 'side'] = 1
    pos.loc[pos['qty']<0, 'side'] = 2
    pos.loc[:, 'qty'] = (3-2*pos['side']) * pos['qty']
    return list(pos.T.to_dict().values())


def query_tdate_fills(tdate, db_table, db_instance, 
        userids, serverids, logger):
    # figure out where clause
    if userids and serverids:
        users_where = """ 
    AND
    userid IN (
        {userids}
    )
    AND
    serverid IN (
        {serverids}
    )"""
    elif userids:
        users_where = """
    AND
    userid IN (
        {userids}
    )"""
    elif serverids:
        users_where = """
    AND
    serverid IN (
        {serverids}
    )"""
    else:
        users_where = ""
    # main sql command
    cmd = """
SELECT 
    id, 
    securityid as secid, 
    userid, 
    serverid,
    price,
    side,
    qty,
    exchtradeid
    FROM {table}.fills
    WHERE (
        tradedate='{tdate}'
        AND
        instance='{instance}'
        AND
        filltype<>2
""".format(
        tdate=to_datetime(tdate).date(),
        instance=db_instance,
        table=db_table,
    ) + users_where.format(
        userids=','.join(map(str, userids)),
        serverids=','.join(map(str, serverids))
    ) + '\n)'
    st = time()
    logger.debug('querying fills:\n%s', cmd)
    fills = glt.db.autoclose_query(cmd)
    # to account for user entries (whicih have a '' exchtradeid)
    fills.loc[fills['exchtradeid']=='', 'exchtradeid'] = -1
    et = time()
    logger.debug('query finished in %0.3f seconds', et-st)
    return list(fills.T.to_dict().values())


DeioFill = namedtuple('DeioFill', ['topic', 'pub_seqnum', 'orderid', 'seqnum', 
        'unknown', 'side', 'secid', 'userid', 'serverid', 'price', 'qty', 
        'exchtradeid', 'ordertype', 'time', 'assumed'])


class MDRepubFillHandler(Thread):
    def __init__(self, servers, users, tdate, db_table, db_instance, load_pos, iface, addr, logger):
        self.servers = servers
        self.users = users
        self.tdate = tdate
        self.db_table = db_table
        self.db_instance = db_instance
        self.logger = logger
        # who are these users?
        self.userids = query_users(users, self.logger)
        self.serverids = query_users(servers, self.logger)

        # and what are these servers?
        # fetch previous fills
        prev_fills = query_tdate_fills(
            tdate, db_table, db_instance, self.userids, self.serverids, logger
        )
        self.logger.debug('fills:\n%s', prev_fills)
        self.fills = deque()
        self.unique_fills = set()
        for fill in prev_fills:
            self.process(fill)
        #fetch prev day pos
        if load_pos:
            prev_pos_fills = query_pos_fills(tdate, db_table, logger)
            self.logger.debug('pos fills:\n%s', prev_pos_fills)
            for fill in prev_pos_fills:
                self.process(fill)
        self.socket = MulticastSocket(iface, addr, publish=False)
        logger.info('listening to real-time fills on %s;%s', iface, addr)
        Thread.__init__(self, target=self.run)
        self.daemon = True
        self.stopped = False

    def process_raw(self, msg):
        # FILL,5,203250005,1,1,bid,515119,11,93,0,1,,UserEntry,20170929T043706.268306,0
        deio_fill = DeioFill(*msg.split(','))
        self.logger.debug('digested fillfwd message %s to %s', msg, deio_fill)
        if deio_fill.exchtradeid:
            exchtradeid = deio_fill.exchtradeid
        else:
            exchtradeid = 'USER%s' % deio_fill.orderid
        fill = {
            'secid': int(deio_fill.secid),
            'price': float(deio_fill.price),
            'serverid': int(deio_fill.serverid),
            'exchtradeid': exchtradeid,
            'userid': int(deio_fill.userid),
            'id': int(deio_fill.orderid),
            'qty': float(deio_fill.qty),
            'side': 1 if deio_fill.side == 'bid' else 2
        }
        return self.process(fill)
        
    def process(self, fill):
        #exchtradeid of -1 is a prev day fill
        if ((self.userids and fill['userid'] not in self.userids) or
                (self.serverids and fill['serverid'] not in self.serverids)) and \
                fill['exchtradeid'] != -1:
            self.logger.warning('ignoring fill: %s', fill)
            return
        #fills are indexed by secid, side, exchtradeid
        fill_key = fill['secid'], fill['side'], fill['exchtradeid']
        if fill_key not in self.unique_fills:
            self.logger.debug('processing new fill: %s', fill)
            self.fills.append(fill)
            if fill['exchtradeid'] != -1:
                self.unique_fills.add(fill_key)

    def latest_fills(self):
        while self.fills:
            yield self.fills.popleft()

    def stop(self):
        self.stopped = True
        self.socket.close()
        self.join()

    def run(self):
        self.logger.info('starting MDRepubFillHandler thread')
        try:
            while True:
                if self.stopped:
                    break
                self.process_raw(self.socket.recv())
                
        except:
            self.logger.exception('MDRepubFillHandler died')
        self.logger.info('MDRepubFillHandler stopped')


