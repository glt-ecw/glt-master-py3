import socket
import zmq
from time import sleep
from collections import deque
from struct import Struct, error as StructError
from glt.tools import extract_mdrepub_fill, now


def request_replay(request_addr):
    context = zmq.Context()
    requests = context.socket(zmq.PUB)
    requests.bind(request_addr)

    sleep(0.2)
    requests.send_pyobj('REPLAY')

    sleep(0.2)
    requests.close()
    context.destroy()


def open_multicast_socket(addr, host):
    ip, port = addr.split(':')
    port = int(port)

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)

    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 32)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 1)

    sock.bind((ip, port))

    sock.setsockopt(
        socket.SOL_IP,
        socket.IP_MULTICAST_IF,
        socket.inet_aton(host)
    )
    sock.setsockopt(
        socket.SOL_IP,
        socket.IP_ADD_MEMBERSHIP,
        socket.inet_aton(ip) + socket.inet_aton(host)
    )
    return sock


class ZMQListener:
    def __init__(self, addr, recv_pyobj=True, subscriptions=None):
        context = zmq.Context()
        sock = context.socket(zmq.SUB)
        sock.connect(addr)
        if subscriptions is None:
            sock.setsockopt(zmq.SUBSCRIBE, '')
        elif type(subscriptions) in (str, str):
            sock.setsockopt(zmq.SUBSCRIBE, subscriptions)
        else:
            for sub in subscription:
                sock.setsockopt(zmq.SUBSCRIBE, sub)
        self.context = context
        self.sock = sock
        if recv_pyobj:
            self.recv = self.sock.recv_pyobj
        else:
            self.recv = self.sock.recv

    def kill(self):
        self.sock.close()
        self.context.destroy()


class RequestHandler(ZMQListener):
    def __init__(self, addr, publisher, cfg=None, logger=None, recv_pyobj=True, 
            replay_messages=None, queue_actions=False, console_cmd_name=None):
        self.logger = logger
        ZMQListener.__init__(self, addr, recv_pyobj=recv_pyobj, subscriptions=None)
        self.publisher = publisher
        self.replay_messages = replay_messages
        self.actions = deque()
        self.queue_actions = queue_actions
        if console_cmd_name is None:
            console_cmd_name = 'console.py'
        specific_cmds = ['reload_models']
        specific_reqs = {}
        for cmd in specific_cmds:
            specific_reqs[cmd] = '%s::%s' % (console_cmd_name, cmd)
            self.log_event('will process following console command: %s' % specific_reqs[cmd], 'info')
        self.specific_reqs = specific_reqs
        self.cfg = cfg

    def log_event(self, evt, level):
        if self.logger:
            self.logger.write(evt, level)

    def run(self):
        self.log_event('starting request listening proc.', 'info')
        while 1:
            try:
                req = self.recv()
                self.log_event('received message on request channel: %s' % req.encode('hex'), 'info')

                action = None
                if req == self.specific_reqs['reload_models']:
                    self.log_event('request to reload config for %s? %s' % (self.publisher, req), 'warn')
                elif self.replay_messages is None or req in self.replay_messages:
                    action = self.publisher.send_known
                    self.log_event('replay requested for %s' % self.publisher, 'warn')

                if action is not None:
                    if self.queue_actions:
                        self.log_event('queuing action', 'info')
                        self.actions.append(action)
                    else:
                        self.log_event('executing action', 'info')
                        action()
            except zmq.ContextTerminated:
                self.log_event('ContextTerminated! killing request handler.', 'error')
                break


class MarketDataUnpacker:
    def __init__(self, struct_fmt, header, max_levels):
        self.create_map(struct_fmt, header, max_levels)

    def create_map(self, struct_fmt, header, max_levels):
        #d_bytes = 8
        levels_map, struct_map = {}, {}
        for i in range(max_levels):
            key = header + 40*(i+1)
            struct_i = Struct(struct_fmt[0] + (i+1)*struct_fmt[1])
            struct_map[key] = struct_i
            levels_map[key] = i+1
        self.map_ = struct_map
        self.levels_ = levels_map

    def unpack(self, bytes):
        """
            returns (n_levels, data)
        """
        n = len(bytes)
        if n not in self.map_:
            raise StructError
        return self.levels_[n], self.map_[n].unpack(bytes)


class FillManager:
    def __init__(self, mdrepub_addr, logger=None):
        self.logger = logger
        context = zmq.Context()
        sock = context.socket(zmq.SUB)
        sock.connect(mdrepub_addr)
        sock.setsockopt(zmq.SUBSCRIBE, 'FILL')
        self.context = context
        self.sock = sock
        self.q_ = deque()

    def log_event(self, evt, level):
        if self.logger:
            self.logger.write(evt, level)

    def run_forever(self):
        while 1:
            try:
                msg = self.sock.recv()
                ts = now()
                dat = extract_mdrepub_fill(msg)
                self.q_.append((ts, dat))
            except zmq.ZMQError:
                self.log_event('error processing fill?', 'error')
                self.kill()
                break

    def pop(self):
        try:
            fill = self.q_.popleft()
        except IndexError:
            fill = None
        return fill

    def is_empty(self):
        return len(self.q_) == 0

    def kill(self):
        self.log_event('fill manager is dead', 'error')
        self.sock.close()
        self.context.destroy()



