import zmq
import pandas as pd
import datetime as dt
from time import sleep
from numpy import int64, float64, isnan, array, nan
from collections import OrderedDict, defaultdict, deque
from glt.tools import now
from struct import Struct, error as StructError
from .modeling import (
    OPMManager, 
    SH_ARR_VOL, SH_ARR_CADJ, SH_ARR_PADJ, SH_ARR_VP, SH_ARR_TTE,
    FILL_ID, FILL_FILLTYPE, FILL_SIDE, FILL_SECURITYID, 
    FILL_PRICE, FILL_QTY, FILL_EXCHTRADEID
)

# mdfwd
MD_SECID = 1
MD_SEQNUM = 2
MD_EVENTTYPE = 3
MD_STATUS = 4
MD_TRADESIDE = 5
MD_NUM_LEVELS = 6
MD_TRADEPRC = 8
MD_TRADEQTY = 9
MD_ACC_VOLUME = 10
MD_BIDPRC_CHANGE = 11
MD_ASKPRC_CHANGE = 12
MD_INDICATIVE_PRC = 13
MD_LAST_IDX = 14

MD_EVENTTYPE_TRADE = 129

# deio struct
PUB_START = 0
PUB_NBYTES = 1
PUB_SERVERID = 2
PUB_SEQNUM = 3
PUB_TIMESTAMP = 4
PUB_MICROSECONDS = 5
PUB_SPOT_PRICE = 6
PUB_SPOT_LAST_CURVE_UPDATE = 7
PUB_SPREAD_OFFSET = 8
PUB_SYNTHETIC = 9
PUB_SYNTHETIC_OFFSET = 10
PUB_TTE = 11
PUB_IR = 12
PUB_TRIGGERID = 13
PUB_ENTRIES = 14
PUB_MSGTYPE = 15
PUB_PED_IDX = 16
PUB_RANDOM = 17

MSGTYPE_FULL = 1
MSGTYPE_ADJ = 2
MSGTYPE_HEARTBEAT = 3

VOLPUB_REPLAY_END = -1


tdata_struct = Struct('<qqBdd')


class OPMDataStruct:
    def __init__(self):
        """
    const static unsigned char PUB_MSG_FULL = 1;
    const static unsigned char PUB_MSG_ADJSTS = 2;
    const static unsigned char PUB_MSG_HEARTBEAT = 3;

    struct PubHeader {
        unsigned int    modelId;
        unsigned int    seqNum;
        TimeStamp       ts;
        double          underlyingMidPrice;
        double          underlyingMidAtLastCurveUpdate;
        double          spreadOffset;
        double          synthetic;
        double          syntheticOffset;
        double          yearsToExpiration;
        double          interestRate;
        unsigned long   triggerId;
        unsigned short  numEntries;
        unsigned char   msgType;
        unsigned char   pedIdx;
        bool            isLastUpdate;
        bool            callCallBacks;
    };

    struct PubFullEntry {
        unsigned int securityId;
        double volatility;
        double atvCallAdjustment;
        double atvPutAdjustment;
        double volpath;

        void set(const StrikeEntry &se) {
            securityId = (se.call.data.securityId) ? se.call.data.securityId : se.put.data.securityId;
            volatility = se.volatility;
            atvAdjustment = se.atvAdjustment;
        }
    };

    struct PubAdjustEntry {
        unsigned int securityId;
        double atvCallAdjustment;
        double atvPutAdjustment;

        void set(const StrikeEntry &se) {
            securityId = (se.call.data.securityId) ? se.call.data.securityId : se.put.data.securityId;
            atvAdjustment = se.atvAdjustment;
        }
    };
        """
        self.hstruct = Struct('<BIIIQqdddddddQHBBI')
        self.hsize = self.hstruct.size
        self.full_pattern = 'Idddd'
        self.adj_pattern = 'Idd'
        self.pub_seqnum = 0
        self.data_slice = array([SH_ARR_VOL, SH_ARR_CADJ, SH_ARR_PADJ, SH_ARR_VP, SH_ARR_TTE], dtype=int64)

        self.dtypes = [
            None,
            [
                ('secid', int64),
                ('vol', float64),
                ('cadj', float64),
                ('padj', float64),
                ('vp', float64)
            ],
            [
                ('secid', int64),
                ('cadj', float64),
                ('padj', float64)
            ],
            None
        ]

    def unpack(self, bytes):
        header = self.hstruct.unpack(bytes[:self.hsize])
        n = header[PUB_ENTRIES]
        msg = header[PUB_MSGTYPE]
        if msg == MSGTYPE_HEARTBEAT:
            return msg, header, None
        elif msg == MSGTYPE_FULL:
            bstruct = Struct(n*self.full_pattern)
        elif msg == MSGTYPE_ADJ:
            bstruct = Struct(n*self.adj_pattern)
        else:
            raise ValueError('unknown msgtype: %d' % msg)
        dat = bstruct.unpack(bytes[self.hsize:])
        ndat = len(dat)
        dat_dtype = self.dtypes[msg]
        ndtypes = len(dat_dtype)
        reshaped_dat = []
        for i in range(0, ndat, ndtypes):
            reshaped_dat.append(dat[i:i+ndtypes])
        return msg, header, array(reshaped_dat, dtype=dat_dtype)

    def pack(self, packtime, opm, term, msgtype, term_i, n_terms):
        self.pub_seqnum += 1
        spot_prc = opm.last_good_spot_prc
        if term in opm.imply_spot:
            spread = opm.spread_md[opm.imply_spot[term]]
            spread_prc = spread[1] * spread[0].prc
        else:
            spread_prc = 0
        if isnan(spread_prc):
            return
        roll = opm.model_roll_prcs[term]
        sh_idx = list(opm.shared_arrays.keys()).index(term)
        sh_arr = opm.shared_arrays[term][:, self.data_slice]
        n = sh_arr.shape[0]
        tte = sh_arr[0, 4]
        ir = opm.ir
        if msgtype == MSGTYPE_HEARTBEAT:
            bstruct = None
        elif msgtype == MSGTYPE_FULL:
            bstruct = Struct(n*self.full_pattern)
        else:
            bstruct = Struct(n*self.adj_pattern)
        model_id = 1263224394
        triggerid = 1234
        header = [
            15,
            self.hstruct.size + (0 if bstruct is None else bstruct.size) - 5,
            model_id,
            self.pub_seqnum,
            packtime / 1000000000, (packtime/1000) % 1000000,
            spot_prc,
            opm.models[term].spot - roll - spread_prc,
            spread_prc,
            spot_prc + roll + spread_prc,
            roll,
            tte,
            ir,
            triggerid,
            0 if msgtype == MSGTYPE_HEARTBEAT else n,
            msgtype,
            sh_idx,
            0 if msgtype == MSGTYPE_HEARTBEAT else (term_i + 1 == n_terms)
        ]
        data = []
        if msgtype == MSGTYPE_HEARTBEAT:
            return self.hstruct.pack(*header)
        for i, secid in enumerate(opm.t2c_map[term]):
            data.append(secid)
            if msgtype == MSGTYPE_FULL:
                data.append(sh_arr[i, 0])
            data.append(sh_arr[i, 1])
            data.append(sh_arr[i, 2])
            if msgtype == MSGTYPE_FULL:
                data.append(sh_arr[i, 3])
        return self.hstruct.pack(*header) + bstruct.pack(*data)


class DeioOPMPublisher(OPMManager, OPMDataStruct):
    def __init__(self, addr, sec_loader, tv_adj, model_defs, db_param, start_time, logger):
        conn_defn = db_param['connection']
        OPMManager.__init__(
            self, sec_loader, model_defs, conn_defn, 
            start_time=start_time, logger=logger
        )
        OPMDataStruct.__init__(self)
        self.tv_adj = tv_adj
        self.tv_adj.ref_model_objects(self)
        self.db_param = db_param

        context = zmq.Context()
        sock = context.socket(zmq.PUB)
        sock.bind(addr)
        self.sock = sock
        self.context = context

        self.should_kill = False
        self.adjusting = False
        self.updating = False
        self.known_secids = set()

    def load_positions(self, yday, today):
        self.tv_adj.load_positions(self.db_param, yday)
        self.tv_adj.load_fills(self.db_param, today)
        self.log_event('calculating initial adjustments', 'info')
        self.tv_adj.create_matrices()
        self.tv_adj.calculate_adjustments(fill_id=0)

    def check_secids(self):
        remove_terms = []
        for term in self.next_model_update:
            options = self.option_defs.loc[self.option_defs.name.str.contains(term)]
            options = options.loc[
                options['call_id'].isin(self.known_secids)|options['put_id'].isin(self.known_secids)
            ]
            if not options.shape[0]:
                remove_terms.append(term)
        for term in remove_terms:
            self.log_event('removing %s from model computations because no md exists.' % term, 'warn')
            self.remove_model(term)

    def process_data(self, data, queue_force_update=False):
        if self.adjusting:
            self.q.append(data)
            return
        self.updating = True
        if data[0] == VOLPUB_REPLAY_END:
            self.log_event('received end-of-replay signal: %d.' % VOLPUB_REPLAY_END, 'info')
            self.log_event('checking known secids from volpublisher.', 'info')
            self.check_secids()
            self.log_event('done. now allowing model updates', 'info')
            self.allow_model_updates = True
        elif not self.allow_model_updates:
            self.known_secids.add(data[0])
            self.log_event('waiting for end-of-replay signal; received secid=%d, data=%0.5f' % (data[0], data[-1]), 'info')
        models_updated = self.update_data(*data, queue_force_update=queue_force_update)
        if models_updated:
            self.tv_adj.calculate_adjustments()
            self.send_all()
        self.updating = False

    def send(self, bytes):
        self.sock.send('MOD,' + bytes)

    def send_data(self, msgtype, terms=None):
        if terms is None:
            terms = self.next_model_update
        n = len(terms)
        if msgtype == MSGTYPE_FULL:
            self.tv_adj.create_matrices()
        for i, term in enumerate(terms):
            ts = now()
            self.send(self.pack(ts, self, term, msgtype, i, n))

    def send_all(self):
        self.send_data(MSGTYPE_FULL)

    def send_known(self):
        self.send_all()

    def send_adjustments(self, send_vols):
        if send_vols:
            self.send_all()
        else:
            self.send_data(MSGTYPE_ADJ)

    def send_heartbeat(self):
        if self.allow_model_updates:
            self.send_data(MSGTYPE_HEARTBEAT)

    def adjust_model(self):
        while self.updating:
            pass

        ts = now()
        self.adjusting = True
        #for secid, data in self.data_map.iteritems():
        #    self.process_data((secid, ts, data))
        self.adjusting = False

        while self.q:
            self.process_data(self.q.popleft(), queue_force_update=True)

    def kill(self):
        self.should_kill = True
        self.sock.close()
        self.context.destroy()

    def run_tv_adj(self):
        fills = self.tv_adj.fill_manager
        timeout = self.tv_adj.timeout
        self.log_event('checking fills every %0.3f seconds' % timeout, 'info')
        while 1:
            if self.should_kill:
                break

            fill_count, option_fill_count = 0, 0
            while not fills.is_empty():
                ts, fill = fills.pop()
                fill_id = fill[FILL_ID]
                secid = fill[FILL_SECURITYID]
                side = fill[FILL_SIDE]
                qty = (3 - 2*side) * fill[FILL_QTY]
                key = (
                    secid,
                    side,
                    fill_id,
                    fill[FILL_EXCHTRADEID],
                    fill[FILL_FILLTYPE]
                )
                if self.tv_adj.is_new_fill(key, fill[FILL_PRICE], qty):
                    num_trades, num_options = self.tv_adj.update_position(secid, qty)
                    fill_count += num_trades
                    option_fill_count += num_options
                    self.tv_adj.unique_fills.add(key)
                self.log_event('%d, %s' % (ts, fill), 'debug')

            if option_fill_count > 0:
                send_vols = self.tv_adj.calculate_adjustments(fill_id=fill_id)
                self.log_event('calculated new adjustments; send_vols=%s' % send_vols, 'debug')
                self.send_adjustments(send_vols)
                self.tv_adj.update_fill_count(fill_count)
            sleep(timeout)
        self.log_event('OPMTheoAdjuster is dead.', 'error')

    def run_heartbeat(self):
        timeout = 0.5
        while 1:
            if self.should_kill:
                break

            self.send_heartbeat()
            sleep(timeout)


class RawVolPublisher(OPMManager):
    def __init__(self, addr, sec_loader, model_defs, logger=None, trade_log_chan=None):
        OPMManager.__init__(self, sec_loader, model_defs, logger=logger)

        # open publishing channel
        context = zmq.Context()
        sock = context.socket(zmq.PUB)
        sock.bind(addr)
        self.sock = sock
        self.context = context

        if trade_log_chan is not None:
            trade_log = context.socket(zmq.PUB)
            trade_log.bind(trade_log_chan)
            self.trade_log = trade_log
        else:
            self.trade_log = None


        self.prev_spot_new_tick = False
        self.prev_spot_usable = False
        self.data_map = OrderedDict()
        self.updating = False
        self.replaying = False
        self.q = deque()
        self.debug_mode = model_defs['volPublisherDebugMode']
        self.acc_volumes_map = defaultdict(float64)

    def update_md(self, ts, md, book_size):
        if self.replaying:
            self.q.append((ts, md, book_size))
            return

        self.updating = True
        secid = md[MD_SECID]
        seqnum = md[MD_SEQNUM]
        acc_volume = md[MD_ACC_VOLUME]
        spot = self.spot
        spot.update_usability(ts)
        spot_usable = not isnan(spot.prc)

        if secid != self.spot_secid and not self.prev_spot_usable and spot_usable:
            for term, roll in self.rolls.items():
                roll.update_future(spot.prc)
                #self.update_roll_prc(term)
            self.send((self.spot_secid, ts, seqnum, spot.prc))
            self.prev_spot_usable = spot_usable

        if secid == self.spot_secid:
            spot.update_deio_md(ts, md, MD_LAST_IDX)
            spot_usable = not isnan(spot.prc)
            if self.prev_spot_usable and not spot_usable:
                self.send((secid, ts, seqnum, spot.prc))
            elif not self.prev_spot_usable and spot_usable:
                self.send((secid, ts, seqnum, spot.prc))
            if not isnan(spot.prc):
                for term, roll in self.rolls.items():
                    roll.update_future(spot.prc)
                    #self.update_roll_prc(term)
                self.data_map[secid] = seqnum, spot.prc
            self.spot_prc = spot.prc
        elif secid in self.spread_md:
            spread = self.spread_md[secid][0]
            spread.update_deio_md(ts, md, MD_LAST_IDX)
            if isnan(spread.prc):
                self.log_event('nan spread: %d, %0.3f, %s' % (secid, spread.prc, md), 'warn')
            else:
                self.data_map[secid] = seqnum, spread.prc
            self.send((secid, ts, seqnum, spread.prc))
        elif spot_usable and secid in self.models:
            model = self.models[secid]
            term = self.term_map[secid]
            roll_prc = self.roll_prcs[term]
            if isnan(roll_prc):
                spot_prc = self.calc_spot_prc(secid)
            else:
                spot_prc = self.calc_spot_prc(secid, roll_prc=roll_prc)
            if not isnan(spot_prc):
                model.update_vol_mdfwd(ts, secid, md, book_size, spot_prc, seqnum, self.spot.seqnum)
                rs = model.raw_option_dict[secid]
                vol = rs.vol
                #if term == 'ESOJ16':
                #    self.log_event('wat: %d, %s, %0.3f' % (secid, term, spot_prc), 'debug')
                if secid not in self.data_map or vol != self.data_map[secid][1]:
                    self.send((secid, ts, seqnum, vol))
                    self.data_map[secid] = seqnum, vol
                if term in self.rolls:
                    self.rolls[term].update_option(rs)
                    if self.update_roll_prc(term):
                        roll_secid = self.roll_secids[term]
                        roll_prc = self.roll_prcs[term]
                        self.send((roll_secid, ts, seqnum, roll_prc))
                        self.data_map[roll_secid] = seqnum, roll_prc
            # handle trade messages
            if acc_volume > self.acc_volumes_map[secid]:
                self.acc_volumes_map[secid] = acc_volume
                tdata = secid, ts, md[MD_TRADESIDE], md[MD_TRADEPRC], md[MD_TRADEQTY]
                self.send_trade(tdata_struct.pack(*tdata))
        self.prev_spot_usable = spot_usable
        self.updating = False

    def send_trade(self, tdata):
        if self.trade_log:
            self.trade_log.send(tdata)

    def update_from_arrays(self, secids, seqnums, data):
        ts = now()
        for secid, seqnum, dat in zip(secids, seqnums, data):
            if secid < 0 and dat == secid:
                continue
            self.log_event('updating data_map: secid=%d, seqnum=%d, dat=%0.5f' % (secid, seqnum, dat), 'info')
            self.send((secid, ts, seqnum, dat))
            self.data_map[secid] = seqnum, dat
        
    def send(self, data):
        self.sock.send_pyobj(data)
        if self.debug_mode:
            self.log_event('raw, %d, %d, %d, %f' % data, 'debug')

    def send_known(self):
        while self.updating:
            pass

        ts = now()
        self.replaying = True
        for secid, (seqnum, data) in self.data_map.items():
            self.send((secid, ts, seqnum, data))
        self.send((VOLPUB_REPLAY_END, ts, 0, VOLPUB_REPLAY_END))
        self.replaying = False

        while self.q:
            self.update_md(*self.q.popleft())

    def kill(self):
        self.sock.close()
        self.context.destroy()

