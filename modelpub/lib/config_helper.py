import os
import re
import json
import datetime as dt

from pandas import to_datetime


def init_configs(argv, today, suppress_output=False):
    try:
        pub_cfg_filename = argv[1]
        model_cfg_filename = argv[2]
    except IndexError:
        print('please specify publisher and model cfg filenames.')

    try:
        day_offset = argv[3]
    except IndexError:
        if not suppress_output:
            print(dt.datetime.now(), 'not using T+1 day adjust parameter')
        day_offset = 0

    which_path = os.path.join(os.path.dirname(os.__file__), os.pardir, os.pardir)

    cfg = read_config(pub_cfg_filename, model_cfg_filename, suppress_output)
    model_cfg = cfg['model']

    #if os.path.abspath(which_path) != os.path.abspath(cfg['correct_python_path']):
    #    print 'use correct python path (%s)' % cfg['correct_python_path']
    #    exit(1)

    today = to_datetime(today).date()
    today += dt.timedelta(int(day_offset))
    if not suppress_output:
        print(dt.datetime.now(), 'implied vols assuming today is', today)

    yday = today - dt.timedelta(days=1)
    while yday.weekday() > 4:
        yday -= dt.timedelta(days=1)

    return cfg, today, yday


def find_securities(f):
    s = re.compile('will always fwd md for security ([0-9]+)')
    return set(map(int, s.findall(f.read())))


def read_config(publisher_filename, model_filename, suppress_output):
    product = os.environ['PRODUCT']

    with open(publisher_filename, 'r') as f:
        cfg = json.load(f)

    with open(model_filename, 'r') as f:
        if not suppress_output:
            print(dt.datetime.now(), 'reading model config:', model_filename)
        model = json.load(f)
        model['product'] = product

    if not suppress_output:
        print(dt.datetime.now(), 'done.')

    mdfwd_cfg = cfg['mdfwd']
    mdfwd_iface = mdfwd_cfg['interface']
    mdfwd_addr = mdfwd_cfg['address']

    if not suppress_output:
        print(dt.datetime.now(), 'done. using address', mdfwd_addr, end=' ') 
        print('on interface', mdfwd_iface)

    secid_filename = os.path.expandvars(mdfwd_cfg['secidFilename'])
    if not suppress_output:
        print(dt.datetime.now(), 'finding relevant securityids in', secid_filename)
    with open(secid_filename, 'r') as f:
        secids = find_securities(f)
    if not suppress_output:
        print(dt.datetime.now(), 'done.')

    vol_cfg = model['volPublisher']
    opm_cfg = model['opmPublisher']
    data_cfg = model['dataReadWrite']

    vol_log_chan = os.path.expandvars(vol_cfg['logChannel'])
    vol_publisher_chan = os.path.expandvars(vol_cfg['channel'])
    vol_request_chan = os.path.expandvars(opm_cfg['volRequestChannel'])
    vols_filename = os.path.expandvars(data_cfg['volsFilename'])

    opm_log_chan = os.path.expandvars(opm_cfg['logChannel'])
    opm_publisher_chan = os.path.expandvars(opm_cfg['channel'])
    opm_request_chan = os.path.expandvars(opm_cfg['opmRequestChannel'])
    opm_filename = os.path.expandvars(data_cfg['opmFilename'])

    securities_filename = os.path.expandvars(data_cfg['securitiesFilename'])

    # if 'logTradeEventChannel' in cfg:
    trade_log_chan = cfg.get('logTradeEventChannel')

    replay_cmd = [os.path.expandvars(mdfwd_cfg['replayCommand'])]

    return {
        # log channel... do we keep this around?
        'vol_log_chan': vol_log_chan,
        'opm_log_chan': opm_log_chan,
        # recording stuff?
        'vols_filename': vols_filename,
        'opm_filename': opm_filename,
        # who am i? gethostname doesn't always work
        'securities_filename': securities_filename,
        # vol publisher relevant connections
        'vol_publisher_chan': vol_publisher_chan,
        'vol_request_chan': vol_request_chan,
        'fillfwd_addr': opm_cfg['fillFwdChannel'],
        # opm publisher
        'opm_publisher_chan': opm_publisher_chan,
        'opm_request_chan': opm_request_chan,
        # mdfwd parameters
        'mdfwd_iface': mdfwd_iface,
        'mdfwd_addr': mdfwd_addr,
        'mdfwd_replay': replay_cmd,
        # db connections
        'db_param': model['dbParameters'],
        #'secid_connection': cfg['securitiesConnection'],
        # other stuff
        'secids': secids,
        'model': model,
        'trade_log_chan': trade_log_chan
    }

