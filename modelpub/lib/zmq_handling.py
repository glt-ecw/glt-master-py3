import zmq

from collections import deque
from threading import Thread


class ZMQListener(Thread):
    def __init__(self, zmq_channel, topic, logger):
        self.zmq_channel = zmq_channel
        self.topic = topic
        # open socket
        context = zmq.Context()
        socket = context.socket(zmq.SUB)
        socket.connect(self.zmq_channel)
        socket.setsockopt(zmq.SUBSCRIBE, self.topic)
        self.socket = socket
        Thread.__init__(self, target=self.run)
        self.daemon = True
        self.stopped = False
        self.msgs = deque()
        self.logger = logger

    def __repr__(self):
        return 'ZMQListener{channel=%s, num_messages=%d}' % (self.zmq_channel, len(self.msgs))

    def stop(self):
        self.stopped = True
        self.socket.close()
        self.join()

    def run(self):
        self.logger.info('spawning thread for %s', self)
        while True:
            try:
                if self.stopped:
                    break
                msg = self.socket.recv()
                self.msgs.append(msg)
                self.logger.debug('received msg=%s on %s', msg, self)
            except:
                self.logger.exception('thread for %s died. closing 0mq socket', self)
                self.socket.close()
                break
        self.logger.info('stopped thread for %s', self)

    def latest_msgs(self):
        while self.msgs:
            msg = self.msgs.popleft()
            yield msg


