import zmq
import datetime as dt

from time import sleep

class LogPublisher:
    def __init__(self, addr, name, debug_mode=True):
        context = zmq.Context()
        sock = context.socket(zmq.PUB)
        sock.bind(addr)
        self.sock = sock
        self.context = context
        sleep(0.1)

        self.classifiers = {
            'warn': 'WARNING',
            'error': 'ERROR',
            'info': 'INFO',
            'debug': 'DEBUG'
        }
        self.name = name
        self.debug_mode = debug_mode
        self.write('starting log', 'info')

    def write(self, evt, level=None):
        if not self.debug_mode and evt == 'debug':
            return

        if level in self.classifiers:
            level = self.classifiers[level]
        else:
            level = 'ERROR'

        if type(evt) in (str, str):
            self._send(evt, level)
        elif type(evt) in (list, tuple):
            for line in evt:
                self._send(line, level)
        else:
            raise ValueError('need to be in string format', evt)

    def _send(self, evt, level):
        self.sock.send_string('%s (%s) : %s : %s' % (dt.datetime.now(), self.name, level, evt))

    def kill(self):
        self.write('ending log', 'info')
        self.sock.close()
        self.context.destroy()
