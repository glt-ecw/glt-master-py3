import numpy as np
import pandas as pd
import datetime as dt
import glt.db
import inspect

from collections import defaultdict, deque, OrderedDict
from itertools import product
from time import sleep, timezone, localtime
from threading import Thread
from struct import Struct, error as StructError
from glt.modeling.option_filters import RawOption
from glt.modeling.option_skews import BezierModel
from glt.modeling.tv_adjust import TvAdjust
from glt.tools import SuperSpotManager, SyntheticRollManager, now


# mdrepub fills
FILL_ID = 0
FILL_FILLTYPE = 1
FILL_SIDE = 2
FILL_SECURITYID = 3
FILL_USERID = 4
FILL_SERVERID = 5
FILL_PRICE = 6
FILL_QTY = 7
FILL_EXCHTRADEID = 8
FILL_ORDERTYPE = 9

# misc
BIZDAYS = 260.0

# mdfwd
MD_SECID = 1

# shared array
SH_ARR_SIZE = 15

SH_ARR_POS = 0
SH_ARR_VOL = 1
SH_ARR_TTE = 2
SH_ARR_MONEYNESS = 3
SH_ARR_VEGA = 4
SH_ARR_ADJ = 5
SH_ARR_K = 6
SH_ARR_IS_SM = 7
SH_ARR_CPOS = 8
SH_ARR_PPOS = 9
SH_ARR_CDELTA = 10
SH_ARR_PDELTA = 11
SH_ARR_CADJ = 12
SH_ARR_PADJ = 13
SH_ARR_VP = 14


def next_time(ts, interval):
    return interval * int(ts / interval + 1)


def output_model_reason(ts, secid, seqnum, st, et, term, model, spot_prc):
    compute_time = ('%0.1fms' % ((et-st)*1e-6)).zfill(7)
    output_fmt = [
        'updated model: %s, ts=%d, secid=%d, seqnum=%d, spot=%0.3f, n=%d, gain=%0.3f, compute time=%s'
    ]
    output_values = [
        (term, ts, secid, seqnum, spot_prc, len(model.smoothed_vol), model.gain, compute_time)
    ]
    for name, update_data, n_data in model.update_data:
        output_fmt.append('%s, pct off: %0.3f (out of %d)')
        output_values.append((name, len(update_data) / n_data, n_data))
        disp = [(id, model.rs_map[id].k, v, sv, dvp, vega, tdev) for id, (v, sv, dvp, vega, tdev) in update_data.items()]
        disp.sort(key=lambda x: x[1])
        for id, k, v, sv, dvp, vega, tdev in disp:
            dev = v-sv
            output_fmt.append(
                ('%d: k: %0.3f, raw: %0.5f, sm: %0.5f, dvp: %0.5f, '
                 'vol diff: %0.5f, vega: %0.1f, tick diff: %0.3f, model'
                 ' tdev: %0.3f')
            )
            output_values.append((id, k, v, sv, dvp, dev, vega, dev*vega, tdev))
    return [x[0] % x[1] for x in zip(output_fmt, output_values)]


class BezierModelRiskAdjusted(BezierModel):
    def __init__(self, raw_options, option_minqty, option_minpar,
            minPutDelta, minCallDelta, degree, band, band_pct, weightMethod,
            cycles_until_update=0, sections=2, exch=None, use_volpath=False, strike_type=None):
        BezierModel.__init__(self,
            raw_options, option_minqty, option_minpar,
            minPutDelta, minCallDelta, degree, band, band_pct, weightMethod,
            cycles_until_update=cycles_until_update, sections=sections, 
            exch=exch, use_volpath=use_volpath,
            strike_type=strike_type
        )
        # for vega management
        self.curr_vega = np.zeros(2, dtype=np.float64)
        self.last_vega = np.zeros(2, dtype=np.float64)
        self.vdiff = 0.0
        self.vega_threshold = np.nan
        self.vega_step = np.nan
        # for theta management
        self.curr_theta = np.zeros(2, dtype=np.float64)
        self.last_theta = np.zeros(2, dtype=np.float64)
        self.tdiff = 0.0
        self.theta_threshold = np.nan
        self.theta_step = np.nan
        
    def add_parameters(self, vega_threshold, theta_threshold, rgain, vega_step=0, theta_step=0):
        self.vega_threshold = vega_threshold
        self.vega_step = vega_step
        self.theta_threshold = theta_threshold
        self.theta_step = theta_step
        self.rgain = rgain

    def reset(self):
        self.vdiff = 0.0
        self.tdiff = 0.0
        self.last_vega[:] = self.curr_vega
        self.update_gain(1-self.rgain)

    def update_risk(self, sh_arr):
        if np.isnan(self.vega_threshold) or np.isnan(self.theta_threshold):
            raise ValueError('nan thresholds. use add_parameters to set thresholds')
        is_call = sh_arr[:, SH_ARR_MONEYNESS]>0
        # compute new vega exposure
        pos_vegas = 0.01 * sh_arr[:, SH_ARR_VEGA] * sh_arr[:, SH_ARR_POS]
        self.curr_vega[0] = pos_vegas[is_call].sum()
        self.curr_vega[1] = pos_vegas[~is_call].sum()
        self.vdiff = np.sum(np.abs(self.curr_vega-self.last_vega))
        # compute new theta exposure
        self.tdiff = np.sum(np.abs(self.curr_theta-self.last_theta))
        return (self.vdiff >= self.vega_threshold) or (self.tdiff >= self.theta_threshold)

    def __repr__(self):
        vpct = self.vdiff / self.vega_threshold
        tpct = self.tdiff / self.theta_threshold
        return (
            'BezierModelRiskAdjusted: vdiff=%0.2f (%0.3f), tdiff=%0.2f (%0.3f), '
            'vega: %s vs %s, theta: %s vs %s'
            '' % (
                self.vdiff, vpct, self.tdiff, tpct, 
                self.curr_vega.round(2), self.last_vega.round(2), 
                self.curr_theta.round(2), self.last_theta.round(2)
            )
        )


class SecurityLoader:
    def __init__(self, logger=None):
        self.logger = logger

    def log_event(self, evt, level):
        if self.logger:
            self.logger.write(evt, level)

    def build_objects(self, cfg, today):
        model_defs = cfg['model']
        secids = cfg['secids']
        #self.securities_conn_defn = cfg['secid_connection']
        self.today = today
        
        self.related_map = { model_defs['product']: model_defs['relatedContracts'] }
        futures_specs = model_defs['frontMonthFuture']
        self.options_product = str(model_defs['product'])
        self.interest_rate = model_defs['interestRate']

        # query contract specs
        self.query_contracts(secids, futures_specs['contract'], today)
        day_adjust = model_defs['dayAdjust']
        self.options['t'] -= day_adjust / BIZDAYS

        # make SpotManager
        futures_min_tick = futures_specs['minTick']
        futures_min_size = futures_specs['minSize']
        pause_window = 1e6 * futures_specs['pauseWindow']
        spot = SuperSpotManager(futures_min_tick, futures_min_size, pause_window)

        self.spot = spot
        self.fut_tick = 0.5 * futures_min_tick

    def as_dict(self):
        ignore_attrs = ['spot', 'logger']
        container = {}
        for attr in [x for x in dir(self) if '__' not in x]:
            if attr not in ignore_attrs and not inspect.ismethod(getattr(self, attr)):
                container[attr] = getattr(self, attr)
        return container

    def rebuild_objects(self, container):
        for attr, obj in container.items():
            setattr(self, attr, obj)
        self.build_spread_md()

    def build_spread_md(self):
        spread_md = {}
        for id, side in self.spread_md_specs:
            manager = SuperSpotManager(100.0, 0, -1)
            spread_md[id] = (manager, side)
        self.spread_md = spread_md

    def query_contracts(self, secids, futures_shortname, today):
        #try:
        #    conn = glt.db.conn(**self.securities_conn_defn)
        cmd = """
            SELECT securities.id, shortname, productcode AS product
            FROM securities
            LEFT JOIN contractDefinitions
            ON (securities.contractdefid=contractDefinitions.id)
            WHERE securities.id IN %s
        """ % (tuple(secids), )
        self.log_event('finding relevant securities', 'debug')
        self.log_event(cmd, 'debug')
        contract_defs = glt.db.autoclose_query(cmd)
        #finally:
        #    conn.close()
        base_products = list(filter(self.related_map.get, contract_defs['product'].unique()))

        related_ids = {}
        #try:
        #    conn = glt.db.conn(**self.securities_conn_defn)
        for prod in base_products:
            for rprod, ptval in self.related_map[prod]:
                names = contract_defs.loc[contract_defs['product']==prod]
                related = names['shortname'].str.replace(prod, rprod)
                cmd = """
                    SELECT shortname, id
                    FROM securities
                    WHERE shortname IN %s
                """ % (tuple(related.apply(str)), )
                self.log_event('finding related contracts', 'debug')
                self.log_event(cmd, 'debug')
                df = glt.db.autoclose_query(cmd)
                df.index = df['shortname']
                new_ids = df.loc[related.values, 'id'].dropna().values.tolist()
                ptvals = list(product(names['id'], (ptval, )))
                related_ids.update(dict(list(zip(new_ids, ptvals))))
        #finally:
        #    conn.close()

        # find front-month futures
        #try:
        #    conn = glt.db.conn(**self.securities_conn_defn)
        cmd = """
            SELECT id, shortname
            FROM securities
            WHERE (
                id in (%s)
                AND
                shortname='%s'
            )
        """ % (','.join(map(str, secids)), futures_shortname)
        self.log_event('finding front-month futures', 'debug')
        self.log_event(cmd, 'debug')
        front_month = glt.db.autoclose_query(cmd)
        if not front_month.shape[0]:
            raise ValueError
        self.spot_secid = front_month.iloc[0]['id']
        #finally:
        #    conn.close()

        # figure out options contracts
        #try:
        #conn = glt.db.conn(**self.securities_conn_defn)
        cmd = """
            SELECT
                secs.id as call_id,
                expdate as expiry,
                underlyingid as uid,
                strike as k,
                shortname as name
            FROM (
                SELECT id, expdate, strike, underlyingid
                FROM options
                WHERE optiontype=1
                AND id IN %s
            ) AS calls
            LEFT JOIN (
                SELECT id, shortname
                FROM securities
                WHERE id IN %s
            ) AS secs
            ON (calls.id=secs.id)
            ORDER BY expdate, strike
        """ % (tuple(secids), tuple(secids))
        self.log_event('querying calls', 'debug')
        self.log_event(cmd, 'debug')
        calls = glt.db.autoclose_query(cmd)
        cmd = """
            SELECT
                id as put_id,
                expdate as expiry,
                strike as k
            FROM options
            WHERE optiontype=2
            AND id in %s
            ORDER BY expdate, strike
        """ % (tuple(secids), )
        self.log_event('querying puts', 'debug')
        self.log_event(cmd, 'debug')
        puts = glt.db.autoclose_query(cmd)
        #finally:
        #    conn.close()

        options = pd.merge(calls, puts, on=['expiry', 'k'], how='inner')
        expiries = options['expiry'].values #.tolist()
        options['t'] = options['expiry'].map(
            lambda d: (1.0+np.busday_count(today, d.date())) / BIZDAYS
        )

        # do we have to work with the spreads? fine...
        uids = options['uid'].unique()
        #try:
        #conn = glt.db.conn(**self.securities_conn_defn)
        if len(uids) == 1:
            query_uids = '(%d)' % uids[0]
        else:
            query_uids = tuple(uids)
        cmd = """
            SELECT 
                secs.id as uid,
                shortname as name,
                expdate as expiry
            FROM (
                SELECT id, expdate
                FROM futures
                WHERE id IN %s
            ) as futs
            LEFT JOIN (
                SELECT id, shortname
                FROM securities
                WHERE id in %s
            ) as secs
            ON (futs.id=secs.id)
            ORDER BY expdate
        """ % (query_uids, query_uids)
        self.log_event('querying underlying ids', 'debug')
        self.log_event(cmd, 'debug')
        underlyings = glt.db.autoclose_query(cmd)
        underlyings['idx'] = underlyings.index
        #finally:
        #    conn.close()
        print(underlyings)
        print(self.spot_secid)
        fm_idx = underlyings.loc[underlyings['uid']==self.spot_secid, 'idx'].iloc[0]
        underlyings = underlyings.loc[underlyings['idx']!=fm_idx]
        underlyings['spreadname'] = None
        underlyings['spreadside'] = 0
        sbool = underlyings['idx']<fm_idx
        underlyings.loc[sbool, 'spreadname'] = underlyings['name'] + '-' + futures_shortname
        underlyings.loc[sbool, 'spreadside'] = -1.0
        underlyings.loc[~sbool, 'spreadname'] = futures_shortname + '-' + underlyings['name']
        underlyings.loc[~sbool, 'spreadside'] = 1.0

        imply_spreads = {}
        spread_md_specs = []
        if underlyings.shape[0]:
            #try:
            #conn = glt.db.conn(**self.securities_conn_defn)
            if underlyings.shape[0] == 1:
                cmd = """
                    SELECT shortname, id
                    FROM securities
                    WHERE shortname='%s'
                """ % (underlyings.iloc[0]['spreadname'], )
            else:
                cmd = """
                    SELECT shortname, id
                    FROM securities
                    WHERE shortname in %s
                """ % (tuple(underlyings['spreadname'].astype(str)), )
            self.log_event('finding relevant spreads', 'debug')
            self.log_event(cmd, 'debug')
            spreads = glt.db.autoclose_query(cmd)
            spreads_map = dict(spreads.values)
            #finally:
            #    conn.close()
            underlyings['spreadid'] = underlyings['spreadname'].apply(spreads_map.get)
            uids = underlyings['uid'].values
            spreadids = underlyings['spreadid'].values
            sides = underlyings['spreadside']
            spread_md_specs = list(zip(spreadids, sides))
            imply_spreads = {k: id for k, id in zip(uids, spreadids)}

        self.secids = secids
        self.spread_md_specs = spread_md_specs
        self.imply_spreads = imply_spreads
        self.related_ids = related_ids
        self.options = options


class OPMManager:
    def __init__(self, sec_loader, model_defs, conn_defn=None, start_time=None, logger=None):
        self.logger = logger
        option_defs = sec_loader.options
        option_defs.index = option_defs['call_id']
        #del option_defs['call_id']

        col_conv = model_defs['volTable']['columnConvert']
        server = model_defs['volTable']['server']
        imply_spreads = sec_loader.imply_spreads
        ir = sec_loader.interest_rate
        self.ir = ir

        model_specs = model_defs['models']
        option_specs = model_defs['optionsMarketSpecification']
        contract_defs = model_defs['optionsContracts']

        use_risk_adjusted_updates = model_defs['useRiskAdjustedUpdates']
        if use_risk_adjusted_updates:
            BezierObject = BezierModelRiskAdjusted
        else:
            BezierObject = BezierModel

        fut_tick = sec_loader.fut_tick

        models, model_intervals = {}, {}
        next_model_update = OrderedDict()
        last_underlyings = {}
        roll_prcs = defaultdict(np.float64)
        model_roll_prcs = defaultdict(np.float64)
        rolls, imply_spot = {}, {}
        roll_secids = {}

        shared_arrays = OrderedDict()
        term_map, c2idx_map, c2p_map, p2c_map = {}, {}, {}, {}
        t2c_map = {}
        for param in contract_defs:
            term = param['contract']
            # reference which model?
            ref = param['modelReference']
            if ref not in model_specs:
                raise KeyError
            specs = model_specs[ref]
            # generate options list
            options = option_defs.loc[option_defs.name.str.contains(term)]
            if not options.shape[0]:
                self.log_event('no security definitions found for %s. bypassing.' % term, 'warn')
                continue
            calls = options.index.values
            puts = options['put_id'].values
            raw_options = OPMManager.init_raw_options(
                RawOption, specs, option_specs, options, fut_tick, ir
            )
            # use spread prices?
            use_spread = param['useSpread']
            uid = options['uid'].iloc[0]
            if use_spread and uid in imply_spreads:
                spreadid = imply_spreads[uid]
                imply_spot.update({id: spreadid for id, _ in raw_options})
                imply_spot[term] = spreadid
            # create model for term key
            model = OPMManager.init_bezier_model(
                BezierObject, specs, raw_options, sec_loader.options_product
            )
            # what should we use for roll prices?
            roll_param = param['rollParameters']
            roll_secid = roll_param['id']
            if roll_secid is not None:
                self.log_event('syntheticUnderlyingId: %d' % roll_secid, 'debug')
                if hasattr(sec_loader, 'spot'):
                    self.log_event('creating synthetic roll manager for %s' % term, 'info')
                    strike_boundary = roll_param['strikeWidthBoundary']
                    dev = roll_param['updateThreshold']
                    rolls[term] = SyntheticRollManager(model, strike_boundary, dev)
                roll_prcs[term] = np.nan #rolls[term].get_roll()
                model_roll_prcs[term] = 0.0
                roll_secids[term] = roll_secid
                roll_secids[roll_secid] = term
            # start the model off right!
            try:
                if conn_defn is not None and start_time is not None:
                    k_map = {col_conv%k: id for id, k in options['k'].items()}
                    st_spot, st_vols = glt.db.query_deio_smoothed_vols(
                        conn_defn, term, start_time, k_map.get, model, server
                    )
                    if not st_vols:
                        self.log_event('no vols found for %s in db. moving on.' % term, 'warn')
                    else:
                        self.log_event('found data for %s: spot=%0.3f, n_vols=%d' % (term, st_spot, len(st_vols)), 'info')
                        model.force_values(st_spot, st_vols)
                        last_underlyings[term] = st_spot
            except ValueError as inst:
                self.log_event('not forcing vols for %s' % term, 'warn')
            if term not in last_underlyings:
                last_underlyings[term] = np.nan
            # update some maps
            models.update((id, model) for id, _ in raw_options)
            term_map.update((id, term) for id, _ in raw_options)
            models[term] = model
            model_intervals[term] = specs['updateInterval'] * 1e6
            st_now = 0
            if start_time is not None:
                is_dst = localtime().tm_isdst
                st_now = pd.to_datetime(start_time).value + (timezone - is_dst * 3600)*1e9
                self.log_event('using start time: %s (%dns from epoch)' % (start_time, st_now), 'info')
                self.log_event('equivalent start time: %s' % pd.to_datetime(st_now), 'info')
            next_model_update[term] = next_time(st_now, 1)
            c2idx_map.update((c, i) for i, c in enumerate(calls))
            c2p_map.update(list(zip(calls, puts)))
            p2c_map.update(list(zip(puts, calls)))
            t2c_map[term] = calls
            # generate master array elements
            # idx:
            #   0 = positions
            #   1 = vols
            #   2 = time to expiration
            #   3 = moneyness
            #   4 = vega
            #   5 = adjustments
            arr = np.zeros((options.shape[0], SH_ARR_SIZE), dtype=np.float64)
            arr[:, SH_ARR_TTE] = options['t']
            arr[:, SH_ARR_K] = options['k']
            shared_arrays[term] = arr
            self.log_event('BezierModel for %s constucted, %s, strike_type=%d' % (term, model, model.strike_type), 'info')
            self.log_event('gain used for smoothed vol updates: %0.3f' % model.gain, 'info')
            if model.__class__ == BezierModelRiskAdjusted:
                ra_param = specs['riskAdjustParameters']
                vthresh = ra_param['vegaThresholdInTicks']
                vstep = ra_param['vegaStep']
                tthresh = ra_param['thetaThresholdInTicks']
                tstep = ra_param['thetaStep']
                rgain = ra_param['riskGain']
                model.add_parameters(
                    vega_threshold=vthresh, theta_threshold=tthresh, 
                    vega_step=vstep, theta_step=tstep,
                    rgain=rgain)
                self.log_event('BezierModelRiskAdjusted initiated.', 'info')
                self.log_event(
                    (
                        'vega_threshold=%0.2f (step=%0.2f), theta_threshold=%0.2f (step=%0.2f), rgain=%0.2f'
                        '' % (vthresh, vstep, tthresh, tstep, rgain)
                    ), 'info'
                )

        self.imply_spot = imply_spot
        self.spread_md = sec_loader.spread_md
        self.roll_prcs = roll_prcs
        self.model_roll_prcs = model_roll_prcs
        self.rolls = rolls
        self.roll_secids = roll_secids

        self.spot_secid = sec_loader.spot_secid
        if hasattr(sec_loader, 'spot'):
            self.spot = sec_loader.spot
        self.spot_prc = np.nan
        self.last_good_spot_prc = np.nan
        self.term_map = term_map
        self.c2idx_map = c2idx_map
        self.c2p_map = c2p_map
        self.p2c_map = p2c_map
        self.t2c_map = t2c_map

        self.product = sec_loader.options_product
        self.models = models
        self.model_intervals = model_intervals
        self.next_model_update = next_model_update
        self.shared_arrays = shared_arrays
        self.last_underlyings = last_underlyings

        for term in model_intervals:
            self.update_array(term, models[term])

        self.option_defs = option_defs.loc[option_defs['call_id'].isin(c2idx_map)]
        self.force_update = False
        self.allow_model_updates = False

    def log_event(self, evt, level):
        if self.logger:
            self.logger.write(evt, level)

    def remove_model(self, term):
        if term in self.next_model_update:
            del self.models[term]
            del self.model_intervals[term]
            del self.next_model_update[term]
            del self.shared_arrays[term]
            if term in self.roll_prcs:
                del self.roll_prcs[term]
                del self.model_roll_prcs[term]
            del self.t2c_map[term]

    @staticmethod
    def init_raw_options(RawOptionClass, specs, option_specs, option_defs, fut_tick, ir):
        # kalman parameters for thin markets
        kalman_q = specs['kalmanQ']
        kalman_r = specs['kalmanR']
        # tick information to determine option price/tv rounding
        nan_bidval = option_specs['nanBidValue']
        large_tick = option_specs['largeTick']
        s_round = option_specs['smallRound']
        l_round = option_specs['largeRound']
        tv_precision = specs['tvPrecision']
        raw_options = []
        if RawOptionClass is None:
            return raw_options
        for call_id, row in option_defs.iterrows():
            put_id = row['put_id']
            k = row['k']
            t = row['t']
            for j, id in enumerate((call_id, put_id)):
                cp = -1 if j > 0 else 1
                rs = RawOptionClass(
                    cp, k, t, ir, fut_tick, s_round, l_round, large_tick, 
                    tv_precision, kalman_q, kalman_r
                )
                raw_options.append((id, rs))
        return raw_options

    @staticmethod
    def init_bezier_model(BezierModelClass, specs, raw_options, opt_product):
        # minimum thresholds to determine "good" options market
        option_minqty = specs['minQty']
        option_minpar = specs['minPar']
        # bezier specific parameters
        p_delta = specs['minPutDelta']
        c_delta = specs['minCallDelta']
        update_interval = specs['updateInterval'] * 1e-3
        degree = specs['bezierDegree']
        band_width = specs['bandWidth']
        band_pct = specs['bandPct']
        weight_type = str(specs['weightType'])
        strike_type = str(specs['strikeType'])
        cycles_until_update = specs['cyclesUntilUpdate']
        sections = specs['sections']
        use_volpath = specs['useVolpath']
        model = BezierModelClass(
            raw_options, option_minqty, option_minpar, 
            p_delta, c_delta, degree, 
            band_width, band_pct, weight_type, 
            cycles_until_update=cycles_until_update,
            sections=sections,
            exch=opt_product, use_volpath=use_volpath,
            strike_type=strike_type
        )
        return model
        
    def update_array(self, term, model):
        spot_prc = model.spot
        sh_arr = self.shared_arrays[term]
        # calculate moneyness for everything
        sh_arr[:, SH_ARR_MONEYNESS] = np.log(model.strikes/spot_prc) / np.sqrt(sh_arr[:, SH_ARR_TTE])
        sh_arr[:, SH_ARR_IS_SM] = 0
        # save vols and vegas
        calls = self.t2c_map[term]
        sm_vols = model.smoothed_vol
        vegas = model.vega
        vp = model.volpath
        rs_map = model.rs_map
        minbound, maxbound = None, None
        rs_otm = model.rs_otm
        for i, cid in enumerate(calls):
            if cid in sm_vols:
                vol = sm_vols[cid]
                sh_arr[i, SH_ARR_VOL] = vol
                sh_arr[i, SH_ARR_VEGA] = vegas[cid]
                sh_arr[i, SH_ARR_IS_SM] = 1
                sh_arr[i, SH_ARR_VP] = vp[cid]
                if sh_arr[i, SH_ARR_MONEYNESS] > 0:
                    sh_arr[i, SH_ARR_CDELTA] = rs_map[cid].calc_delta(spot_prc, vol)
                    sh_arr[i, SH_ARR_PDELTA] = -1 + rs_map[cid].calc_delta(spot_prc, vol)
                else:
                    sh_arr[i, SH_ARR_CDELTA] = 1 + rs_map[cid].calc_delta(spot_prc, vol)
                    sh_arr[i, SH_ARR_PDELTA] = rs_map[cid].calc_delta(spot_prc, vol)
                if minbound is None:
                    minbound = i
                    sh_arr[:minbound, SH_ARR_VOL] = vol
                    for j in range(minbound):
                        sh_arr[j, SH_ARR_VEGA] = rs_otm[j].calc_vega(spot_prc, vol)
                        if sh_arr[j, SH_ARR_MONEYNESS] > 0:
                            sh_arr[j, SH_ARR_CDELTA] = rs_otm[j].calc_delta(spot_prc, vol)
                            sh_arr[j, SH_ARR_PDELTA] = -1 + rs_otm[j].calc_delta(spot_prc, vol)
                        else:
                            sh_arr[j, SH_ARR_CDELTA] = 1 + rs_otm[j].calc_delta(spot_prc, vol)
                            sh_arr[j, SH_ARR_PDELTA] = rs_otm[j].calc_delta(spot_prc, vol)
            elif minbound is not None and maxbound is None:
                maxbound = i
                vol = sh_arr[maxbound-1, SH_ARR_VOL]
                sh_arr[maxbound:, SH_ARR_VOL] = vol
                for j in range(maxbound, sh_arr.shape[0]):
                    sh_arr[j, SH_ARR_VEGA] = rs_otm[j].calc_vega(spot_prc, vol)
                    if sh_arr[j, SH_ARR_MONEYNESS] > 0:
                        sh_arr[j, SH_ARR_CDELTA] = rs_otm[j].calc_delta(spot_prc, vol)
                        sh_arr[j, SH_ARR_PDELTA] = -1 + rs_otm[j].calc_delta(spot_prc, vol)
                    else:
                        sh_arr[j, SH_ARR_CDELTA] = 1 + rs_otm[j].calc_delta(spot_prc, vol)
                        sh_arr[j, SH_ARR_PDELTA] = rs_otm[j].calc_delta(spot_prc, vol)

    def check_bands(self, ts, term, spot_prc, force_update=False):
        self.next_model_update[term] = next_time(ts, self.model_intervals[term])
        st, et = now(), 0
        try:
            model = self.models[term]
            model.check_bands(spot_prc, force_update=force_update)
            if model.updated:
                self.update_array(term, model)
                self.model_roll_prcs[term] = self.roll_prcs[term]
                et = now()
        except ValueError as inst:
            self.log_event('ValueError for %s. skipping.' % term, 'warn')
            self.log_event('%s' % inst, 'warn')
            for rs in model.rs_otm:
                self.log_event('k=%0.3f, vol=%0.5f, delta=%0.3f' % (rs.k, rs.vol, rs.calc_delta(spot_prc, rs.vol)), 'warn')
        if not model.updated:
            model = None
        return model, st, et

    def update_data(self, secid, ts, seqnum, dat, queue_force_update=False):
        # did you change the model parameters? if so, we need to force an update
        if queue_force_update:
            self.force_update = True
        models_updated = []
        # evaluate model at set interval
        if self.allow_model_updates and not self.spot_isnan():
            for term in self.next_model_update:
                model_ts = self.next_model_update[term]
                spot_prc = self.calc_spot_prc(term, roll_prc=self.roll_prcs[term])
                self.last_underlyings[term] = spot_prc
                if ts >= model_ts:
                    model, st, et = self.check_bands(ts, term, spot_prc, force_update=self.force_update)
                    if model:
                        models_updated.append(term)
                        self.log_event(
                            output_model_reason(
                                ts, secid, seqnum, st, et, term, model, self.last_underlyings[term]
                            ), 'debug'
                        )
        # now update md or vol
        if secid == self.spot_secid:
            self.spot_prc = dat
            if not np.isnan(dat):
                self.last_good_spot_prc = dat
        elif secid in self.roll_secids:
            term = self.roll_secids[secid]
            self.roll_prcs[term] = dat
            self.log_event('processing roll: secid=%d (%s), prc=%0.3f' % (secid, term, dat), 'debug')
        elif secid in self.spread_md:
            self.spread_md[secid][0].force_price(dat)
        elif secid in self.models:
            model = self.models[secid]
            model.force_rawvol(secid, dat)
        else:
            self.log_event('unknown secid: %d' % secid, 'warn')
        # we forced the model to update because some asshole changed the parameters
        if models_updated and self.force_update:
            self.log_event('forced update is on. models updated: %d' % len(models_updated), 'debug')
            self.force_update = False
        return models_updated
             
    def calc_spot_prc(self, key, roll_prc=0):
        spot_prc = self.spot_prc + roll_prc
        if key in self.imply_spot:
            spread = self.spread_md[self.imply_spot[key]]
            spot_prc += spread[1] * spread[0].prc
        return spot_prc

    def spot_isnan(self):
        return np.isnan(self.spot_prc)

    def roll_isnan(self):
        return np.isnan(sum(self.roll_prcs.values()))

    def any_isnan(self):
        return self.spot_isnan() or self.roll_isnan()

    def update_roll_prc(self, term):
        new_roll_prc = self.rolls[term].get_roll()
        if not np.isnan(new_roll_prc) and new_roll_prc != self.roll_prcs.get(term):
            self.roll_prcs[term] = new_roll_prc
            return True
        return False


class OPMTheoAdjuster(TvAdjust):
    def __init__(self, sec_loader, model_defs, fill_manager, logger=None):
        self.logger = logger

        # tv config params
        option_specs = model_defs['optionsMarketSpecification']
        tv_config = model_defs['tvAdjust']
        contract_defs = model_defs['optionsContracts']

        self.max_adjustment = tv_config['maxAdjustment']
        self.wing_factor = tv_config['wingFactor']
        self.width_factor = tv_config['widthFactor']
        self.time_factor = tv_config['timeFactor']     
        self.cap_pct = tv_config['vegaCapPercentage']
        self.strike_inc = option_specs['strikeIncrement']
        self.timeout = 1e-3 * tv_config['fillRetrievalInterval']
        
        TvAdjust.__init__(
            self, self.max_adjustment, self.width_factor, self.wing_factor, 
            self.time_factor, self.cap_pct, self.strike_inc
        )

        # synthetic adjustments?
        synthetic_penny_adj = defaultdict(np.float64)
        for param in contract_defs:
            term = param['contract']
            roll_param = param['rollParameters']
            if roll_param['id'] is not None:
                synthetic_penny_adj[term] = roll_param['pennyAdjustSize']
        self.synthetic_penny_adj = synthetic_penny_adj

        self.fill_manager = fill_manager

        self.inventory = defaultdict(np.float64)
        self.should_kill = False

        self.secids = sec_loader.secids
        self.related_ids = sec_loader.related_ids
        self.options = sec_loader.options

        self.fill_count = 0
        self.unique_fills = set()

        # new risk-adjusted updates
        use_risk_adjusted_updates = model_defs['useRiskAdjustedUpdates']
        if use_risk_adjusted_updates:
            self.calculate_adjustments = self.calculate_risk_adjusted_model
        else:
            self.calculate_adjustments = self.calculate_standard_adjustments

    def log_event(self, evt, level):
        if self.logger:
            self.logger.write(evt, level)

    def load_positions(self, db_param, date):
        conn_defn = db_param['connection']
        try:
            conn = glt.db.conn(**conn_defn)
            cmd = """
                SELECT securityid, sum(position) AS pos
                FROM positions
                WHERE tradedate='%s'
                AND securityid IN %s
                GROUP BY securityid
            """ % (date, tuple(self.secids.union(self.related_ids)))
            positions = conn.frame_query(cmd)
        finally:
            conn.close()

        if not positions.shape[0]:
            return

        positions.index = positions['securityid']
        positions = positions['pos']

        count = 0
        for secid, pos in positions.items():
            num_trades, _ = self.update_position(secid, pos)
            count += num_trades
        self.log_event('loaded %d positions.' % count, 'info')

    def load_fills(self, db_param, date):
        conn_defn = db_param['connection']
        table = db_param['fillsTable']
        instance = db_param['fillsInstance']
        try:
            conn = glt.db.conn(**conn_defn)
            cmd = """
                SELECT
                    securityid, side, id, exchtradeid, filltype,
                    (3 - 2*side) * qty as fillqty, price
                FROM %s.fills
                WHERE tradedate='%s'
                AND instance='%s'
            """ % (table, date, instance)
            fills = conn.frame_query(cmd)
        finally:
            conn.close()

        if fills.shape[0] == 0:
            return

        keys = list(zip(
            fills['securityid'].values,
            fills['side'].values,
            fills['id'].values,
            fills['exchtradeid'].values,
            fills['filltype'].values
        ))
        prices = fills['price'].values
        qtys = fills['fillqty'].values

        count = 0
        for key, price, qty in zip(keys, prices, qtys):
            if self.is_new_fill(key, price, qty):
                num_trades, _ = self.update_position(key[0], qty)
                count += num_trades
                self.unique_fills.add(key)
        self.fill_count += count
        self.log_event('loaded %d fills from db.' % count, 'info')

    def is_new_fill(self, key, price, qty):
        is_fake = price == 0 and key[3] == ''
        return not is_fake and key not in self.unique_fills

    def update_fill_count(self, fill_count):
        self.fill_count += fill_count

    def update_position(self, secid, qty):
        if secid in self.related_ids:
            secid, ptval = self.related_ids[secid]
            qty *= ptval
        if secid not in self.secids:
            return 0, 0
        self.inventory[secid] += qty
        return 1, self.update_array(secid, qty)

    def update_array(self, secid, qty):
        if secid in self.p2c_map:
            secid = self.p2c_map[secid]
            qty_i = SH_ARR_PPOS
        else:
            qty_i = SH_ARR_CPOS
        if secid in self.c2idx_map:
            term = self.term_map[secid]
            self.shared_arrays[term][self.c2idx_map[secid], SH_ARR_POS] += qty
            self.shared_arrays[term][self.c2idx_map[secid], qty_i] += qty
            return 1
        return 0

    def calculate_synthetic_adjustments(self, term):
        if self.synthetic_penny_adj[term] == 0:
            return 0, 0
        sh_arr = self.shared_arrays[term]
        put_side = np.dot(sh_arr[:, SH_ARR_PDELTA], sh_arr[:, SH_ARR_PPOS])
        call_side = np.dot(sh_arr[:, SH_ARR_CDELTA], sh_arr[:, SH_ARR_CPOS])
        adj = -0.01 * (put_side + call_side) / self.synthetic_penny_adj[term]
        return adj * sh_arr[:, SH_ARR_CDELTA], adj * sh_arr[:, SH_ARR_PDELTA]

    # calculating adjustments must return boolean value
    # so it knows which message type to send (if standard, always False;
    # if risk_adjusted, sometimes we need to send updated vols)
    def calculate_standard_adjustments(self, **kwargs):
        self.log_event('calculating standard adjustments', 'debug')
        self.create_adjustments(self.shared_arrays, self.last_underlyings)
        # add handling for synthetic roll adjustments based on computed deltas
        for term, sh_arr in self.shared_arrays.items():
            call_adj, put_adj = self.calculate_synthetic_adjustments(term)
            sh_arr[:, SH_ARR_CADJ] = sh_arr[:, SH_ARR_ADJ] + call_adj
            sh_arr[:, SH_ARR_PADJ] = sh_arr[:, SH_ARR_ADJ] + put_adj
        return False

    def calculate_risk_adjusted_model(self, fill_id=None, **kwargs):
        if fill_id is None:
            return False

        send_vols = False
        for term, sh_arr in self.shared_arrays.items():
            model = self.opm.models[term]
            if model.update_risk(sh_arr):
                spot_prc = self.last_underlyings[term]
                self.log_event('%s (before reset): %s' % (term, model), 'debug')
                model.update_gain(1.0)
                ts = now()
                model, st, et = self.opm.check_bands(ts, term, spot_prc, force_update=True)
                if model:
                    self.log_event(
                        output_model_reason(ts, 0, fill_id, st, et, term, model, spot_prc), 'debug'
                    )
                model.reset()
                if not send_vols:
                    send_vols = True
                self.log_event('%s (after reset): %s' % (term, model), 'debug')
            else:
                self.log_event('%s (no reset): %s' % (term, model), 'debug')
            # now compute standard adjustments
            self.calculate_standard_adjustments()
        return send_vols

    def kill(self):
        self.should_kill = True

    def ref_model_objects(self, opm):
        self.term_map = opm.term_map
        self.t2c_map = opm.t2c_map
        self.c2idx_map = opm.c2idx_map
        self.p2c_map = opm.p2c_map
        self.shared_arrays = opm.shared_arrays
        self.last_underlyings = opm.last_underlyings
        # I gave up
        self.opm = opm
        # what is our current inventory?
        #for secid, qty in self.inventory.iteritems():
        #    self.update_array(secid, qty)

