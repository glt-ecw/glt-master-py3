import zmq
import glt.db

from time import time, sleep
from numpy import nan, isnan
from collections import deque, namedtuple, OrderedDict
from struct import Struct
from threading import Thread, Lock
from glt.publishing.modelpub import OPMSharedArray
from glt.modeling.tv_adjust import TvAdjust

# local
from .zmq_handling import ZMQListener
from .fill_handling import MDRepubFillHandler


lock = Lock()


MSGTYPE_FULL = 1
MSGTYPE_ADJUST = 2
MSGTYPE_HEARTBEAT = 3


Timestamp = namedtuple('Timestamp', ['sec', 'usec'])


def timestamp_now():
    ts = time()
    return Timestamp(int(ts), int(1000000 * (ts%1)))


"""
    const static unsigned char PUB_MSG_FULL = 1;
    const static unsigned char PUB_MSG_ADJSTS = 2;
    const static unsigned char PUB_MSG_HEARTBEAT = 3;

    struct PubHeader {
        unsigned int    modelId;
        unsigned int    seqNum;
        TimeStamp       ts;
        double          underlyingMidPrice;
        double          underlyingMidAtLastCurveUpdate;
        double          syntheticOffset;
        double          yearsToExpiration;
        double          interestRate;
        unsigned long   triggerId;
        unsigned short  numEntries;
        unsigned char   msgType;
        unsigned char   pedIdx;
        bool            isLastUpdate;
        bool            callCallBacks;
    };

    struct PubFullEntry {
        unsigned int securityId;
        double volatility;
        double atvCallAdjustment;
        double atvPutAdjustment;
        double volpath;

        void set(const StrikeEntry &se) {
            securityId = (se.call.data.securityId) ? se.call.data.securityId : se.put.data.securityId;
            volatility = se.volatility;
            atvAdjustment = se.atvAdjustment;
        }
    };

    struct PubAdjustEntry {
        unsigned int securityId;
        double atvCallAdjustment;
        double atvPutAdjustment;

        void set(const StrikeEntry &se) {
            securityId = (se.call.data.securityId) ? se.call.data.securityId : se.put.data.securityId;
            atvAdjustment = se.atvAdjustment;
        }
    };
"""

class PubHeader(Struct):
    def __init__(self, model_id, yte, interest_rate, ped_idx, 
            num_entries, is_last_update):
        # 15 means something to Deio... I don't remember
        self.firstval = 15
        #self.total_size = ?
        self.model_id = model_id
        self.seqnum = 0
        #self.ts = TimeStamp(0, 0)
        #self.underlying_midprice = nan
        #self.underlying_mid_at_last_update = nan
        #self.synthetic_offset = nan
        self.yte = yte
        self.interest_rate = interest_rate
        #self.triggerid = 0
        self.num_entries = num_entries
        #self.msg_type = MSGTYPE_HEARTBEAT
        self.ped_idx = ped_idx
        self.is_last_update = is_last_update
        # for packing
        Struct.__init__(self, '<BIIIQqdddddQHBBI')

    def pack_data(self, msg_type, underlying_midprc=nan, underlying_mid_at_last_update=nan, 
            synthetic_offset=nan, triggerid=0):
        self.seqnum += 1
        ts = timestamp_now()
        num_entries = 0 if msg_type==MSGTYPE_HEARTBEAT else self.num_entries
        entry_struct = PubEntry.full_struct if msg_type == MSGTYPE_FULL else PubEntry.adjust_struct
        total_size = self.size + num_entries * entry_struct.size - 5
        self.total_size = total_size
        return self.pack(
            self.firstval, total_size, self.model_id, self.seqnum, ts.sec, ts.usec,
            underlying_midprc, underlying_mid_at_last_update, synthetic_offset, self.yte,
            self.interest_rate, triggerid, num_entries, msg_type,
            self.ped_idx, self.is_last_update
        )


class PubEntry:
    full_struct = Struct('Idddd')
    adjust_struct = Struct('Idd')

    def __init__(self, secid):
        self.secid = secid

    def pack_full_entry(self, vol, vp, call_adjust, put_adjust):
        return self.full_struct.pack(self.secid, vol, call_adjust, put_adjust, vp)
    
    def pack_adjust_entry(self, call_adjust, put_adjust):
        return self.adjust_struct.pack(self.secid, call_adjust, put_adjust)


class ModelPacker(PubHeader):
    def __init__(self, secids, opm_shared_arr, model_id, yte, interest_rate,
            ped_idx, is_last_update):
        self.secids = secids
        self.opm_shared_arr = opm_shared_arr
        self.pub_entries = list(map(PubEntry, self.secids))
        PubHeader.__init__(self, model_id, yte, interest_rate, ped_idx,
            len(self.secids), is_last_update)

    def pack_all(self, msg_type, **header_kwargs): 
        entries = []
        if msg_type == MSGTYPE_ADJUST:
            shared_arr = self.opm_shared_arr.get_pub_adjust()
            for i, entry in enumerate(self.pub_entries):
                dat = entry.pack_adjust_entry(*shared_arr[i])
                entries.append(dat)
        elif msg_type == MSGTYPE_FULL:
            shared_arr = self.opm_shared_arr.get_pub_full()
            for i, entry in enumerate(self.pub_entries):
                dat = entry.pack_full_entry(*shared_arr[i])
                entries.append(dat)
        return '%s%s' % (self.pack_data(msg_type, **header_kwargs), ''.join(entries))


class MDMidCalculator:
    def __init__(self, secid, price_multiplier):
        self.secid = secid
        self.price_multiplier = price_multiplier
        self.price = nan

    def update(self, pymd):
        self.price = 0.5 * (pymd.bids[0].price + pymd.asks[0].price) / self.price_multiplier

    #TODO: is this needed?  jff
    def update_secid(self, secid):
        self.secid = secid


class OPMDataPublisher(Thread):
    def __init__(self, model_id, price_multiplier, zmq_pub_channel, models, 
            publish_interval, adjusting_cfg, logger):
        self.logger = logger
        self.model_id = model_id
        self.zmq_pub_channel = zmq_pub_channel
        self.pub_data = OrderedDict()
        self.price_multiplier = price_multiplier
        # init byte packers and shared arrays
        self.n_models = len(models)
        self.put_to_call = {}
        self.calls = set()
        self.secid_to_pub_data = {}
        self.shared_arrays = {}
        for i, model in enumerate(models):
            self.tdate = model.tdate()
            self.put_to_call.update(model.put_to_call())
            self.logger.info('initializing publishing for model #%d, %s', i, model)
            # init shared_arr
            secids = model.option_secids()
            self.calls.update(secids)
            opm_shared_arr = OPMSharedArray(secids, model.yte)
            self.logger.info('created OPMSharedArray. model.secids=%s, model.yte=%0.4f', 
                secids, model.yte)
            # init pub_packer
            packer = ModelPacker(
                secids, opm_shared_arr, self.model_id, model.yte, 
                model.interest_rate, i, i==self.n_models-1
            )
            self.logger.info('created ModelPacker. option_secids=%s', secids)
            model.ref_shared_array(opm_shared_arr)
            opm_shared_arr.set_strikes(model.secid_to_strike())
            self.logger.info('model now has reference to opm_shared_arr: %s', opm_shared_arr)
            pub_dat = {
                'model': model,
                'underlying': MDMidCalculator(model.future_secid(), self.price_multiplier),
                'packer': packer,
                'shared_arr': opm_shared_arr,
                'seqnum': -1
            }
            self.shared_arrays[i] = opm_shared_arr.values
            sleep(5)
            self.pub_data[i] = pub_dat
            for secid in secids:
                self.secid_to_pub_data[secid] = pub_dat

            # set shared array with loaded values
            model.set_sh_array()

        # tv adjust
        self.adjusting_cfg = adjusting_cfg
        self.tv_adj = TvAdjust(maxAdjust=adjusting_cfg['maxAdjustment'], widthFactor=adjusting_cfg['widthFactor'],
            wingFactor=adjusting_cfg['wingFactor'], timeFactor=adjusting_cfg['timeFactor'], capPct=adjusting_cfg['vegaCapPercentage'],
            strikeInc=adjusting_cfg['strikeIncrement'])
        
        self.has_new_fills = False
        # establish zmq publishing
        self.publish_interval = publish_interval
        self.logger.info('publish_interval=%d', self.publish_interval)
        context = zmq.Context()
        socket = context.socket(zmq.PUB)
        socket.bind(self.zmq_pub_channel)
        self.socket = socket
        self.requests = deque()
        # thread stuff
        Thread.__init__(self, target=self.run)
        self.daemon = True
        self.stopped = False

    def update_adjustment_parameters(self, maxAdj, widthFactor, wingFactor, timeFactor, vegaCapPct):
        self.tv_adj.update_parameters(maxAdj, widthFactor, wingFactor, timeFactor, vegaCapPct)
        self.create_adjustments()

    def update_model_parameters(self, cfg):
        #cfg here is an array of dicts, one for each model
        #loop through expiries (ie models)
        for i, pub_dat in self.pub_data.items():
            model = pub_dat['model']
            #loop through cfg model grouping
            for param_group in cfg:
                if param_group['parameters']['name'] == model.model_name():
                    model.update_parameters(param_group['parameters'])
                    #update the underlying in the MDMidCalculator obj
                    if param_group['parameters']['underlying_secid'] != pub_dat['underlying'].secid:
                        self.logger.debug('new goddamn mdmid calc secid %s' % model.future_secid())
                        pub_dat['underlying'] = MDMidCalculator(model.future_secid(), self.price_multiplier)
        
    def create_adjustments(self):
        '''kick the tv adjust, and copy to call/put adjust cols'''
        self.tv_adj.create_adjustments(self.shared_arrays)
        # now copy adjustment to call and put adjusts
        for i, pub_dat in self.pub_data.items():
            pub_dat['shared_arr'].copy_adjustment_to_call_put()
        self.has_new_fills = True
 
    def update_models(self, pymd):
        for i, pub_dat in self.pub_data.items():
            # update the model
            model = pub_dat['model']
            model.copy_pymd_ptr(pymd)
            model.update_valid_md()
            model.evaluate_md()
            # now update the underlying
            u = pub_dat['underlying']
            if u.secid == pymd.secid:
                u.update(pymd)
                #self.logger.debug('new u mid! pymd=%s, u.price=%0.3f', pymd, u.price)

    def update_position(self, fill):
        secid = fill['secid']
        if secid in self.calls:
            call = secid
        elif secid in self.put_to_call:
            call = self.put_to_call[secid]
        else:
            self.logger.warning('disregarding fill %s because not relevant call or put', fill)
            return
        self.logger.debug('processing fill: %s', fill)
        pos = (3-2*fill['side']) * fill['qty']
        # update position in shared array
        shared_arr = self.secid_to_pub_data[call]['shared_arr']
        # this will only update the call pos.  Future: to add put pos, need to change this 
        # method to accept a put flag or something like that.
        shared_arr.modify_position_at(call, pos)
        self.has_new_fills = True

    def send(self):
        send_is_last = False
        for i, pub_dat in self.pub_data.items():
            packer = pub_dat['packer']
            u = pub_dat['underlying']
            model = pub_dat['model']
            lock.acquire()
            if model.seqnum() != pub_dat['seqnum'] or send_is_last:
                # if any expiration chagnes you have to send the last one for deio to pub to db
                send_is_last = True
                msg_type = MSGTYPE_FULL
                pub_dat['seqnum'] = model.seqnum()
            elif self.has_new_fills:
                # if you find yourself not publishing adjusmtments there is probably
                # a race condition between setting this false and the flag not being true
                # in run_opm in order to create adjustments... call it here?
                msg_type = MSGTYPE_ADJUST
                self.has_new_fills = False
            else:
                msg_type = MSGTYPE_HEARTBEAT
            payload = 'MOD,' + packer.pack_all(
                msg_type, underlying_midprc=u.price, 
                underlying_mid_at_last_update=model.spot(),
                synthetic_offset=model.roll(), triggerid=4321)
            self.socket.send(payload)
            lock.release()
            self.logger.debug(
                '%s : sent msg_type=%d, len(payload)=%d, bytes_len=%d', model.model_name(), msg_type, len(payload), 
                packer.total_size
            )
            model.log_values()
    
    def dump_data(self):
        # dump vol data to json
        for i, pub_dat in self.pub_data.items():
            pub_dat['model'].dump_data()

    def stop(self):
        self.stopped = True
        self.join()

    def run(self):
        self.logger.info('starting OPMDataPublisher in %s seconds', self.publish_interval)
        sleep(self.publish_interval)
        self.logger.info('publish!')
        count = 0
        while True:
            # take over the thread!
            if self.stopped:
                break
            # any requests?
            while self.requests:
                request = self.requests.popleft()
                self.logger.warning('received request: %s', request)
                # the purpose of the request is to send deio the full message,
                # so pretend we've never updated
                for i, pub_dat in self.pub_data.items():
                    pub_dat['seqnum'] = -1
            # now send stuff?
            self.send()
            # give it up
            sleep(self.publish_interval)
            count += 1
        self.logger.warning('stopped OPMDataPublisher')
        self.socket.close()
        self.logger.warning('closed 0mq socket')

    def add_request(self, request):
        self.requests.append(request)
