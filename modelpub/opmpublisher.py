import sys
import os
import pickle
import subprocess
import socket
import datetime as dt
import numpy as np
import pandas as pd
import glt.db
import pickle

from time import sleep
from threading import Thread
from glt.tools import now
from .lib.config_helper import init_configs
from .lib.listening import (
    request_replay, 
    ZMQListener, 
    RequestHandler,
    FillManager
)
from .lib.modeling import SecurityLoader, OPMTheoAdjuster
from .lib.publishing import DeioOPMPublisher
from .lib.logging import LogPublisher


if __name__ == '__main__':
    today = dt.date.today()
    cfg, today, yday = init_configs(sys.argv, today)
    model_cfg = cfg['model']

    debug_mode = model_cfg['opmPublisherDebugMode']
    logname = '%s: opmpublisher.py' % model_cfg['name']
    logger = LogPublisher(cfg['opm_log_chan'], logname, debug_mode=debug_mode)
    logger.write('starting opmpublisher.py', 'info')

    # load securities from pickle
    secs = SecurityLoader(logger=logger)
    with open(os.path.join(cfg['securities_filename']), 'rb') as f:
        secs.rebuild_objects(pickle.load(f))

    # listen to fills
    fillfwd_addr = cfg['fillfwd_addr']
    fills = FillManager(fillfwd_addr, logger=logger)
    fills_thread = Thread(target=fills.run_forever)
    fills_thread.daemon = True
    fills_thread.start()

    # initialize tv adjuster
    db_param = cfg['db_param']
    tv_adj = OPMTheoAdjuster(secs, model_cfg, fills, logger=logger)

    # let's get the model going
    start_time = dt.datetime.now().replace(microsecond=0)
    logger.write('finding last state of vols at or before: %s' % start_time, 'info')
    opm = DeioOPMPublisher(
        cfg['opm_publisher_chan'], secs, tv_adj, model_cfg, db_param, 
        start_time=start_time, logger=logger
    )
    logger.write('connected to %s for publishing.' % cfg['opm_publisher_chan'], 'info')

#    for term in opm.shared_arrays:
#        print term
#        print pd.DataFrame(opm.shared_arrays[term]).to_string()
#    with open('jff_test_shared_array.p', 'wb') as f:
#        pickle.dump(opm.shared_arrays, f)
#    exit()

    # start threads
    adjuster_thread = Thread(target=opm.run_tv_adj)
    adjuster_thread.daemon = True
    adjuster_thread.start()

    # what's happening with the rest of the world
    logger.write('creating vol listener', 'info')
    vols = ZMQListener(cfg['vol_publisher_chan'], recv_pyobj=True)

    # retrieve some data
    already_replayed, first_sent = False, False
    logger.write('done.', 'info')

    hb_thread = Thread(target=opm.run_heartbeat)
    hb_thread.daemon = True
    hb_thread.start()

    logger.write('opening request listener', 'info')
    requests = RequestHandler(
        cfg['opm_request_chan'], opm, cfg=cfg, logger=logger, recv_pyobj=False, queue_actions=True
    )
    reqs_thread = Thread(target=requests.run)
    reqs_thread.daemon = True
    reqs_thread.start()
    logger.write('done', 'info')

    # request replay from volpublisher
    logger.write('requesting known md.', 'info')
    request_replay(cfg['vol_request_chan'])
    logger.write('done.', 'info')

    while 1:
        try:
            data = vols.recv()
            ts = now()
            opm.process_data(data)

            # check whether we sent any data yet
            # also check for pending requests for replays
            if not first_sent and not opm.any_isnan() and opm.allow_model_updates:
                # catch up on positions and fills on the day
                logger.write('time to load known positions', 'info')
                opm.load_positions(yday, today)
                logger.write('done', 'info')
                if requests.actions:
                    logger.write('checking request queue', 'info')
                    while requests.actions:
                        logger.write('sending known smoothed vols', 'info')
                        send_all = requests.actions.popleft()
                        send_all()
                else:
                    logger.write('sending known smoothed vols', 'info')
                    opm.send_all()
                logger.write('done.', 'info')
                first_sent = True
            elif first_sent and requests.actions:
                logger.write('checking request queue after initial send', 'info')
                while requests.actions:
                    logger.write('sending known smoothed vols', 'info')
                    send_all = requests.actions.popleft()
                    send_all()
                
        except KeyboardInterrupt:
            print(logger.write('KeyboardInterrupt! exiting', 'error'))
            # kill run_forever loops and close connections
            requests.kill()
            tv_adj.kill()
            opm.kill()
            fills.kill()
            vols.kill()
            sleep(0.1)
            logger.kill()
            break

    # make sure threads are dead
    reqs_thread.join()
    hb_thread.join()
    fills_thread.join()
    adjuster_thread.join()
