import sys
import os
import shutil

if __name__ == '__main__':
    pycore_filename = sys.argv[1]
    datapath = os.path.dirname(pycore_filename)
    pythonpath = os.path.join(os.path.dirname(os.__file__), os.pardir, os.pardir)
    pythonpath = os.path.abspath(pythonpath)
    n = len(pythonpath)+1
    with open(pycore_filename, 'r') as f:
        for line in f.read().split('\n'):
            if not line:
                continue
            if pythonpath not in line:
                raise ValueError('wrong python path: %s not in %s' % (pythonpath, line))
            relpath = os.path.join(datapath, line[n:])
            if os.path.isdir(line):
                continue
            newdir = os.path.dirname(relpath)
            if not os.path.exists(newdir):
                os.makedirs(newdir)
            elif not os.path.exists(line):
                print('restoring', line)
                shutil.copyfile(relpath, line)
            print('copy back to restore directory', relpath)
            shutil.copyfile(line, relpath)
