import argparse
import zmq
import os
import json

from collections import namedtuple
from time import sleep


positive_responses = [
    'true', 'yes', 'ja', 'yep', 'yeah', 'yup', 'si'
]

negative_responses = [
    'false', 'no', 'nej', 'nay', 'nope'
]


FakePosition = namedtuple('FakePosition', ['secid', 'side', 'price', 'qty'])


class ZMQSocket:
    def __init__(self, addr):
        self.addr = addr

    def __enter__(self):
        context = zmq.Context()
        socket = context.socket(zmq.PUB)
        socket.bind(self.addr)
        self.socket = socket
        return self

    def send(self, msg):
        print('sending msg to', self.addr)
        self.socket.send(msg)
        sleep(0.5)

    def send_command(self, command, action):
        self.send(cmd_message(command, action))

    def __exit__(self, exc_type, exc_value, traceback):
        self.socket.close()
     

def cmd_message(command, action):
    return 'CONSOLE:%s -- %s' % (command, action)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Modelpub (OPM) Console Command')

    # add arguments here
    parser.add_argument('--addr',
        nargs='?',
        help='0mq channel used to send requests'
    )
    parser.add_argument('--test', 
        nargs='?',
        help='nothing special... please ignore'
    )
    parser.add_argument('--debug', 
        nargs='?',
        help='set DEBUG logging on if true',
        metavar='BOOLEAN'
    )
    parser.add_argument('--add-position',
        nargs=4,
        help='adds position for securityId',
        metavar=('SECID', 'SIDE', 'PRICE', 'QTY')
    )
    parser.add_argument('--reload-adjustment',
        help='reload adjustment params from "tvAdjust" - "adjusting" section of cfg'
    )
    parser.add_argument('--posdump',
        help='print shared array contents'
    )
    parser.add_argument('--reload-model',
        help='reload model params from "models" section of cfg'
    )
    #parser.add_argument('--reload-config',
    #    nargs='?',
    #    help='reload config filename',
    #    metavar='FILENAME'
    #)

    # what do we have?
    args = parser.parse_args()

    if args.test:
        print('cool, the console command script works!')
    if args.debug:
        with ZMQSocket(args.addr) as socket:
            arg = args.debug.lower()
            if arg in positive_responses:
                #socket.send('CONSOLE:setLogLevel -- DEBUG')
                socket.send_command('setLogLevel', 'DEBUG')
            elif arg in negative_responses:
                socket.send_command('setLogLevel', 'INFO')
    if args.add_position:
        position = FakePosition(*args.add_position)
        side = int(position.side)
        if side not in (1, 2):
            print('invalid side. must be 1 for bid or 2 for offer')
        qty = int(position.qty)
        if qty <= 0:
            print('qty must be > 0')
        with ZMQSocket(args.addr) as socket:
            socket.send_command('addPosition', ','.join(args.add_position))
    if args.reload_adjustment:
        filename = '/home/deio/tools/cfg/model.test.nko.json'
        with open(filename, 'r') as f:
            cfg = json.load(f)
        adj_params = cfg['tvAdjust']['adjusting']
        with ZMQSocket(args.addr) as socket:
            print('reloadAdjustments', str(adj_params))
            socket.send_command('reloadAdjustments', str(adj_params))
    if args.posdump:
        with ZMQSocket(args.addr) as socket:
            print('dumping pos...')
            socket.send_command('dumpPos', 'true')
    if args.reload_model:
        filename = '/home/deio/tools/cfg/model.test.nko.json'
        with open(filename, 'r') as f:
            cfg = json.load(f)
        model_params = cfg['models']
        with ZMQSocket(args.addr) as socket:
            print('reloadModel', str(model_params))
            socket.send_command('reloadModel', str(model_params))
        
        
    #if args.reload_config:
    #    filename = args.reload_config
    #    if not os.path.exists(filename):
    #        print 'filename %s does not exist' % filename
    #    with ZMQSocket(args.addr) as socket:
    #        socket.send_command('reloadConfig', filename)
    print('done')
