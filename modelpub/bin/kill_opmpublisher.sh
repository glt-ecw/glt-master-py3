#!/bin/bash
set -euo pipefail

PYTHONPATH=/glt/tools/miniconda-deio
CFGPATH=/glt/app/cfg

scriptpath=$1
publisher_cfg=$2
model_cfg=$3

rm -f $scriptpath/core.*[0-9]

echo "killing opm publisher"
$PYTHONPATH/bin/python $scriptpath/seek_and_destroy.py opmpublisher $publisher_cfg $model_cfg
$PYTHONPATH/bin/python $scriptpath/seek_and_destroy.py opmrecorder $publisher_cfg $model_cfg

