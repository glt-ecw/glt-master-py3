#!/bin/bash

PYTHONPATH=/glt/tools/miniconda-deio

tdate_adjust=$4

set -euo pipefail

scriptpath=$1
publisher_cfg=$2
model_cfg=$3

# kill everything
echo "using scriptpath=$scriptpath"
$scriptpath/bin/kill_volpublisher.sh $scriptpath $publisher_cfg $model_cfg
$scriptpath/bin/kill_opmpublisher.sh $scriptpath $publisher_cfg $model_cfg

echo "fetching mdfwd security ids"
$scriptpath/bin/find_mdfwdids.sh

echo "starting vol publisher"
$PYTHONPATH/bin/python $scriptpath/volrecorder.py $publisher_cfg $model_cfg $tdate_adjust &
$PYTHONPATH/bin/python $scriptpath/volpublisher.py $publisher_cfg $model_cfg $tdate_adjust

