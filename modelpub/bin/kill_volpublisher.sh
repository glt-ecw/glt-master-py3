#!/bin/bash
set -euo pipefail

PYTHONPATH=/glt/tools/miniconda-deio

scriptpath=$1
publisher_cfg=$2
model_cfg=$3

echo "killing vol publisher"
$PYTHONPATH/bin/python $scriptpath/seek_and_destroy.py volpublisher $publisher_cfg $model_cfg
$PYTHONPATH/bin/python $scriptpath/seek_and_destroy.py volrecorder $publisher_cfg $model_cfg

