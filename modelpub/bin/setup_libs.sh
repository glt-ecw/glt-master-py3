#!/bin/bash
set -euo pipefail

wget https://repo.continuum.io/miniconda/Miniconda-latest-Linux-x86_64.sh
bash Miniconda-latest-Linux-x86_64.sh
/glt/tools/miniconda-deio/bin/conda update conda
/glt/tools/miniconda-deio/bin/conda install numpy scipy scikit-learn cython pandas psutil
/glt/tools/miniconda-deio/bin/pip install pyzmq --install-option="--zmq=/glt/tools/zeromq-2.2.0.gcc482"
/glt/tools/miniconda-deio/bin/pip install oursql
