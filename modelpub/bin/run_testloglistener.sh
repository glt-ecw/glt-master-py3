#!/bin/bash

set -euo pipefail

scriptpath=.
CFGPATH=cfg

server=$1
product=$2

set +u
tdate_adjust=$3
set -u

publisher_cfg=$CFGPATH/$server.test.modelpub.json
model_cfg=$CFGPATH/model.test.$product.json

echo "starting log listener"
$scriptpath/bin/run_loglistener.sh $scriptpath $publisher_cfg $model_cfg

