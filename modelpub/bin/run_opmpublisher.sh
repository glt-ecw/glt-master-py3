#!/bin/bash

PYTHONPATH=/glt/tools/miniconda-deio

tdate_adjust=$4

set -euo pipefail

scriptpath=$1
publisher_cfg=$2
model_cfg=$3

# kill existing?
$scriptpath/bin/kill_opmpublisher.sh $scriptpath $publisher_cfg $model_cfg

echo "starting opm publisher"
$PYTHONPATH/bin/python $scriptpath/opmrecorder.py $publisher_cfg $model_cfg $tdate_adjust &
$PYTHONPATH/bin/python $scriptpath/opmpublisher.py $publisher_cfg $model_cfg $tdate_adjust

