#!/bin/bash
set -euo pipefail

OUTDIR=/glt/logs
OUTFILE=$OUTDIR/mdfwd.secids.out

grep "will always fwd md for security" `lslog` > $OUTFILE
