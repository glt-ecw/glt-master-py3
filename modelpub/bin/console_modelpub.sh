#!/bin/bash

PYTHONPATH=/glt/tools/miniconda-deio

cmd=$4
arg=$5

set -euo pipefail

scriptpath=$1
publisher_cfg=$2
model_cfg=$3

$PYTHONPATH/bin/python $scriptpath/console.py $publisher_cfg $model_cfg $cmd $arg

