#!/bin/bash
set -euo pipefail

PYTHONPATH=/glt/tools/miniconda-deio

scriptpath=$1
publisher_cfg=$2
model_cfg=$3

echo "starting log listener"
$PYTHONPATH/bin/python $scriptpath/loglistener.py $publisher_cfg $model_cfg

