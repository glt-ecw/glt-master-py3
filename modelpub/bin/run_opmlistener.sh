#!/bin/bash

PYTHONPATH=/glt/tools/miniconda-deio

tdate_adjust=$4

set -euo pipefail

scriptpath=$1
publisher_cfg=$2
model_cfg=$3

echo "starting opm listener"
$PYTHONPATH/bin/python $scriptpath/opmlistener.py $publisher_cfg $model_cfg $tdate_adjust

