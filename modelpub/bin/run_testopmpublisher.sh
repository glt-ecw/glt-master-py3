#!/bin/bash

set -euo pipefail

scriptpath=.
CFGPATH=cfg

server=$1
product=$2

set +u
tdate_adjust=$3
set -u

publisher_cfg=$CFGPATH/$server.test.modelpub.json
model_cfg=$CFGPATH/model.test.$product.json

echo "starting test vol publisher"
$scriptpath/bin/run_opmpublisher.sh $scriptpath $publisher_cfg $model_cfg $tdate_adjust

