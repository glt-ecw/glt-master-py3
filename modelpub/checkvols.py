import sys
import zmq

from time import sleep
from glt.tools import now
from .lib.config_helper import init_configs

def send_request(context, request_channel):
    requests = context.socket(zmq.PUB)
    requests.bind('ipc://%s' % request_channel)

    sleep(0.25)
    requests.send_pyobj('REPLAY')

    sleep(0.25)
    requests.close()

if __name__ == '__main__':
    publish_channel = sys.argv[1]
    request_channel = sys.argv[2]
    try:
        secid = int(sys.argv[3])
    except IndexError:
        print('no secid specified. listening to all.')
        secid = None

    context = zmq.Context()
    sock = context.socket(zmq.SUB)
    sock.connect('ipc://%s' % publish_channel)
    sock.setsockopt(zmq.SUBSCRIBE, '')

    already_replayed = False
    
    while 1:
        if not already_replayed:
            send_request(context, request_channel)
            already_replayed = True

        data = sock.recv_pyobj()
        ts = now()

        if secid is None or data[0] == secid:
            print(ts-data[1], data)

