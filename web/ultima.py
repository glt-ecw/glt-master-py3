import os
import psutil

my_procs = [
    'run_tornado.py',
    'odin',
    'heimdall',
    'mjolnir',
    'thor',
]

for proc in psutil.process_iter():
    for args in proc.cmdline():
        if any(p in args for p in my_procs):
            print('killing', proc.pid, proc.cmdline())
            os.kill(proc.pid, 9)
