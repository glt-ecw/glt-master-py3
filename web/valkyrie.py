import sys
import json

from time import sleep
from websocket import create_connection

if len(sys.argv) > 3:
    port = sys.argv[1]
    command = sys.argv[2]
    specific = sys.argv[3]
elif len(sys.argv) > 1:
    port = sys.argv[1]
    command = sys.argv[2]
    specific = ''
else:
    print('issue a command to the websocket server')
    sys.exit()

client = create_connection('ws://10.1.31.202:' + port)
#client = create_connection('ws://10.130.33.102:8804')
data = {
        'msg': 'admin',
        'command': command,
        'specific': specific
    }
client.send('0' + json.dumps(data))
sleep(0.1)
print('sent command:', command, 'with specifics', specific, end=' ')
print('to websocket server. thank you.')
client.close()
