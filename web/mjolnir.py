import tornado.websocket
import tornado.httpserver
import tornado.ioloop
import tornado.web

from datetime import datetime
from time import sleep

port = 41111

data = dict()

# websocket server
class WebSocketServer(tornado.websocket.WebSocketHandler):
    def open(self):
        pass

    def on_message(self, message):
        if message == 'start':
            self.write_message('zap')
        elif message == 'zapped':
            self.write_message('finished')

    def on_close(self):
        pass

app = tornado.web.Application([
        (r'/', WebSocketServer)
    ])

if __name__ == '__main__':
    print('mjolnir awaits thor.') 
    try:
        # websocket server
        http_server = tornado.httpserver.HTTPServer(app)
        http_server.listen(port)
        tornado.ioloop.IOLoop.instance().start()
    finally:
        print('all dead!')
