import sys
import os
import threading
import inspect
import zmq
import subprocess
#import simplejson as json
import json
import datetime as dt
import numpy as np
import pandas as pd

from ciso8601 import parse_datetime
from collections import deque
from time import sleep
from functools import partial
from datetime import datetime
from dateutil.rrule import rruleset, DAILY, rrule, MO, TU, WE, TH, FR

# tornado
import tornado.websocket
import tornado.httpserver
import tornado.ioloop
import tornado.web

# glt stuff
#import pylib.mysqldb as mysqldb
import glt.math.bs as bs
import glt.db as mysqldb
#import pylib.bs_test as bs
from pylib.deio_defs import *

ports = {
    #'sy': 8802,
    'w': 8802,
    'bk': 8804,
    'jpx': 8806,
    'cme': 8808,
    'hy': 8810,
    'lead': 8812,
}

system = sys.argv[1]
if len(sys.argv) > 2:
    port = int(sys.argv[2])
else:
    port = ports[system]

if len(sys.argv) > 3:
    days = int(sys.argv[3])
else:
    days = 0

#try:
#    port = int(sys.argv[1])
#except:
#    port = 8801

global listeners
listeners = {}
mysql_commands = deque()

exchange      = EXCHANGES[system]
tzinfo        = TZINFO[exchange]
prod_host     = MYSQL_HOSTS[system]
prod_db       = MYSQL_DB[system]
uat_db        = MYSQL_UATDB[system]
defs_host     = DBSERVER01
defs_db       = DEFS_DB
vols_table    = VOL_TABLE[exchange]
settles_table = SETTLES_TABLE[exchange]
holiday_table = HOLIDAYS.get(system)
fee_table     = FEE_TABLES[system]
bro_table     = BRO_TABLES[system]
theos_table   = THEOS_TABLES[exchange]
xtrades_table = XTRADES_TABLES[system]
rate          = RATES[exchange]
exch_category = EXCH_CATEGORY[exchange]
ptvals        = lambda x: PT_VALUES[exchange].get(x, 0)
cash_norm     = CASH_NORMALIZER[exchange]
norm_qty      = NORM_QTY[exchange]
index_ids     = INDEX_IDS[exchange]
currency_conv = CURRENCY_CONV[exchange]
dupe_handling = DUPE_HANDLING[system]
margin        = exchange in MARGIN_SCENARIOS
pos_cross     = POSITION_CROSS.get(system)
if margin:
    margin_scenarios       = MARGIN_SCENARIOS[exchange]
    margin_vol_shock       = MARGIN_VOL_SHOCK[exchange]
    margin_outrights       = MARGIN_OUTRIGHTS[exchange]
    margin_spreads         = MARGIN_SPREADS[exchange]
    margin_minimums        = MARGIN_MINIMUMS[exchange]
    margin_options_product = MARGIN_OPTIONS_PRODUCT[exchange]

def currency_byproduct(product):
    return currency_conv.get(product) or currency_conv.get('CASH')

def todt(d):
    return datetime.combine(d, dt.time(0, 0, tzinfo=tzinfo))

today_date = datetime.now(tzinfo).date() + dt.timedelta(days=days)
today = todt(today_date)
print('starting server. today is', today)
#model_start = today + dt.timedelta(hours=9, minutes=0, seconds=0)
#model_stop = today + dt.timedelta(hours=15, minutes=40, seconds=0)

global data_store
data_store = {}
data_store['defs_catchup'] = { 
        'msg': 'defs_catchup',
        'defs': [],
        'rate': rate,
        'bizdays': BIZDAYS,
        'system': system
    }
data_store['md_catchup'] = { 'msg': 'md_catchup' }
data_store['fill_catchup'] = { 'msg': 'fill_catchup', 'fills': [] }
data_store['vols'] = { 'msg': 'vols' }
data_store['spreads'] = { 'msg': 'spreads' }
data_store['settles'] = { 'msg': 'settles', 'prc': {}, 'bs': {} }
data_store['monthpnl'] = { 'msg': 'monthpnl', 'value': 0.0 }

# helper functions
def cleanFill(json_obj):
    irrelevant = [
        #'filltype', 
        'seqnum', 
        'id', 
        'filltime', 
        'usfilltime', 
        'exchtradeid'
    ]
    #print json_obj
    for p in irrelevant:
        if p in json_obj:
            del json_obj[p]
    return 0

def syncDefs(json_obj, listener=None):
    id = 'securityid' in json_obj and json_obj['securityid']
    strid = str(id)
    if id not in contracts:
        defs = []
        if id not in all_defs:
            return defs
        w = all_defs[id]
        uid = w['underlyingid']
        eid = w['equivid']
        for oid in (uid, eid):
            if oid > 0 and oid not in contracts:
                stroid = str(oid)
                optdef = { 'msg': 'defs', stroid: all_defs[oid] }
                data_store['defs_catchup']['defs'].append(optdef)
                defs.append(optdef)
                contracts.add(oid)
        secdef = { 'msg': 'defs', strid: w }
        data_store['defs_catchup']['defs'].append(secdef)
        defs.append(secdef)
        contracts.add(id)
        return defs
    return []

def callback(listener, json_obj):
    msg = json.dumps(json_obj, allow_nan=False)
    sleep(0.001)
    if listener:
        return io_loop.add_callback(partial(listener.on_message, msg))

def processTheosTable(table):
    spreads = { 'msg': 'spreads' }
    curr_leanids = set()
    for i, row in sconn.frame_query("select shortname, lean, prev, now from " + table).iterrows():
        name, lean, prev, now = row
        if name in name_defs and lean in name_defs:
            strid = str(name_defs[name])
            leanid = int(name_defs[lean])
            spread = { 'lean': leanid, 'theo': now }
            spreads[strid] = spread
            if leanid not in leanids:
                leanids[leanid] = set()
            leanids[leanid].add(strid)
            curr_leanids.add(leanid)
    for leanid in set(leanids)-curr_leanids:
        del leanids[leanid]
    return spreads

# for sending messages
global io_loop, kill_switch
io_loop = tornado.ioloop.IOLoop.instance()
kill_switch = False

global all_defs
mconn = mysqldb.conn(defs_host, 'deio', '!w3alth!', defs_db)
users = mconn.frame_query("select userid, username from users")
users.index = users['userid'].astype(str)
users = users['username'].to_dict()
users["-1"] = "prev"
users["-2"] = "keepout"
users["-3"] = "spec"
users["-4"] = "gamma"
users["-5"] = "batman"
users["-6"] = "bane"
users["-7"] = "temp"
userids = {v: k for k, v in list(users.items())}
data_store['defs_catchup']['users'] = users
data_store['defs_catchup']['cash_normalizer'] = cash_norm #ptvals('CASH')
data_store['defs_catchup']['base_currency'] = currency_conv['CASH']

if system == 'lead':
    # hack-a-licious
    daily_loss_filename = '/glt/app/cfg/lead.dailyLossLimit.cfg'
    daily_loss = subprocess.check_output(['ssh', '10.138.33.103', 'cat', daily_loss_filename])
    daily_loss = int(daily_loss.replace('\n', ''))
    print('daily loss filename found', daily_loss_filename)
else:
    # pnl limits? we don't need no stinkin' pnl limits
    daily_loss = -1000000000

data_store['defs_catchup']['daily_pnl_loss'] = daily_loss * cash_norm

mconn.execute("use SECURITIES")

condefids = mconn.frame_query("select id from contractDefinitions \
        where exchid in " + exch_category)['id']
condefids = condefids.apply(int)
condefids = str(tuple(condefids))

all_defs = mconn.frame_query("\
    select \
        securities.id, 'future' as ctype, \
        shortname, expdate, 0 as strike, \
        0 as op, 0 as underlyingid, 0 as coupon, \
        '' as product \
    from securities \
    inner join futures on securities.id=futures.id \
    where expdate >= '" + today_date.isoformat() + "' \
    and contractdefid in " + condefids + " \
    union all \
    select \
        securities.id, 'option' as ctype, \
        shortname, expdate, strike, \
        optiontype as op, underlyingid, 0 as coupon, \
        '' as product \
    from securities \
    inner join options on securities.id=options.id \
    where expdate >= '" + today_date.isoformat() + "' \
    and contractdefid in " + condefids + " \
    union all \
    select \
        securities.id, if(strike=0, 'stock', 'warrant') as ctype, \
        shortname, expdate, strike, \
        if(strike=0, 0, warranttype) as op, 0 as underlyingid, \
        0 as coupon, \
        'CASH' as product \
    from securities \
    inner join warrants on securities.id=warrants.id \
    where expdate >= '" + today_date.isoformat() + "' \
    and contractdefid in " + condefids + " \
    union all \
    select \
        securities.id, 'bond' as ctype, \
        shortname, expdate, 0 as strike, \
        0 as op, 0 as underlyingid, coupon, \
        'KTBC' as product \
    from securities \
    inner join bonds on securities.id=bonds.id \
    where expdate >= '" + today_date.isoformat() + "' \
    and contractdefid in " + condefids + " \
    order by expdate, op, id")
mconn.close()
mconn = mysqldb.conn(prod_host, 'deio', '!w3alth!', prod_db)
if system in SYSTEMS[exchange]: #('w', 'sy', 'hy', 'bk'): # 'bk':
    vconn = mysqldb.conn(DBSERVER01, 'deio', '!w3alth!', 'DEIO_W')
    #vconn = mysqldb.conn(BD02, 'deio', '!w3alth!', 'DEIO_BK')
else:
    vconn = mconn

all_defs['equivid'] = 0
all_defs['equivproduct'] = ''
all_defs['norm_prc'] = 1.0
all_defs['norm_qty'] = 1.0
all_defs.index = all_defs['id'].astype(int)
for product, id in list(index_ids.items()):
    all_defs.ix[id, 'ctype'] = 'index'

def yte_calc(expdate):
    days = rrule(DAILY, dtstart=today, until=todt(expdate), byweekday=(MO, TU, WE, TH, FR))
    return days.count()/BIZDAYS
uniq_expdate = set(all_defs['expdate'])
ytes = {d: yte_calc(d) for d in uniq_expdate}
all_defs['yte'] = all_defs['expdate'].apply(lambda x: ytes[x])
del all_defs['id']
name_defs = {r['shortname']: int(i) for i, r in all_defs.iterrows()}

prodname = {
    'future': future_product,
    'option': option_product,
    'warrant': warrant_product
}
opts = all_defs[all_defs['ctype']=='option']
wars = all_defs[all_defs['ctype']=='warrant']
for ctype, func in list(prodname.items()):
    idx = all_defs[all_defs['ctype']==ctype].index
    # shorter names for warrants
    if ctype != 'warrant':
        all_defs.ix[idx, 'product'] = all_defs.ix[idx, 'shortname'].apply(func)
        all_defs.ix[idx, 'equivproduct'] = all_defs.ix[idx, 'product']
        if ctype == 'option' and not vols_table:
            all_defs.ix[idx, 'ctype'] = 'future'
            all_defs.ix[idx, 'strike'] = 0
            all_defs.ix[idx, 'op'] = 0
            all_defs.ix[idx, 'underlyingid'] = 0
        if ctype == 'option':
            print(all_defs.ix[idx].head(100).to_string())
    else:
        all_defs.ix[idx, 'norm_prc'] = WARRANT_PRC_MULTIPLIER
        all_defs.ix[idx, 'norm_qty'] = WARRANT_SIZE_MULTIPLIER
        all_defs.ix[idx, 'shortname'] \
                = all_defs.ix[idx, 'shortname'].apply(warrant_shortername)
        print('find equivalent contracts for warrants')
        relcol = ['shortname', 'expdate', 'strike', 'op']
        for i, row in wars.iterrows():
            name, expdate, strike, op = row[relcol]
            bool = (opts['op']==op)&(opts['strike']==strike)&(opts['expdate']==expdate)
            equivs = opts[opts['shortname'].str.startswith('KO')&bool].index
            if len(equivs):
                all_defs.ix[i, 'equivid'] = equivs[0]
                all_defs.ix[i, 'equivproduct'] = all_defs.ix[equivs[0], 'product']


##### CHRISTINE: START HERE
mkoo_ix = all_defs[(all_defs['ctype']=='option')&(all_defs['shortname'].str[:4]=='MKOO')].index
all_defs.ix[mkoo_ix, 'norm_qty'] = MKOO_SIZE_MULTIPLIER
mkoo = all_defs.ix[mkoo_ix]
for i, row in mkoo.iterrows():
    name, expdate, strike, op = row[relcol]
    bool = (opts['op']==op)&(opts['strike']==strike)&(opts['expdate']==expdate)
    equivs = opts[opts['shortname'].str.startswith('KO')&bool].index
    if len(equivs):
        all_defs.ix[i, 'equivid'] = equivs[0]
        all_defs.ix[i, 'equivproduct'] = all_defs.ix[equivs[0], 'product']
print(all_defs.ix[mkoo_ix].to_string())
##### CHRISTINE: STOP HERE

del opts
del wars
del mkoo

all_defs['ptval'] = all_defs['product'].apply(ptvals)*cash_norm #ptvals('CASH')
all_defs['currency'] = all_defs['product'].apply(currency_byproduct)
products = set(all_defs['product'])

for product in products:
    if product not in norm_qty:
        continue
    pids = all_defs[all_defs['product']==product].index
    nproducts = norm_qty[product]
    all_defs.ix[pids, 'equivproduct'] = nproducts[0]
    all_defs.ix[pids, 'norm_qty'] = ptvals(product)/ptvals(nproducts[0])
    for id, row in all_defs.ix[pids].iterrows():
        for np in nproducts:
            ncontract = row['shortname'].replace(product, np)
            nids = all_defs[all_defs['shortname']==ncontract].index
            if len(nids):
                all_defs.ix[id, 'equivid'] = nids[0]
                #all_defs.ix[id, 'norm_qty'] = ptvals(product) / ptvals(all_defs.ix[nids[0], 'product'])
                if len(row['shortname'].split('_')) == 1:
                    print(ncontract, 'found for', row['shortname'], all_defs.ix[id, 'norm_qty'])
                break

# before we change all_defs to a dict, create md_mute
global md_mute
md_mute = set(all_defs[all_defs['op']>0].index)
md_items = ('msg', 'securityid', 'prc')

# handle zmq data
context = zmq.Context()
handler = context.socket(zmq.SUB)
handler.connect('ipc://asgard.ipc')
handler.setsockopt(zmq.SUBSCRIBE, '')

contracts = set()

# connect again, but to GLTRESULTS
sconn = mysqldb.conn(RES01, 'gltloader', 'gltloader', 'GLTRESULTS')
# monthly pnl garbage
if system in SYSTEMS['krx']:
    relevant_year = today.year
    relevant_month = today.month
    mpnl = sconn.frame_query("select ifnull(sum(revenue-fees-bro), 0) as monthpnl "
                             "from pnl." + system + " "
                             "where year(date)=" + str(relevant_year) + " "
                             "and month(date)=" + str(relevant_month) + " "
                             "order by date").ix[0, 'monthpnl']
    data_store['monthpnl']['value'] = mpnl

holidays = []
#if holiday_table:
#    holidays = sconn.frame_query('select * from ' + holiday_table)['date'].values
#    if today in [todt(h) for h in holidays]:
#        sys.exit("holiday today. exiting")
dateset = rruleset()
dateset.rrule(rrule(DAILY, 
        dtstart=today-dt.timedelta(days=30), until=today, 
        byweekday=(MO, TU, WE, TH, FR))
    )
for hol in holidays:
    dateset.exdate(todt(hol))
yday = list(dateset)[-2].date()
#yday_db = yday.isoformat().replace('-', '_')
yday = todt(yday)
prevpos = sconn.frame_query("""
    select securityid, position as pos from eod.positions where tradedate='%s' and instance='%s'
""" % (yday.date(), prod_db))

#yday_tables = sconn.frame_query("show tables in " + yday_db).values[:, 0]
# previous position
#prevpos = sconn.frame_query("select \
#        securityid, pos from " + yday_db + ".eod_positions \
#        where instance='" + prod_db + "'")
prevpos.index = prevpos['securityid']
if len(prevpos):
    print('found previous positions')
    prevpos = prevpos.ix[all_defs.index].dropna()

settles = sconn.frame_query("""
    select securityid, price as prc from eod.settles where tradedate='%s'
""" % yday.date())
if settles.shape[0]:
    settles.index = settles['securityid']
    settles = settles.ix[all_defs.index].dropna()
    settles = settles['prc'].to_dict()
    for product, id in list(index_ids.items()):
        if id in settles:
            data_store['settles']['prc'][str(id)] = settles[id]
else:
    settles = {}
# previously saved volatilities
prevvols = sconn.frame_query("""
    SELECT securityid, raw_volatility as vol, roll from eod.vols
    WHERE tradedate='%s'
""" % yday.date())
prevvols.index = prevvols['securityid']
prevvols = prevvols.ix[all_defs.index].dropna()
del prevvols['securityid']
prevvols = prevvols.T.to_dict()
# exchange fees
if fee_table:
    fees = sconn.frame_query("select * from " + fee_table)
    fees.index = fees['product_contract']
    del fees['product_contract']
    data_store['defs_catchup']['fees'] = fees.T.to_dict()
else:
    data_store['defs_catchup']['fees'] = {}
# brokerage
if bro_table:
    bro = sconn.frame_query("select * from " + bro_table)
    bro.index = bro['product_contract']
    del bro['product_contract']
    data_store['defs_catchup']['bro'] = bro.T.to_dict()
else:
    data_store['defs_catchup']['bro'] = {}
# position crosses?
pos_cross_data = {}
if pos_cross:
    for inst, userid in list(pos_cross.items()):
        ipos = sconn.frame_query("select * from " + yday_db + ".eod_positions \
            where instance='" + inst + "' and pos<>0")
        ipos.index = ipos['securityid'].astype(int)
        pos_cross_data[userid] = ipos['pos'].to_dict()

# join all of previous data and create mock fills for fill_catchup
prev = prevpos.join(all_defs)
prev['prc'] = pd.Series(settles)
prev = prev.join(pd.DataFrame(prevvols).T)
prev['securityid'] = prev.index
prev = prev.sort(['ctype', 'op', 'expdate'])
prev = prev.fillna(0.0)

# make all_defs as a dict
all_defs['expdate'] = all_defs['expdate'].astype(str)
all_defs = all_defs.T.to_dict()
rev_defs = {v['shortname']: k for k, v in list(all_defs.items())}

all_options = set([x[0] for x in [v for v in iter(all_defs.items()) if v[1]['ctype']=='option']])

# futures spreads
leanids = {}
data_store['spreads'] = processTheosTable('eod.spread_theos')
for leanid, tids in list(leanids.items()):
    syncDefs({'securityid': leanid})
    for strid in tids:
        syncDefs({'securityid': int(strid)})

print('reorder securities?')
previds = set(prev.index).union(list(prevvols.keys()))
ordered_previds = []
for id, row in prev.iterrows():
    if id not in previds:
        continue
    equivid = all_defs[id]['equivid']
    if equivid > 0 and equivid in previds:
        ordered_previds.append(equivid)
        previds.remove(equivid)
        if equivid in leanids:
            print('equivid is a lean', equivid, all_defs[equivid]['shortname'])
            for strtid in leanids[equivid]:
                tid = int(strtid)
                if tid in previds:
                    ordered_previds.append(tid)
                    previds.remove(tid)
    elif str(id) in data_store['spreads']:
        print(id, all_defs[id]['shortname'], 'requires lean. skipping')
        continue
    ordered_previds.append(id)
    previds.remove(id)
    if id in leanids:
        print('it is a lean', id, all_defs[id]['shortname'])
        for strtid in leanids[id]:
            tid = int(strtid)
            if tid in previds:
                ordered_previds.append(tid)
                previds.remove(tid)

for id in previds:
    if id not in ordered_previds and id not in prevvols:
        ordered_previds.append(id)

print('reordered')
for id in ordered_previds:
    print(id, all_defs[id]['shortname'])

print(ordered_previds)

# precompute options settles
for id in prevvols:
    strid = str(id)
    # calculate option settles
    v = prevvols[id]['vol']
    roll = prevvols[id]['roll']
    optdef = all_defs[id]
    op = -1 if optdef['op']==2 else 1
    x = optdef['strike']
    expiry = todt(parse_datetime(optdef['expdate']))
    u = settles[optdef['underlyingid']] + roll
    caldays = (expiry-yday).days
    daygen = (yday + dt.timedelta(x+1) for x in range(caldays))
    dte = 1 + sum(1 for day in daygen if day.weekday() < 5)
    t = dte/BIZDAYS
    data_store['settles']['prc'][str(id)] = round(bs.tv(op, x, u, v, t, rate), 3)
    bs_tmp = data_store['settles']['bs']
    vol = round(v, 5)
    roll = round(roll, 3)
    data_store['vols'][strid] = { 'vol': vol, 'roll': roll }
    bs_tmp[strid] = { 'vol': vol, 'roll': roll }

for id in ordered_previds:
    if id not in prev.index:
        continue
    row = prev.ix[id]
    id = int(id)
    strid = str(id)
    optcol = ['op', 'strike', 'expdate', 'vol', 'roll', 'underlyingid']
    mock = {
            'msg': 'fill', 
            'exchtradeid': 'P' + str(id),
            'seqnum': 0,
            'userid': -1,
            'usfilltime': 0,
            'filltime': today.isoformat(),
            'securityid': id,
            'id': id,
            'serverid': -1,
            'filltype': 0,
            'ordertype': 'previous',
            'side': 1 if row['pos'] >= 0 else 2,
            'qty': abs(row['pos'])
        }
    # need to fill in price
    equiv = row['equivid']
    if equiv > 0 and equiv in ordered_previds:
        strequiv = str(equiv)
        #print equiv in data_store['md_catchup'], id in all_defs
        #price = data_store['md_catchup'][equiv] / all_defs[id]['norm_prc']
        price = data_store['settles']['prc'][strequiv] / all_defs[id]['norm_prc']
        mock['price'] = round(price, 1 if row['ctype'] == 'warrant' else 3)
        prev.ix[id, 'prc'] = mock['price']
    else:
        if row['ctype'] in ('future', 'stock', 'bond'):
            if strid in data_store['spreads']:
                leanid = data_store['spreads'][strid]['lean']
                theo = data_store['spreads'][strid]['theo']
                while str(leanid) in data_store['spreads']:
                    templean = data_store['spreads'][str(leanid)]['lean']
                    temptheo = data_store['spreads'][str(leanid)]['theo']
                    theo += temptheo
                    leanid = templean
                    print('layered theos', strid, leanid, theo)
                mock['price'] = prev.ix[leanid, 'prc'] + theo
                print(strid, leanid, prev.ix[leanid, 'prc'], theo, mock['price'])
            else:
                mock['price'] = row['prc']
        elif row['ctype'] == 'option':
            mock['price'] = data_store['settles']['prc'][strid]
        else:
            mock['price'] = row['prc']
    syncDefs(mock)
    cleanFill(mock)
    data_store['fill_catchup']['fills'].append(mock)
    data_store['md_catchup'][id] = mock['price']
    data_store['settles']['prc'][strid] = round(mock['price'], 5)
    #print id, mock['price']
    #if id == 192946 or equiv == 192946:
    #    print 'okay', mock['price'], mock['side'], mock['qty']

for userid, upos in list(pos_cross_data.items()):
    for id, idpos in list(pos_cross_data[userid].items()):
        if id not in all_defs:
            continue
        id = int(id)
        for s in (1, -1):
            idpos = s * int(idpos)
            mock = {
                    'msg': 'fill', 
                    'exchtradeid': 'P' + str(userid) + str(id),
                    'seqnum': 0,
                    'userid': userid if s == 1 else -1,
                    'usfilltime': 0,
                    'filltime': today.isoformat(),
                    'securityid': id,
                    'id': id,
                    'serverid': userid if s == 1 else -1,
                    'filltype': 0,
                    'ordertype': 'prev_cross',
                    'side': 1 if idpos >= 0 else 2,
                    'qty': abs(idpos),
                    'price': data_store['settles']['prc'][str(id)],
                }
            cleanFill(mock)
            mock['xtrade'] = 1
            print(mock)
            data_store['fill_catchup']['fills'].append(mock)

for id in prevvols:
    syncDefs({'securityid': id})

# margin stuff
if margin:
    op_prod = margin_options_product
    uid = index_ids[op_prod]
    u_settle = settles[uid]
    syncDefs({'securityid': uid})
    data_store['md_catchup'][uid] = u_settle
    data_store['margin'] = {
        'msg': 'margin',
        'scenarios': margin_scenarios,
        'vol_shock': margin_vol_shock,
        'outrights': margin_outrights,
        'spreads': margin_spreads,
        'minimums': margin_minimums,
        'options_product': op_prod,
        'options_underlying_id': index_ids[op_prod],
        'dumb_vols': {}
    }
    for id in prevvols:
        strid = str(id)
        op = -1 if all_defs[id]['op']==2 else 1
        prc = settles.get(id) or data_store['settles']['prc'][strid] or 0.01
        x = all_defs[id]['strike']
        u = settles[index_ids[op_prod]]
        t = (1.0+(todt(parse_datetime(all_defs[id]['expdate'])) - yday).days)/365
        v = bs.implied_vol(op, prc, x, u, t, rate)
        v = 0.03 if v < 0.03 else v
        data_store['margin']['dumb_vols'][strid] = round(v, 3)

# keep track of uniq fills to avoid dupes
def isUniq(fill, uniq_fills):
    if not dupe_handling:
        return True
    key = fill['securityid'], fill['side'], \
          fill['id'], fill['exchtradeid'], fill['filltype']
    if key not in uniq_fills:
        uniq_fills.add(key)
        return True
    return False

uniq_fills = set()

# listens to zmq messages
def DeioListener():
    threadname = inspect.stack()[0][3]
    
    try:
        while 1:
            if kill_switch:
                break
            
            data = json.loads(handler.recv())
            if data['msg'] == 'md' and data.get('securityid') in md_mute:
                continue
            elif data['msg'] == 'fill' and system not in data['system']:
                continue
            
            # fill handling
            if data['msg'] == 'fill' and data['securityid'] in all_defs:
                msgs = syncDefs(data)
                if isUniq(data, uniq_fills):
                    cleanFill(data)
                    msgs.append(data)
                    listeners_now = list(listeners.keys())
                    for msg in msgs:
                        for listener in listeners_now:
                            callback(listener, msg)
                    # save for new listeners
                    data_store['fill_catchup']['fills'].append(data)
            # market data handling
            elif data['msg'] == 'md' and data['securityid'] in all_defs:
                if data['prc'] == 0:
                    continue
                msgs = syncDefs(data)
                id = data['securityid']
                if all_defs[id]['equivid'] > 0:
                    continue
                """
                if id in leanids:
                    # go thruogh all of the ids leaning on this contract
                    for tstrid in leanids[id]:
                        tid = int(tstrid)
                        leanid = data_store['spreads'][tstrid]['lean']
                        theo = data_store['spreads'][tstrid]['theo']
                        prc = data['prc'] + theo
                        mock = {
                                'msg': 'md',
                                'securityid': tid,
                                'prc': prc,
                                'altered': 1
                            }
                        msgs.extend(syncDefs(data))
                        msgs.append(mock)
                        data_store['md_catchup'][tid] = prc
                elif str(id) in data_store['spreads']:
                    continue
                """
                msgs.append(data)
                listeners_now = list(listeners.keys())
                for listener in listeners_now:
                    for msg in msgs:
                        callback(listener, msg)
                # save for new listeners
                data_store['md_catchup'][id] = data['prc']
        # end loop
    except Exception as inst:
        import traceback, io
        fp = io.StringIO()
        traceback.print_exc(file=fp)
        errormsg = fp.getvalue()
        print(errormsg)
    finally:
        print(threadname, 'finished.')


def ModelsListener():
    threadname = inspect.stack()[0][3]
    
    new_prc_threshold = 4
    
    def retrieved_md():
        cmd = "select securityid, lasttradepx as prc from MD"
        qmd = mconn.frame_query(cmd)
        qmd['securityid'] = qmd['securityid'].apply(int)
        #dependents = set(qmd['securityid'].values)-set(leanids)
        qmd['order'] = 0
        qmd.loc[qmd['securityid'].isin(leanids), 'order'] = -1
        qmd = qmd.sort('order')
        md = []
        for i, row in qmd.iterrows():
            data = row.to_dict()
            data['msg'] = 'md'
            if data['prc'] is None or data['prc'] == 0:
                continue
            msgs = syncDefs(data)
            id = data['securityid']
            if id not in all_defs or all_defs[id]['underlyingid'] > 0 or all_defs[id]['equivid'] > 0:
                continue
            if id in leanids:
                # go thruogh all of the ids leaning on this contract
                for tstrid in leanids[id]:
                    tid = int(tstrid)
                    leanid = data_store['spreads'][tstrid]['lean']
                    theo = data_store['spreads'][tstrid]['theo']
                    prc = data['prc'] + theo
                    mock = {
                            'msg': 'md',
                            'securityid': tid,
                            'prc': prc,
                            'altered': 1
                        }
                    msgs.extend(syncDefs(data))
                    msgs.append(mock)
                    data_store['md_catchup'][tid] = prc
            elif str(id) in data_store['spreads']:
                continue
            #md.append(data)
            md.extend(msgs)
            md.extend(data)
            data_store['md_catchup'][id] = data['prc']
        print(md)
        return md

    def retrieved_fills():
        cmd = "select id, seqnum, filltype, side, securityid, userid, \
               if(ordertype=18, 'StrategyPre', 'Regular') as ordertype, \
               serverid, price, qty, \
               if(exchtradeid='', concat('K', id), exchtradeid) \
                   as exchtradeid, filltime \
               from fills \
               where tradedate='" + today_date.isoformat() + "' \
               and filltype<>2 and price<>0"
        qfills = mconn.frame_query(cmd)
        qfills['qty'] = qfills['qty'].astype(int)
        msgs = []
        fills = []
        for i, fill in qfills.iterrows():
            fill = fill.to_dict()
            if isUniq(fill, uniq_fills):
                fill['filltime'] = fill['filltime'].isoformat()
                fill['price'] = round(fill['price'], 5)
                msgs.extend(syncDefs(fill))
                fills.append(fill)
                data_store['fill_catchup']['fills'].append(fill)
        mock = { 'msg': 'fill_catchup', 'fills': fills }
        msgs.append(mock)
        return msgs
    
    def modify_theo(spec):
        spreads = data_store['spreads']
        try:
            id = int(spec[0])
        except ValueError:
            return 0
        data = spec[1].split(',')
        if spec[0] not in spreads:
            if len(data) < 2 or id not in all_defs:
                return 0
            name = all_defs[id]['shortname']
            try:
                prc = float(data[0])
                lean = int(data[1])
            except ValueError:
                return 0
            if lean not in all_defs:
                return 0
            leanname = all_defs[lean]['shortname']
            newdata = [name, leanname, 1, data[0], data[0]]
            sconn.insert_data(theos_table, newdata)
            return 1
        else:
            name = all_defs[id]['shortname']
            if data[0] == '!':
                cmd = "delete from " + theos_table + " \
                        where shortname='" + name + "'"
                sconn.execute(cmd)
                return 1
            try:
                prc = str(float(data[0]))
            except ValueError:
                return 0
            cmd = "update " + theos_table + " set now=" + prc + " \
                where shortname='" + name + "'"
            sconn.execute(cmd)
            return 1
    
    def process_xtrade(items, write):
        print(items)
        if items[0] == 'retrieve':
            print('retrieving cross trades')
            qxtrades = sconn.frame_query("select * from " + xtrades_table)
            for i, row in qxtrades.iterrows():
                me, you, name, price, side, qty = row
                me_id = userids[me]
                you_id = userids[you]
                secid = rev_defs[name]
                process_xtrade([me_id, you_id, secid, price, side, qty], False)
            return 1
        else:
            properties = ('me', 'you', 'securityid', 'price', 'side', 'qty')
            try:
                trade = dict(list(zip(properties, items)))
            except:
                print('malformed cross trade message. ignored')
                return 0
            people = ['me', 'you']
            for i, side in enumerate((1, -1)):
                trade_side = 1 if side * int(trade['side']) > 0 else 2
                userid = int(trade[people[i]])
                mock = {
                    'msg': 'fill',
                    'securityid': int(trade['securityid']), 
                    'userid': userid, 
                    'price': float(trade['price']),
                    'side': trade_side,
                    'qty': int(trade['qty']),
                    'xtrade': 1
                }
                data_store['fill_catchup']['fills'].append(mock)
                listeners_now = list(listeners.keys())
                print(mock)
                for listener in listeners_now:
                    callback(listener, mock)
            if write:
                me = users[trade['me']]
                you = users[trade['you']]
                name = all_defs[int(trade['securityid'])]['shortname']
                price = trade['price']
                side = trade['side']
                qty = trade['qty']
                sconn.insert_data(xtrades_table, [me, you, name, price, side, qty])
            return 1
                
    try:
        xtrade_count = 0
        last_prc = {}
        hb_countdown = 10
        while 1:
            # send a heartbeat to the listeners
            if hb_countdown > 0:
                hb_countdown -= 1
            else:
                listeners_now = list(listeners.keys())
                for listener in listeners_now:
                    callback(listener, 1)
                hb_countdown = 10

            # chill, killer
            sleep(1.1)

            if kill_switch:
                break
           
            now = datetime.now(tzinfo)

            # any pending commands from admin?
            while mysql_commands:
                order = mysql_commands.pop().split('|')
                if order[0] == 'md':
                    print('fetching current md')
                    msgs = retrieved_md()
                    print('retrieved and processed. sending to listeners.')
                    listeners_now = list(listeners.keys())
                    for msg in msgs:
                        for listener in listeners_now:
                            callback(listener, msg)
                elif order[0] == 'fills':
                    print('querying fills from db')
                    msgs = retrieved_fills()
                    print('retrieved and processed. sending to listeners')
                    print(len(msgs[-1]['fills']), 'unaccounted for')
                    listeners_now = list(listeners.keys())
                    for msg in msgs:
                        for listener in listeners_now:
                            callback(listener, msg)
                elif order[0] == 'spreads':
                    spec = order[1].split('=')
                    if len(spec) < 2:
                        continue
                    print('changing spread', order[1])
                    success = modify_theo(spec)
                    if success:
                        print('executed.')
                    else:
                        print('failed.')
                elif order[0] == 'xtrade':
                    items = order[1].split(',')
                    success = process_xtrade(items, True)
                    if success:
                        print('executed.')
                    else:
                        print('failed.')
                    print(items)
                # yup
            
            # now important things
            if all_options:
                qvols = vconn.frame_query("""
                    SELECT securityid, volatility as vol, synthetic as roll
                    FROM optionsPricingModelData
                    WHERE securityid in (%s)
                    """ % (', '.join(map(str, all_options)), )
                )
                qvols = qvols.fillna(0.0)
                qvols.index = qvols['securityid'].astype(int)
                del qvols['securityid']
                qvols = qvols.T.to_dict()
            else:
                qvols = {}
            msgs = []
            uids = set()
            vols = {}
            new_prc = {}
            for id, data in list(qvols.items()):
                if id not in all_defs:
                    continue
                strid = str(id)
                msgs.extend(syncDefs({ 'securityid': id }))
                defs = all_defs[id]
                uid = defs['underlyingid']
                #print defs['shortname'], defs['expdate'], uid
                if uid not in data_store['md_catchup']:
                    continue
                fprc = data_store['md_catchup'][uid]
                if uid not in uids:
                    uids.add(uid)
                op = -1 if defs['op'] == 2 else 1
                x = defs['strike']
                yte = defs['yte']
                v = data['vol']
                roll = data['roll']
                u = fprc + roll
                vol_obj = { 'vol': round(v, 5), 'roll': round(roll, 3) }
                if id not in last_prc:
                    last_prc[id] = 0.0
                vols[strid] = vol_obj
                oprc = round(bs.tv(op, x, u, v, yte, rate), 3)
                if abs(oprc - last_prc[id]) > 0.001:
                    new_prc[id] = oprc
            # determine whether to update clients
            #for k, v in vols.items():
            #    if int(k) in all_defs:
            #        print all_defs[int(k)]['shortname'], v
            if len(new_prc) > new_prc_threshold:
                vols['msg'] = 'vols'
                msgs.append(vols)
                data_store['vols'] = vols
                for id in new_prc:
                    last_prc[id] = new_prc[id]
            listeners_now = list(listeners.keys())
            for msg in msgs: 
                for listener in listeners_now:
                    callback(listener, msg)
            
            # spread values now
            spreads = processTheosTable(theos_table)
            is_new = False
            if len(spreads) != len(data_store['spreads']):
                is_new = True
            for strid in spreads:
                if is_new:
                    break
                if strid == 'msg':
                    continue
                theo = spreads[strid]['theo']
                if strid not in data_store['spreads'] \
                        or theo != data_store['spreads'][strid]['theo']:
                    is_new = True
            if is_new:
                msgs = []
                for strid in spreads:
                    if strid == 'msg':
                        continue
                    leanid = spreads[strid]['lean']
                    #if leanid not in data_store['md_catchup']:
                    #    print 'cannot find', leanid
                    #    continue
                    mocks = [
                        {'securityid': int(strid)},
                        {'securityid': leanid} #spreads[strid]['lean']}
                    ]
                    for mock in mocks:
                        msgs.extend(syncDefs(mock))
                    #if leanid not in data_store['md_catchup']:
                    #    print leanid, 'not found yet...'
                    #    continue
                    if leanid not in data_store['md_catchup']:
                        print('leanid', leanid, 'not in md yet? take the settle...')
                        syncDefs({'securityid': leanid})
                        if leanid not in settles:
                            data_store['md_catchup'][leanid] = 0
                        else:
                            data_store['md_catchup'][leanid] = settles[leanid] #mock['price']
                    newprc = data_store['md_catchup'][leanid] + spreads[strid]['theo']
                    mock_md = {'msg': 'md', 'securityid': int(strid), 'prc': newprc}
                    msgs.append(mock_md)
                msgs.append(spreads)
                data_store['spreads'] = spreads
                listeners_now = list(listeners.keys())
                for msg in msgs:
                    for listener in listeners_now:
                        callback(listener, msg)

    except Exception as inst:
        import traceback, io
        fp = io.StringIO()
        traceback.print_exc(file=fp)
        errormsg = fp.getvalue()
        print(errormsg)
    finally:
        print(threadname, 'finished.')

def stringify_keys(mydict):
    output = {}
    for k, v in list(mydict.items()):
        if type(k) != type(''):
            output[str(k)] = v
        else:
            output[k] = v
    return output

# websocket server
class WebSocketServer(tornado.websocket.WebSocketHandler):
    def open(self):
        keys = list(data_store.keys())
        for pkey in ('defs_catchup', 'md_catchup'):
            keys.pop(keys.index(pkey))
            #self.write_message(json.dumps(data_store[pkey], allow_nan=False))
            self.write_message(json.dumps(stringify_keys(data_store[pkey]), allow_nan=False))
        for key in keys:
            self.write_message(json.dumps(data_store[key], allow_nan=False))
        listeners[self] = {}
        print('OPEN.', len(listeners), 'listeners.')

    def on_message(self, message):
        if message[0] == '0':
            data = json.loads(message[1:]) #, object_hook=as_str)
            msg = data['msg']
            command = data['command']
            specific = data['specific']
            if msg != 'admin':
                return
            if command == 'md':
                mysql_commands.appendleft('md')
            elif command == 'fills':
                mysql_commands.appendleft('fills')
            elif command == 'spreads':
                mysql_commands.appendleft('spreads|'+specific)
            elif command == 'xtrade':
                print(specific)
                mysql_commands.appendleft('xtrade|'+specific)
        elif message[0] == '2':
            # heartbeat response... ignore
            return 0
        self.write_message(message)

    def on_close(self):
        listeners.pop(self)
        print('CLOSE.', len(listeners), 'listeners.')

app = tornado.web.Application([
        (r'/', WebSocketServer)
    ])

if __name__ == '__main__':
    # start some threads
    deioThread = threading.Thread(target=DeioListener)
    deioThread.daemon = True
    deioThread.start()
  
    modelsThread = threading.Thread(target=ModelsListener)
    modelsThread.daemon = True
    modelsThread.start()
    
    print('starting websocket server.') 
    try:
        # websocket server
        http_server = tornado.httpserver.HTTPServer(app)
        http_server.listen(port)
        tornado.ioloop.IOLoop.instance().start()
    except Exception as inst:
        import traceback, io
        fp = io.StringIO()
        traceback.print_exc(file=fp)
        errormsg = fp.getvalue()
        print(errormsg)
    finally:
        kill_switch = True
        mconn.close()
        vconn.close()
        sconn.close()
        #handler.shutdown()
        context.destroy()
        deioThread.join()
        modelsThread.join()
        print('all dead!')
