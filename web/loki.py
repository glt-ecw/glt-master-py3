import subprocess, signal, os

pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]

for pid in pids:
  try:
    proc = open(os.path.join('/proc', pid, 'cmdline'), 'rb').read()
    if 'run_tornado' in proc or 'thor' in proc:
      os.kill(int(pid), signal.SIGKILL)
      print('killed', pid, 'of', proc)
  except:
    print('error on', pid)
    continue
