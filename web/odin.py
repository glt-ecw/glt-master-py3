import zmq
import threading
import simplejson as json

from collections import deque
from ciso8601 import parse_datetime as parse_datetime

datasources = {
    'sy': 'epgm://10.1.31.202;239.160.160.170:20107',
    'hy': 'epgm://10.1.31.202;239.50.50.168:26811',
    #'bk': 'epgm://10.1.31.202;239.161.161.170:20177',
    'jpx': 'epgm://10.1.31.202;239.50.50.110:30107',
    'cme': 'epgm://10.1.31.202;239.101.101.107:40700',
    'w': 'epgm://10.1.31.202;239.50.50.168:12811',
    'wuat': 'epgm://10.1.31.202;239.50.50.168:13811',
    #'lead': 'epgm://10.1.31.202;239.138.50.110:13807',
}

context = zmq.Context()
md = context.socket(zmq.SUB)
fills = dict()
for source, addr in list(datasources.items()):
    if 'uat' not in source:
        md.connect(addr)
    sock = context.socket(zmq.SUB)
    sock.connect(addr)
    sock.setsockopt(zmq.SUBSCRIBE, 'FILL')
    fills[source] = sock
md.setsockopt(zmq.SUBSCRIBE, 'MD')

publisher = context.socket(zmq.PUB)
publisher.bind('ipc://asgard.ipc')

# heim handling
heim = context.socket(zmq.SUB)
heim.connect('ipc://rainbow_bridge.ipc')
heim.setsockopt(zmq.SUBSCRIBE, '')

#def object_hook(dct):
#    output = dict()
#    for k, v in dct.items():
#        if type(v) == type(u""):
#            output[k] = str(v)
#        else:
#            output[k] = v
#    return output

def heim_proc():
    print('starting heim connections')
    while 1:
        msg = json.loads(heim.recv()) #, object_hook=object_hook)
        if msg['msg'] == 'md':
            id = msg['securityid']
            publisher.send(json.dumps(msg))
        elif msg['msg'] == 'fill':
            publisher.send(json.dumps(msg))

# fills handling
msg_md_col = ['msg', 'securityid'] + 6*[0] + ['prc'] + 9*[0]
msg_fill_col = ['msg', 'id', 'seqnum', 'filltype', 'side',
                'securityid', 'userid', 'serverid', 'price',
                #'qty', 'exchtradeid', 'strategy', 'filltime']
                'qty', 'exchtradeid', 'ordertype', 'filltime']

def fills_proc(system):
    sock = fills[system]
    print('starting fills connection to', system)
    while 1:
        raw = sock.recv().split(',')
        raw[0] = raw[0].lower()
        msg = dict(list(zip(msg_fill_col, raw)))
        for key, func in list(msg_fill_parser.items()):
            if func:
                msg[key] = func(msg[key])
                if key == 'filltype' and msg[key] == 2:
                    continue
            elif key == 'exchtradeid' and msg[key] == '':
                msg[key] = 'K' + str(msg['id'])
        msg['system'] = system
        json_msg = json.dumps(msg)
        publisher.send(json_msg)
        #heim.send(json_msg)

def int_bidoffer(item):
    return 1 if item == 'bid' else 2

def reformat_dt(item):
    return parse_datetime(item).isoformat()

msg_fill_parser = {
    'msg': None,
    'id': int,
    'seqnum': int,
    'filltype': int,
    'side': int_bidoffer,
    'securityid': int,
    'userid': int,
    'serverid': int,
    'price': float,
    'qty': int,
    'exchtradeid': None,
    'ordertype': None,
    'filltime': reformat_dt
}

procs = dict()
p = threading.Thread(target=heim_proc)
p.start()
procs['heim'] = p
for system, sock in list(fills.items()):
    p = threading.Thread(target=fills_proc, args=(system,))
    p.start()
    procs[system] = p

print('odin knows all')
try:
    prevprc = dict()
    while 1:
        raw = md.recv().split(',')
        raw[0] = raw[0].lower()
        msg = dict(list(zip(msg_md_col, raw)))
        del msg[0]
        msg['securityid'] = int(msg['securityid'])
        id = msg['securityid']
        if msg['prc'] == 'nan':
            msg['prc'] = 0.0
        else:
            msg['prc'] = float(msg['prc'])
        if id not in prevprc or msg['prc'] != prevprc[id]:
            prevprc[id] = msg['prc']
            json_msg = json.dumps(msg)
            publisher.send(json_msg)
finally:
    context.destroy()
    for system, proc in list(procs.items()):
        print('terminating', system)
        proc.join()
