#import MySQLdb as mysql
#import oursql as mysql
import sqlalchemy

from numpy import array
#from pandas.io.sql import frame_query
from pandas import DataFrame

def columntype(column_list, type_list, index_col=None):
    column_str = ' ( '
    for k, t in zip(column_list, type_list):
        column_str += k+' '+t+', '
    if index_col:
        column_str += 'INDEX idx_'+index_col+' ('+index_col+' ASC)'
        return column_str + ' ) '
    else:
        return column_str[:-2] + ' ) '
def columnvalues(column_list, values_list):
    cv_str = ''
    for c, v in zip(column_list, values_list):
        cv_str += c+' = '
        if type(v) == type(''):
            cv_str += '\''+v+'\', '
        else:
            cv_str += '\''+str(v)+'\', '
    return cv_str[:-2]
def values(values_list):
    values_str = ' ( '
    for k in values_list:
        if type(k) == type(''):
            values_str += '\''+k+'\', '
        else:
            values_str += '\''+str(k)+'\', '
    return values_str[:-2] + ' ) '
def columns(columns_list):
    columns_str = ' ( '
    for k in columns_list:
        columns_str += k+', '
    return columns_str[:-2] + ' ) '

class conn():
    conn = None
    def __init__(self, host, user, passwd, db='', tz=None):
        #   'mysql+oursql://deio:!w3alth!@10.1.31.202/DEIO_SY'
        self.connection_string = "mysql+oursql://%s:%s@%s/%s"%(user,passwd,host,db)
        self.engine = sqlalchemy.create_engine(self.connection_string)
        self.open()
        self.set_time_zone(tz) 
    def close(self):
        if self.conn:
            self.conn.close()
    def execute(self, command):
        self.result = self.conn.execute(command)
    def fetchall(self, dtype=object):
        if self.result:
            return array(self.result.fetchall(), dtype=dtype)
        return None
    def frame_query(self, command, index_col=None):
        self.execute(command)
        columns = list(self.result.keys())
        data = self.fetchall()
        if len(data):
            return DataFrame(data=data, columns=columns)
        else:
            return DataFrame(columns=columns)
    def open(self):
        self.close()
        self.conn = self.engine.connect()
    def set_time_zone(self, tz=None):
      if tz:
        self.execute('set time_zone=\''+tz+'\'')
    # functions for writing and inserting
    def create_table(self, name, column_list, type_list, index_col=None):
        column_str = columntype(column_list, type_list, index_col)
        cmd_txt = 'CREATE TABLE IF NOT EXISTS '+name+column_str
        self.execute(cmd_txt)
    def create_unique_index(self, name, idx_column):
        cmd_txt = 'CREATE UNIQUE INDEX idx_const_'+idx_column+' ON '\
            +name+'('+idx_column+')'
        self.execute(cmd_txt)
    def insert_data(self, name, data):
        self.execute('INSERT INTO '+name+' VALUES '+values(data))
    def upsert_data(self, name, column_list, data):
        column_str = 0
        cmd_txt = 'INSERT INTO ' + name \
            + columns(column_list) \
            + ' VALUES ' + values(data) \
            + ' ON DUPLICATE KEY UPDATE ' + columnvalues(column_list, data)
        self.execute(cmd_txt)
