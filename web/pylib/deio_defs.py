import pytz

RES01 = '10.1.31.200'
DBSERVER01 = '10.1.31.200'
BD02 = '10.130.33.103'
LABCAP01 = '10.180.33.103'

SECURITIES = 'SECURITIES'
KRX_DB = 'DEIO_W'
KRX_UATDB = 'DEIO_UAT'
CME_DB = 'DEIO_CME'
DEFS_DB = 'DEIO_META'
JPX_DB = 'DEIO_JPA'

KRX_VOL = 'eod_krxvols'
CME_VOL = 'eod_cmevols'

MONTHCODE = 'FGHJKMNQUVXZ'

WARRANT_SIZE_MULTIPLIER = 0.0002
WARRANT_PRC_MULTIPLIER = 0.01
MKOO_SIZE_MULTIPLIER = 0.2

BIZDAYS = 261.0

TZINFO = {
    'krx': pytz.timezone('Asia/Tokyo'), # Asia/Seoul by default is +8:30, not +9:00
    'cme': pytz.timezone('America/Chicago'),
    'jpx': pytz.timezone('Asia/Tokyo'),
}

SYSTEMS = {
    'krx': ('bk', 'sy', 'hy', 'w', 'lead'),
    'jpx': ('jpx',),
    'cme': ('cme',),
  }

DUPE_HANDLING = {
    'w': True,
    'sy': True,
    'hy': True,
    'bk': True,
    'lead': True,
    'jpx': True,
    'cme': True,
}

EXCHANGES = {
    'kis': 'krx',
    'bk': 'krx',
    'cme': 'cme',
    'sy': 'krx',
    'hy': 'krx',
    'w': 'krx',
    'lead': 'krx',
    'jpx': 'jpx',
    'sy_archive': 'krx'
  }

EXCH_CATEGORY = {
    'krx': '(1)',
    'jpx': '(4, 5)',
    'cme': '(2)'
}

RATES = {
    'krx': 0.025,
    'cme': 0.001,
    'jpx': 0.001,
  }

FEE_TABLES = {
    'kis': 'krx_fees',
    'bk': 'krx_fees',
    'cme': None,
    'sy': 'krx_fees',
    'w': 'krx_fees',
    'hy': 'krx_fees',
    'lead': 'krx_fees',
    'jpx': 'jpx_fees',
    'sy_archive': 'krx_fees'
  }

BRO_TABLES = {
    'kis': 'kis_bro',
    'bk': None,
    'cme': None,
    'sy': 'sy_bro',
    'w': 'w_bro',
    'hy': 'hy_bro',
    'lead': None,
    'jpx': 'jpx_bro',
    'sy_archive': 'sy_bro'
  }

HOLIDAYS = {
    'kis': 'krx_holidays',
    'bk': 'krx_holidays',
    #'cme': None,
    'sy': 'krx_holidays',
    'w': 'krx_holidays',
    'hy': 'krx_holidays',
    'lead': 'krx_holidays',
    #'jpx': None,
    'sy_archive': 'krx_holidays'
  }

MYSQL_HOSTS = {
    'kis': DBSERVER01,
    'bk': BD02,
    'cme': DBSERVER01,
    'sy': DBSERVER01,
    'w': DBSERVER01,
    'hy': DBSERVER01,
    'lead': DBSERVER01,
    'jpx': DBSERVER01,
    'sy_archive': LABCAP01
  }

MYSQL_DB = {
    'kis': 'DEIO_KIS',
    'bk': 'DEIO_BK',
    'cme': 'DEIO_CME',
    'sy': 'DEIO_SY',
    'w': 'DEIO_W',
    'hy': 'DEIO_HY',
    'lead': 'DEIO_LEADTEST',
    'jpx': 'DEIO_JPA',
    'sy_archive': 'SY_ARCHIVE'
  }

MYSQL_UATDB = {
    'kis': 'DEIO_UAT',
    'bk': 'DEIO_UAT',
    'cme': 'DEIO_CMEUAT',
    'sy': 'DEIO_SYUAT',
    'w': None,
    'hy': None,
    'lead': None,
    'jpx': None,
    'sy_archive': None
  }

TIMEZONE_ADJUST = {
    'bk': None,
    'sy': None,
    'w': None,
    'lead': None,
    'sy_archive': '+9:00',
    'cme': None
  }

MANUAL_MUTE = {
    'krx': (2844,),
    'jpx': (),
    'cme': (),
}

INDEX_IDS = {
    'krx': {
        'KO': 2844,
        'MKO': 2844
    },
    'jpx': {},
    'cme': {}
}

POSITION_CROSS = {
    'sy': {
        'sy03': 40
    }
}

VOL_TABLE = {
    'krx': 'eod_krxvols',
    'jpx': None, #vols'
    'cme': 'eod_cmevols',
  }

SETTLES_TABLE = {
    'krx': 'krx_settles',
    'jpx': 'jpx_settles',
    'cme': 'cme_settles',
  }

VOLS_DB = {
    'krx': KRX_DB,
    'krxuat': KRX_UATDB,
    'cme': CME_DB
  }

EXCEL_MD = {
    'kis': 'kis_md',
    'bk': None,
    'sy': 'sy_md',
    'cme': 'cme_md'
  }

EXCEL_EOD = {
    'kis': 'eod_kis',
    'bk': 'eod_bk',
    'sy': 'eod_sy',
    'sy_archive': 'eod_sy',
    'cme': 'eod_cme'
  }

THEOS_TABLES = {
    'krx': 'krx_theo',
    'jpx': 'jpx_theo',
    'cme': 'cme_theo'
  }

POSITIONTRADES_TABLES = {
    'kis': 'kis_position_trades',
    'bk': None,
    'sy': 'sy_position_trades',
    'cme': 'cme_position_trades'
  }

XTRADES_TABLES = {
    'kis': None,
    'bk': 'bk_xtrades',
    'sy': 'sy_xtrades',
    'w': 'w_xtrades',
    'hy': 'hy_xtrades',
    'lead': 'lead_xtrades',
    'jpx': 'jpx_xtrades',
    'sy_archive': None,
    'cme': None #'cme_xtrades'
  }

# margin stuff

MARGIN_SCENARIOS = {
    'krx': 31
}

MARGIN_VOL_SHOCK = {
    'krx': [1.3, 0.7]
}

MARGIN_OUTRIGHTS = {
    'krx': {
        'KO': 0.09,
        'MKO': 0.09,
        'KWU': 0.045,
        'KTB3': 0.009,
        'KTB10': 0.0255,
        'KSFSKH': 0.1425,

    }
}

MARGIN_SPREADS = {
    'krx': {
        'KO': 0.018,
        'MKO': 0.018,
        'KWU': 0.003,
        'KTB3': 0.003,
        'KTB10': 0.003,
        'KSFSKH': 0.0
    }
}

MARGIN_MINIMUMS = {
    'krx': {
        'KO': 50000,
        'MKO': 50000,
        'KWU': 50000,
        'KTB3': 50000,
        'KTB10': 10000,
        'KSFSKH': 50000
    }
}

MARGIN_OPTIONS_PRODUCT = {
    'krx': 'KO'
}

CASH_NORMALIZER = {
    'krx': 1e-5,
    'jpx': 1e-2,
    'cme': 1.0
}

PT_VALUES = {
    'krx': {
        'KTB3':     1000000.0,
        'KTB5':     1000000.0,
        'KTB10':    1000000.0,
        'KWU':      10000.0,
        'KO':       500000.0,
        'MKO':      100000.0,
        'KTBC':     10000000.0,
        'CASH':     1.0 #1e-5
    },
    'cme': {
        'ES': 50.0,
        'NKD': 5.0,
        'NIY': 500.0,
        'KE': 50.0,
        'ZW': 50.0,
        'ZL': 600.0,
        'ZM': 100.0,
        'ZS': 50.0,
        'ZC': 50.0
    },
    'jpx': {
        'CASH': 1.0, #1e-2, #1.0,
        'NK': 1000.0, 
        'MNK': 100.0,
        'SNK': 500.0,
        'TP': 10000.0,
        'MTP': 1000.0,
        'JGBM': 1000000.0,
        'JGBL': 1000000.0,
        'SJGBL': 100000.0,
        'SNF': 2.0,
        'STW': 100.0,
        'SXU': 1.0,
        'SSG': 200.0,
        'NK4': 100.0,
    }
}

CURRENCY_CONV = {
    'krx': {
        'CASH': 'KRW',
    },
    'cme': {
        'CASH': 'USD',
        'ES': 'USD',
        'NKD': 'USD',
        'NIY': 'JPY'
    },
    'jpx': {
        'CASH': 'USD',
        'NK': 'JPY', 
        'MNK': 'JPY',
        'SNK': 'JPY',
        'TP': 'JPY',
        'JGBM': 'JPY',
        'JGBL': 'JPY',
        'SJGBL': 'JPY',
        'SSG': 'SGD',
        'NK4': 'JPY', 
    }
}

NORM_QTY = {
    'krx': {
        'MKO': ('KO',),
    },
    'cme': {
        'SP': ('ES',),
    },
    'jpx': {
        'MNK': ('NK',),
        'SNK': ('NK', 'MNK',),
        'MTP': ('TP',),
        #'SMJBL': ('JGBL',)
    }
}

RELATED_PRODUCTS = {
  'krx': dict(),
  'cme': {
      'SP':   ('ES', 5.0),
      'EW':   ('ES', 1.0),
      'EW1':  ('ES', 1.0),
      'EW2':  ('ES', 1.0),
      'EW3':  ('ES', 1.0),
      'EW4':  ('ES', 1.0)
    }
  }

ABN_PRODUCT_CONVERSION = {
    'CN': 'SXU',
    'IN': 'SNF',
}

ABN_MONTHCODE = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 
                 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']

# functions
def future_product(name):
  return name[:-3]
def option_product(name):
  return name.split('_')[0][:-4]
def warrant_product(name):
  return name[:name.find('W')]
def warrant_house(name):
  return name[name.find('W')+1:name.find('_')]
def warrant_monthcode(dt):
  return MONTHCODE[dt.month-1]+str(dt.year%100)
def warrant_shortername(name):
  split = name.split('_')
  if len(split) == 1:
    return name
  else:
    expiry = MONTHCODE[int(split[1][-4:-2])-1]+split[1][2:4]
    return split[0][:6]+':'+expiry+'_'+split[2]
