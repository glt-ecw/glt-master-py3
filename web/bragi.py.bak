import sys
import os
import operator
import datetime as dt
import ciso8601

import numpy as np

import json

from ciso8601 import parse_datetime
from websocket import create_connection

ip = 'ws://10.1.31.202'
port = sys.argv[1]

monitors = {
    'margin': {
        'limit': np.inf,
        'func': operator.__gt__
    },
    'daily_pnl': {
        'limit': -np.inf,
        'func': operator.__lt__
    },
    'monthly_pnl': {
        'limit': -np.inf,
        'func': operator.__lt__
    },
    'etf_notional': {
        'limit': np.inf,
        'func': operator.__gt__
    }
}

EPSILON = 1e-4
nd_constants = {'m': np.sqrt(2 * np.pi), 'sqrt2': np.sqrt(2)}

def prettify(x):
    factor = 1000
    return int(round(x / factor)*factor)

def erfc(x):
    z = abs(x)
    t = 1.0/(1 + z/2)
    r = t * np.exp(-z * z - 1.26551223 + t * (1.00002368 + \
                   t * (0.37409196 + t * (0.09678418 + t * (-0.18628806 + \
                   t * (0.27886807 + t * (-1.13520398 + t * (1.48851587 + \
                   t * (-0.82215223 + t * 0.17087277)))))))))
    return r if x >= 0 else 2-r

def implied_vol(model_object, u, prc, seed_vol=0, threshold=None):
    ticks = threshold or 0.001
    guess = 0.0
    v = seed_vol if seed_vol > 0 else 1.0
    for i in xrange(20):
        model_object.calculate_greeks(u, v)
        guess = model_object.tv
        diff = prc - guess
        if abs(diff) < ticks:
            return v
        vega = 100 * model_object.vega
        if vega < ticks:
            return -1
        v += diff / vega
    return -1

def n_pdf(x):
    return np.exp(-pow(x, 2)/2)/nd_constants['m']

def n_cdf(x):
    return 0.5*erfc(-x/nd_constants['sqrt2'])


class BlackModelOXTR:
    def __init__(self, bizdays, op, x, t, r):
        self.bizdays = bizdays
        self.one_day = 1 / bizdays
        self.op = op
        self.x = x
        self.t = EPSILON if t <= 0 else t
        self.r = r
        self.neg_rt = np.exp(-r * t)
        self.log_x = np.log(x)
        self.sqrt_t = np.sqrt(t)

    def pre_calc(self, u, v):
        d1 = (np.log(u) - self.log_x + 0.5 * pow(v, 2) * self.t) / (v * self.sqrt_t)
        d2 = d1 - v * self.sqrt_t
        self.n_cdf_d1 = n_cdf(self.op * d1)
        self.n_cdf_d2 = n_cdf(self.op * d2)
        self.n_pdf_d1 = n_pdf(d1)
        self.tv = self.neg_rt * self.op * (u * self.n_cdf_d1 - self.x * self.n_cdf_d2)
        return 0

    def calculate_greeks(self, u, vol):
        v = EPSILON if vol <= 0 else vol
        self.pre_calc(u, v)
        self.vega = 0.01 * u * self.neg_rt * self.sqrt_t * self.n_pdf_d1 
        return 0

class BlackScholesModelOXTR:
    def __init__(self, bizdays, op, x, t, r):
        self.bizdays = bizdays
        self.one_day = 1 / bizdays
        self.op = op
        self.x = x
        self.t = EPSILON if t <= 0 else t
        self.r = r
        self.neg_rt = np.exp(-r * t)
        self.log_x = np.log(x)
        self.sqrt_t = np.sqrt(t)

    def pre_calc(self, u, v):
        d1 = (np.log(u) - self.log_x + (self.r + 0.5 * pow(v, 2)) * self.t) / (v * self.sqrt_t)
        d2 = d1 - v * self.sqrt_t
        self.n_cdf_d1 = n_cdf(self.op * d1)
        self.n_cdf_d2 = n_cdf(self.op * d2)
        self.n_pdf_d1 = n_pdf(d1)
        self.tv = self.op * (u * self.n_cdf_d1 - self.x * self.neg_rt * self.n_cdf_d2)
        return 0

    def calculate_greeks(self, u, vol):
        v = EPSILON if vol <= 0 else vol
        self.pre_calc(u, v)
        self.vega = 0.01 * u * self.sqrt_t * self.n_pdf_d1 
        return 0

    def implied_vol(self, u, prc, seed_vol=0, threshold=None):
        return implied_vol(self, u, prc, seed_vol, threshold)


class Monitor:
    def __init__(self, name, limit, comparison_func):
        self.violation = False
        self.name = name
        self.comparison_func = comparison_func
        self.set_limit(limit)

    def check_value(self, value):
        if not self.violation and self.comparison_func(value, self.limit):
            self.violation = True
            print 'violation', self.name, value, 'past limit', self.limit
        elif self.violation and not self.comparison_func(value, self.limit):
            self.violation = False
            print 'back to normal', self.name, value, 'under limit', self.limit
        return self.violation

    def set_limit(self, limit):
        self.limit = limit
        return 0


class Decider:
    def __init__(self, start_func, stop_func):
        self.any_violations = False
        self.start_func = start_func
        self.stop_func = stop_func
        self.monitors = {}

    def add_monitor(self, key, limit, comparison_func):
        self.monitors[key] = Monitor(key, limit, comparison_func)
        return 0

    def check_violations(self):
        if not self.any_violations and any([m.violation for k, m in self.monitors.items()]):
            print 'need to send stop'
            self.stop_func()
            self.any_violations = True
        elif self.any_violations and not any([m.violation for k, m in self.monitors.items()]):
            print 'removing stop'
            self.start_func()
            self.any_violations = False
        return self.any_violations

    def check_value(self, name, value):
        if name in self.monitors:
            self.monitors[name].check_value(value)
            self.check_violations()
            return 0
        else:
            print 'cannot find monitor', name
            return 1

    def set_limit(self, name, limit):
        if name in self.monitors:
            self.monitors[name].set_limit(limit)
            return 0
        else:
            print 'cannot find monitor', name
            return 1


class DataStore:
    inventory = {}
    prcqty = {}
    defs = {}
    defitems = ['ctype', 'shortname', 'op', 'strike', 'ptval', 'product']
    md = {}
    options = {}
    monthpnl = 0.0
    def __init__(self, data):
        for key in data:
            if key == 'defs' or key == 'users':
                # maybe do something interesting here?
                continue
            else:
                self.defs[key] = data[key]
        for cdata in data['defs']:
            del cdata['msg']
            self.add_contract_def(cdata)

    def add_contract_def(self, cdata):
        newdef = {}
        for strid, data in cdata.items():
            id = int(strid)
            for defitem in self.defitems:
                newdef[defitem] = data[defitem]
            newdef['yte_calendar'] = ((parse_datetime(data['expdate']).date()-dt.datetime.now().date()).days+1.0)/365
            if data['ctype'] == 'option':
                oxtr = BlackModelOXTR(self.defs['bizdays'], -1 if newdef['op']>1 else 1, 
                                      newdef['strike'], data['yte'], self.defs['rate'])
                self.options[id] = {'vol': 0.0, 'roll': 0.0, 'oxtr': oxtr, 'uid': data['underlyingid']}
            self.defs[id] = newdef
            self.inventory[id] = 0
            self.prcqty[id] = 0
        return 0

    def catchup_fills(self, data):
        for fill in data['fills']:
            self.add_fill(fill)
        return 0

    def add_fill(self, data):
        side = -1 if data['side'] == 2 else 1
        id = data['securityid']
        self.inventory[id] += side * data['qty']
        self.prcqty[id] += side * data['qty'] * data['price']
        return 0

    def catchup_md(self, data):
        for strid in data:
            id = int(strid)
            if self.defs[id]['op'] == 0:
                cdata = {'securityid': id, 'prc': data[strid]}
                self.update_md(cdata)
        return 0

    def update_md(self, data):
        self.md[data['securityid']] = data['prc']

    def update_vols(self, data):
        for strid, values in data.items():
            id = int(strid)
            option = self.options[id]
            v, roll = values['vol'], values['roll']
            u = self.md[option['uid']] + roll
            option['oxtr'].calculate_greeks(u, v)
            option['vol'] = v
            option['roll'] = roll
            self.md[id] = option['oxtr'].tv
        return 0

class Margin:
    derivative_types = ['future', 'option']
    positions = {}
    invalid_ids = []
    ptvals = {}
    futures = {}
    options = {}
    scenarios = {}
    display = []
    datastore = None
    
    def __init__(self, parameters, datastore):
        self.datastore = datastore
        self.minimums = parameters['minimums']
        self.options['product'] = parameters['options_product']
        self.options['uid'] = parameters['options_underlying_id']
        #self.options['vols'] = {}
        self.options['vol_shock'] = np.array(parameters['vol_shock'], dtype=np.float64)
        self.options['contracts'] = {}
        self.options['underlying_array'] = np.zeros(parameters['scenarios'] * len(parameters['vol_shock']), dtype=np.float64)
        self.outright_rates = parameters['outrights']
        self.spread_rates = parameters['spreads']
        self.num_scenarios = parameters['scenarios']
        self.buckets = np.zeros(self.options['underlying_array'].shape[0])
        self.margin = 0.0
        self.scenario_number = 0

    def clear_scenarios(self):
        self.margin = 0.0
        self.scenario_number = 0
        for i in xrange(len(self.buckets)):
            self.buckets[i] = 0
        for product in self.scenarios:
            if product == self.options['product']:
                for i in xrange(len(self.scenarios[product].shape)):
                    self.scenarios[product][i] = 0
            else:
                self.scenarios[product] = 0.0
        return 0

    def clear_futures(self):
        for product in self.futures:
            self.clear_futures_product(product)

    def clear_futures_product(self, product):
        self.futures[product]['net_positions'] = 0
        self.futures[product]['abs_positions'] = 0
        self.futures[product]['min_positions'] = np.inf

    def read_inventory(self):
        self.clear_futures()
        one_day = 1.0/365
        for strid in self.datastore.inventory:
            id = int(strid)
            if id in self.invalid_ids:
                continue
            idef = datastore.defs[id]
            yte = idef['yte_calendar'] - one_day
            if (yte < one_day):
                continue
            product, ctype = idef['product'], idef['ctype']
            if id not in self.positions:
                if ctype not in self.derivative_types:
                    self.invalid_ids.append(id)
                    continue
                self.positions[id] = {'contract': idef['shortname'], 'inventory': 0}
                if ctype == 'future':
                    if product not in self.futures:
                        self.futures[product] = {'contracts': []}
                        self.clear_futures_product(product)
                    self.futures[product]['contracts'].append(id)
                else:
                    self.options['contracts'][id] = BlackScholesModelOXTR(365.0, -1 if idef['op']>1 else 1, 
                                                                          idef['strike'], yte, self.datastore.defs['rate'])
                if product not in self.scenarios:
                    self.ptvals[product] = round(idef['ptval'] / self.datastore.defs['cash_normalizer'])
                    if product == self.options['product']:
                        self.scenarios[product] = np.zeros(len(self.options['vol_shock']) * self.num_scenarios, dtype=np.float64)
                    else:
                        self.scenarios[product] = 0.0
            self.positions[id]['inventory'] = self.datastore.inventory[id]
            pos = self.positions[id]['inventory']
            if ctype == 'future':
                self.futures[product]['net_positions'] += pos
                self.futures[product]['abs_positions'] += abs(pos)
                min_position = np.inf if pos == 0 else abs(pos)
                self.futures[product]['min_positions'] = min(self.futures[product]['min_positions'], min_position)
        return 0

    def compute_scenarios(self):
        self.read_inventory()
        self.sync_md()
        return 0

    def sync_md(self):
        self.clear_scenarios()
        futures, options = self.futures, self.options
        p = self.positions
        options_scenarios = self.scenarios[options['product']]
        md = self.datastore.md
        contracts = options['contracts']
        divisor = np.floor(self.num_scenarios/2)
        opt_ptval = self.ptvals[options['product']]
        opt_rate = self.outright_rates[options['product']]
        shock = options['vol_shock']
        vols = {}
        u_base = md[options['uid']]
        option_premium = 0
        oa_factor = -0.3
        for product in futures:
            id = futures[product]['contracts'][0]
            prc = md[id]
            if futures[product]['abs_positions'] == abs(futures[product]['net_positions']):
                futures[product]['min_positions'] = 0
            outright_factor = prc * self.ptvals[product] * self.outright_rates[product]
            spread_margin = abs(futures[product]['min_positions']) * prc * self.ptvals[product] * self.spread_rates[product]
            if product == options['product']:
                for i in xrange(self.num_scenarios):
                    margin = -self.futures[product]['net_positions'] * outright_factor * (i-divisor)/divisor + spread_margin
                    for j in xrange(2):
                        self.buckets[2*i+j] += margin
            else:
                self.scenarios[product] = abs(futures[product]['net_positions']) * outright_factor + spread_margin
                for j in xrange(len(self.buckets)):
                    self.buckets[j] += self.scenarios[product]
        for strid in contracts:
            id = int(strid)
            pos = p[id]['inventory']
            if pos == 0:
                continue
            prc = md[id]
            oxtr = contracts[id]
            newvol = oxtr.implied_vol(u_base, prc, vols.get(id) or 1.0)
            newvol = 0.03 if newvol < 0 else newvol
            vols[id] = max(0.01 * (np.floor(100*newvol - 0.5) if pos > 0 else np.ceil(100*newvol + 0.5)), 0.03)
        for i in xrange(len(self.buckets)):
            u = u_base * (1 + opt_rate * (np.floor(i/2)-divisor)/divisor)
            for strid in contracts:
                id = int(strid)
                pos = p[id]['inventory']
                if pos == 0:
                    continue
                vol = shock[i%2] * vols[id]
                prc = md[id]
                oxtr = contracts[id]
                qty_val = pos * opt_ptval
                premium = qty_val * prc
                if i == 0:
                    option_premium -= premium
                oxtr.calculate_greeks(u, vol)
                margin = premium - qty_val * oxtr.tv
                option_adjustment = -np.inf
                if i == 60 and oxtr.op > 0 and pos < 0:
                    oxtr.calculate_greeks(u_base * (1 + opt_rate * 2), vol)
                    option_adjustment = oa_factor * (qty_val * oxtr.tv - premium)
                elif i == 1 and oxtr.op > 0 and pos > 0:
                    oxtr.calculate_greeks(u_base * (1 - opt_rate * 2), vol)
                    option_adjustment = oa_factor * (premium - qty_val * oxtr.tv)
                elif i == 0 and oxtr.op < 0 and pos < 0:
                    oxtr.calculate_greeks(u_base * (1 - opt_rate * 2), vol)
                    option_adjustment = oa_factor * (qty_val * oxtr.tv - premium)
                elif i == 61 and oxtr.op < 0 and pos > 0:
                    oxtr.calculate_greeks(u_base * (1 + opt_rate * 2), vol)
                    option_adjustment = oa_factor * (premium - qty_val * oxtr.tv)
                max_margin = max(margin, option_adjustment)
                self.buckets[i] += max_margin
        self.margin = np.max(self.buckets)
        self.scenario_number = np.where(self.buckets==self.margin)[0][0] + 1
        self.margin += option_premium
        self.margin = prettify(self.margin)

def start_process():
    print 'starting'
    # insert subprocess
    return 0

def stop_process():
    print 'stopping'
    # insert subprocess
    return 0    

monitor_names = ['margin', 'daily_pnl', 'monthly_pnl', 'etf_notional']

decider = Decider(start_process, stop_process)

for name, details in monitors.items():
    decider.add_monitor(name, details['limit'], details['func'])
        
for i in range(2, 2+len(monitor_names)):
    if len(sys.argv) >= i+1:
        name = monitor_names[i-2]
        limit = np.float64(sys.argv[i])
        print 'loading', name, 'limit', limit
        decider.set_limit(name, limit)

host = ':'.join([ip, port])

print 'connecting to', host
ws = create_connection(host)
print 'websocket connection is open. ready to compute margin'

# create margin object
margin = None
datastore = None

try:
    while True:
        msg = ws.recv()
        if msg == '1':
            continue
        data = json.loads(msg)
        if data['msg'] == 'defs_catchup':
            del data['msg']
            datastore = DataStore(data)
            print 'defs caught up'
        elif data['msg'] == 'defs':
            del data['msg']
            datastore.add_contract_def(data)
        elif data['msg'] == 'fill_catchup':
            del data['msg']
            datastore.catchup_fills(data)
            print 'fills caught up'
        elif data['msg'] == 'fill':
            datastore.add_fill(data)
            if margin:
                margin.compute_scenarios()
                decider.check_value('margin', margin.margin)
        elif data['msg'] == 'md_catchup':
            del data['msg']
            datastore.catchup_md(data)
            print 'md caught up'
        elif data['msg'] == 'md':
            datastore.update_md(data)
            if margin:
                margin.sync_md()
                decider.check_value('margin', margin.margin)
        elif data['msg'] == 'vols':
            del data['msg']
            datastore.update_vols(data)
            if margin:
                margin.sync_md()
                decider.check_value('margin', margin.margin)
        elif data['msg'] == 'margin':
            margin = Margin(data, datastore)
            margin.compute_scenarios()
            decider.check_value('margin', margin.margin)
finally:
    print 'closing websocket connection'
    ws.close()
