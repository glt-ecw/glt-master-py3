import zmq
import simplejson as json
import threading

from time import sleep

deliver_addr = 'tcp://*:7777'
#receive_addr = 'tcp://10.130.33.102:7778'
receive_addr = 'tcp://10.130.33.103:7777'

context = zmq.Context()

# publish to heimdall ipc
publisher = context.socket(zmq.PUB)
publisher.bind('ipc://rainbow_bridge.ipc')

# receive and deliver
receive = context.socket(zmq.SUB)
receive.connect(receive_addr)
receive.setsockopt(zmq.SUBSCRIBE, '')

deliver = context.socket(zmq.PUB)
deliver.bind(deliver_addr)

# listen to drone.ipc
drone = context.socket(zmq.SUB)
drone.connect('ipc://asgard.ipc')
drone.setsockopt(zmq.SUBSCRIBE, '')

dont_send = set()
dont_need = set()

#def object_hook(dct):
#    output = dict()
#    for k, v in dct.items():
#        if type(v) == type(u""):
#           output[k] = str(v)
#        else:
#           output[k] = v
#    return output

def drone_proc():
    while 1:
        json_msg = drone.recv()
        msg = json.loads(json_msg) #, object_hook=object_hook)
        if msg.get('heimdall'):
            continue
        if msg.get('msg'):
            if msg['msg'] == 'md':
                id = msg['securityid']
                if id not in dont_send:
                    deliver.send(json_msg)
                if id not in dont_need:
                    dont_need.add(id)
            elif msg['msg'] == 'fill':
                deliver.send(json_msg)

proc = threading.Thread(target=drone_proc)
proc.start()

print('heimdall sees all')
try:
    while 1:
        json_msg = receive.recv()
        msg = json.loads(json_msg) #, object_hook=object_hook)
        if msg.get('msg'):
            if msg['msg'] == 'ignore':
                id = msg['securityid']
                dont_send.add(id)
            elif msg['msg'] == 'md':
                id = msg['securityid']
                if id in dont_need:
                    resp = {'msg': 'ignore', 'securityid': id}
                    json_resp = json.dumps(resp)
                    deliver.send(json_resp)
                else:
                    msg['heimdall'] = 1
                    publisher.send(json.dumps(msg))
            elif msg['msg'] == 'fill':
                msg['heimdall'] = 1
                publisher.send(json.dumps(msg))
finally:
    context.destroy()
    proc.join()

