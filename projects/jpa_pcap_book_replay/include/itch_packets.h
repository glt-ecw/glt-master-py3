/*
 * Copyright (c) 2016 <copyright holder> <email>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 */

#ifndef ITCH_PACKETS_H
#define ITCH_PACKETS_H

#include <iostream>
#include <iomanip>
#include <cstdint>
#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <deque>
#include "md_constants.h"
#include "pcap_tools.h"


using std::string;
using std::cout;
using std::endl;
using std::vector;
using std::unordered_map;
using std::unordered_set;
using std::deque;
using std::stoull;


const size_t ITCH_SESSION_IDX = 0;
const size_t ITCH_SEQNUM_IDX = 10;
const size_t ITCH_MESSAGE_COUNT_IDX = 18;
// T
const size_t MSG_T_SECONDS = 1;
// R
const size_t MSG_R_BOOK_ID = 5;
const size_t MSG_R_SYMBOL = 9;
// O
const size_t MSG_O_NANOS = 1;
const size_t MSG_O_BOOK_ID = 5;
const size_t MSG_O_STATE = 9;
const size_t MSG_O_STATE_SIZE = 20;
// A
const size_t MSG_A_NANOS = 1;
const size_t MSG_A_ORDER_ID = 5;
const size_t MSG_A_BOOK_ID = 13;
const size_t MSG_A_SIDE = 17;
const size_t MSG_A_POSITION = 18;
const size_t MSG_A_QUANTITY = 22;
const size_t MSG_A_PRICE = 30;
// E
const size_t MSG_E_NANOS = 1;
const size_t MSG_E_ORDER_ID = 5;
const size_t MSG_E_BOOK_ID = 13;
const size_t MSG_E_SIDE = 17;
const size_t MSG_E_EXECUTED_QUANTITY = 18;
const size_t MSG_E_MATCH_ID = 26;
const size_t MSG_E_COMBO_GROUP_ID = 34;
// C
const size_t MSG_C_NANOS = 1;
const size_t MSG_C_ORDER_ID = 5;
const size_t MSG_C_BOOK_ID = 13;
const size_t MSG_C_SIDE = 17;
const size_t MSG_C_EXECUTED_QUANTITY = 18;
const size_t MSG_C_MATCH_ID = 26;
const size_t MSG_C_COMBO_GROUP_ID = 34;
const size_t MSG_C_PRICE = 52;
const size_t MSG_C_CROSS = 56;
// D
const size_t MSG_D_NANOS = 1;
const size_t MSG_D_ORDER_ID = 5;
const size_t MSG_D_BOOK_ID = 13;
const size_t MSG_D_SIDE = 17;
// Z
const size_t MSG_Z_BOOK_ID = 5;
// SNAPSHOT
const uint16_t SNAPSHOT_SEQNUM_SIZE = 20;


enum class PacketStatus {
    Good = 1,
    NoPayload,
    SequenceBreak,
    CatchingUp,
    Catastrophy
};

enum class ItchProcessingStatus {
    Good = 1,
    EnqueueMessages,
    Catastrophy
};


using BookSymbolMap = unordered_map<uint32_t, string>;
using ContractsOpen = unordered_set<uint32_t>;


uint16_t WhichSide(char);


struct ItchMarketDataMessage {
    char tag{'\0'};
    uint16_t side{0};
    uint32_t book_id{0}, exch_nanos{0};
    uint64_t order_id{0}, seqnum{0}, qty{0};
    int32_t price{0};
};

using ItchMessageQueue = deque<ItchMarketDataMessage>;
//using ItchMessageQueueMap = unordered_map<uint32_t, ItchMessageQueue>;


struct ItchOrderBootstrap {
    uint64_t exch_sec, order_id{0};
    uint32_t book_id{0}, position{0};
    uint16_t side{0};
    int32_t price{0};
    uint64_t qty{0};
};


class ItchMulticastPacket {
public:
    PacketStatus pkt_state;
    ItchProcessingStatus proc_state;
    bool corrupted, from_snapshot, end_of_packet, ignore = false;
    char tag;
    char state[MSG_O_STATE_SIZE];
    int32_t price;
    string symbol;
    uint16_t side, num_messages, count;
    uint32_t book_id, exch_nanos, payload_len;
    uint64_t pkt_seqnum, msg_seqnum, exch_ts, pkt_ts, order_id, qty, channel, position;
    ItchMessageQueue messages;
    ItchMulticastPacket();
    void Reset();
    void AllowProcessing();
    void OnlyEnqueue();
    PacketStatus PrepareDecoding(uint64_t, const NetworkPacket&);
    bool ReadMessage();
    void ReadFromBootstrap(uint64_t, uint64_t, const ItchOrderBootstrap&);
    void Enqueue();
    bool ReadMessageFromQueue();
private:
    const uint8_t *data;
    uint64_t exch_sec;
};

const uint16_t SNAPSHOT_TAG = 3;


class ItchSnapshot {
public:
    bool completed, reset_books;
    uint64_t seqnum, exch_ts;
    BookSymbolMap book_symbols;
    ContractsOpen contracts_open;
    vector<ItchOrderBootstrap> orders;
    ItchSnapshot();
    ~ItchSnapshot();
    void Reset();
    void ReadData(const NetworkPacket&);
    uint16_t GetNumRemainingBytes() const;
private:
    uint8_t *remaining_bytes;
    uint16_t num_remaining_bytes;
    uint64_t exch_sec;
};

#endif // ITCH_PACKETS_H
