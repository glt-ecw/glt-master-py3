#ifndef ITCHBOOKBUILDER_H
#define ITCHBOOKBUILDER_H

#include <algorithm>
#include <list>
#include <unordered_map>
#include <vector>
#include "book_building.h"
#include "itch_packets.h"


const int8_t ITCH_PROCESS_MESSAGE_SUCCESS = 0;
const int8_t ITCH_PROCESS_MESSAGE_BAD_SEQNUM = -1;
const int8_t ITCH_PROCESS_MESSAGE_CORRUPTED = 1;

const uint16_t MAGIC_NUM_PARTICIPANTS = 26;
const uint16_t TOPIC_TYPE_MAGIC = TOPIC_TYPE_MD + 1;

const uint16_t ITCH_TOP_LEVEL_NO_CHANGE = 0;
const uint16_t ITCH_TOP_LEVEL_QTY_CHANGE = 1;
const uint16_t ITCH_TOP_LEVEL_PRICE_IMPROVED = 2;
const uint16_t ITCH_TOP_LEVEL_PRICE_WORSENED = 3;


struct IndividualOrder {
    int32_t price, qty;
    uint16_t side;
    uint32_t position{0};
    uint64_t id;
};


using OrderMap = std::unordered_map<uint64_t, IndividualOrder>;
using OrderList = std::list<uint64_t>;
using OrderListMap = std::unordered_map<int32_t, OrderList>;
using PendingOrderVec = std::vector<IndividualOrder>;
using PendingOrderVecMap = std::unordered_map<int32_t, PendingOrderVec>;


class ItchBookBuilder : public BookBuilder {
public:
    bool market_opening, market_open, reading_snapshot, displayed_pending;
    char msg_type;
    int32_t seq_no, magic_bidqty, magic_askqty, top_bidprc, top_askprc, top_bidqty, top_askqty;
    uint16_t top_bid_change, top_ask_change;
    OrderMap bid_orders, ask_orders;
    OrderListMap bid_order_list_map, ask_order_list_map;
    PendingOrderVecMap bid_pending_orders, ask_pending_orders;
    ItchBookBuilder();
    ~ItchBookBuilder();
    void Reset();
    bool IsMarketOpening() const;
    void MarketOpening();
    bool IsMarketOpen() const;
    void MarketOpen();
    bool IsMarketClosed() const;
    void MarketClose();
    void ProcessDelete(OrderMap&, OrderListMap&, uint64_t);
    int32_t ProcessTrade(OrderMap&, OrderListMap&, uint64_t, int32_t);
    int8_t ProcessMessage(const ItchMulticastPacket&);
    void ComputeTopLevel();
};

#endif // ITCHBOOKBUILDER_H
