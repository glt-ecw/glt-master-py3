/*
 * Copyright (c) 2016 <copyright holder> <email>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 */

#ifndef CONFIG_HELPER_H
#define CONFIG_HELPER_H

#include <cstdlib>
#include <vector>
#include <unordered_map>
#include <string>
#include "pcap_tools.h"

using std::string;
using std::vector;
using std::unordered_map;


// arguments
const string ARG_CFG = "cfg";
const string ARG_PCAP_FILENAME = "pcap-filename";
const string ARG_DEFS = "defs";
const string ARG_CONTRACTS = "contracts";
//const string ARG_START_TIME = "start-time";
//const string ARG_END_TIME = "end-time";
//const string ARG_SNAPSHOT_END_TIME = "snapshot-end-time";
//const string ARG_TZ_OFFSET = "tz-offset";
const string ARG_CHANNELS_CFG = "channels-cfg";
const string ARG_SNAPSHOT_CFG = "snapshot-cfg";
const string ARG_DAT_FILENAME_ID = "dat-filename-id";
const string ARG_SNAPSHOT_SRC_IP = "snapshot-src-ip";
const string ARG_OUTPUT_DAT_FILEPATH = "dat-filepath";
const string ARG_OUTPUT_CSV_FILENAME = "csv-filename";


using IPPortVector = vector<uint64_t>;
using ArgMap = unordered_map<string, string>;
using DeioDefsMap = unordered_map<string, uint64_t>;
using SecurityIdMap = unordered_map<uint32_t, uint64_t>;
using BookIdMap = unordered_map<uint64_t, uint32_t>;


uint64_t FindIntIPPort(const string&, uint16_t);
void ReadConfiguration(const string&, ArgMap&);
int DumpIntoArgMap(int, char**, ArgMap&);
void DumpIntoDeioDefsMap(const string&, DeioDefsMap&);
void DumpIntoPortMap(const string&, IPPortVector&);


#endif // CONFIG_HELPER_H
