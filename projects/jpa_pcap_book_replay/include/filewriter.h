/*
 * Copyright (c) 2016 <copyright holder> <email>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 */

#ifndef FILEWRITER_H
#define FILEWRITER_H

#include <cstdint>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <cstdint>
#include <fstream>


using std::string;
using std::endl;
using std::cout;
using std::setw;
using std::setfill;
using std::stringstream;


class OutputMDBinaryFile {
protected:
    std::ofstream file_;
    uint16_t count_;
    uint64_t filesize_, size_limit_;
    string path_, identifier_, ext_;
    bool writeable_;
    void CreateNew();
public:
    string current_filename;
    OutputMDBinaryFile();
    ~OutputMDBinaryFile();
    void Close();
    void Init(const string&, const string&, const string&, uint64_t);
    void Write(char*, size_t);
    bool Writeable();
};


class OutputMDAsciiFile {
protected:
    std::ofstream file_;
    bool writeable_;
public:
    string filename;
    OutputMDAsciiFile();
    ~OutputMDAsciiFile();
    void Close();
    void Init(const string&);
    void Write(const string&);
    void Write(const char*);
    bool Writeable();
};

#endif // FILEWRITER_H
