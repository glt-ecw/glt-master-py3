#ifndef ITCHBOOKBUILDER_H
#define ITCHBOOKBUILDER_H

#include <vector>
#include <ostream>
#include <unordered_map>
#include "marketdatainput.h"


struct BookLevel {
    int price, qty, participants, side;    
};


typedef std::vector<BookLevel> BookVector;
typedef BookVector::iterator BookVectorIter;
typedef BookVector::reverse_iterator BookVectorRIter;
typedef std::unordered_map<int, BookLevel> OrderMap;
typedef OrderMap::iterator OrderMapIter;


class BookDepth {
public:
    int side_, depth_;
    BookVector levels;
    BookDepth(int, int);
    BookDepth();
    ~BookDepth();
    void Init(int, int);
    void Add(int, int);
    void Remove(int, int, bool);
    void Sort();
    void Clear();
};


class ItchBookBuilder {
public:
    int md_messages, seqnum, seq_no, pkt_len, msg_count;
    int buyprice, buyvolume, sellprice, sellvolume;
    std::string time, exch_ts;
    char msg_type;
    BookDepth bids, asks;
    OrderMap orders;
    bool market_opening, market_open, corrupted;
    char last_msg_type;
    ItchBookBuilder(int);
    ItchBookBuilder();
    ~ItchBookBuilder();
    void Init(int);
    void ProcessDelete(int);
    int ProcessTrade(int, int, bool);
    void ProcessMessage(const MarketDataInput&);
    void PrintColumns();
};

std::ostream& operator<<(std::ostream&, const BookDepth&);
std::ostream& operator<<(std::ostream&, const ItchBookBuilder&);

#endif // ITCHBOOKBUILDER_H
