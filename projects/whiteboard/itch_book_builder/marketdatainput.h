/*
 * Copyright (c) 2016 <copyright holder> <email>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 */

#ifndef MARKETDATAINPUT_H
#define MARKETDATAINPUT_H

#include <string>

const int MD_SEQNUM = 0;
const int MD_TIME = 1;
const int MD_PKT_LEN = 2;
const int MD_SESS_ID = 3;
const int MD_SEQ_NO = 4;
const int MD_MSG_CNT = 5;
const int MD_MSG_LEN = 6;
const int MD_MSG_TYPE = 7;
const int MD_ORDER_ID = 8;
const int MD_BOOK_ID = 9;
const int MD_SIDE = 10;
const int MD_POS = 11;
const int MD_QTY = 12;
const int MD_PRICE = 13;
const int MD_MATCHID = 14;
const int MD_COMBO_GROUP_ID = 15;
const int MD_EQ_BQTY = 16;
const int MD_EQ_AQTY = 17;
const int MD_EQ_PRC = 18;
const int MD_MSG = 19;
const int MD_EXCH_TS = 20;
const int MD_OID = 21;
const int MD_DT = 22;
const int MD_IDX = 23;


class MarketDataInput {
public:
    /*
     * columns in csv: seqnum,time,pkt_len,sess_id,seq_no,msg_cnt,msg_len,
     *                 msg_type,order_id,book_id,side,pos,qty,price,matchid,combo_group_id,
     *                 eq_bqty,eq_aqty,eq_prc,msg,exch_ts,oid,dt,idx
     */
    int seqnum, pkt_len, sess_id, seq_no, msg_cnt, msg_len, side;
    int book_id, pos, qty, price, combo_group_id, eq_bqty, eq_aqty, eq_prc, msg;
    int oid, idx;
    double dt;
    unsigned long long matchid;
    std::string time, order_id, exch_ts;
    char msg_type;
    MarketDataInput();
    ~MarketDataInput();
    void Tokenize(const std::string&);
};

#endif // MARKETDATAINPUT_H
