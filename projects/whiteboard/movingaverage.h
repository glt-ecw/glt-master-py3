/*
 * Copyright (c) 2016 <copyright holder> <email>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 */

#ifndef MOVINGAVERAGE_H
#define MOVINGAVERAGE_H
#include <deque>

struct DataNode {
    long long ts;
    double value;
};

class MovingAverage {
  long long time_window_;
  std::deque<DataNode> data;
  double sum_values_;
public:
  double average;
  MovingAverage(long long);
  MovingAverage(const MovingAverage& other);
  ~MovingAverage();
  MovingAverage& operator=(const MovingAverage& other);
  bool operator==(const MovingAverage& other) const;
  void Init(long long time_window);
  void PushValue(long long ts, double value);
  void PopValues(long long ts);
};

#endif // MOVINGAVERAGE_H
