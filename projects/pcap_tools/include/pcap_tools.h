/*
 * Copyright (c) 2016 <copyright holder> <email>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef PCAP_TOOLS_H
#define PCAP_TOOLS_H

#include <cstdint>
#include <string>
#include <cstring>
#include <pcap.h>

using PacketHeader = pcap_pkthdr;

const int32_t HEADER_SIZE = 24;
const int64_t ONE_SEC = 1000000000;

const uint16_t UDP = 17;
const uint16_t TCP = 6;

const size_t ETHERNET_HEADER_SIZE = 14;
const size_t IPV4_HEADER_SIZE = 20;
//const size_t IPV6_HEADER_SIZE = 40;

// internet layer constants
const size_t INTERNET_VERSION_IDX = 14;
const size_t INTERNET_TOTAL_LENGTH_IDX = 16;
const size_t INTERNET_PROTOCOL_IDX = 23;
const size_t INTERNET_SRC_IP_IDX = 26;
const size_t INTERNET_DST_IP_IDX = 30;
// transport layer constants
const size_t TRANSPORT_SRC_PORT_IDX = 34;
const size_t TRANSPORT_DST_PORT_IDX = 36;
// udp transport
const size_t TRANSPORT_UDP_PKT_LEN_IDX = 38;
const size_t TRANSPORT_UDP_CHECKSUM_IDX = 40;
const size_t TRANSPORT_UDP_DATA_IDX = 42;
// tcp transport
const size_t TRANSPORT_TCP_SEQ_NUM_IDX = 38;
const size_t TRANSPORT_TCP_ACK_NUM_IDX = 42;
const size_t TRANSPORT_TCP_DATA_OFFSET_IDX = 46;
const size_t TRANSPORT_TCP_WINDOW_SIZE_IDX = 48;
const size_t TRANSPORT_TCP_CHECKSUM_IDX = 50;
const size_t TRANSPORT_TCP_URGENT_POINTER_IDX = 52;
const size_t TRANSPORT_TCP_DATA_IDX = 54;


using std::string;

string FormatIPBytes(const uint8_t*, size_t);
string FormatIntIPPort(uint64_t ip_port);
bool AreIPsEqual(uint64_t ip_port_a, uint64_t ip_port_b);
bool ArePortsEqual(uint64_t ip_port_a, uint64_t ip_port_b);


template <typename T>
T CopyBigEndian(const uint8_t *bytes, size_t start) {
    size_t num_bytes = sizeof(T);
    char buf[num_bytes];
    for (size_t i=0; i<num_bytes; ++i) {
        buf[num_bytes - 1 - i] = bytes[start + i];
    }
    T value;
    std::memcpy(&value, buf, num_bytes);
    return value;
}


class InternetLayer {
public:
    uint16_t version, protocol, total_length;
    uint64_t isrc_ip, idst_ip;
    InternetLayer();
    void Reset();
    void DecodeBytes(const uint8_t*);
};


class TransportLayer {
public:
    uint16_t src_port, dst_port, pkt_len, payload_len, payload_index, tcp_data_offset;
    uint32_t seq_num, ack_num;
    uint8_t reserved;
    bool ns, cwr, ece, urg, ack, psh, rst, syn, fin;
    const uint8_t *payload;
    TransportLayer();
    ~TransportLayer();
    void Reset();
    void DecodeBytes(const uint8_t*, uint16_t version, uint16_t protocol, uint16_t total_length);
};


class NetworkPacket : public InternetLayer, public TransportLayer {
public:
    uint64_t src_ip_port, dst_ip_port;
    NetworkPacket();
    ~NetworkPacket();
    void DecodeBytes(const uint8_t*);
    uint8_t PeekAt(uint16_t);
};


class PcapReader {
public:
    pcap_t *pcap;
    char *errbuff;
    int64_t rvalue, count, datalen;
    PacketHeader *header;
    const uint8_t *data;
    int64_t ts;
    NetworkPacket packet;
    PcapReader(const string&);
    ~PcapReader();
    bool Next();
};


#endif // PCAP_TOOLS_H
