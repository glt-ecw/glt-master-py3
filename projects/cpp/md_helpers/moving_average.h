#ifndef MOVING_AVERAGE_H
#define MOVING_AVERAGE_H

//#include <cmath>
#include <unordered_map>
#include "accumulator.h"


class MovingAverageWithEntropy : public TimeWindowAccumulator<double, double>
{
    double half_life_ = std::numeric_limits<double>::quiet_NaN();
public:
    MovingAverageWithEntropy()=default;
    ~MovingAverageWithEntropy()=default;
    
    void Init(uint64_t t_time_window, double t_half_life)
    {
        half_life_ = t_half_life;
        TimeWindowAccumulator<double, double>::Init(t_time_window);
        value_ = std::numeric_limits<double>::quiet_NaN();
    }
    
    void AddData(uint64_t time, double value) {
        TimeWindowAccumulator<double, double>::AddData(time, value);
    }
    
    double Value() const
    {
        if (!initialized_ || Size() == 0) {
            return std::numeric_limits<double>::quiet_NaN();
        }
        uint64_t time = LastEventTime();
        double sum_value = 0, sum_w = 0;
        for (const auto &dat : data_.AsQueue()) {
            double time_diff = double(time - dat.time);
            double w = std::exp(-time_diff / half_life_);
            sum_value += w * dat.value;
            sum_w += w;
        }
        return sum_value / sum_w;
    }
    
    void Clear()
    {
        TimeWindowAccumulator<double, double>::Clear(std::numeric_limits<double>::quiet_NaN());
    }
};


template <typename KeyType>
class MAWEManager
{
    double half_life;
    uint64_t time_window;
    std::unordered_map<KeyType, MovingAverageWithEntropy> things_;
public:
    MAWEManager()=default;
    ~MAWEManager()=default;
    void Init(uint64_t t_time_window, double t_half_life)
    {
        time_window = t_time_window;
        half_life = t_half_life;
    }
    
    bool Has(KeyType key) const
    {
        return things_.find(key) != things_.end();
    }
    
    void AddData(KeyType key, uint64_t time, double value)
    {
        if (!Has(key)) {
            things_[key].Init(time_window, half_life);
        }
        things_[key].AddData(time, value);
    }
    
    double Value(KeyType key) const
    {
        if (!Has(key)) {
            return std::numeric_limits<double>::quiet_NaN();
        }
        return things_.at(key).Value();
    }
    
    void Clear()
    {
        for (auto &thing : things_) {
            thing.second.Clear();
        }
    }
};

using MAWEManagerINT64 = MAWEManager<int64_t>;
using MAWEManagerINT32 = MAWEManager<int32_t>;
using MAWEManagerINT16 = MAWEManager<int16_t>;
using MAWEManagerUINT64 = MAWEManager<uint64_t>;
using MAWEManagerUINT32 = MAWEManager<uint32_t>;
using MAWEManagerUINT16 = MAWEManager<uint16_t>;

/*
struct TradeEvent {
    uint64_t time;
    uint16_t side;
    int32_t price;
    int32_t qty;
};


using TradeNode = DataEvent<int32_t>;


template <class AccQtyType>
class VolumeAccumulatorTemplate : public TimeWindowAccumulator<int32_t, AccQtyType>
{
public:
    AccQtyType AddTrade(uint64_t time, int32_t qty) {
        return TimeWindowAccumulator<int32_t, AccQtyType>::AddData(time, qty);
    }
    
    void RemoveTrades(uint64_t time) {
        TimeWindowAccumulator<int32_t, AccQtyType>::RemoveData(time);
    }
    
    AccQtyType Qty() const {
        return TimeWindowAccumulator<int32_t, AccQtyType>::value_;
    }
};

using VolumeAccumulator = VolumeAccumulatorTemplate<int32_t>;
using DoubleVolumeAccumulator = VolumeAccumulatorTemplate<double>;
*/

#endif // MOVING_AVERAGE_H
