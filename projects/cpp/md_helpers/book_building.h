/*
 * Copyright (c) 2016 <copyright holder> <email>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 */

#ifndef BOOK_BUILDING_H
#define BOOK_BUILDING_H

#include <cstdio>
#include <limits>
#include <vector>
#include <string>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <sstream>
#include <ostream>
#include <algorithm>
#include <unordered_map>
#include "md_constants.h"
#include <iostream>

using std::cout;
using std::endl;
using std::sprintf;
using std::string;
using std::memcpy;
using std::remove_if;
using std::find_if;
using std::sort;
using std::numeric_limits;
using std::stringstream;
using std::ostringstream;
using std::ostream;
using std::vector;


// book depth of MDOrderBook
const uint16_t MAX_DEPTH = 5;

const uint16_t TOPIC_SIZE = 16;
const uint16_t TOPIC_TYPE_MD = 2;
const uint16_t TOPIC_TYPE_TRADE = 5;

const uint16_t MD_ORDER_BOOK_BYTE_ARRAY_PACKET_LEN = 0;
const uint16_t MD_ORDER_BOOK_BYTE_ARRAY_EOP = 2;
const uint16_t MD_ORDER_BOOK_BYTE_ARRAY_CHANNEL = 4;
const uint16_t MD_ORDER_BOOK_BYTE_ARRAY_MSG_IDX = 12;
const uint16_t MD_ORDER_BOOK_BYTE_ARRAY_TOPIC = 14;
const uint16_t MD_ORDER_BOOK_BYTE_ARRAY_SECID = 30;
const uint16_t MD_ORDER_BOOK_BYTE_ARRAY_SEQNUM = 38;
const uint16_t MD_ORDER_BOOK_BYTE_ARRAY_TIME = 46;
const uint16_t MD_ORDER_BOOK_BYTE_ARRAY_EXCH_TIME = 54;
const uint16_t MD_ORDER_BOOK_BYTE_ARRAY_BUYPRICE = 62;
const uint16_t MD_ORDER_BOOK_BYTE_ARRAY_BUYVOLUME = 66;
const uint16_t MD_ORDER_BOOK_BYTE_ARRAY_SELLPRICE = 70;
const uint16_t MD_ORDER_BOOK_BYTE_ARRAY_SELLVOLUME = 74;
const uint16_t MD_ORDER_BOOK_BYTE_ARRAY_LEVELS = 78;

const uint16_t MD_ORDER_BOOK_BASE_STRUCT_SIZE = MD_ORDER_BOOK_BYTE_ARRAY_LEVELS;

// DEPRECATED (old JPX parsed output)
const uint16_t MD_ORDER_BOOK_CSV_PACKET_LEN = 0;
const uint16_t MD_ORDER_BOOK_CSV_EOP = 1;
const uint16_t MD_ORDER_BOOK_CSV_CHANNEL = 2;
const uint16_t MD_ORDER_BOOK_CSV_MSG_IDX = 3;
const uint16_t MD_ORDER_BOOK_CSV_TOPIC = 4;
const uint16_t MD_ORDER_BOOK_CSV_SECID = 5;
const uint16_t MD_ORDER_BOOK_CSV_SEQNUM = 6;
const uint16_t MD_ORDER_BOOK_CSV_TIME = 7;
const uint16_t MD_ORDER_BOOK_CSV_EXCH_TIME = 8;
const uint16_t MD_ORDER_BOOK_CSV_BUYPRICE = 9;
const uint16_t MD_ORDER_BOOK_CSV_BUYVOLUME = 10;
const uint16_t MD_ORDER_BOOK_CSV_SELLPRICE = 11;
const uint16_t MD_ORDER_BOOK_CSV_SELLVOLUME = 12;
const uint16_t MD_ORDER_BOOK_CSV_LEVELS_START = 13;

// BRUCE'S KRX PARSING
// #pktNum,ts,tsInt,exchTsInt,channel,seqNum,msgType,symbol,securityId,mdStatus,
// exchMdStatus,tradePrice,tradeQty,tradeAggressorSide,totalVolumeTraded,totalBidQty,totalOfferQty,
// totalBidNumPart,totalOfferNumPart,indicPrice,bPx0,bQty0,bNP0,bPx1,bQty1,bNP1,bPx2,bQty2,bNP2,
// bPx3,bQty3,bNP3,bPx4,bQty4,bNP4,oPx0,oQty0,oNP0,oPx1,oQty1,oNP1,oPx2,oQty2,oNP2,oPx3,oQty3,oNP3,
// oPx4,oQty4,oNP4
const uint16_t BRUCE_KRX_CSV_PACKET_NUM = 0;
const uint16_t BRUCE_KRX_CSV_TIME_STRING = 1;
const uint16_t BRUCE_KRX_CSV_INT_TIME = 2;
const uint16_t BRUCE_KRX_CSV_INT_EXCH_TIME = 3;
const uint16_t BRUCE_KRX_CSV_CHANNEL = 4;
const uint16_t BRUCE_KRX_CSV_SEQNUM = 5;
const uint16_t BRUCE_KRX_CSV_MSG = 6;
const uint16_t BRUCE_KRX_CSV_SYMBOL = 7;
const uint16_t BRUCE_KRX_CSV_SECID = 8;
const uint16_t BRUCE_KRX_CSV_MD_STATUS = 9;
const uint16_t BRUCE_KRX_CSV_EXCH_MD_STATUS = 10;
const uint16_t BRUCE_KRX_CSV_TRADE_PRICE = 11;
const uint16_t BRUCE_KRX_CSV_TRADE_QTY = 12;
const uint16_t BRUCE_KRX_CSV_TRADE_SIDE = 13;
const uint16_t BRUCE_KRX_CSV_TOTAL_VOLUME_TRADED = 14;
const uint16_t BRUCE_KRX_CSV_TOTAL_BID_QUANTITY = 15;
const uint16_t BRUCE_KRX_CSV_TOTAL_ASK_QUANTITY = 16;
const uint16_t BRUCE_KRX_CSV_TOTAL_BID_PARTICIPANTS = 17;
const uint16_t BRUCE_KRX_CSV_TOTAL_ASK_PARTICIPANTS = 18;
const uint16_t BRUCE_KRX_CSV_INDICATIVE_PRICE = 19;
const uint16_t BRUCE_KRX_CSV_ORDER_BOOK_START = 20;

const uint16_t BRUCE_KRX_CSV_ORDER_BOOK_DEPTH = 5;

// BRUCE'S JPX PARSING
// #pktNum,ts,tsInt,exchTsInt,channel,seqNum,msgType,symbol,securityId,mdStatus,
// exchMdStatus,tradePrice,tradeQty,tradeAggressorSide,totalVolumeTraded,totalBidQty,totalOfferQty,
// totalBidNumPart,totalOfferNumPart,indicPrice,bPx0,bQty0,bNP0,bPx1,bQty1,bNP1,bPx2,bQty2,bNP2,
// bPx3,bQty3,bNP3,bPx4,bQty4,bNP4,oPx0,oQty0,oNP0,oPx1,oQty1,oNP1,oPx2,oQty2,oNP2,oPx3,oQty3,oNP3,
// oPx4,oQty4,oNP4,bidAvolQty,offerAvolQty,pktLen,endOfPacket
const uint16_t BRUCE_JPX_CSV_PACKET_NUM = 0;
const uint16_t BRUCE_JPX_CSV_TIME_STRING = 1;
const uint16_t BRUCE_JPX_CSV_INT_TIME = 2;
const uint16_t BRUCE_JPX_CSV_INT_EXCH_TIME = 3;
const uint16_t BRUCE_JPX_CSV_CHANNEL = 4;
const uint16_t BRUCE_JPX_CSV_SEQNUM = 5;
const uint16_t BRUCE_JPX_CSV_MSG = 6;
const uint16_t BRUCE_JPX_CSV_SYMBOL = 7;
const uint16_t BRUCE_JPX_CSV_SECID = 8;
const uint16_t BRUCE_JPX_CSV_MD_STATUS = 9;
const uint16_t BRUCE_JPX_CSV_EXCH_MD_STATUS = 10;
const uint16_t BRUCE_JPX_CSV_TRADE_PRICE = 11;
const uint16_t BRUCE_JPX_CSV_TRADE_QTY = 12;
const uint16_t BRUCE_JPX_CSV_TRADE_SIDE = 13;
const uint16_t BRUCE_JPX_CSV_TOTAL_VOLUME_TRADED = 14;
const uint16_t BRUCE_JPX_CSV_TOTAL_BID_QUANTITY = 15;
const uint16_t BRUCE_JPX_CSV_TOTAL_ASK_QUANTITY = 16;
const uint16_t BRUCE_JPX_CSV_TOTAL_BID_PARTICIPANTS = 17;
const uint16_t BRUCE_JPX_CSV_TOTAL_ASK_PARTICIPANTS = 18;
const uint16_t BRUCE_JPX_CSV_INDICATIVE_PRICE = 19;
const uint16_t BRUCE_JPX_CSV_ORDER_BOOK_START = 20;
const uint16_t BRUCE_JPX_CSV_BID_AVOL_QTY = 50;
const uint16_t BRUCE_JPX_CSV_ASK_AVOL_QTY = 51;
const uint64_t BRUCE_JPX_CSV_PACKET_LEN = 52;
const uint64_t BRUCE_JPX_CSV_END_OF_PACKET = 53;


const uint16_t BRUCE_JPX_CSV_ORDER_BOOK_DEPTH = 5;


enum class ChannelType {
    None,
    Integer,
    StringIP
};


void PrintMDOrderBookColumns(uint16_t, ostream&);
string PrintMDOrderBookColumns(uint16_t);


struct BookLevel {
    int32_t price = 0, qty = 0, participants = 0;
};


using BookVector = vector<BookLevel>;


class MDOrderBook {
private:
    uint16_t depth_, byte_array_bid_idx_, byte_array_ask_idx_;
public:
    uint16_t packet_len, eop, byte_array_len, msg_idx;
    vector<char> topic;
    char *byte_array;
    uint64_t secid, seqnum, time, exch_time, channel;
    int32_t buyprice, buyvolume, sellprice, sellvolume;
    int32_t bid_avol_qty, ask_avol_qty;
    bool is_open;
    BookVector bids;
    BookVector asks;
    MDOrderBook();
    ~MDOrderBook();
    void Init(uint16_t);
    void NullifyByteArray();
    void InitByteArray();
    void ParseDeprecatedJPXCSV(const char*, uint16_t);
    void ParseDeprecatedJPXCSV(const string &line);
    void ParseDeprecatedJPXCSV(const char*, uint16_t, uint16_t, uint16_t);
    void ToCSV(ostream&) const;
    string ToCSV() const;
    void FillByteArray();
    void ParseKRXCSV(const char* line, uint16_t size, uint16_t start_at, uint16_t end_at, 
                       double price_multiplier, ChannelType chan_type);
    void ParseKRXCSV(const char* line, uint16_t size, uint16_t start_at, uint16_t end_at, 
                       double price_multiplier);
    void ParseKRXCSV(const char* line, uint16_t size, uint16_t start_at, uint16_t end_at);
    void ParseJPXCSV(const char* line, uint16_t size, uint16_t start_at, uint16_t end_at, 
                       double price_multiplier, ChannelType chan_type);
    void ParseJPXCSV(const char* line, uint16_t size, uint16_t start_at, uint16_t end_at, 
                       double price_multiplier);
    void ParseJPXCSV(const char* line, uint16_t size, uint16_t start_at, uint16_t end_at);
    bool HasByteArray() const;
};


class PriceEquals {
    int32_t price_;
public:
    PriceEquals(int32_t);
    bool operator() (const BookLevel&) const;
};


bool CompareBidLevels(const BookLevel&, const BookLevel&);

bool CompareAskLevels(const BookLevel&, const BookLevel&);


class BookDepth {
public:
    uint16_t side_, depth_;
    BookVector levels;
    BookDepth(uint16_t);
    BookDepth();
    ~BookDepth();
    void Init(uint16_t);
    void Add(int32_t, int32_t, int32_t);
    void Add(int32_t, int32_t);
    void Remove(int32_t, int32_t, bool, int32_t);
    void Sort();
    void Clear();
    uint64_t Size() const;
    void FillMDOrderBook(BookVector&);
};


class BookBuilder {
public:
    bool initialized;
    BookDepth bids, asks;
    MDOrderBook md;
    BookBuilder();
    ~BookBuilder();
    void Init(uint64_t);
    void ResetBooks(bool);
    void ResetBooks();
    bool HasBooks() const;
    void RemoveParticipant(uint16_t, int32_t, int32_t);
    void ModifyDown(uint16_t, int32_t, int32_t);
    void FillMDOrderBook();
};

#endif // BOOK_BUILDING_H
