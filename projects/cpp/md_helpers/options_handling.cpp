#include "options_handling.h"


Option::Option(int16_t t_option_type, double t_strike, double t_tte, double t_interest_rate) :
    initialized(true),
    strike(t_strike),
    interest_rate(t_interest_rate),
    tte(t_tte),
    last_u(std::numeric_limits<double>::quiet_NaN()),
    option_type(t_option_type)
{}

void Option::Init(int16_t t_option_type, double t_strike, double t_tte, double t_interest_rate)
{
    if (std::isnan(t_strike) || std::isnan(t_tte) || std::isnan(t_interest_rate)) {
        return;
    }
    option_type = t_option_type;
    strike = t_strike;
    tte = t_tte;
    interest_rate = t_interest_rate;
    initialized = true;
}

bool Option::Initialized() const
{
    return initialized;
}

void Option::CalculateImpliedValues(double price, double u)
{
    if (!initialized) {
        return;
    }
    if (std::isnan(last_u) || u != last_u) {
        implied.moneyness = log(u / strike);
        last_u = u;
    }
    double seed_vol(std::isnan(implied.vol) ? 1 : implied.vol);
    implied.vol = ::CalculateImpliedVol(option_type, strike, tte, 
                                    interest_rate, u, price,
                                    seed_vol, DEFAULT_TICK_THRESHOLD, DEFAULT_ITERATIONS);
    implied.delta = ::CalculateDelta(option_type, strike, tte, interest_rate, u, implied.vol);
    implied.gamma = ::CalculateGamma(strike, tte, interest_rate, u, implied.vol);
    implied.vega = ::CalculateVega(strike, tte, interest_rate, u, implied.vol);
}


OptionPair::OptionPair(double t_strike, double t_tte, double t_interest_rate) :
    initialized(true),
    strike(t_strike),
    tte(t_tte),
    interest_rate(t_interest_rate),
    call_bid(1, t_strike, t_tte, t_interest_rate),
    call_ask(1, t_strike, t_tte, t_interest_rate),
    put_bid(-1, t_strike, t_tte, t_interest_rate),
    put_ask(-1, t_strike, t_tte, t_interest_rate)
{}


void OptionPair::Init(double t_strike, double t_tte, double t_interest_rate)
{
    if (std::isnan(t_strike) || std::isnan(t_tte) || std::isnan(t_interest_rate)) {
        return;
    }
    strike = t_strike;
    tte = t_tte;
    interest_rate = t_interest_rate;
    call_bid.Init(1, t_strike, t_tte, t_interest_rate);
    call_ask.Init(1, t_strike, t_tte, t_interest_rate);
    put_bid.Init(-1, t_strike, t_tte, t_interest_rate);
    put_ask.Init(-1, t_strike, t_tte, t_interest_rate);
    initialized = true;
}

bool OptionPair::Initialized() const
{
    return initialized;
}

void OptionPair::UpdateUnderlying(double t_u_bid, double t_u_ask)
{
    u_bid = t_u_bid;
    u_ask = t_u_ask;
}

int16_t OptionPair::WhichOTM() const
{
    if (std::isnan(u_bid)) {
        return 0;
    }
    return (strike >= u_bid ? 1 : -1);
}


void OptionPair::CalculateOTMImpliedValues(double bid_price, double ask_price, bool as_market_taker)
{
    int16_t op = WhichOTM();
    if (op == 0) {
        return;
    } else if (op > 0) {
        if (as_market_taker) {
            call_bid.CalculateImpliedValues(bid_price, u_ask);
            call_ask.CalculateImpliedValues(ask_price, u_bid);
        } else {
            call_bid.CalculateImpliedValues(bid_price, u_bid);
            call_ask.CalculateImpliedValues(ask_price, u_ask);
        }
    } else {
        if (as_market_taker) {
            put_bid.CalculateImpliedValues(bid_price, u_bid);
            put_ask.CalculateImpliedValues(ask_price, u_ask);
        } else {
            put_bid.CalculateImpliedValues(bid_price, u_ask);
            put_ask.CalculateImpliedValues(ask_price, u_bid);
        }
    }
}

OptionImpliedValues* OptionPair::ImpliedBidPtr()
{
    int16_t op = WhichOTM();
    if (op == 0) {
        return nullptr;
    } else if (op > 0) {
        return &(call_bid.implied);
    } else {
        return &(put_bid.implied);
    }
}

OptionImpliedValues* OptionPair::ImpliedAskPtr()
{
    int16_t op = WhichOTM();
    if (op == 0) {
        return nullptr;
    } else if (op > 0) {
        return &(call_ask.implied);
    } else {
        return &(put_ask.implied);
    }
}



OptionResampler::OptionResampler(int16_t t_option_type, double t_strike, double t_tte, double t_interest_rate, 
                                 uint64_t resampling_interval) :
    Option(t_option_type, t_strike, t_tte, t_interest_rate),
    price_resampler(resampling_interval),
    vol_resampler(resampling_interval),
    initialized(true)
{}

void OptionResampler::Init(int16_t t_option_type, double t_strike, double t_tte, double t_interest_rate, 
                           uint64_t resampling_interval)
{
    Option::Init(t_option_type, t_strike, t_tte, t_interest_rate);
    price_resampler.Init(resampling_interval);
    vol_resampler.Init(resampling_interval);
    initialized = true;
}

void OptionResampler::Record(uint64_t time, double price, double u)
{
    if (!initialized) {
        return;
    }
    CalculateImpliedValues(price, u);
    price_resampler.Record(time, price);
    vol_resampler.Record(time, implied.vol);
}

void OptionResampler::ForceSamplesAround(uint64_t start_time, uint64_t end_time)
{
    price_resampler.ForceSamplesAround(start_time, end_time);
    vol_resampler.ForceSamplesAround(start_time, end_time);
}

uint64_t OptionResampler::NumSamples() const
{
    return price_resampler.NumSamples();
}

int8_t OptionResampler::CopyPricesToArray(double *dst_arr, uint64_t size)
{
    return price_resampler.CopyValuesToArray(dst_arr, size);
}

int8_t OptionResampler::CopyVolsToArray(double *dst_arr, uint64_t size)
{
    return vol_resampler.CopyValuesToArray(dst_arr, size);
}

int8_t OptionResampler::CopyPriceSamplesToArray(Sample *dst_arr, uint64_t size)
{
    return price_resampler.CopySamplesToArray(dst_arr, size);
}

int8_t OptionResampler::CopyVolSamplesToArray(Sample *dst_arr, uint64_t size)
{
    return vol_resampler.CopySamplesToArray(dst_arr, size);
}

uint64_t OptionResampler::FirstSampleTime() const
{
    return price_resampler.FirstSampleTime();
}

uint64_t OptionResampler::LastSampleTime() const
{
    return price_resampler.LastSampleTime();
}

MeanValueResampler* OptionResampler::PriceMVRPtr()
{
    return &price_resampler;
}

MeanValueResampler* OptionResampler::VolMVRPtr()
{
    return &vol_resampler;
}


/*

OptionContractPrices::OptionContractPrices() :
    call(std::numeric_limits<double>::quiet_NaN()),
    put(std::numeric_limits<double>::quiet_NaN())
{}



RollManager::RollManager() :
    initialized_(false)
{
    
}


void RollManager::Init(uint64_t time_window, double half_life, double strike_boundary)
{
    // moving average with entropy
    ma_.Init(time_window, half_life);
    initialized_ = true;
}
*/