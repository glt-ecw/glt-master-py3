#ifndef FILL_HANDLING_H
#define FILL_HANDLING_H


struct DeioFill
{
    uint64_t deioid, fwd_seqnum, seqnum;
    uint16_t fill_type, side;
    uint64_t secid;
    uint16_t userid, serverid;
    double price;
    int32_t qty, remaining_qty;
    std::string exch_trade_id, fill_time;
    uint16_t order_type;
    uint16_t assumed_fill_type;
};


//class Spread


#endif // FILL_HANDLING_H