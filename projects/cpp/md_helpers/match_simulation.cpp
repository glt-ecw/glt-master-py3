#include "match_simulation.h"

std::ostream& operator<<(std::ostream &out, const OrderType &order_type)
{
    switch (order_type) {
        case OrderType::Limit:
        {
            out << "Limit";
            break;
        }
        case OrderType::FAK:
        {
            out << "FAK";
            break;
        }
        case OrderType::GhostFAK:
        {
            out << "GhostFAK";
            break;
        }
        case OrderType::GhostLimit:
        {
            out << "GhostLimit";
            break;
        }
        default:
        {
            break;
        }
    }
    return out;
}

std::ostream& operator<<(std::ostream &out, const FillCondition &condition)
{
    out << int(condition);
    return out;
}


bool IsValidOrder(uint64_t secid, uint16_t side, int64_t price, int64_t qty)
{
    return secid != 0 && qty > 0 && price != 0 && side != 0;
}

QuantityState::QuantityState(int32_t curr_qty) : curr(curr_qty) {}

void QuantityState::Update(int32_t curr_qty)
{
    prev = curr;
    curr = curr_qty;
}

int32_t QuantityState::Current() const
{
    return curr;
}

int32_t QuantityState::Previous() const
{
    return prev;
}

std::ostream& operator<<(std::ostream &out, const QuantityState &qs)
{
    out << "QuantityState{prev=" << qs.Previous() << ", ";
    out << "curr=" << qs.Current() << "}";
    return out;
}


// MarketState
void MarketState::Update(const MDOrderBook &md, bool use_exch_time)
{
    const BookLevel &curr_bid = md.bids[0];
    const BookLevel &curr_ask = md.asks[0];
    if (curr_bid.qty <= 0 || curr_ask.qty <= 0) {
        return;
    }
    
    seqnum = md.seqnum;
    if (use_exch_time) {
        time = md.exch_time;
    } else {
        time = md.time;
    }
    
    improved_bid = curr_bid.price > top_bid;
    improved_ask = curr_ask.price < top_ask;
    top_bid = curr_bid.price;
    top_ask = curr_ask.price;
    
    bids.clear();
    asks.clear();
    std::copy_if(std::begin(md.bids), std::end(md.bids), std::back_inserter(bids),
                 [](const BookLevel &level) {
                     return level.qty > 0;
                });
    std::copy_if(std::begin(md.asks), std::end(md.asks), std::back_inserter(asks),
                 [](const BookLevel &level) {
                     return level.qty > 0;
                });
    
    buyprice = md.buyprice;
    buyvolume = md.buyvolume;
    sellprice = md.sellprice;
    sellvolume = md.sellvolume;
    
    if (buyvolume > 0 && ask_adjust.find(buyprice) != ask_adjust.end()) {
        TradeReconcileAdjustBackBy(SIDE_BID, buyprice, buyvolume);
    }
    
    if (sellvolume > 0 && bid_adjust.find(sellprice) != bid_adjust.end()) {
        TradeReconcileAdjustBackBy(SIDE_ASK, sellprice, sellvolume);
    }
    
    if (improved_bid) {
        RemoveAdjustQtysFrom(SIDE_BID, top_bid);
    }
    
    if (improved_ask) {
        RemoveAdjustQtysFrom(SIDE_ASK, top_ask);
    }
    
    initialized_ = true;
}

void MarketState::RemoveAdjustQtysFrom(uint16_t side, int32_t top_price)
{
    auto &adjust = (side == SIDE_BID ? bid_adjust : ask_adjust);
    auto it = adjust.begin();
    if (side == SIDE_BID) {
        while (it != adjust.end()) {
            if (it->first < top_price) {
                it = adjust.erase(it);
            } else {
                ++it;
            }
        }
    } else {
        while (it != adjust.end()) {
            if (it->first > top_price) {
                it = adjust.erase(it);
            } else {
                ++it;
            }
        }
    }
}

void MarketState::TradeReconcileAdjustBackBy(uint16_t side, int32_t trade_price, int32_t &trade_volume)
{
    auto &adjust = (side == SIDE_BID ? ask_adjust : bid_adjust);
    if (trade_volume + adjust[trade_price] < 0) {
        adjust[trade_price] += trade_volume;
        trade_volume = 0;
    } else {
        trade_volume += adjust[trade_price];
        adjust.erase(trade_price);
    }
}

bool MarketState::Initialized() const
{
    return initialized_;
}

void MarketState::AdjustQtyBy(uint16_t side, int32_t price, int32_t qty)
{
    auto &adjusted = (side == SIDE_BID ? bid_adjust : ask_adjust);
    if (adjusted[price] + qty > 0) {
        std::cerr << "adjusted[price]=" << adjusted[price] << " + qty=" << qty << " > 0" << '\n';
        adjusted[price] = 0;
    } else {
        adjusted[price] += qty;
    }
}

void MarketState::ReduceAggregateQtyBy(uint16_t side, int32_t price, int32_t qty)
{
    if (qty <= 0) {
        std::cerr << "MarketState::ReduceAggregateQtyBy(uint16_t, int32_t, int32_t): qty=" << qty << '\n';
        return;
    }
    auto &adjust = (side == SIDE_BID ? bid_adjust : ask_adjust);
    for (const auto &level : (side == SIDE_BID ? bids : asks)) {
        if ((side == SIDE_BID && level.price < price) || (side == SIDE_ASK && level.price > price)) {
            break;
        }
        int32_t mkt_qty(level.qty + adjust[level.price]);
        if (mkt_qty <= 0) {
            continue;
        } else if (mkt_qty-qty >= 0) {
            adjust[level.price] -= qty;
            break;
        } else {
            adjust[level.price] -= mkt_qty;
            qty -= mkt_qty;
        }
    }
}

int32_t MarketState::QtyAt(uint16_t side, int32_t price) const
{
    return RealQtyAt(side, price) + AdjustedQtyAt(side, price);
}

int32_t MarketState::RealQtyAt(uint16_t side, int32_t price) const
{
    const auto &levels = (side == SIDE_BID ? bids : asks);
    const auto it = std::find_if(std::begin(levels), std::end(levels),
                                 [&price](const BookLevel &level) {
                                     return price == level.price;
                                 });
    return it == std::end(levels) ? 0 : it->qty;
}

int32_t MarketState::AdjustedQtyAt(uint16_t side, int32_t price) const
{
    const auto &adjusted = (side == SIDE_BID ? bid_adjust : ask_adjust);
    return adjusted.find(price) == adjusted.end() ? 0 : adjusted.at(price);
}

bool MarketState::HasPrice(uint16_t side, int32_t price) const
{
    const auto &levels = (side == SIDE_BID ? bids : asks);
    const auto it = std::find_if(std::begin(levels), std::end(levels),
                                 [&price](const BookLevel &level) {
                                     return price == level.price;
                                 });
    return it != std::end(levels);
    
}

int32_t MarketState::WorstBid() const
{
    return bids.empty() ? std::numeric_limits<int32_t>::min() : bids.back().price;
}

int32_t MarketState::WorstAsk() const
{
    return asks.empty() ? std::numeric_limits<int32_t>::max() : asks.back().price;
}

bool MarketState::IsWorseThanMarketShown(uint16_t side, int32_t price) const
{
    return (side == SIDE_BID ? price < WorstBid() : price > WorstAsk());
}

int32_t MarketState::AggregateQtyAt(uint16_t side, int32_t price) const
{
    if (side == SIDE_BID) {
        return std::accumulate(std::begin(bids), std::end(bids), 0,
                               [&](int32_t left, const BookLevel &right) {
                                   return (right.price >= price ? left + right.qty : left) + AdjustedQtyAt(SIDE_BID, right.price);
                               });
    } else {
        return std::accumulate(std::begin(asks), std::end(asks), 0,
                               [&](int32_t left, const BookLevel &right) {
                                   return (right.price <= price ? left + right.qty : left) + AdjustedQtyAt(SIDE_ASK, right.price);
                               });
    }
}

BookLevel& MarketState::FindBestLevel(uint16_t side, BookLevel &level)
{
    if (side == SIDE_BID) {
        auto it = std::find_if(std::begin(bids), std::end(bids),
                               [&](const BookLevel &level) {
                                   return level.qty + bid_adjust[level.price] > 0;
                               });
        if (it == std::end(bids)) {
            std::cerr << "MarketState::BestLevel(uint16_t): reached end of bids..." << '\n';
        } else {
            level = *it;
        }
    } else {
        auto it = std::find_if(std::begin(asks), std::end(asks),
                               [&](const BookLevel &level) {
                                   return level.qty + ask_adjust[level.price] > 0;
                               });
        if (it == std::end(asks)) {
            std::cerr << "MarketState::BestLevel(uint16_t): reached end of bids..." << '\n';
        } else {
            level = *it; 
        }
    }
    return level;
}


// SimulatedFill
SimulatedFill::SimulatedFill(uint64_t secid, int32_t price, int32_t order_price, uint16_t side, int32_t qty, 
                             int32_t last_position, const MarketState &mkt_state, 
                             FillCondition condition, uint64_t orderid) :
        condition_(condition),
        price_(price), order_price_(order_price), qty_(qty), 
        best_eval_price_(side == SIDE_BID ? std::numeric_limits<int32_t>::min() : std::numeric_limits<int32_t>::max()),
        side_(side),
        id_([]() { static int64_t match_id = 0; return ++match_id; }()),
        last_position_(last_position), orderid_(orderid),
        secid_(secid), seqnum_(mkt_state.seqnum), time_(mkt_state.time)
{
}

uint64_t SimulatedFill::Time() const
{
    return time_;
}

void SimulatedFill::UpdateBestMarket(const MarketState& mkt_state)
{
    if (side_ == SIDE_BID) {
        int32_t top_price = mkt_state.top_bid;
        if (top_price >= best_eval_price_) {
            int32_t top_qty = mkt_state.QtyAt(SIDE_BID, top_price);
            if (top_qty <= 0) {
                return;
            }
            if (top_price > best_eval_price_) {
                best_eval_seqnum_ = mkt_state.seqnum;
                best_eval_timedelta_ = mkt_state.time - time_;
                best_eval_price_ = top_price;
                best_eval_qty_ = top_qty;
            } else if (best_eval_qty_ > top_qty) {
                best_eval_seqnum_ = mkt_state.seqnum;
                best_eval_timedelta_ = mkt_state.time - time_;
                best_eval_qty_ = top_qty;
            }
        }
    } else {
        int32_t top_price = mkt_state.top_ask;
        if (top_price <= best_eval_price_) {
            int32_t top_qty = mkt_state.QtyAt(SIDE_ASK, top_price);
            if (top_qty <= 0) {
                return;
            }
            if (top_price < best_eval_price_) {
                best_eval_seqnum_ = mkt_state.seqnum;
                best_eval_timedelta_ = mkt_state.time - time_;
                best_eval_price_ = top_price;
                best_eval_qty_ = top_qty;
            } else if (best_eval_qty_ > top_qty) {
                best_eval_seqnum_ = mkt_state.seqnum;
                best_eval_timedelta_ = mkt_state.time - time_;
                best_eval_qty_ = top_qty;
            }
        }
    }
}


uint64_t SimulatedFill::BestEvalSeqnum() const
{
    return best_eval_seqnum_;
}


std::ostream& operator<<(std::ostream &out, const SimulatedFill &fill)
{
    out << "SimulatedFill{id=" << fill.id_ << ", ";
    out << "orderid=" << fill.orderid_ << ", ";
    out << "secid=" << fill.secid_ << ", ";
    out << "seqnum=" << fill.seqnum_ << ", ";
    out << "time=" << fill.time_ << ", ";
    out << "price=" << fill.price_ << ", ";
    out << "side=" << fill.side_ << ", ";
    out << "qty=" << fill.qty_ << ", ";
    out << "last_position=" << fill.last_position_ << ", ";
    out << "condition=" << fill.condition_ << ", ";
    out << "eval_seqnum=" << fill.best_eval_seqnum_ << ", ";
    out << "eval_dt=" << fill.best_eval_timedelta_ << ", ";
    out << "eval_price=" << fill.best_eval_price_ << ", ";
    out << "eval_qty=" << fill.best_eval_qty_ << ", ";
    out << "match_ticks=" << ((fill.side_ == SIDE_BID ? 1 : -1) * (fill.best_eval_price_ - fill.price_)) << ", ";
    out << "order_ticks=" << ((fill.side_ == SIDE_BID ? 1 : -1) * (fill.best_eval_price_ - fill.order_price_)) << "}";
    return out;
}


// QueuePosition
int32_t QueuePosition::Update(int32_t position)
{
    if (first < 0) {
        first = position;
    }
    prev = curr;
    if (position < 0) {
        curr = 0;
    } else {
        curr = position;
    }
    return curr;
}

std::ostream& operator<<(std::ostream &out, const QueuePosition &qpos)
{
    out << "first=" << qpos.first << ", prev=" << qpos.prev << ", curr=" << qpos.curr;
    return out;
}

BasicOrder::BasicOrder(uint64_t t_secid, uint64_t t_client_orderid, const char* t_strategy_name, uint64_t t_trigger_time, 
                       uint64_t t_trigger_seqnum, int32_t t_price, 
                       uint16_t t_side, int32_t t_qty, OrderType t_order_type, const char* t_context, 
                       double t_bypass_market_pct) :
    order_type(t_order_type),
    bypass_market_pct(t_bypass_market_pct < 0 ? 0 : (t_bypass_market_pct > 1 ? 1 : t_bypass_market_pct)),
    strategy_name(t_strategy_name),
    context(t_context),
    side(t_side),
    client_orderid(t_client_orderid),
    trigger_time(t_trigger_time),
    trigger_seqnum(t_trigger_seqnum),
    secid(t_secid),
    price(t_price), qty(t_qty)
{
}
/*
BasicOrder::BasicOrder(const BasicOrder& other) :
    order_type(other.order_type),
    bypass_market_pct(other.bypass_market_pct),
    strategy_name(other.strategy_name),
    context(other.context),
    side(other.side),
    client_orderid(other.client_orderid),
    trigger_time(other.trigger_time),
    trigger_seqnum(other.trigger_seqnum),
    secid(other.secid),
    price(other.price), qty(other.qty)
{
}
*/

std::ostream& operator<<(std::ostream& out, const BasicOrder &order)
{
    out << "BasicOrder{client_orderid=" << order.client_orderid << "}";
    return out;
}


// OrderQueuer
OrderQueuer::OrderQueuer() :
    id([]() { static uint64_t order_queuer_id = 0; return ++order_queuer_id; }()),
    orders(),
    prepared_limit_orders(),
    bid_limit_orders(),
    ask_limit_orders(),
    acks()
{
}

OrderQueuer::~OrderQueuer()
{
    cout << "unanswered acks on OrderQueuer id=" << id << ":\n";
    for (const auto &ack : acks) {
        cout << ack << '\n';
    }
    cout << "~OrderQueuer() called for id=" << id << '\n';
}

uint64_t OrderQueuer::PrepareOrder(int64_t secid, const char* strategy_name, uint64_t trigger_time, 
                               uint64_t trigger_seqnum, int32_t price, 
                               uint16_t side, int32_t qty, OrderType order_type, 
                               const char* context, double bypass_market_pct)
{
    if (qty <= 0) {
        return 0;
    }
    ++client_orderid;
    orders.emplace_back(secid, client_orderid, strategy_name, trigger_time, 
                        trigger_seqnum, price, 
                        side, qty, order_type, context, bypass_market_pct);
    return client_orderid;
}


uint64_t OrderQueuer::PrepareLimitOrder(int64_t secid, const char* strategy_name, uint64_t trigger_time, 
                                    uint64_t trigger_seqnum, int32_t price, 
                                    uint16_t side, int32_t qty, const char* context)
{
    uint64_t my_orderid = PrepareOrder(secid, strategy_name, trigger_time, 
                                    trigger_seqnum, price, 
                                    side, qty, OrderType::Limit, context, ZERO_BYPASS_PCT);
    auto &orderids = (side == SIDE_BID ? bid_limit_orders[secid] : ask_limit_orders[secid]);
    orderids[price].insert(my_orderid);
    prepared_limit_orders.emplace(my_orderid, orders.back());
    return my_orderid;
}

uint64_t OrderQueuer::PrepareFAKOrder(int64_t secid, const char* strategy_name, uint64_t trigger_time, 
                                    uint64_t trigger_seqnum, int32_t price, 
                                    uint16_t side, int32_t qty, const char* context)
{
    return PrepareOrder(secid, strategy_name, trigger_time, 
                        trigger_seqnum, price, 
                        side, qty, OrderType::FAK, context, ZERO_BYPASS_PCT);
}

uint64_t OrderQueuer::PrepareSprintOrder(int64_t secid, const char* strategy_name, uint64_t trigger_time, 
                                     uint64_t trigger_seqnum, int32_t price, 
                                     uint16_t side, int32_t qty, const char* context, double bypass_market_pct)
{
    return PrepareOrder(secid, strategy_name, trigger_time, 
                        trigger_seqnum, price, 
                        side, qty, OrderType::Limit, context, bypass_market_pct);
}

uint64_t OrderQueuer::PrepareGhostFAKOrder(int64_t secid, const char* strategy_name, uint64_t trigger_time, 
                                              uint64_t trigger_seqnum, int32_t price, 
                                              uint16_t side, int32_t qty, const char* context)
{
    return PrepareOrder(secid, strategy_name, trigger_time, 
                        trigger_seqnum, price, 
                        side, qty, OrderType::GhostFAK, context, ZERO_BYPASS_PCT);
}

uint64_t OrderQueuer::PrepareGhostLimitOrder(int64_t secid, const char* strategy_name, uint64_t trigger_time, 
                                              uint64_t trigger_seqnum, int32_t price, 
                                              uint16_t side, int32_t qty, const char* context)
{
    return PrepareOrder(secid, strategy_name, trigger_time, 
                        trigger_seqnum, price, 
                        side, qty, OrderType::GhostLimit, context, ZERO_BYPASS_PCT);
}

uint64_t OrderQueuer::PrepareGhostSprintOrder(int64_t secid, const char* strategy_name, uint64_t trigger_time, 
                                              uint64_t trigger_seqnum, int32_t price, 
                                              uint16_t side, int32_t qty, const char* context, double bypass_market_pct)
{
    return PrepareOrder(secid, strategy_name, trigger_time, 
                        trigger_seqnum, price, 
                        side, qty, OrderType::GhostLimit, context, bypass_market_pct);
}

const std::deque<BasicOrder>& OrderQueuer::Orders() const
{
    return orders;
}

uint64_t OrderQueuer::ID() const
{
    return id;
}

bool OrderQueuer::HasOrders() const
{
    return !orders.empty();
}

void OrderQueuer::ClearOrders()
{
    orders.clear();
}

void OrderQueuer::ReceiveAck(SimulatedOrderAck& ack)
{
    acks.push_back(ack);
}

bool OrderQueuer::HasAcks() const
{
    return !acks.empty();
}

SimulatedOrderAck OrderQueuer::TopAck()
{
    return acks.front();
}

void OrderQueuer::RemoveAck()
{
    if (!acks.empty()) {
        const SimulatedOrderAck &ack = acks.front();
        acks.pop_front();
    }
}

uint64_t OrderQueuer::CurrentClientOrderID() const
{
    return client_orderid;
}


// SimulatedOrder
SimulatedOrder::SimulatedOrder(uint64_t me_id, uint64_t origin_id, uint64_t client_orderid,
                               string strategy_name, uint64_t trigger_time, 
                               uint64_t trigger_seqnum, uint64_t secid, int32_t price, 
                               uint16_t side, int32_t qty, OrderType order_type, string context, 
                               double bypass_market_pct) :
    order_type_(order_type),
    bypass_market_pct_(bypass_market_pct < 0 ? 0 : (bypass_market_pct > 1 ? 1 : bypass_market_pct)),
    qty_(qty), price_(price),
    remaining_qty_(qty), 
    side_(side),
    id_([]() { static uint64_t order_id = 0; return ++order_id; }()),
    secid_(secid),
    me_id_(me_id),
    origin_id_(origin_id),
    client_orderid_(client_orderid),
    trigger_time_(trigger_time),
    trigger_seqnum_(trigger_seqnum),
    strategy_name_(std::move(strategy_name)), context_(std::move(context))
{
    if (IsFAK()) {
        queue_position_.Update(0);
    }
    active_ = IsValid();
}

bool SimulatedOrder::IsValid() const
{
    return IsValidOrder(secid_, side_, price_, remaining_qty_);
}

int32_t SimulatedOrder::FirstPosition() const
{
    return queue_position_.first;
}

int32_t SimulatedOrder::LastPosition() const
{
    return queue_position_.curr;
}

void SimulatedOrder::ModifyQueue(int32_t qty_ahead)
{
    if (IsFAK() || qty_ahead < 0) {
        return; // SIMULATED_ORDER_MODIFY_QUEUE_FAILED;
    }
    queue_position_.Update(qty_ahead);
    //return SIMULATED_ORDER_MODIFY_QUEUE_SUCCESS;
}

void SimulatedOrder::AddPlaceInfo(uint64_t time, uint64_t seqnum)
{
    place_time_ = time;
    place_seqnum_ = seqnum;
}

void SimulatedOrder::AddCancelInfo(uint64_t time, uint64_t seqnum)
{
    cancel_time_ = time;
    cancel_seqnum_ = seqnum;
}

int32_t SimulatedOrder::MatchToMarket(int32_t market_price, int32_t filled_qty, 
                                      const MarketState &mkt_state, FillCondition condition,
                                      FillQueue &fills)
{
    if (!active_ || queue_position_.curr > 0) {
        std::cerr << "cannot match " << *this << '\n';
        return SIMULATED_ORDER_CANNOT_MATCH;
    }
    
    if (filled_qty >= remaining_qty_) {
        filled_qty = remaining_qty_;
        remaining_qty_ = 0;
    } else {
        remaining_qty_ -= filled_qty;
    }
    SimulatedFillPtr fill_ptr(new SimulatedFill(secid_, market_price, price_, side_, filled_qty, 
                                                queue_position_.prev, mkt_state, 
                                                condition, id_));
    fills.emplace_back(std::move(fill_ptr));
    ++num_fills_;
    if (remaining_qty_ == 0) {
        active_ = false;
    }
    return filled_qty;
}

void SimulatedOrder::Deactivate()
{
    active_ = false;
}

int32_t SimulatedOrder::RemainingQty() const
{
    return remaining_qty_;
}

uint64_t SimulatedOrder::ID() const
{
    return id_;
}

int32_t SimulatedOrder::Price() const
{
    return price_;
}

uint16_t SimulatedOrder::Side() const
{
    return side_;
}

bool SimulatedOrder::IsFAK() const
{
    return order_type_ == OrderType::FAK || order_type_ == OrderType::GhostFAK;
}

uint64_t SimulatedOrder::TriggerTime() const
{
    return trigger_time_;
}

int32_t SimulatedOrder::BypassMarketBy(int32_t mkt_qty) const
{
    return bypass_market_pct_ * mkt_qty;
}

bool SimulatedOrder::CanPlaceImmediately() const
{
    return bypass_market_pct_ > 0;
}

OrderType SimulatedOrder::Type() const
{
    return order_type_;
}

bool SimulatedOrder::IsRealType() const
{
    return order_type_ == OrderType::Limit || order_type_ == OrderType::FAK;
}

uint64_t SimulatedOrder::NumFills() const
{
    return num_fills_;
}

bool SimulatedOrder::IsActive() const
{
    return active_;
}

uint64_t SimulatedOrder::OriginID() const
{
    return origin_id_;
}

uint64_t SimulatedOrder::ClientOrderID() const
{
    return client_orderid_;
}

uint64_t SimulatedOrder::MEID() const
{
    return me_id_;
}



std::ostream& operator<<(std::ostream &out, const SimulatedOrder &order)
{
    out << "SimulatedOrder{id=" << order.ID() << ", ";
    out << "client_orderid=" << order.client_orderid_ << ", ";
    out << "me_id=" << order.me_id_ << ", ";
    out << "strategy={" << order.strategy_name_ << "}, ";
    out << "secid=" << order.secid_ << ", ";
    out << "live=" << order.IsActive() << ", ";
    out << "order_type=" << order.Type() << ", ";
    out << "seqnums=(" << order.trigger_seqnum_ << ", " << order.place_seqnum_ << ", " << order.cancel_seqnum_ << "), ";
    out << "times=(" << order.trigger_time_ << ", " << order.place_time_ << ", " << order.cancel_time_ << "), ";
    out << "price=" << order.Price() << ", ";
    out << "side=" << order.side_ << ", ";
    out << "qty=" << order.qty_ << ", ";
    out << "rem_qty=" << order.RemainingQty() << ", ";
    out << "first_pos=" << order.FirstPosition() << ", ";
    out << "last_pos=" << order.LastPosition() << ", ";
    out << "context={" << order.context_ << "}, ";
    out << "num_fills=" << order.NumFills() << "}";
    return out;
}


// SimulatedOrderAck
SimulatedOrderAck::SimulatedOrderAck(const SimulatedOrder &order) :
    active(order.IsActive()),
    orderid(order.ID()),
    client_orderid(order.ClientOrderID()),
    origin_id(order.OriginID()),
    me_id(order.MEID()),
    num_fills(order.NumFills()),
    remaining_qty(order.RemainingQty()),
    last_position(order.LastPosition())
{
}

std::ostream& operator<<(std::ostream& out, const SimulatedOrderAck &ack)
{
    out << "SimulatedOrderAck{";
    out << "origin_id=" << ack.origin_id << ", ";
    out << "orderid=" << ack.orderid << ", ";
    out << "client_orderid=" << ack.client_orderid << ", ";
    out << "me_id=" << ack.me_id << ", ";
    out << "live=" << ack.active << ", ";
    out << "remaining_qty=" << ack.remaining_qty << ", ";
    out << "last_position=" << ack.last_position << ", ";
    out << "num_fills=" << ack.num_fills << "}";
    return out;
}

// SimulatedOrderCollection
SimulatedOrderCollection::~SimulatedOrderCollection()
{
    cout << "inactive orders:\n";
    for (const auto &order_ptr : inactive_orders) {
        cout << *order_ptr << '\n';
    }
    cout << "neglected fills:\n";
    for (const auto &fill_ptr : neglected_fills_) {
        cout << *fill_ptr << '\n';
    }
    cout << "remaining orders:\n";
    for (const auto &poq_pair : bid_orders) {
        for (const auto &order_ptr : poq_pair.second) {
            cout << *order_ptr << '\n';
        }
    }
    for (const auto &poq_pair : ask_orders) {
        for (const auto &order_ptr : poq_pair.second) {
            cout << *order_ptr << '\n';
        }
    }
    for (const auto &poq_pair : ghost_bid_orders) {
        for (const auto &order_ptr : poq_pair.second) {
            cout << *order_ptr << '\n';
        }
    }
    for (const auto &poq_pair : ghost_ask_orders) {
        for (const auto &order_ptr : poq_pair.second) {
            cout << *order_ptr << '\n';
        }
    }
    cout << "neglected acks:\n";
    for (const auto &ack : neglected_acks) {
        cout << ack << '\n';
    }
    cout << "~SimulatedOrderCollection() called for secid=" << secid_ << '\n';
}

void SimulatedOrderCollection::Init(uint64_t secid, bool use_exch_time)
{
    secid_ = secid;
    use_exch_time_ = use_exch_time;
    num_orders_placed_ = 0;
    num_fills_ = 0;
    initialized_ = true;
}

void UpdateQueueAtMarket(int32_t mkt_qty, OrderQueue &order_queue, AllowBypass allow_bypass)
{
    int32_t order_qty_before(0);
    for (SimulatedOrderPtr &order_ptr : order_queue) {
        SimulatedOrder &order = *order_ptr;
        if (allow_bypass == AllowBypass::Yes) {
            mkt_qty -= order.BypassMarketBy(mkt_qty);
        }
        if (order.IsRealType()) {
            int32_t position = order_qty_before > mkt_qty ? order_qty_before : mkt_qty;
            if (position < order.LastPosition()) {
                order.ModifyQueue(position);
            }
            order_qty_before = order.LastPosition() + order.RemainingQty();
        } else if (mkt_qty < order.LastPosition()) {
            order.ModifyQueue(mkt_qty);
        }
    }
}

void SimulatedOrderCollection::UpdateUnresolvedOrders()
{
    // bid unresolved orders
    {
        const auto &levels = mkt_state.bids;
        auto it = unresolved_bid_orders.begin();
        while (it != unresolved_bid_orders.end()) {
            int32_t price(it->first);
            auto level_it = std::find_if(std::begin(levels), std::end(levels),
                                         [&price](const BookLevel &level) {
                                             return price == level.price;
                                         });
            if (level_it != std::end(levels)) {
                auto &unresolved_queue = it->second;
                auto mkt_qty = mkt_state.QtyAt(SIDE_BID, price);
                UpdateQueueAtMarket(mkt_qty, unresolved_queue, AllowBypass::Yes);
                auto &active_orders = bid_orders[price];
                while (!unresolved_queue.empty()) {
                    SimulatedOrderPtr &order_ptr = unresolved_queue.front();
                    order_prices[order_ptr->ID()] = order_ptr->Price();
                    // acknowledge that the order was placed
                    neglected_acks.emplace_back(*order_ptr);
                    active_orders.emplace_back(std::move(order_ptr));
                    ++num_orders_placed_;
                    unresolved_queue.pop_front();
                }
                it = unresolved_bid_orders.erase(it);
            } else {
                ++it;
            }
        }
    }
    // ask unresolved orders
    {
        const auto &levels = mkt_state.bids;
        auto it = unresolved_ask_orders.begin();
        while (it != unresolved_ask_orders.end()) {
            int32_t price(it->first);
            auto level_it = std::find_if(std::begin(levels), std::end(levels),
                                         [&price](const BookLevel &level) {
                                             return price == level.price;
                                         });
            if (level_it != std::end(levels)) {
                auto &unresolved_queue = it->second;
                auto mkt_qty = mkt_state.QtyAt(SIDE_ASK, price);
                UpdateQueueAtMarket(mkt_qty, unresolved_queue, AllowBypass::Yes);
                auto &active_orders = ask_orders[price];
                while (!unresolved_queue.empty()) {
                    SimulatedOrderPtr &order_ptr = unresolved_queue.front();
                    order_prices[order_ptr->ID()] = order_ptr->Price();
                    // acknowledge that the order was placed
                    neglected_acks.emplace_back(*order_ptr);
                    active_orders.emplace_back(std::move(order_ptr));
                    ++num_orders_placed_;
                    unresolved_queue.pop_front();
                }
                it = unresolved_ask_orders.erase(it);
            } else {
                ++it;
            }
        }
    }
}

void SimulatedOrderCollection::UpdateQueuePosition()
{
    for (const auto &bid : mkt_state.bids) {
        if (bid.price == mkt_state.sellprice || bid_orders.find(bid.price) == bid_orders.end()) {
            continue;
        }
        if (ghost_bid_orders.find(bid.price) != ghost_bid_orders.end()) {
            UpdateQueueAtMarket(mkt_state.QtyAt(SIDE_BID, bid.price), ghost_bid_orders.at(bid.price), AllowBypass::No);
        }
        if (bid_orders.find(bid.price) != bid_orders.end()) {
            UpdateQueueAtMarket(mkt_state.QtyAt(SIDE_BID, bid.price), bid_orders.at(bid.price), AllowBypass::No);
        }
    }
    for (const auto &ask : mkt_state.asks) {
        if (ask.price == mkt_state.buyprice || ask_orders.find(ask.price) == ask_orders.end()) {
            continue;
        }
        if (ghost_ask_orders.find(ask.price) != ghost_ask_orders.end()) {
            UpdateQueueAtMarket(mkt_state.QtyAt(SIDE_ASK, ask.price), ghost_ask_orders.at(ask.price), AllowBypass::No);
        }
        if (ask_orders.find(ask.price) != ask_orders.end()) {
            UpdateQueueAtMarket(mkt_state.QtyAt(SIDE_ASK, ask.price), ask_orders.at(ask.price), AllowBypass::No);
        }
    }
}

int32_t SimulatedOrderCollection::UpdateQueueFromTrades(int32_t traded_qty, ActualTrade actual_trade, 
                                                        OrderQueue &order_queue, IsReal is_real) 
{
    int32_t qty_accounted_for(0), last_position(0);
    while (!order_queue.empty()) {
        SimulatedOrderPtr &order_ptr = order_queue.front();
        SimulatedOrder &order = *order_ptr;
        int32_t qty_tank(traded_qty - order.LastPosition());
        if (qty_tank <= 0) {
            break;
        }
        if (order.RemainingQty() <= qty_tank) {
            // fully filled; set order to front of the queue and match
            int32_t current_position = order.LastPosition() + order.RemainingQty();
            qty_accounted_for += current_position - last_position;
            order.ModifyQueue(0);
            order.MatchToMarket(
                order.Price(), order.RemainingQty(), mkt_state,
                actual_trade == ActualTrade::Yes ? FillCondition::FullRestingTradeEvent : FillCondition::FullRestingMarketSizeIncrease,
                neglected_fills_
            );
            // acknowledge that the order was filled
            neglected_acks.emplace_back(order);
            ++num_fills_;
            inactive_orders.emplace_back(std::move(order_ptr));
            order_queue.pop_front();
            last_position = current_position;
        } else {
            // partially filled; let the loop below match and update queue
            break;
        }
    }
    for (SimulatedOrderPtr &order_ptr : order_queue) {
        SimulatedOrder &order = *order_ptr;
        if (order.LastPosition() - traded_qty >= 0) {
            order.ModifyQueue(order.LastPosition() - traded_qty);
        } else {
            int32_t qty_remaining(traded_qty - order.LastPosition());
            order.ModifyQueue(0);
            if (order.RemainingQty() > qty_remaining) {
                qty_accounted_for += qty_remaining;
                order.MatchToMarket(
                    order.Price(), qty_remaining, mkt_state,
                    actual_trade == ActualTrade::Yes ? FillCondition::PartRestingTradeEvent : FillCondition::PartRestingMarketSizeIncrease,
                    neglected_fills_
                );
                // acknowledge that the order was filled
                neglected_acks.emplace_back(order);
                ++num_fills_;
            }
        }
    }
    if (is_real == IsReal::Yes) {
        return qty_accounted_for;
    } else {
        return 0;
    }
}

void SimulatedOrderCollection::MatchOrders(OrderBidMap &bid_orders, OrderAskMap &ask_orders, IsReal is_real)
{
    // handle bid orders
    {
        if (mkt_state.sellvolume > 0) {
            int32_t traded_volume(mkt_state.sellvolume);
            auto it = bid_orders.begin();
            while (it != bid_orders.end() && it->first >= mkt_state.sellprice && traded_volume > 0) {
                auto &active_orders = it->second;
                traded_volume -= UpdateQueueFromTrades(traded_volume, ActualTrade::Yes, active_orders, is_real);
                if (active_orders.empty()) {
                    it = bid_orders.erase(it);
                } else {
                    ++it;
                }
            }
        }
        auto it = bid_orders.begin();
        while (it != bid_orders.end() && it->first >= mkt_state.top_ask) {
            int32_t price(it->first);
            int32_t traded_qty(mkt_state.AggregateQtyAt(SIDE_ASK, price));
            if (traded_qty > 0) {
                auto &active_orders = it->second;
                int32_t volume = UpdateQueueFromTrades(traded_qty, ActualTrade::No, active_orders, is_real);
                if (active_orders.empty()) {
                    it = bid_orders.erase(it);
                } else {
                    ++it;
                }
                if (volume > 0) {
                    mkt_state.ReduceAggregateQtyBy(SIDE_ASK, price, volume);
                }
            } else {
                ++it;
            }
        }
    }
    // handle ask orders
    {
        if (mkt_state.buyvolume > 0) {
            int32_t traded_volume(mkt_state.buyvolume);
            auto it = ask_orders.begin();
            while (it != ask_orders.end() && it->first <= mkt_state.buyprice && traded_volume > 0) {
                auto &active_orders = it->second;
                traded_volume -= UpdateQueueFromTrades(traded_volume, ActualTrade::Yes, active_orders, is_real);
                if (active_orders.empty()) {
                    it = ask_orders.erase(it);
                } else {
                    ++it;
                }
            }
        }
        auto it = ask_orders.begin();
        while (it != ask_orders.end() && it->first >= mkt_state.top_bid) {
            int32_t price(it->first);
            int32_t traded_qty(mkt_state.AggregateQtyAt(SIDE_BID, price));
            if (traded_qty > 0) {
                auto &active_orders = it->second;
                int32_t volume = UpdateQueueFromTrades(traded_qty, ActualTrade::No, active_orders, is_real);
                if (active_orders.empty()) {
                    it = ask_orders.erase(it);
                } else {
                    ++it;
                }
                if (volume > 0) {
                    mkt_state.ReduceAggregateQtyBy(SIDE_BID, price, volume);
                }
            } else {
                ++it;
            }
        }
    }
}

bool SimulatedOrderCollection::OrderFullyCollided(SimulatedOrder &order)
{
    if (!mkt_state.Initialized()) {
        return false;
    }
    BookLevel best_level;
    uint16_t opp_side(order.Side() == SIDE_BID ? SIDE_ASK : SIDE_BID);
    mkt_state.FindBestLevel(opp_side, best_level);
    
    if ((order.Side() == SIDE_ASK && order.Price() > best_level.price) ||
        (order.Side() == SIDE_BID && order.Price() < best_level.price) ||
        mkt_state.AggregateQtyAt(opp_side, order.Price()) <= 0)
    {
        return false;
    }
    order.ModifyQueue(0);
    if (opp_side == SIDE_ASK) {
        for (const auto &ask : mkt_state.asks) {
            if (ask.qty <= 0) {
                continue;
            }
            if (order.Price() < ask.price) {
                break;
            }
            int32_t remaining_qty(order.RemainingQty());
            if (remaining_qty <= ask.qty) {
                order.MatchToMarket(ask.price, remaining_qty, mkt_state, FillCondition::FullMarketCollision, neglected_fills_);
                // does this order affect the liquidity in the market?
                if (order.IsRealType()) {
                    ++num_fills_;
                    mkt_state.AdjustQtyBy(opp_side, ask.price, -remaining_qty);
                }
                return true;
            } else {
                order.MatchToMarket(ask.price, ask.qty, mkt_state, FillCondition::PartMarketCollision, neglected_fills_);
                // does this order affect the liquidity in the market?
                if (order.IsRealType()) {
                    ++num_fills_;
                    mkt_state.AdjustQtyBy(opp_side, ask.price, -ask.qty);
                }
            }
        }
    } else {
        for (const auto &bid : mkt_state.bids) {
            if (bid.qty <= 0) {
                continue;
            }
            if (order.Price() > bid.price) {
                break;
            }
            int32_t remaining_qty(order.RemainingQty());
            if (remaining_qty <= bid.qty) {
                order.MatchToMarket(bid.price, remaining_qty, mkt_state, FillCondition::FullMarketCollision, neglected_fills_);
                // does this order affect the liquidity in the market?
                if (order.IsRealType()) {
                    ++num_fills_;
                    mkt_state.AdjustQtyBy(opp_side, bid.price, -remaining_qty);
                }
                return true;
            } else {
                order.MatchToMarket(bid.price, bid.qty, mkt_state, FillCondition::PartMarketCollision, neglected_fills_);
                // does this order affect the liquidity in the market?
                if (order.IsRealType()) {
                    ++num_fills_;
                    mkt_state.AdjustQtyBy(opp_side, bid.price, -bid.qty);
                }
            }
        }
    }
    return false;
}

NewOrderCondition SimulatedOrderCollection::UpdateQueueNewOrder(SimulatedOrder &order)
{
    int32_t position = 0;
    bool is_real = order.IsRealType();
    if (is_real) {
        if (order.Side() == SIDE_BID && 
            bid_orders.find(order.Price()) != bid_orders.end() && 
            !bid_orders.at(order.Price()).empty()) 
        {
            const auto &last_order = *(bid_orders.at(order.Price()).back());
            position = last_order.LastPosition() + last_order.RemainingQty();
        } else if (order.Side() == SIDE_ASK &&
                ask_orders.find(order.Price()) != ask_orders.end() &&
                !ask_orders.at(order.Price()).empty())
        {
            const auto &last_order = *(ask_orders.at(order.Price()).back());
            position = last_order.LastPosition() + last_order.RemainingQty();
        }
    }

    if (!mkt_state.Initialized()) {
        order.ModifyQueue(position);
    } else if (!mkt_state.HasPrice(order.Side(), order.Price())) {
        if (mkt_state.IsWorseThanMarketShown(order.Side(), order.Price())) {
            return NewOrderCondition::Unresolved;
        } else {
            order.ModifyQueue(position);
        }
    } else {
        // think about which makes sense... QtyAt or RealQtyAt (does the adjust qty matter in determining
        // queue position for a new order? maybe so if we care to match wash trades
        int32_t qty_ahead(mkt_state.RealQtyAt(order.Side(), order.Price()));
        if (qty_ahead <= 0) {
            order.ModifyQueue(position);
        } else {
            qty_ahead -= order.BypassMarketBy(qty_ahead);
            order.ModifyQueue(position > qty_ahead ? position : qty_ahead);
        }
    }
    if (is_real) {
        return NewOrderCondition::Good;
    } else {
        return NewOrderCondition::NotReal;
    }
}

void SimulatedOrderCollection::TakeOrder(SimulatedOrderPtr order_ptr, uint64_t time, uint64_t seqnum)
{
    order_ptr->AddPlaceInfo(time, seqnum);
    auto collided_fully = OrderFullyCollided(*order_ptr);
    if (order_ptr->IsFAK()) {
        order_ptr->Deactivate();
    }
    if (order_ptr->Side() == SIDE_BID) {
        if (order_ptr->IsFAK() || collided_fully) {
            // ack the order activity
            neglected_acks.emplace_back(*order_ptr);
            // the order doesn't exist anymore
            inactive_orders.emplace_back(std::move(order_ptr));
        } else {
            auto retval = UpdateQueueNewOrder(*order_ptr);
            if (retval == NewOrderCondition::Unresolved) {
                unresolved_bid_orders[order_ptr->Price()].emplace_back(std::move(order_ptr));
            } else if (retval == NewOrderCondition::NotReal) {
                auto &order_queue = ghost_bid_orders[order_ptr->Price()];
                // ack the order activity
                neglected_acks.emplace_back(*order_ptr);
                // it's alive!
                order_queue.emplace_back(std::move(order_ptr));
            } else {
                auto &order_queue = bid_orders[order_ptr->Price()];
                order_prices[order_ptr->ID()] = order_ptr->Price();
                // ack the order activity
                neglected_acks.emplace_back(*order_ptr);
                // it's a ghost!
                order_queue.emplace_back(std::move(order_ptr));
                ++num_orders_placed_;
            }
        }
    } else {
        if (order_ptr->IsFAK() || collided_fully) {
            // ack the order activity
            neglected_acks.emplace_back(*order_ptr);
            // the order doesn't exist anymore
            inactive_orders.emplace_back(std::move(order_ptr));
        } else {
            auto retval = UpdateQueueNewOrder(*order_ptr);
            if (retval == NewOrderCondition::Unresolved) {
                unresolved_ask_orders[order_ptr->Price()].emplace_back(std::move(order_ptr));
            } else if (retval == NewOrderCondition::NotReal) {
                auto &order_queue = ghost_ask_orders[order_ptr->Price()];
                // ack the order activity
                neglected_acks.emplace_back(*order_ptr);
                // it's a ghost!
                order_queue.emplace_back(std::move(order_ptr));
            } else {
                auto &order_queue = ask_orders[order_ptr->Price()];
                order_prices[order_ptr->ID()] = order_ptr->Price();
                // ack the order activity
                neglected_acks.emplace_back(*order_ptr);
                // it's alive!
                order_queue.emplace_back(std::move(order_ptr));
                ++num_orders_placed_;
            }
        }
    }
}

bool SimulatedOrderCollection::RemoveOrder(uint64_t id)
{
    if (!initialized_) {
        return false;
    }
    
    return true;
}

void SimulatedOrderCollection::Update(const MDOrderBook &md)
{
    if (!initialized_ || md.secid != secid_) {
        return;
    }
    
    mkt_state.Update(md, use_exch_time_);
    
    UpdateUnresolvedOrders();
    MatchOrders(ghost_bid_orders, ghost_ask_orders, IsReal::No);
    MatchOrders(bid_orders, ask_orders, IsReal::Yes);
    UpdateQueuePosition();
}

uint64_t SimulatedOrderCollection::NumOrdersAt(uint16_t side, int32_t price) const
{
    if (side == SIDE_BID) {
        if (bid_orders.find(price) == bid_orders.end()) {
            return 0;
        } else {
            return bid_orders.at(price).size();
        }
    } else if (side == SIDE_ASK) {
        if (ask_orders.find(price) == ask_orders.end()) {
            return 0;
        } else {
            return ask_orders.at(price).size();
        }
    } else {
        return 0;
    }
}

uint64_t SimulatedOrderCollection::NumOrdersPlaced() const
{
    return num_orders_placed_;
}

uint64_t SimulatedOrderCollection::NumFills() const
{
    return num_fills_;
}

bool SimulatedOrderCollection::HasAcks() const
{
    return !neglected_acks.empty();
}

SimulatedOrderAck& SimulatedOrderCollection::TopAck()
{
    return neglected_acks.front();
}

void SimulatedOrderCollection::RemoveAck()
{
    if (!neglected_acks.empty()) {
        neglected_acks.pop_front();
    }
}


void SimulatedOrderCollection::TransferFillsTo(FillQueue &fills)
{
    while (!neglected_fills_.empty()) {
        fills.emplace_back(std::move(neglected_fills_.front()));
        neglected_fills_.pop_front();
    }
}

uint64_t SimulatedOrderCollection::SecID() const
{
    return secid_;
}


// SimulatedMatchingEngine
SimulatedMatchingEngine::SimulatedMatchingEngine(uint64_t id, uint64_t latency, bool use_exch_time, uint64_t evaluate_timedelta)
{
    Init(id, latency, use_exch_time, evaluate_timedelta);
}

SimulatedMatchingEngine::~SimulatedMatchingEngine()
{
    cout << "evaluated fills:\n";
    for (const auto &fill_ptr : evaluated_fills_) {
        cout << *fill_ptr << '\n';
    }
    cout << "fills that haven't been evaluated:\n";
    for (const auto &sec_fills : fills_) {
        for (const auto &fill_ptr : sec_fills.second) {
            cout << *fill_ptr << '\n';
        }
    }
    /*
    cout << "acks never processed:\n";
    for (const auto &id_ack_pair : acks) {
        for (const auto &ack : id_ack_pair.second) {
            cout << "origin_id=" << id_ack_pair.first << ", ack=" << ack << '\n';
        }
    }*/
    cout << "~SimulatedMatchingEngine() id=" << id_ << " called" << '\n';
}


void SimulatedMatchingEngine::Init(uint64_t id, uint64_t latency, bool use_exch_time, uint64_t evaluate_timedelta)
{
    id_ = id;
    latency_ = latency;
    use_exch_time_ = use_exch_time;
    evaluate_timedelta_ = evaluate_timedelta;
    initialized_ = true;
}

void SimulatedMatchingEngine::AddSecurity(uint64_t secid)
{
    auto &collection = order_collections_[secid];
    collection.Init(secid, use_exch_time_);
}

void SimulatedMatchingEngine::ReadMD(const MDOrderBook * const md_ptr)
{
    if (order_collections_.find(md_ptr->secid) == order_collections_.end()) {
        return;
    }
    uint64_t secid = md_ptr->secid;
    auto &collection = order_collections_[secid];
    
    auto &prematch = orders_prematch[secid];
    uint64_t md_time = use_exch_time_ ? md_ptr->exch_time : md_ptr->time;

    while (!prematch.empty() && latency_ + prematch.front()->TriggerTime() < md_time) {
        collection.TakeOrder(std::move(prematch.front()), md_time, md_ptr->seqnum);
        prematch.pop_front();
    }
    // now update market data
    collection.Update(*md_ptr);
    
    // evaluate fills?
    EvaluateFills(secid);
    
    // any new acks?
    ReceiveAcksFrom(collection);
    
    // any new fills?
    ReceiveFillsFrom(collection);
}

void SimulatedMatchingEngine::EnqueueOrder(uint64_t origin_id, uint64_t secid, uint64_t client_orderid,
                                           const string &strategy_name, uint64_t trigger_time, 
                                           uint64_t trigger_seqnum, int32_t price, 
                                           uint16_t side, int32_t qty, OrderType order_type, const string &context, 
                                           double bypass_market_pct)
{
    if (!initialized_ || !IsValidOrder(secid, side, price, qty)) {
        return;
    }
    SimulatedOrderPtr order_ptr(new SimulatedOrder(id_, origin_id, client_orderid,
                                                   strategy_name, trigger_time,
                                                   trigger_seqnum, secid, price,
                                                   side, qty, order_type, context, bypass_market_pct));
    
    if (order_ptr->CanPlaceImmediately() || latency_ == 0) {
        auto &collection = order_collections_[secid];
        collection.TakeOrder(std::move(order_ptr), trigger_time, trigger_seqnum);
        // any acks?
        ReceiveAcksFrom(collection);
        // any fills?
        ReceiveFillsFrom(collection);
    } else {
        orders_prematch[secid].emplace_back(std::move(order_ptr));
    }
    
    
}

void SimulatedMatchingEngine::EnqueueOrdersFrom(OrderQueuer* oq)
{
    if (oq == nullptr) {
        return;
    }
    uint64_t origin_id = oq->ID();
    if (origin_id >= order_queuers.size()) {
        order_queuers.resize(2*origin_id, nullptr);
    }
    if (order_queuers[origin_id] == nullptr) {
        order_queuers[origin_id] = oq;
    }
    
    for (const auto &bo : oq->Orders()) {
        EnqueueOrder(origin_id, bo.secid, bo.client_orderid, bo.strategy_name, bo.trigger_time,
            bo.trigger_seqnum, bo.price, bo.side, bo.qty, bo.order_type, bo.context, bo.bypass_market_pct);
    }
}

const SimulatedOrderCollection& SimulatedMatchingEngine::operator[](uint64_t secid) const
{
    return order_collections_.at(secid);
}

bool SimulatedMatchingEngine::ValidSecID(uint64_t secid) const
{
    return order_collections_.find(secid) != order_collections_.end();
}

void SimulatedMatchingEngine::EvaluateFills(uint64_t secid)
{
    auto &fills = fills_[secid];
    const auto &mkt_state = order_collections_[secid].mkt_state;
    
    while (!fills.empty() && evaluate_timedelta_ + fills.front()->Time() < mkt_state.time) {
        if (fills.front()->BestEvalSeqnum() == 0) {
            fills.front()->UpdateBestMarket(mkt_state);
        }
        evaluated_fills_.emplace_back(std::move(fills.front()));
        fills.pop_front();
    }
    for (const auto &fill_ptr : fills) {
        if (fill_ptr->Time() + latency_ <= mkt_state.time) {
            fill_ptr->UpdateBestMarket(mkt_state);
        }
    }
}

void SimulatedMatchingEngine::ReceiveAcksFrom(SimulatedOrderCollection &collection)
{
    while (collection.HasAcks()) {
        SimulatedOrderAck &ack = collection.TopAck();
        OrderQueuer *oq = order_queuers[ack.origin_id];
        oq->ReceiveAck(ack);
        collection.RemoveAck();
    }
}

void SimulatedMatchingEngine::ReceiveFillsFrom(SimulatedOrderCollection& collection)
{
    collection.TransferFillsTo(fills_[collection.SecID()]);
}





