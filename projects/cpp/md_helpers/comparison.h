#ifndef COMPARISON_H
#define COMPARISON_H

#include <cmath>


inline bool IsNAN(double val)
{
    return std::isnan(val);
}


inline bool EQ_EPSILON(double a, double b, double epsilon)
{
    return std::abs(a-b) <= epsilon;
}


inline bool NE_EPSILON(double a, double b, double epsilon)
{
    return !EQ_EPSILON(a, b, epsilon);
}


inline bool GT_EPSILON(double a, double b, double epsilon)
{
    return a - b > epsilon;
}


inline bool GTE_EPSILON(double a, double b, double epsilon)
{
    return GT_EPSILON(a, b, epsilon) || EQ_EPSILON(a, b, epsilon);
}


inline bool LT_EPSILON(double a, double b, double epsilon)
{
    return GT_EPSILON(b, a, epsilon);
}


inline bool LTE_EPSILON(double a, double b, double epsilon)
{
    return GTE_EPSILON(b, a, epsilon);
}

#endif