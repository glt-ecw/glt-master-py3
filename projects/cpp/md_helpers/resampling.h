#ifndef RESAMPLING_H
#define RESAMPLING_H

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <deque>
#include <limits>

#include "comparison.h"
#include "accumulator.h"


class Sample
{
public:
    uint64_t time, num_measurements;
    double value;
    Sample(uint64_t t_time, uint64_t t_num_measurements, double t_value) :
        time(t_time),
        num_measurements(t_num_measurements),
        value(t_value)
    {}
    Sample() :
        Sample(0, 0, std::numeric_limits<double>::quiet_NaN())
    {}
};


class MeanValueResampler
{
    bool initialized, recording;
    double last_value;
    int64_t interval = 0;
    uint64_t last_recorded_time;
    std::deque<Sample> samples;
public:
    DoubleTWAcc within_interval;
    MeanValueResampler(uint64_t t_interval) :
        initialized(true),
        recording(false),
        last_value(std::numeric_limits<double>::quiet_NaN()),
        interval(t_interval),
        last_recorded_time(0),
        samples()
    {
        InitAveragers();
    }
    
    MeanValueResampler() : 
        initialized(false),
        recording(false),
        interval(0),
        last_recorded_time(0),
        samples()
    {}
 
    bool Initialized() const
    {
        return initialized;
    }
    
    bool Recording() const
    {
        return recording;
    }
    
    void InitAveragers()
    {
        within_interval.Init(interval + 1);
    }
    
    void Init(uint64_t t_interval)
    {
        interval = t_interval;
        InitAveragers();
        initialized = true;
    }
    
    void Record(uint64_t time, double value)
    {
        if (!initialized) {
            return;
        }
        
        if (!recording) {
            last_recorded_time = (time / interval) * interval;
            recording = true;
            if (time == last_recorded_time) {
                samples.emplace_back(last_recorded_time, 1, value);
                return;
            }
        }
        int64_t time_diff = static_cast<int64_t>(time - last_recorded_time);
        while (time_diff > interval) {
            last_recorded_time += interval;
            if (std::isnan(within_interval.Value())) {
                samples.emplace_back(last_recorded_time, 1, last_value);
            } else {
                samples.emplace_back(last_recorded_time, 
                                     within_interval.Size(), 
                                     within_interval.Value() / within_interval.Size());
                last_value = within_interval.LastEventValue();
                within_interval.Clear(std::numeric_limits<double>::quiet_NaN());
            }
            time_diff = static_cast<int64_t>(time - last_recorded_time);
        }
        within_interval.AddData(time, value);
        if (time_diff == interval) {
            last_recorded_time += interval;
            samples.emplace_back(last_recorded_time, 
                                 within_interval.Size(), 
                                 within_interval.Value() / within_interval.Size());
            last_value = within_interval.LastEventValue();
            within_interval.Clear(std::numeric_limits<double>::quiet_NaN());
        }
    }
    
    void ForceSample()
    {
        last_recorded_time += interval;
        uint64_t num_measurements;
        double value;
        if (std::isnan(within_interval.Value())) {
            num_measurements = 1;
            value = last_value;
        } else {
            num_measurements = within_interval.Size();
            value = within_interval.Value() / within_interval.Size();
            last_value = within_interval.LastEventValue();
            within_interval.Clear(std::numeric_limits<double>::quiet_NaN());
        }
        samples.emplace_back(last_recorded_time, num_measurements, value);
    }
    
    void ForceSamplesTo(uint64_t time)
    {
        while (last_recorded_time < time) {
            ForceSample();
        }
    }
    
    void ForceSamplesAround(uint64_t start_time, uint64_t end_time)
    {
        PopFrontTo(start_time);
        ForceSamplesTo(end_time);
    }
    
    uint64_t LastRecordedTime() const
    {
        return last_recorded_time;
    }
    
    uint64_t NumSamples() const
    {
        return samples.size();
    }
    
    bool Empty() const
    {
        return samples.empty();
    }
    
    void PopFront()
    {
        if (Empty()) {
            return;
        }
        samples.pop_front();
    }
    
    Sample PopFrontSample()
    {
        if (Empty()) {
            return Sample();
        }
        auto sample = samples.front();
        samples.pop_front();
        return sample;
    }
    
    void PopFront(uint64_t number)
    {
        if (number == 0) {
            return;
        }
        for (uint64_t i = 0; !Empty() && i < number; ++i) {
            samples.pop_front();
        }
    }
    
    uint64_t PopFrontTo(uint64_t time)
    {
        uint64_t count = 0;
        for (auto it = samples.cbegin(); it != samples.cend() && it->time < time; ++it) {
            ++count;
        }
        PopFront(count);
        return count;
    }
    
    /*
    void PopBack()
    {
        if (Empty()) {
            return;
        }
        samples.pop_back();
    }
    
    Sample PopBackSample()
    {
        if (Empty()) {
            return Sample();
        }
        auto sample = samples.back();
        samples.pop_back();
        return sample;
    }
    
    void PopBack(uint64_t number)
    {
        if (number == 0) {
            return;
        }
        for (uint64_t i = 0; !Empty() && i < number; ++i) {
            samples.pop_back();
        }
    }
    void PopBackTo(uint64_t time)
    {
        uint64_t count = 0;
        for (auto it = samples.crbegin(); it != samples.crend() && it->time > time; ++it) {
            ++count;
        }
        PopBack(count);
    }*/
    
    const Sample& Front() const
    {
        return samples.front();
    }
    
    const Sample& Back() const
    {
        return samples.back();
    }

    const std::deque<Sample>& Samples() const
    {
        return samples;
    }
    
    uint64_t FirstSampleTime() const
    {
        if (Empty()) {
            return 0;
        }
        return samples.front().time;
    }
    
    uint64_t LastSampleTime() const
    {
        if (Empty()) {
            return 0;
        }
        return samples.back().time;
    }
        
    int8_t CopyValuesToArray(double *dst_arr, uint64_t size)
    {
        if (NumSamples() != size) {
            // do not attempt to copy
            return 1;
        }
        const auto &samples = Samples();
        for (uint64_t i=0; i<size; ++i) {
            *(dst_arr + i) = samples[i].value;
        }
        return 0;
    }
    
    int8_t CopySamplesToArray(Sample *dst_arr, uint64_t size)
    {
        if (NumSamples() != size) {
            // do not attempt to copy
            return 1;
        }
        const auto &samples = Samples();
        for (uint64_t i=0; i<size; ++i) {
            const auto &src_sample = samples[i];
            auto &dst_sample = *(dst_arr + i);
            dst_sample.time = src_sample.time;
            dst_sample.num_measurements = src_sample.num_measurements;
            dst_sample.value = src_sample.value;
        }
        return 0;
    }
};


class MVRWithinBounds : public MeanValueResampler
{
    double distance = std::numeric_limits<double>::quiet_NaN();
    double epsilon = std::numeric_limits<double>::quiet_NaN();
    double minval = std::numeric_limits<double>::quiet_NaN();
    double maxval = std::numeric_limits<double>::quiet_NaN();
    
    void Trim()
    {
        if (Empty()) {
            return;
        } else if (std::isnan(minval)) {
            const auto &front = Front();
            minval = front.value;
            maxval = front.value;
            const auto &samples = Samples();
            for (const auto &sample : samples) {
                if (LT_EPSILON(sample.value, minval, epsilon)) {
                    minval = sample.value;
                }
                if (GT_EPSILON(sample.value, maxval, epsilon)) {
                    maxval = sample.value;
                }
            }
            return;
        }
        const Sample &back = Back();
        if (LT_EPSILON(back.value, minval, epsilon)) {
            minval = back.value;
            if (GT_EPSILON(maxval - minval, distance, epsilon)) {
                maxval = minval + distance;
                uint64_t num_samples = NumSamples(), count = 0;
                const auto &samples = Samples();
                for (auto it = samples.crbegin();
                        it != Samples().crend() && GTE_EPSILON(maxval, it->value, epsilon); 
                        ++it) {
                    ++count;
                }
                PopFront(num_samples - count);
            } else {
                // do nothing?
            }
        } else if (GT_EPSILON(back.value, maxval, epsilon)) {
            maxval = back.value;
            if (GT_EPSILON(maxval - minval, distance, epsilon)) {
                minval = maxval - distance;
                uint64_t num_samples = NumSamples(), count = 0;
                const auto &samples = Samples();
                for (auto it = samples.crbegin();
                        it != samples.crend() && LTE_EPSILON(minval, it->value, epsilon); 
                        ++it) {
                    ++count;
                }
                PopFront(num_samples - count);
            } else {
                // do nothing?
            }
        }
    }
public:
    MVRWithinBounds(double t_distance, double t_epsilon, uint64_t interval) :
        MeanValueResampler(interval),
        distance(t_distance),
        epsilon(t_epsilon)
    {}
    
    MVRWithinBounds() : MeanValueResampler() {};
    
    void Init(double t_distance, double t_epsilon, uint64_t interval)
    {
        MeanValueResampler::Init(interval);
        distance = t_distance;
        epsilon = t_epsilon;
    }
    
    void Record(uint64_t time, double value)
    {
        MeanValueResampler::Record(time, value);
        Trim();
    }
    
    void ForceSample(bool trim)
    {
        MeanValueResampler::ForceSample();
        if (trim) {
            Trim();
        }
    }
    
    void ForceSamplesTo(uint64_t time, bool trim)
    {
        MeanValueResampler::ForceSamplesTo(time);
        if (trim) {
            Trim();
        }
    }
    
    void ForceSamplesAround(uint64_t start_time, uint64_t end_time, bool trim)
    {
        MeanValueResampler::ForceSamplesAround(start_time, end_time);
        if (trim) {
            Trim();
        }
    }
    
    double MinVal() const
    {
        return minval;
    }
    
    double MaxVal() const
    {
        return maxval;
    }

};


// functions unrelated to MeanValueResampler but help with python modules
inline double CalculateEntropy(uint64_t time, uint64_t final_time, double half_life)
{
    return std::exp2(-1.0 * static_cast<double>(final_time - time) / half_life);
}

inline void CalculateMVREntropy(MeanValueResampler *mvr, double half_life, double *dst_arr, uint64_t size)
{
    if (mvr == nullptr || dst_arr == nullptr || mvr->NumSamples() != size) {
        return;
    }
    uint64_t final_time = mvr->LastRecordedTime();
    auto it_src = std::begin(mvr->Samples());
    for (uint64_t i=0; i<size; ++i) {
        *(dst_arr + i) = CalculateEntropy(it_src->time, final_time, half_life);
        ++it_src;
    }
}


inline void CalculateMVRDiffAndCopyValuesToArray(MeanValueResampler *mvr_left, MeanValueResampler *mvr_right, 
                                              double *dst_arr, uint64_t size)
{
    if (mvr_left == nullptr
        || mvr_right == nullptr
        || dst_arr == nullptr
        || mvr_left->NumSamples() != size
        || mvr_right->NumSamples() != size) {
        return;
    }
    auto it_left = std::begin(mvr_left->Samples());
    auto it_right = std::begin(mvr_right->Samples());
    for (uint64_t i=0; i<size; ++i) {
        *(dst_arr + i) = it_left->value - it_right->value;
        ++it_left;
        ++it_right;
    }
}

inline double CalculateMVREntropyWeightedValue(MeanValueResampler *mvr, double half_life)
{
    if (mvr->NumSamples() == 0) {
        return std::numeric_limits<double>::quiet_NaN();
    }
    uint64_t final_time = mvr->LastRecordedTime();
    double total_wvalue = 0, total_w = 0;
    for (const auto &sample : mvr->Samples()) {
        double w = CalculateEntropy(sample.time, final_time, half_life);
        total_wvalue += sample.value * w;
        total_w += w;
    }
    return total_wvalue / total_w;
}

#endif
