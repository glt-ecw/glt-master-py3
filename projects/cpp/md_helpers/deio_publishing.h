#ifndef DEIO_PUBLISHING_H
#define DEIO_PUBLISHING_N

#include <cstdint>
#include <limits>
#include <cstring>
#include <vector>


const uint64_t DEIO_PAIR_HEADER_SIZE = 16;
const uint64_t DEIO_PAIR_VALUES_SIZE = 48;


struct DeioPairHeader
{
    uint8_t version, filler1 = 0, filler2 = 0, filler3 = 0;
    uint16_t model_id, num_spreads;
    uint64_t seqnum = 0;
    
    void Init(uint8_t t_version, uint16_t t_model_id, uint16_t t_num_spreads)
    {
        version = t_version;
        model_id = t_model_id;
        num_spreads = t_num_spreads;
    }
    
    uint64_t IncSeqnum()
    {
        ++seqnum;
        return seqnum;
    }
};


struct DeioPairValues
{
    uint8_t spread_type = 0, filler1 = 0, filler2 = 0, filler3 = 0;
    uint32_t securityid_u = 0;
    uint32_t securityid_a = 0, securityid_b = 0;
    double spread_value, ref_u, spread_value_du, improve_u_offset;
    
    DeioPairValues() :
        spread_value(std::numeric_limits<double>::quiet_NaN()),
        ref_u(std::numeric_limits<double>::quiet_NaN()),
        spread_value_du(std::numeric_limits<double>::quiet_NaN()),
        improve_u_offset(0)
    {}
    
    void Init(uint8_t t_spread_type, uint32_t t_securityid_a, uint32_t t_securityid_b, uint32_t t_securityid_u)
    {
        spread_type = t_spread_type;
        securityid_a = t_securityid_a;
        securityid_b = t_securityid_b;
        securityid_u = t_securityid_u;
    }
};


class DeioPairPacker
{
    bool initialized = false;
    uint16_t num_values;
    uint64_t num_bytes;
    DeioPairHeader header;
    std::vector<DeioPairValues> values;
    //std::vector<char> byte_array;
    char *byte_array;
public:
    DeioPairPacker()
    {
        byte_array = nullptr;
        
    }
    ~DeioPairPacker() 
    {
        if (byte_array != nullptr) {
            delete[] byte_array;
        }
        
    }
    void Init(uint8_t version, uint16_t model_id, uint16_t num_spreads)
    {
        header.Init(version, model_id, num_spreads);
        num_values = num_spreads;
        values = std::vector<DeioPairValues>(num_values);
        //num_bytes = sizeof(DeioPairHeader) + num_values * sizeof(DeioPairValues);
        num_bytes = DEIO_PAIR_HEADER_SIZE + num_values * DEIO_PAIR_VALUES_SIZE;
        byte_array = new char[num_bytes]; //std::vector<char>(num_bytes);
        std::memcpy(byte_array + 0, &(header.version), 1);
        std::memcpy(byte_array + 4, &(header.model_id), 2);
        std::memcpy(byte_array + 6, &(header.num_spreads), 2);
        initialized = true;
    }
    
    void InitValuesAt(uint16_t index, uint8_t spread_type, uint32_t securityid_a, uint32_t securityid_b, uint32_t securityid_u)
    {
        if (index > num_values) {
            return;
        }
        DeioPairValues &value = values[index];
        value.Init(spread_type, securityid_a, securityid_b, securityid_u);
        // pack into byte_array
        uint64_t value_start = sizeof(DeioPairHeader) + index * sizeof(DeioPairValues);
        std::memcpy(byte_array + value_start + 0, &(value.spread_type), 1);
        std::memcpy(byte_array + value_start + 4, &(value.securityid_u), 4);
        std::memcpy(byte_array + value_start + 8, &(value.securityid_a), 4);
        std::memcpy(byte_array + value_start + 12, &(value.securityid_b), 4);
        std::memcpy(byte_array + value_start + 16, &(value.spread_value), 8);
        std::memcpy(byte_array + value_start + 24, &(value.ref_u), 8);
        std::memcpy(byte_array + value_start + 32, &(value.spread_value_du), 8);
        std::memcpy(byte_array + value_start + 40, &(value.improve_u_offset), 8);
    }
    
    void UpdateValuesAt(uint16_t index, double spread_value, double ref_u, double spread_value_du, double improve_u_offset)
    {
        if (!initialized || index > num_values) {
            return;
        }
        DeioPairValues &value = values[index];
        value.spread_value = spread_value;
        value.ref_u = ref_u;
        value.spread_value_du = spread_value_du;
        value.improve_u_offset = improve_u_offset;
        uint64_t value_start = sizeof(DeioPairHeader) + index * sizeof(DeioPairValues);
        std::memcpy(byte_array + value_start + 16, &(value.spread_value), 8);
        std::memcpy(byte_array + value_start + 24, &(value.ref_u), 8);
        std::memcpy(byte_array + value_start + 32, &(value.spread_value_du), 8);
        std::memcpy(byte_array + value_start + 40, &(value.improve_u_offset), 8);
    }
    
    uint64_t Prepare()
    {
        if (!initialized) {
            return 0;
        }
        uint64_t seqnum = header.IncSeqnum();
        std::memcpy(byte_array + 8, &(header.seqnum), 8);
        return seqnum;
    }
    
    char* ByteArray() const
    {
        return byte_array;
    }

    uint64_t NumBytes() const
    {
        return num_bytes;
    }
};

#endif
