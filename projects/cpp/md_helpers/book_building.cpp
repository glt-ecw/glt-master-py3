/*
 * Copyright (c) 2016 <copyright holder> <email>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 */

#include "book_building.h"

#include <iostream>


void PrintMDOrderBookColumns(uint16_t depth, ostream &out) {
    out << "pkt_len,eop,channel,msg_idx,topic,secid,seqnum,time,exch_time,buyprice,buyvolume,sellprice,sellvolume,";
    char buf[128];
    for (uint16_t i=0; i<depth; ++i) {
        sprintf(buf, "bidprc%d,bidqty%d,bidpar%d,", i, i, i);
        out << buf;
    }
    for (uint16_t i=0; i<depth; ++i) {
        sprintf(buf, "askprc%d,askqty%d,askpar%d", i, i, i);
        out << buf;
        if (i < MAX_DEPTH - 1) {
            out << ",";
        }
    }
    out << "\n";
}

string PrintMDOrderBookColumns(uint16_t depth)
{
     ostringstream stream;
     PrintMDOrderBookColumns(depth, stream);
     return stream.str().c_str();
}


MDOrderBook::MDOrderBook() :
    byte_array(nullptr)
{
    Init(MAX_DEPTH);
    byte_array = nullptr;
}


MDOrderBook::~MDOrderBook() {
    NullifyByteArray();
}


bool MDOrderBook::HasByteArray() const {
    return byte_array != nullptr;
}


void MDOrderBook::NullifyByteArray() {
    if (byte_array != nullptr) {
        delete[] byte_array;
    }
    byte_array = nullptr;
}


void MDOrderBook::InitByteArray() {
    NullifyByteArray();
    byte_array_len = MD_ORDER_BOOK_BASE_STRUCT_SIZE + depth_ * 24;
    byte_array = new char[byte_array_len];
}


void MDOrderBook::Init(uint16_t depth) 
{
    byte_array_bid_idx_ = MD_ORDER_BOOK_BYTE_ARRAY_LEVELS;
    byte_array_ask_idx_ = MD_ORDER_BOOK_BYTE_ARRAY_LEVELS + 12 * depth;
    packet_len = 0;
    //eop = 'F';
    eop = 0;
    channel = 0;
    msg_idx = 0;
    seqnum = 0;
    time = 0;
    exch_time = 0;
    secid = 0;
    buyprice = 0;
    buyvolume = 0;
    sellprice = 0;
    sellvolume = 0;
    bid_avol_qty = 0;
    ask_avol_qty = 0;
    // write topic to MDOrderBook
    topic = vector<char>(TOPIC_SIZE);
    string my_topic("md");
    uint16_t i(0);
    for (; i<my_topic.length(); ++i) {
        topic[i] = my_topic[i];
    }
    for (; i<TOPIC_SIZE; ++i) {
        topic[i] = ' ';
    }
    bids = BookVector(depth);
    asks = BookVector(depth);
    depth_ = depth;
    InitByteArray();
}


void MDOrderBook::ParseDeprecatedJPXCSV(const char *line, uint16_t size) 
{
    ParseDeprecatedJPXCSV(line, size, 0, numeric_limits<uint16_t>::max());
}

void MDOrderBook::ParseDeprecatedJPXCSV(const string &line)
{
    ParseDeprecatedJPXCSV(line.c_str(), line.length());
}


void MDOrderBook::ParseDeprecatedJPXCSV(const char *line, uint16_t size, uint16_t start_at, uint16_t end_at) 
{
    char tmp[size];
    strcpy(tmp, line);
    char *tline = tmp;
    char *p = strsep(&tline, ",");
    int16_t count(0);
    while (p != nullptr) {
        if (count < start_at) {
            p = strsep(&tline, ",");
            ++count;
            continue;
        }
        if (count >= end_at) {
            break;
        }
        switch (count) {
            case MD_ORDER_BOOK_CSV_PACKET_LEN:
            {
                packet_len = atoi(p);
                break;
            }
            case MD_ORDER_BOOK_CSV_EOP:
            {
                eop = atoi(p);
                break;
            }
            case MD_ORDER_BOOK_CSV_CHANNEL:
            {
                channel = strtoull(p, nullptr, 10);
                break;
            }
            case MD_ORDER_BOOK_CSV_MSG_IDX:
            {
                msg_idx = atoi(p);
                break;
            }
            case MD_ORDER_BOOK_CSV_TOPIC:
            {
                for (uint16_t i=0; i<TOPIC_SIZE; ++i) {
                    topic[i] = p[i];
                }
                break;
            }
            case MD_ORDER_BOOK_CSV_SECID:
            {
                secid = strtoull(p, nullptr, 10);
                break;
            }
            case MD_ORDER_BOOK_CSV_SEQNUM:
            {
                seqnum = strtoull(p, nullptr, 10);
                break;
            }
            case MD_ORDER_BOOK_CSV_TIME:
            {
                time = strtoull(p, nullptr, 10);
                break;
            }
            case MD_ORDER_BOOK_CSV_EXCH_TIME:
            {
                exch_time = strtoull(p, nullptr, 10);
                break;
            }
            case MD_ORDER_BOOK_CSV_BUYPRICE:
            {
                buyprice = atoi(p);
                break;
            }
            case MD_ORDER_BOOK_CSV_BUYVOLUME:
            {
                buyvolume = atoi(p);
                break;
            }
            case MD_ORDER_BOOK_CSV_SELLPRICE:
            {
                sellprice = atoi(p);
                break;
            }
            case MD_ORDER_BOOK_CSV_SELLVOLUME:
            {
                sellvolume = atoi(p);
                break;
            }
            default:
            {
                if (count > 0) {
                    bool ask_side(count >= depth_ * 3 + MD_ORDER_BOOK_CSV_LEVELS_START);
                    uint16_t level_idx((count - MD_ORDER_BOOK_CSV_LEVELS_START) / 3);
                    uint16_t item_idx((count - MD_ORDER_BOOK_CSV_LEVELS_START) % 3);
                    BookVector &levels = (ask_side ? asks : bids);
                    if (ask_side) {
                        level_idx -= depth_;
                    }
                    switch (item_idx) {
                        case 0:
                        {
                            levels[level_idx].price = atoi(p);
                            break;
                        }
                        case 1:
                        {
                            levels[level_idx].qty = atoi(p);
                            break;
                        }
                        case 2:
                        {
                            levels[level_idx].participants = atoi(p);
                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }
                }
                break;
            }
        }
        p = strsep(&tline, ",");
        ++count;
    }
}


void MDOrderBook::ToCSV(ostream &out) const
{
    out << packet_len << ",";
    out << eop << ",";
    out << channel << ",";
    out << msg_idx << ",";
    for (uint16_t i=0; i<TOPIC_SIZE; ++i) {
        out << topic[i];
    }
    out << ",";
    out << secid << ",";
    out << seqnum << ",";
    out << time << ",";
    out << exch_time << ",";
    out << buyprice << ",";
    out << buyvolume << ",";
    out << sellprice << ",";
    out << sellvolume << ",";
    for (const BookLevel &level : bids) {
        out << level.price << ",";
        out << level.qty << ",";
        out << level.participants << ",";
    }
    uint16_t count(0);
    for (const BookLevel &level : asks) {
        out << level.price << ",";
        out << level.qty << ",";
        out << level.participants;
        if (++count < depth_) {
            out << ",";
        }
    }
    return;
}

string MDOrderBook::ToCSV() const
{
    ostringstream out;
    /*
    out << packet_len << ",";
    out << eop << ",";
    out << channel << ",";
    out << msg_idx << ",";
    for (uint16_t i=0; i<TOPIC_SIZE; ++i) {
        out << topic[i];
    }
    out << ",";
    out << secid << ",";
    out << seqnum << ",";
    out << time << ",";
    out << exch_time << ",";
    out << buyprice << ",";
    out << buyvolume << ",";
    out << sellprice << ",";
    out << sellvolume << ",";
    for (const BookLevel &level : bids) {
        out << level.price << ",";
        out << level.qty << ",";
        out << level.participants << ",";
    }
    uint16_t count(0);
    for (const BookLevel &level : asks) {
        out << level.price << ",";
        out << level.qty << ",";
        out << level.participants;
        if (++count < depth_) {
            out << ",";
        }
    }*/
    ToCSV(out);
    out << "\n";
    return out.str();
}


void MDOrderBook::FillByteArray()
{
    memcpy(byte_array + MD_ORDER_BOOK_BYTE_ARRAY_PACKET_LEN, &packet_len, sizeof(packet_len));
    memcpy(byte_array + MD_ORDER_BOOK_BYTE_ARRAY_EOP, &eop, sizeof(eop));
    memcpy(byte_array + MD_ORDER_BOOK_BYTE_ARRAY_CHANNEL, &channel, sizeof(channel));
    memcpy(byte_array + MD_ORDER_BOOK_BYTE_ARRAY_MSG_IDX, &msg_idx, sizeof(msg_idx));
    for (uint16_t i=0; i<TOPIC_SIZE; ++i) {
        byte_array[i + MD_ORDER_BOOK_BYTE_ARRAY_TOPIC] = topic[i];
    }
    //memcpy(byte_array + MD_ORDER_BOOK_BYTE_ARRAY_TOPIC, topic, TOPIC_SIZE);
    memcpy(byte_array + MD_ORDER_BOOK_BYTE_ARRAY_SECID, &secid, sizeof(secid));
    memcpy(byte_array + MD_ORDER_BOOK_BYTE_ARRAY_SEQNUM, &seqnum, sizeof(seqnum));
    memcpy(byte_array + MD_ORDER_BOOK_BYTE_ARRAY_TIME, &time, sizeof(time));
    memcpy(byte_array + MD_ORDER_BOOK_BYTE_ARRAY_EXCH_TIME, &exch_time, sizeof(exch_time));
    memcpy(byte_array + MD_ORDER_BOOK_BYTE_ARRAY_BUYPRICE, &buyprice, sizeof(buyprice));
    memcpy(byte_array + MD_ORDER_BOOK_BYTE_ARRAY_BUYVOLUME, &buyvolume, sizeof(buyvolume));
    memcpy(byte_array + MD_ORDER_BOOK_BYTE_ARRAY_SELLPRICE, &sellprice, sizeof(sellprice));
    memcpy(byte_array + MD_ORDER_BOOK_BYTE_ARRAY_SELLVOLUME, &sellvolume, sizeof(sellvolume));
    uint16_t i;
    for (i=0; i<depth_; ++i) {
        const BookLevel &level = bids[i];
        uint16_t offset(i * 12);
        memcpy(byte_array + byte_array_bid_idx_ + offset, &(level.price), sizeof(level.price));
        memcpy(byte_array + byte_array_bid_idx_ + offset + 4, &(level.qty), sizeof(level.qty));
        memcpy(byte_array + byte_array_bid_idx_ + offset + 8, &(level.participants), sizeof(level.participants));
    }
    for (i=0; i<depth_; ++i) {
        const BookLevel &level = asks[i];
        uint16_t offset(i * 12);
        memcpy(byte_array + byte_array_ask_idx_ + offset, &(level.price), sizeof(level.price));
        memcpy(byte_array + byte_array_ask_idx_ + offset + 4, &(level.qty), sizeof(level.qty));
        memcpy(byte_array + byte_array_ask_idx_ + offset + 8, &(level.participants), sizeof(level.participants));
    }    
}


void MDOrderBook::ParseKRXCSV(const char *line, uint16_t size, uint16_t start_at, uint16_t end_at, 
                              double price_multiplier, ChannelType chan_type)
{
    // I need to process info from market data only after processing the next line
    char tmp[size];
    strcpy(tmp, line);
    char *tline = tmp;
    char *p = strsep(&tline, ",");
    int16_t count(0);
    //uint64_t last_packet_num = 0;
    int32_t trade_price(0), trade_qty(0);
    while (p != nullptr) {
        if (count < start_at) {
            p = strsep(&tline, ",");
            ++count;
            continue;
        }
        if (count >= end_at) {
            break;
        }
        switch (count) {
            case BRUCE_KRX_CSV_PACKET_NUM:
            {
                //uint64_t packet_num = atoi(p);
                //eop = (packet_num != last_packet_num);
                //last_packet_num = packet_num;
                break;
            }
            //case BRUCE_CSV_TIME_STRING:
            //{
            //    break;
            //}
            case BRUCE_KRX_CSV_INT_TIME:
            {
                time = strtoull(p, nullptr, 10);
                break;
            }
            case BRUCE_KRX_CSV_INT_EXCH_TIME:
            {
                exch_time = strtoull(p, nullptr, 10);
                break;
            }
            case BRUCE_KRX_CSV_CHANNEL:
            {
                if (chan_type == ChannelType::None) {
                    channel = 0;
                } else {
                    switch (chan_type) {
                        case ChannelType::Integer:
                        {
                            channel = strtoull(p, nullptr, 10);
                            break;
                        }
                        case ChannelType::StringIP:
                        {
                            uint64_t ip_a, ip_b, ip_c, ip_d;
                            uint64_t port;
                            std::sscanf(p, "%lu.%lu.%lu.%lu:%lu", &ip_a, &ip_b, &ip_c, &ip_d, &port);
                            channel = (ip_d << 16) + (ip_c << 24) + (ip_b << 32) + (ip_a << 40) + port;
                            break;
                        }
                        default:
                        {
                            channel = 0;
                            break;
                        }
                    }
                }
                break;
            }
            case BRUCE_KRX_CSV_SEQNUM:
            {
                seqnum = strtoull(p, nullptr, 10);
                break;
            }
            case BRUCE_KRX_CSV_MSG:
            {
                uint64_t n_msg = std::strlen(p);
                uint16_t n_topic = (n_msg > TOPIC_SIZE ? TOPIC_SIZE : n_msg);
                for (uint16_t i=0; i<n_topic; ++i) {
                    topic[i] = p[i];
                }
                break;
            }
            //case BRUCE_CSV_SYMBOL:
            //{
            //    break;
            //}
            case BRUCE_KRX_CSV_SECID:
            {
                secid = atoi(p);
                break;
            }
            case BRUCE_KRX_CSV_MD_STATUS:
            {
                is_open = (p[0] == 'O');
                break;
            }
            //case BRUCE_CSV_EXCH_MD_STATUS:
            //{
            //    break;
            //}
            case BRUCE_KRX_CSV_TRADE_PRICE:
            {
                double value = atof(p);
                trade_price = std::lround(price_multiplier * value);
                break;
            }
            case BRUCE_KRX_CSV_TRADE_QTY:
            {
                trade_qty = atoi(p);
                break;
            }
            case BRUCE_KRX_CSV_TRADE_SIDE:
            {
                char side(p[0]);
                if (side == 'B') {
                    buyprice = trade_price;
                    buyvolume = trade_qty;
                    sellprice = 0;
                    sellvolume = 0;
                } else if (side == 'O') {
                    buyprice = 0;
                    buyvolume = 0;
                    sellprice = trade_price;
                    sellvolume = trade_qty;
                } else {
		    buyprice = 0;
		    buyvolume = 0;
		    sellprice = 0;
		    sellvolume = 0;
		}
                break;
            }
            //case BRUCE_CSV_TOTAL_VOLUME_TRADED:
            //{
            //    break;
            //}
            //case BRUCE_CSV_TOTAL_BID_QUANTITY:
            //{
            //    break;
            //}
            //case BRUCE_CSV_TOTAL_ASK_QUANTITY:
            //{
            //    break;
            //}
            //case BRUCE_CSV_TOTAL_BID_PARTICIPANTS:
            //{
            //    break;
            //}
            //case BRUCE_CSV_TOTAL_ASK_PARTICIPANTS:
            //{
            //    break;
            //}
            //case BRUCE_CSV_INDICATIVE_PRICE:
            //{
            //    break;
            //}
            default:
            {
                if (count >= BRUCE_KRX_CSV_ORDER_BOOK_START) {
                    bool ask_side(count >= depth_ * 3 + BRUCE_KRX_CSV_ORDER_BOOK_START);
                    uint16_t level_idx((count - BRUCE_KRX_CSV_ORDER_BOOK_START) / 3);
                    uint16_t item_idx((count - BRUCE_KRX_CSV_ORDER_BOOK_START) % 3);
                    BookVector &levels = (ask_side ? asks : bids);
                    if (ask_side) {
                        level_idx -= depth_;
                    }
                    switch (item_idx) {
                        case 0:
                        {
                            double value = atof(p);
                            levels[level_idx].price = std::lround(price_multiplier * value);
                            break;
                        }
                        case 1:
                        {
                            levels[level_idx].qty = atoi(p);
                            break;
                        }
                        case 2:
                        {
                            levels[level_idx].participants = atoi(p);
                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }
                }
                break;
            }
        }
        p = strsep(&tline, ",");
        ++count;
    }
}


void MDOrderBook::ParseKRXCSV(const char* line, uint16_t size, uint16_t start_at, uint16_t end_at, 
                              double price_multiplier)
{
    ParseKRXCSV(line, size, start_at, end_at, price_multiplier, ChannelType::StringIP);
}


void MDOrderBook::ParseKRXCSV(const char* line, uint16_t size, uint16_t start_at, uint16_t end_at)
{
    ParseKRXCSV(line, size, start_at, end_at, 10000);
}


void MDOrderBook::ParseJPXCSV(const char *line, uint16_t size, uint16_t start_at, uint16_t end_at, 
                              double price_multiplier, ChannelType chan_type)
{
    // I need to process info from market data only after processing the next line
    char tmp[size];
    strcpy(tmp, line);
    char *tline = tmp;
    char *p = strsep(&tline, ",");
    int16_t count(0);
    //uint64_t last_packet_num = 0;
    int32_t trade_price(0), trade_qty(0);
    while (p != nullptr) {
        if (count < start_at) {
            p = strsep(&tline, ",");
            ++count;
            continue;
        }
        if (count >= end_at) {
            break;
        }
        switch (count) {
            case BRUCE_JPX_CSV_PACKET_NUM:
            {
                //uint64_t packet_num = atoi(p);
                //eop = (packet_num != last_packet_num);
                //last_packet_num = packet_num;
                break;
            }
            //case BRUCE_CSV_TIME_STRING:
            //{
            //    break;
            //}
            case BRUCE_JPX_CSV_INT_TIME:
            {
                time = strtoull(p, nullptr, 10);
                break;
            }
            case BRUCE_JPX_CSV_INT_EXCH_TIME:
            {
                exch_time = strtoull(p, nullptr, 10);
                break;
            }
            case BRUCE_JPX_CSV_CHANNEL:
            {
                if (chan_type == ChannelType::None) {
                    channel = 0;
                } else {
                    switch (chan_type) {
                        case ChannelType::Integer:
                        {
                            channel = strtoull(p, nullptr, 10);
                            break;
                        }
                        case ChannelType::StringIP:
                        {
                            uint64_t ip_a, ip_b, ip_c, ip_d;
                            uint64_t port;
                            std::sscanf(p, "%lu.%lu.%lu.%lu:%lu", &ip_a, &ip_b, &ip_c, &ip_d, &port);
                            // for japan, just take the last digit of the ip
                            channel = ip_d;
                            //channel = (ip_d << 16) + (ip_c << 24) + (ip_b << 32) + (ip_a << 40) + port;
                            break;
                        }
                        default:
                        {
                            channel = 0;
                            break;
                        }
                    }
                }
                break;
            }
            case BRUCE_JPX_CSV_SEQNUM:
            {
                seqnum = strtoull(p, nullptr, 10);
                break;
            }
            case BRUCE_JPX_CSV_MSG:
            {
                uint64_t n_msg = std::strlen(p);
                uint16_t n_topic = (n_msg > TOPIC_SIZE ? TOPIC_SIZE : n_msg);
                for (uint16_t i=0; i<n_topic; ++i) {
                    topic[i] = p[i];
                }
                break;
            }
            //case BRUCE_CSV_SYMBOL:
            //{
            //    break;
            //}
            case BRUCE_JPX_CSV_SECID:
            {
                secid = atoi(p);
                break;
            }
            case BRUCE_JPX_CSV_MD_STATUS:
            {
                is_open = (p[0] == 'O');
                //std::cout << "md.seqnum=" << seqnum << ", is_open=" << is_open << ", p=" << p << '\n';
                break;
            }
            //case BRUCE_CSV_EXCH_MD_STATUS:
            //{
            //    break;
            //}
            case BRUCE_JPX_CSV_TRADE_PRICE:
            {
                double value = atof(p);
                trade_price = std::lround(price_multiplier * value);
                break;
            }
            case BRUCE_JPX_CSV_TRADE_QTY:
            {
                trade_qty = atoi(p);
                break;
            }
            case BRUCE_JPX_CSV_TRADE_SIDE:
            {
                char side(p[0]);
                // we don't care about any trade messages except E in itch
                if (topic[0] == 'E') {
                    if (side == 'B') {
                        buyprice = trade_price;
                        buyvolume = trade_qty;
                        sellprice = 0;
                        sellvolume = 0;
                    } else if (side == 'O') {
                        buyprice = 0;
                        buyvolume = 0;
                        sellprice = trade_price;
                        sellvolume = trade_qty;
                    } else {
                        buyprice = 0;
                        buyvolume = 0;
                        sellprice = 0;
                        sellvolume = 0;
                    }
                } else {
                    buyprice = 0;
                    buyvolume = 0;
                    sellprice = 0;
                    sellvolume = 0;
                }
                break;
            }
            //case BRUCE_CSV_TOTAL_VOLUME_TRADED:
            //{
            //    break;
            //}
            //case BRUCE_CSV_TOTAL_BID_QUANTITY:
            //{
            //    break;
            //}
            //case BRUCE_CSV_TOTAL_ASK_QUANTITY:
            //{
            //    break;
            //}
            //case BRUCE_CSV_TOTAL_BID_PARTICIPANTS:
            //{
            //    break;
            //}
            //case BRUCE_CSV_TOTAL_ASK_PARTICIPANTS:
            //{
            //    break;
            //}
            //case BRUCE_CSV_INDICATIVE_PRICE:
            //{
            //    break;
            //}
            case BRUCE_JPX_CSV_BID_AVOL_QTY:
            {
                bid_avol_qty = atoi(p);
                break;
            }
            case BRUCE_JPX_CSV_ASK_AVOL_QTY:
            {
                ask_avol_qty = atoi(p);
                break;
            }
            case BRUCE_JPX_CSV_PACKET_LEN:
            {
                packet_len = atoi(p);
                break;
            }
            case BRUCE_JPX_CSV_END_OF_PACKET:
            {
                eop = atoi(p);
                break;
            }
            default:
            {
                if (count >= BRUCE_JPX_CSV_ORDER_BOOK_START 
                        && count < BRUCE_JPX_CSV_BID_AVOL_QTY) {
                    bool ask_side(count >= depth_ * 3 + BRUCE_JPX_CSV_ORDER_BOOK_START);
                    uint16_t level_idx((count - BRUCE_JPX_CSV_ORDER_BOOK_START) / 3);
                    uint16_t item_idx((count - BRUCE_JPX_CSV_ORDER_BOOK_START) % 3);
                    BookVector &levels = (ask_side ? asks : bids);
                    if (ask_side) {
                        level_idx -= depth_;
                    }
                    switch (item_idx) {
                        case 0:
                        {
                            double value = atof(p);
                            levels[level_idx].price = std::lround(price_multiplier * value);
                            break;
                        }
                        case 1:
                        {
                            levels[level_idx].qty = atoi(p);
                            break;
                        }
                        case 2:
                        {
                            levels[level_idx].participants = atoi(p);
                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }
                }
                break;
            }
        }
        p = strsep(&tline, ",");
        ++count;
    }
}


void MDOrderBook::ParseJPXCSV(const char* line, uint16_t size, uint16_t start_at, uint16_t end_at, 
                              double price_multiplier)
{
    ParseJPXCSV(line, size, start_at, end_at, price_multiplier, ChannelType::StringIP);
}


void MDOrderBook::ParseJPXCSV(const char* line, uint16_t size, uint16_t start_at, uint16_t end_at)
{
    ParseJPXCSV(line, size, start_at, end_at, 10000);
}


PriceEquals::PriceEquals(int32_t price) : 
    price_(price) 
{
    
};


bool PriceEquals::operator() (const BookLevel &level) const 
{
    return level.price == price_;
}


bool CompareBidLevels(const BookLevel &a, const BookLevel &b) 
{
    return a.price < b.price;
}


bool CompareAskLevels(const BookLevel &a, const BookLevel &b) 
{
    return a.price > b.price;
}


BookDepth::BookDepth(uint16_t side) 
{
    Init(side);
}


BookDepth::BookDepth() 
{
    Init(0);
}


BookDepth::~BookDepth()
{
    
};


void BookDepth::Init(uint16_t side) 
{
    side_ = side;
}


void BookDepth::Add(int32_t price, int32_t qty, int32_t participants) 
{
    auto it = find_if(levels.begin(), levels.end(), PriceEquals(price));
    if (it != levels.end()) {
        it->qty += qty;
        it->participants += participants;
    } else {
        BookLevel level;
        level.price = price;
        level.qty = qty;
        level.participants = participants;
        levels.push_back(level);
        Sort();
    }
    return;
}


void BookDepth::Add(int32_t price, int32_t qty) 
{
    Add(price, qty, 1);
}


void BookDepth::Remove(int32_t price, int32_t qty, bool order_removed, int32_t participants=1) 
{
    auto it = find_if(levels.begin(), levels.end(), PriceEquals(price));
    if (it != levels.end()) {
        it->qty -= qty;
        if (order_removed) {
            //--(it->participants);
            it->participants -= participants;
        }
        if (it->qty == 0) {
            //Sort();
            if (it->participants != 0) {
                throw "Zero qty but participants still exist?";
            }
            levels.erase(it);
        } else if (it->qty < 0 || it->participants < 0) {
            throw "Negative qty or participant detected!";
        }
    }
    return;
}


void BookDepth::Sort() 
{
    sort(levels.begin(), levels.end(), side_ == SIDE_BID ? CompareBidLevels : CompareAskLevels);
}


void BookDepth::Clear() 
{
    levels.clear();
}

uint64_t BookDepth::Size() const
{
    return levels.size();
}


void BookDepth::FillMDOrderBook(BookVector &md_levels) 
{
    size_t num_levels(levels.size());
    uint16_t depth(md_levels.size());
    uint16_t i, n(num_levels < depth ? num_levels : depth);
    for (i=0; i<n; ++i) {
        const BookLevel &level = levels[num_levels-1-i];
        md_levels[i].price = level.price;
        md_levels[i].qty = level.qty;
        md_levels[i].participants = level.participants;
    }
    for (; i<depth; ++i) {
        md_levels[i].price = 0;
        md_levels[i].qty = 0;
        md_levels[i].participants = 0;
    }
    return;
}


BookBuilder::BookBuilder() : 
    initialized(false),
    bids(SIDE_BID),
    asks(SIDE_ASK)
{
    
}


BookBuilder::~BookBuilder() 
{
    
}


void BookBuilder::Init(uint64_t secid) 
{
    md.secid = secid;
    initialized = true;
    return;
}


void BookBuilder::ResetBooks(bool reset_initialized)
{
    bids.Clear();
    asks.Clear();
    initialized = !reset_initialized;
    return;
}


void BookBuilder::ResetBooks()
{
    ResetBooks(true);
}

bool BookBuilder::HasBooks() const
{
    return bids.Size() > 0 && asks.Size() > 0;
}


void BookBuilder::RemoveParticipant(uint16_t side, int32_t price, int32_t qty) 
{
    if (side == SIDE_BID) {
        bids.Remove(price, qty, true);
    } else {
        asks.Remove(price, qty, true);
    }
    return;
}


void BookBuilder::ModifyDown(uint16_t side, int32_t price, int32_t qty) 
{
    if (side == SIDE_BID) {
        bids.Remove(price, qty, false);
    } else {
        asks.Remove(price, qty, false);
    }
    return;
}


void BookBuilder::FillMDOrderBook() 
{
    bids.FillMDOrderBook(md.bids);
    asks.FillMDOrderBook(md.asks);
    return;
}
