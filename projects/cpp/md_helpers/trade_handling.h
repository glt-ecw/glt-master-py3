/*
 * Copyright (c) 2016 <copyright holder> <email>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 */

#ifndef TRADE_HANDLING_H
#define TRADE_HANDLING_H

#include "book_building.h"
#include "accumulator.h"


struct TradeEvent {
    uint64_t time;
    uint16_t side;
    int32_t price;
    int32_t qty;
};


using TradeNode = DataEvent<int32_t>;


template <class AccQtyType>
class VolumeAccumulatorTemplate : public TimeWindowAccumulator<int32_t, AccQtyType>
{
public:
    AccQtyType AddTrade(uint64_t time, int32_t qty) {
        return TimeWindowAccumulator<int32_t, AccQtyType>::AddData(time, qty);
    }
    
    void RemoveTrades(uint64_t time) {
        TimeWindowAccumulator<int32_t, AccQtyType>::RemoveData(time);
    }
    
    AccQtyType Qty() const {
        return TimeWindowAccumulator<int32_t, AccQtyType>::value_;
    }
};

using VolumeAccumulator = VolumeAccumulatorTemplate<int32_t>;
using DoubleVolumeAccumulator = VolumeAccumulatorTemplate<double>;


#endif // TRADE_HANDLING_H
