#ifndef OPTIONS_HANDLING_H
#define OPTIONS_HANDLING_H

#include <cmath>
#include <cstdint>
#include <limits>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#include "comparison.h"
#include "resampling.h"


const double ALMOST_ZERO = 1e-8;
const double M_1_SQRT2PI = 0.5 * M_SQRT1_2 * M_2_SQRTPI;

// for implied vol calculations
const double DEFAULT_TICK_THRESHOLD = 0.001;
const uint64_t DEFAULT_ITERATIONS = 20;


inline double NormalCDF(double x)
{
    return 0.5 + 0.5 * std::erf(x * M_SQRT1_2);
}


inline double NormalPDF(double x)
{
    return M_1_SQRT2PI * std::exp(-0.5 * x * x);
}


inline double CalculateD1(double strike, double tte, double u, double vol)
{
    if (LTE_EPSILON(tte, 0, ALMOST_ZERO)) {
        tte = ALMOST_ZERO;
    }
    if (LTE_EPSILON(vol, 0, ALMOST_ZERO)) {
        vol = ALMOST_ZERO;
    }
    return (std::log(u / strike) + 0.5 * vol * vol * tte) / (vol * std::sqrt(tte));
}


inline double CalculateD2(double strike, double tte, double u, double vol)
{
    return CalculateD1(strike, tte, u, vol) - vol * std::sqrt(tte);
}


inline double NormalCDFD1(int16_t option_type, double strike, double tte, double u, double vol)
{
    return NormalCDF(option_type * CalculateD1(strike, tte, u, vol));
}


inline double NormalCDFD2(int16_t option_type, double strike, double tte, double u, double vol)
{
    return NormalCDF(option_type * CalculateD2(strike, tte, u, vol));
}


inline double NPVAdjust(double tte, double interest_rate)
{
    return std::exp(-interest_rate * tte);
}


inline double CalculateTV(int16_t option_type, double strike, double tte,
                          double interest_rate, double u, double vol)
{
    double c_npv = option_type * NPVAdjust(interest_rate, tte);
    double cdf_d1 = NormalCDFD1(option_type, strike, tte, u, vol);
    double cdf_d2 = NormalCDFD2(option_type, strike, tte, u, vol);
    return c_npv * (u * cdf_d1 - strike * cdf_d2);
}


inline double CalculateDelta(int16_t option_type, double strike, double tte,
                             double interest_rate, double u, double vol)
{
    double c_npv = option_type * NPVAdjust(interest_rate, tte);
    return c_npv * NormalCDFD1(option_type, strike, tte, u, vol);
}


inline double CalculateGamma(double strike, double tte,
                             double interest_rate, double u, double vol)
{
    double npv = NPVAdjust(interest_rate, tte);
    return npv * NormalPDF(CalculateD1(strike, tte, u, vol)) / (u * vol * std::sqrt(tte));
}


inline double CalculateVega(double strike, double tte, 
                            double interest_rate, double u, double vol)
{
    double u_npv = u * NPVAdjust(interest_rate, tte);
    return u_npv * std::sqrt(tte) * NormalPDF(CalculateD1(strike, tte, u, vol));
}


inline double CalculateImpliedVol(int16_t option_type, double strike, double tte, 
                                  double interest_rate, double u, double price,
                                  double seed_vol, double tick_threshold, uint64_t iterations)
{
    if (std::isnan(strike)
        || std::isnan(tte)
        || std::isnan(interest_rate)
        || std::isnan(u)
        || std::isnan(price)
        || std::isnan(seed_vol)
        || std::isnan(tick_threshold)
        || LT_EPSILON(price, 0, ALMOST_ZERO) 
        || LT_EPSILON(seed_vol, 0, ALMOST_ZERO) 
        || LTE_EPSILON(tick_threshold, 0, ALMOST_ZERO)) {
        return std::numeric_limits<double>::quiet_NaN();
    } else if (EQ_EPSILON(price, 0, ALMOST_ZERO) || EQ_EPSILON(seed_vol, 0, ALMOST_ZERO)) {
        return 0;
    }
    double dp = 0, vol = seed_vol;
    for (uint64_t i = 0; i < iterations; ++i) {
        dp = price - CalculateTV(option_type, strike, tte, interest_rate, u, vol);
        if (LTE_EPSILON(std::abs(dp), tick_threshold, ALMOST_ZERO)) {
            return vol;
        }
        vol += dp / CalculateVega(strike, tte, interest_rate, u, vol);
    }
    return std::numeric_limits<double>::quiet_NaN();
}


struct OptionImpliedValues {
    double moneyness = std::numeric_limits<double>::quiet_NaN();
    double vol = std::numeric_limits<double>::quiet_NaN();
    double delta  = std::numeric_limits<double>::quiet_NaN();
    double gamma = std::numeric_limits<double>::quiet_NaN();
    double vega = std::numeric_limits<double>::quiet_NaN();
};


class Option
{
private:
    bool initialized = false;
    double strike = std::numeric_limits<double>::quiet_NaN();
    double interest_rate = std::numeric_limits<double>::quiet_NaN();
    double tte = std::numeric_limits<double>::quiet_NaN();
    double last_u = std::numeric_limits<double>::quiet_NaN();
    int16_t option_type = 0;
public:
    OptionImpliedValues implied;
    Option(int16_t t_option_type, double t_strike, double t_tte, double t_interest_rate);
    Option() = default;
    void Init(int16_t t_option_type, double t_strike, double t_tte, double t_interest_rate);
    bool Initialized() const;
    void CalculateImpliedValues(double price, double u);
};


class OptionPair
{
private:
    bool initialized = false;
    double strike = std::numeric_limits<double>::quiet_NaN();
    double tte = std::numeric_limits<double>::quiet_NaN();
    double interest_rate = std::numeric_limits<double>::quiet_NaN();
public:
    double u_bid = std::numeric_limits<double>::quiet_NaN();
    double u_ask = std::numeric_limits<double>::quiet_NaN();
    Option call_bid, call_ask, put_bid, put_ask;
    OptionPair(double t_strike, double t_tte, double t_interest_rate);
    OptionPair() = default;
    void Init(double t_strike, double t_tte, double t_interest_rate);
    bool Initialized() const;
    void UpdateUnderlying(double t_u_bid, double t_u_ask);
    int16_t WhichOTM() const;
    void CalculateOTMImpliedValues(double bid_price, double ask_price, bool as_market_taker);
    OptionImpliedValues* ImpliedBidPtr();
    OptionImpliedValues* ImpliedAskPtr();
};


class OptionResampler : public Option
{
    MeanValueResampler price_resampler, vol_resampler;
    bool initialized = false;
public:
    OptionResampler(int16_t t_option_type, double t_strike, double t_tte, double t_interest_rate, 
                    uint64_t resampling_interval);
    OptionResampler() = default;
    void Init(int16_t t_option_type, double t_strike, double t_tte, double t_interest_rate, 
              uint64_t resampling_interval);
    void Record(uint64_t time, double price, double u);
    void ForceSamplesAround(uint64_t start_time, uint64_t end_time);
    uint64_t NumSamples() const;
    int8_t CopyPricesToArray(double *dst_arr, uint64_t size);
    int8_t CopyVolsToArray(double *dst_arr, uint64_t size);
    int8_t CopyPriceSamplesToArray(Sample *dst_arr, uint64_t size);
    int8_t CopyVolSamplesToArray(Sample *dst_arr, uint64_t size);
    uint64_t FirstSampleTime() const;
    uint64_t LastSampleTime() const;
    MeanValueResampler* PriceMVRPtr();
    MeanValueResampler* VolMVRPtr();
};

/*
template <class KeyType>
class OptionsManager
{
private:
    bool initialized_;
    double tte_, interest_rate_;
    std::unordered_map<KeyType, Option> options;
public:
    OptionsManager() :
        initialized_(false),
        tte_(std::numeric_limits<double>::quiet_NaN()),
        interest_rate_(std::numeric_limits<double>::quiet_NaN())
    {
        
    }

    void Init(double tte, double interest_rate, uint32_t num_reserved)
    {
        tte_ = tte;
        interest_rate_ = interest_rate;
        options.reserve(num_reserved);
        initialized_ = true;
    }

    void AddOption(KeyType key, int16_t option_type, double strike)
    {
        if (!initialized_ || options.find(key) != options.end()) {
            return;
        }
        Option &option = options[key];
        option.Init(option_type, strike, tte_, interest_rate_);
    }

    double CalculateImpliedVol(KeyType key, double u, double price)
    {
        Option &option = options[key];
        option.CalculateImplied(u, price);
        return option.implied.vol;
    }
};


using IntKeyOptionsManager = OptionsManager<int64_t>;


struct OptionContractPrices
{
    double call, put;
    OptionContractPrices();
};


// assume keys int64_t
class RollManager
{
private:
    bool initialized_;
    MovingAverageWithEntropy ma_;
    std::unordered_set<int64_t> sampled_strikes_;
    std::unordered_map<int64_t, OptionContractPrices> option_prices_;
public:
    RollManager();
    void Init(uint64_t time_window, double half_life, double strike_boundary);
};
*/

/*
class OptionVolatilitySampler
{
private:
    bool initialized_;
    uint64_t time_window_;
    TimeMovingAverage mma_, vma_;
public:
    OptionVolatilitySampler() :
        initialized_(false),
        time_window_(0)
    {
        
    }
    
    void Init(uint64_t time_window)
    {
        time_window_ = time_window;
        mma_.Init(time_window_);
        vma_.Init(time_window_);
        initialized_ = true;
    }
    
    void AddData(uint64_t time, double moneyness, double vol)
    {
        mma_.AddData(time, moneyness);
        vma_.AddData(time, vol);
    }
    
    double Moneyness() const
    {
        return mma_.Value();
    }
    
    double Vol() const
    {
        return vma_.Value();
    }
    
    double Volatility() const
    {
        return Vol();
    }
};
*/




#endif // OPTIONS_HANDLING_H
