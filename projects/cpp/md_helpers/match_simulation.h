#include <algorithm>
#include <cstdint>
#include <deque>
#include <iostream>
#include <limits>
#include <numeric>
#include <ostream>
#include <string>
#include <map>
#include <memory>
#include <unordered_set>
#include <unordered_map>
#include <vector>

#include "book_building.h"

using std::string;

const double ZERO_BYPASS_PCT = 0;

const int16_t SIMULATED_ORDER_CANNOT_MATCH = -1;

enum class OrderType {
    Limit = 0,
    FAK,
    GhostFAK,
    GhostLimit
};

std::ostream& operator<<(std::ostream &out, const OrderType &order_type);

// use bypass for unresolved orders
enum class AllowBypass {
    No = 0,
    Yes
};

// actual trade?
enum class ActualTrade {
    No = 0,
    Yes
};

// placing new SimulatedOrder
enum class NewOrderCondition {
    Good = 0, 
    Unresolved,
    NotReal
};

enum class IsReal {
    No = 0,
    Yes
};

// SimulatedFill conditions
enum class FillCondition {
    FullRestingTradeEvent = 1,
    PartRestingTradeEvent,
    FullRestingMarketSizeIncrease,
    PartRestingMarketSizeIncrease,
    FullMarketCollision,
    PartMarketCollision,
    FullWash,
    PartWash
};

std::ostream& operator<<(std::ostream &out, const FillCondition &condition);

class MarketState;
class BasicOrder;
class OrderQueuer;
class SimulatedOrderCollection;
class SimulatedMatchingEngine;

class QuantityState;
using PQPair = std::pair<int32_t, QuantityState>;
using BidQtyMap = std::map<int32_t, QuantityState, std::greater<int32_t>>;
using AskQtyMap = std::map<int32_t, QuantityState, std::less<int32_t>>;

using PQHashMap = std::unordered_map<int32_t, int32_t>;

class SimulatedFill;
using SimulatedFillPtr = std::unique_ptr<SimulatedFill>;
using FillQueue = std::deque<SimulatedFillPtr>;

class SimulatedOrderAck;
using OrderAckQueue = std::deque<SimulatedOrderAck>;

class SimulatedOrder;
using SimulatedOrderPtr = std::unique_ptr<SimulatedOrder>;

using OrderQueue = std::deque<SimulatedOrderPtr>;
using OrderBidMap = std::map<int32_t, OrderQueue, std::greater<int32_t>>;
using OrderAskMap = std::map<int32_t, OrderQueue, std::less<int32_t>>;
using OrderIDMap = std::unordered_map<uint64_t, int32_t>;


class QuantityState
{
private:
    int32_t prev = 0, curr = 0;
public:
    explicit QuantityState(int32_t);
    ~QuantityState() = default;
    QuantityState() = delete;
    QuantityState(const QuantityState &other) = default;
    QuantityState& operator=(const QuantityState &other) = default;
    QuantityState(QuantityState &&other) = default;
    QuantityState& operator=(QuantityState &&other) = default;
    void Update(int32_t);
    int32_t Current() const;
    int32_t Previous() const;
};

std::ostream& operator<<(std::ostream&, const QuantityState&);


class MarketState
{
private:
    bool initialized_ = false;
    PQHashMap bid_adjust, ask_adjust;
    void RemoveAdjustQtysFrom(uint16_t side, int32_t top_price);
    void TradeReconcileAdjustBackBy(uint16_t side, int32_t trade_price, int32_t &trade_volume);
public:
    bool improved_bid = false, improved_ask = false;
    uint64_t seqnum = 0, time = 0;
    int32_t top_bid = std::numeric_limits<int32_t>::min();
    int32_t top_ask = std::numeric_limits<int32_t>::max();
    int32_t buyprice = 0, buyvolume = 0, sellprice = 0, sellvolume = 0;
    std::vector<BookLevel> bids, asks;
    MarketState() = default;
    ~MarketState() = default;
    MarketState(const MarketState &other) = delete;
    MarketState& operator=(const MarketState &other) = delete;
    MarketState(MarketState &&other) = delete;
    MarketState& operator=(MarketState &&other) = delete;
    void Update(const MDOrderBook &md, bool use_exch_time);
    bool Initialized() const;
    void AdjustQtyBy(uint16_t side, int32_t price, int32_t qty);
    int32_t QtyAt(uint16_t side, int32_t price) const;
    int32_t RealQtyAt(uint16_t side, int32_t price) const;
    int32_t AdjustedQtyAt(uint16_t side, int32_t price) const;
    bool HasPrice(uint16_t side, int32_t price) const;
    int32_t AggregateQtyAt(uint16_t side, int32_t price) const;
    void ReduceAggregateQtyBy(uint16_t side, int32_t price, int32_t qty);
    BookLevel& FindBestLevel(uint16_t side, BookLevel &level);
    int32_t WorstBid() const;
    int32_t WorstAsk() const;
    bool IsWorseThanMarketShown(uint16_t side, int32_t price) const;
};


class SimulatedFill
{
    const FillCondition condition_;
    const int32_t price_, order_price_, qty_;
    int32_t best_eval_price_, best_eval_qty_ = 0, match_ticks_ = 0, order_ticks_ = 0;
    const uint16_t side_;
    const uint64_t id_, last_position_, orderid_, secid_, seqnum_, time_;
    uint64_t best_eval_timedelta_ = 0, best_eval_seqnum_ = 0;
public:
    explicit SimulatedFill(uint64_t secid, int32_t price, int32_t order_price, uint16_t side, int32_t qty,
                           int32_t last_position, const MarketState &mkt_state, 
                           FillCondition condition, uint64_t orderid);
    ~SimulatedFill() = default;
    SimulatedFill() = delete;
    SimulatedFill(const SimulatedFill &other) = delete;
    SimulatedFill& operator=(const SimulatedFill &other) = delete;
    SimulatedFill(SimulatedFill &&other) = default;
    SimulatedFill& operator=(SimulatedFill &&other) = default;
    uint64_t Time() const;
    void UpdateBestMarket(const MarketState &mkt_state);
    uint64_t BestEvalSeqnum() const;
    friend std::ostream& operator<<(std::ostream &out, const SimulatedFill &fill);
};

std::ostream& operator<<(std::ostream&, const SimulatedFill&);


class QueuePosition
{
public:
    int32_t curr = std::numeric_limits<int32_t>::max();
    int32_t prev = std::numeric_limits<int32_t>::max();
    int32_t first = -1;
    QueuePosition() = default;
    ~QueuePosition() = default;
    QueuePosition(const QueuePosition &other) = delete;
    QueuePosition(QueuePosition &&other) = delete;
    QueuePosition& operator=(const QueuePosition &other) = delete;
    QueuePosition& operator=(QueuePosition &&other) = delete;
    int32_t Update(int32_t);
};

std::ostream& operator<<(std::ostream&, const QueuePosition&);


class BasicOrder
{
public:
    const OrderType order_type;
    const double bypass_market_pct;
    const std::string strategy_name, context;
    //const char *strategy_name;
    //const char *context;
    const uint16_t side;
    const uint64_t client_orderid, trigger_time, trigger_seqnum, secid;
    const int32_t price, qty;
    explicit BasicOrder(uint64_t t_secid, uint64_t client_orderid, const char *t_strategy_name, uint64_t t_trigger_time,
                        uint64_t t_trigger_seqnum, int32_t t_price,
                        uint16_t t_side, int32_t t_qty, OrderType t_order_type, const char *t_context, 
                        double bypass_market_pct);
    /*
    BasicOrder() = delete;
    ~BasicOrder() = default;
    BasicOrder(const BasicOrder &other);
    BasicOrder(BasicOrder &&other) = delete;
    BasicOrder& operator=(const BasicOrder &other) = delete;
    BasicOrder& operator=(BasicOrder &&other) = delete;
    */
};

std::ostream& operator<<(std::ostream&, const BasicOrder&);

using PriceOrderIDs = std::unordered_map<int32_t, std::unordered_set<uint64_t>>;

class OrderQueuer
{
    const uint64_t id;
    uint64_t client_orderid = 0;
    std::deque<BasicOrder> orders;
    std::unordered_map<uint64_t, BasicOrder> prepared_limit_orders;
    std::unordered_map<uint64_t, PriceOrderIDs> bid_limit_orders, ask_limit_orders;
    OrderAckQueue acks;
    uint64_t PrepareOrder(int64_t secid, const char *strategy_name, uint64_t trigger_time,
                      uint64_t trigger_seqnum, int32_t price,
                      uint16_t side, int32_t qty, OrderType order_type, 
                      const char *context, double bypass_market_pct);
public:
    OrderQueuer();
    ~OrderQueuer();
    OrderQueuer(const OrderQueuer& other) = delete;
    OrderQueuer(OrderQueuer&& other) = delete;
    OrderQueuer& operator=(const OrderQueuer& other) = delete;
    OrderQueuer& operator=(OrderQueuer&& other) = delete;
    uint64_t PrepareLimitOrder(int64_t secid, const char *strategy_name, uint64_t trigger_time,
                               uint64_t trigger_seqnum, int32_t price,
                               uint16_t side, int32_t qty, const char *context);
    uint64_t PrepareFAKOrder(int64_t secid, const char *strategy_name, uint64_t trigger_time,
                             uint64_t trigger_seqnum, int32_t price,
                             uint16_t side, int32_t qty, const char *context);
    uint64_t PrepareSprintOrder(int64_t secid, const char *strategy_name, uint64_t trigger_time,
                                uint64_t trigger_seqnum, int32_t price,
                                uint16_t side, int32_t qty, const char *context, double bypass_market_pct);
    uint64_t PrepareGhostFAKOrder(int64_t secid, const char *strategy_name, uint64_t trigger_time,
                                  uint64_t trigger_seqnum, int32_t price,
                                  uint16_t side, int32_t qty, const char *context);
    uint64_t PrepareGhostLimitOrder(int64_t secid, const char *strategy_name, uint64_t trigger_time,
                                    uint64_t trigger_seqnum, int32_t price,
                                    uint16_t side, int32_t qty, const char *context);
    uint64_t PrepareGhostSprintOrder(int64_t secid, const char *strategy_name, uint64_t trigger_time,
                                     uint64_t trigger_seqnum, int32_t price,
                                     uint16_t side, int32_t qty, const char *context, double bypass_market_pct);
    const std::deque<BasicOrder>& Orders() const;
    uint64_t ID() const;
    bool HasOrders() const;
    void ClearOrders();
    void ReceiveAck(SimulatedOrderAck &ack);
    bool HasAcks() const;
    SimulatedOrderAck TopAck();
    void RemoveAck();
    uint64_t CurrentClientOrderID() const;
};


class SimulatedOrder
{
    QueuePosition queue_position_;
    // trigger and basic order info
    bool active_ = false;
    const OrderType order_type_;
    const double bypass_market_pct_;
    const int32_t qty_, price_;
    int32_t remaining_qty_;
    const uint16_t side_;
    uint64_t id_;
    uint64_t num_fills_ = 0;
    const uint64_t secid_, me_id_, origin_id_, client_orderid_;
    mutable uint64_t trigger_time_, place_time_ = 0, cancel_time_ = 0;
    mutable uint64_t trigger_seqnum_, place_seqnum_ = 0, cancel_seqnum_ = 0;
    const string strategy_name_, context_;
public:
    
    explicit SimulatedOrder(uint64_t me_id, uint64_t origin_id, uint64_t client_orderid, 
                            string strategy_name, uint64_t trigger_time,
                            uint64_t trigger_seqnum, uint64_t secid, int32_t price,
                            uint16_t side, int32_t qty, OrderType order_type, string context, 
                            double bypass_market_pct);
    ~SimulatedOrder() = default;
    SimulatedOrder() = delete;
    SimulatedOrder(const SimulatedOrder &other) = delete;
    SimulatedOrder& operator=(const SimulatedOrder &other) = delete;
    SimulatedOrder(SimulatedOrder &&other) = delete;
    SimulatedOrder& operator=(SimulatedOrder &&other) = delete;
    bool IsValid() const;
    int32_t FirstPosition() const;
    int32_t LastPosition() const;
    void ModifyQueue(int32_t);
    void AddPlaceInfo(uint64_t time, uint64_t seqnum);
    void AddCancelInfo(uint64_t time, uint64_t seqnum);
    int32_t MatchToMarket(int32_t market_price, int32_t filled_qty, 
                          const MarketState &mkt_state, FillCondition condition, 
                          FillQueue &fills);
    void Deactivate();
    int32_t RemainingQty() const;
    uint64_t ID() const;
    int32_t Price() const;
    uint16_t Side() const;
    bool IsFAK() const;
    const std::vector<SimulatedFill>& Fills() const;
    uint64_t TriggerTime() const;
    int32_t BypassMarketBy(int32_t mkt_qty) const;
    bool CanPlaceImmediately() const;
    OrderType Type() const;
    bool IsRealType() const;
    uint64_t NumFills() const;
    bool IsActive() const;
    uint64_t OriginID() const;
    uint64_t ClientOrderID() const;
    uint64_t MEID() const;
    friend std::ostream& operator<<(std::ostream& out, const SimulatedOrder &order);
};

std::ostream& operator<<(std::ostream&, const SimulatedOrder&);


struct SimulatedOrderAck
{
    bool active;
    uint64_t orderid, client_orderid, origin_id, me_id, num_fills;
    int64_t remaining_qty, last_position;
    SimulatedOrderAck() = default;
    ~SimulatedOrderAck() = default;
    SimulatedOrderAck(const SimulatedOrder &order);
    SimulatedOrderAck(const SimulatedOrderAck &ack) = default;
    SimulatedOrderAck& operator=(const SimulatedOrderAck &ack) = default;
    SimulatedOrderAck(SimulatedOrderAck &&ack) = default;
    SimulatedOrderAck& operator=(SimulatedOrderAck &&ack) = default;
    friend std::ostream& operator<<(std::ostream& out, const SimulatedOrderAck &ack);
};

std::ostream& operator<<(std::ostream&, const SimulatedOrderAck&);


class SimulatedOrderCollection
{
private:
    bool initialized_, use_exch_time_;
    uint64_t secid_, num_orders_placed_, num_fills_;
    FillQueue neglected_fills_;
    OrderAckQueue neglected_acks;
    void UpdateUnresolvedOrders();
    int32_t UpdateQueueFromTrades(int32_t traded_qty, ActualTrade actual_trade, 
                                  OrderQueue &order_queue, IsReal is_real);
    bool OrderFullyCollided(SimulatedOrder &order);
    NewOrderCondition UpdateQueueNewOrder(SimulatedOrder &order);
    void UpdateQueuePosition();
    void MatchOrders(OrderBidMap &bid_orders, OrderAskMap &ask_orders, IsReal is_real);
public:
    MarketState mkt_state;
    OrderBidMap bid_orders, unresolved_bid_orders, ghost_bid_orders;
    OrderAskMap ask_orders, unresolved_ask_orders, ghost_ask_orders;
    OrderQueue inactive_orders;
    OrderIDMap order_prices;
    ~SimulatedOrderCollection();
    SimulatedOrderCollection() = default;
    SimulatedOrderCollection(const SimulatedOrderCollection& other) = delete;
    SimulatedOrderCollection& operator=(const SimulatedOrderCollection& other) = delete;
    SimulatedOrderCollection(SimulatedOrderCollection &&other) = delete;
    SimulatedOrderCollection& operator=(SimulatedOrderCollection &&other) = delete;
    void Init(uint64_t secid, bool use_exch_time);
    void TakeOrder(SimulatedOrderPtr order_ptr, uint64_t time, uint64_t seqnum);
    bool RemoveOrder(uint64_t id);
    void Update(const MDOrderBook &md);
    uint64_t NumOrdersAt(uint16_t side, int32_t price) const;
    uint64_t NumOrdersPlaced() const;
    uint64_t NumFills() const;
    bool HasAcks() const;
    SimulatedOrderAck& TopAck();
    void RemoveAck();
    void TransferFillsTo(FillQueue &fills);
    uint64_t SecID() const;
};


class SimulatedMatchingEngine
{
private:
    bool initialized_, use_exch_time_;
    uint64_t id_, latency_, evaluate_timedelta_;
    std::unordered_map<uint64_t, FillQueue> fills_;
    FillQueue evaluated_fills_;
    std::unordered_map<uint64_t, SimulatedOrderCollection> order_collections_;
    std::unordered_map<uint64_t, OrderQueue> orders_prematch;
    std::vector<OrderQueuer*> order_queuers;
    void EnqueueOrder(uint64_t origin_id, uint64_t secid, uint64_t client_orderid,
                      const string &strategy_name, uint64_t trigger_time,
                      uint64_t trigger_seqnum, int32_t price,
                      uint16_t side, int32_t qty, OrderType order_type, const string &context, 
                      double bypass_market_pct);
    void ReceiveAcksFrom(SimulatedOrderCollection &collection);
    void ReceiveFillsFrom(SimulatedOrderCollection &collection);
public:
    explicit SimulatedMatchingEngine(uint64_t id, uint64_t latency, bool use_exch_time, uint64_t evaluated_timedelta);
    SimulatedMatchingEngine() = default;
    ~SimulatedMatchingEngine();
    SimulatedMatchingEngine(const SimulatedMatchingEngine &other) = delete;
    SimulatedMatchingEngine& operator=(const SimulatedMatchingEngine &other) = delete;
    SimulatedMatchingEngine(SimulatedMatchingEngine &&other) = delete;
    SimulatedMatchingEngine& operator=(SimulatedMatchingEngine &&other) = delete;
    void Init(uint64_t id, uint64_t latency, bool use_exch_time, uint64_t evaluated_timedelta);
    void AddSecurity(uint64_t secid);
    void ReadMD(const MDOrderBook * const md_ptr);
    void EnqueueOrdersFrom(OrderQueuer *oq);
    const SimulatedOrderCollection& operator[](uint64_t secid) const;
    bool ValidSecID(uint64_t secid) const;
    void EvaluateFills(uint64_t secid);
};

