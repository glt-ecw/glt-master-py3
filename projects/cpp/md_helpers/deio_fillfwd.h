#ifndef DEIO_FILLFWD_H
#define DEIO_FILLFWD_H


#include <cmath>
#include <cstring>
#include <string>
#include <unordered_map>
#include <unordered_set>

#include "order_entry.h"

/*
 * oss << "FILL,"
                << fill->id() << ","
                << ++_fillFwdSeqNum << ","
                << fill->seqNum() << ","
                << fill->fillType() << ","
                << Order::sideStr(fill->side()) << ","
                << fill->securityId() << ","
                << fill->userId() << ","
                << fill->serverId() << ","
                << fill->price() << ","
                << fill->quantity() << ","
                << fill->exchTradeId() << ","
                << fill->orderTypeStr() << ","
                << fill->fillTime().toStringLocal() << ","
                << fill->assumedFillType();

struct AssumedFill {
        enum Type {
                AF_NORMAL = 0, // Normal fill
                AF_ASSUMED, // Assumed fill
                AF_LATE, // Real fill that came in after an assumed fill

                MaxValue
        };
};
*/ 
// FILL,1,176500042,3,1,offer,515134,11,94,20040,1,b6d23b00:44356d5f,Manual,20170726T002402.931204,1

const uint16_t DEIO_FILL_TAG = 0;
const uint16_t DEIO_FILL_FWD_SEQNUM = 1;
const uint16_t DEIO_FILL_DEIOID = 2;
const uint16_t DEIO_FILL_SEQNUM = 3;
const uint16_t DEIO_FILL_FILL_TYPE = 4;
const uint16_t DEIO_FILL_SIDE = 5;
const uint16_t DEIO_FILL_SECID = 6;
const uint16_t DEIO_FILL_USERID = 7;
const uint16_t DEIO_FILL_SERVERID = 8;
const uint16_t DEIO_FILL_PRICE = 9;
const uint16_t DEIO_FILL_QTY = 10;
const uint16_t DEIO_FILL_EXCH_TRADE_ID = 11;
const uint16_t DEIO_FILL_ORDER_TYPE = 12;
const uint16_t DEIO_FILL_FILL_TIME = 13;
const uint16_t DEIO_FILL_ASSUMED_FILL_TYPE = 14;

const std::string DEIO_FILL_ORDER_TYPE_STR_MANUAL = "Manual";
const std::string DEIO_FILL_ORDER_TYPE_STR_HEDGE = "Hedge";
const std::string DEIO_FILL_ORDER_TYPE_STR_AUTOQUOTE = "AutoQuote";

const uint16_t DEIO_FILL_ORDER_TYPE_UNSPECIFIED = 0;
const uint16_t DEIO_FILL_ORDER_TYPE_MANUAL = 1;
const uint16_t DEIO_FILL_ORDER_TYPE_HEDGE = 4;
const uint16_t DEIO_FILL_ORDER_TYPE_AUTOQUOTE = 5;

/*
struct DeioFill
{
    uint64_t deioid, fwd_seqnum, seqnum;
    uint16_t fill_type, side;
    uint64_t secid;
    uint16_t userid, serverid;
    double price;
    int32_t qty, remaining_qty;
    std::string exch_trade_id, fill_time;
    uint16_t order_type;
    uint16_t assumed_fill_type;
};
*/

class DeioFillFWDReader 
{
private:
    bool ignore_fill = false;
    uint64_t fwd_seqnum = 0;
    //std::unordered_map<uint64_t, std::unordered_set<uint16_t>> secid_order_types;
public:
    DeioFill fill;
    DeioFillFWDReader() :
        //secid_order_types(),
        fill()
    {}
    
    //void AddSecID(uint64_t secid, uint16_t order_type) 
    //{
    //    secid_order_types[secid].insert(order_type);
    //}
    
    bool IgnoreFill() const
    {
        return ignore_fill;
    }
    
    void Process(const char *str, uint64_t length)
    {
        char *copied = new char[length+1];
        std::strcpy(copied, str);
        ignore_fill = false;
        char *p = std::strtok(copied, ",");
        uint64_t count = 0;
        while (p != nullptr && !ignore_fill) {
            switch (count) {
                case DEIO_FILL_FWD_SEQNUM:
                {
                    uint64_t seqnum = static_cast<uint64_t>(std::atoi(p));
                    if (seqnum <= fwd_seqnum) {
                        ignore_fill = true;
                    } else {
                        fwd_seqnum = seqnum;
                        fill.fwd_seqnum = fwd_seqnum;
                    }
                    break;
                }
                case DEIO_FILL_DEIOID:
                {
                    fill.deioid = static_cast<uint64_t>(std::atoi(p));
                    break;
                }
                case DEIO_FILL_SEQNUM:
                {
                    fill.seqnum = static_cast<uint64_t>(std::atoi(p));
                    break;
                }
                case DEIO_FILL_FILL_TYPE:
                {
                    fill.fill_type = static_cast<uint16_t>(std::atoi(p));
                    break;
                }
                case DEIO_FILL_SIDE:
                {
                    fill.side = p[0] == 'b' ? 1 : 2;
                    break;
                }
                case DEIO_FILL_SECID:
                {
                    fill.secid = static_cast<uint64_t>(std::atoi(p));
                    //ignore_fill = secid_order_types.find(fill.secid) == secid_order_types.end();
                    break;
                }
                case DEIO_FILL_USERID:
                {
                    fill.userid = static_cast<uint16_t>(std::atoi(p));
                    break;
                }
                case DEIO_FILL_SERVERID:
                {
                    fill.serverid = static_cast<uint16_t>(std::atoi(p));
                    break;
                }
                case DEIO_FILL_PRICE:
                {
                    fill.price = std::atof(p);
                    break;
                }
                case DEIO_FILL_QTY:
                {
                    fill.qty = static_cast<int32_t>(std::round(std::atof(p)));
                    break;
                }
                case DEIO_FILL_EXCH_TRADE_ID:
                {
                    fill.exch_trade_id = std::string(p);
                    break;
                }
                case DEIO_FILL_ORDER_TYPE:
                {
                    std::string order_type_str(p);
                    if (order_type_str == DEIO_FILL_ORDER_TYPE_STR_MANUAL) {
                        fill.order_type = DEIO_FILL_ORDER_TYPE_MANUAL;
                    } else if (order_type_str == DEIO_FILL_ORDER_TYPE_STR_HEDGE) {
                        fill.order_type = DEIO_FILL_ORDER_TYPE_HEDGE;
                    } else if (order_type_str == DEIO_FILL_ORDER_TYPE_STR_AUTOQUOTE) {
                        fill.order_type = DEIO_FILL_ORDER_TYPE_AUTOQUOTE;
                    } else {
                        fill.order_type = DEIO_FILL_ORDER_TYPE_UNSPECIFIED;
                    }
                    //const auto &order_types = secid_order_types.at(fill.secid);
                    //ignore_fill = order_types.find(fill.order_type) == order_types.end();
                    break;
                }
                case DEIO_FILL_FILL_TIME:
                {
                    fill.fill_time = std::string(p);
                    break;
                }
                case DEIO_FILL_ASSUMED_FILL_TYPE:
                {
                    fill.assumed_fill_type = static_cast<uint16_t>(std::atoi(p));
                    break;
                }
                default:
                {
                    break;
                }
            }
            p = std::strtok(NULL, ",");
            ++count;
        }
        delete[] copied;
    }
};

#endif