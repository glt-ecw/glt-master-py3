/*
 * Copyright (c) 2016 <copyright holder> <email>
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 */

#ifndef ACCUMULATOR_H
#define ACCUMULATOR_H

#include <cstdint>
#include <deque>
#include <limits>


template <class DataType>
class DataEvent 
{
public:
    uint64_t time;
    DataType value;
};


template <class DataType, class AccumulatedType>
class Accumulator 
{
private:
    std::deque<DataEvent<DataType>> data_;
    AccumulatedType value_;
public:
    Accumulator() :
        value_(0)
    {
        DataType dat(0);
        dat /= 1;
        value_ /= 1;
    }
    
    ~Accumulator() 
    {
        
    }
    
    AccumulatedType Push(const DataEvent<DataType> &dat)
    {
        data_.push_back(dat);
        value_ += dat.value;
        return value_;
    }
    
    AccumulatedType Pop() 
    {
        if (Size() == 0) {
            return 0;
        }
        DataEvent<DataType> &dat = data_.front();
        value_ -= dat.value;
        data_.pop_front();
        return value_;
    }
    
    AccumulatedType Value() const 
    {
        return value_;
    }
    
    uint64_t Size() const 
    {
        return data_.size();
    }
    
    DataEvent<DataType> Front() const 
    {
        return data_.front();
    }
    
    DataEvent<DataType> Back() const
    {
        return data_.back();
    }
    
    void Reset(AccumulatedType value) 
    {
        data_.clear();
        value_ = value;
    }
    
    void Reset()
    {
        Reset(0);
    }
    
    const std::deque<DataEvent<DataType>>& AsQueue() const
    {
        return data_;
    }
};


template <class DataType, class AccumulatedType>
class TimeWindowAccumulator
{
protected:
    bool initialized_, saturated_;
    uint64_t time_window_;
    Accumulator<DataType, AccumulatedType> data_;
    AccumulatedType value_;
public:
    TimeWindowAccumulator() :
        initialized_(false),
        saturated_(false),
        value_(0)
    {
        
    }
    
    ~TimeWindowAccumulator() {
        
    }
    
    void Init(uint64_t time_window) {
        Clear();
        value_ = 0;
        time_window_ = time_window;
        initialized_ = true;
    }
    
    AccumulatedType AddData(uint64_t time, const DataType &val) {
        if (!initialized_) {
            return 0;
        }
        bool removed = RemoveData(time);
        if (!saturated_ && removed) {
            saturated_ = true;
        }
        DataEvent<DataType> evt;
        evt.time = time;
        evt.value = val;
        value_ = data_.Push(evt);
        return value_;
    }
    
    DataType FirstEventValue() const
    {
        return data_.Front().value;
    }
    
    uint64_t FirstEventTime() const
    {
        return data_.Front().time;
    }
    
    DataType LastEventValue() const
    {
        return data_.Back().value;
    }
    
    uint64_t LastEventTime() const
    {
        return data_.Back().time;
    }
    
    bool RemoveData(uint64_t time) {
        bool removed = false;
        while (Size() > 0 && time >= FirstEventTime() && time - FirstEventTime() >= time_window_) {
            data_.Pop();
            removed = true;
        }
        return removed;
    }
    
    AccumulatedType Value() const {
        return value_;
    }
    
    void Clear(AccumulatedType value) {
        data_.Reset();
        value_ = value;
    }
    
    void Clear() {
        Clear(0);
    }
    
    uint64_t TimeWindow() const {
        return time_window_;
    }
    
    uint64_t Size() const {
        return data_.Size();
    }
    
    bool IsSaturated() const {
        return saturated_;
    }
};


template <class DataType, class AccumulatedType>
class NElementsAccumulator 
{
protected:
    bool initialized_, saturated_;
    uint64_t num_elements_;
    Accumulator<DataType, AccumulatedType> data_;
    AccumulatedType value_;
public:
    NElementsAccumulator() :
        initialized_(false),
        saturated_(false),
        value_(0)
    {
        
    }
    ~NElementsAccumulator() {
        
    }
    
    void Init(uint64_t num_elements) {
        Clear();
        value_ = 0;
        num_elements_ = num_elements;
        initialized_ = true;
    }
    
    AccumulatedType AddData(const DataType &val) {
        if (!initialized_) {
            return 0;
        }
        uint64_t size(data_.Size());
        if (size >= num_elements_) {
            data_.Pop();
            saturated_ = true;
        }
        DataEvent<DataType> evt;
        evt.value = val;
        value_ = data_.Push(evt);
        return value_;
    }
    
    DataType FirstEventValue() const
    {
        return data_.Front().value;
    }
    
    DataType LastEventValue() const
    {
        return data_.Back().value;
    }
    
    AccumulatedType Value() const {
        return value_;
    }
    
    void Clear() {
        data_.Reset();
        value_ = 0;
    }
    
    uint64_t NumElements() const {
        return num_elements_;
    }
    
    uint64_t Size() const {
        return data_.Size();
    }
    
    bool IsSaturated() const {
        return saturated_;
    }
};


using DoubleTWAcc = TimeWindowAccumulator<double, double>;
using DoubleNEAcc = NElementsAccumulator<double, double>;


class MovingAverage : public DoubleNEAcc
{
private:
    double ma_;
public:
    MovingAverage() :
        ma_(0)
    {
        
    }
    
    double AddData(double value) 
    {
        ma_ = DoubleNEAcc::AddData(value) / Size();
        return ma_;
    }
    
    double Value() const
    {
        return ma_;
    }
    
    void Clear()
    {
        DoubleNEAcc::Clear();
        value_ = std::numeric_limits<double>::quiet_NaN();
        ma_ = value_;
    }
};


class TimeMovingAverage : public DoubleTWAcc
{
private:
    double ma_;
public:
    TimeMovingAverage() :
        ma_(0)
    {
        
    }
    
    double AddData(uint64_t time, double value)
    {
        ma_ = DoubleTWAcc::AddData(time, value) / Size();
        return ma_;
    }
    
    double Value() const
    {
        return ma_;
    }
};

#endif // ACCUMULATOR_H
