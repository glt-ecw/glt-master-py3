#ifndef DEIO_MDFWD_H
#define DEIO_MDFWD_H

#include <cmath>
#include <cstdint>
#include <cstring>
#include <limits>
#include <vector>
#include <unordered_map>

#include "book_building.h"

#include <iostream>

//#include <iostream>

/*
 *     
struct FwdEvent {
                struct Header {
                        unsigned int            serverId;
                        unsigned int        securityId;
                        unsigned long       exchSeqNum;
                        unsigned long           deioTimeSec;
                        unsigned long           deioTimeNsec;

                        // Pack all the char fields together to save space.
                        unsigned char       eventType;
                        unsigned char       status;
                        unsigned char       lastTradeAggressorSide;
                        unsigned char       numLevels;

                        // Trade specific fields.
                        double              lastTradePrice;
                        double              lastTradeQty;
                        double              accumVol;

                        // Price change specific fields;
                        double              priceChangeBidPrice;
                        double              priceChangeOfferPrice;

                        double                          indicativePrice;

                        bool                            replay;
                };

        struct BookLevel {
            double          bidPrice;
            double          bidQty;
            double          offerPrice;
            double          offerQty;
            unsigned int    bidNumPart;
            unsigned int    offerNumPart;
        };

                const static size_t MaxLevels = 10;

                Header                          header;
                BookLevel                       levels[MaxLevels]; // There will only be header.numLevels of these.

        typedef boost::shared_ptr<FwdEvent> pointer;
        void reset() { memset(this, 0, sizeof(*this)); }
        unsigned int length() const { return sizeof(Header) + header.numLevels * sizeof(BookLevel); }
    };
 * 
 */


const uint64_t DEIO_MDFWD_HEADER_SERVER_ID = 0;
const uint64_t DEIO_MDFWD_HEADER_SECURITY_ID = 4;
const uint64_t DEIO_MDFWD_HEADER_SEQNUM = 8;
const uint64_t DEIO_MDFWD_HEADER_TIME_SEC = 16;
const uint64_t DEIO_MDFWD_HEADER_TIME_NANOS = 24;
const uint64_t DEIO_MDFWD_HEADER_EVENT_TYPE = 32;
const uint64_t DEIO_MDFWD_HEADER_STATUS = 33;
const uint64_t DEIO_MDFWD_HEADER_TRADE_DIRECTION = 34;
const uint64_t DEIO_MDFWD_HEADER_NUM_LEVELS = 35;
const uint64_t DEIO_MDFWD_HEADER_TRADE_PRICE = 40;
const uint64_t DEIO_MDFWD_HEADER_TRADE_QTY = 48;
const uint64_t DEIO_MDFWD_HEADER_TOTAL_VOLUME = 56;
const uint64_t DEIO_MDFWD_HEADER_BID_PRICE_CHANGE = 64;
const uint64_t DEIO_MDFWD_HEADER_ASK_PRICE_CHANGE = 72;
const uint64_t DEIO_MDFWD_HEADER_INDICATIVE = 80;
const uint64_t DEIO_MDFWD_HEADER_REPLAY = 88;

const uint64_t DEIO_MDFWD_LEVELS_START = 96;
const uint64_t DEIO_MDFWD_LEVEL_NUM_BYTES = 40;

const uint64_t DEIO_MDFWD_LEVELS_BID_PRICE = 0;
const uint64_t DEIO_MDFWD_LEVELS_BID_QTY = 8;
const uint64_t DEIO_MDFWD_LEVELS_ASK_PRICE = 16;
const uint64_t DEIO_MDFWD_LEVELS_ASK_QTY = 24;
const uint64_t DEIO_MDFWD_LEVELS_BID_PARTICIPANTS = 32;
const uint64_t DEIO_MDFWD_LEVELS_ASK_PARTICIPANTS = 36;

const uint64_t MAX_LEVELS = 10;


template <typename T>
T CopyBigEndian(char *bytes, uint64_t start) {
    size_t num_bytes = sizeof(T);
    char buf[num_bytes];
    for (size_t i=0; i<num_bytes; ++i) {
        buf[num_bytes - 1 - i] = bytes[start + i];
    }
    T value;
    std::memcpy(&value, buf, num_bytes);
    return value;
}


template <typename T>
T CopyLittleEndian(char *bytes, uint64_t start) {
    T value;
    std::memcpy(&value, bytes + start, sizeof(T));
    return value;
}


struct DeioMDFWDHeader 
{
        uint32_t server_id, security_id;
        uint64_t seqnum, time_sec, time_nanos;

        // Pack all the char fields together to save space.
        uint8_t event_type, status, trade_direction, num_levels;

        // Trade specific fields.
        double trade_price, trade_qty, total_volume;

        // Price change specific fields;
        double bid_price_change, ask_price_change;

        double indicative;

        bool replay;
};


struct DeioMDFWDBookLevel 
{
    double bid_price = 0, bid_qty = 0;
    double ask_price, ask_qty = 0;
    uint32_t bid_participants = 0, ask_participants = 0;
    void Clear()
    {
        bid_price = 0;
        bid_qty = 0;
        ask_price = 0;
        ask_qty = 0;
        bid_participants = 0;
        ask_participants = 0;
    }
    
    void CopyBytes(char *bytes, uint64_t start_at)
    {
        bid_price = CopyLittleEndian<double>(bytes, start_at + DEIO_MDFWD_LEVELS_BID_PRICE);
        bid_qty = CopyLittleEndian<double>(bytes, start_at + DEIO_MDFWD_LEVELS_BID_QTY);
        ask_price = CopyLittleEndian<double>(bytes, start_at + DEIO_MDFWD_LEVELS_ASK_PRICE);
        ask_qty = CopyLittleEndian<double>(bytes, start_at + DEIO_MDFWD_LEVELS_ASK_QTY);
        bid_participants = CopyLittleEndian<uint32_t>(bytes, start_at + DEIO_MDFWD_LEVELS_BID_PARTICIPANTS);
        ask_participants = CopyLittleEndian<uint32_t>(bytes, start_at + DEIO_MDFWD_LEVELS_ASK_PARTICIPANTS);
    }
};

template <typename T>
T DoubleTo(double value)
{
    if (std::isnan(value)) {
        return 0;
    } else {
        return static_cast<T>(value);
    }
}


class DeioMDFWDReader
{
    bool use_md = false;
    
    void CopyBytes(char *bytes)
    {
        header.server_id = CopyLittleEndian<uint32_t>(bytes, DEIO_MDFWD_HEADER_SERVER_ID);
        header.security_id = CopyLittleEndian<uint32_t>(bytes, DEIO_MDFWD_HEADER_SECURITY_ID);
        
        header.seqnum = CopyLittleEndian<uint64_t>(bytes, DEIO_MDFWD_HEADER_SEQNUM);
        header.time_sec = CopyLittleEndian<uint64_t>(bytes, DEIO_MDFWD_HEADER_TIME_SEC);
        header.time_nanos = CopyLittleEndian<uint64_t>(bytes, DEIO_MDFWD_HEADER_TIME_NANOS);
        
        header.event_type = CopyLittleEndian<uint8_t>(bytes, DEIO_MDFWD_HEADER_EVENT_TYPE);
        header.status = CopyLittleEndian<uint8_t>(bytes, DEIO_MDFWD_HEADER_STATUS);
        header.trade_direction = CopyLittleEndian<uint8_t>(bytes, DEIO_MDFWD_HEADER_TRADE_DIRECTION);
        header.num_levels = CopyLittleEndian<uint8_t>(bytes, DEIO_MDFWD_HEADER_NUM_LEVELS);
        
        header.trade_price = CopyLittleEndian<double>(bytes, DEIO_MDFWD_HEADER_TRADE_PRICE);
        header.trade_qty = CopyLittleEndian<double>(bytes, DEIO_MDFWD_HEADER_TRADE_QTY);
        header.total_volume = CopyLittleEndian<double>(bytes, DEIO_MDFWD_HEADER_TOTAL_VOLUME);
        
        header.bid_price_change = CopyLittleEndian<double>(bytes, DEIO_MDFWD_HEADER_BID_PRICE_CHANGE);
        header.ask_price_change = CopyLittleEndian<double>(bytes, DEIO_MDFWD_HEADER_ASK_PRICE_CHANGE);
        header.indicative = CopyLittleEndian<double>(bytes, DEIO_MDFWD_HEADER_INDICATIVE);
        
        header.replay = static_cast<bool>(CopyLittleEndian<uint8_t>(bytes, DEIO_MDFWD_HEADER_REPLAY));
        
        if (header.num_levels == 0 || header.num_levels > MAX_LEVELS) {
            // for now: if there is no book, do not process any header data
            // we may want to process just volume events, but Deio should generate
            // a book for us to use
            use_md = false;
            return;
        }
        
        uint64_t i=0;
        while (i < levels.size() && i < header.num_levels) {
            uint64_t start_at = DEIO_MDFWD_LEVELS_START + i * DEIO_MDFWD_LEVEL_NUM_BYTES;
            levels[i].CopyBytes(bytes, start_at);
            ++i;
        }
        while (i < levels.size()) {
            levels[i].Clear();
            ++i;
        }
        use_md = true;
    }
public:
    MDOrderBook md;
    DeioMDFWDHeader header;
    std::vector<DeioMDFWDBookLevel> levels;
    std::unordered_map<uint32_t, uint64_t> last_seqnums;
    std::unordered_map<uint32_t, double> total_volumes;
    int32_t indicative;
    DeioMDFWDReader() :
        levels(MAX_LEVELS)
    {
        md.Init(MAX_LEVELS);
        md.eop = 1;
    }
    
    ~DeioMDFWDReader() = default;
    
    void Process(char *bytes, double price_multiplier)
    {
        CopyBytes(bytes);
        if (!use_md) {
            return;
        }
        md.secid = header.security_id;
        md.time = header.time_sec * 1000000000 + header.time_nanos;
        md.seqnum = header.seqnum;
        // handle trades
        if (total_volumes.find(md.secid) == total_volumes.end()) {
            total_volumes[md.secid] = header.total_volume;
        }
        if (header.total_volume > total_volumes[md.secid]) {
            if (header.trade_direction == 2) {
                md.buyprice = header.trade_price;
                md.buyvolume = header.trade_qty;
                md.sellprice = 0;
                md.sellvolume = 0;
            } else if (header.trade_direction == 3) {
                md.buyprice = 0;
                md.buyvolume = 0;
                md.sellprice = header.trade_price;
                md.sellvolume = header.trade_qty;
            }
            total_volumes[md.secid] = header.total_volume;
        } else {
            md.buyprice = 0;
            md.buyvolume = 0;
            md.sellprice = 0;
            md.sellvolume = 0;
        }
        // now book levels
        for (uint64_t i=0; i<levels.size(); ++i) {
            DeioMDFWDBookLevel &level = levels[i];
            BookLevel &bid = md.bids[i];
            BookLevel &ask = md.asks[i];
            bid.price = DoubleTo<int32_t>(std::round(price_multiplier * level.bid_price));
            bid.qty = DoubleTo<int32_t>(level.bid_qty);
            bid.participants = DoubleTo<int32_t>(level.bid_participants);
            ask.price = DoubleTo<int32_t>(std::round(price_multiplier * level.ask_price));
            ask.qty = DoubleTo<int32_t>(level.ask_qty);
            ask.participants = DoubleTo<int32_t>(level.ask_participants);
        }
        indicative = DoubleTo<int32_t>(std::round(price_multiplier * header.indicative));
        last_seqnums[md.secid] = md.seqnum;
    }
    
    bool UseMD() const
    {
        return use_md;
    }
};

#endif // DEIO_MDFWD_H
