#ifndef PERF_H_
#define PERF_H_

#include <limits>
#include <cstdint>
#include "Maths.h"

class Perf0 { 
  
  double model_sumsq;
  double data_sumsq;
  double prod_sum;
  double prod_abs_sum;
  double err_sumsq;
  int64_t corr_dir_ct;
  int64_t nz_data_ct;
  int64_t ct; 
  //stats for when the model predicts a value greater than 1.0 
  int64_t model_gT_ct;
  int64_t model_data_NZ_gT_ct;
  int64_t model_gT_dir_ct;
  
  double gT_prod_sum;
  double gT_model_sumsq;
  double gT_data_sumsq;
  double gT_err_sumsq;

  double min_data;
  double max_data;
  double min_model;
  double max_model;
public:

  double T = 0.5;

  Perf0() {
    reset();
  }

  void reset() { 
    model_sumsq = 0;
    data_sumsq = 0;
    prod_sum = 0;
    prod_abs_sum = 0;
    err_sumsq = 0;
    corr_dir_ct = 0;
    nz_data_ct = 0;
    ct = 0;

    model_gT_ct = 0;
    model_data_NZ_gT_ct = 0;
    model_gT_dir_ct = 0;
    gT_prod_sum = 0;
    gT_model_sumsq = 0;
    gT_data_sumsq = 0;
    gT_err_sumsq = 0;
  
    min_data = std::numeric_limits<double>::max();
    min_model = std::numeric_limits<double>::max();
    max_data = std::numeric_limits<double>::lowest();
    max_model = std::numeric_limits<double>::lowest();
  }

  void add(double model_v, double data_v) {     
    if(model_v < min_model)
      min_model = model_v;
    if(model_v > max_model)
      max_model = model_v;
    if(data_v < min_data)
      min_data = data_v;
    if(data_v > max_data)
      max_data = data_v;
    model_sumsq += model_v * model_v;
    data_sumsq += data_v * data_v;
    double p = model_v * data_v;
    prod_sum += p; 
    prod_abs_sum += p >= 0.0 ? p : -p;
    err_sumsq += (model_v - data_v) * (model_v - data_v);
    corr_dir_ct += ((model_v * data_v) > 0.0 ? 1L : 0L);//(int64_t) Maths::Funcs::sgnZi(model_v * data_v);
    nz_data_ct += ((data_v != 0.0) ? 1L : 0L);
    if(model_v > T || model_v < -T) { 
      model_gT_dir_ct += model_v * data_v > 0.0 ? 1L : 0L;//(int64_t) Maths::Funcs::sgnZi(model_v * data_v);
      model_data_NZ_gT_ct += data_v != 0.0 ? 1L : 0L;
      model_gT_ct++;
      gT_prod_sum += p; 
      gT_err_sumsq += (model_v - data_v) * (model_v - data_v);
      gT_model_sumsq += model_v * model_v;
      gT_data_sumsq += data_v * data_v;
    }
    ct++;
  }

  void print() { 
    printf("  Dot-norm1  cos(angle):  %lf\n", prod_sum / sqrt(data_sumsq) / sqrt(model_sumsq));
    printf("       Dot-norm2  %% dir:  %lf\n", prod_sum / prod_abs_sum);
    printf("                    R^2:  %lf\n", 1.0 - err_sumsq / data_sumsq);
    printf("             Avg Data^2:  %lf\n", data_sumsq / (double) ct);
    printf("            Avg Model^2:  %lf\n", model_sumsq / (double) ct);
    printf("           Data %% NZero:  %lf [%li / %li]\n", (double) nz_data_ct / (double) ct, nz_data_ct, ct);
    printf(" Model/NZ %% Dir Correct:  %lf [%li / %li]\n", (double) corr_dir_ct / (double) nz_data_ct, corr_dir_ct , nz_data_ct);
    printf("      Total CT %li  Data min,max: %lf, %lf  Model min,max: %lf, %lf\n", ct, min_data, max_data, min_model, max_model);
    printf("      Current T setting:  %lf ... Ct %li NZ Ct %li\n", T, model_gT_ct, model_data_NZ_gT_ct);
    if(model_data_NZ_gT_ct) {     
      printf("        gT NZ %% Correct:  %lf\n",  (double) model_gT_dir_ct / (double) model_data_NZ_gT_ct);
      printf("gT Dot-norm1 cos(angle):  %lf\n", gT_prod_sum / sqrt(gT_data_sumsq) / sqrt(gT_model_sumsq));
      printf("                 gT R^2:  %lf\n", 1.0 - gT_err_sumsq / gT_data_sumsq);
      printf("          gT Avg Data^2:  %lf\n", gT_data_sumsq / (double) model_gT_ct);
      printf("         gT Avg Model^2:  %lf\n", gT_model_sumsq / (double) model_gT_ct);

    }
  }

};



#endif
