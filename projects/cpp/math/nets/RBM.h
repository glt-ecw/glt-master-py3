#ifndef RBM_H_
#define RBM_H_

#include "../Rand2.h"
#include "Funcs.h"
#include <stdexcept>
#include <vector>
#include <cstring>
#include <string>
#include <cstdio>
#include <random>

class RBM { 
  //visible units + 1 bias unit
  std::vector<double> v_;
  //hidden units + 1 bias unit
  std::vector<double> h_;
  //v_.size() * h_.size() weight vector
  std::vector<double> w_;
  std::vector<double> dw_;
    
  //temporary storage to prevent calling malloc too much
  std::vector<double> tmpvec;

public:
  
  RBM(int v_ct, int h_ct) { 
    if(v_ct <= 0 || h_ct <= 0)
      throw std::runtime_error("invalid rbm sizes");
    v_.resize(v_ct+1, (double) 0.0);
    v_.back() = 1.0;
    h_.resize(h_ct+1, (double) 0.0);
    h_.back() = 1.0;
    //note: very last value of v_ and h_ represents a bias node
    w_.resize((v_ct+1)*(h_ct+1), (double) 0.0);
    dw_.resize((v_ct+1)*(h_ct+1), (double) 0.0);
    //note: very last value of w_ represents a connection between
    //the two bias nodes, which is kind of meaningless since both
    //of those nodes are forced on all the time. 
  }

  /////////////////////////////////////////////////////////////////////
  ////functions for basic management of vals //////////////////////////
  /////////////////////////////////////////////////////////////////////

  int hCt() const { 
    return h_.size()-1;
  }

  int vCt() const { 
    return v_.size()-1;
  }

  int wCt() const { 
    //last element of w_ is a connection between two bias nodes, which
    //is meaningless 
    return w_.size() - 1;
  }
  
  inline double & w(int i_visible, int j_hidden) { 
    return w_[i_visible * h_.size() + j_hidden];
  }
  
  inline double & dw(int i_visible, int j_hidden) { 
    return dw_[i_visible * h_.size() + j_hidden];
  }
  
  void stdOut() { 
    printf("RBM visible ct %i hidden ct %i\n", (int) v_.size() -1, (int) h_.size() - 1);
    printf("Vis state:\n");
    for(int i = 0; i < (int) v_.size()-1; i++)
      printf("%3i %lf\n", i, v_[i]);
    printf("Bias from vis to hid %lf\n", v_.back());
    printf("Hid state:\n");
    for(int i = 0; i < (int) h_.size()-1; i++)
      printf("%3i %lf\n", i, h_[i]);
    printf("Bias from hid to vis %lf\n", h_.back());
  }

  void setInput(const std::vector<double> & input) { 
    if(input.size() != v_.size()-1U)
      throw std::runtime_error("Invalid input size");
    memcpy(v_.data(), input.data(), input.size() * sizeof(double));
  }

  /////////////////////////////////////////////////////////////////////
  ////functions for injecting random noise ////////////////////////////
  /////////////////////////////////////////////////////////////////////
  
  //add gaussian noise scaled by r to all of the weights
  void addGuassianNoiseToW(double r = 1.0) {
    std::normal_distribution<> d(0,1);
    //std::mt19937 gen;
    auto & gen = Rand2::localGen();
    for(int i = 0; i < wCt(); i++)
      w_[i] +=  d(gen) * r;
  }
  
  void addGuassianNoiseToDW(double r = 1.0) {
    std::normal_distribution<> d(0,1);
    //std::mt19937 gen;
    auto & gen = Rand2::localGen();
    for(int i = 0; i < wCt(); i++)
      dw_[i] +=  d(gen) * r;
  }

  //add gaussian noise to all of the hiddens 
  //note: each node either stores activations 
  //or stores probabilities. adding gaussian noise
  //to probabilities doesn't make much sense unless
  //we clamp them later.
  void addGuassianNoiseToH(double r = 1.0) { 
    std::normal_distribution<> d(0,1);
    //std::mt19937 gen;
    auto & gen = Rand2::localGen();
    for(int i = 0; i < hCt(); i++)
      h_[i] +=  d(gen) * r;
  }
  
  //add gaussian noise to all of the visibles
  void addGuassianNoiseToV(double r = 1.0) { 
    std::normal_distribution<> d(0,1);
    //std::mt19937 gen;
    auto & gen = Rand2::localGen();
    for(int i = 0; i < vCt(); i++)
      v_[i] +=  d(gen) * r;
  }


  /////////////////////////////////////////////////////////////////////
  ////functions for adjusting layer values ////////////////////////////
  /////////////////////////////////////////////////////////////////////

  void clampH() { 
    for(int j = 0; j < hCt(); j++) { 
      if(h_[j] < 0.0)
        h_[j] = 0.0;
      else if(h_[j] > 1.0)
        h_[j] = 1.0;
    }
  }
  
  void clampV() { 
    for(int i = 0; i < vCt(); i++) { 
      if(v_[i] < 0.0)
        v_[i] = 0.0;
      else if(v_[i] > 1.0)
        v_[i] = 1.0;
    }
  }
  
  void calcSigmoidH() { 
    for(int j = 0; j < hCt(); j++)
      h_[j] = Sigmoid::f(h_[j]);
  }  
  
  void calcSigmoidV() { 
    for(int i = 0; i < vCt(); i++)
      v_[i] = Sigmoid::f(v_[i]);
  }
  
  void sampleHProbTo01() { 
    auto & gen = Rand2::localGen();
    std::uniform_real_distribution<> dis(0, 1);
    for(int j = 0; j < hCt(); j++) 
      h_[j] = dis(gen) <= h_[j] ? 1.0 : 0.0;    
  }
  
  void sampleVProbTo01() { 
    auto & gen = Rand2::localGen();
    std::uniform_real_distribution<> dis(0, 1);
    for(int i = 0; i < vCt(); i++) 
      h_[i] = dis(gen) <= h_[i] ? 1.0 : 0.0;    
  }
  
  /////////////////////////////////////////////////////////////////////
  //// calculate one layer's value from another's /////////////////////
  /////////////////////////////////////////////////////////////////////
  
  
  void calcHProbFromV01() { 
    for(int j = 0; j < (int) h_.size() - 1; j++) { 
      h_[j] = 0.0;
      for(int i = 0; i < (int) v_.size(); i++) { 
        h_[j] += w(i,j) * v_[i];
      }
      h_[j] = Sigmoid::f(h_[j]);
    }
  }
  
  void calcVProbFromH01() { 
    for(int i = 0; i < (int) v_.size() - 1; i++) { 
      v_[i] = 0.0;
      for(int j = 0; j < (int) h_.size(); j++) { 
        v_[i] += w(i,j) * v_[j];
      }
      v_[i] = Sigmoid::f(v_[i]);
    }
  }


  //feasible for small rbm's, this can be used to get an exact estimate
  //of activation probabilities when the visible units are holding activation
  //probabilities in the range of 0-1. The calculation take O(2^vCt()), so 
  //when we have more than a handful of visible units it's not feasible. 
  void calcExactHProbFromVProb() { 
    //if we can do this in 1/nano it will take us over 
    //200 years if we need to do it for more than 63 bits
    //so assume 63 bits is enough
    if(v_.size() - 1LU > 63LU) { 
      throw std::runtime_error("Calculating exact prob from prob only supports opposite side units up to 63");
    }
    for(int j = 0; j < (int) h_.size() - 1; j++) { 
      h_[j] = 0.0;
      uint64_t bitmask = 0;
      double probsum = 0.0;
      while(!(bitmask >> (v_.size()-1LU))) { 
        double sum = 0.0;
        double prob = 1.0;
        for(int i = 0; i < (int) v_.size()-1; i++) { 
          if(bitmask & (1LU << (uint64_t) i)) { 
            prob *= v_[i];
            sum += w(i,j);
          } else { 
            prob *= (1.0 - v_[i]);
          }
        }
        //bias unit is always on 
        //prob *= 1.0;
        sum += w(v_.size()-1, j);


        //finally accumulate the expected value for h_[j] 
        h_[j] += Sigmoid::f(sum) * prob;
        //and accumulate the probsum 
        probsum += prob;
        bitmask++;
      }
      //normalize by probsum to help deal with numerical issues ...
      h_[j] /= probsum;
    }
  }

  void calcExactVProbFromHProb() { 
    if(h_.size() - 1LU > 64LU) { 
      throw std::runtime_error("Calculating exact prob from prob only supports opposite side units up to 63");
    }
    for(int i = 0; i < (int) v_.size() - 1; i++) { 
      v_[i] = 0.0;
      uint64_t bitmask = 0;
      double probsum = 0.0;
      while(!(bitmask >> (h_.size()-1LU))) { 
        double sum = 0.0;
        double prob = 1.0;
        for(int j = 0; j < (int) h_.size()-1; j++) { 
          if(bitmask & (1LU << (uint64_t) j)) { 
            //this node resulted in a 1!!!
            prob *= h_[j];//this is the probability of it being on
            sum += w(i,j);//it contributes this much to the activation when it's on
          } else { 
            prob *= (1.0 - h_[j]);//probability of the node being off
            //when the node is off it contributes nothing to the activation
          }
        }
        //add the effect of the bias node
        //the bias node is on with probability 1
        //and so we only look at one case for the bias node
        sum += w(i, h_.size()-1);
        
        //we now have the activation, and probability of seeing this activation
        //we're going to get the probability of this node being on given this 
        //activation by simply taking the sigmoid of it. if we sum the product of 
        //those two over all possible activation combinations then we will have 
        //the net probability of this node being on.
        v_[i] += Sigmoid::f(sum) * prob;

        //we keep a sum of all the probabilities just in case 
        //due to numerical error they don't add to 1, we can 
        //adjust that out. this probably isn't necassary in 
        //most cases.
        probsum += prob;

        //we use the bitmask to keep track of which nodes are on and off in 
        //this while loop. advance to the next combination of on/off nodes
        //by incrementing the bitmask.
        bitmask++;
      }

      //paranoid normalization of probability, probably not necassary.
      //since we may be taking the sum of A LOT of numbers, we're just putting this
      //hear just in case...
      v_[i] /= probsum;
    }
  }

  //This is a stop gap from calculating the exact probabilities, which is unfeasible for large numbers
  //of units. It approximates the probability by making a few samples. There's not much of an advantage
  //to this method; 
  void calcSampledHProbFromVProb(int samples) { 
    if(samples <= 0)
      throw std::runtime_error("need more samples");
    for(int j = 0; j < hCt(); j++)
      h_[j] = 0.0;
    std::vector<double> & v_tmp = tmpvec;
    v_tmp.resize(v_.size());
    v_tmp.back() = 1.0;
    auto & gen = Rand2::localGen();
    std::uniform_real_distribution<> dis(0, 1);
    for(int iter = 0; iter < samples; iter++) { 
      for(int i = 0; i < vCt(); i++) { 
        v_tmp[i] = dis(gen) <= v_[i] ? 1.0 : 0.0; 
      }
      for(int j = 0; j < hCt(); j++) { 
        double htmp = 0.0;
        for(int i = 0; i < (int) v_.size(); i++)
          htmp += v_tmp[i] * w(i,j);
        h_[j] += Sigmoid::f(htmp) / (double) samples;        
      }
    }
  }

  void calcSampledVProbFromHProb(int samples) {
    if(samples <= 0)
      throw std::runtime_error("need more samples");
    for(int j = 0; j < vCt(); j++)
      v_[j] = 0.0;
    std::vector<double> & h_tmp = tmpvec;
    h_tmp.resize(h_.size());//, (double) 0.0);
    h_tmp.back() = 1.0;
    auto & gen = Rand2::localGen();
    std::uniform_real_distribution<> dis(0, 1);
    for(int iter = 0; iter < samples; iter++) { 
      for(int j = 0; j < hCt(); j++) { 
        h_tmp[j] = dis(gen) <= h_[j] ? 1.0 : 0.0; 
      }
      for(int i = 0; i < vCt(); i++) { 
        double vtmp = 0.0;
        for(int j = 0; j < (int) h_.size(); j++)
          vtmp += h_tmp[j] * w(i,j);
        v_[i] += Sigmoid::f(vtmp) / (double) samples;        
      }
    }
  }

  //r is positive during the wake phase and negative during the sleep phase
  void addToDW(double r) { 
    for(int i = 0; i < (int) v_.size(); i++) { 
      for(int j = 0; j < (int) h_.size(); j++)
        dw(i,j) += v_[i] * h_[i] * r;
    }
    dw_.back() = 0.0; //for the connection between the bias units
  }

  void addDWtoW(double r) { 
    for(int i = 0; i < wCt(); i++)
      w_[i] = dw_[i] * r;
  }

  void scaleDW(double r) { 
    for(int i = 0; i < wCt(); i++)
      dw_[i] *= r;
  }

  void scaleW(double r) { 
    for(int i = 0; i < wCt(); i++)
      w_[i] *= r;
  }
  void scaleWBias(double r) { 
    for(int i = 0; i < vCt(); i++)
      w(i, hCt()) *= r;
    for(int j = 0; j < hCt(); j++)
      w(vCt(), j) *= r;
  }

  void scaleDWBias(double r) { 
    for(int i = 0; i < vCt(); i++)
      dw(i, hCt()) *= r;
    for(int j = 0; j < hCt(); j++)
      dw(vCt(), j) *= r;
  }
};



#endif
