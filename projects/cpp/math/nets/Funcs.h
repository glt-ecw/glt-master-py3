#ifndef NET_FUNCS_H_
#define NET_FUNCS_H_

#include <cmath>
#include <cstdint>

///////////////////////////////////////////////////////////////////////////////////////
//// Begin single in single out differentiable functions without parameters ///////////
///////////////////////////////////////////////////////////////////////////////////////

struct Sigmoid { 
  static double f(double v) { 
    return 1.0 / (1.0 + exp(-v));
  }
  static float f(float v) { 
    return 1.0f / (1.0f + exp(-v));
  }
  static double df_dv(double v) { 
    double tmp = f(v);
    return tmp * (1.0 - tmp);    
  }  
  static float df_dv(float v) { 
    float tmp = f(v);
    return tmp * (1.0f - tmp);    
  }
};


struct Logit {  
  static double f(double v) { 
    //Note: as v becomes even modestly large logit(v) -> v
    //but the numerical operation of calculating exp(v) for even modestly large v's results in NaN's
    if(v > 500.0)
      return v;
    return log(1.0 + exp(v));
  }
  static double df_dv(double v) { 
    return Sigmoid::f(v);
  }
};

struct ATan {
  static double f(double v) {
    return atan(v);
  }
  static double df_dv(double v) {
    return 1.0/(1.0 + v*v);
  }
};


//square root function mirrored about x and y, note: the derivative is undefined at 0 and approaches +/- inf as we approach 0
struct SignRoot { 
  static double f(double v) { 
    return (v >= 0.0) ? sqrt(v) : -sqrt(-v);
  }
  static double df_dv(double v) { 
    return (v >= 0.0) ? 0.5/(sqrt(v)) : 0.5/(sqrt(-v));
  }
};

//log function trucated at 1.0 and flipped accross the origin to give an unbounded S shaped thingy
struct SignLog1 { 
  static double f(double v) { 
    return (v >= 0.0) ? log(v + 1.0) : -log(-v + 1.0);
  }
  static double df_dv(double v) { 
    return (v >= 0.0) ? 1.0 / (v + 1.0) : 1.0/(-v + 1.0);
  }
};

//Custom non-linear monotonically increasing function
struct NonLinearMonotonic { 
  static double f(double v) { 
    return (v <= 0.0) ? ((1.0-(v-1.0)*(v-1.0))/4.0) : ( sqrt(v+1)-1 );
  }
  static double df_dv(double v) { 
    return (v <= 0.0) ? (1.0-v)/2.0 : 0.5/sqrt(v+1);
  }
};

//This looks a regular sigmod, but with the ends squashed to 0
struct NonLinearS { 
  static double f(double v) {
    return v / (v*v + 1.0);
  }
  static double df_dv(double v) { 
    return 1.0 / (v*v + 1.0) - 2.0*v*v / (v*v+1.0)/(v*v+1.0);
  }
};


//looks like a gaussian - has a little bump at the middle
struct NonLinearExpQuadratic { 
  static double f(double v) { 
    return exp(-(v*v));
  }
  static double df_dv(double v) { 
    return -exp(-(v*v)) * 2.0 * v;
  }
};





#endif
