#ifndef DGRAPH_H_
#define DGRAPH_H_

//generic directed graph class

#include <stdexcept>
#include <vector>
#include <list>
template <typename NODE_T, typename EDGE_T>
class DGraph { 
  struct Edge;

  struct Node { 
    int id;
    std::vector<Edge *> p; //parents 
    std::vector<Edge *> c; //children
    int tag;

    NODE_T data;
 
    Node() { }
    Node(const NODE_T & d) : data(d) { }


    void removeEdgesFromHereToNodePtr(Node * dst, std::vector<Edge *> & removed) { 
      std::vector<Edge *> nc;//new children
      for(int i = 0; i < (int) c.size(); i++) { 
        if(c[i]->d == dst) {
          removed.push_back(c[i]);
          c[i] = 0;
        } else 
          nc.push_back(c[i]);
      }
      c = nc;
    }
    void removeEdgesToHereFromNodePtr(Node * src) { 
      std::vector<Edge *> np;//new parents
      for(int i = 0; i < (int) p.size(); i++) { 
        if(p[i]->s == src)
          p[i] = 0;
        else
          np.push_back(p[i]);
      }
      p = np;
    }
  };
  
  struct Edge { 
    int id;
    Node * s;
    Node * d;

    EDGE_T data;
  };

  std::vector<Node *> n_;
  std::vector<Edge *> e_;
  int nIdCtr_ = 0;
  int eIdCtr_ = 0;

public:
  void deepCopyOther(const DGraph & other) {
    //not particularly optimzed... but hopefully we're node calling this too often..
    deleteAll();
    nIdCtr_ = other.nIdCtr_;
    eIdCtr_ = other.eIdCtr_;
    for(int i = 0; i < other.nCt(); i++) {
      n_.push_back(new Node(*other.n_[i]));
      n_.back()->c.resize(0);
      n_.back()->p.resize(0);
    }
    for(int i = 0; i < other.nCt(); i++) {
      for(int j = 0; j < (int) other.n_[i]->c.size(); j++) { 
        //printf("%i -> %i/%i\n", i, j, (int) other.n_[i]->c.size());
        eForceCreate(other.n_[i]->id, other.n_[i]->c[j]->d->id, other.n_[i]->c[j]->data, other.n_[i]->c[j]->id);
      }
    }
    //okay.. right now the e_ list and each node's p (parent) list may be in a different order
    //from the lists in the other... later we can add an option to skip this step in case it 
    //takes too much time. For now the method of sorting the lists is n^2 but doing some trickery
    //with the tags could be made nlogn. 
    for(int i = 0; i < nCt(); i++) { 
      for(int j = 0; j < (int) n_[i]->p.size(); j++) { 
        //printf("%i/%i %p  -- %i %i\n",i, (int) n_.size(), n_[i], n_[i]->id, other.n_[i]->id);
        //printf("  %i/%i %p\n",j, (int) n_[i]->p.size(), n_[i]->p[j]);
        //printf("    %i/%i %p\n",i, (int) other.n_.size(), other.n_[i]);
        //printf("      %i/%i %p\n",j, (int) other.n_[i]->p.size(), other.n_[i]->p[j]);
        if(n_[i]->p[j]->id != other.n_[i]->p[j]->id) { 
          for(int j2 = j+1; j2 < (int) n_[i]->p.size(); j2++) { 
            if(n_[i]->p[j2]->id == other.n_[i]->p[j]->id) {
              std::swap(n_[i]->p[j], n_[i]->p[j2]);
              break;
            }
          }
        }
      }
    }
    for(int i = 0; i < eCt(); i++) { 
      if(e_[i]->id != other.e_[i]->id) { 
        for(int i2 = i+1; i2 < eCt(); i2++) { 
          if(e_[i2]->id == other.e_[i]->id) { 
            std::swap(e_[i], e_[i2]);
            break;
          }
        }
      }
    }

  }
private:


  void deleteEdgesFromTo_(Node * s, Node * d) { 
    std::vector<Edge *> removed;
    s->removeEdgesFromHereToNodePtr(d, removed);
    d->removeEdgesToHereFromNodePtr(s);
    for(int i = 0 ; i < (int) removed.size(); i++) { 
      for(int j = 0; j < (int) e_.size(); j++) { 
        if(e_[j] == removed[i]) {
          e_[j] = 0;
          break;
        }
      }
      delete removed[i];      
    } 
    std::vector<Edge *> ne;
    for(int j = 0; j < (int) e_.size(); j++) { 
      if(e_[j])
        ne.push_back(e_[j]);
    }
    e_ = ne;
  }

  Node * nById_(int id) { 
    for(auto * n : n_) { 
      if(n->id == id)
        return n;
    }
    return 0;
  }

  Edge * eById_(int id) { 
    for(auto * e : e_) {
      if(e->id == id)
        return e;
    }
    return 0;
  }

  std::vector<int> getAllTags_() { 
    std::vector<int> ret;
    for(int i = 0; i < (int) n_.size(); i++)
      ret.push_back(n_[i]->tag);
    return ret;
  }
  
  void setAllTags_(const std::vector<int> & t) { 
    if(t.size() != n_.size())
      throw std::runtime_error("Set all tags size does not match node count");
    for(int i = 0; i < (int) n_.size(); i++)
      n_[i]->tag = t[i];
  }

  void setAllTags_(int tag) { 
    for(auto * n : n_)
      n->tag = tag;
  }

  std::vector<Node *> noParentNodes_() { 
    std::vector<Node *> ret;
    for(auto * n : n_) {
      if(!n->p.size())
        ret.push_back(n);
    }
    return ret;
  }
  
  std::vector<Node *> noChildNodes_() { 
    std::vector<Node *> ret;
    for(auto * n : n_) {
      if(!n->c.size())
        ret.push_back(n);
    }
    return ret;
  }



public:

  struct NodeWithNeighbors {

    NODE_T * n;
    
    struct C { 
      EDGE_T * e;
      NODE_T * n;
      int eid;
      int nid;
    };
    struct P { 
      EDGE_T * e;
      NODE_T * n;
      int eid;
      int nid;
    };
    
    std::vector<C> c;
    std::vector<P> p;
  
    int nid;
  };

  
  
private:
  std::vector<Node *> feedForwardOrder_(std::vector<Node *> sources) { 
    //initialize the tags to 0
    setAllTags_(0);
    //set the tags for sources so that they will definitely _not_ be double included in the list
    for(auto * n : sources) 
      n->tag = n_.size() + e_.size(); 

    //the idea is we iterate through the sources and increment the tag for each child of each source
    //once a child has tag equal to number of parents, that means it's safe to add it to the sources 
    //list too. this way the sources list contains nodes that have an ancestral ordering. In the case
    //of loops, this breaks down, since a node is essentially it's own ancestor
    int idx = 0;
    while(idx < (int) sources.size()) { 
      for(auto * e : sources[idx]->c) { 
        e->d->tag++;
        if(e->d->tag == (int) e->d->p.size()) { 
          sources.push_back(e->d);
        }
      }
      idx++;
    }
    return sources;
  }

  //This helper returns a feedForwardOrder that minimizes the number of times we insert a child before it's ancestors into the order. 
  //It does this through recursion, exploring every possible break junction, which is VERY expensive for large graphs with lots of breaks.
  //breaks tracks how many nodes we had to forcibly insert into sources to break a loop
  //and ebreaks tracks how many edges were broken to achieve that as well. for now we minimize
  //breaks first, and ebreaks second. in theory minimizing just ebreaks or some weighted combo 
  //of the two is possible. 
  std::vector<Node *> feedForwardOrder_MinimalLoopBreaks0Helper_(std::vector<Node *> sources, int idx, int & breaks, int & ebreaks) { 
    while(idx < (int) sources.size()) { 
      for(auto * e : sources[idx]->c) { 
        e->d->tag++;
        if(e->d->tag == (int) e->d->p.size()) { 
          sources.push_back(e->d);
        }
      }
      idx++;
    }
    if(sources.size() != n_.size()) { 
     

      //we're gonna iterate over each break possible to find the one that 
      //results in the least possible future breaks... this is expensive
      //like exponential in time and memory possibly
      std::vector<Node *> best;
      int min_brk = n_.size() + e_.size();
      int min_ebrk = n_.size() + e_.size();

      bool found = false;
      //an example of when found is false but sources != n_.size() is when 
      //the user has forcibly deactivated a node by setting it's tag to it's 
      //parent size or when a noParentNode is not already in the sources list

      for(int i = 0; i < (int) n_.size(); i++) { 
        int sn = n_[i]->p.size() - n_[i]->tag;
        if(sn > 0) { 
          found = true;
          //okay, this node is a candidate for a break
          //save the state of all the tags
          std::vector<int> save_tags = getAllTags_();
          //add the node
          sources.push_back(n_[i]);
          //do not allow it to double activate 
          n_[i]->tag = n_[i]->p.size();
          //recursively see how many more breaks this results in
          int brk = 1;
          int ebrk = sn;
          std::vector<Node *> src2 = feedForwardOrder_MinimalLoopBreaks0Helper_(sources, idx, brk, ebrk);
          //restore sources and tags
          sources.resize(sources.size()-1);
          setAllTags_(save_tags);
          //did this result in any improvement?
          if(brk < min_brk) {
            min_brk = brk;
            min_ebrk = ebrk;
            best = src2;
          } else if(brk == min_brk && ebrk < min_ebrk) { 
            //tie break on breaking less edges
            min_ebrk = ebrk;
            best = src2;     
          }
        }
      }
      if(!found)
        return sources;
      
      breaks += min_brk;
      ebreaks += min_ebrk;
      return best;
    }
    return sources;
  }

  std::vector<Node *> feedForwardOrder_MinimalLoopBreaks0_(std::vector<Node *> sources) { 
    //NOTE: this is very slow if there's lots of breaks!!!
    setAllTags_(0);
    for(auto * n : sources) 
      n->tag = n_.size() + e_.size(); 


    int brks = 0;
    int ebrks = 0;
    return feedForwardOrder_MinimalLoopBreaks0Helper_(sources, 0, brks, ebrks);
  }

  std::vector<Node *> feedForwardOrder_HeuristicLoopBreaks0_(std::vector<Node *> sources) { 
    setAllTags_(0);
    for(auto * n : sources) 
      n->tag = n_.size() + e_.size(); 
    
    int idx = 0;
    while(1) { 
      while(idx < (int) sources.size()) { 
        for(auto * e : sources[idx]->c) { 
          e->d->tag++;
          if(e->d->tag == (int) e->d->p.size()) { 
            sources.push_back(e->d);
          }
        }
        idx++;
      }
      //note: this isn't that optimized ... cleverly keeping track of unused nodes could be much faster
      if(sources.size() != n_.size()) { 
        //attempt to resolve a loop junction by forcibly adding the loop 
        //node to the sources list.
        Node * best = 0;
        int best_srcs_needed = n_.size() + e_.size();
        //First, try to get the node that has at least one parent node value
        //filled out, but also needs the least amount of additional parent 
        //nodes filled out.
        for(int i = 0; i < (int) n_.size(); i++) { 
          int sn = n_[i]->p.size() - n_[i]->tag;
          if(sn > 0 && sn < best_srcs_needed && n_[i]->tag > 0) { 
            best_srcs_needed = sn;
            best = n_[i];
          }        
        }
        //Okay, there must be a disjoint loop here... so just find the 
        //node that needs the least amount of additional parent nodes 
        //filled out. 
        if(!best) { 
          for(int i = 0; i < (int) n_.size(); i++) { 
            int sn = n_[i]->p.size() - n_[i]->tag;
            if(sn > 0 && sn < best_srcs_needed) { 
              best_srcs_needed = sn;
              best = n_[i];
            }        
          }
        }
        
        if(best) {
          //okay, we have an additional node to place into the sources list
          //set that nodes tag so that it won't be double added. 
          best->tag = best->p.size();
          sources.push_back(best);      
        } else { 
          //This means a new candidate node was node found!.
          //an example of when found is false but sources != n_.size() is when 
          //the user has forcibly deactivated a node by setting it's tag to it's 
          //parent size or when a noParentNode is not already in the sources list
          return sources;
        }
      } else 
        return sources;
    }    
    return sources;
  }

public:

  std::vector<NodeWithNeighbors> feedForwardOrder(int brk_loop_style = 0) {     
    //note: the back prop order can be simply the reverse of the feedforward order,
    //since A child_of B is the same as B parent_of A. Note that in the presence of 
    //loops the meaning of this breaks down as we may want something different since 
    //a true back or forward ordering doesn't exist.  
    
    std::vector<Node *> tmp;
    if(brk_loop_style == 1)
      tmp = feedForwardOrder_MinimalLoopBreaks0_(noParentNodes_());
    else if(brk_loop_style == 2)
      tmp = feedForwardOrder_HeuristicLoopBreaks0_(noParentNodes_());
    else { 
      tmp = feedForwardOrder_(noParentNodes_());
    }

    std::vector<NodeWithNeighbors> ret;
    ret.resize(tmp.size());
    for(int i = 0; i < (int) tmp.size(); i++) { 
      auto & r = ret[i];
      r.n = &(tmp[i]->data);
      r.nid = tmp[i]->id;
      r.c.resize(tmp[i]->c.size());
      for(int i2 = 0; i2 < (int) tmp[i]->c.size(); i2++) {
        r.c[i2].e = &(tmp[i]->c[i2]->data);
        r.c[i2].n = &(tmp[i]->c[i2]->d->data);
        r.c[i2].nid = tmp[i]->c[i2]->d->id;
        r.c[i2].eid = tmp[i]->c[i2]->id;
      } 
      r.p.resize(tmp[i]->p.size());
      for(int i2  = 0; i2 < (int) tmp[i]->p.size(); i2++) { 
        r.p[i2].e = &(tmp[i]->p[i2]->data);
        r.p[i2].n = &(tmp[i]->p[i2]->s->data);
        r.p[i2].nid = tmp[i]->p[i2]->s->id;
        r.p[i2].eid = tmp[i]->p[i2]->id;
      }     
    }
    return ret;
  }

    

  int leftIsAncestorOfRight(int idL, int idR) { 
    auto * nL = nById_(idL);
    if(!nL)
      return false;
    setAllTags_(0);
    std::list<Node *> tmp;
    tmp.push_back(nL);
    nL->tag = 1;
    while(!tmp.empty()) {   
      Node * fr = tmp.front();
      for(auto * e : fr->c) { 
        if(e->d->tag == 0) { 
          if(e->d->id == idR)
            return true;
          e->d->tag = 1;
          tmp.push_back(e->d); 
        }
      }
      tmp.pop_front();
    }
    return false;
  }

  int addNode() { 
    auto * n = new Node;
    n->id = nIdCtr_++;
    n_.push_back(n);    
    return n->id;
  }  

  int addNode(const NODE_T & d) { 
    auto * n = new Node(d);
    n->id = nIdCtr_++;
    n_.push_back(n);    
    return n->id;
  }

  NODE_T * nDataBack() { 
    if(!n_.size())
      return 0;
    return &(n_.back()->data);
  }

  int nBackId() const { 
    if(!n_.size())
      return -1;
    return n_.back()->id;
  }

  NODE_T * nData(int node_id) {
    auto * n = nById_(node_id);
    if(!n)
      return 0;
    return &(n->data);
  }

  bool hasConn(int src_node_id, int dst_node_id) { 
    auto * s = nById_(src_node_id);
    if(!s)
      return false;
    auto * d = nById_(dst_node_id);
    if(!d)
      return false;
  }

  int connect(int src_node_id, int dst_node_id) { 
    auto * s = nById_(src_node_id);
    if(!s)
      return -1;
    auto * d = nById_(dst_node_id);
    if(!d)
      return -1;
    
    auto * e = new Edge;
    e->id = eIdCtr_++;
    e->s = s;
    e->d = d;
    s->c.push_back(e);
    d->p.push_back(e);
    e_.push_back(e);
    return e->id;
  }
  
  EDGE_T * eData(int edge_id) { 
    auto * e = eById_(edge_id);
    if(!e)
      return 0;
    return &(e->data);
  }
  
  EDGE_T * eData(int src_node_id, int dst_node_id, int conn_idx) { 
    auto * s = nById_(src_node_id);
    if(!s)
      return 0;
    for(int i = 0; i < (int) s->c.size(); i++) { 
      if(s->c[i]->d->id == dst_node_id) { 
        if(conn_idx == 0)
          return &(s->c[i]->data);
        conn_idx--;
      }
    } 
    return 0;
  }

  //Note: this may be dangerous in the sense that two nodes might end up with the 
  //same id. Use this if you're recycling node ids or _know_ what you're doing. This
  //is primarily used by deep copy method for now...
  void eForceCreate(int src_node_id, int dst_node_id, const EDGE_T & edat, int force_id) { 
    auto * s = nById_(src_node_id);
    auto * d = nById_(dst_node_id);
    if(!s || !d) {
      printf("Failed to force create from %i to %i\n", src_node_id, dst_node_id);
      return;
    }
    auto * e = new Edge {force_id, s, d, edat};
    s->c.push_back(e);
    d->p.push_back(e);
    e_.push_back(e);
  }

  EDGE_T * eDataCreate(int src_node_id, int dst_node_id) { 
    auto * ret = eData(src_node_id, dst_node_id, 0);
    if(ret)
      return ret;
    //else
    int r = connect(src_node_id, dst_node_id);
    if(r >= 0) 
      return eDataBack();
    //else
    return 0;
  }

  EDGE_T * eDataBack() { 
    if(!e_.size())
      return 0;
    return &(e_.back()->data);
  }

  int nct() const { 
    return n_.size();
  }
  int nCt() const {
    return nct();
  }

  int ect() const { 
    return e_.size();
  }
  int eCt() const { 
    return ect();
  }

  template <typename LAMBDA>
  void eachEdgeLV(LAMBDA lambda) { 
    for(auto & e : e_) { 
      lambda(e->data);
    }
  }
  
  template <typename LAMBDA>
  void eachChildEdge_NodeRawIdx_LV(int raw_idx, LAMBDA lambda) { 
    if(raw_idx < 0 || raw_idx >= (int) n_.size())
      return;
    for(auto * ch : n_[raw_idx]->c) { 
      lambda(ch->data);
    }
  }
      
  template <typename LAMBDA>
  void eachEdgeWithSrcDstNodeIdsLV(LAMBDA lambda) { 
    for(auto * edge : e_) { 
      lambda(edge->data, edge->s->id, edge->d->id);
    } 
  }
  
  template <typename LAMBDA>
  void eachEdgeToRawIdx_WithSrcNodeIdsLV(int raw_dst_idx, LAMBDA lambda) { 
    for(auto * edge : n_[raw_dst_idx]->p) { 
      lambda(edge->data, edge->s->id);
    }
  }
  
  template <typename LAMBDA>
  void eachEdgeFromRawIdx_WithDstNodeIdsLV(int raw_src_idx, LAMBDA lambda) { 
    for(auto * edge : n_[raw_src_idx]->c) { 
      lambda(edge->data, edge->d->id);
    }
  }    


  void deleteAllEdgesFromTo(int from_id, int to_id) { 
    Node * s = nById_(from_id);
    Node * d = nById_(to_id);
    deleteEdgesFromTo_(s, d);
  }

  std::vector<int> unconnectedNodeIds(int src_id, bool search_parent_dir=true, bool search_child_dir=true) { 
    std::vector<int> ret;
    setAllTags_(0);
    Node * s = nById_(src_id);
    s->tag = 1;
    std::list<Node *> tmp;
    tmp.push_back(s);
    while(!tmp.empty()) { 
      auto * f = tmp.front();
      tmp.pop_front();
      if(search_child_dir) { 
        for(auto * c_e : f->c) { 
          if(!c_e->d->tag) { 
            c_e->d->tag = 1;
            tmp.push_back(c_e->d);
          }
        }
      }
      if(search_parent_dir) { 
        for(auto * p_e : f->p) { 
          if(!p_e->s->tag) { 
            p_e->s->tag = 1;
            tmp.push_back(p_e->s);
          }
        }
      }
    }
    for(int i = 0; i < (int) n_.size(); i++) { 
      if(!(n_[i]->tag))
        ret.push_back(n_[i]->id);
    }
    return ret;
  }

  void deleteAll() { 
    for(auto * n : n_)
      delete n;
    for(auto * e : e_)
      delete e;
    n_.resize(0);
    e_.resize(0);
  }

};




#endif
