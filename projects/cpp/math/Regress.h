#ifndef REGRESS_H_
#define REGRESS_H_

#include "Maths.h"
#include <cmath>
#include <vector>


//...a little bit of an akward approach to regression....


struct PrescaleX { 
  std::vector<double> std_devs;
  
  void prescale(std::vector<double> & x) { 
    prescale(x.data());
  }
  void prescale(double * x) { 
    for(int i = 0; i < (int) std_devs.size(); i++)
      x[i] /= std_devs[i];
  }

  int xct() const { 
    return std_devs.size();
  }
};

struct PostscaleY { 
  double std_dev;
  void postscale(double & y) { 
    y *= std_dev;
  }
  void scaleTestData(double & y) { 
    y /= std_dev;
  }
};

struct RawCoeffs { 
  std::vector<double> c;
  double calc(const std::vector<double> & x) {
    return calc(x.data());
  }
  double calc(const double * x) {
    double r = 0.0; 
    for(int i = 0; i < (int) c.size(); i++)
      r += c[i] * x[i];
    return r;
  }
};

struct MultiRegressStats { 

  int xct;
  int yct;
  std::vector<double> x_sums;
  std::vector<std::vector<double>> xT_x;
  std::vector<std::vector<double>> xT_y;
  std::vector<double> yT_y;
  std::vector<double> y_sums;
  int64_t ct;

  void reset(int xc, int yc = 1) { 
    xct = xc;
    yct = yc;
    x_sums.resize(0);
    x_sums.resize(xct, (double) 0.0);
    xT_x.resize(xct);
    for(auto & v : xT_x) {
      v.resize(0);
      v.resize(xct, (double) 0.0);
    }

    xT_y.resize(0);
    xT_y.resize(xct);
    for(auto & yr : xT_y)
      yr.resize(yct, (double) 0.0);
    
    yT_y.resize(0);
    yT_y.resize(yct, 0.0);
    y_sums.resize(0);
    y_sums.resize(yct, 0.0);
    ct = 0;
  }
  
  void add(const std::vector<double> & x, double y) { 
    if(!x_sums.size()) {
      reset(x.size(), 1);
    }
    add(x.data(), &y);
  }

  void add(const std::vector<double> & x, const std::vector<double> & y) { 
    if(!x_sums.size()) {
      reset(x.size(), y.size());
    }
    add(x.data(), y.data());
  }

  void add(const double * x, const double * y) { 
    for(int i =0; i < xct; i++) {
      x_sums[i] += x[i];
      for(int j = 0; j < yct; j++) 
        xT_y[i][j] += x[i] * y[j];
    }
    for(int i = 0; i < yct; i++) 
      y_sums[i] += y[i];
    for(int i = 0; i < yct; i++) 
      yT_y[i] += y[i]*y[i];

    for(int i =0; i < xct; i++) {
      //do half the multiplies for off diagonals
      for(int j = i+1; j < xct; j++) { 
        xT_x[i][j] += x[i]*x[j];
      }
      //set up the diagonals
      xT_x[i][i] += x[i]*x[i];
    }
    ct++;
  }


  //NOTE: call this when we're done!!!
  //this will fin in the other half of the off-diagonal elements. 
  void fillInOtherOffDiagonal() { 
    for(int i =0; i < xct; i++) { 
      for(int j = i+1; j < xct; j++) { 
        xT_x[j][i] = xT_x[i][j];
      }
    }
  }

  void printParams() { 
    printf("CT %li\n", ct);
    printf("xT_x:\n");
    for(int i = 0; i < xct; i++) { 
      for(int j = 0; j < xct; j++)
        printf("%lf ", xT_x[i][j]);
      printf("\n");
    }
    printf("xT_y:\n");
    for(int i = 0; i < xct; i++) {
      for(int j = 0; j < yct; j++) { 
        printf("%lf ", xT_y[i][j]);
      }
      printf("\n");
    }
    printf("yT_y:\n");
    for(int i = 0; i < yct; i++)
      printf("%lf ", yT_y[i]);
    printf("\n");     
  }

  PrescaleX normalizeByX() { 
    PrescaleX ret;
    //We want every x-series to have a std-dev / var of 1
    //This means that E(X^2) - E(X)^2 = 1.0 
    //Here we assume E(X) = 0
    //so we have E(X^2) * CT in xT_x[i][i]
    //if we divide each series by W then xT_x[i][i] becomes E(X^2) * CT / W / W
    //we need xT_x[i][i] to equal CT since E(X^2) needs to be 1. Therefore we 
    //need W to equal sqrt(E(X^2)) which is sqrt(xT_x[i][i] / CT) - which is the same as the std-devs...
    for(int i = 0; i < xct; i++) { 
      double sq = sqrt(xT_x[i][i] / (double) ct);
      for(int j = i+1; j < xct; j++)  
        xT_x[i][j] /= sq;
      for(int j = 0; j < i; j++)  
        xT_x[j][i] /= sq;
      for(int j = 0; j < yct; j++)
        xT_y[i][j] /= sq;// * sqrt(yT_y / (double) ct);/////////////////////
      xT_x[i][i] = (double) ct;//(sq * sq);//1.0;
      ret.std_devs.push_back(sq);
    }

    fillInOtherOffDiagonal();
    return ret;
  }

  PostscaleY normalizeByY(int y_idx) { 
    //this will just divide each of xT_y by sqrt(yT_y / ct) similar to how normalizeByX works...
    double sq = sqrt(yT_y[y_idx] / (double) ct);
    for(int i = 0; i < (int) xct; i++)
      xT_y[i][y_idx] /= sq;
    yT_y[y_idx] = (double) ct;
    return PostscaleY{sq};
  }

  double coeffError(const std::vector<double> & coeffs, double l2_param_penalty, double l1_param_penalty, int y_idx) { 
    if(xT_x[xct-1][0] != xT_x[0][xct-1]) {
      fillInOtherOffDiagonal();
      printf("Filled in transpose elements (1)..., please do this manually next time as auto doesn't always work! [check %lf]\n", xT_x[xct-1][0]);
    }
    double err = 0.0;
    for(int i = 0; i < xct; i++) { 
      double p = 0.0;
      for(int j = 0; j < xct; j++) { 
        p += coeffs[j] * xT_x[i][j];
      }
      err += (p - xT_y[i][y_idx])*(p - xT_y[i][y_idx]) + coeffs[i] * coeffs[i] * l2_param_penalty + Maths::Funcs::abs(coeffs[i]) * l1_param_penalty;
    }
    return err;
  }
  
  double coeffError(const RawCoeffs & coeffs, double l2_param_penalty, double l1_param_penalty, int y_idx) { 
    return coeffError(coeffs.c, l2_param_penalty, l1_param_penalty, y_idx);
  }

  RawCoeffs getCoeffs(double l2_param_penalty, double l1_param_penalty, int max_iter, int y_idx) { 
    if(xT_x[xct-1][0] != xT_x[0][xct-1]) {
      fillInOtherOffDiagonal();
      printf("Filled in transpose elements (2)..., please do this manually next time as auto doesn't always work! [check %lf]\n", xT_x[xct-1][0]);
    }
    std::vector<double> ret;
    ret.resize(xct, (double) 0.0); 
    
    //double err_prev = coeffError(ret, l2_param_penalty, l1_param_penalty);
    double rate = 0.5;
    
    double e0 = coeffError(ret, l2_param_penalty, l1_param_penalty, y_idx);
    
    //TODO: for now we do an akward gradient descent ... in the future we can switch to 
    //something more efficient like gauss-seidel. Also does L1 min on correlation data 
    //the same as the L1 min on the raw data? 
    for(int iter = 0; iter < max_iter; iter++) {
    
      std::vector<double> prev = ret;

      for(int i = 0; i < xct; i++) { 
        
        //printf("---- coeffs are :");
        double p = 0.0;
        for(int j = 0; j < xct; j++) { 
          //printf("%lf vs %lf | ", ret[j], xT_y[j][y_idx]);
          p += ret[j] * xT_x[i][j];
        }
        //printf("\n");
        double err = p - xT_y[i][y_idx];
        for(int k = 0; k < xct; k++)  {
          //printf("err is %lf delta %lf for k %i\n", err, rate * (2.0 * err * xT_x[i][k] + 2.0 * ret[k] * l2_param_penalty + Maths::Funcs::sgnZ(ret[k]) * l1_param_penalty), k);
          ret[k] -= rate * (2.0 * err * xT_x[i][k] + 2.0 * ret[k] * ret[k] * l2_param_penalty + Maths::Funcs::sgnZ(ret[k]) * l1_param_penalty);        
        }
      }
      
      double e1 = coeffError(ret, l2_param_penalty, l1_param_penalty, y_idx);
      //printf("%lf %lf\n", e1, e0);
      if(e1 > e0 * 1.1) {
        //failed to improve, and got worse by over 10%
        //printf("rollback\n");
        ret = prev;
        rate /= 2.0;
      } else if(e1 > e0) { 
        rate /= 1.5;
      } else { 
        //improved!!!
        rate *= 1.1;
        e0 = e1;
      }
    }
    return RawCoeffs{ret};
  };

};




#endif




