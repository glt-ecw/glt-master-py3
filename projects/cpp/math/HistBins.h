#ifndef HIST_BINS_H_
#define HIST_BINS_H_

#include "PVTBins.h"
#include "../util/structures/SeqSeqRing.h"
#include <cstdint>
#include <string>

//This is used to keep track of historical prices in bins
//The bins are organized so that the sum of:
//  volume, elements, delta_t times certain weights is below a threshold
//
//Basically historical elements are inserted into the bins until a trigger
//condition is reached. At that point the bins need to be rebalanced. 
//Once that's done we can index into the bins and get information about them. 
//
//One way to use future data with the bins is to forcibly pop data from the leftmost
//bin and then move data to the left until it _exceeds_ the bin threshold condition


class HistBinsPVT { 
private:
  
  struct BinInfo { 
    BinSpec spec;

    int64_t v_sum;
    int64_t sv_sum;

    void clear() { 
      v_sum = 0;
      sv_sum = 0;
    }

    std::string specStr() const { 
      return spec.toStr();
    }
  };

  SeqSeqRing<Pvt> bins_;
  std::vector<BinInfo> info_;

public:

  HistBinsPVT() {  }

  void clear() { 
    for(int i = 0; i < (int) bins_.grps(); i++)
      info_[i].clear();
    bins_.clear();
  }

  void addBin(const BinSpec & bs) {     
    bins_.newGrpPushBack();
    info_.push_back({bs, 0L, 0L});
  }
  void addBinHead(const BinSpec & bs) { 
    bins_.newGrpPushFront();
    info_.insert(info_.begin(), {bs, 0L, 0L});
  }

  std::string specStr(int bin_id) const { 
    return info_[bin_id].specStr();
  }

  void push(const Pvt & pvt)  {
    bins_.push(pvt);
    info_.back().v_sum += pvt.v;
    info_.back().sv_sum += pvt.sv;
  }
  
  //Move data in bins right to left and return the number of pvt data points
  //removed from the whole sequence of bins (basically the number popped from the first bin).
  int rebalance(bool keep_above_thresh, bool force_check_all = false) { 
    int rem_ct = 0;
    for(int i = (int) bins_.grps()-1; i >= 0; i--) {       
      
      bool bin_touched = false;
      //Repeatedly move the data from the i-th bin to it's left while the data in that bin exceeds a threshold
      while(bins_.grpSize(i) > 1) { 
        
        auto & left = bins_.grpHead(i);
        
        //Not sure what to use for the "right" end of the bin for measuring delta-p or delta-t
        //auto & right = bins_.grpTail(i);
        auto & right = bins_.grpNextOrTail(i);//this uses the start of the rightmost bin

        //the sum is just a weighting of the bin data
        double sum;
        if(keep_above_thresh) { 
          sum = (info_[i].v_sum - bins_.grpTail(i).v) * info_[i].spec.v_w + 
                (bins_.grpNextOrTailPrv(i).ts - left.ts) * info_[i].spec.t_w + 
                (bins_.grpSize(i) - 1) * info_[i].spec.el_w;
        } else { 
          sum = info_[i].v_sum * info_[i].spec.v_w +  (right.ts - left.ts) * info_[i].spec.t_w + bins_.grpSize(i) * info_[i].spec.el_w;
        }

        if(sum > info_[i].spec.sum_thresh) {
          //remove the pvt point from this bin!
          if(i > 0) {
            //if there is another bin to the left of this one, transfer stuff into there 
            info_[i-1].v_sum += left.v;
            info_[i-1].sv_sum += left.sv;
          } else {
            //this element was removed from the entire sequence
            rem_ct++;
          }
          //remove stuff from this bin
          info_[i].v_sum -= left.v;          
          info_[i].sv_sum -= left.sv;    
          //the data structure based removal      
          bins_.grpPop(i); 
          bin_touched = true;
        } else
          break;
      }
      //if no data points were moved from this bin to the next, then there's
      //no way the state of any of the next bins can change, so just stop now
      if(bins_.grpSize(i) > 1 && !bin_touched && !force_check_all)
        break;      
    }
    return rem_ct;
  }

  int ct() const { 
    return bins_.grps();
  }

  int64_t vol(int idx) {
    return info_[idx].v_sum;
  }

  int64_t sv(int idx) { 
    return info_[idx].sv_sum;
  }

  int64_t deltaP(int idx) { 
    if(bins_.grpEmpty(idx))
      return 0;
    const auto & left = bins_.grpHead(idx);
    //const auto & right = bins_.grpTail(idx);
    const auto & right = bins_.grpNextOrTail(idx);
    return right.p - left.p;
  }
  
  int64_t deltaT(int idx) {   
    if(bins_.grpEmpty(idx))
      return 0;
    const auto & left = bins_.grpHead(idx);
    //const auto & right = bins_.grpTail(idx);
    const auto & right = bins_.grpNextOrTail(idx);
    return right.ts - left.ts;
  }

  bool empty(int idx) const { 
    return bins_.grpEmpty(idx); 
  }

  int64_t headP(int idx) { 
    if(bins_.grpEmpty(idx))
      return 0;
    return bins_.grpHead(idx).p;
  }

};



#endif
