#ifndef HMM_H_
#define HMM_H_
#include <cstdio>
#include <cstdlib>
#include <list>
#include <cmath>
#include <limits>
#include <vector>
#include "Rand2.h"
class HMM { 

  std::vector<double> init_state_probs_;
  std::vector<double> ln_init_state_probs_;
  //matrix layout is PROBABILITY = transition_mtx_[FROM_STATE][TO_STATE]
  std::vector<std::vector<double>> transition_mtx_;
  std::vector<std::vector<double>> ln_transition_mtx_;
  //emission layout is PROBABILITY = emissions_[FROM_STATE][OBSERVATION]
  std::vector<std::vector<double>> emissions_;
  std::vector<std::vector<double>> ln_emissions_;

public:

  HMM(int states, int obs) { 
    init_state_probs_.resize(states);
    ln_init_state_probs_.resize(states);
    transition_mtx_.resize(states);
    ln_transition_mtx_.resize(states);
    for(auto & m : transition_mtx_)
      m.resize(states);
    for(auto & m : ln_transition_mtx_)
      m.resize(states);
    emissions_.resize(states);
    ln_emissions_.resize(states);
    for(auto & m : emissions_)
      m.resize(obs);
    for(auto & m : ln_emissions_)
      m.resize(obs);
  }

  void stdOut() { 
    const int sc = stateCt();
    const int oc = obsCt();
    printf("States: %i Obs: %i\n", sc, oc);
    for(int i = 0; i < sc; i++) { 
      printf("Init prob %i = %lf\n", i, initP(i));
    }
    for(int i = 0; i < sc; i++) { 
      for(int j  =0 ; j < sc; j++) { 
        printf("Tx prob %i->%i = %lf\n", i,j, transP(i,j));
      }
    }
    for(int i = 0; i < sc; i++) { 
      for(int j  =0 ; j < oc; j++) { 
        printf("Emit prob %i->%i = %lf\n", i,j, emitP(i,j));
      }
    }
  }
   
  double & initP(int i) { 
    return init_state_probs_[i];
  }
  double & emitP(int i, int j) { 
    return emissions_[i][j];
  }
  double & transP(int i, int j) { 
    return transition_mtx_[i][j];
  }
  void normalizeProbsCalcLog() { 
    const int sc = stateCt();
    const int oc = obsCt();
    double s = 0.0;
    for(int i = 0; i < sc; i++) {
      if(init_state_probs_[i] < 0.0)
        init_state_probs_[i] = 0.0;
      s += init_state_probs_[i];
    }
    for(int i = 0; i < sc; i++) {
      init_state_probs_[i] /= s;
      ln_init_state_probs_[i] = log(init_state_probs_[i]);
    }
    for(int i = 0; i < sc; i++) {
      s = 0.0;
      for(int j = 0; j < sc; j++) {
        if(transition_mtx_[i][j] < 0.0)
          transition_mtx_[i][j] = 0.0;
        s += transition_mtx_[i][j];
      }
      for(int j = 0; j < sc; j++) {
        transition_mtx_[i][j] /= s;
        ln_transition_mtx_[i][j] = log(transition_mtx_[i][j]);
      } 
      s = 0.0;
      for(int j = 0; j < oc; j++) { 
        if(emissions_[i][j] < 0.0)
          emissions_[i][j] = 0.0;
        s += emissions_[i][j];
      }
      for(int j = 0; j < oc; j++) {
        emissions_[i][j] /= s;
        ln_emissions_[i][j] = log(emissions_[i][j]);
      } 
    }
  }

  struct Path { 
    std::vector<int> obs;
    std::vector<int> state;
  };

  Path genPath(int ct) { 
    Path ret;
   
    double v;
    const int sc = stateCt();
    const int oc = obsCt();
    int i;//keep track of state
    int j;//keep track of emission
    
    //pick an initial state
    v = Rand2::rand01();
    for(i = 0; i < sc; i++) { 
      v -= init_state_probs_[i];
      if(v <= 0) {
        break;
      }
    }
    if(i == sc)
      i--;

    //generate ct points 
    while(ct > 0) { 
      
      //pick an emission for the current state
      v = Rand2::rand01();
      //printf("v set %lf\n", v);
      for(j = 0; j < oc; j++) { 
        v -= emissions_[i][j];
        if(v <= 0) {
          break;
        }        
      }
      if(j == oc)
        j--;
      
      //printf("j is %i\n", j);
      //save the emission and state
      //printf("state %i obs %i\n", i, j);
      ret.obs.push_back(j);
      ret.state.push_back(i);
    
      //pick a new state based on transition
      int ip = i;
      v = Rand2::rand01();
      for(i = 0; i < sc; i++) { 
        v -= transition_mtx_[ip][i];
        if(v <= 0)
          break;
      }
      if( i == sc)
        i--;
      
      ct--;

    }

    return ret;
  }

  int obsCt() const { 
    return emissions_[0].size();
  }

  int stateCt() const { 
    return init_state_probs_.size();
  }

  
  //just do lnSum over all the logProbFB to get the total 
  //log probability of a given observation...
  //
  //return Alpha-t ... probability of observing obs[0],obs[1],obs[2] ... obs[t] at times 0 through t while being in state i at time t
  //the length of the returned vecvec will be t+1
  std::vector<std::vector<double>> logProbForw(const int * obs, const int t) { 
    const int sc = stateCt();
    std::vector<double> ret;
    std::vector<double> tmp;
    ret.resize(sc);
    tmp.resize(sc);
    std::vector<std::vector<double>> all_probs;
    for(int i = 0; i < sc; i++) 
      ret[i] = lnProd( ln_init_state_probs_[i] , ln_emissions_[i][obs[0]] );
    all_probs.push_back(ret);
    
    for(int o = 1; o <= t; o++) { 
      for(int j = 0; j < sc; j++) { 
        //j is the state we're going to 
        tmp[j] = lnZero();
        for(int i = 0; i < sc; i++) { 
          //going from i to j
          tmp[j] = lnSum(tmp[j], lnProd(ret[i],ln_transition_mtx_[i][j]));
        }
        //going to j and observint obs[o]
        tmp[j] = lnProd(tmp[j], ln_emissions_[j][obs[o]]);
      }
      ret.swap(tmp);
      all_probs.push_back(ret);
    }
    return all_probs;; 
  }
  std::vector<double> probForwBackNorm(const int * obs, const int t) { 
    auto r = logProbForw(obs, t).back();
    double norm = lnZero();
    for(auto rel : r) 
      norm = lnSum(norm, rel);
    double psum = 0.0;
    for(auto & rel : r) { 
      rel = exp(lnDiv(rel, norm));
      psum += rel;
    }
    for(auto & rel : r)
     rel /= psum;
    return r; 
  }


  //return Beta-t ... probability of observering obs[t0+1],obs[t0+2]...obs[tL] at time's t0+1 through tL, while being in state obs[t0] at time t0
  //the length of the returned vecvec will be tL - t0 + 1
  std::vector<std::vector<double>> logProbBack(const int * obs, const int t0, const int tL) { 

    const int sc = stateCt();
    std::vector<double> ret;
    std::vector<double> tmp;
    ret.resize(sc);
    tmp.resize(sc);
    std::list<std::vector<double>> all_probs;
    std::vector<std::vector<double>> all_probs_ret;
    
    //setup B_tL (this is the probability of not observing anything)
    for(int i = 0; i < sc; i++) 
      ret[i] = log(1.0);
    //B_(tL-1) is the probability of observing obs[tL] and being in some state at time tL-1
    all_probs.push_front(ret);
    
    for(int o = tL-1; o >= t0; o--) { 
      for(int i = 0; i < sc; i++) { 
        //j is the state we're going to 
        tmp[i] = lnZero();
        for(int j = 0; j < sc; j++) { 
          //going from i to j
          tmp[i] = lnSum( tmp[i], lnProd(lnProd(ln_transition_mtx_[i][j], ln_emissions_[j][obs[o+1]]), ret[j]));
        }
    
      }
      ret.swap(tmp);
      all_probs.push_front(ret);
    }
    for(const auto & p : all_probs)
      all_probs_ret.push_back(p);
    return all_probs_ret;
  }


  //get the probability of being in state i at time t give obs[0],obs[1] ... obs[tL] 
  std::vector<std::vector<double>> logProbState(const int * obs, const int tL) { 
    const int sc = stateCt();
    std::vector<double> ret;
    std::vector<std::vector<double>> all_ret;
    ret.resize(sc);
    
    auto fv = logProbForw(obs, tL);//.back();
    auto bv = logProbBack(obs, 0, tL);//.front();
    
    for(int o = 0; o <= tL; o++) { 
      double ln_denom = lnZero();
      for(int i = 0; i < sc; i++)
        ln_denom = lnSum(ln_denom, lnProd(fv[o][i], bv[o][i]));

      for(int i = 0; i < sc; i++)
        ret[i] = lnDiv(lnProd(fv[o][i], bv[o][i]) , ln_denom);
    
      all_ret.push_back(ret);
    }

    return all_ret;
  }
  
  double logProbData(const int * obs, const int tL) { 
    //const auto r = logProbState(obs, tL);
    const auto r = logProbForw(obs, tL);//.back();
    const int sc = stateCt();
    double sum = lnZero();
    for(int i = 0; i < sc; i++) { 
      sum = lnSum(sum, r.back()[i]);
    }
    return sum;
  }


  //pick the most likely state using the logProbState 
  std::vector<int> maxProbState(const int * obs, const int tL) { 
    //TODO: do we use the forward or raw prob of the state here?
    auto r = logProbState(obs, tL);
    std::vector<int> ret;
    const int sc = stateCt();

    for(int o = 0; o <= tL; o++) { 
      double maxv = r[o][0];
      int maxi = 0;
      for(int i = 1; i < sc; i++) { 
        if(r[o][i] >= maxv) { 
          maxv = r[o][i];
          maxi = i;
        }
      }
      ret.push_back(maxi);
    }
    return ret;
  }
  std::vector<double> wProbState(const int * obs, const int tL) { 
    auto r = logProbState(obs, tL);
    std::vector<double> ret;
    const int sc = stateCt();

    for(int o = 0; o <= tL; o++) { 
      double v = 0.0;
      double psum = 0.0;
      for(int i = 0; i < sc; i++) { 
        v += exp(r[o][i]) * (double) i;
        psum += exp(r[o][i]);
      }
      v /= psum;
      ret.push_back(v);
    }
    return ret;
  }


  //TODO: this is a hacky test function, it takes some observations
  //then computes what the most likely states were given those obs 
  //then computes what the most likely obs were given those states.
  //If the obs come from the model then they should be close-ish
  std::vector<int> maxProbObs(const int * obs, const int tL) { 
    
    //get the most likely states at each point in time
    //auto r = maxProbState(obs, tL);
    auto r = logViterbi(obs, tL);
    std::vector<int> ret;
    const int oc = obsCt();

    for(int o = 0; o <= tL; o++) { 
      const int state = r[o].second;
      int maxi = 0;
      double maxv = ln_emissions_[state][0];
      for(int i = 1; i < oc; i++) { 
        if(ln_emissions_[state][i] >= maxv) {
          maxv = ln_emissions_[state][i];
          maxi = i;
        }
      }
      ret.push_back(maxi);
    }
    return ret;
  }


  //find the most likely state sequence give obs[0],obs[1],obs[2] ... obs[t]
  //returns a vector whose index i contains the highest single path probability of 
  //reaching state i at time t 
  //There's a bit of an issue here where if two source states are equally likely, the 
  //current implementation biases to picking the one with the higher index. Something 
  //that picks in a random but deterministic way might be better. This can be observed
  //in HMM's with many states that can jump all over the place.
  //The first component of a returned pair specifies the probability of this state path. 
  std::vector<std::pair<double, int>> logViterbi(const int * obs, const int t) { 
    const int sc = stateCt();
    std::vector<double> ret; //max probability among all paths ending in state [j]
    ret.resize(sc);
    std::vector<double> tmp;
    tmp.resize(sc);
    std::vector<int> psi;
    psi.resize(sc);
    std::vector<std::vector<double>> prob_bt;
    std::vector<std::vector<int>> psi_bt;
    std::list<std::pair<double, int>> ml_path_list;
    std::vector<std::pair<double, int>> ml_path;
    //std::vector<int> psi; //
    //psi.resize(sc);
    for(int i = 0; i < sc; i++) {
      ret[i] = lnProd( ln_init_state_probs_[i] , ln_emissions_[i][obs[0]] );
      psi[i] = -1;
    }
    prob_bt.push_back(tmp);
    psi_bt.push_back(psi);

    for(int o = 1; o <= t; o++) { 
      //for each state j, we want to find the most likely
      //path going to that state from any of the other states
      //i, this way we can get a most likely path for each state
      for(int j = 0; j < sc; j++) { 
        //going from 0 to j
        int maxi = 0;
        double maxv = lnProd(ret[0], ln_transition_mtx_[0][j]);
        //now compare to going from i to j;
        for(int i = 1; i < sc; i++) { 
          double tv = lnProd(ret[i], ln_transition_mtx_[i][j]);
          if(tv > maxv) { 
            maxv = tv;
            maxi = i; 
          }
        }
        //most likely state that we came from is now max
        //most likely probability is maxv
        tmp[j] = lnProd(maxv, ln_emissions_[j][obs[o]]);
        psi[j] = maxi;
      }
      ret.swap(tmp);
      
      prob_bt.push_back(ret);
      psi_bt.push_back(psi);
    }
    //we have a whole bunch of vec's of vec's which contains 
    //the max probability of being in state j at time idx and 
    //the previous state which gives that max 
    double maxv = prob_bt[prob_bt.size()-1][0];
    int maxi = 0;
    for(int i = 1; i < sc; i++) { 
      if(prob_bt[prob_bt.size()-1][i] > maxv) { 
        maxv = prob_bt[prob_bt.size()-1][i];
        maxi = i;
      }
    }
    ml_path_list.push_front({maxv, maxi});
    
    for(int o = (int) psi_bt.size()-1; o >= 1; o--){ 
      maxi = psi_bt[o][maxi];
      //psi_bt[i][maxi] gives us the previous state that 
      //led to this current max path 
      maxv = prob_bt[o-1][maxi];
      ml_path_list.push_front({maxv, maxi});  
    
    }
    
    for(auto a : ml_path_list)
      ml_path.push_back(a);

    return ml_path;
  }


  //this uses the forward and backward results to compute the 
  //probability of going from state i->j at times t->t+1 for any obs sequence 
  //
  void baumWelch(int * obs, int tL) { 
    const int sc = stateCt();
    const int oc = obsCt();
    std::vector<std::vector<double>> ln_xi; //prob of going from state i to state j at time t
    ln_xi.resize(sc);
    for(auto & x : ln_xi)
      x.resize(sc,  lnZero());
    std::vector<std::vector<double>> ln_sum_itoj;
    ln_sum_itoj = ln_xi;
    std::vector<double> ln_sum_fromi;
    ln_sum_fromi.resize(sc, lnZero());
    std::vector<std::vector<double>> ln_sum_fromi_obsk;
    ln_sum_fromi_obsk.resize(sc);
    for(auto & ls : ln_sum_fromi_obsk)
      ls.resize(oc, lnZero());

    auto fv =  logProbForw(obs, tL);
    auto bv =  logProbBack(obs, 0, tL);

    for(int t = 0; t < tL; t++) { 
      double norm = lnZero();
      for(int i = 0; i < sc; i++){ 
        for(int j = 0; j < sc; j++) { 
          //probability going from i at time t to j at time t}1
          //this is the prob of: 
          //  (being in state i at time t and observing everything up to and including t) times 
          //  (going from i to j) times 
          //  (being in state j at time t+1 and observing everything after t+1) times 
          //  (being in state j and observing obs[t+1])
          ln_xi[i][j] = lnProd(lnProd(lnProd(fv[t][i], ln_transition_mtx_[i][j]), ln_emissions_[j][obs[t+1]]), bv[t+1][j]);
          norm = lnSum(norm, ln_xi[i][j]);
        }
      }
      for(int i = 0; i < sc; i++){ 
        for(int j = 0; j < sc; j++) { 
          ln_xi[i][j] = lnDiv(ln_xi[i][j], norm);
        }
      }
      //now ln_xi contains the log prob of going from state i to j at times t and t+1
      for(int i = 0; i < sc; i++) { 
        double from_i_sum = lnZero();
        for(int j = 0; j < sc; j++) 
          from_i_sum = lnSum(from_i_sum, ln_xi[i][j]);
        ln_sum_fromi[i] = lnSum(ln_sum_fromi[i], from_i_sum);
        ln_sum_fromi_obsk[i][obs[t]] = lnSum( ln_sum_fromi_obsk[i][obs[t]] , from_i_sum );
      }
      for(int i = 0; i < sc; i++) { 
        for(int j = 0; j < sc; j++) { 
          ln_sum_itoj[i][j] = lnSum(ln_sum_itoj[i][j], ln_xi[i][j]);
        }
      }
      //done with time t
    
    
      if(t == 0) { 
        ln_init_state_probs_ = ln_sum_fromi;
        for(int i = 0; i < sc; i++)
          init_state_probs_[i] = exp(ln_init_state_probs_[i]);
      }
  
    }
  
    for(int i = 0; i < sc; i++) {
      for(int j = 0; j < sc; j++) {
        ln_transition_mtx_[i][j] = lnDiv(ln_sum_itoj[i][j], ln_sum_fromi[i]) ;
        transition_mtx_[i][j] = exp(ln_transition_mtx_[i][j]);
      }
    }
    for(int i = 0; i < sc; i++) { 
      for(int j = 0; j < oc; j++) { 
        ln_emissions_[i][j] = lnDiv(ln_sum_fromi_obsk[i][j] , ln_sum_fromi[i]);
        emissions_[i][j] = exp(ln_emissions_[i][j]);
      }
    }
  
    //emission layout is PROBABILITY = emissions_[FROM_STATE][OBSERVATION]
    //std::vector<std::vector<double>> emissions_;
    //std::vector<std::vector<double>> ln_emissions_;
      

  }


  static double lnZero() { 
    return -std::numeric_limits<double>::infinity();
  }


  //x -> e^x
  static double eexp(double v) { 
    
    //already returns 0.0 for -inf in c++ ......
    //
    //if(v == -std::numeric_limits<double>::infinity())
    //  return 0.0;

    return exp(v);
  }

  //x -> ln(x)
  static double eln(double v) { 
    
    //already returns -inf for 0.0 in c++ ......
    //
    //if(v == 0.0)
    //  return -std::numeric_limits<double>::infinity();

    return log(v);
  }
  
  //ln(x), ln(y) -> ln(x + y)
  static double lnSum(double ln_v0, double ln_v1) { 
    
    //should be ok with infs for now...
      
    //return log(exp(ln_v0) + exp(ln_v1));
    if(ln_v0 == ln_v1) { 
      //ln(x + x) = ln(2*x) = ln(x) + ln(2.0)
      return ln_v0 + log(2.0);    
    } else if(ln_v0 > ln_v1) { 
      //note: ln(x+y) = ln(x*(1.0 + x/y)) = ln_x + ln(1.0 + x/y) = ln_x + ln(1.0 + e^ln_x / e^ln_y) = ln_x + ln(1.0 + e^(ln_x - ln_y))
      return ln_v0 + log(1.0 + exp(ln_v1 - ln_v0));
    } else { //ln_v1 < ln_v0
      return ln_v1 + log(1.0 + exp(ln_v0 - ln_v1));
    }

  }
  static double lnSum2(double ln_v0, double ln_v1) { 
    return log(exp(ln_v0) + exp(ln_v1));
  }

  //ln(x), ln(y) -> ln(x) + ln(y) (== ln(x*y)) 
  static double lnProd(double ln_v0, double ln_v1) { 
    
    //-inf + x is already -inf in c++ .....

    return ln_v0 + ln_v1;
  }

  static double lnDiv(double ln_v0, double ln_v1) { 
    return ln_v0 - ln_v1;
  }


};



#endif
