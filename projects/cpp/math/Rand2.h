#ifndef RAND2_H_
#define RAND2_H_

#include <random>

struct Rand2 { 
 
  static std::mt19937 & localGen(int seed) { 
    //static std::mt19937 * gen = 0;
    static std::mt19937 * gen = new std::mt19937(seed);
    //if(!gen)
    //  gen = new std::mt19937(seed);
    return *gen;
  }

  static std::mt19937 & localGen() {
    return localGen(0);
  } 

  static double rand01() { 
    auto & gen = localGen();
    std::uniform_real_distribution<> dis(0, 1);
    return dis(gen);
  }

  //NOTE: this includes min, but excludes max ... ie result is on interval [min,max)  
  static double randAB(double min, double max) { 
    auto & gen = localGen();
    std::uniform_real_distribution<> dis(min, max);
    return dis(gen); 
  }
  
  static double normal(double std_dev = 1.0) { 
    auto & gen = localGen();
    std::normal_distribution<> dis(0.0, std_dev);
    return dis(gen);
  }

  


};

#endif
