#ifndef PVT_BINS_H_
#define PVT_BINS_H_

#include <cstdint>
#include <string>

struct Pvt { 
  int64_t p;
  int64_t v;
  int64_t ts;
  int64_t sv;
};

  
struct BinSpec { 

  double sum_thresh;
  
  double el_w;
  double v_w;
  double t_w;

  std::string toStr() const { 
    return "b[el:"+(el_w ? std::to_string(sum_thresh/el_w) : "_" )+
            ",v:"+(v_w ? std::to_string(sum_thresh/v_w) : "_")+
            ",t:"+(t_w ? std::to_string((sum_thresh/t_w) / 1000.0 / 1000.0 / 1000.0) : "_") +"]";
  }

};

#endif
