#ifndef AHEAD_BINS_H_
#define AHEAD_BINS_H_

#include <string>
#include <vector>
#include "PVTBins.h"
#include "../util/structures/SeqSeqRing.h"

//The bins are for looking into the future
//Using these is a little bit more complicated.
//
//First instead of just filling this structure with Pvt points
//and getting values, we must instead query the structure if it 
//needs more Pvt points. The way it works is it fills each bin until
//that bin's weighted sum _exceeds_ a threshold. After rebalancing we
//can query wether or not the rightmost bin is full.
//When it's all full we're ready to get future/lookahead values. 

class AheadBinsPVT { 
  struct BinInfo { 
    BinSpec spec;

    int64_t v_sum;
    int64_t sv_sum;
  
    void clear() { 
      v_sum = 0;
      sv_sum = 0;
    }

    std::string specStr() const {
      return spec.toStr();
    }
  };

  SeqSeqRing<Pvt> bins_;
  std::vector<BinInfo> info_;

public:

  AheadBinsPVT() {  }

  std::string specStr(int bin_id) const { 
    return info_[bin_id].specStr();
  }

  void clear() { 
    for(int i = 0; i < (int) bins_.grps(); i++)
      info_[i].clear();
    bins_.clear();
  }

  void addBin(const BinSpec & bs) {     
    bins_.newGrpPushBack();
    info_.push_back({bs, 0L, 0L});
  }

  void push(const Pvt & pvt)  {
    bins_.push(pvt);
    info_.back().v_sum += pvt.v;
    info_.back().sv_sum += pvt.sv;
  }

  double binSum(int idx) const { 
    if(bins_.grpEmpty(idx)) { 
      return info_[idx].v_sum * info_[idx].spec.v_w;
    } else { 
      auto & left = bins_.grpHead(idx);
      auto & right = bins_.grpNextOrTail(idx);//this uses the start of the rightmost bin
      return info_[idx].v_sum * info_[idx].spec.v_w +  (right.ts - left.ts) * info_[idx].spec.t_w + bins_.grpSize(idx) * info_[idx].spec.el_w;
    }
  }

  bool exceeds(int idx) const {
    return binSum(idx) > info_[idx].spec.sum_thresh;
  }
  
  //The first call should have use_last_exceeder set to false.
private:
  int last_exceeder_ = 0;
public: 
  
  void forcePop(int ct) { 
    last_exceeder_ = 0;
    bins_.grpGTEPopSzCheckCB(0, ct, [this](const Pvt & pvt, int bin_idx) { 
      info_[bin_idx].v_sum -= pvt.v;
      info_[bin_idx].sv_sum -= pvt.sv;
      //called for each bin thats removed
    });
  }

  bool empty() const { 
    return bins_.empty();
  }

  int64_t ts0() const { 
    return bins_.byIdx(0).ts;
  }

  //Move data in bins right to left
  //Returns true if the last bin satisfies the threshold. 
  //There's a small optimization that keeps us from re-calculating 
  //stats for bins if this is being called in a while loop and in conjunction
  //with forcePop
  bool rebalance(bool use_last_exceeder = true) { 
    int i = 0;
    if(use_last_exceeder) {
      i = last_exceeder_;
    } else { 
      last_exceeder_ = 0;
    }

    for(; i < (int) bins_.grps()-1; i++) { 
      while(1) { 
        //if we exceed the specs, move to the next bin
        if(exceeds(i)) {
          last_exceeder_ = i;
          break;
        }
        //move a new element into this bin using pop
        bool emptyb = (!bins_.grpGTEPopSzCheckCB(i+1,1, [this, i](const Pvt & pvt, int bin_idx) { 
          info_[bin_idx].v_sum -= pvt.v;
          info_[bin_idx].sv_sum -= pvt.sv;
          info_[i].v_sum += pvt.v;
          info_[i].sv_sum += pvt.sv;
        }));
        if(emptyb)
          return false; //no more data in other bins!
      }       
    }
    return exceeds(bins_.grps()-1);
  }

  //Sometimes bin0 is used as a buffer 
  //so that we're not focused on the events
  //immediately after the current time. However
  //we may want to move an elements that cause 
  //bin 0 to be an exceeder back to bin 1 since 
  //we might REALLY want to react to these. 
  void unpopBin0Exceeder() { 
    while(exceeds(0))
      bins_.revGrpPop(0);
  }

  int ct() const { 
    return bins_.grps();
  }

  int64_t vol(int idx) {
    return info_[idx].v_sum;
  }

  int64_t sv(int idx) { 
    return info_[idx].sv_sum;
  }

  int64_t deltaP(int idx) { 
    if(bins_.grpEmpty(idx))
      return 0;
    const auto & left = bins_.grpHead(idx);
    //const auto & right = bins_.grpTail(idx);
    const auto & right = bins_.grpNextOrTail(idx);
    return right.p - left.p;
  }
  
  int64_t deltaT(int idx) {   
    if(bins_.grpEmpty(idx))
      return 0;
    const auto & left = bins_.grpHead(idx);
    //const auto & right = bins_.grpTail(idx);
    const auto & right = bins_.grpNextOrTail(idx);
    return right.ts - left.ts;
  }

  int szi(int idx) { 
    return bins_.grpSize(idx);
  }

  bool empty(int idx) const { 
    return bins_.grpEmpty(idx); 
  }

  int64_t headP(int idx) { 
    if(bins_.grpEmpty(idx))
      return 0;
    return bins_.grpHead(idx).p;
  }

  int64_t headT(int idx) { 
    if(bins_.grpEmpty(idx))
      return 0;
    return bins_.grpHead(idx).ts;
  }

};



#endif
  
