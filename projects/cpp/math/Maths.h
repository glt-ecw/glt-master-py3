#ifndef MATHS_H_
#define MATHS_H_

#include <vector>
#include <cstdint>
#include <cmath>
#include <stdexcept>

namespace Maths {

struct Funcs { 

  static double sgn(double x) { 
    if(x >= 0.0)
      return 1.0;
    return -1.0;
  }
  static double sgnZ(double x) { 
    if(x > 0.0)
      return 1.0;
    if(x < 0.0)
      return -1.0;
    return 0.0;
  }
  static int sgnZi(double x) { 
    if(x > 0.0)
      return 1;
    if(x < 0.0)
      return -1;
    return 0;
  }
  static int sgnZi(int x) { 
    if(x > 0)
      return 1;
    if(x < 0)
      return -1;
    return 0;
  }
  static double abs(double x) { 
    if(x >= 0.0)
      return x;
    return x;
  }

};



//This class is a convenience class for providing a view into a single series of floats/doubles/ints with 
//some kind of stride and offset. It has some convenience functions for strictly increasing series that 
//allow us to get a linearly interpolated value at some location. We can also use binary search to find 
//the location of a value in the series. This may be useful if this series contains something like timestamps.
template <typename T>
struct ArrInterp { 
 
  T * arr = 0; 
  long int idx0 = 0L;
  double w_p0 = 1.0;
  double w_p1 = 0.0;
  long int stride = 1L;
  long int offs = 0L;
  long int ct = 0L;

  const T & operator[](long int idx) const { 
    return arr[stride*idx + offs];
  }

  const T & head() const { 
    //if(ct <= 0L || !arr)
    //  throw std::runtime_error("Trying to get head in empty arrinterp. ct: "+std::to_string(ct) + " ptr==Null:"+((!arr) ?"true":"false"));
    return arr[offs];
  }

  const T & tail() const { 
    //if(ct <= 0L || !arr)
    //  throw std::runtime_error("Trying to get tail in empty arrinterp. ct: "+std::to_string(ct) + " ptr==Null:"+((!arr) ?"true":"false"));
    return arr[stride*(ct-1L) + offs];
  }

  bool empty() const {
    return ct <= 0L;
  }
  
  //binary search that assumes an increasing series. It sets the member variable idx0 so that
  //the equation (*this)[idx0] <= target <= (*this)[idx0+1] is satisfied. If there's multiple
  //values for idx0 that satisfy this it is unspecified which one will be returned.  
  bool binSearchIncr(const T & target) {     
    if(ct <= 0L || !arr) 
      throw std::runtime_error("Trying to search in invalid arrinterp. ct: "+std::to_string(ct) + " ptr==Null:"+((!arr) ?"true":"false"));
    
    if(target < head()) {
      idx0 = -1;
      return false;
    }
    
    if(target > tail()) { 
      idx0 = ct;
      return false;
    }

    if(ct == 1)  {
      //edge case... be careful here
      idx0 = 0;
      w_p0 = 1.0;
      w_p1 = 0.0;
      return;
    }
    
    long int min = 0;
    long int max = ct-1;
    long int mid = ct / 2;
    
    for(int i = 0; i < 2*ct; i++) { 
      if(target < arr[stride*mid+offs]) { 
        //mid is indexing into something greater than target
        //so we need to look at lower mids... so our new max is mid
        max = mid;
      } else if(target > arr[stride*(mid+1)+offs]) { 
        //mid is indexing into something less than target
        //so our new min is mid
        min = mid; 
      } else {
        //we've found a point (mid) such that the point at 
        //mid is LTE the target and the point at mid+1 is GTE to target
        idx0 = mid;
        double v0_to_target = target - arr[stride*idx0+offs];
        double v0_to_v1 = arr[stride*(idx0+1L)+offs] - arr[stride*idx0+offs];        
        w_p1 = v0_to_v1 != 0.0 ? v0_to_target / v0_to_v1 : 0.0;
        w_p0 = 1.0 - w_p1;
        return true;///DONE!
      }
    }
    throw std::runtime_error("searchIncrArrFor failed after " + std::to_string(2L*ct) + 
          " iterations looking for target that is in bounds. min: " + std::to_string(mid) + 
          " max: " + std::to_string(max)); 
    return false;
  }

  //Start linearly searching for target from the current idx0.
  //This is useful if we're iterating through one series with another
  //series. 
  bool nextIncrInterp(const T & target) {
    long int i = idx0;
    if(i <= 0)
      i = 0;
    for(; i < ct-1L; i++) { 
      if((*this)[i] <= target && target <= (*this)[i+1]) { 
        //found :)
        idx0 = i;   
        double v0_to_target = target - arr[stride*idx0+offs];
        double v0_to_v1 = arr[stride*(idx0+1L)+offs] - arr[stride*idx0+offs];        
        w_p1 = v0_to_v1 != 0.0 ? v0_to_target / v0_to_v1 : 0.0;
        w_p0 = 1.0 - w_p1;
        return true;///DONE!
      } else if((*this)[i] >= target) { 
        //increasing i will only make things worse!
        return false;
      }
    }
    //went past the end and didn't find anything
    idx0 = ct;
    return false; 
  }
  

  bool validIdx() const { 
    return idx0 >= 0 && idx0 < ct;
  }
  

  T interpValAtOffs(long int offs2) const { 
    if(ct == 1) 
      return arr[idx0 * stride + offs2] * w_p0;
    return arr[idx0 * stride + offs2] * w_p0 + arr[(idx0 + 1L) * stride + offs2] * w_p1;
  }




  struct Stat { 
    double sum = 0;
    double sumsq = 0;
    double avg = 0;
    double var = 0;
    double stddev = 0;
    long int ct = 0;
  };


  Stat calcStats() const { 
    Stat ret;
    ret.ct = ct;
    for(long int i = 0; i < ct; i++) { 
      ret.sum += (*this)[i];
      ret.sumsq += (*this)[i]*(*this)[i];
    }
    ret.avg = ret.sum / (double) ct;
    ret.var = ret.sumsq / (double) ct - ret.avg * ret.avg;
    ret.stddev = sqrt(ret.var);
  }
  

};

typedef ArrInterp<float> ArrInterpF; 
typedef ArrInterp<double> ArrInterpD; 






};




#endif
