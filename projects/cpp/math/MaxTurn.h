#ifndef MAX_TURN_H_
#define MAX_TURN_H_

#include "../util/structures/FixedElRing.h"

class MaxDownTurn { 
public:
  struct Pt { 
    double v;
    int64_t ts;

    double operator-(const Pt & r) const { 
      return v - r.v;
    }
    bool operator<(const Pt & r) const { 
      return v < r.v;
    }  
    bool operator<=(const Pt & r) const { 
      return v <= r.v;
    }  
  };


private:

  //we add stuff at the tail, therefore stuff at the tail has higher ts
  typedef FixedElRing<Pt> Ring;
  FixedElRing<Ring> downs_; 
  int size_ = 0;

public:

  int size() const {
    return size_;
  }

  void push(Pt x) { 
    if(!downs_.empty() && x <= downs_.tail().tail()) {
      downs_.tail().pushSafe(x);
      size_++;
      //printf("go\n");
      while(downs_.size() >= 2) { 
        Ring & p = downs_.revIdx(1);
        Ring & c = downs_.tail();
        //for each thing we have head > tail
        //printf("%lf %i %lf %i\n", c.head() - c.tail(), c.size(), p.head() - p.tail(), p.size());
        if(c.head() - c.tail() >= p.head() - p.tail()) {
          //printf("rem\n");
          //diff in c is greater than p, so p is useless so replace p with c 
          //doing swap instead of p=c since it's faster
          size_ -= p.size();
          //p = c;
          p.swap(c);
          downs_.popTailKnown();
        } else 
          break;
      }
    } else if(!downs_.empty() && downs_.tail().size() == 1) { 
      downs_.tail().head() = x;
    } else { 
      Ring d(1);
      d.pushSafe(x);
      downs_.pushSafe(d);
      size_++;
    }
  }

  void clear() { 
    for(int i = 0; i < downs_.size(); i++) { 
      downs_[i].clear();
    }
    downs_.clear();
  }

  void pop() { 
    if(downs_.empty())
      return;
    downs_.head().popSafe();
    size_--;
    if(downs_.head().empty())
      downs_.popKnown();
    else if(downs_.size() >= 2) {
      Ring & p = downs_[0];
      Ring & c = downs_[1];
      if(c.head() - c.tail() >= p.head() - p.tail()) {
        size_ -= p.size();
        downs_.popKnown();
      } 
    }
  }

  bool empty() const { 
    return downs_.empty();
  }

  std::pair<Pt,Pt> maxDown() const { 
    return {downs_.head().tail(), downs_.head().head()};
  }

  double maxDv() const { 
    if(empty())
      return 0.0;
    return downs_.head().tail().v - downs_.head().head().v;
  }

  int drops() const { 
    return downs_.size();
  }

  //the time duration of the largest downturn 
  int64_t maxDt() const { 
    if(empty())
      return 0;
    return downs_.head().tail().ts - downs_.head().head().ts;
  }

  //the timestamp of the oldest element of the largest downturn
  int64_t maxTL() const { 
    if(empty())
      return 0;
    return downs_.head().head().ts;
  }


  int64_t dt() const { 
    if(empty())
      return 0;
    return downs_.tail().tail().ts - downs_.head().head().ts;
  }

};



#endif
