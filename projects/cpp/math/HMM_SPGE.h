#ifndef HMM_SPGE_H_
#define HMM_SPGE_H_

//Since the typical HMM with N states has N^2 transitions, that places 
//a huge time penalty on most algorithms. This sparse implementation uses
//a directed graph so that 0 probability transitions do not contribute to 
//the runtime. 

#include <cmath>
#include <limits>
#include <vector>
#include "Rand2.h"
#include "nets/DGraph.h"

class HMM_SPGE { 
  
  std::vector<double> init_state_probs_;
  std::vector<double> ln_init_state_probs_;

  //the probability of going from one node to another node
  //is represented using a directed graph structure. The edge
  //structure contains that probability. This is allows optimizing 
  //for a sparse transition probabilities. 
  struct Node {
  };
  struct Edge {
    double prob;
    double lnProb;
  };
  //In addition to the connections represented by the graph, there
  //is an additional (potentially redundant) connection going from 
  //every node to every node. 
  std::vector<double> jumpProb_;
  std::vector<double> lnJumpProb_;

  //graph describing valid transitions.
  //these may easily contain loops. this is used instead
  //of the typical matrix, since we are optimizing for a 
  //sparsely connected graph. for a lot of cases this make 
  //sense (imagine a huge 2D or 3D grid where we can only 
  //jump to a few nearest nodes from a given node). 
  DGraph<Node,Edge> g_;

  //emission layout is PROBABILITY = emissions_[FROM_STATE]
public:
  struct EmitSpec { 
    std::vector<double> dot; 
    double mean;
    double var;
    double a;
    double b;
    int how;

    static const int HOW_DIST = 0;
    static const int HOW_DOT = 1;
    static const int HOW_REG = 2;
  
    void howDist() { 
      how = HOW_DIST;
    }
    void howDot() { 
      how = HOW_DOT;
    }
    void howReg() { 
      how = HOW_REG;
    }

    double calcX(const double * xp) const { 
      double x = 0.0;
      if(how == HOW_DIST) { 
        for(int i = 0; i < (int) dot.size(); i++)
          x += (xp[i] - dot[i])*(xp[i] - dot[i]);
        x = sqrt(x);
      } else if(how == HOW_DOT) { 
        for(int i = 0; i < (int) dot.size(); i++) 
          x += xp[i] * dot[i];
      } else if(how == HOW_REG) { 
        for(int i = 0; i < (int) dot.size()-1; i++)
          x += xp[i] * dot[i];        
        x += dot.back(); //the last parameter of dot is a per-state offset - usually leave this at 0
        x = x - xp[dot.size()-1]; //mean is a global offset, which is kind of redundant - usually leave this at 0
      }
      return x;
    }

    //the pdf for these specs is:
    double pdf(const double * xp) const { 
      double x = calcX(xp);
      return 1.0 / sqrt(2.0 * var * M_PI) * exp(-(x - mean)*(x - mean) / (2.0 * var));  
    }
    double lnPdf(const double *  xp) const { 
      double x = calcX(xp);
      return -0.5 * log(2.0 * var * M_PI) - (x-mean)*(x-mean)/(2.0 * var);
      //return eln(pdf(xp));
    }
    //possible for future optimizations:
    void updateAB() { 
      a = -0.5 * log(2.0 * var * M_PI);
      b = -1.0 / (2.0 * var);
    }
    double lnPdfAB(const double * xp) const { 
      double x = calcX(xp);
      return a + (x-mean)*(x-mean)*b;
    }
  
  };
private:
  std::vector<EmitSpec> emissions_;

public:
 
  void deepCopy(const HMM_SPGE & other) { 
    init_state_probs_ = other.init_state_probs_;
    ln_init_state_probs_ = other.ln_init_state_probs_;
    jumpProb_ = other.jumpProb_;
    lnJumpProb_ = other.lnJumpProb_;
    emissions_ = other.emissions_;
    g_.deepCopyOther(other.g_);
  }

  HMM_SPGE(const HMM_SPGE & other) { 
    deepCopy(other);
  }

  HMM_SPGE & operator=(const HMM_SPGE & other) { 
    deepCopy(other);
    return *this;
  }

  ~HMM_SPGE() { 
    g_.deleteAll();
  }

  HMM_SPGE() { }

  HMM_SPGE(int states, int ndim) { 
    init_state_probs_.resize(states, 1.0 / (double) states);
    ln_init_state_probs_.resize(states, eln(1.0 / (double) states));
    emissions_.resize(states);
    for(auto & m : emissions_) {
      m.mean = 0.0;
      m.var = 1.0;
      m.dot.resize(ndim, (double) 1.0);
    }
    for(int i = 0; i < states; i++)
      g_.addNode();
    jumpProb_.resize(stateCt(),(double) 0.0);
    lnJumpProb_.resize(stateCt(), (double) lnZero());
  }

  void setFromToProb(int from_id, int to_id, double prob) {
    if(prob == 0.0) {
      g_.deleteAllEdgesFromTo(from_id, to_id);
      return;
    }
    if(from_id < 0 || to_id < 0 || from_id >= stateCt() || to_id >= stateCt()) { 
      printf("ERROR: from_id %i to_id %i stateCt %i\n", from_id, to_id, stateCt());
      fflush(stdout);
    }
    Edge * e = g_.eDataCreate(from_id, to_id);
    e->prob = prob;
    e->lnProb = eln(prob);
  }
  void setTransP(int from_id, int to_id, double prob) {
    setFromToProb(from_id, to_id, prob);
  }
  void setJumpProb(double p) { 
    jumpProb_.resize(0);
    jumpProb_.resize(stateCt(), p);
  }

  //make sure stateCt() = res*res
  void xyRegGridSetupHelper(int res, double base_transp, double x1_transp, double y1_transp, double stay_transp, double xscale, double yscale, double var) { 
    double dres = res;
    for(int i = 0; i < res; i++) { 
      for(int j = 0; j < res; j++) { 
        int ic0 = i * res + j;
        for(int i2 = 0; i2 < res; i2++) { 
          for(int j2 = 0; j2 < res; j2++) { 
            int ic2 = i2 * res + j2;
            if((i == i2) && (j == j2)) {
              setFromToProb(ic0,ic2, stay_transp);
            } else if(i == i2 && (j == j2+1 || j == j2-1)) { 
              if(j == 0 || j == res-1)
                setFromToProb(ic0,ic2, 2*x1_transp);
              else  
                setFromToProb(ic0,ic2, x1_transp);
            } else if((j == j2) && (i == i2+1 || i == i2-1)) {        
              if(i == 0 ||( i == res-1))
                setFromToProb(ic0,ic2, 2*y1_transp);           
              else    
                setFromToProb(ic0,ic2, y1_transp);           
            }
          }
        }
        double di = (double)i/(dres-1.0) - 0.5;
        double dj = (double)j/(dres-1.0) - 0.5;
        di *= yscale*2.0;
        dj *= xscale*2.0;
        emitP(ic0).dot = {di, dj, 0.0};
        //printf("%lf %lf %lf\n", di, dj, var);
        emitP(ic0).howReg();
        emitP(ic0).mean = 0.0;
        emitP(ic0).var = var;
      }
    }
    jumpProb_.resize(0);
    jumpProb_.resize(stateCt(),(double) base_transp);
    normalizeProbsCalcLog();
  }
  
  //make sure stateCt() = res*res*res
  void xyzRegGridSetupHelper(int res, double base_transp, double x1_transp, double y1_transp, double z1_transp, double stay_transp, double xscale, double yscale, double zscale, double var) { 
    double dres = res;
    for(int i = 0; i < res; i++) { 
      for(int j = 0; j < res; j++) { 
        for(int k = 0; k < res; k++) { 
          int ic0 = i * res *res + j * res + k;
          for(int i2 = 0; i2 < res; i2++) { 
            for(int j2 = 0; j2 < res; j2++) { 
              for(int k2 = 0; k2 < res; k2++) { 
                int ic2 = i2 * res * res + j2 * res + k2;
                if((i == i2) && (j == j2) && (k == k2)) {
                  setFromToProb(ic0,ic2, stay_transp);
                } else if(i == i2 && k == k2 && (j == j2+1 || j == j2-1)) { 
                  if(j == 0 || j == res-1)
                    setFromToProb(ic0,ic2, 2*x1_transp);
                  else  
                    setFromToProb(ic0,ic2, x1_transp);
                } else if(j == j2 && k == k2 && (i == i2+1 || i == i2-1)) {        
                  if(i == 0 ||( i == res-1))
                    setFromToProb(ic0,ic2, 2*y1_transp);           
                  else    
                    setFromToProb(ic0,ic2, y1_transp);           
                } else if(i == i2 && j == j2  && (k == k2+1 || k == k2-1)) { 
                  if(k == 0 ||( k == res-1))
                    setFromToProb(ic0,ic2, 2*z1_transp);           
                  else    
                    setFromToProb(ic0,ic2, z1_transp);           
                }
              }
            }
          }
          double di = (double)i/(dres-1.0) - 0.5;
          double dj = (double)j/(dres-1.0) - 0.5;
          double dk = (double)k/(dres-1.0) - 0.5;
          di *= yscale*2.0;
          dj *= xscale*2.0;
          dk *= zscale*2.0;
          emitP(ic0).dot = {di, dj, dk, 0.0};
          //printf("%lf %lf %lf\n", di, dj, var);
          emitP(ic0).howReg();
          emitP(ic0).mean = 0.0;
          emitP(ic0).var = var;
        }        
      }
    }
    jumpProb_.resize(0);
    jumpProb_.resize(stateCt(),(double) base_transp);
    normalizeProbsCalcLog();
  }
  
  int stateCt() const { 
    return init_state_probs_.size();
  }
  double & initP(int i) { 
    return init_state_probs_[i];
  }
  EmitSpec & emitP(int i) { 
    return emissions_[i];
  }
  int obsCt() { 
    return emissions_[0].dot.size();
  }
  
  void normalizeProbsCalcLog() { 
    const int sc = stateCt();
    double s = 0.0;
    for(int i = 0; i < sc; i++) {
      if(init_state_probs_[i] < 0.0)
        init_state_probs_[i] = 0.0;
      s += init_state_probs_[i];
    }
    if(!std::isfinite(s) || s <= 0.0) { 
      printf("ERROR! Init state prob normalizer is not valid: %lf\n", s);      
    }
    for(int i = 0; i < sc; i++) {
      init_state_probs_[i] /= s;
      ln_init_state_probs_[i] = log(init_state_probs_[i]);
    }
    
    for(int i = 0; i < sc; i++) {
      s = jumpProb_[i];
      g_.eachChildEdge_NodeRawIdx_LV(i, [&s](Edge & edge) { 
            if(edge.prob < 0.0)
              edge.prob = 0.0;
            s+= edge.prob;
          });

      if(!std::isfinite(s) || s <= 0.0) { 
        printf("ERROR! Transition from %i state prob normalizer is not valid: %lf\n", i, s);      
      }

      g_.eachChildEdge_NodeRawIdx_LV(i, [&s](Edge & edge) { 
            edge.prob /= s;
            edge.lnProb = eln(edge.prob);
          });

      jumpProb_[i] /= s;
      lnJumpProb_[i] = eln(jumpProb_[i]);
    }
  }



  //just do lnSum over all the logProbFB to get the total 
  //log probability of a given observation...
  //
  //return Alpha-t ... probability of observing obs[0],obs[1],obs[2] ... obs[t] at times 0 through t while being in state i at time t
  //the length of the returned vecvec will be t+1
  std::vector<std::vector<double>> logProbForw(const double * obs, const int t) { 
    const int sc = stateCt();
    const int oc = obsCt();
    std::vector<double> ret;
    std::vector<double> tmp;
    ret.resize(sc);
    tmp.resize(sc);
    std::vector<std::vector<double>> all_probs;
    for(int i = 0; i < sc; i++) 
      ret[i] = lnProd( ln_init_state_probs_[i] , emissions_[i].lnPdf(obs+0) );
    all_probs.push_back(ret);
    
    for(int o = 1; o <= t; o++) { 
      
      //there's a fixed minimum probability of jumping from all the states 
      //to one state collectively - 
      double jump_prob = lnZero();
      for(int hj = 0; hj < (int) ret.size(); hj++)
        jump_prob = lnSum(jump_prob, lnProd(ret[hj], lnJumpProb_[hj]));

      for(int j = 0; j < sc; j++) { 
        //j is the state we're going to 
        tmp[j] = jump_prob;
      }
      g_.eachEdgeWithSrcDstNodeIdsLV([&tmp, &ret](const Edge & e, int src_id, int dst_id) { 
            tmp[dst_id] = lnSum(tmp[dst_id], lnProd(ret[src_id], e.lnProb));
          });
      for(int j = 0; j < sc; j++) { 
        //going to j and observint obs[o]
        tmp[j] = lnProd(tmp[j], emissions_[j].lnPdf(obs+o*oc));
      }
      ret.swap(tmp);
      all_probs.push_back(ret);
    }
    return all_probs;
  }

  std::vector<double> probForwBackNorm(const double * obs, const int t) { 
    auto r = logProbForw(obs, t).back();
    double norm = lnZero();
    for(auto rel : r) 
      norm = lnSum(norm, rel);
    double psum = 0.0;
    for(auto & rel : r) { 
      rel = exp(lnDiv(rel, norm));
      psum += rel;
    }
    for(auto & rel : r)
     rel /= psum;
    return r; 
  }

  
  //return Beta-t ... probability of observering obs[t0+1],obs[t0+2]...obs[tL] at time's t0+1 through tL, while being in state obs[t0] at time t0
  //the length of the returned vecvec will be tL - t0 + 1
  std::vector<std::vector<double>> logProbBack(const double * obs, const int t0, const int tL) { 

    const int sc = stateCt();
    const int oc = obsCt();
    std::vector<double> ret;
    std::vector<double> tmp;
    ret.resize(sc);
    tmp.resize(sc);
    std::list<std::vector<double>> all_probs;
    std::vector<std::vector<double>> all_probs_ret;
    //setup B_tL (this is the probability of not observing anything - thinking inductively it makes sense to set this to 1.0)
    for(int i = 0; i < sc; i++) 
      ret[i] = log(1.0);
    //B_(t-1) is the probability of observing obs[t] through obs[tL] and being in some state at time t-1
    //inductively we have to look at all the ways to go from s_[t-1] to each possible state at time t, take into account
    //the different possible observation densities at the different states at time t and the inductive probability for the those states
    all_probs.push_front(ret);
    
    for(int o = tL-1; o >= t0; o--) { 
      
      double jump_prob = lnZero();
      for(int hj = 0; hj < (int) ret.size(); hj++)
        jump_prob = lnSum(jump_prob, lnProd( emissions_[hj].lnPdf(obs+(o+1)*oc), ret[hj]));
      
      for(int i = 0; i < sc; i++) { 
        //j is the state we're going to, we're jumping from i to j, the base jump probability affects that  
        tmp[i] = lnProd(jump_prob, lnJumpProb_[i]);
      }
    
      g_.eachEdgeWithSrcDstNodeIdsLV([&tmp, &ret, this, obs, o, oc](const Edge & e, int src_id, int dst_id) { 
            tmp[src_id] = lnSum(tmp[src_id], lnProd(lnProd(e.lnProb, emissions_[dst_id].lnPdf(obs+(o+1)*oc)), ret[dst_id]));
          });        
        //for(int j = 0; j < sc; j++) { 
        //  //going from i to j
        //  tmp[i] = lnSum( tmp[i], lnProd(lnProd(ln_transition_mtx_[i][j], emissions_[j].lnPdf(obs+(o+1)*oc)), ret[j]));
        //}
      //}
   
      ret.swap(tmp);
      all_probs.push_front(ret);
    }
    for(const auto & p : all_probs)
      all_probs_ret.push_back(p);
     return all_probs_ret;
  }
  

  //get the probability of being in state i at time t give obs[0],obs[1] ... obs[tL] 
  std::vector<std::vector<double>> logProbState(const double * obs, const int tL) { 
    const int sc = stateCt();
    std::vector<double> ret;
    std::vector<std::vector<double>> all_ret;
    ret.resize(sc);
    
    auto fv = logProbForw(obs, tL);//.back();
    auto bv = logProbBack(obs, 0, tL);//.front();
    
    for(int o = 0; o <= tL; o++) { 
      double ln_denom = lnZero();
      for(int i = 0; i < sc; i++)
        ln_denom = lnSum(ln_denom, lnProd(fv[o][i], bv[o][i]));

      for(int i = 0; i < sc; i++)
        ret[i] = lnDiv(lnProd(fv[o][i], bv[o][i]) , ln_denom);
    
      all_ret.push_back(ret);
    }

    return all_ret;
  }
  
  double logProbData(const double * obs, const int tL) { 
    //const auto r = logProbState(obs, tL);
    const auto r = logProbForw(obs, tL);//.back();
    const int sc = stateCt();
    double sum = lnZero();
    for(int i = 0; i < sc; i++) { 
      sum = lnSum(sum, r.back()[i]);
    }
    return sum;
  }

  //pick the most likely state using the logProbState 
  std::vector<int> maxProbState(const double * obs, const int tL) { 
    //TODO: do we use the forward or raw prob of the state here?
    auto r = logProbState(obs, tL);
    std::vector<int> ret;
    const int sc = stateCt();

    for(int o = 0; o <= tL; o++) { 
      double maxv = r[o][0];
      int maxi = 0;
      for(int i = 1; i < sc; i++) { 
        if(r[o][i] >= maxv) { 
          maxv = r[o][i];
          maxi = i;
        }
      }
      ret.push_back(maxi);
    }
    return ret;
  }
  
  std::vector<double> wProbState(const double * obs, const int tL) { 
    auto r = logProbState(obs, tL);
    std::vector<double> ret;
    const int sc = stateCt();

    for(int o = 0; o <= tL; o++) { 
      double v = 0.0;
      double psum = 0.0;
      for(int i = 0; i < sc; i++) { 
        v += exp(r[o][i]) * (double) i;
        psum += exp(r[o][i]);
      }
      v /= psum;
      ret.push_back(v);
    }
    return ret;
  }
  std::vector<double> wProbDot(const double * obs, const int tL) { 
    auto r = logProbState(obs, tL);
    std::vector<double> ret;
    std::vector<double> tmp_ev;
    const int sc = stateCt();
    const int oc = obsCt();

    for(int o = 0; o <= tL; o++) { 
      tmp_ev.resize(0);
      tmp_ev.resize(oc, (double) 0.0);
      double psum = 0.0;
      for(int i = 0; i < sc; i++) { 
        for(int j = 0; j < oc; j++)
          tmp_ev[j] += exp(r[o][i]) * (double) emissions_[i].dot[j];
        psum += exp(r[o][i]);
      }
      for(int j = 0; j < oc; j++) {
        tmp_ev[j] /= psum;
        ret.push_back(tmp_ev[j]);
      }
    }
    return ret;
  }

  std::vector<double> wForwProbState(const double * obs, const int tL) {
    auto r = logProbForw(obs, tL);
    
    std::vector<double> ret;
    const int sc = stateCt();

    for(int o = 0; o <= tL; o++) {
      double v = 0.0;
      double pmax = std::numeric_limits<double>::lowest();
      for(int i = 0; i < sc; i++) {
        if(r[o][i] > pmax)
          pmax = r[o][i];
      }
      double psum = 0.0;
      for(int i = 0; i < sc; i++) {
        v += exp(r[o][i] - pmax) * (double) i;
        psum += exp(r[o][i] - pmax);
      }
      v /= psum;
      ret.push_back(v);
    }
    return ret;
  }
  
  std::vector<double> wForwProbDot(const double * obs, const int tL) {
    auto r = logProbForw(obs, tL);
    
    std::vector<double> tmp_ev;
    std::vector<double> ret;
    const int sc = stateCt();
    const int oc = obsCt();

    for(int o = 0; o <= tL; o++) {
      tmp_ev.resize(0);
      tmp_ev.resize(oc, (double) 0.0);
      double pmax = std::numeric_limits<double>::lowest();
      for(int i = 0; i < sc; i++) {
        if(r[o][i] > pmax)
          pmax = r[o][i];
      }
      double psum = 0.0;
      for(int i = 0; i < sc; i++) {
        for(int j = 0; j < oc; j++)
          tmp_ev[j] += exp(r[o][i] - pmax) * (double) emissions_[i].dot[j];
        psum += exp(r[o][i] - pmax);
      }
      for(int j = 0; j < oc; j++) {
        tmp_ev[j] /= psum;
        ret.push_back(tmp_ev[j]);
      }
    }
    return ret;
  }
  



  //find the most likely state sequence give obs[0],obs[1],obs[2] ... obs[t]
  //returns a vector whose index i contains the highest single path probability of 
  //reaching state i at time t 
  //There's a bit of an issue here where if two source states are equally likely, the 
  //current implementation biases to picking the one with the higher index. Something 
  //that picks in a random but deterministic way might be better. This can be observed
  //in HMM's with many states that can jump all over the place.
  //The first component of a returned pair specifies the probability of this state path. 
  std::vector<std::pair<double, int>> logViterbi(const double * obs, const int t) { 
    const int sc = stateCt();
    const int oc = obsCt();
    std::vector<double> ret; //max probability among all paths ending in state [j]
    ret.resize(sc);
    std::vector<double> tmp;
    tmp.resize(sc);
    std::vector<int> psi;
    psi.resize(sc);
    std::vector<std::vector<double>> prob_bt;
    std::vector<std::vector<int>> psi_bt;
    std::list<std::pair<double, int>> ml_path_list;
    std::vector<std::pair<double, int>> ml_path;
    //std::vector<int> psi; //
    //psi.resize(sc);
    for(int i = 0; i < sc; i++) {
      ret[i] = lnProd( ln_init_state_probs_[i] , emissions_[i].lnPdf(obs+0) );
      psi[i] = -1;
    }
    prob_bt.push_back(tmp);
    psi_bt.push_back(psi);

    for(int o = 1; o <= t; o++) { 
      //for each state j, we want to find the most likely
      //path going to that state from any of the other states
      //i, this way we can get a most likely path for each state
      
      //maximum of minimum probs of jumping to some state from any other state. this may "bridge" some 
      //unlikely paths so it should be taken into account even though it's effect may appear small.
      double max_jump_prob = lnZero();
      int max_jump_i = -1;
      for(int hj = 0; hj < (int) ret.size(); hj++) {
        double jump_prob = lnProd(ret[hj], lnJumpProb_[hj]);
        if(jump_prob > max_jump_prob) {
          max_jump_prob = jump_prob;
          max_jump_i = hj;
        }
      }

      
      for(int j = 0; j < sc; j++) { 
        int maxi = -1;
        double maxv = std::numeric_limits<double>::lowest();
        g_.eachEdgeToRawIdx_WithSrcNodeIdsLV(j, [&maxv, &maxi, &ret, this](const Edge & e, int src_id) {
              double tv = lnSum(lnProd(ret[src_id], e.lnProb), lnProd(lnJumpProb_[src_id], ret[src_id]));
              //double tv = lnProd(ret[src_id], e.lnProb);
              if(tv > maxv) { 
                maxv = tv;
                maxi = src_id;
              }
            }); 
      //  //going from 0 to j
      //  int maxi = 0;
      //  double maxv = lnProd(ret[0], ln_transition_mtx_[0][j]);
      //  //now compare to going from i to j;
      //  for(int i = 1; i < sc; i++) { 
      //    double tv = lnProd(ret[i], ln_transition_mtx_[i][j]);
      //    if(tv > maxv) { 
      //      maxv = tv;
      //      maxi = i; 
      //    }
      //  }
        //most likely state that we came from is now maxi
        //most likely probability is maxv
        //we need to check if there is a jump shortcut first
        if(max_jump_prob > maxv) {
          maxv = max_jump_prob;
          maxi = max_jump_i;
        }
        tmp[j] = lnProd(maxv, emissions_[j].lnPdf(obs+o*oc));
        psi[j] = maxi;
      }

     


      ret.swap(tmp);
      
      prob_bt.push_back(ret);
      psi_bt.push_back(psi);
    }
    //we have a whole bunch of vec's of vec's which contains 
    //the max probability of being in state j at time idx and 
    //the previous state which gives that max 
    double maxv = prob_bt[prob_bt.size()-1][0];
    int maxi = 0;
    for(int i = 1; i < sc; i++) { 
      if(prob_bt[prob_bt.size()-1][i] > maxv) { 
        maxv = prob_bt[prob_bt.size()-1][i];
        maxi = i;
      }
    }
    ml_path_list.push_front({maxv, maxi});
    
    for(int o = (int) psi_bt.size()-1; o >= 1; o--){ 
      maxi = psi_bt[o][maxi];
      //psi_bt[i][maxi] gives us the previous state that 
      //led to this current max path 
      maxv = prob_bt[o-1][maxi];
      ml_path_list.push_front({maxv, maxi});  
    
    }
    
    for(auto a : ml_path_list)
      ml_path.push_back(a);

    return ml_path;
  }
  
  std::vector<double> viterbiDot(const double * obs, const int t) { 
    auto v = logViterbi(obs, t);
    std::vector<double> ret;
    for(int i = 0; i < (int) v.size(); i++) { 
      for(int j = 0; j < obsCt(); j++)
        ret.push_back(emitP(v[i].second).dot[j]);
    }
    return ret;
  }
  
  //This uses the forward and backward results to compute the 
  //probability of going from state i->j at times t->t+1 for any obs sequence .
  //We can measure the number of jumps between states to optimize the transition
  //probabilities. 
  void baumWelch(const double * obs, int tL) { 
    //TODO: finish implementing this 
    const int sc = stateCt();
    const int oc = obsCt();
    (void) oc;
    std::vector<std::vector<double>> ln_xi; //prob of going from state i to state j at time t
    ln_xi.resize(sc);
    for(auto & x : ln_xi)
      x.resize(sc,  lnZero());
    std::vector<std::vector<double>> ln_sum_itoj;
    ln_sum_itoj = ln_xi;
    std::vector<double> ln_sum_fromi;
    ln_sum_fromi.resize(sc, lnZero());
    
    //std::vector<std::vector<double>> ln_sum_fromi_obsk;
    //ln_sum_fromi_obsk.resize(sc);
    //for(auto & ls : ln_sum_fromi_obsk)
    //  ls.resize(oc, lnZero());
    //
    //If we want to re-estimate the emission parameters, we no longer have 
    //states we can sum up probabilities for. Actually we could discretize 
    //each gaussian and re-estimate the mean and variance after the fact from
    //this discretization. A more correct way would be to calculate E(X) and E(X^2)
    //for each state. That may actually be somewhat reasonable once everything has 
    //been normalized.   

    auto fv =  logProbForw(obs, tL);
    auto bv =  logProbBack(obs, 0, tL);

    for(int t = 0; t < tL; t++) { 
      double norm = lnZero();
      //ln_xi that are at 0 will stay at zero because there are no edges there
      //NOTE: we're ignoring the implicit edges due to the "jump" probability, 
      //but we can definitely factor them in. That could easily result in us 
      //needing to generate new connection and having this turn into an O(n^4) 
      //non-sparsely connected graph. or we could do some type of clever adjustment
      //of the jump probability. for now... we keep the jump probability at 0. 
      for(int i = 0; i < sc; i++){ 
        g_.eachEdgeFromRawIdx_WithDstNodeIdsLV(i, [&](const Edge & e, int dst_id) {
              //fv[t][i] * trans[i][j] * emit[j][obs[t+1]] * bv[t+1][dwwdwdaj]
              ln_xi[i][dst_id] = lnProd(lnProd(lnProd(fv[t][i], lnSum(e.lnProb, lnJumpProb_[i])), emissions_[dst_id].lnPdf(obs+(t+1)*oc)), bv[t+1][dst_id]);
              norm = lnSum(norm, ln_xi[i][dst_id]);
            });
        
        //for(int j = 0; j < sc; j++) { 
        //  //probability going from i at time t to j at time t}1
        //  //this is the prob of: 
        //  //  (being in state i at time t and observing everything up to and including t) times 
        //  //  (going from i to j) times 
        //  //  (being in state j at time t+1 and observing everything after t+1) times 
        //  //  (being in state j and observing obs[t+1])
        //  ln_xi[i][j] = lnProd(lnProd(lnProd(fv[t][i], ln_transition_mtx_[i][j]), emissions_[j].lnPdf(obs+(t+1)*oc)), bv[t+1][j]);
        //  norm = lnSum(norm, ln_xi[i][j]);
        //}
      }
      for(int i = 0; i < sc; i++){ 
        for(int j = 0; j < sc; j++) { 
          ln_xi[i][j] = lnDiv(ln_xi[i][j], norm);
        }
      }
      //now ln_xi contains the log prob of going from state i to j at times t and t+1
      for(int i = 0; i < sc; i++) { 
        double from_i_sum = lnZero();
        for(int j = 0; j < sc; j++) 
          from_i_sum = lnSum(from_i_sum, ln_xi[i][j]);
        ln_sum_fromi[i] = lnSum(ln_sum_fromi[i], from_i_sum);
        //here we need to estimate sum and sumsq
        //ln_sum_fromi_obsk[i][obs[t]] = lnSum( ln_sum_fromi_obsk[i][obs[t]] , from_i_sum );
        //from_i is the probability of leaving state i at time t
        //if we make sure that all the from_i's are normalized this could easily be used for 
        //calculating the sum and sum_sq from each i 
        //TODO......... implement and test
      }
      for(int i = 0; i < sc; i++) { 
        for(int j = 0; j < sc; j++) { 
          ln_sum_itoj[i][j] = lnSum(ln_sum_itoj[i][j], ln_xi[i][j]);
        }
      }
      //done with time t
    
    
      if(t == 0) { 
        ln_init_state_probs_ = ln_sum_fromi;
        for(int i = 0; i < sc; i++)
          init_state_probs_[i] = exp(ln_init_state_probs_[i]);
      }
  
    }
  
    //TODO...
    for(int i = 0; i < sc; i++) {
      for(int j = 0; j < sc; j++) {
        //ln_transition_mtx_[i][j] = lnDiv(ln_sum_itoj[i][j], ln_sum_fromi[i]) ;
        //transition_mtx_[i][j] = exp(ln_transition_mtx_[i][j]);
        Edge * e = g_.eData(i,j,0);
        if(e) { 
          //printf("update %i->%i from %lf to ",i,j, e->lnProb);
          e->lnProb = lnDiv(ln_sum_itoj[i][j], ln_sum_fromi[i]) ;
          //printf("%lf\n", e->lnProb);
          e->prob = eexp(e->lnProb);
          if(!std::isfinite(e->prob)) { 
            printf("ERROR: edge %i->%i had ln_sum_itoj %lf ln_sum_fromi %lf lnProb %lf and prob %lf\n", i,j, ln_sum_itoj[i][j], ln_sum_fromi[i], e->lnProb, e->prob);
          }
        }
      }
    }

    //for(int i = 0; i < sc; i++) { 
    //  for(int j = 0; j < oc; j++) { 
    //    ln_emissions_[i][j] = lnDiv(ln_sum_fromi_obsk[i][j] , ln_sum_fromi[i]);
    //    emissions_[i][j] = exp(ln_emissions_[i][j]);
    //  }
    //}
   
    //TODO: ... maybe normalize externally??? ...
    normalizeProbsCalcLog(); 
  

  }

  void addNoiseToEdges(double r) { 
    //log edge probability ranges from -infinity to 0 
    //regular probability ranges from 0 1 
    for(int i = 0; i < stateCt(); i++) {     
      double ln_psum = lnZero();
      g_.eachEdgeFromRawIdx_WithDstNodeIdsLV(i, [&](Edge & e, int dst_id) {
            e.lnProb += Rand2::normal() * r;
            ln_psum = lnSum(e.lnProb, ln_psum); 
          });
      g_.eachEdgeFromRawIdx_WithDstNodeIdsLV(i, [&](Edge & e, int dst_id) {
            e.lnProb = lnDiv(e.lnProb, ln_psum);
            e.prob = eexp(e.lnProb);
          });
    }
  }
  
  void resetNoiseyEdges(double r) { 
    //log edge probability ranges from -infinity to 0 
    //regular probability ranges from 0 1 
    for(int i = 0; i < stateCt(); i++) {     
      double ln_psum = lnZero();
      g_.eachEdgeFromRawIdx_WithDstNodeIdsLV(i, [&](Edge & e, int dst_id) {
            e.lnProb = Rand2::normal() * r;
            ln_psum = lnSum(e.lnProb, ln_psum); 
          });
      g_.eachEdgeFromRawIdx_WithDstNodeIdsLV(i, [&](Edge & e, int dst_id) {
            e.lnProb = lnDiv(e.lnProb, ln_psum);
            e.prob = eexp(e.lnProb);
          });
    }
  }
  
  

  static double lnZero() { 
    return -std::numeric_limits<double>::infinity();
  }


  //x -> e^x
  static double eexp(double v) { 
    
    //already returns 0.0 for -inf in c++ ......
    //
    //if(v == -std::numeric_limits<double>::infinity())
    //  return 0.0;

    return exp(v);
  }

  //x -> ln(x)
  static double eln(double v) { 
    
    //already returns -inf for 0.0 in c++ ......
    //
    //if(v == 0.0)
    //  return -std::numeric_limits<double>::infinity();

    return log(v);
  }
  
  //ln(x), ln(y) -> ln(x + y)
  static double lnSum(double ln_v0, double ln_v1) { 
    
    //should be ok with infs for now...
      
    //return log(exp(ln_v0) + exp(ln_v1));
    if(ln_v0 == ln_v1) { 
      //ln(x + x) = ln(2*x) = ln(x) + ln(2.0)
      return ln_v0 + log(2.0);    
    } else if(ln_v0 > ln_v1) { 
      //note: ln(x+y) = ln(x*(1.0 + x/y)) = ln_x + ln(1.0 + x/y) = ln_x + ln(1.0 + e^ln_x / e^ln_y) = ln_x + ln(1.0 + e^(ln_x - ln_y))
      return ln_v0 + log(1.0 + exp(ln_v1 - ln_v0));
    } else { //ln_v1 < ln_v0
      return ln_v1 + log(1.0 + exp(ln_v0 - ln_v1));
    }

  }
  static double lnSum2(double ln_v0, double ln_v1) { 
    return log(exp(ln_v0) + exp(ln_v1));
  }

  //ln(x), ln(y) -> ln(x) + ln(y) (== ln(x*y)) 
  static double lnProd(double ln_v0, double ln_v1) { 
    
    //-inf + x is already -inf in c++ .....

    return ln_v0 + ln_v1;
  }

  static double lnDiv(double ln_v0, double ln_v1) { 
    return ln_v0 - ln_v1;
  }

  std::vector<int> unconnectedNodeIds(int id0, bool search_parent_dir, bool search_child_dir) { 
    return g_.unconnectedNodeIds(id0, search_parent_dir, search_child_dir);
  }
          
  double nodeDotDist2(int id0, int id1) { 
    double d2 = 0.0;
    int oc = obsCt();
    for(int i = 0; i < oc; i++){ 
      double diff = emitP(id0).dot[i] - emitP(id1).dot[i];
      d2 += diff*diff;
    }
    return d2;
  }
  
  int transCt() const {
    return g_.ect();
  }

  std::vector<double> samplePathDots(int src_id, int ct) { 
    std::vector<double> ret;
    int oc = obsCt();
    for(int step = 0; step < ct; step++) { 
      for(int i = 0; i < oc; i++)
        ret.push_back(emitP(src_id).dot[i]);
        
      int d = -1;
      double r_p = Rand2::randAB(0.0, 1.0);
      
      g_.eachEdgeFromRawIdx_WithDstNodeIdsLV(src_id, [&](const Edge & e, int dst_id) {
            r_p -= e.prob;
            if(d == -1 && r_p <= 0.0) { 
              d = dst_id;
            }
          });
      
      if(d != -1) { 
        src_id = d;   
      }

    }
    return ret;
  }
  
  std::vector<double> wPathDots(int src_id, int ct) { 
    //this is very similar to calculating the forward 
    //probability. The difference is that it forces us to start 
    //in a specific location and don't care about observation 
    //probabilities. 

    std::vector<double> ret;
    
    const int oc = obsCt();
    const int sc = stateCt();
    
    std::vector<double> sprob;
    std::vector<double> tmp;
    sprob.resize(sc);
    tmp.resize(sc);
    for(int i = 0; i < sc; i++) 
      sprob[i] = (i == src_id ? eln(1.0) : lnZero());
    
    for(int o = 1; o <= ct; o++) { 
      
      double max_p = std::numeric_limits<double>::lowest();
      for(int i = 0; i < sc; i++) {
        if(sprob[i] > max_p)
          max_p = sprob[i];
      }
      double psum = 0.0;
      ret.resize(ret.size()+oc, (double) 0.0);
      for(int i = 0; i < sc; i++) {
        for(int j = 0; j < oc; j++) { 
          ret[ret.size()-oc+j] += emitP(i).dot[j] * exp(sprob[i] - max_p);
        }
        psum += exp(sprob[i] - max_p);
      }
      for(int j = 0; j < oc; j++) {
        ret[ret.size()-oc+j] /= psum;
      }

      double jump_prob = lnZero();
      for(int hj = 0; hj < (int) sprob.size(); hj++)
        jump_prob = lnSum(jump_prob, lnProd(sprob[hj], lnJumpProb_[hj]));
      for(int j = 0; j < sc; j++) { 
        //j is the state we're going to 
        tmp[j] = jump_prob;
      }
      g_.eachEdgeWithSrcDstNodeIdsLV([&tmp, &sprob](const Edge & e, int src_id, int dst_id) { 
            tmp[dst_id] = lnSum(tmp[dst_id], lnProd(sprob[src_id], e.lnProb));
          });
      //don't apply the emissions probabilities...
      sprob.swap(tmp);
    }
    
    return ret;
  }

  void setAllVars(double v) { 
    for(int i = 0; i < stateCt(); i++)
      emitP(i).var = v;
  }

};









#endif

