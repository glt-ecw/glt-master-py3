#ifndef GZ_PCAP_STRM_H_
#define GZ_PCAP_STRM_H_
  
//compile with -lz

//useage:
//
//GZPcapRdr rdr(filename_here) 
//while(rdr.valid()) { 
//  ... //do something with rdr.nanos() and rdr.data()
//  rdr.next() //go to the next packet
//}
//rdr.close() //this is optional

#include <cstdint>
#include <string>
#include <vector>
#include <zlib.h>
#include <stdexcept>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>

class GZPcapRdr { 
public:
  
  struct FileHdr { 
    uint32_t magic; //determines the endianness and if this is a nanosecond capture (vs micros?)
    uint16_t ver_major; //we probably don't care
    uint16_t ver_minor; //no care
    int32_t gmt_to_local;//for converting gmt timestamps to other zones (wtf pcap?) should be 0
    uint32_t sig_figs;//usually 0, see magic for precision
    uint32_t snap_len;//max length of a packet
    uint32_t network_type; //data link type, 1 is for ETH
  
    uint32_t snapLenSwappedCheck() const { 
      //NOTE: htonl and ntohl do the same thing...
      if(hasSwapped())
        return htonl(snap_len);
      return snap_len;
    }

    bool magicSameEndian() const { 
      return magic == 0xa1b2c3d4;
    }

    bool magicSwappedEndian() const { 
      return magic == 0xd4c3b2a1;
    }

    bool magicSameEndianNanos() const { 
      return magic == 0xa1b23c4d;
    }

    bool magicSwappedEndianNanos() const { 
      return magic == 0x4d3cb2a1;
    }

    bool hasSomeMagic() const { 
      return magicSameEndian() || magicSwappedEndian() || magicSameEndianNanos() || magicSwappedEndianNanos();
    }

    bool hasNanos() const { 
      return magicSameEndianNanos() || magicSwappedEndianNanos();
    }

    bool hasSwapped() const { 
      return magicSwappedEndian() || magicSwappedEndianNanos();
    }

    void setMagic() { 
      magic = 0xa1b2c3d4;
    }

    void setMagicNanos() { 
      magic = 0xa1b23c4d;
    }
  };

  struct PktHdr { 
    uint32_t ts_sec;//seconds
    uint32_t ts_us;//micros or nanos depending on the file header magic
    uint32_t incl_len; //number of bytes of packet saved, could be less than orig_len
    uint32_t orig_len; //original length of packet
  
    uint32_t rdLen(bool swapped) const { 
      return swapped ? htonl(incl_len) : incl_len;
    }

    uint64_t nanos(bool swapped, bool file_is_nanos) const { 
      uint32_t sec = swapped ? htonl(ts_sec) : ts_sec;
      uint32_t us = swapped ? htonl(ts_us) : ts_us;
      return (uint64_t) sec * 1000000000UL + (uint64_t) us * (file_is_nanos ? 1UL : 1000UL);
    }
  };


private:
  std::string fname_;
  FileHdr f_hdr_;
  PktHdr pkt_hdr_;
  //TODO: not wrapped in auto pointer, so not reference counted
  //DO not Copy!
  gzFile file_;
  bool has_nanos_;
  bool has_swapped_;
  std::vector<char> data_;

  GZPcapRdr & operator=(const GZPcapRdr &) = delete;
  GZPcapRdr(const GZPcapRdr & other) = delete;

public:

  void infoStdOut() { 
    printf("Fname %s isSwapped %i isNanos %i snapLen %u\n", fname_.c_str(), has_swapped_, has_nanos_, f_hdr_.snapLenSwappedCheck());
  }

  GZPcapRdr(const std::string & fname) { 
    fname_ = fname;
    
    struct stat st;
    if(-1 == stat(fname.c_str(), &st)) {
      if(errno == ENOENT)
        throw std::runtime_error("Attempting to open gzip'd pcap file, but running stat on file ["+fname+"] returned -1, due to file doesn't exist");
      else if(errno == EACCES) 
        throw std::runtime_error("Attempting to open gzip'd pcap file, but running stat on file ["+fname+"] returned -1, due to access permissions");
      else
        throw std::runtime_error("Attempting to open gzip'd pcap file, but running stat on file ["+fname+"] returned -1, not sure of reason");
    }
    if(st.st_size == 0) 
      throw std::runtime_error("Attemptign to oepn gzip'd pcap file, but running stat on file ["+fname+"] reveals size 0 file");

    file_ = gzopen(fname.c_str(), "rb");
    if(!file_) { 
      throw std::runtime_error("Attempting to open gzip'd pcap file ["+fname+"] but gzOpen returned NULL");         
    }
  
    int ct = gzread(file_, &f_hdr_, sizeof(f_hdr_));
    if(ct != (int) sizeof(f_hdr_)) {
      gzclose(file_);
      throw std::runtime_error("Attempting to open gzip'd pcap file ["+fname+"] but could read " + 
            std::to_string(ct) + " bytes but file hdr size should be " + std::to_string(sizeof(f_hdr_)) + " bytes");
    }    

    has_nanos_ = f_hdr_.hasNanos();
    has_swapped_ = f_hdr_.hasSwapped();
    if(has_swapped_)
      data_.resize(f_hdr_.snapLenSwappedCheck());
    else
      data_.resize(f_hdr_.snap_len);
   
    next();
  }

  bool ok() const { 
    return file_ != 0;
  }

  void next() { 
    int ct = gzread(file_, &pkt_hdr_, sizeof(pkt_hdr_));
    if(ct < (int) sizeof(pkt_hdr_)) {
      close();
    } else { 
      data_.resize( pkt_hdr_.rdLen(has_swapped_) );
      ct = gzread(file_, &(data_[0]), data_.size());
      if(ct < (int) sizeof(pkt_hdr_)) {
        close();
      }
    }
  }
    
  
  uint64_t nanos() const { 
    return pkt_hdr_.nanos(has_nanos_, has_swapped_);
  }

  const std::vector<char> & data() const {
    return data_;
  }  


  void close() { 
    if(file_)
      gzclose(file_);
    file_ = 0;
  }


  ~GZPcapRdr() { 
    close();
  }
};




#endif
