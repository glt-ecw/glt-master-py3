#ifndef POPEN_HLPR_H_
#define POPEN_HLPR_H_

//This implements popen with fork / exec / dup2 / wait
//the problem with regular popen is that it
//doesn't seperate stderr and stdout. This wrapper allows
//us to see stderr and stdout and provides a nice OO interface
//along with extra features (kill process, write to stdin, etc). 
//TODO: can we run sudo commands? is there a nice way to get the 
//password from the user and then re-use later? tty stuff?

//Basic usage:
//
//There's two constructors:
//RunningCmd(const std::string & cmd)
//RunningCmd(const std::string & cmd, const std::vector<std::string> & args)
//
//The first one calls cmd via "sh -c $cmd" and is essentially equivalent 
//to typing the contents of cmd into your shell. The shell is highly recommended
//as it will parse space seperated arguments and handle complex compound commands. 
//
//If you want more explicit control over cmd, use the second constructor but be careful 
//that the args must be split explicitely into a vector.
//
//Once the RunningCmd object is created the command is launched via fork.
//
//There's lots of ways to interact with the running command, but this function handles most cases:
//pollBufsUntilRemotePipesClosedTimeoutAndErrorCheck(int millis, bool fail_on_nonempty_stderr = false)
//It tries to read the stderr and stdout of the running command and then waits until the
//command dies or it times out. If it times out it tries to kill the command. If a timeout
//or non-zero exit status occured then the function throws an exception. 

#include <unistd.h>
#include <stdexcept>
#include <string>
#include <cstring>
#include <cerrno>
#include <csignal>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/epoll.h>

#include "net/Sock.h"//set non-blocking 

class RunningCmd {
  std::string cmd_;
  std::vector<std::string> args_;
  std::string out_;
  bool saw_out_eof_;
  bool saw_err_eof_;
  std::string err_;
  pid_t child_pid_;
  int exit_status_;
  bool wait_done_;
  int64_t start_time_; 

  int out_fd_;
  int err_fd_;
  int in_fd_;
  
  static uint64_t micros_() { 
    struct timeval tv;
    gettimeofday(&tv, 0);
    return tv.tv_sec * 1000UL * 1000UL + tv.tv_usec;
  }

  void cons_(const std::string & cmd, const std::vector<std::string> & args) { 
    cmd_ = cmd;
    args_ = args;

    start_time_ = micros_();

    //step 1 create pipes 
    //step 2 fork commands
    //step 3 CHILD - using dup2, set stderr,stdout,stdin to our end of the pipes
    //step 3 PARENT - close the unused end of the pipes, 

    //reading from a closed pipe returns 0's

    saw_out_eof_ = false;
    saw_err_eof_ = false;
    wait_done_ = false;
    exit_status_ = 0;

    int out_pipe[2];
    int err_pipe[2];
    int in_pipe[2];

    int r = 0;
    r |= pipe(out_pipe);
    r |= pipe(err_pipe);
    r |= pipe(in_pipe);
    if(r < 0) { 
      printf("ERROR, Got error on pipe!\n");
      return;
    }

    pid_t pid = fork();
    if(pid == 0) { 
      //CHILD!      
      
      //replace our IO fd's with the pipe fd's
      r |= dup2(out_pipe[1], fileno(stdout));
      r |= dup2(err_pipe[1], fileno(stderr));
      r |= dup2(in_pipe[1], fileno(stdin));
        
      if(r < 0) { 
        printf("ERROR, Got error on dup2 opening pipes\n");
        exit(-1000);
      }

      close(out_pipe[0]);
      close(err_pipe[0]);
      close(in_pipe[0]);
      
      //execl takes a list of arguments ending in (char *) 0
      //execle allows us to specify the environment variables of the child
      //execlp emulates the shell in searching for paths that don't contain a /
      
      //TODO...
      char ** args_p = new char*[args_.size()+2];
      args_p[0] = &(cmd_[0]);
      for(int i = 0; i < (int) args_.size(); i++)
        args_p[i+1] = &(args_[i][0]);
      args_p[args_.size()+1] = 0;  
      r |= execvp(&(cmd_[0]), (char * const *)args_p);
      
      printf("Failed to execute bash command %s! errno %i=%s\n", cmd_.c_str(), errno, strerror(errno));
      exit(-1002);

    } else if (pid > 0) { 
      //PARENT! got pid of child
      child_pid_ = pid;
      
      ::close(out_pipe[1]);
      ::close(err_pipe[1]);
      ::close(in_pipe[1]);
     
      out_fd_ = out_pipe[0];
      err_fd_ = err_pipe[0];
      in_fd_ = in_pipe[0];
    
      Sock::setNonBlocking(out_fd_);
      Sock::setNonBlocking(err_fd_);
      Sock::setNonBlocking(in_fd_);

    } else { 
      //ERROR!
      printf("ERROR, Got error on fork! errno %i=%s\n", errno, strerror(errno));
    }

  }

  static int readIntoBuf_(int fd, std::string & buf, int amt) { 
    int bufsz = buf.size();
    buf.resize(bufsz + amt);
    int ramt = ::read(fd, &(buf[bufsz]), amt);
    if(ramt > 0)
      buf.resize(bufsz + ramt);
    else
      buf.resize(bufsz);
    return ramt;
  }

  RunningCmd(const RunningCmd & other) = delete;
  RunningCmd & operator=(const RunningCmd & other) = delete;
public: 

  RunningCmd(const std::string & cmd, const std::vector<std::string> & args) {
    cons_(cmd, args);
  }
  
  RunningCmd(const std::string & cmd) {
    cons_("sh", {"-c", cmd});
  }

  int write(const std::string & msg) { 
    return ::write(in_fd_, msg.c_str(), msg.size());
  }

  void pollBufsWhileOk() { 
    int r0 =  1;
    if(!saw_out_eof_) { 
      while((r0 = readIntoBuf_(out_fd_, out_, 1024*32)) > 0);
      if(r0 == 0)
        saw_out_eof_ = true;
    }
    if(!saw_err_eof_) { 
      r0 =  1;
      while((r0 = readIntoBuf_(err_fd_, err_, 1024*32)) > 0);
      if(r0 == 0)
        saw_err_eof_ = true;     
    }
  }

  void pollBufsTimeout(int millis) { 
    if(remotePipesClosed())
      return; 

    int ep = epoll_create1(0);
    
    epoll_event ev[2];
    bzero(ev, 2 * sizeof(epoll_event));

    if(!saw_out_eof_) { 
      ev[0].events = EPOLLIN | EPOLLRDHUP | EPOLLHUP | EPOLLERR; //EPOLLET
      ev[0].data.fd = out_fd_;//ignored
      epoll_ctl(ep, EPOLL_CTL_ADD, out_fd_, &(ev[0]));
    }
    
    if(!saw_err_eof_) {       
      ev[1].events = EPOLLIN | EPOLLRDHUP | EPOLLHUP | EPOLLERR; //EPOLLET
      ev[1].data.fd = err_fd_;//ignored
      epoll_ctl(ep, EPOLL_CTL_ADD, err_fd_, &(ev[1]));
    }

    epoll_event rcvd;
    
    int c = epoll_wait(ep, &rcvd, 1, millis);
    for(int tries = 0; tries < 5 && (c == -1 && errno == EINTR); tries++)
      c = epoll_wait(ep, &rcvd, 1, millis); //retry if process is interrupted ... 

    //printf("epoll got c %i\n", c);
    ::close(ep);
   
    if(c == 0)//no events
      return;
    if(c < 0) { //epoll error
      printf("ERROR, waiting on process pipes with epoll, errno %i=%s\n", errno, strerror(errno));
    }
    
    pollBufsWhileOk();
  }

  void pollBufsUntilRemotePipesClosedTimeout(int millis) { 
    
    if(millis < 0) { 
      while(!remotePipesClosed())
        pollBufsTimeout(-1);
      return;
    }

    int64_t t0 = micros_();
    pollBufsTimeout(millis);
    
    if(millis == 0)
      return;
    
    while(!remotePipesClosed()) { 
      int64_t t1 = micros_();
      //printf("time left is %li\n", millis - (t1-t0+999L)/1000L);
      if((t1 - t0 + 999L) / 1000L > millis)
        return; 
      pollBufsTimeout(millis - (t1-t0+999L)/1000L);        
    }
  }
   
  void pollBufsUntilRemotePipesClosedTimeoutAndErrorCheck(int millis, bool fail_on_nonempty_stderr = false) { 
    pollBufsUntilRemotePipesClosedTimeout(millis);
    //printf("poll done\n");
    if(millis < 0)
      wait(true);
    else
      wait(false);
    //printf("wait done\n");
    if(!wait_done_) { 
      kill();
      if(err_.size()) 
        throw std::runtime_error("External code failed to finish, issued kill to process: " + std::to_string(child_pid_) + " stderr: " + err_);
      else
        throw std::runtime_error("External code failed to finish, issued kill to process: " + std::to_string(child_pid_) + " stderr is empty");
    } else if(exitStatus() != 0) { 
      if(err_.size())
        throw std::runtime_error("External code has non-zero exit status: " + std::to_string(exitStatus()) + " on process: " + std::to_string(child_pid_) + " and stderr is: " + err_);
      else
        throw std::runtime_error("External code has non-zero exit status: " + std::to_string(exitStatus()) + " on process: " + std::to_string(child_pid_) + " and stderr is empty");
    } else if(err_.size() && fail_on_nonempty_stderr)
        throw std::runtime_error("External code has non-empty stderr on process: " + std::to_string(child_pid_) + " the stderr is: "+ err_);
  }



  //Waiting with a timeout would be nice, but is kind of tricky.   
  //One way to implement this is to launch a helper subprocess that 
  //launches two subprocess with one that does actual work and another
  //that sleeps for the timeout and then kills worker. The question is
  //do we waste a huge amount of ram by doing this?
  void wait(bool block) { 
    if(child_pid_ > 0 && !wait_done_) { 
      int wres = waitpid(child_pid_, &exit_status_, block ? 0 : WNOHANG);
      if(wres < 0) { 
        printf("ERROR, waitpid errno %i=%s\n", errno, strerror(errno));
      } else if (wres == child_pid_) {
        wait_done_ = true;
      }
    }
  }

  
  bool remotePipesClosed() const {
    return saw_out_eof_ && saw_err_eof_;
  }

  bool dead() const { 
    return wait_done_;
  }

  bool childExitedNormally() const { 
    return WIFEXITED(exit_status_);
  }

  int exitStatus() const { 
    return WEXITSTATUS(exit_status_);
  }

  bool childExitedDueToSignal() const { 
    return WIFSIGNALED(exit_status_);
  }

  const std::string & out() const { 
    return out_;
  }

  const std::string & err() const { 
    return err_;
  }

  void kill() { 
    if(child_pid_ > 0 && !wait_done_) { 
      //int r = ::kill(child_pid_, SIGTERM);
      int r = ::kill(child_pid_, SIGKILL);
      if(r < 0) { 
        printf("ERROR, kill failed due to errno %i=%s\n", errno, strerror(errno));
      }
    }
  }

  int64_t age() const { 
    return micros_() - start_time_;
  }

  ~RunningCmd() { 
    ::close(out_fd_);
    ::close(err_fd_);
    ::close(in_fd_);
  }
//  void 
};



#endif
