#ifndef DATE_PY_H_
#define DATE_PY_H_

#include <cstring>
#include <cstdint>
#include <sys/time.h>
#include <cstdlib>
#include "RunningCmd.h"

//this is super dirty slow shit... executing a program to increment a date, can easily overload the OS
class DatePy { 

  int year_;
  int month_;
  int day_;


  void setupTm_(struct tm & timeptr) { 
    timeptr.tm_sec = 0;//0-61, 0-60 in c++11 (0-59 usually, 2 extra for leap seconds)
    timeptr.tm_min = 0;//0-59
    timeptr.tm_hour = 0;//0-23
    timeptr.tm_mday = day_;//1-31
    timeptr.tm_mon = month_-1;//0-11, kind of odd
    timeptr.tm_year = year_ - 1900;//years since 1900
    timeptr.tm_wday = -1;//0-6 (days since Sunday(?)), this is ignored by mktime
    timeptr.tm_yday = -1;//0-365 (days since jan1(?)), this is ignored by mktime
    timeptr.tm_isdst = -1;//less than zero means we're not sure about DST
  }

  void normalize_() { 

    struct tm timeptr;
    setupTm_(timeptr);
     
    //NOTE: mktime may not be thread safe, and if the TZ changes in between these calls (unlikely) we could be 
    //getting messed up results. it might be good to lock a static mutex around these calls ... or at least be 
    //very careful with threads. 
    time_t timev = mktime(&timeptr);
    bzero(&timeptr, sizeof(timeptr));
    localtime_r(&timev, &timeptr);  
    
    year_ = timeptr.tm_year + 1900;
    month_ =  timeptr.tm_mon+1;
    day_ =  timeptr.tm_mday;
  }

public:
  std::string yyyymmdd() const { 
    char buf[16];
    sprintf(buf, "%04i%02i%02i\n", year_, month_, day_);
    return std::string(buf, 8);
  }
  std::string ymdStr() const {
    return yyyymmdd();
  }
  void today() { 
    time_t timev = time(0);
    struct tm timeptr;
    bzero(&timeptr, sizeof(timeptr));
    localtime_r(&timev, &timeptr);  

    year_ = timeptr.tm_year + 1900;
    month_ =  timeptr.tm_mon+1;
    day_ =  timeptr.tm_mday;
  }
  int ymdInt() const {
    return year_ * 10000 + month_ * 100 + day_;
  }
  int year() const { 
    return year_;
  }
  int month() const {
    return month_;
  }
  int day() const {
    return day_;
  }
  std::string yearStr() const { 
    return std::to_string(year_);
  }
  std::string monthStr() const { 
    return std::to_string(month_);
  }
  std::string dayStr() const { 
    return std::to_string(day_);
  }
  DatePy() { 
    //...uninitialized...
  }

  DatePy(int ymdv) { 
    year_ = ymdv / 10000;
    month_ = (ymdv / 100) % 100;
    day_ = ymdv % 100;
  }

  DatePy(int y, int m, int d) { 
    year_ = y;
    month_ = m;
    day_ = d;    
  }

  //prefix
  DatePy & operator++() { 
    day_++;
    if(day_ > 25)
      normalize_();
    return *this;
  }
  
  //prefix
  DatePy & operator--() { 
    day_--;
    if(day_ < 2)
      normalize_();
    return *this;
  }


  //note: postfix ++ should return a copy of the object without modifications
  DatePy operator++(int) { 
    DatePy ret = *this;
    day_++;
    if(day_ > 25)
      normalize_();
    return ret;
  }

  DatePy & operator+=(int v) { 
    if(v > 0) { 
      for(; v > 0; v--)
        (*this)++;
    } else { 
      for(; v < 0; v++)
        (*this)--;
    }  
    return *this;
  }
  
  DatePy & operator-=(int v) { 
    return (*this) += -v;
  }


  //postfix
  DatePy operator--(int) { 
    DatePy ret = *this;
    day_--;
    if(day_ < 2)
      normalize_();
    return ret;
  }

  void fromYYYYMMDD(const std::string str) { 
    year_ = std::atoi(str.substr(0,4).c_str());
    month_ = std::atoi(str.substr(4,2).c_str());
    day_ = std::atoi(str.substr(6,2).c_str());
  }

  bool operator==(const DatePy & other) const { 
    return year_==other.year_ && month_ == other.month_ && day_==other.day_;
  }
  bool operator<(const DatePy & other) const { 
    if(year_ < other.year_)
      return true;
    if(year_ > other.year_)
      return false;
    if(month_ < other.month_)
      return true;
    if(month_ > other.month_)
      return false;
    if(day_ < other.day_)
      return true;
    return false;
  }
  bool operator>(const DatePy & other) const {
    return other < *this;
  }
  bool operator<=(const DatePy & other) const { 
    return (*this < other) || (*this == other);
  }
  bool operator>=(const DatePy & other) const { 
    return other<=(*this);
  }
  
  //Gives the difference between two dates in days
  //NOTE: this is very innefecient
  int operator-(const DatePy & other) const { 
    if(*this >= other) { 
      DatePy cpy = other;
      int ret = 0;
      while(*this > cpy) { 
        cpy++;
        ret++;
      }
      return ret;
    } else { 
      return -(other - *this);
    }
  }

  DatePy operator-(int v) const { 
    DatePy ret = *this;
    ret += -v;
    return ret;
  }

  DatePy operator+(int v) const { 
    DatePy ret = *this;
    ret += v;
    return ret;
  }

  void normalize() { 
    normalize_();
  }


  int daysSinceSunday() { 
    struct tm timeptr;
    setupTm_(timeptr);
    
    time_t timev = mktime(&timeptr);
    bzero(&timeptr, sizeof(timeptr));
    localtime_r(&timev, &timeptr);  
    
    return timeptr.tm_wday; 
  }

  bool isMonday() { 
    return daysSinceSunday() == 1;
  }

  bool isWeekend() { 
    int dss = daysSinceSunday();
    return dss == 0 || dss == 6;
  }

  bool isWeekday() { 
    return !isWeekend();
  }

    
  //Note: getting the micros from a date/time works is easy with c. 
  //Going from micros back to date/time (via localtime_r) usually
  //fucks up the hours due to DST ... all in all kind of a pain in 
  //the ass, and could have been just a local issue with the kernel.
  //Either way, to do it in c, we have to change the env variable:
  //setenv("TZ", "ROK", 1);
  //To 

  int64_t localMicros() { 
    struct tm timeptr;
    setupTm_(timeptr);
    time_t timev = mktime(&timeptr);
    return (int64_t) timev * 1000L * 1000L;
  }  
  
  //NOTE: according to the man page, this function is non-standard and unadvisable ... not sure what happens during DST
  int64_t gmMicros() { 
    struct tm timeptr;
    setupTm_(timeptr);
    time_t timev = timegm(&timeptr);
    return (int64_t) timev * 1000L * 1000L;
  }


  static void timeTestStdOut() { 
    DatePy d0(2016, 2, 10);
    int64_t est_ts_0 = d0.extToMicrosTZ(tzEST());
    int64_t local_ts_0 = d0.localMicros();
    int64_t c_gm_ts_0 = d0.gmMicros();
    int64_t py_gm_ts_0 = d0.extToMicrosTZ("GMT");

    printf("2016/2/10 relative to local ...  est %lf c_gm %lf py_gm %lf\n",
            (double)(est_ts_0 - local_ts_0) / 1000.0 / 1000.0 / 60.0 / 60.0,
            (double)(c_gm_ts_0 - local_ts_0) / 1000.0 / 1000.0 / 60.0 / 60.0,
            (double)(py_gm_ts_0 - local_ts_0) / 1000.0 / 1000.0 / 60.0 / 60.0);
    
    DatePy tmp(0,0,0);
    int h,m,s;
    int64_t tst = est_ts_0 + 1000L * 10L * 1000L * 1000L;
    tmp.extFromMicros(tst, tzKorea(), h,m,s);
    printf("EST ts + 10,000s in Korean is %04i-%02i-%02i %02i:%02i:%02i\n", tmp.year_,tmp.month_,tmp.day_,h,m,s);

    DatePy d1(2016, 4, 10);
    int64_t est_ts_1 = d1.extToMicrosTZ(tzEST());
    int64_t local_ts_1 = d1.localMicros();
    int64_t c_gm_ts_1 = d1.gmMicros();
    int64_t py_gm_ts_1 = d1.extToMicrosTZ("GMT");
    
    printf("2016/4/10 relative to local ...  est %lf c_gm %lf py_gm %lf\n",
            (double)(est_ts_1 - local_ts_1) / 1000.0 / 1000.0 / 60.0 / 60.0,
            (double)(c_gm_ts_1 - local_ts_1) / 1000.0 / 1000.0 / 60.0 / 60.0,
            (double)(py_gm_ts_1 - local_ts_1) / 1000.0 / 1000.0 / 60.0 / 60.0);
    
    tst = est_ts_1 + 1000L * 10L * 1000L * 1000L;
    tmp.extFromMicros(tst, tzKorea(), h,m,s);
    printf("EST ts + 10,000s in Korean is %04i-%02i-%02i %02i:%02i:%02i\n", tmp.year_,tmp.month_,tmp.day_,h,m,s);
  }
  
  static std::string tzUTC() { 
    return "UTC";
  }

  static std::string tzKorea() { 
    return "Asia/Seoul";
  }

  static std::string tzGermany() { 
    return "Europe/Berlin";
  }

  static std::string tzEST() { 
    return "America/New_York";
  }

  static std::string tzCST() {
    return "America/Chicago";
  }

  int64_t extToMicrosTZ(std::string tz) { 
    //this is where the insanity begins, we call python, which has much nicer timezone libraries than c/c++ ...
    //maybe we can use boost?
    std::string py_prog = 
R"ESCXX(
import datetime
import pytz
import time
kst = pytz.timezone(')ESCXX" + tz+ R"ESCXX(')
utc = pytz.timezone('UTC')
zero = datetime.datetime.utcfromtimestamp(0)
zero = utc.localize(zero)
d = kst.localize(datetime.datetime()ESCXX" + yearStr() +","+monthStr()+","+dayStr()+R"ESCXX(, 0, 0, 0, 0))
ts = (int((d-zero).total_seconds()))
print ts
)ESCXX";
    //printf("%s\n", py_prog.c_str());
    RunningCmd rc("python", {"-c", py_prog});
    rc.pollBufsUntilRemotePipesClosedTimeoutAndErrorCheck(-1);
    return atol(rc.out().c_str()) * 1000L * 1000L;         
  }
  
  int64_t extToMicrosTZ(std::string tz, int hour, int minute, int second) { 
    std::string py_prog = 
R"ESCXX(
import datetime
import pytz
import time
kst = pytz.timezone(')ESCXX" + tz+ R"ESCXX(')
utc = pytz.timezone('UTC')
zero = datetime.datetime.utcfromtimestamp(0)
zero = utc.localize(zero)
d = kst.localize(datetime.datetime()ESCXX" + yearStr() +","+monthStr()+","+dayStr()+","+std::to_string(hour)+","+std::to_string(minute)+","+std::to_string(second)+R"ESCXX(, 0))
ts = (int((d-zero).total_seconds()))
print ts
)ESCXX";
    //printf("%s\n", py_prog.c_str());
    RunningCmd rc("python", {"-c", py_prog});
    rc.pollBufsUntilRemotePipesClosedTimeoutAndErrorCheck(-1);
    return atol(rc.out().c_str()) * 1000L * 1000L;         
  }


  void extFromMicros(int64_t micros, std::string tz) { 
    int64_t seconds = micros/1000L/1000L;
    std::string py_prog = 
R"ESCXY(
import datetime
import pytz
import time
tz = pytz.timezone(')ESCXY" + tz + R"ESCXY(')
dt = datetime.datetime.fromtimestamp()ESCXY" + std::to_string(seconds) + R"ESCXY(, tz)
print dt.year, dt.month, dt.day
)ESCXY";
    //printf("%s\n", py_prog.c_str());
    RunningCmd rc("python", {"-c", py_prog});
    rc.pollBufsUntilRemotePipesClosedTimeoutAndErrorCheck(-1);
    auto str = rc.out();
    char * nxt = &(str[0]);
    year_ = strtol(nxt, &nxt, 10);
    nxt++;
    month_ = strtol(nxt, &nxt, 10);
    nxt++;
    day_ = strtol(nxt, &nxt, 10);
    //printf("%i %i %i\n", year_, month_, day_);
    //printf("%s\n", yyyymmdd().c_str());
  }

  void extFromMicros(int64_t micros, std::string tz, int & hour, int & min, int & sec) { 
    int64_t seconds = micros/1000L/1000L;
    std::string py_prog = 
R"ESCXY(
import datetime
import pytz
import time
tz = pytz.timezone(')ESCXY" + tz + R"ESCXY(')
dt = datetime.datetime.fromtimestamp()ESCXY" + std::to_string(seconds) + R"ESCXY(, tz)
print dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second
)ESCXY";
    //printf("%s\n", py_prog.c_str());
    RunningCmd rc("python", {"-c", py_prog});
    rc.pollBufsUntilRemotePipesClosedTimeoutAndErrorCheck(-1);
    auto str = rc.out();
    char * nxt = &(str[0]);
    year_ = strtol(nxt, &nxt, 10);
    nxt++;
    month_ = strtol(nxt, &nxt, 10);
    nxt++;
    day_ = strtol(nxt, &nxt, 10);
    nxt++;
    hour = strtol(nxt, &nxt, 10);
    nxt++;
    min = strtol(nxt, &nxt, 10);
    nxt++;
    sec = strtol(nxt, &nxt, 10);
    //printf("%i %i %i\n", year_, month_, day_);
    //printf("%s\n", yyyymmdd().c_str());
  }

  //this converts micros to a string representing that time with a given timezome  
  static std::string strExtFromMicros(int64_t micros, std::string tz) { 
    DatePy dp;
    int hour,min,sec;
    dp.extFromMicros(micros, tz, hour, min, sec);
    char buf[256];
    bzero(buf, sizeof(buf));
    sprintf(buf, "%04i-%02i-%02i %02i:%02i:%02i.%06i", dp.year_, dp.month_, dp.day_, hour,min,sec, (int)(micros % 1000000L));
    buf[sizeof(buf)-1] = 0;
    return std::string(buf);
  }

  static std::string utcStrFromMicros(int64_t micros) { 
    time_t t = micros / 1000000L;
    struct tm res;
    bzero(&res, sizeof(res));
    gmtime_r(&t, &res);
    char buf[256];
    bzero(buf, sizeof(buf));
    sprintf(buf, "%04i-%02i-%02i %02i:%02i:%02i.%06i", res.tm_year + 1900, res.tm_mon+1, res.tm_mday, res.tm_hour, res.tm_min, res.tm_sec, (int)(micros % 1000000L));
    buf[sizeof(buf)-1] = 0;
    return std::string(buf);
  }

  static int64_t microsFromParticipantTS(const std::string & parts, const std::string & tz) { 
    //expects a timestamp from the participants file of the format: 20160420T073003.918089
    DatePy dp;
    dp.fromYYYYMMDD(parts.substr(0,8));
    int c = 9;
    std::string h = parts.substr(c,2); 
    c+=2;
    std::string m = parts.substr(c,2); 
    c+=2;
    std::string s = parts.substr(c,2); 
    c+=2;
    c+= 1;
    std::string u = parts.substr(c,6);
    return dp.extToMicrosTZ(tz, std::atoi(h.c_str()), std::atoi(m.c_str()) , std::atoi(s.c_str())) + std::atol(u.c_str());
  }

  static int64_t microsFromParticipantsHMSU(const std::string & hmsu) { 
    //give a timestamp: 20160420T073003.918089
    // we parse this part in us: 073003.918089
    int64_t ret = 0;
 
    ret += (hmsu[0] - '0') * 60L * 60L * 1000L * 1000L * 10L;
    ret += (hmsu[1] - '0') * 60L * 60L * 1000L * 1000L;
 
    ret += (hmsu[2] - '0') * 60L * 1000L * 1000L * 10L;
    ret += (hmsu[3] - '0') * 60L * 1000L * 1000L;
 
    ret += (hmsu[4] - '0') * 1000L * 1000L * 10L;
    ret += (hmsu[5] - '0') * 1000L * 1000L;
  
  
    ret += (hmsu[7] - '0') * 1000L * 100L;
    ret += (hmsu[8] - '0') * 1000L * 10L;
    ret += (hmsu[9] - '0') * 1000L ;
    ret += (hmsu[10] - '0') * 100L;
    ret += (hmsu[11] - '0') * 10L;
    ret += (hmsu[12] - '0') * 1L;
    
    return ret;
  }

};


#endif

