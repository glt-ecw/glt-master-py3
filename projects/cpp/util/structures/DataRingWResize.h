#ifndef DATA_RING_W_RESIZE_H_
#define DATA_RING_W_RESIZE_H_
#include <vector>
#include <cstdint>
#include <cstring>
#include <string>


//primary class is DataRingWResize 
//
//These data rings have the functions:
//
//char * getPushable(uint32_t sz) 
//void savePushable(uint32_t sz)
//bool getPoppable(char * & ptr, uint32_t & sz)  
//void popKnown(uint32_t sz) 
//
//The use of this data ring is to queue raw chunks of data. The typical use is when synchronizing timestamped 
//data coming from multiple streams, or attempting to delay a stream -- often data needs to be queued up.
//This dataring forgoes lockfree/threadsafe operation in order to provide dynamic resizing, which makes
//this more ideal in single-threaded application or applications that use additional locking and care
//only about throughput and not latency. 
//
//A second class defined as:
//template <typename HDR_TO_SIZE>
//class DataRingWResizeHdrBased
//...exists in the second half of this file. This class simply takes a template that exposes a function
//which defines the size of the elements in the data ring based on the elements themselves instead of 
//having to store an additional size value as the regular DataRingWResize does. 
//
//TODO: we me add a DataRingWResizeHdrBasedRef which has a reference to another class which
//can decode the packet size from the header (since the decode function may not be compile time
//available or static, as in the regular DataRingWResizeHdrBased)
//
//Attention: be careful! getPushable can cause a pointer returned by getPoppable to become invalidated ...
//since getPushable can trigger a resize and result in the underlying buffer to be destroyed or at least
//the data in the buffer being moved around. 

class DataRingWResize { 

  std::vector<char> data_;
  uint32_t start_ = 0;
  uint32_t end_ = 0; 
public:

  DataRingWResize() { 
    data_.resize(32);
  }

  void clear() { 
    start_ = 0;
    end_ = 0;
  }

  bool resize_flag = false;

  char * getPushable(uint32_t sz) { 
    uint32_t sz4 = (sz + 3U) / 4U * 4U;
    uint32_t ep = end_ % (uint32_t) (data_.size()/2U); 
    uint32_t sp = start_ % (uint32_t) (data_.size()/2U);
    //printf("gp sz %u sz4 %u ep %u sp %u datasz %lu\n", sz, sz4, ep, sp, data_.size());
    //could just check if [ sz + end_ - start_ + 1U + sizeof(uint32_t) > data_.size()/2U ] but 
    //this _slightly_ more expensive check allows utilize up to data_.size() bytes in some edge cases
    if(ep + sz4 + 1U + sizeof(uint32_t) >= data_.size() || ep + sz4 + 1U + sizeof(uint32_t) >= sp) { 

      //prepare a new vector of suitable size for storing all the existing data + new data
      std::vector<char> tmp;
      uint32_t ts = data_.size();
      while(sz4 + end_ - start_ + sizeof(uint32_t) + 1U >= ts / 2U)
        ts *= 2;
      tmp.resize(ts);
      
      //iterate through the data chunks and copy them over 
      if(ep > sp) {
        //printf("short circuit memcpy\n");
        memcpy(&tmp[0], &data_[sp], ep - sp);
      } else { 
        uint32_t srcp = sp;
        uint32_t dstp = 0;
        uint32_t tmpsz = 0;
        while(srcp != ep) { 
          //iterate through the buffer and copy
          tmpsz = (*reinterpret_cast<uint32_t *>(&data_[srcp]) + 3U) / 4U * 4U;
          //printf("copying from %u to %u of sz %u+4\n", srcp, dstp, tmpsz);
          memcpy(&tmp[dstp], &data_[srcp], tmpsz + sizeof(uint32_t));
          srcp += tmpsz + sizeof(uint32_t);
          srcp = srcp % (uint32_t) (data_.size() / 2U);
          dstp += tmpsz + sizeof(uint32_t); 
          //printf("srcp %u ep %u\n", srcp, ep);
        }
      }
      end_ -= start_;
      start_ = 0;
      sp = start_;
      ep = end_;
      data_ = tmp;
      resize_flag = true;
    }
    return &(data_[ep+sizeof(uint32_t)]);
  }

  void savePushable(uint32_t sz) {
    uint32_t ep = end_ % (uint32_t) (data_.size() / 2U); 
    *reinterpret_cast<uint32_t *>(&data_[ep]) = sz;
    //printf("saving size of %u\n", sz);
    end_ += sizeof(uint32_t) + (sz + 3U) / 4U * 4U;
  }

  bool getPoppable(char * & ptr, uint32_t & sz) { 
    if(end_ == start_)
      return false;
    uint32_t sp = start_ % (uint32_t) (data_.size()/2U);
    sz = *(uint32_t *) &data_[sp];
    ptr = &data_[sp+sizeof(uint32_t)];
    return true;
  }

  void popKnown(uint32_t sz) { 
    start_ += sizeof(uint32_t) + (sz + 3U) / 4U * 4U;
  }


};


template <typename HDR_TO_SIZE>
class DataRingWResizeHdrBased { 

  std::vector<char> data_;
  uint32_t start_ = 0;
  uint32_t end_ = 0; 
public:

  //HDR_TO_SIZE & hdr_to_size;
  //this has to implement 

  DataRingWResizeHdrBased() { 
    data_.resize(32);
  }

  void clear() { 
    start_ = 0;
    end_ = 0;
  }

  bool resize_flag = false;

  char * getPushable(uint32_t sz) { 
    uint32_t sz4 = (sz + 3U) / 4U * 4U;
    uint32_t ep = end_ % (uint32_t) (data_.size()/2U); 
    uint32_t sp = start_ % (uint32_t) (data_.size()/2U);
    //printf("gp sz %u sz4 %u ep %u sp %u datasz %lu\n", sz, sz4, ep, sp, data_.size());
    //could just check if [ sz + end_ - start_ + 1U + sizeof(uint32_t) > data_.size()/2U ] but 
    //this _slightly_ more expensive check allows utilize up to data_.size() bytes in some edge cases
    //if(ep + sz4 + 1U >= data_.size() || (ep < sp && ep + sz4 + 1U >= sp)) { 
    if(sz + end_ - start_ + 1U + sizeof(uint32_t) > data_.size() / 2U) { 

      //prepare a new vector of suitable size for storing all the existing data + new data
      std::vector<char> tmp;
      uint32_t ts = data_.size();
      while(sz4 + end_ - start_ + 1U >= ts / 2U)
        ts *= 2;
      tmp.resize(ts);
      
      //iterate through the data chunks and copy them over 
      if(ep > sp) {
        //printf("short circuit memcpy\n");
        memcpy(&tmp[0], &data_[sp], ep - sp);
      } else { 
        uint32_t srcp = sp;
        uint32_t dstp = 0;
        uint32_t tmpsz = 0;
        while(srcp != ep) { 
          //iterate through the buffer and copy
          tmpsz = HDR_TO_SIZE::getSz(&data_[srcp]);
          //printf("copying from %u to %u of sz %u+4\n", srcp, dstp, tmpsz);
          memcpy(&tmp[dstp], &data_[srcp], tmpsz);
          srcp += tmpsz;
          srcp = srcp % (uint32_t) (data_.size() / 2U);
          dstp += tmpsz;
          //printf("srcp %u ep %u\n", srcp, ep);
        }
      }
      end_ -= start_;
      start_ = 0;
      sp = start_;
      ep = end_;
      data_ = tmp;
      resize_flag = true;
      //printf("resized to %lu\n", data_.size());
    }
    return &(data_[ep]);
  }

  void savePushable(uint32_t sz) {
    end_ += (sz + 3U) / 4U * 4U;
  }

  bool getPoppable(char * & ptr, uint32_t & sz) { 
    if(end_ == start_)
      return false;
    uint32_t sp = start_ % (uint32_t) (data_.size()/2U);
    sz = HDR_TO_SIZE::getSz(&data_[sp]);
    ptr = &data_[sp];
    return true;
  }

  void popKnown(uint32_t sz) { 
    start_ += (sz + 3U) / 4U * 4U;
  }


};



#endif

