#ifndef SEQ_SEQ_EL_LIST_RING_H_
#define SEQ_SEQ_EL_LIST_RING_H_
#include <vector>
#include <cstdint>
#include <stdexcept>
#include <string>

//This class provides a simple mechanism for storing a sequence of resizable sequences of consecutive data in a ring buffer.
//When data is removed from the N-th sequence it is then appended in the (N-1)-th sequence - with the exception that data 
//removed from the 0-th sequence is throw out. 
//
//A typical use case is when analyzing some time series we want to keep the values from [0-10min] ago, [10min-20min] ago
// [20-30min] ago and so forth. The usual approach would enlist multiple std::lists, but removing from one grp and placing 
// in another could result in memory allocations/deallocations whereas in this case it's merely a integer increment. 
//
//TODO: it is theoretically possible to move elements in either direction but that isn't implemented yet

template <typename D, bool do_throw=true>
class SeqSeqRing { 

  //raw data
  std::vector<D> data_;
 
  //the N-th sequence starts at mark[N] and ends right before mark[N+1] 
  //so in total there are N+1 marks denoting the start/finishes of sequences
  std::vector<uint32_t> marks_;

  //since the sizes of our data_ vector are always powers of 2, and we often do % data_.size()
  //we replace that with and & call since & is much faster than doing int division
  uint32_t sz_and_;
public:

  SeqSeqRing() : SeqSeqRing(0) { } 

  SeqSeqRing(int grp_ct) { 
    if(grp_ct < 0) 
      throw std::runtime_error("Cannot create seqellistring with grp ct " + std::to_string(grp_ct));
    marks_.resize(grp_ct+1, (uint32_t) 0);
    sz_and_ = 0x7;
    data_.resize(8);
  }

  bool empty() const {
    return marks_[0] == marks_.back();
  }

  void clear() { 
    for(int i = 0; i < (int) marks_.size(); i++)
      marks_[i] = 0;
  }

  //add a new empty group to the back
  void newGrpPushBack() { 
    marks_.push_back(marks_.back());
  }

  //add a zero size group to the front, NOTE: all other group idx's will increase by 1
  void newGrpPushFront() { 
    marks_.insert(marks_.begin(), marks_.front());
  }


  const D & byIdx(uint32_t idx) const { 
    if(do_throw) {
      if((int) idx < 0 || (int) idx >= (int) size())
        throw std::runtime_error("Trying to get el by idx "+std::to_string(idx)+" but range is 0 to "+std::to_string(size()));
    }
    //return data_[(marks_.front() + idx) % (uint32_t) data_.size()];
    return data_[(marks_.front() + idx) & sz_and_];
  }

  //get the size of the whole thing
  uint32_t size() const {
    return marks_.back() - marks_.front();
  }

private:
  void sizeCheck_() { 
    if(size() + 2U >= (uint32_t) data_.size()) {
      //printf("resizing from sz %u, end %u start %u, arr sz is %lu\n", size(), marks_.back(), marks_.front(), data_.size());
      std::vector<D> data2;
      data2.resize(data_.size() * 2);
      for(uint32_t i = 0; i < size(); i++) {
        data2[i] = byIdx(i);      
        //printf("copy %i (rpos %u) v %li\n", i, ((marks_.front() + i) & sz_and_), data2[i].p);
      }
      //printf("sz_and is %x before, add 1 is %x, times 2 is %x\n", sz_and_, sz_and_ + 1U, (sz_and_ + 1U)*2U);
      sz_and_ = (sz_and_ + 1U) * 2U - 1U;
      //printf("sz_and is %x after\n", sz_and_);
      for(uint32_t i = 1; i < (uint32_t) marks_.size(); i++) 
        marks_[i] -= marks_[0];
      marks_[0] = 0;
      data_ = data2;
    }
  }
public:


  void pushHead(const D & v) { 
    if(do_throw && marks_.size() < 2U)
      throw std::runtime_error("Cannot pushHead element when 0 groups are present");
    sizeCheck_();
    //data_[(uint32_t)(marks_.front() - 1U) % (uint32_t) data_.size()] = v;
    data_[(uint32_t)(marks_.front() - 1U) & sz_and_] = v;
    marks_.front() -= 1U;
  }

  void push(const D & v) { 
    if(do_throw && marks_.size() < 2U)
      throw std::runtime_error("Cannot push element when 0 groups are present");
    sizeCheck_();
    //data_[marks_.back() % (uint32_t) data_.size()] = v;
    //printf("push to %u (%u)\n", marks_.back() & sz_and_, marks_.back());
    data_[marks_.back() & sz_and_] = v;
    marks_.back()++;
  }

  //Pop up $ct elements from the first groups that have elements with an id
  //greater than or equal to $grp_idx. Return the number of elements popped.
  uint32_t grpGTEPopSzCheck(int grp_idx, uint32_t ct) { 
    return grpGTEPopSzCheckCB(grp_idx, ct, [](const D & el, int grp_idx) { } );
  }
  
  //lambda is called for every element removed, provided a ref to that element
  //and the index of the group...
  template <typename LAMBDA> 
  uint32_t grpGTEPopSzCheckCB(int grp_idx, uint32_t ct, const LAMBDA & lambda) { 
    if(do_throw) {
      if(grp_idx < 0 || grp_idx >= (int) grps())
        throw std::runtime_error("Trying to pop from grp GTE "+std::to_string(grp_idx)+" but range is 0 to "+std::to_string(grps()));
    }
    {
      uint32_t maxp = (marks_.back() - marks_[grp_idx]);
      //printf("maxp %u\n", maxp);
      if(ct >= maxp) {
        //a simple shortcut, if we have 0 els do nothing
        if(maxp == 0)
          return 0;  
        //all elements to the right of grp_idx have been popped
        for(int i = grp_idx; i < (int) grps(); i++) {
          for(uint32_t t0 = marks_[i]; t0 != marks_[i+1]; t0++) 
            lambda(data_[t0 & sz_and_ ],i);
          marks_[i] = marks_.back();
        }
        return maxp;
      }
    }
    
    uint32_t dv = marks_[grp_idx] + ct;
    for(int i = grp_idx; i < (int) grps(); i++) { 
      for(uint32_t t0 = marks_[i]; t0 != marks_[i+1] && t0 != dv; t0++) 
        lambda(data_[t0 & sz_and_ ],i);
      marks_[i] = dv;
      //if grp_i has enough elements in it then marks_[i+1] - marks_[i] should be less than the total size of this thing
      //printf("rem %u from %i delta %u size %lu\n", ct, i, marks_[i+1]-marks_[i], data_.size());
      if((marks_[i+1] - marks_[i]) <= data_.size())
        break;
    }
    return ct;
  }

  //pop an element from some group, throw and exception / crash if that group is empty
  //the element will be moved to a group whose index is $grp_idx - 1 however if $grp_idx - 1 
  //is less than zero then the element will be discarded. 
  void grpPop(int grp_idx) { 
    //the head of that grp advances and the tail of the prev grp advances
    if(do_throw) {
      if(grp_idx < 0 || grp_idx >= (int) grps())
        throw std::runtime_error("Trying to pop from grp "+std::to_string(grp_idx)+" but range is 0 to "+std::to_string(grps()));
      if(grpEmpty(grp_idx))
        throw std::runtime_error("Trying to pop from empty grp with idx " + std::to_string(grp_idx));
    }
    
    //printf("pop from %u (%u) sz %u grp sz %u\n", marks_[grp_idx] & sz_and_, marks_[grp_idx], size(), grpSize(grp_idx));
    marks_[grp_idx]++;
  }

  //pop an element from an some group, except now it's index is increased 
  void revGrpPop(int grp_idx) {
    if(do_throw) {
      if(grp_idx < 0 || grp_idx >= (int) grps())
        throw std::runtime_error("Trying to rev-pop from grp "+std::to_string(grp_idx)+" but range is 0 to "+std::to_string(grps()));
      if(grpEmpty(grp_idx))
        throw std::runtime_error("Trying to rev-pop from empty grp with idx " + std::to_string(grp_idx));
    }
    
    marks_[grp_idx+1U]--;
  }

  uint32_t grps() const { 
    return marks_.size() - 1;
  }

  int grpsi() const { 
    return marks_.size() - 1;
  }

  const D & grpEl(int grp_idx, uint32_t el_idx) const {
    if(do_throw) {
      if(grp_idx < 0 || grp_idx >= (int) grps())
        throw std::runtime_error("Trying to get el "+std::to_string(el_idx)+" from grp "+std::to_string(grp_idx)+" but grp range is 0 to "+std::to_string(grps()));
      if((int) el_idx < 0 || el_idx >= grpSize(grp_idx))
        throw std::runtime_error("Trying to get el "+std::to_string(el_idx)+" from grp "+std::to_string(grp_idx)+" but grp size is "+std::to_string(grpSize(grp_idx)));
    }
    //return data_[(marks_[grp_idx] + el_idx) % (uint32_t) data_.size()];
    return data_[(marks_[grp_idx] + el_idx) & sz_and_];
  }

  uint32_t grpSize(int grp_idx) const { 
    if(do_throw) {
      if(grp_idx < 0 || grp_idx >= (int) grps())
        throw std::runtime_error("Trying to get size from grp "+std::to_string(grp_idx)+" but range is 0 to "+std::to_string(grps()));
    }
    return marks_[grp_idx+1] - marks_[grp_idx];
  }
  int grpSizei(int grp_idx) const {
    return grpSize(grp_idx);
  }

  bool grpEmpty(int grp_idx) const { 
    if(do_throw) {
      if(grp_idx < 0 || grp_idx >= (int) grps())
        throw std::runtime_error("Trying to get check empty from grp "+std::to_string(grp_idx)+" but range is 0 to "+std::to_string(grps()));
    }
    return marks_[grp_idx+1] == marks_[grp_idx];
  }

  const D & grpHead(int grp_idx) const { 
    if(do_throw) {
      if(grp_idx < 0 || grp_idx >= (int) grps())
        throw std::runtime_error("Trying to get head from grp "+std::to_string(grp_idx)+" but range is 0 to "+std::to_string(grps()));
      if(grpEmpty(grp_idx))
        throw std::runtime_error("Trying to get head empty grp with idx " + std::to_string(grp_idx));
    }
    //return data_[marks_[grp_idx] % (uint32_t) data_.size()];
    return data_[marks_[grp_idx] & sz_and_];
  }

  const D & grpTail(int grp_idx) const { 
    if(do_throw) { 
      if(grp_idx < 0 || grp_idx >= (int) grps())
        throw std::runtime_error("Trying to get tail from grp "+std::to_string(grp_idx)+" but range is 0 to "+std::to_string(grps()));
      if(grpEmpty(grp_idx))
        throw std::runtime_error("Trying to get tail empty grp with idx " + std::to_string(grp_idx));
    }
    //return data_[(marks_[grp_idx+1]-1U) % (uint32_t) data_.size()];
    return data_[(marks_[grp_idx+1]-1U) & sz_and_];
  }

  uint32_t grpSizeGTE(int grp_idx) const { 
    if(do_throw) {
      if(grp_idx < 0 || grp_idx >= (int) grps())
        throw std::runtime_error("Trying to get grpSizeGTE from grp "+std::to_string(grp_idx)+" but range is 0 to "+std::to_string(grps()));
    }
    return marks_.back() - marks_[grp_idx];
  }

  const D & grpNextOrTail(int grp_idx) const { 
    //TODO: test
    if(do_throw) {
      if(grp_idx < 0 || grp_idx >= (int) grps())
        throw std::runtime_error("Trying to get grpNextOrTail from grp "+std::to_string(grp_idx)+" but range is 0 to "+std::to_string(grps()));
      if(grpSizeGTE(grp_idx) == 0U)
        throw std::runtime_error("Trying to get grpNextOrTail from grp "+std::to_string(grp_idx)+" but not elements remain");
    }
    if(marks_.back() - marks_[grp_idx+1]) 
      return data_[marks_[grp_idx+1] & sz_and_]; 
    return data_[(marks_[grp_idx+1]-1U) & sz_and_]; 
  }

  const D & grpNextOrTailPrv(int grp_idx) const { 
    if(do_throw) {
      if(grp_idx < 0 || grp_idx >= (int) grps())
        throw std::runtime_error("Trying to get grpNextOrTailPrv from grp "+std::to_string(grp_idx)+" but range is 0 to "+std::to_string(grps()));
      if(grpSizeGTE(grp_idx) <= 1U)
        throw std::runtime_error("Trying to get grpNextOrTailPrv from grp "+std::to_string(grp_idx)+" but not enough elements remain");
    }
    if(marks_.back() - marks_[grp_idx+1]) 
      return data_[(marks_[grp_idx+1]-1U) & sz_and_]; 
    return data_[(marks_[grp_idx+1]-2U) & sz_and_]; 
  }


};


#endif
