#ifndef ASSOC_OPS_H_
#define ASSOC_OPS_H_

#include "FixedElRing.h"
#include <utility>
#include <algorithm>
#include <string>
  
//Intro:
//
//This class caches at least the last minX1() elements seen in a ringbuffer. When a new element is 
//pushed into the ringbuffer, this class can apply a prespecified arbitrary associative operator on 
//the buffer of the cached element buffer in constant time (per element). 
//A small penalty can be paid to compute the operator on a restricted set of variable length sub-buffers.
//
//
//---------Conventions of element storage and theory----------
//
//Specifically, when an element is the most recently inserted it's 'age' is considered 0 ... every time an element is inserted into 
//the buffer, the other elements' age increases by 1. The associative operator has an inherent "left" and "right" side (note that it 
//might not be commutative). Elements on the "left" are the ones with greater age than elements on the "right". 
//
//Here is an example of a 4 element buffer which stores data that has 2 members "v" and "ts" with the age of each element underneath:
//
//buffer: { v: 4, ts: 131 }  , { v: 7, ts: 191 } ,  { v: 3, ts: 204 } , { v : 5, ts: 220 } 
//age:    3                    2                    1                   0 
//name:   el_age_3             el_age_2             al_age_1            el_age_0
//
//An operator called OP applied to this buffer will compute:
// reduction = el_age_3 OP el_age_2 OP el_age_1 OP el_age_0 
//
//Since the operator is associative we can rearrange the order in which OP is computed, but el_age_N+1 must always be 
//to the right of el_age_N :
//
//  reduction = OP(el_age_3, OP(el_age_2, OP(el_age_1, OP(el_age_0)))) = OP(OP(el_age_3, el_age_2), OP(el_age_1, el_age_0)) = ...
//
//an example associative operator "OP" is to examine the LHS and RHS 'v' values and returns:
//  if LHS.v > RHS.v 
//    return LHS
//  else
//    return RHS
//
//--------Details on bounds of sub buffers-------
//
//The class can perform the reduction on elements with age starting at X0 up to and including X1 in O(X1/minX1) * O(OP) time.
//The class needs to know the value of maxX1 and minX1 ahead of time - these inclusively bound what X1 can be. X0 can
//be from 0 to and including minX1/2 + 1 
//
//When specifying maxX1 and minX1 the class will do some rounding, so max/min/X0/X1 can be queried. 
//The associative structure is primed with an init element. 
//If you want your associative operator to ignore the init element, create an init element that stores that its
//in its state an invalid flag and have your associative operator be aware of "invalid" elements 


//Example associative operator to compute the max of a list of doubles:
struct AssocMaxD { 

  //have a type V that describes what's stored in the buffer 
  typedef double V;

  //provide a static function that applies the operator on left and right 
  //and places the result into dst 
  static void OP(V & dst, const V & left, const V & right) { 
    if(left > right)
      dst = left;
    else
      dst = right;
  }  


};

//Example associative operator to compute the concatenation of strings
struct AssocOpStrCat { 

  typedef std::string V;

  static void OP(V & dst, const V & left, const V & right) { 
    dst = left + "," + right;
  }

};


//The class...
template <typename ASSOC_OP_SPEC>
class AssocOps { 

  //setup basic types
  typedef typename ASSOC_OP_SPEC::V V_t;
  typedef FixedElRing<V_t> Ring;
  typedef FixedBasicFullElRingAnySize<Ring> RingRing; 

  RingRing forw_;
  RingRing rev_;
  Ring els_;

  int sz_;    

public:

  //Query bounds getters: 
  //
  //x1 should be >= minX1()
  int minX1() const { 
    return sz_ / 2;
  }
  //x1 should be <= maxX1()
  int maxX1() const { 
    return sz_ / 2 * rev_.size();
  }
  //x0() should be >= minX0()
  int minX0() const {
    return 0;
  }
  //x1 should be <= maxX0()
  int maxX0() const { 
    return sz_ / 2 + 1;
  }

  
  AssocOps(int max_x1, int min_x1, const V_t & init_v) { 
  
    //make sure order of min and max is correct
    if(max_x1 < min_x1) { 
      throw std::runtime_error("min_x11 is greater than max_x11");
    }

    //make the physical sz a multiple of 2. This avoids complications later on 
    sz_ = (min_x1 / 2) * 2;
    
    //make sz even...
    if(sz_ / 2 * 2 != sz_ || sz_ <= 0)
      throw std::runtime_error("sz of AssocOps needs to be a positive multiple of 2, but is: " + std::to_string(sz_) + " minx: " + std::to_string(min_x1) + " maxx:" + std::to_string(max_x1));

    //basically we have these things called "rings". We have 2 forward rings and use 1 of them to 
    //compute the associative operator on sz_/2 through sz_ elements. We also have some number of 
    //reverse rings, each of which store sz_/2 elements each except the first which stores fewer
    //and cannot be accessed for queries. Here we calculate how many reverse rings in a naive way:
    int min_rings = 0;
    while((min_rings * sz_ / 2) < max_x1) {
      min_rings++;
    }

    //we need at least 2 rings in order to be able to do this effectively ...
    if(min_rings < 2)
      throw std::runtime_error("LOGIC ERROR!!! Need at least 2 rings, but got: " + std::to_string(min_rings) + " minx: " + std::to_string(min_x1) + " maxx:" + std::to_string(max_x1));
  
    //we need to keep els around for reverse ring
    els_.reset(sz_);
 
    //use a temporary ring to initialize the storage of the RingRings
    //A RingRing is a ringbuffer of ringbuffers. 
    Ring tmp;
    tmp.reset(sz_);

    //To understand how this works is pretty tricky... ask me to draw the triangle picture
    //These are two overlapping forward buffers
    forw_.reset(2);
    forw_.setAllStorageTo(tmp);

    //Rev stores min_rings of cached data also can be explained by triangles ( iluminati confirmed )
    tmp.reset(sz_ / 2);
    rev_.reset(min_rings);
    rev_.setAllStorageTo(tmp);
  
    //To get the state all sync'd up we only need the forw_[1] to have sz/2 more elements than forw_[0]
    //This is important because the size of forw_[1] is used to trigger state changes when elements are 
    //inserted into the buffer
    forw_[1].pushKnown(init_v);
    for(int i = 0; i < sz_/2; i++) {
      V_t & f_dst = forw_[1].pushAndRef();
      ASSOC_OP_SPEC::OP(f_dst, forw_[1].revIdx(1), init_v);
    }
    forw_[0].pushKnown(init_v);
    
    //We init the other elements to prevent uninitialized
    //memory access error in debuggers...
    for(int i = 0; i < rev_.size(); i++)
      rev_[i].pushKnown(init_v);
    
    //initialize els with a single init elements to prevent an unitialized memory access in the debugger
    els_.forcePush(init_v);
    
    //now we push enough elements so that queries don't 
    //reference uninitialized elements and all the query
    //structures are filled properly 
    for(int i = 0; i < max_x1 + min_x1; i++)
      push(init_v);
  }

 
  void push(const V_t & v) { 
    if(forw_[1].size() == sz_) { 

      //This basically swaps forw_[0] and forw_[1]
      //By convention buf_[i+1] is older than buf_[i]
      //We swap [0] with [1] and clear the elements from 
      //[0] and place the newest element in there   
      forw_.advance();
      forw_[0].clear();
      forw_[0].pushKnown(v);

      //The element is also accumulated into what was previously
      //the youngest forward buffer. Due to the akward definition
      //of OP we need to do this two step OP application.
      // A more clear pseudocode of what happens is:
      // forw_[1].pushBack( OP( forw_[1].tail(), v ) );
      V_t & f_dst = forw_[1].pushAndRef();
      ASSOC_OP_SPEC::OP(f_dst, forw_[1].revIdx(1), v);//multiply the pre-tail by the new element 
      
      //Rev goes through a similar thing, except that there
      //may be several rev buffers. 
      rev_.advance();
      rev_[0].clear();
      rev_[0].pushKnown(els_.revIdx(0));

      //Keep track of all elements to be used to construct the rest of rev
      //forcePush evicts elements from els as necassary 
      els_.forcePush(v);

      //everything O(OP) up to here

    } else { 

      //This is what's called most of the time:
      //place the element into both forward lists
      V_t & f_dst0 = forw_[0].pushAndRef();
      ASSOC_OP_SPEC::OP(f_dst0, forw_[0].revIdx(1), v);
      V_t & f_dst1 = forw_[1].pushAndRef();
      ASSOC_OP_SPEC::OP(f_dst1, forw_[1].revIdx(1), v);
      
      //place the element into the current reverse list
      //remember that these elements are being accumulated in reverse
      //so the tail of this list is actually the _right_ side for the OP
      V_t & rev_dst = rev_[0].pushAndRef();
      ASSOC_OP_SPEC::OP(rev_dst, els_.revIdx((forw_[0].size()-1)*2), rev_[0].revIdx(1));

      //Once again keep track of all elements so they can be used in rev
      els_.forcePush(v);
    
      //All O(OP)
    }
  }
  
private:
  //cached storage for the query, this enables us do operations on structures that 
  //require internal storage without doing allocations if OP and V_t is implemented 
  //carefully and reuses internal storage. 
  V_t tmp0_; 
  V_t tmp1_; 
public:

  //x0 is the position of the first element in terms of age ... valid ranges are 0 through maxX0()
  //x1 is the position after the last element in terms of age ... valid ranges are minX1() through maxX1()
  //typically maxX0() = minX1()/2 + 1
  //The runtime will be O(OP) * O(X1 / minX1)
  void query(V_t & dst, int x1, int x0) { 
    
    //include x1 in our return value, just convention
    x1++; 

    //throw exception if we can't generate the buffer reduction. Actually sometimes
    //x0 and x1 can be outside of their computed bounds and we can still get the buffer. 
    //For now we allow these bound violations to succeed... but maybe we should be strict 
    //and enforce a constant min/max/X0/X1
    if(x0 > forw_[1].size() || x1 < forw_[1].size() || x0 >= x1 || x0 < 0 || x1 < 0) { 
      throw std::runtime_error("invalid x0 or x1 in query x0: " + std::to_string(x0) + " x1: " + std::to_string(x1) + " forw_[1].size(): " + std::to_string(forw_[1].size()));
      return;
    }

    //We re-purpose x1 to track how many elements we need to go past to reach the end of our selection
    //as we iterate through the internal buffer.. We have to have the "end" of our element selection 
    //be past forw_[1] so remove all those elements from x1 off the bat.
    x1 -= forw_[1].size();
    if(!x1) { 
      //If we don't need any more elements, then we must be using only forw_[1].
      //Select the portion of forw_[1] that contains elements of age x0 through x1 (the end)
      dst = forw_[1][forw_[1].size() - x0 - 1];
      //We're done!
      return;
    }

    //We deal with an edge case when x0 == maxX0()
    int i;
    if(x0 == forw_[1].size()) { 
      //Edge case confirmed, we need to ignore forw_[1]
      //and initialize our state from rev_[1] 
      if(rev_[1].size() >= x1) {
        //rev_[1] contains x1 also! so just return the 
        //appropriate subset of rev_[1]
        dst = rev_[1][x1-1];
        return;
      } else { 
        //init tmp to rev_[1]
        tmp0_ = rev_[1].tail();
        x1 -= rev_[1].size();
        i = 2;
      }
    } else { 
      //Non-edge case. Here we put the elements with
      //ages x0 to to forw_[1].size()-1 into our result  
      //accumulator
      tmp0_ = forw_[1][forw_[1].size() - x0 - 1];
      i = 1;
    }  
    
    //Use pre-allocated temporary storage when calling OP and storing temporary indermediate results
    auto * t0 = &tmp0_;
    auto * t1 = &tmp1_;

    for(; i < (int) rev_.size(); i++) { 
      //Ok, we basically go through the lists and accumulate results until we have enough elements
      //to reach x1. Each list has minX1/2 elements in it, so this will take O(X1/minX1)*O(OP). Note 
      //that big-O notation doesn't care about the factor of 2.  
      if(rev_[i].size() >= x1) {
        //we've reached x1, peace out!
        ASSOC_OP_SPEC::OP(dst, rev_[i][x1-1], *t0);
        return;
      } else { 
        //didn't reach x1 so consume the whole list in O(OP)
        ASSOC_OP_SPEC::OP(*t1, rev_[i].tail(), *t0);
        std::swap(t0, t1);
        x1 -= rev_[i].size();
      }
    }
    //We couldn't generate the buffer because X1 > maxX1
    throw std::runtime_error("Exceeded size of buffer by " + std::to_string(x1) + " recomended max is " + std::to_string(maxX0()) + " and els_.size() is " + std::to_string(els_.size())) ;
    dst = *t0;
  }

  //a wrapper to avoid the akward query syntax
  V_t queryRet(int x1, int x0) { 
    V_t tmp;
    query(tmp, x1, x0);
    return tmp;
  }

}; 







#endif
