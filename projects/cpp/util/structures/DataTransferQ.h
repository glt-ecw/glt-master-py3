#ifndef DATA_TRANSFER_QUEUE_H_
#define DATA_TRANSFER_QUEUE_H_

#include <vector>
#include <cstdint>
#include <cstring>
#include <cstdio>

//a single producer single consumer stream between two threads
class DataTransferQ {

  //data for the consumer
  uint32_t start_;
  uint32_t cached_end_;
  
  char pad0[64 - sizeof(uint32_t) * 2];//cache line padding
  
  //data for the producer
  uint32_t end_;
  uint32_t cached_start_;

  char pad1[64 - sizeof(uint32_t) * 2];//cache line padding 

  //read only or debug
  char * data_;
  uint32_t sz_;
  uint32_t sz_and_;
  uint32_t alloc_sz_;

public:

  DataTransferQ(uint32_t sz, uint32_t max_el_sz) { 
    uint32_t sz2 = 16;
    while(sz2 < sz)
      sz2 *= 2;
    if(max_el_sz < 16)
      max_el_sz = 16;    
    max_el_sz = (max_el_sz + 3U) / 4U * 4U;

    alloc_sz_ = sz2 + max_el_sz + 8; 
    sz_ = sz2;
    sz_and_ = sz2 - 1U;
    data_ = new char[alloc_sz_];
    
    start_ = 0;
    cached_end_ = 0; 
    end_ = 0;
    cached_start_ = 0;
  } 

  char * getPushable(uint32_t push_sz) { 
    //round up to size to nearest 4 
    uint32_t sz4 = (push_sz + 3U) / 4U * 4U;
    char * end_ptr = data_ + (end_ & sz_and_);

    //try to see if there's enough space 
    //end_ - cached_start_  is basically the size from the producer perspective
    //see if adding sz4 and 4 more bytes puts us past the limit of the queue size
    if(end_ - cached_start_ + sz4 + 4U <= sz_)
      return end_ptr + 4U; //we reserve 4 bytes for the "sz" argument
    
    //not enough space so re-read the start variable
    cached_start_ = *(volatile uint32_t *) &start_;    

    //yay, the consumer must have consumed (only reason why start_ may have moved)
    if(end_ - cached_start_ + sz4 + 4U <= sz_)
      return end_ptr + 4U;
    
    //nopes.... start_ did not move
    return 0;
  }


  void pushKnown(char * dst, uint32_t push_sz) { 
    //ok we've written the data and want to commit it, first write the size of the element
    *(volatile uint32_t *) (dst - 4) = push_sz;
    //now it's time to let the consumer know that more data has been added
    //TODO: ...perform a synchronizing write barrier here..., but can writes be re-ordered on x86?
    *(volatile uint32_t *) &end_ = end_ + (push_sz + 3U)/4U*4U + 4U;
  }

  //2nd version that recalculates the end pos, not sure if this is faster or slower
  void pushKnown(uint32_t push_sz) { 
    //ok we've written the data and want to commit it, first write the size of the element
    *(volatile uint32_t *) (data_ + (end_ & sz_and_)) = push_sz;
    //now it's time to let the consumer know that more data has been added
    //TODO: ...perform a synchronizing write barrier here..., but can writes be re-ordered on x86?
    *(volatile uint32_t *) &end_ = end_ + (push_sz + 3U)/4U*4U + 4U;
  }
  
  //this places data in the buffer, if we already know the size and have the data waiting
  //might be faster by a miniscule amount subject to compiler optimizations ...
  bool pushBufCombo(const char * push_data, uint32_t push_sz) { 
    uint32_t sz4 = (push_sz + 3U) / 4U * 4U;
    char * end_ptr = data_ + (end_ & sz_and_);
    //printf("end - cs %u\n", end_ - cached_start_);
    if(end_ - cached_start_ + sz4 + 4U <= sz_) {
      memcpy(end_ptr + 4U, push_data, push_sz);
      *(volatile uint32_t *) end_ptr = push_sz;
      *(volatile uint32_t *) &end_ = end_ + sz4 + 4U;
      return true;
    }
    
    cached_start_ = *(volatile uint32_t *) &start_;    
      //printf("reload end - cs %u\n", end_ - cached_start_);
    //TODO: may need some barrier here? 
    
    if(end_ - cached_start_ + sz4 + 4U <= sz_) {
      memcpy(end_ptr + 4U, push_data, push_sz);
      *(volatile uint32_t *) end_ptr = push_sz;
      *(volatile uint32_t *) &end_ = end_ + sz4 + 4U;
      return true;
    }
    return false;
  }
  
  void pushBufComboSpin(const char * push_data, uint32_t push_sz) { 
    uint32_t sz4 = (push_sz + 3U) / 4U * 4U;
    char * end_ptr = data_ + (end_ & sz_and_);
    //printf("end - cs %u\n", end_ - cached_start_);
    if(end_ - cached_start_ + sz4 + 4U <= sz_) {
      memcpy(end_ptr + 4U, push_data, push_sz);
      *(volatile uint32_t *) end_ptr = push_sz;
      *(volatile uint32_t *) &end_ = end_ + sz4 + 4U;
      return;
    }
   
    while(1) {  
      cached_start_ = *(volatile uint32_t *) &start_;    
        //printf("reload end - cs %u\n", end_ - cached_start_);
      //TODO: may need some barrier here? 
      
      if(end_ - cached_start_ + sz4 + 4U <= sz_) {
        memcpy(end_ptr + 4U, push_data, push_sz);
        *(volatile uint32_t *) end_ptr = push_sz;
        *(volatile uint32_t *) &end_ = end_ + sz4 + 4U;
        return;
      }
    }
  }


  uint32_t estSizeAsConsumer() {
    cached_end_ = *(volatile uint32_t *)&end_;
    return cached_end_ - start_;
  }

  char * getPoppable(uint32_t & pop_sz) { 
    
    char * start_ptr = data_ + (start_ & sz_and_);
   
    //is there any data in the queue? 
    if(cached_end_ - start_) { 
      //the raw size is stored
      pop_sz = *(volatile uint32_t *) start_ptr;
      //the data is write after the size pointer
      return start_ptr + 4U;
    }
    
    //nope... ok try to reread to see if the producer has moved the end of the queue by adding data?
    cached_end_ = *(volatile uint32_t *)&end_;
    //TODO: is there anything in the CPU that may prevent 
    //the stuff sitting in the data_ buffer from not being
    //read properly? should we put barriers here?... (see x86 TSO)
    //maybe a read barrier should be in place?
    
    //has the end of the queue changed after rereading the end?
    if(cached_end_ - start_) { 
      pop_sz = *(volatile uint32_t *) start_ptr;
      return start_ptr + 4U;
    }

    //nope...
    return 0;
  }

  void popKnown(uint32_t pop_sz) { 
    //... we keep the queue pointers at multiples of 4 and need to add 4 for the 
    //raw size pointer ....
    *(volatile uint32_t *) &start_ = start_ + (pop_sz + 3U)/4U*4U + 4U; 
  }



}; 





#endif
