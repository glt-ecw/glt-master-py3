#ifndef FIXED_EL_RING_H_
#define FIXED_EL_RING_H_

#include <vector>
#include <cstdint>
#include <stdexcept>

//TODO: needs test

//Note: the size of the FixedElRing is restricted to a power of 2 for 
//implementation simplicity and running speed. Having non-powers of two 
//needs more changes than just a modulo because:
//   (end_ % 2^32) % sz_ = X and ((end_ + Y) % 2^32) % sz_ may not be X+Y if end_+Y wraps around
//example:
//  let's say we're dealing with 3 bits, so 8, end is 6 and sz is 3 (end is at 0)
//  if we add 2 to end we should end up at (6+2) % 3 = 2 but due to overflow 8 is treated as 0 and 0 % 3 is 0
template <typename T>
class FixedElRing {

  std::vector<T> data_;
  uint32_t start_;
  uint32_t end_;
  uint32_t sz_;
  uint32_t sz_and_;

public:
 
  void swap(FixedElRing<T> & other) { 
    std::swap(data_, other.data_);
    std::swap(start_, other.start_);
    std::swap(end_, other.end_);
    std::swap(sz_, other.sz_);
    std::swap(sz_and_, other.sz_and_);
  }


  FixedElRing() {
    reset(1);
  } 
  
  FixedElRing(int sz) {
    reset(sz);
  }

  void setAllStorageTo(const T & d) { 
    for(int i = 0; i < (int) data_.size(); i++)
      data_[i] = d;
  }
  
  void reset(int sz) { 
    if(sz <= 0)
      throw std::runtime_error("Size for FixedElRing is too small: " + std::to_string(sz));    
    int sz2 = 1;
    while(sz2 < sz)
      sz2 *= 2;
    data_.resize(sz2);
    sz_ = sz2;
    sz_and_ = sz2-1U;
    start_ = 0;
    end_ = 0; 
  }

  void resize(int sz) {
    if(sz <= 0)
      throw std::runtime_error("Re-Size for FixedElRing is too small: " + std::to_string(sz));    
    std::vector<T> new_data;
    int sz2 = 1;
    while(sz2 < sz)
      sz2 *= 2;
    new_data.resize(sz2);
    
    int old_sz = size();
    for(int i = 0 ; i < old_sz && i < sz; i++)
      new_data[i] = (*this)[i];

    sz_ = sz2;
    sz_and_ = ((uint32_t)sz2)-1U;
    //printf("resized from %i to %u sz and is %x\n", old_sz, sz_, sz_and_);
    start_ = 0;
    end_ = old_sz < sz ? old_sz : sz;
    data_ = new_data;
  }

  //does the resize
  void pushSafe(const T & v) { 
    if(size() == maxSize()) { 
      resize(2*size());
    }
    pushKnown(v);
  }

  void clear() { 
    start_ = 0;
    end_ = 0;
  }

  int maxSize() const {
    return data_.size();
  }

  int size() const {
    return end_ - start_;
  }

  bool empty() const {
    return end_ == start_;
  }

  bool full() const {
    return end_ - start_ == sz_;
  }

  //no bounds check
  void pushKnown(const T & v) { 
    data_[end_ & sz_and_] = v;
    end_++;
  }

  void popTailKnown() { 
    end_--;
  }

  void popSafe() { 
    if(end_ != start_)
      start_++;
  }

  //no bounds check
  void popKnown() { 
    start_++;
  }

  //no bounds check
  const T & head() const { 
    return data_[start_ & sz_and_];  
  }
  
  //no bounds check
  T & head() { 
    return data_[start_ & sz_and_];  
  }
  
  //no bounds check
  const T & tail() const { 
    return data_[(end_ - 1U) & sz_and_];  
  }
  
  //no bounds check
  T & tail() { 
    return data_[(end_ - 1U) & sz_and_];  
  }
  
  //no bounds check
  T & pushAndRef() {
    end_++;
    return tail();
  }    

  //evicts the head element 
  void forcePush(const T & v) { 
    if(full())
      start_++; 
    pushKnown(v);
  }

  //no bounds check
  const T & operator[](uint32_t idx) const { 
    return data_[(start_ + idx) & sz_and_];
  }
  
  //no bounds check
  T & operator[](uint32_t idx) { 
    return data_[(start_ + idx) & sz_and_];
  }
  
  //no bounds check
  const T & revIdx(uint32_t r_idx) const { 
    return data_[(end_ - 1U - r_idx) & sz_and_];
  }

  //no bounds check
  T & revIdx(uint32_t r_idx) { 
    return data_[(end_ - 1U - r_idx) & sz_and_];
  }

};



//See comment about advance() function call
template <typename T>
class FixedBasicFullElRingAnySize { 
  
  std::vector<T> data_;
  uint32_t cur_;

public:
  
  FixedBasicFullElRingAnySize() {
    reset(1);
  } 
  
  FixedBasicFullElRingAnySize(int sz) {
    reset(sz);
  }

  void setAllStorageTo(const T & d) { 
    for(int i = 0; i < (int) data_.size(); i++)
      data_[i] = d;
  }
  
  void reset(int sz) { 
    if(sz <= 0)
      throw std::runtime_error("Size for FixedFullElRingAnySize is too small: " + std::to_string(sz));    
    data_.resize(sz);
    cur_ = 0;
  }

  int size() const {
    return data_.size();
  }

  //This function makes it so that what was referenced by RING[IDX] must now be referenced by RING[IDX+1]
  //This increases the IDX of each element by 1. If one overwrites RING[0] every time advance is called then 
  //the oldest elements will have the largest IDX.
  void advance() { 
    if(cur_ == 0)
      cur_ = data_.size()-1;
    else
      cur_--;
  }
  
  //NOTE: idx must be positive and less than 2^32 - data_.size()
  T & operator[](uint32_t idx) { 
    return data_[(cur_ + idx) % data_.size()];
  }
  

};




#endif
