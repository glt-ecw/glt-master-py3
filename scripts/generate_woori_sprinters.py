import sys
import os
import json
import pandas as pd
import numpy as np
import glt.db

from collections import defaultdict, deque
from glt.math import bs

BIZDAYS = 260.0
INTEREST_RATE = 0.015

MAX_ORDER = 1988
MIN_TICK = 0.01

BID_PREMIUM_BOOST = 1.05

IU_TICK_THRESHOLD = 0.001
IU_ITERATIONS = 100


def add_options_defs(vols):
    # strike and call/put?
    defs = vols['shortname'].str.extract(r'_([0-9\.]+)([CP])', expand=True)
    vols['strike'] = defs[0].astype(float)
    vols['op'] = defs[1].apply(lambda x: 1 if x == 'C' else -1)


def filter_front_otm(vols, futures=None):
    # compute intrinsic
    if futures is None:
        raise ValueError('need futures value')
    vols['u'] = futures + vols['roll']
    vols['intrinsic'] = (vols['op'] * (vols['u'] - vols['strike'])).apply(lambda x: max(x, 0))
    # which term?
    vols['term'] = vols['shortname'].apply(lambda s: s[4:6] + s[3])
    terms = vols['term'].unique()
    terms.sort()
    return vols.loc[(vols['term'] == terms[0]) & (vols['intrinsic'] == 0)].sort_values('strike')


def add_yte(vols, tradedate):
    cmd = """
        SELECT expdate
        FROM SECURITIES.options
        WHERE id=%d
    """ % vols.iloc[0]['secid']
    expiry = glt.db.autoclose_query(cmd)['expdate'].astype(str).iloc[0]
    vols['dte'] = np.busday_count(tradedate, expiry) + 1
    vols['yte'] = vols['dte'] / BIZDAYS


def add_greeks(vols):
    greeks = np.zeros((len(vols), 2), dtype=float)
    for i, (op, x, u, v, t) in enumerate(zip(*(vols[col] for col in ['op', 'strike', 'u', 'vol', 'yte']))):
        greeks[i, 0] = bs.tv(x, u, v, t, INTEREST_RATE, op)
        greeks[i, 1] = bs.delta(x, u, v, t, INTEREST_RATE, op)
    vols['tv'] = greeks[:, 0]
    vols['delta'] = greeks[:, 1]


def query_front_vols(tdate, futures=None):
    tradedate = pd.to_datetime(tdate).date().isoformat()
    cmd = """
        SELECT
            securityid as secid,
            model_volatility as vol,
            roll
        FROM eod.vols
        WHERE (
            tradedate='%s'
        )
    """ % tradedate
    vols = glt.db.autoclose_query(cmd)
    secids_str = ', '.join(map(str, vols['secid'].unique()))
    cmd = """
        SELECT securities.id as secid, shortname, expdate
        FROM SECURITIES.securities
        LEFT JOIN SECURITIES.options
        ON (securities.id=options.id)
        WHERE (
            securities.id in (%s)
            AND
            shortname regexp '^KOO'
            AND
            expdate>'%s'
        )
    """ % (secids_str, tradedate)
    secs = glt.db.autoclose_query(cmd)
    vols = vols.loc[vols['secid'].isin(secs['secid'].values)]
    vols = pd.merge(vols, secs, on='secid', how='left')
    add_options_defs(vols)
    otm = filter_front_otm(vols, futures=futures)
    add_yte(otm, tradedate)
    add_greeks(otm)
    otm['abs_delta'] = np.abs(otm['delta'])
    return otm.reset_index(drop=True)


def option_order_qty(proposed_qty, mod_qty=50):
    qty = min(MAX_ORDER, proposed_qty)
    if qty % mod_qty == 0:
        qty += np.random.choice([-11, 11])
    return qty


class OptionsOrderCreator:
    def __init__(self, dte, bidqty_map=None, askqty_map=None, underlying_performance=None,
                 max_aggro_orders=0, qty_multiplier=0.0):
        # these are arbitrarily decided
        if dte > 20:
            lo_ddte = 0.3
            hi_ddte = 0.7
            xtra = 0.6
        elif dte > 10:
            lo_ddte = 0.3
            hi_ddte = 0.6
            xtra = 0.8
        elif dte >= 5:
            lo_ddte = 0.4
            hi_ddte = 0.6
            xtra = 1.0
        elif dte >= 3:
            lo_ddte = 0.5
            hi_ddte = 0.5
            xtra = 2.0
        elif dte >= 2:
            lo_ddte = 0.6
            hi_ddte = 0.5
            xtra = 3.0
        elif dte >= 1:
            lo_ddte = 0.6
            hi_ddte = 0.5
            xtra = 5.0
        self.lo_dyte = lo_ddte / BIZDAYS
        self.hi_dyte = hi_ddte / BIZDAYS
        self.xtra = xtra
        self.bidqty_map = dict(bidqty_map)
        self.askqty_map = dict(askqty_map)
        self.bid_max_u_performance = underlying_performance['bids']
        self.ask_max_u_performance = underlying_performance['asks']
        self.aggressive_bid_max_u_performance = underlying_performance['aggressiveBids']
        self.max_aggro_orders = max_aggro_orders
        self.qty_multiplier = qty_multiplier
        self.premium_boost = 1.0

    def generate_orders(self, selected, batch_number=0, bid_orders_map=None, ask_orders_map=None):
        rel_cols = ['secid', 'shortname', 'op', 'strike', 'u', 'vol', 'yte', 'abs_delta']
        bid_orders, ask_orders = [], []
        used_bids, used_asks = 2 * [None]
        for secid, name, op, x, u, v, t, delta in zip(*(selected[col] for col in rel_cols)):
            delta_key = int(100 * delta)
            # find used prices in orders maps
            if bid_orders_map is not None:
                used_bids = bid_orders_map[secid]
            if ask_orders_map is not None:
                used_asks = ask_orders_map[secid]
            # calculate competitive bids
            for bidprc, bidqty in self.calc_bid_orders(op, x, u, v, t):
                bid_orders.append(['bid', secid, name, op, bidprc, bidqty, delta_key, batch_number, False])
                if used_bids is not None:
                    used_bids.add(bidprc)
            # calculate aggressive bids
            for bidprc, bidqty in self.calc_aggressive_bid_orders(used_bids, op, x, u, v, t):
                bid_orders.append(['bid', secid, name, op, bidprc, bidqty, delta_key, batch_number, True])
                if used_bids is not None:
                    used_bids.add(bidprc)
            # calculate competitive asks
            for askprc, askqty in self.calc_ask_orders(op, x, u, v, t):
                ask_orders.append(['offer', secid, name, op, askprc, askqty, delta_key, batch_number, False])
                if used_asks is not None:
                    used_asks.add(askprc)
        return bid_orders, ask_orders

    def calc_bid_orders(self, op, x, u, v, t):
        bid_t = t - self.hi_dyte
        implied_u = u
        otm_tv = bs.tv(x, u, v, bid_t, INTEREST_RATE, op)
        bidprc = round(otm_tv, 2)
        max_du = self.bid_max_u_performance * u
        premium_boost = self.premium_boost
        while bidprc > MIN_TICK and abs(implied_u - u) < max_du:
            otm_delta = bs.delta(x, implied_u, v, bid_t, INTEREST_RATE, op)
            key = int(round(100 * abs(otm_delta)))
            if key in self.bidqty_map:
                bidqty = option_order_qty(
                    int(self.xtra * self.qty_multiplier * premium_boost * self.bidqty_map[key])
                )
                yield (bidprc, bidqty)
                premium_boost *= BID_PREMIUM_BOOST
            bidprc = round(bidprc - MIN_TICK, 2)
            implied_u = bs.iu(x, v, bid_t, INTEREST_RATE, op, bidprc, implied_u, IU_TICK_THRESHOLD, IU_ITERATIONS)

    def calc_aggressive_bid_orders(self, current_bid_orders, op, x, u, v, t):
        bid_t = t - self.lo_dyte
        otm_tv = bs.tv(x, u, v, bid_t, INTEREST_RATE, op)
        bidprc = round(otm_tv, 2)
        lowest_bidprc = max(current_bid_orders) if current_bid_orders else 0
        implied_u = u
        max_du = self.aggressive_bid_max_u_performance * u
        premium_boost = self.premium_boost / BID_PREMIUM_BOOST
        order_count = 0
        while bidprc > MIN_TICK and abs(implied_u - u) < max_du:
            if order_count >= self.max_aggro_orders:
                break
            otm_delta = bs.delta(x, implied_u, v, bid_t, INTEREST_RATE, op)
            key = int(round(100 * abs(otm_delta)))
            if key in self.bidqty_map and bidprc > lowest_bidprc:
                bidqty = option_order_qty(
                    int(self.xtra * self.qty_multiplier * premium_boost * self.bidqty_map[key])
                )
                yield (bidprc, bidqty)
                premium_boost /= BID_PREMIUM_BOOST
            bidprc = round(bidprc + MIN_TICK, 2)
            implied_u = bs.iu(x, v, bid_t, INTEREST_RATE, op, bidprc, implied_u, IU_TICK_THRESHOLD, IU_ITERATIONS)
            order_count += 1

    def calc_ask_orders(self, op, x, u, v, t):
        ask_t = t - self.lo_dyte
        implied_u = u
        otm_tv = bs.tv(x, u, v, ask_t, INTEREST_RATE, op)
        askprc = round(otm_tv, 2)
        max_du = self.ask_max_u_performance * u
        while askprc > MIN_TICK and abs(implied_u - u) < max_du:
            otm_delta = bs.delta(x, implied_u, v, ask_t, INTEREST_RATE, op)
            key = int(round(100 * abs(otm_delta)))
            if key in self.askqty_map:
                askqty = option_order_qty(
                    int(self.xtra * self.qty_multiplier * self.askqty_map[key])
                )
                yield (askprc, askqty)
            askprc = round(askprc + MIN_TICK, 2)
            implied_u = bs.iu(x, v, ask_t, INTEREST_RATE, op, askprc, implied_u, IU_TICK_THRESHOLD, IU_ITERATIONS)


class OptionSelector:
    def __init__(self, df, which, how_many=3, upper_delta=0.051, low_tv=0.02):
        self.df = df
        if which == 'call':
            self.op = 1
        elif which == 'put':
            self.op = -1
        else:
            raise ValueError('which should be "call" or "put"')
        self.how_many = how_many
        self.upper_delta = upper_delta
        self.low_tv = low_tv
        self.used_secids = set()
        self.last_idx = None

    def next_batch(self):
        df = self.df
        used = self.used_secids
        low = self.low_tv
        delta = self.upper_delta
        # select next chunk
        if self.how_many <= 0:
            print('self.how_many <= 0 for self.op=%d' % self.op)
            return None
        if self.op > 0:
            selected = df.loc[(~df['secid'].isin(used)) & (df['tv'] > low) & (df['abs_delta'] <= delta)].iloc[
                       :self.how_many]
        else:
            selected = df.loc[(~df['secid'].isin(used)) & (df['tv'] > low) & (df['abs_delta'] <= delta)].iloc[
                       -self.how_many:]
        # if there is nothing selected, return None
        if len(selected) == 0:
            return None
        else:
            if self.op > 0:
                if self.last_idx is None or self.last_idx == selected.index[0]:
                    self.last_idx = selected.index[-1] + 1
                else:
                    print('whoa nelly! looks like we are done here')
                    return None
            else:
                if self.last_idx is None or self.last_idx == selected.index[-1]:
                    self.last_idx = selected.index[0] - 1
                else:
                    print('whoa nelly! looks like we are done here')
                    return None
        self.how_many = max(self.how_many - 1, 1)
        self.used_secids.update(selected['secid'].values)
        return selected.sort_values('abs_delta')


def orders_to_dataframe(orders):
    df = pd.DataFrame(orders, columns=['side', 'secid', 'shortname', 'op', 'price', 'qty', 'delta', 'batch', 'aggro'])
    df['allowed'] = True
    df['secid'] = df['secid'].round().astype(int)
    return df


def find_max_allowed_bid_prices(max_prices):
    last_prices = set()
    max_map = max_prices.to_dict()
    for secid, price in max_prices.items():
        price = round(price, 2)
        while price in last_prices:
            price = round(price - MIN_TICK, 2)
        max_map[secid] = price
        last_prices.add(price)
    return max_map


def calc_redundant_orders(orders, options_specs):
    orders.columns
    redundancy_specs = options_specs['competitiveRedundancy']
    repeat_by = params['numberOfPids']
    secid_repeated = defaultdict(int)
    actual_orders = []
    for i, row in orders.iterrows():
        order_row = row.tolist()
        # last column is whether we should place the order
        if not order_row[-1]:
            continue
        # and the 8th column is whether the order is aggressive
        side = order_row[0]
        if order_row[8] or redundancy_specs[side] is None:
            actual_orders.append(order_row)
            continue
        delta_map = redundancy_specs[side]['delta_map']
        delta = order_row[6]
        if delta in delta_map:
            secid = order_row[1]
            if secid_repeated[secid] == delta_map[delta]:
                actual_orders.append(order_row)
                continue
            for _ in range(redundancy_specs[side]['repeatBy']):
                actual_orders.append(order_row)
            secid_repeated[secid] += 1
        else:
            actual_orders.append(order_row)

    return pd.DataFrame(actual_orders, columns=orders.columns)


def random_qty(base, dev=0):
    if dev < 0:
        dev = 0
    if dev == 0:
        return base
    return np.random.randint(base - dev, base + dev)


def find_front_futures(tdate, params):
    tradedate = pd.to_datetime(tdate).date()
    cmd = """
        select secid, shortname, expdate
        from
        (
            select id as secid, shortname
            from SECURITIES.securities
            where (
            shortname regexp '^(%s)[A-Z][0-9]{2}$'
            )
        ) as futs
        left join (
            select id, expdate from SECURITIES.futures
            where expdate>'%s'
        ) as expiry
        on (futs.secid=expiry.id)
        where expdate is not null
        order by expdate
    """ % ('|'.join(params['contracts']), tradedate)
    contracts = glt.db.autoclose_query(cmd)
    contracts['product'] = contracts['shortname'].str.extract('(.+).[0-9]{2}', expand=False)
    print('found futures contracts')
    print(contracts.to_string())
    front_map = {}
    for product in contracts['product'].unique():
        idx = params['contracts'][product]['whichMonthIndex']
        df = contracts.loc[contracts['product']==product].iloc[idx]
        front_map[product] = df['secid']
    return front_map


def generate_futures_orders(params, product, secid, center_price):
    futures_params = params['contracts'][product]
    specs = futures_params['specifications']

    num_bids = futures_params['numberOfBids']
    num_asks = futures_params['numberOfAsks']
    num_aggro = specs['numberOfAggressiveOrders']

    num_bids -= num_aggro / 2
    num_asks -= num_aggro / 2

    ticks_away = specs['pointsAwayFromCenter']
    qty = specs['qty']
    qty_dev = specs['randomQtyDeviation']
    aggro_qty = specs['aggressiveQty']
    tick = specs['tick']

    bidprc = round(center_price - ticks_away, 2)
    askprc = round(center_price + ticks_away, 2)

    aggro_bidprc = round(bidprc + tick, 2)
    aggro_askprc = round(askprc - tick, 2)

    orders = []
    place_bid = True
    while num_bids > 0 and num_asks > 0:
        if place_bid:
            orders.append(['bid', secid, product, 0, bidprc, random_qty(qty, qty_dev), -1, 0, False])
            bidprc = round(bidprc - tick, 2)
            num_bids -= 1
        else:
            orders.append(['offer', secid, product, 0, askprc, random_qty(qty, qty_dev), -1, 0, False])
            askprc = round(askprc + tick, 2)
            num_asks -= 1
        place_bid = not place_bid

    while num_bids > 0:
        orders.append(['bid', secid, product, 0, bidprc, random_qty(qty, qty_dev), -1, 0, False])
        bidprc = round(bidprc - tick, 2)
        num_bids -= 1

    while num_asks > 0:
        orders.append(['offer', secid, product, 0, askprc, random_qty(qty, qty_dev), -1, 0, False])
        askprc = round(askprc + tick, 2)
        num_asks -= 1

    place_bid = True
    while num_aggro > 0:
        if place_bid:
            orders.append(['bid', secid, product, 0, aggro_bidprc, random_qty(aggro_qty, qty_dev), -1, 0, True])
            aggro_bidprc = round(aggro_bidprc + tick, 2)
        else:
            orders.append(['offer', secid, product, 0, aggro_askprc, random_qty(aggro_qty, qty_dev), -1, 0, True])
            aggro_askprc = round(aggro_askprc - tick, 2)
        place_bid = not place_bid
        num_aggro -= 1

    return orders_to_dataframe(orders)


def generate_extra_orders(params, product, secid, num_extra, start_bid, start_ask):
    futures_params = params['contracts'][product]
    specs = futures_params['specifications']

    qty = specs['qty']
    qty_dev = specs['randomQtyDeviation']
    tick = specs['tick']

    bidprc = round(start_bid - tick, 2)
    askprc = round(start_ask + tick, 2)

    orders = []

    place_bid = True
    while num_extra > 0:
        if place_bid:
            orders.append(['bid', secid, product, 0, bidprc, random_qty(qty, qty_dev), -1, 0, False])
            bidprc = round(bidprc - tick, 2)
        else:
            orders.append(['offer', secid, product, 0, askprc, random_qty(qty, qty_dev), -1, 0, False])
            askprc = round(askprc + tick, 2)
        place_bid = not place_bid
        num_extra -= 1

    return orders_to_dataframe(orders)


def generate_kospi_options_orders(params, vols):
    # generate options orders
    options_params = params['contracts']['KOO']
    options_specs = options_params['specifications']
    call_specs = options_specs['calls']
    put_specs = options_specs['puts']

    absolute_min_delta = options_specs['absoluteMinDelta']
    absolute_max_delta = options_specs['absoluteMaxDelta']

    vols = vols.loc[vols['abs_delta'].between(absolute_min_delta, absolute_max_delta)]

    # separate calls and puts
    puts = vols.loc[vols['op'] < 0]
    calls = vols.loc[vols['op'] > 0]

    # initialize options order creator
    call_options_creator_params = {
        'bidqty_map': call_specs['deltaBidQtyMap'],
        'askqty_map': call_specs['deltaAskQtyMap'],
        'underlying_performance': call_specs['assumedUnderlyingPerformance'],
        'max_aggro_orders': call_specs['numberOfAggressiveOrders'],
        'qty_multiplier': call_specs['qtyMultiplier']
    }
    put_options_creator_params = {
        'bidqty_map': put_specs['deltaBidQtyMap'],
        'askqty_map': put_specs['deltaAskQtyMap'],
        'underlying_performance': put_specs['assumedUnderlyingPerformance'],
        'max_aggro_orders': put_specs['numberOfAggressiveOrders'],
        'qty_multiplier': put_specs['qtyMultiplier']
    }
    dte = vols['dte'].iloc[0]
    call_order_creator = OptionsOrderCreator(dte, **call_options_creator_params)
    put_order_creator = OptionsOrderCreator(dte, **put_options_creator_params)

    # set up call and put selectors
    lowest_order_price = options_specs['lowestOrderPrice']
    call_batches_specs = call_specs['competitiveBatches']
    put_batches_specs = put_specs['competitiveBatches']

    how_many_calls = call_batches_specs['numberOfContracts']
    call_upper_delta = call_batches_specs['maxDelta']
    call_selector = OptionSelector(calls, 'call', how_many=how_many_calls, upper_delta=call_upper_delta,
                                   low_tv=lowest_order_price)

    how_many_puts = put_batches_specs['numberOfContracts']
    put_upper_delta = put_batches_specs['maxDelta']
    put_selector = OptionSelector(puts, 'put', how_many=how_many_puts, upper_delta=put_upper_delta,
                                  low_tv=lowest_order_price)

    # fill lists with orders generated by order_creator
    bid_orders, ask_orders = [], []
    bid_orders_map, ask_orders_map = defaultdict(set), defaultdict(set)
    batch_number = 1

    # alternate between put and call batches
    selector = call_selector
    selected = selector.next_batch()
    order_creator = call_order_creator
    seen_lowest_put, seen_lowest_call = False, False
    while selected is not None:
        if selector == call_selector and seen_lowest_call:
            selector = put_selector
            selected = selector.next_batch()
            order_creator = put_order_creator
            continue
        elif selector == put_selector and seen_lowest_put:
            selector = call_selector
            selected = selector.next_batch()
            order_creator = call_order_creator
            continue
        print('batch:')
        print(selected.to_string())

        # generate orders then extend current list of orders
        new_bid_orders, new_ask_orders = order_creator.generate_orders(selected, batch_number, bid_orders_map,
                                                                       ask_orders_map)
        bid_orders.extend(new_bid_orders)
        ask_orders.extend(new_ask_orders)

        # fetch next batch
        if selector == call_selector:
            selector = put_selector
            order_creator = put_order_creator
        else:
            selector = call_selector
            order_creator = call_order_creator
        selected = selector.next_batch()
        batch_number += 1

    # now go through calls
    if selector != call_selector:
        print('last selector was put_selector! fetch next batch from call_selector')
        selected = call_selector.next_batch()
        order_creator = call_order_creator
    while selected is not None:
        if seen_lowest_call:
            break
        print('remaining call batch:')
        print(selected.to_string())

        # generate orders then extend current list of orders
        new_bid_orders, new_ask_orders = order_creator.generate_orders(selected, batch_number, bid_orders_map,
                                                                       ask_orders_map)
        bid_orders.extend(new_bid_orders)
        ask_orders.extend(new_ask_orders)

        # fetch next batch
        selected = call_selector.next_batch()
        batch_number += 1

    # now go through puts
    if selector != put_selector:
        print('last selector was call_selector! fetch next batch from put_selector')
        selected = put_selector.next_batch()
        order_creator = put_order_creator
    while selected is not None:
        if seen_lowest_put:
            break
        print('remaining put batch:')
        print(selected.to_string())

        # generate orders then extend current list of orders
        new_bid_orders, new_ask_orders = order_creator.generate_orders(selected, batch_number, bid_orders_map,
                                                                       ask_orders_map)
        bid_orders.extend(new_bid_orders)
        ask_orders.extend(new_ask_orders)

        # fetch next batch
        selected = put_selector.next_batch()
        batch_number += 1

    bid_orders = orders_to_dataframe(bid_orders)
    ask_orders = orders_to_dataframe(ask_orders)

    # fix bid orders
    call_orders = bid_orders.loc[bid_orders['op'] > 0]
    first_calls = call_orders.drop_duplicates('secid', keep='first')
    max_calls = call_orders.groupby('secid')['price'].max().loc[first_calls['secid'].values]

    put_orders = bid_orders.loc[bid_orders['op'] < 0]
    first_puts = put_orders.drop_duplicates('secid', keep='first')
    max_puts = put_orders.groupby('secid')['price'].max().loc[first_puts['secid'].values]

    max_prices = find_max_allowed_bid_prices(max_calls)
    max_prices.update(find_max_allowed_bid_prices(max_puts))

    bid_orders['allowed'] = bid_orders['price'].round(2) <= bid_orders['secid'].map(max_prices).round(2)

    bid_orders = calc_redundant_orders(bid_orders, options_specs)
    ask_orders = calc_redundant_orders(ask_orders, options_specs)

    print('competitive KOO orders computed')

    # now handle remaining orders
    max_call_delta = vols.loc[vols['secid'].isin(first_calls['secid']), 'abs_delta'].max()
    if np.isnan(max_call_delta):
        max_call_delta = absolute_min_delta
    max_put_delta = vols.loc[vols['secid'].isin(first_puts['secid']), 'abs_delta'].max()
    if np.isnan(max_put_delta):
        max_put_delta = absolute_min_delta

    valid_calls = (vols['op'] > 0) & (vols['abs_delta'] > max_call_delta)
    valid_puts = (vols['op'] < 0) & (vols['abs_delta'] > max_put_delta)

    remaining_calls = vols.loc[valid_calls]
    remaining_puts = vols.loc[valid_puts]

    print('remaining calls')
    print(remaining_calls.to_string())
    print('remaining puts')
    print(remaining_puts.to_string())

    # remaining calls
    remaining_call_bid_orders, remaining_call_ask_orders = call_order_creator.generate_orders(remaining_calls)
    remaining_call_bid_orders = orders_to_dataframe(remaining_call_bid_orders)
    remaining_call_ask_orders = orders_to_dataframe(remaining_call_ask_orders)

    # remaining puts
    remaining_put_bid_orders, remaining_put_ask_orders = put_order_creator.generate_orders(remaining_puts)
    remaining_put_bid_orders = orders_to_dataframe(remaining_put_bid_orders)
    remaining_put_ask_orders = orders_to_dataframe(remaining_put_ask_orders)

    remaining_bid_orders = remaining_call_bid_orders.append(remaining_put_bid_orders)
    remaining_bid_orders['idx'] = np.arange(len(remaining_bid_orders))
    remaining_bid_orders = remaining_bid_orders.sort_values(['delta', 'idx'])
    del remaining_bid_orders['idx']

    remaining_ask_orders = remaining_call_ask_orders.append(remaining_put_ask_orders)
    remaining_ask_orders['idx'] = np.arange(len(remaining_ask_orders))
    remaining_ask_orders = remaining_ask_orders.sort_values(['delta', 'idx'])
    del remaining_ask_orders['idx']

    # handle bids
    all_bid_orders = bid_orders.append(remaining_bid_orders).reset_index(drop=True)
    all_bid_orders = all_bid_orders.iloc[:options_params['numberOfBids']]

    # fancy striping
    striped_orders = []
    for batch in all_bid_orders['batch'].unique():
        batch_orders = all_bid_orders.loc[all_bid_orders['batch'] == batch]
        striped_orders.append(stripe_option_orders(batch_orders, params['numberOfPids']))
    if not striped_orders:
        all_bid_orders = pd.DataFrame()
    else:
        all_bid_orders = pd.DataFrame().append(striped_orders)

    # handle asks
    all_ask_orders = ask_orders.append(remaining_ask_orders).reset_index(drop=True)
    all_ask_orders = all_ask_orders.iloc[:options_params['numberOfAsks']]

    # more fancy striping
    striped_orders = []
    for batch in all_ask_orders['batch'].unique():
        batch_orders = all_ask_orders.loc[all_ask_orders['batch'] == batch]
        striped_orders.append(stripe_option_orders(batch_orders, params['numberOfPids']))
    if not striped_orders:
        all_ask_orders = pd.DataFrame()
    else:
        all_ask_orders = pd.DataFrame().append(striped_orders)

    return all_bid_orders.append(all_ask_orders).reset_index(drop=True)


def check_config(param):
    print('checking config')
    max_orders = param['maxOrders']
    remaining = max_orders
    give_remaining_to = None
    if len(param['contracts']) == 0:
        raise ValueError('no contracts configured. nothing to do')
    elif len(param['contracts']) == 1:
        return #give_remaining_to = param['contracts'].keys()[0], 'numberOfBids'
    else:
        for product, product_param in param['contracts'].items():
            num_bids = product_param['numberOfBids']
            if num_bids > 0:
                remaining -= num_bids
            elif give_remaining_to is not None:
                raise ValueError('parameters have more than one number of orders configured to -1. please fix')
            else:
                give_remaining_to = (product, 'numberOfBids')
            num_asks = product_param['numberOfAsks']
            if num_asks > 0:
                remaining -= num_asks
            elif give_remaining_to is not None:
                raise ValueError('parameters have more than one number of orders configured to -1. please fix')
            else:
                give_remaining_to = (product, 'numberOfAsks')
            print('product=%s, num_bids=%d, num_asks=%d' % (product, num_bids, num_asks))
        print('adjusting product=%s, side=%s to %d' % (give_remaining_to + (max(0, remaining),)))
    if 'KOO' in param['contracts']:
        redundancy = param['contracts']['KOO']['specifications']['competitiveRedundancy']
        for side, specs in redundancy.items():
            if specs is None:
                continue
            specs['delta_map'] = dict(specs['deltaToNumberOfLevelsMap'])
    product, side = give_remaining_to
    param['contracts'][product][side] = remaining


def stripe_option_orders(options_orders, max_orders):
    secids = options_orders['secid'].unique()
    by_secid = defaultdict(deque)
    secid_counts = defaultdict(int)
    for secid in options_orders['secid'].unique():
        sec_orders = options_orders.loc[options_orders['secid'] == secid]
        for i, row in sec_orders.iterrows():
            by_secid[secid].append(row.tolist())
    # begins striping
    idx = 0
    last_secid = None
    next_secid = secids[idx]
    new_orders = []
    while by_secid:
        if next_secid not in by_secid:
            idx += 1
        else:
            if last_secid != next_secid:
                order_count = 1
                secid_counts[next_secid] = 1
            q = by_secid[next_secid]
            order = q.popleft()
            new_orders.append(order)
            if not q:
                del by_secid[next_secid]
                del secid_counts[next_secid]
                idx += 1
            elif q[0][4] != order[4]:
                if order_count > 1:
                    idx += 1
                elif secid_counts[next_secid] >= max_orders:
                    idx += 1
            else:
                order_count += 1
            secid_counts[next_secid] += 1
        last_secid = next_secid
        next_secid = secids[idx % len(secids)]
    return pd.DataFrame(new_orders, columns=options_orders.columns)


if __name__ == '__main__':
    cfg_filename, tdate, sprinter_filename, ko_futures, ktb3_futures = sys.argv[1:]
    with open(cfg_filename, 'r') as f:
        params = json.load(f)
    check_config(params)
    num_pids = params['numberOfPids']

    cfg_filename, tdate, sprinter_filename, ko_futures, ktb3_futures = sys.argv[1:]
    ko_futures = round(float(ko_futures), 3)
    ktb3_futures = round(float(ktb3_futures), 3)

    # query vols
    vols = query_front_vols(tdate, futures=ko_futures)
    vols.head()

    orders = {}
    # options
    if 'KOO' in params['contracts']:
        orders['KOO'] = generate_kospi_options_orders(params, vols)

    # futures
    front_futures_map = find_front_futures(tdate, params)

    if 'KO' in params['contracts']:
        orders['KO'] = generate_futures_orders(params, 'KO', front_futures_map['KO'], ko_futures)
    if 'KTB3' in params['contracts']:
        orders['KTB3'] = generate_futures_orders(params, 'KTB3', front_futures_map['KTB3'], ktb3_futures)

    order_columns = ['secid', 'side', 'price', 'qty']
    sprinter_type = params['sprinterType']
    user = params['sprinterUser']
    order_counts = defaultdict(int)

    total_orders_placed = 0
    with open(sprinter_filename, 'w') as f:
        for product, num_orders in params['ordering']:
            last_count = order_counts[product]
            if num_orders == -1:
                product_orders = orders[product].iloc[last_count:]
            else:
                product_orders = orders[product].iloc[last_count:last_count+num_orders]
            num_orders = len(product_orders)
            product_orders['qty'] -= (np.arange(len(product_orders)) + total_orders_placed) % num_pids
            f.write('### sprinting %d orders for %s\n' % (num_orders, product))
            if not len(product_orders):
                continue
            for i, (secid, side, price, qty) in enumerate(zip(*(product_orders[col] for col in order_columns))):
                idx = (total_orders_placed + i) % num_pids
                order = '%s,%s,%0.2f,%d,%s,%s,%d\n' % (secid, side, price, qty, sprinter_type, user, (qty/2)-idx)
                f.write(order)
            order_counts[product] += num_orders
            total_orders_placed += num_orders
            f.write('### so far sprinted %d total orders for %s\n' % (order_counts[product], product))
            f.write('###\n')

        # do we have extras?
        if total_orders_placed < params['maxOrders']:
            extra_product = params['extraOrders']['product']
            product_params = params['contracts'][extra_product]
            if 'extraEligible' not in product_params or not product_params['extraEligible']:
                print('%s not eligible for extra orders. check your config')
            else:
                print('only placed %d orders. extra orders go to: %s' % (total_orders_placed, extra_product))
                product_orders = orders[extra_product]
                start_bid = product_orders.loc[product_orders['side']=='bid', 'price'].min()
                start_ask = product_orders.loc[product_orders['side']=='offer', 'price'].max()
                num_extra = params['maxOrders'] - total_orders_placed
                print(start_bid, start_ask, 'num_extra', num_extra)
                extra_orders = generate_extra_orders(
                    params, extra_product, front_futures_map[extra_product], num_extra, start_bid, start_ask
                )
                extra_orders['qty'] -= (np.arange(len(extra_orders)) + total_orders_placed) % num_pids
                f.write('### extra %d orders for %s!\n' % (num_extra, extra_product))
                for i, (secid, side, price, qty) in enumerate(zip(*(extra_orders[col] for col in order_columns))):
                    idx = (total_orders_placed + i) % num_pids
                    order = '%s,%s,%0.2f,%d,%s,%s,%d\n' % (secid, side, price, qty, sprinter_type, user, (qty/2)-idx)
                    f.write(order)
                f.write('###\n')


    print(sprinter_filename, 'written')
    print('done')
