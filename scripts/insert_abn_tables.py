import zipfile
import xml.etree.ElementTree as ET
import sys
import numpy as np
import pandas as pd
import glob
from pandas.io import sql
from sqlalchemy import create_engine, MetaData, Table, select
import sys
import subprocess
import datetime as dt
import glt.db
import os

conn_defn = {
    'host': '10.125.0.200',
    'user': 'gltloader',
    'pw': 'gltloader',
    'db': 'pnl'
}


def findProductSymbol(node):
        symbols = node.findall('./Product/Symbol')
        if symbols:
            return symbols[0].text
        else:
            return ''

#parses xml file for netliq values
def Net_Liquidation(x):
    with zipfile.PyZipFile(x, 'r') as zf:
        xml_txt = None
        for obj in zf.filelist:
            with zf.open(obj.filename) as f:
                xml_txt = f.read()
            if xml_txt is not None:
                break
    root = ET.fromstring(xml_txt)
   
    date = np.array([pd.to_datetime(x.text) for x in root.findall("./AccountPositions/CashPosition/ProcessingDate")])
    descriptions = np.array([x.text for x in root.findall("./AccountPositions/CashPosition/CashAmountDescription")])
    sides = np.array([1 if x.text == 'C' else -1 for x in root.findall("./AccountPositions/CashPosition/CashPositionNew/ValueDC")])
    values = np.array([float(x.text) for x in root.findall("./AccountPositions/CashPosition/CashPositionNew/Value")])
    Values = values * sides
    Currency = np.array([x.text for x in root.findall("./AccountPositions/CashPosition/CashPositionNew/ValueCur")])
    Product = [findProductSymbol(x) for x in root.findall("./AccountPositions/CashPosition")]
    
    PNL = list(zip(date, descriptions, Values, Currency, Product))
    pnl = pd.DataFrame(PNL)
    pnl.columns = ['date', 'description', 'value', 'currency', 'product']
    pnl['seqnum'] = pnl.groupby(['description', 'product']).cumcount()
    pnl['account_id'] = np.nan
    pnl['account_id'] = pnl['account_id'].replace([None], [""])
    pnl = pnl.loc[:, ['date', 'account_id', 'description', 'value', 'currency', 'product', 'seqnum']]
    
    return pnl

    
def findPosition(node):
    Long = node.findall('./QuantityLong')
    Short = node.findall('./QuantityShort')
    if Long:
        return float(Long[0].text)
    else:
        return -1 * float(Short[0].text)

#parses xml for position values
def Position(x):
    with zipfile.PyZipFile(x, 'r') as zf:
        xml_txt = None
        for obj in zf.filelist:
            with zf.open(obj.filename) as f:
                xml_txt = f.read()
            if xml_txt is not None:
                break
    root = ET.fromstring(xml_txt)
    date = np.array([pd.to_datetime(x.text) for x in root.findall("./AccountPositions/FuturePosition/ProcessingDate")])
    Product = np.array([x.text for x in root.findall("./AccountPositions/FuturePosition/Product/Symbol")])
    Position = np.array([findPosition(x) for x in root.findall("./AccountPositions/FuturePosition")])
    Settle = np.array([float(x.text) for x in root.findall("./AccountPositions/FuturePosition/Valuationprice")])
    TradingUnit = np.array([float(x.text) for x in root.findall("./AccountPositions/FuturePosition/Tradingunit")])
    Expiration = np.array([pd.to_datetime(x.text) for x in root.findall("./AccountPositions/FuturePosition/Product/Expiry")])
     
    Pos = list(zip(date, Product, Position, Settle, TradingUnit, Expiration))
    POS = pd.DataFrame(Pos)

    if not len(POS):
        return pd.DataFrame()

    POS.columns = ['date', 'product', 'position', 'settle', 'tradingUnit', 'expiry']
    
    POS['account_id'] = np.nan
    POS['account_id'] = POS['account_id'].replace([None], [""])
    POS = POS.loc[:, ['date', 'account_id', 'product', 'position', 'settle', 'tradingUnit', 'expiry']] 
    expiry = POS['expiry']
    POS['expiry_year'] = expiry.apply(lambda x: x.year)
    POS['expiry_month'] = expiry.apply(lambda x: x.month)
    
    key_cols = ['product', 'expiry_year', 'expiry_month']
    theo_key_cols = ['theoproduct', 'expiry_year', 'expiry_month']

    Price_map = {(a,b,c): d for a, b, c, d in POS[key_cols + ['settle']].values}

    product_map = {
        'NK': 'NK225F',
        'JB': 'JGBLF'
    }
    
    theo_map = {
        'NK': 'JPY',
        'JB': 'JPY'
    }

    POS['theoproduct'] = (POS['product'].apply(product_map.get)).replace([None], [""], regex = True)
    POS['theo1'] = list(map(Price_map.get, list(map(tuple, POS[theo_key_cols].values))))
    POS['theo'] = POS['theo1'].fillna(POS['settle'])
    POS['currency'] = (POS['product'].apply(theo_map.get)).replace([None], [""], regex = True)
    
    del POS['expiry_year']
    del POS['expiry_month']
    del POS['theo1']

 
    return POS


datestr = sys.argv[1]
filenames = glob.glob('/glt/storage/abn/*%s**-1812-C1812-POSL-*'%datestr)
filenames.sort()

# net liq stuff
#try:
#    conn = glt.db.conn(**conn_defn)
#    date = conn.frame_query("select max(date) from abn_netliq")
    
#finally:
#    conn.close()

#datenew = date.values[0, 0].strftime('%Y%m%d')
#datenew = pd.to_datetime(str(date.values[0, 0])).strftime('%Y%m%d')
#dates = map(lambda x: os.path.basename(x)[:8], filenames)
#newdat = filter(lambda x: x >= datenew, dates)
#filename = filter(lambda x: any(d in x for d in newdat), filenames)
print(filenames)
data = []
for filename1 in filenames:
    dat = Net_Liquidation(filename1)
    data.append(dat)
    
if len(data):
    cash = pd.DataFrame().append(data)
    cash_columns_str = str(tuple(cash.columns)).replace('\'', '')

    print(cash_columns_str)
    try:
        conn = glt.db.conn(**conn_defn)
        for i, row in cash.iterrows():
            (date, account_id, description, value, currency, product, seqnum) = row.values
            output = ('insert into abn_netliq %s values %s '
                    % (cash_columns_str, tuple(row.values),)
                     )
            output2 = ("on duplicate key update %s='%s', %s=%f, %s='%s'"
                    % ('account_id', account_id, 'value', value, 'currency', currency)
                     )
            #output = 'insert into abn_netliq %s' % (str(tuple(cash.columns)).replace('\'', ''), )
            #output2 = ' values%s' % (tuple(row.values), )
            
            OP = output + output2
            conn.execute(OP)
        print(conn.frame_query("select * from abn_netliq"))

    finally:
        conn.close()    

# position stuff
try:
    conn = glt.db.conn(**conn_defn)
    last_settle = conn.frame_query("select max(date) from abn_settlement")
    
finally:
    conn.close()
    
last_settle_date = pd.to_datetime(str(last_settle.values[0, 0])).strftime('%Y%m%d')
all_dates = [os.path.basename(x)[:8] for x in filenames]
files_since_last_settle = [x for x in all_dates if x > last_settle_date]
relevant_settle_filenames = [x for x in filenames if any(d in x for d in files_since_last_settle)]

data = []
for filename in relevant_settle_filenames:
    dat = Position(filename)
    if len(dat):
        data.append(dat)

if len(data):
    settlement = pd.DataFrame().append(data)
    settle_columns_str = str(tuple(settlement.columns)).replace('\'', '')
    try:
        conn = glt.db.conn(**conn_defn)
        for i, row in settlement.iterrows():
            (date, account_id, product, position, settle, tradingUnit, expiry, theoProduct, theo, currency) = row.values

            output = ('insert into abn_settlement %s values %s ' 
                    % (settle_columns_str, tuple(row.values),)
                     )
            output2 = ("on duplicate key update %s='%s', %s=%f, %s=%f, %s=%d, %s=%f, %s='%s'"
                    % ('account_id', account_id, 'position', position, 'settle', settle, 
                       'tradingUnit', tradingUnit, 'theo', theo, 'currency', currency)
                     )

            OP = output + output2
            conn.execute(OP)
        print(conn.frame_query("select * from abn_settlement"))

    finally:
            conn.close()
else:
    print("no new settlement data to add")


#    print datestr

