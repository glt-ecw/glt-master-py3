import sys
import os
import json
import glt.db
import pandas as pd


# e.g.: python save_settlement_prices.py 20161115 DEIO_W


def read_config(json_filename, instance):
    cfg, param = {}, {}
    with open(json_filename, 'r') as f:
         cfg = json.load(f)
    if instance not in cfg:
        raise KeyError('%s does not exist in save_positions.py config')
    cfg = cfg[instance]
    param['user_settles'] = cfg['universalReadWriteConnectionUser']
    param['user_md'] = cfg['mdConnectionUser']
    param['settles'] = cfg['settlesTable']
    param['uat'] = cfg['uatInstance']
    return param


def show_databases_like(conn, instance):
    cmd = """
        SHOW databases like '%s'
    """ % instance
    return conn.frame_query(cmd)


def query_settles(conn, instance):
    cmd = """
        SELECT securityid as secid, lasttradepx as prc
        FROM %s.MD
    """ % instance
    return conn.frame_query(cmd)


def insert_data(conn, table, date, row, real_insert=False, show_cmd=True):
    cmd = """
        INSERT INTO %s
        (tradedate, securityid, price)
        VALUES('%s', %d, %0.4f)
        ON DUPLICATE KEY UPDATE
            tradedate='%s',
            securityid=%d,
            price=%0.4f
    """ % (table, date, row[0], row[1], date, row[0], row[1])
    if show_cmd:
        print(cmd)
    if real_insert:
        conn.execute(cmd)


if __name__ == '__main__':
    cfg_path = os.path.join(os.environ['GLT_MASTER_PATH'], 'cfg')
    mysql_cfg_filename = os.path.join(cfg_path, glt.db.CONFIG_BASENAME)
    config = glt.db.read_mysql_config(mysql_cfg_filename)

    tdate, prod = sys.argv[1:]

    script_cfg_filename = os.path.join(cfg_path, 'deio_eod.json')
    param = read_config(script_cfg_filename, prod)
    user_settles = param['user_settles']
    user_md = param['user_md']
    uat = param['uat']
    settles = param['settles']

    today = pd.to_datetime(tdate).date()

    for user in [user_settles, user_md]:
        if user not in config:
            print('%s does not exist in %s' % (user, mysql_cfg_filename))
            exit(1)

    # establish connection
    conn_settles, conn_md = None, None
    try:
        conn_settles = glt.db.MysqlConnection(**config[user_settles])
        conn_md = glt.db.MysqlConnection(**config[user_md])

        prices = query_settles(conn_md, prod)

        # check if uat exists
        if uat is not None:
            uat_dbs = show_databases_like(conn_md, uat)
            if len(uat_dbs):
                uat_prices = query_settles(conn_md, uat)
                uat_prices = uat_prices.loc[~uat_prices['secid'].isin(prices['secid'].values)]
                prices = prices.append(uat_prices)

        prices = prices.fillna(0)

        print(prices.to_string())

        for row in zip(*(prices[col].values for col in prices)):
            insert_data(conn_settles, settles, today, row, real_insert=True)
        conn_settles.commit()

    finally:
        if conn_settles is not None:
            print('closing connection')
            conn_settles.close()
        else:
            print('connection non-existent')
        if conn_md is not None:
            print('closing connection')
            conn_md.close()
        else:
            print('connection non-existent')