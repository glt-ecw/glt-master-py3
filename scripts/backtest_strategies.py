import sys
import json

from glt.backtesting.simulator import Simulator, now
from traceback import print_exc


if __name__ == '__main__':
    exchange, config_filename = sys.argv[1:]

    try:
        print(now(), 'reading config at', config_filename)
        with open(config_filename, 'r') as f:
            cfg = json.load(f)
            print(json.dumps(cfg, indent=4, separators=(',', ': ')), '\n')
    except Exception as e:
        print(now(), 'ERROR in reading %s' % config_filename)
        print(now(), 'error message: %s' % e)
        print_exc()
        print(now(), 'quitting')
        exit(1)

    simulator = Simulator(cfg['matchingEngines'])

    strategies_config = cfg['strategies']
    if not strategies_config:
        print(now(), 'no strategies to run. quitting')
        exit(1)

    for strategy_config in strategies_config:
        strat_object_name = strategy_config['objectName']
        param = strategy_config['parameters']
        exec('from glt.backtesting import %s' % strat_object_name)
        for me_id in strategy_config['useMatchingEngineIDs']:
            simulator.init_strategy(globals()[strat_object_name], param, me_id)

    # start reading file for backtesting
    print(now(), 'now reading market data from stdin')
    simulator.ready = True
    # text from sys.stdin will be unicode... we need bytes
    num_lines = simulator.process_from(iter(sys.stdin), exchange)

    print(now(), 'done. lines read:   %d' % num_lines)
