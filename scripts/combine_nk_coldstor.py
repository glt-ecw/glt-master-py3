
import os
import re
import sys
import glob
import gzip
import numpy as np
import pandas as pd
import datetime as dt
import pandas.tseries.offsets as toffsets
#import bcolz
import subprocess

import glt.md.omx as omx
from glt import tools, manuscraper as nkm, manuscraper_beta as nkmb
from glt import md as md
"""
SEC_CONN = {
    'host': '10.1.31.200',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'SECURITIES'
}
"""
SEC_CONN = {
    'host': '10.125.0.200',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'SECURITIES'
}


from glt import db as db

def find_secid_bydate(date):
    ## find front month secid
    try:
        conn = db.conn(**SEC_CONN)
        # nk
        nk_secids = conn.frame_query(
            ("""
                SELECT id FROM SECURITIES.futures
                where id in (SELECT id FROM SECURITIES.securities where contractDefId=124) and expDate > '%s' limit 1;
                """) %(date)
        )

        # mnk
        mnk_secids = conn.frame_query(
            ("""
                SELECT id FROM SECURITIES.futures
                where id in (SELECT id FROM SECURITIES.securities where contractDefId=126) and expDate > '%s' limit 1;
                """) %(date)
        )

        # tp
        tp_secids = conn.frame_query(
            ("""
                SELECT id FROM SECURITIES.futures
                where id in (SELECT id FROM SECURITIES.securities where contractDefId=128) and expDate > '%s' limit 1;
                """) %(date)
        )   
     
    finally:
        conn.close()
    
    return nk_secids.id.values[0], mnk_secids.id.values[0], tp_secids.id.values[0]


#datestr = argv[1]

#print('numpy', np.version.version)
#print('pandas', pd.__version__)
#print('bcolz', bcolz.version.__version__)

#comb_dir = '/glt/ssd/manuscraper/md/comb'
#comb_csv = os.path.join(comb_dir, datestr + '.csv')
#comb_h5 = os.path.join(comb_dir, datestr + '.h5')
BD1_MULT = 1000000000

def combine_nk(datestr, streaker_ms, gains, min_devs, weakturnqty=1):
    # ONLY AGGREGATES EVEN PRICES FOR BIGS AND MINIS!!!!!!!!


    # starting here

    nk_secid, mnk_secid, tp_secid = find_secid_bydate(datestr)

    data = {}
    #jpa_fname='/glt/storage/txt/parsed_pcap/md/nk_books.%s.csv.gz'%datestr
    jpa_fname='/glt/coldstor/csv/jpa.%s.both.books.csv.gz'%datestr

    if os.path.isfile(jpa_fname):
        nk_books = md.standard.read_jpa_csv(jpa_fname, use_int_prices=True, buysell=True, time_adjust='9h', open_only=True)
        nk_books['timestamp'] = pd.to_datetime(nk_books['time']).astype(int)
        #nk_books=pd.read_csv(jpa_fname, engine='c', compression='gzip')
        ## need better way to identify Nk and MNK secids
        for book, secid in zip(['nk_book','mnk_book'], [nk_secid, mnk_secid]):
            jpx_data=[]
            jpx_data = nk_books.loc[nk_books.secid==secid]
            #print(jpx_data.shape)
            if not len(jpx_data):
                print(datestr, 'has no jpx data. moving on.')
                return None
            else:

                ## mimic how deio consolidates and publishes md
                jpx_data['eop_int'] = jpx_data['eop'].astype(int)
                output = tools.deio_pub_reconciliation(jpx_data)
    
                new_cols = ('keep_bool', 'buyvolume', 'buyprice', 'sellvolume', 'sellprice') 
                for i, col in enumerate(new_cols):

                    if col in jpx_data.columns:
                        del jpx_data[col]

                    jpx_data[col] = output[:, i]

                
                for col in ('eop_int',):
                    if col in jpx_data.columns:
                        del jpx_data[col]

                data[book]=jpx_data.loc[jpx_data['keep_bool']==1]

            del jpx_data

        del nk_books
    else:
        print(datestr, 'No jpx md file.\n')
        return None

    def gen_mktcols(mktcol):
        return ['%s%d' % (mktcol, j) for j in range(my_depth)]

    ##############################################
    ### INPUTS
    """
    weakturnqty = int(argv[2])
    streaker_ms = int(argv[3])
    min_dev = int(argv[4])
    gain = int(argv[5])
    turnqty = int(argv[6])
    turntick = int(argv[7])
    accqty_threshold = int(argv[8])
    time_to_turn = int(argv[9])
    minvolume = int(argv[10])
    """
    ##############################################

    trade_cols = 'buyprice', 'buyvolume', 'sellprice', 'sellvolume'
    sides = 'bid', 'ask'
    pqps = 'prc', 'qty', 'par'
    nodupe_cols = tuple('%s%s%d' % (ba, pq, i) for ba in sides for i in range(10) for pq in pqps) + trade_cols

    start_time = pd.to_datetime(datestr + 'T090005.000000')
    end_time = pd.to_datetime(datestr + 'T150955.000000')

    symbol_info = [
        ('nk_book', { 'sym_int': 1, 'size_scale': 10, 'price_divisor': 10000, 'weakturnqty': weakturnqty, 'mintick': 10 }),
        ('mnk_book', { 'sym_int': 2, 'size_scale': 1, 'price_divisor': 10000, 'weakturnqty': weakturnqty*10, 'mintick': 5 }),
        #('snk_book', { 'sym_int': 3, 'size_scale': 5, 'price_divisor': 100, 'weakturnqty': weakturnqty*2, 'mintick': 5 })
    ]
    
    #################################
    ### Load NK books
    STOP_REASON = 12

    last_idx = 0
    latest_ma42_timestamp = None
    data_cols = None
    nodupe=None
    weak_nkdf_turns=None

    for key, defs in symbol_info:
        mintick = defs['mintick']
        weakturnqty = defs['weakturnqty']
        size_scale = defs['size_scale']
        sym_int = defs['sym_int']
        price_divisor = defs['price_divisor']

        dat = data[key]

        if key == 'snk_book':
            dat['time'] += sgx_time_adjust
            dat['timestamp'] += sgx_time_adjust.value
            
        if key == 'mnk_book':
            dat['triggerid'] = 50000000000 + dat['seqnum']
            
        elif key == 'nk_book':
            dat['triggerid'] = 40000000000 + dat['seqnum']
        
        #dat = dat.loc[dat.bo2reason!=STOP_REASON]
        dat = dat.loc[dat['time'].between(start_time, end_time)]

        dat['sym_int'] = sym_int
        dat['myseqnum'] = last_idx + np.arange(dat.shape[0], dtype=np.int64)
        last_idx += dat.shape[0]

        ## check for shitty books
        check_stime = pd.to_datetime(datestr + 'T091000.000000')
        #if latest_ma42_timestamp:
        #    check_stime = check_stime if check_stime > pd.to_datetime(latest_ma42_timestamp) else pd.to_datetime(latest_ma42_timestamp)
                    
        check_etime = pd.to_datetime(datestr + 'T150055.000000')
        dat['timediffs'] = dat['timestamp'].diff() / 1.0e9
        
        if (np.any(dat.timediffs.values > 3 * 60)) or (pd.to_datetime(dat.time.values[0]) > check_stime or pd.to_datetime(dat.time.values[-1]) < check_etime):
            print('bad books found for %s\n'%datestr)
            return None
        else:
            del dat['timediffs']

        if price_divisor != 1:
            price_col = [col for col in dat.columns if any(p in col for p in ('prc', 'price'))]
            dat.loc[:, price_col] = dat.loc[:, price_col].values/price_divisor

        if size_scale != 1:
            qty_col = [col for col in dat.columns if any(q in col for q in ('qty', 'volume'))]
            dat.loc[:, qty_col] = dat.loc[:, qty_col].values * size_scale

        nodupe = tools.df_no_repeat(dat, nodupe_cols)

        weak_nkdf_turns = tools.find_turns(nodupe, weakturnqty, mintick)
        weak_nkdf_turns.columns = ['sym_'+x for x in weak_nkdf_turns.columns]
        
        nodupe = nodupe.join(weak_nkdf_turns)
        
        #even_bidprc0 = (10*np.floor(0.1*nodupe['bidprc0'].values)).round().astype(np.int64)
        #even_askprc0 = (10*np.ceil(0.1*nodupe['askprc0'].values)).round().astype(np.int64)

        #nodupe['even_bidprc0'] = even_bidprc0
        #nodupe['even_askprc0'] = even_askprc0
        #nodupe['even_bidqty0'] = nodupe['bidqty0']
        #nodupe['even_askqty0'] = nodupe['askqty0']
        #nodupe.loc[nodupe['even_bidprc0']==nodupe['bidprc1'], 'even_bidqty0'] += nodupe['bidqty1']
        #nodupe.loc[nodupe['even_askprc0']==nodupe['askprc1'], 'even_askqty0'] += nodupe['askqty1']
        
        # calc book imbalance for NK, MNK

        msg_bool = nodupe['time'] != nodupe['time'].shift(-1)
        buylevelclear_bool = np.logical_and(nodupe['buyvolume'] > 0, nodupe['buyprice'] != nodupe['askprc0'])
        selllevelclear_bool = np.logical_and(nodupe['sellvolume'] > 0, nodupe['sellprice'] != nodupe['bidprc0']) 

        nodupe['uniq_msg'] = np.nan
        nodupe.loc[msg_bool, 'uniq_msg'] = 1
        nodupe['imbal'] = 1.0 * nodupe['uniq_msg'] * (nodupe['bidqty0'] - nodupe['askqty0']) / (nodupe['bidqty0'] + nodupe['askqty0'])
        nodupe.loc[buylevelclear_bool, 'imbal'] = 1.0
        nodupe.loc[selllevelclear_bool, 'imbal'] = -1.0

        imbal_dat = nodupe.dropna(subset=['imbal'])
        imbals = np.roll(list(imbal_dat.imbal.values) + [0], 1)[:-1]
        imbal_ts = imbal_dat.time.values
        imbal_map = dict(list(zip(imbal_ts, imbals)))
        nodupe['prev_imbal'] = nodupe['time'].map(imbal_map)
        #print(nodupe.loc[:, ['time','sym_int','bidqty0','bidprc0','askqty0','askprc0','buyprice','buyvolume','sellprice','sellvolume','uniq_msg','imbal','prev_imbal']].head(2).to_string())
            
        data[key] = nodupe
       
        if data_cols is None:
            data_cols = nodupe.columns
        
        #print(key, sym_int, dat.shape[0], nodupe.shape, 'weakturnqty', weakturnqty, end=' ')
        #print('mintick', mintick, nodupe['sym_turn_direction'].unique())
        del dat, nodupe
        
    mkt = pd.DataFrame().append(list(data.values())).sort_values(by=['timestamp', 'myseqnum'])
    #if latest_ma42_timestamp is not None:
    #    mkt = mkt.loc[mkt['timestamp']>=latest_ma42_timestamp]
    mkt.index = mkt['myseqnum']
    mkt = mkt.loc[:, np.unique(data_cols)]
    
    print('got to beg of mkt')
    #print('total length:', mkt.shape[0])
    ## Add individual contract columns
    for sym_int in mkt.sym_int.unique():
        #print(sym_int)
        
        mkt['bidprc0_%d'%sym_int] = mkt.loc[mkt.sym_int == sym_int, 'bidprc0']
        mkt['bidqty0_%d'%sym_int] = mkt.loc[mkt.sym_int == sym_int, 'bidqty0']
        mkt['askprc0_%d'%sym_int] = mkt.loc[mkt.sym_int == sym_int, 'askprc0']
        mkt['askqty0_%d'%sym_int] = mkt.loc[mkt.sym_int == sym_int, 'askqty0']
        mkt['bidprc1_%d'%sym_int] = mkt.loc[mkt.sym_int == sym_int, 'bidprc1']
        mkt['bidqty1_%d'%sym_int] = mkt.loc[mkt.sym_int == sym_int, 'bidqty1']
        mkt['askprc1_%d'%sym_int] = mkt.loc[mkt.sym_int == sym_int, 'askprc1']
        mkt['askqty1_%d'%sym_int] = mkt.loc[mkt.sym_int == sym_int, 'askqty1']
        mkt['uniq_msg_%d'%sym_int] = mkt.loc[mkt.sym_int == sym_int, 'uniq_msg']
        mkt['imbal_%d'%sym_int] = mkt.loc[mkt.sym_int == sym_int, 'imbal']
        mkt['prev_imbal_%d'%sym_int] = mkt.loc[mkt.sym_int == sym_int, 'prev_imbal']
         
    prices = tuple(x for x in mkt.columns if 'prc' in x)
    qtys = tuple(x for x in mkt.columns if 'qty' in x)
    imbals = tuple(x for x in mkt.columns if 'imbal' in x)
    uniq_msgs = tuple(x for x in mkt.columns if 'uniq_msg' in x)

    mkt.loc[:, prices + qtys] = mkt.loc[:, prices + qtys].fillna(method='ffill')
    mkt.loc[:, imbals] = mkt.loc[:, imbals].fillna(method='ffill')
    
    #print(mkt.loc[:, ['time','sym_int','bidqty0_1','bidprc0_1','askqty0_1','askprc0_1','buyprice','buyvolume','sellprice','sellvolume','uniq_msg_1','uniq_msg_2','imbal_1','imbal_2','prev_imbal_1','prev_imbal_2']].head(50).to_string())

    st = dt.datetime.now()
    my_depth = 5

    st = dt.datetime.now()
    
    # ONLY AGGREGATES EVEN PRICES FOR BIGS/MINIS HERE
    agg = nkmb.aggregate_even_book(mkt, mkt.sym_int.unique(), my_depth=my_depth, tick_width=10, mkt_depth=5)

    #print(dt.datetime.now() - st)

    mktcols = 'bidprc', 'bidqty', 'askprc', 'askqty'
    agg_data = [pd.DataFrame(agg[:, :, i], index=mkt.index, columns=gen_mktcols(mktcols[i])) for i in range(4)]

    nkdf = None
    for dat in agg_data:
        if nkdf is None:
            nkdf = dat
        else:
            nkdf = nkdf.join(dat)

    del data, agg, agg_data, dat
    #for col in trade_cols:
    #    nkdf[col] = mkt[col]

    pqs = 'prc', 'qty'
    ba_pq_bycontract0 = tuple('%s%s0_%d' % (ba, pq, i) for ba in sides for i in range(1,3) for pq in pqs)
    ba_pq_bycontract1 = tuple('%s%s1_%d' % (ba, pq, i) for ba in sides for i in range(1,3) for pq in pqs)
    imbal_cols = tuple(['uniq_msg_1','uniq_msg_2','imbal_1', 'imbal_2', 'prev_imbal_1', 'prev_imbal_2'])
    par_cols = tuple('%spar%d' % (ba, i) for ba in sides for i in range(4))
    #ba_pq_bycontract0 = tuple('%s%s0_%d' % (ba, pq, i) for ba in sides for i in xrange(1,4) for pq in pqs)
    #ba_pq_bycontract1 = tuple('%s%s1_%d' % (ba, pq, i) for ba in sides for i in xrange(1,4) for pq in pqs)

    for col in ba_pq_bycontract0 + ba_pq_bycontract1 + trade_cols + par_cols + imbal_cols:
        nkdf[col] = mkt[col]

    nkdf_nodupe_cols = tuple('%s%s%d' % (ba, pq, i) for ba in sides for i in range(my_depth) for pq in pqps) + trade_cols
    nkdf_nodupe_cols += ba_pq_bycontract0 + ba_pq_bycontract1 + imbal_cols
    nkdf = tools.df_no_repeat(nkdf.loc[:, nkdf_nodupe_cols], nkdf.columns)

    insert_cols = 'triggerid', 'time', 'timestamp', 'sym_int', 'sym_turn_direction', 'sym_turn_price',
    insert_symcols = 'bidprc0', 'bidqty0', 'askprc0', 'askqty0', #'even_bidprc0', 'even_bidqty0', 'even_askprc0', 'even_askqty0'

    for i, col in enumerate(insert_cols):
        #print(i, col)
        nkdf.insert(i, col, mkt[col])
        
    for col in insert_symcols:
        newcol = 'sym_%s' % col
        nkdf[newcol] = mkt[col]
        #print(col, newcol)
    
    del mkt

    #print(datestr, dt.datetime.now(), 'finding unique ticks')
    nkdf['unique_tick'] = tools.unique_ticks(nkdf)
    nkdf = nkdf.loc[np.logical_and(nkdf['bidqty0']>0, nkdf['askqty0']>0)]

    # add prev_volume info

    pkt_bool = nkdf['time'] != nkdf['time'].shift(-1)
    pkt_df = nkdf.loc[pkt_bool==1]
    pkt_df['pktnum'] = np.arange(len(pkt_df))
    nkdf = nkdf.merge(pkt_df.loc[:, ['time','pktnum']], how='left', on='time')

    dat_shift = nkdf.groupby('time').sum()[['buyvolume','sellvolume']].reset_index().rename(columns={'buyvolume':'tot_buyvolume', 'sellvolume':'tot_sellvolume'})
    dat_shift['prev_buyvolume'] = dat_shift.tot_buyvolume.shift(1)
    dat_shift['prev_sellvolume'] = dat_shift.tot_sellvolume.shift(1)

    buyprices = nkdf.loc[nkdf.buyprice > 0, ['pktnum','buyprice']].drop_duplicates(['pktnum'])
    buyprices['prev_buyprice'] = buyprices['buyprice'] 
    buyprices['pktnum'] = buyprices.pktnum + 1

    sellprices = nkdf.loc[nkdf.sellprice > 0, ['pktnum','sellprice']].drop_duplicates(['pktnum'])
    sellprices['prev_sellprice'] = sellprices['sellprice'] 
    sellprices['pktnum'] = sellprices.pktnum + 1

    nkdf = nkdf.merge(dat_shift.loc[:, ['time','prev_buyvolume','prev_sellvolume','tot_buyvolume','tot_sellvolume']], how='left', on='time')
    nkdf = nkdf.merge(sellprices.loc[:, ['pktnum','prev_sellprice']], how='left', on='pktnum')
    nkdf = nkdf.merge(buyprices.loc[:, ['pktnum','prev_buyprice']], how='left', on='pktnum')
    
    del pkt_df
    del dat_shift
    del buyprices
    del sellprices
    print('got to end of mkt')

    #print(datestr, dt.datetime.now(), 'calculating streaks (sniper style)')
    streaker_us = (100, 250, 500, 1000, 2500, 5000, 10000, 1000000)
    #streaker_ms = (0.1, 0.2, 0.5, 1, 5, 25, 50, 105,)#(25, 105, 255)
    ignore_sym = 1
    ignore_qtys = (0, 200)
    ## test streaker window sizes in microseconds
    #streaker_windows = {us: long(us*1e3) for us in streaker_us} #(5e6, 25e6, 100e6)
    streaker_windows = {ms: int(ms*1e6) for ms in streaker_ms} #(5e6, 25e6, 100e6)
    reset_qtys = True

    for us, streaker_window in list(streaker_windows.items()):
        if us < 1.0:
            #label = "%dus" %(us)
            label = "%dus" %int(1000.0*us)
        else:
            label = "%dms" %(us)
        #print("Calculating streaks for %dus and streaker window %d" %(us, streaker_window))
        streaks_sniper, vwaps_sniper = nkm.sniper_streaker_sides(nkdf, streaker_window, 0)

        nkdf['acc_buyqty_'+label] = streaks_sniper[:, 0]
        nkdf['acc_sellqty_'+label] = streaks_sniper[:, 1]
        #nkdf['acc_buycnt_'+label] = streaks_sniper[:, 2]
        #nkdf['acc_sellcnt_'+label] = streaks_sniper[:, 3]
        #nkdf['buyvwap_'+label] = vwaps_sniper[:, 0]
        #nkdf['sellvwap_'+label] = vwaps_sniper[:, 1]
        #nkdf['vwap_'+label] = vwaps_sniper[:, 2]
        #Percent of book qty traded
        nkdf['acc_buypct_'+label] = nkdf['acc_buyqty_'+label] / (nkdf['askqty0'] + nkdf['acc_buyqty_'+label])
        nkdf['acc_sellpct_'+label] = nkdf['acc_sellqty_'+label] / (nkdf['bidqty0'] + nkdf['acc_sellqty_'+label])
        #nkdf['acc_totqty_'+label] = nkdf['acc_buyqty_'+label] - nkdf['acc_sellqty_'+label] #buys minus sells
 
    # separate contract streaks

    print('got to end of streaks')
    # modify later depending on how deio reads data?
    #nodupe_nkdf = nkdf
    # add weighted mids
    #wtmid_bool =  nkdf['bidprc0'] != nkdf['askprc0']
    nkdf['wtmid_bidqty0'] = 0.5
    nkdf['wtmid_askqty0'] = 0.5
    #nkdf['wtmid_bidqty0'][wtmid_bool] = nkdf['bidprc0'] * nkdf['bidqty0'] / (1. * nkdf['bidprc0'] * nkdf['bidqty0'] + nkdf['askprc0'] * nkdf['askqty0']) 
    #nkdf['wtmid_askqty0'][wtmid_bool] = nkdf['askprc0'] * nkdf['askqty0'] / (1. * nkdf['bidprc0'] * nkdf['bidqty0'] + nkdf['askprc0'] * nkdf['askqty0']) 
    nkdf['wtmid_bidqty0'] = nkdf['bidprc0'] * nkdf['bidqty0'] / (1. * nkdf['bidprc0'] * nkdf['bidqty0'] + nkdf['askprc0'] * nkdf['askqty0']) 
    nkdf['wtmid_askqty0'] = nkdf['askprc0'] * nkdf['askqty0'] / (1. * nkdf['bidprc0'] * nkdf['bidqty0'] + nkdf['askprc0'] * nkdf['askqty0']) 

    # ADD BOOK IMBALANCE FEATURES HERE #
    # finding moving averages and deviations
    ewz_cols = ('_bidqty0_', '_bidqty1_', '_askqty0_', '_askqty1_')
    #min_devs = (10, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000)#100)
    #gains = (0.01, 0.05, 0.1, 0.15, 0.20, 0.25):#(0.01, 0.02, 0.05, 0.1, 0.2):

    use_unique_qtys = True
    #print(datestr, dt.datetime.now(), 'calculating moving averages and deviations of market sizes')
    print('got to beg of gain/mindevs')
    for min_dev in min_devs:
        for gain in gains:
            #print(min_dev, gain)
            #print(datestr, dt.datetime.now(), 'computing with gain', gain, 'with min', min_dev)
            ew_mkt_means = nkm.ew_top_market(nkdf, gain, not use_unique_qtys, min_dev)

            for i in (4, 5, 6, 7):
                ew_mkt_means[:, i] = np.sqrt(ew_mkt_means[:, i]) ### variance of kalman filter output

            cols = [col + str(min_dev) for col in ewz_cols]
            dev_cols = [col + '_dev' for col in cols]
            z_cols = [col + '_z' for col in cols]
            prefix = 'mov_%dpct' % (int(100. *gain))
            #for i, col in enumerate(prefix + col for col in cols + dev_cols + z_cols):
            for i, col in enumerate(prefix + col for col in cols + dev_cols + z_cols):
                print(i, col)
                nkdf[col] = ew_mkt_means[:, i]
                #if '_z' in col:
                #    nkdf[col] = ew_mkt_means[:, i]
            del ew_mkt_means

    #print(datestr, dt.datetime.now(), len(nkdf), 'rows', len(nkdf), 'unique rows', len(nkdf.columns), 'columns')
    
    #print(datestr, dt.datetime.now(), 'done')
    

    return nkdf




