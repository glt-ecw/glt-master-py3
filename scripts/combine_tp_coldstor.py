import os
import re
import sys
import glob
import gzip
import numpy as np
import pandas as pd
import datetime as dt
import pandas.tseries.offsets as toffsets
#import bcolz
import subprocess

import glt.md.omx as omx
from glt import tools, manuscraper as nkm, manuscraper_beta as nkmb

from glt import md as md
"""
SEC_CONN = {
    'host': '10.1.31.200',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'SECURITIES'
}
"""
SEC_CONN = {
    'host': '10.125.0.200',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'SECURITIES'
}


from glt import db as db

def find_secid_bydate(date):
    ## find front month secid
    try:
        conn = db.conn(**SEC_CONN)
        # nk
        nk_secids = conn.frame_query(
            ("""
                SELECT id FROM SECURITIES.futures
                where id in (SELECT id FROM SECURITIES.securities where contractDefId=124) and expDate > '%s' limit 1;
                """) %(date)
        )

        # mnk
        mnk_secids = conn.frame_query(
            ("""
                SELECT id FROM SECURITIES.futures
                where id in (SELECT id FROM SECURITIES.securities where contractDefId=126) and expDate > '%s' limit 1;
                """) %(date)
        )

        # tp
        tp_secids = conn.frame_query(
            ("""
                SELECT id FROM SECURITIES.futures
                where id in (SELECT id FROM SECURITIES.securities where contractDefId=128) and expDate > '%s' limit 1;
                """) %(date)
        )   
     
    finally:
        conn.close()
    
    return nk_secids.id.values[0], mnk_secids.id.values[0], tp_secids.id.values[0]


def combine_tp(datestr, streaker_ms, gains, min_devs, weakturnqty=1):

    nk_secid, mnk_secid, tp_secid = find_secid_bydate(datestr)

    filename = '/glt/coldstor/csv/jpa.%s.both.books.csv.gz' % datestr
    book = md.standard.read_jpa_csv(filename, use_int_prices=True, buysell=True, time_adjust='9h', open_only=True)#, end_time='%sT1500'%datestr)    
    #print tp_secid    
    nkdf = book.loc[(book.secid==tp_secid)&(book.time.between('%sT090005'%datestr,'%sT15'%datestr))]
    
    ## mimic how deio consolidates and publishes md
    nkdf['eop_int'] = nkdf['eop'].astype(int)
    output = tools.deio_pub_reconciliation(nkdf)

    new_cols = ('keep_bool', 'buyvolume', 'buyprice', 'sellvolume', 'sellprice')
    for i, col in enumerate(new_cols):

        if col in nkdf.columns:
            del nkdf[col]

        nkdf[col] = output[:, i]


    for col in ('eop_int',):
        if col in nkdf.columns:
            del nkdf[col]

    # These should only contain either eop messages, consolidated trade messages, etc... 
    nkdf = nkdf.loc[nkdf['keep_bool']==1]

    nkdf['triggerid'] = (4e10 + nkdf['seqnum']).astype(int).astype(str).astype(int)
    nkdf['timestamp'] = pd.to_datetime(nkdf['time']).astype(int)

    del book
    
    if not len(nkdf):
        print("no rel md")
        return pd.DataFrame()

    for col in nkdf.columns:
        if 'prc' in col or 'price' in col:
            nkdf[col] = (nkdf[col]/1000.0).astype(int)

    # finding turn info

    nkdf['unique_tick'] = tools.unique_ticks(nkdf)
    #print md['unique_tick'].head(2).to_string()

    turnqty, turntick = 5, 5 ## Could modify so you record all turnqtys and filter later?
    time_to_turn = 500 #ms

    turn_cols = 'turn_direction', 'turn_price', 'prev_tick'
    for col in turn_cols:
        if col in nkdf.columns:
            del nkdf[col]

    nkdf = nkdf.join(tools.find_turns(nkdf, turnqty, turntick))
    nkdf['direction'] = nkdf['turn_direction']
    nkdf['sym_int'] = 0

    ## nest streaker window sizes in microseconds
    #streaker_windows = {us: long(us*1e3) for us in streaker_us} #(5e6, 25e6, 100e6)
    streaker_windows = {ms: int(ms*1e6) for ms in streaker_ms} #(5e6, 25e6, 100e6)
    reset_qtys = True

    for us, streaker_window in list(streaker_windows.items()):
        if us < 1.0:
            #label = "%dus" %(us)
            label = "%dus" %int(1000.0 * us)
        else:
            label = "%dms" %(us)

        #print("Calculating streaks for %dus and streaker window %d" %(us, streaker_window))
        streaks_sniper, vwaps_sniper = nkm.sniper_streaker_sides(nkdf, streaker_window, 0)

        nkdf['acc_buyqty_'+label] = streaks_sniper[:, 0]
        nkdf['acc_sellqty_'+label] = streaks_sniper[:, 1]
        #nkdf['acc_buycnt_'+label] = streaks_sniper[:, 2]
        #nkdf['acc_sellcnt_'+label] = streaks_sniper[:, 3]
        #nkdf['buyvwap_'+label] = vwaps_sniper[:, 0]
        #nkdf['sellvwap_'+label] = vwaps_sniper[:, 1]
        #nkdf['vwap_'+label] = vwaps_sniper[:, 2]
        #Percent of book qty traded
        nkdf['acc_buypct_'+label] = nkdf['acc_buyqty_'+label] / (nkdf['askqty0'] + nkdf['acc_buyqty_'+label])
        nkdf['acc_sellpct_'+label] = nkdf['acc_sellqty_'+label] / (nkdf['bidqty0'] + nkdf['acc_sellqty_'+label])
        #nkdf['acc_totqty_'+label] = nkdf['acc_buyqty_'+label] - nkdf['acc_sellqty_'+label] #buys minus sells
 
    # separate contract streaks

    print('got to end of streaks')
    # modify later depending on how deio reads data?
    #nodupe_nkdf = nkdf
    # add weighted mids
    #wtmid_bool =  nkdf['bidprc0'] != nkdf['askprc0']
    nkdf['wtmid_bidqty0'] = 0.5
    nkdf['wtmid_askqty0'] = 0.5
    #nkdf['wtmid_bidqty0'][wtmid_bool] = nkdf['bidprc0'] * nkdf['bidqty0'] / (1. * nkdf['bidprc0'] * nkdf['bidqty0'] + nkdf['askprc0'] * nkdf['askqty0']) 
    #nkdf['wtmid_askqty0'][wtmid_bool] = nkdf['askprc0'] * nkdf['askqty0'] / (1. * nkdf['bidprc0'] * nkdf['bidqty0'] + nkdf['askprc0'] * nkdf['askqty0']) 
    nkdf['wtmid_bidqty0'] = nkdf['bidprc0'] * nkdf['bidqty0'] / (1. * nkdf['bidprc0'] * nkdf['bidqty0'] + nkdf['askprc0'] * nkdf['askqty0']) 
    nkdf['wtmid_askqty0'] = nkdf['askprc0'] * nkdf['askqty0'] / (1. * nkdf['bidprc0'] * nkdf['bidqty0'] + nkdf['askprc0'] * nkdf['askqty0']) 

    # ADD BOOK IMBALANCE FEATURES HERE #
    msg_bool = nkdf['time'] != nkdf['time'].shift(-1)
    buylevelclear_bool = np.logical_and(nkdf['buyvolume'] > 0, nkdf['buyprice'] != nkdf['askprc0'])
    selllevelclear_bool = np.logical_and(nkdf['sellvolume'] > 0, nkdf['sellprice'] != nkdf['bidprc0']) 

    nkdf['buypct'] = 1.0 * nkdf['buyvolume'] / (nkdf['buyvolume'] + nkdf['askqty0'])
    nkdf['sellpct'] = 1.0 * nkdf['sellvolume'] / (nkdf['sellvolume'] + nkdf['bidqty0'])

    nkdf.loc[buylevelclear_bool, 'buypct'] = 1
    nkdf.loc[selllevelclear_bool, 'sellpct'] = 1

    nkdf['uniq_msg'] = np.nan
    nkdf.loc[msg_bool, 'uniq_msg'] = 1 

    nkdf['imbal'] = 1.0 * (nkdf['bidqty0'] - nkdf['askqty0']) / (nkdf['bidqty0'] + nkdf['askqty0'])
    nkdf['par_imbal'] = 1.0 * (nkdf['bidpar0'] - nkdf['askpar0']) / (nkdf['bidpar0'] + nkdf['askpar0'])

    nkdf.loc[buylevelclear_bool, 'imbal'] = 1.0 
    nkdf.loc[selllevelclear_bool, 'imbal'] = -1.0
    nkdf.loc[buylevelclear_bool, 'par_imbal'] = 1.0 
    nkdf.loc[selllevelclear_bool, 'par_imbal'] = -1.0

    # finding moving averages and deviations
    ewz_cols = ('_bidqty0_', '_bidqty1_', '_askqty0_', '_askqty1_')
    #min_devs = (10, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000)#100)
    #gains = (0.01, 0.05, 0.1, 0.15, 0.20, 0.25):#(0.01, 0.02, 0.05, 0.1, 0.2):

    use_unique_qtys = True
    #print(datestr, dt.datetime.now(), 'calculating moving averages and deviations of market sizes')
    #print('got to beg of gain/mindevs')
    for min_dev in min_devs:
        for gain in gains:
            #print(min_dev, gain)
            #print(datestr, dt.datetime.now(), 'computing with gain', gain, 'with min', min_dev)
            ew_mkt_means = nkm.ew_top_market(nkdf, gain, not use_unique_qtys, min_dev)

            for i in (4, 5, 6, 7):
                ew_mkt_means[:, i] = np.sqrt(ew_mkt_means[:, i]) ### variance of kalman filter output

            cols = [col + str(min_dev) for col in ewz_cols]
            dev_cols = [col + '_dev' for col in cols]
            z_cols = [col + '_z' for col in cols]
            prefix = 'mov_%dpct' % (int(100. *gain))
            #for i, col in enumerate(prefix + col for col in cols + dev_cols + z_cols):
            for i, col in enumerate(prefix + col for col in cols + dev_cols + z_cols):
                #print(i, col)
                if '_z' in col:
                    nkdf[col] = ew_mkt_means[:, i]
            del ew_mkt_means

    #print(datestr, dt.datetime.now(), len(nkdf), 'rows', len(nkdf), 'unique rows', len(nkdf.columns), 'columns')
    
    #print(datestr, dt.datetime.now(), 'done')
    

    return nkdf




