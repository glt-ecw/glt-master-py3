
import sys
import os
import numpy as np
import pandas as pd
import subprocess
import re
from time import sleep
import glob
from datetime import datetime

non_avol2 = re.compile('([0-9]{8}T[0-9]{6}\.[0-9]{6,9})\|[0-9]+\|SniperStrategyMonitor.cpp\|[0-9]+\|FINFO\|id: ([0-9]+), '
                         'Fired with sideTrigger:[A-Z|a-z|0-9|\.\:\ ]+ exchSeq: ([0-9]+) [A-Z|a-z|0-9|\.\:\ ]+, mdTime: \[([0-9]+) ([0-9]+)\], stratTime: \[([0-9]+) '
                         '([0-9]+)\], fwdTime: \[([0-9]+) ([0-9]+)\], placeTime: \[([0-9]+) ([0-9]+)\], placeTimeDiff: ([\-0-9]+)[A-Z|a-z|0-9|\.\:\ ]+')

non_avol3 = re.compile('([0-9]{8}T[0-9]{6}\.[0-9]{6,9})\|[0-9]+\|SniperStrategyMonitor.cpp\|[0-9]+\|FINFO\|id: ([0-9]+), '
                         'Fired with sideTrigger:[A-Z|a-z|0-9|\.\:\ ]+ exchSeq: ([0-9]+) [A-Z|a-z|0-9|\.\:\ ]+, mdTime: \[([0-9]+) ([0-9]+)\], stratTime: \[([0-9]+) '
                         '([0-9]+)\], fwdTime: \[([0-9]+) ([0-9]+)\], placeTime: \[([0-9]+) ([0-9]+)\][A-Z|a-z|0-9|\.\:\ ]*')

"""
avol = re.compile('([0-9]{8}T[0-9]{6}\.[0-9]{6,9})\|[0-9]+\|SniperStrategyMonitor.cpp\|[0-9]+\|FINFO\|id: ([0-9]+), '
                  'assume slugger trigger found on the side: [A-Z|a-z|0-9|\.\:\|\[\] ]+, seqNum: ([0-9]+), mdTime: \[([0-9]+) ([0-9]+)\], stratTime: \[([0-9]+) '
                  '([0-9]+)\], fwdTime: \[([0-9]+) ([0-9]+)\], placeTime: \[([0-9]+) ([0-9]+)\] placeTimeDiff: ([\-0-9]+)')
"""


non_avol = re.compile('([0-9]{8}T[0-9]{6}\.[0-9]{6,9})\|[0-9]+\|SniperStrategyMonitor.cpp\|[0-9]+\|FINFO\|id: ([0-9]+), '
                         'Fired with sideTrigger:[A-Z|a-z|0-9|\.\:\ ]+ exchSeq: ([0-9]+) [A-Z|a-z|0-9|\.\:\ ]+, mdTime: \[([0-9]+) ([0-9]+)\], stratTime: \[([0-9]+) '
                         '([0-9]+)\], fwdTime: \[([0-9]+) ([0-9]+)\], placeTime: \[([0-9]+) ([0-9]+)\] fireType: ([\-0-9]+)[A-Z|a-z|0-9|\.\:\ ]+')

avol = re.compile('([0-9]{8}T[0-9]{6}\.[0-9]{6,9})\|[0-9]+\|SniperStrategyMonitor.cpp\|[0-9]+\|FINFO\|id: ([0-9]+), '
                  'assume slugger trigger found on the side: [A-Z|a-z|0-9|\.\:\|\[\] ]+, seqNum: ([0-9]+), mdTime: \[([0-9]+) ([0-9]+)\], stratTime: \[([0-9]+) '
                  '([0-9]+)\], fwdTime: \[([0-9]+) ([0-9]+)\], placeTime: \[([0-9]+) ([0-9]+)\][A-Z|a-z|0-9|\.\ ]*: ([0-9]+)[A-Z|a-z|0-9|\.\ ]*')

busyconn = re.compile('([0-9]{8}T[0-9]{6}\.[0-9]{6,9})\|[0-9]+\|[A-Z|a-z|0-9|\|\.\:\ \]\[]+fastSendOrder:[A-Z|a-z|0-9|\.\:\ ]+ (Modify|Place) for order ([0-9]+), busyCount: ([0-9]).')




strat_columns = ['timestr', 'strategyid', 'triggerid', 'mdTime', 'usmdTime', 'stratTime', 
                 'usstratTime', 'fwdTime', 'usfwdTime', 'placeTime', 'usplaceTime', 'fwd2placeTime']

seconds_columns = 'mdTime','stratTime','fwdTime','placeTime'

int_columns = 'triggerid','strategyid','fwd2placeTime'

servermap = {
    'jpa01': 89,
    'jpa02': 90,
    'jpa03': 91,
    'jpa04': 93,
    'jpa05': 94,
    'jpa06': 96
}

jpaservers = 'jpa01','jpa02','jpa03','jpa04','jpa05','jpa06'

df_all = pd.DataFrame()

if __name__ == '__main__':
    output_dir = "/glt/storage/csv/"

    datestr = sys.argv[1]

    df_all = pd.DataFrame()
    error_all = pd.DataFrame()
    for server in jpaservers:
        print(server)
        mdlogfnames = glob.glob("/glt/storage/%s/jpaexchserver.JPA.%s*%s*.log"%(server, server, datestr))
        print(mdlogfnames)
        if len(mdlogfnames):
            for mdlogfname in mdlogfnames:
                print(mdlogfname)
                df = pd.DataFrame()
                with open(mdlogfname, "r") as f:
                    lines = f.readlines()
                
                res = []
                error = []
                #timestr, triggerid, mdTime, usMdTime, stratTime, usStratTime, fwdTime, usFwdTime, placeTime, usPlaceTime, placeTimeDiff
                for line in lines:
                    if 'Fired with sideTrigger' in line:
                        try:
                            non_avol_line = re.findall(non_avol, line)
                            non_avol_line2 = re.findall(non_avol2, line)
                            non_avol_line3 = re.findall(non_avol3, line)
                            if non_avol_line:
                                res.append(list(non_avol_line[0]))
                            elif non_avol_line2:
                                res.append(list(non_avol_line2[0]))
                            elif non_avol_line3:
                                res.append(list(non_avol_line3[0])+[0])
                        except:
                            print(line)
                    elif "assume slugger trigger" in line:
                        try:
                            #print(list(re.findall(avol, line)[0]))
                            res.append(list(re.findall(avol, line)[0]))
                        except:
                            print(line)
                    elif 'fastSendOrder' in line:
                        try:
                            error.append(list(re.findall(busyconn, line)[0]))
                        except:
                            print(line)
                    else:
                        continue

                df = pd.DataFrame(res, columns = strat_columns)
                #placeTime == 0 means modifies... ignore those for now
                #df = df.loc[(df.placeTime != '0')&(df.fwdTime != '0')]
                df = df.loc[(df.placeTime != '0')]
                df['serverid'] = servermap[server]

                for col in seconds_columns:
                    df[col] = datetime(1970, 1, 1, 9) + df[col].values.astype(int) * pd.offsets.Second() + df["us" + col].values.astype(int) * pd.offsets.Nano()        
                    #df[col+"str"] = [pd.to_datetime(x).strftime("%Y%m%dT%H%M%S.%f") for x in df[col].values] #strftime does not support ns

                for col in int_columns:
                    df[col] = df[col].values.astype(int)
                
                print(df.shape)
                df_all = pd.concat([df_all, df])
                                   
                error_df = pd.DataFrame(error, columns = ['timestr','queueType','deioid','busycount'])        
                error_all = pd.concat([error_all, error_df])
                
    #df_all['md2stratTime'] = ( pd.to_datetime(df_all['stratTime']).values.astype(long) - pd.to_datetime(df_all['mdTime']).values.astype(long) )
    #df_all['totalStrat'] = ( pd.to_datetime(df_all['placeTime']).values.astype(long) - pd.to_datetime(df_all['mdTime']).values.astype(long) )
    #df_all['fwdStrat'] = ( pd.to_datetime(df_all['fwdTime']).values.astype(long) - pd.to_datetime(df_all['stratTime']).values.astype(long) )
    #df_all['strat2Place'] = ( pd.to_datetime(df_all['fwdTime']).values.astype(long) - pd.to_datetime(df_all['stratTime']).values.astype(long) )

    #df_all['strat2fwdTime'] = ( pd.to_datetime(df_all['fwdTime']).values.astype(long) - pd.to_datetime(df_all['stratTime']).values.astype(long) )            

    df_all.to_csv('/glt/storage/csv/parsed_strat_times_exch_logs.%s.csv'%(datestr))
    error_all.to_csv('/glt/storage/csv/parsed_strat_times_exch_errorlogs.%s.csv'%(datestr))
    os.system('gzip -f /glt/storage/csv/parsed*%s*.csv'%datestr)



    #os.system('rm /glt/storage/csv/parsed_strat_times_exch_logs.%s.csv'%(datestr))
