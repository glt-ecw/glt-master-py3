
import sys
import os
import datetime as dt

import numpy as np
import pandas as pd

import glt.db

HOLIDAY_TABLES = {
    'jpx': 'jpx.holidays',
    'w': 'krx.holidays',
    'bk': 'krx.holidays'
}

RES01_CFG = {
    'host': '10.1.21.201',
    'user': 'gltloader',
    'pw': 'gltloader',
    'db': 'eod'
}

DEIODB_CFG = {
    'jpx': {
        'host': '10.1.31.202',
        'user': 'deio',
        'pw': '!w3alth!',
        'db': 'DEIO_JPX'
    },
    'w': {
        'host': '10.1.31.202',
        'user': 'deio',
        'pw': '!w3alth!',
        'db': 'DEIO_W'
    },
    'bk': {
        'host': '10.130.33.103',
        'user': 'deio',
        'pw': '!w3alth!',
        'db': 'DEIO_BK'
    },
    'securities': {
        'host': '10.1.31.202',
        'user': 'deio',
        'pw': '!w3alth!',
        'db': 'SECURITIES'
    }
}

def query_last_tradedate(instance):
    today = None
    if instance not in DEIODB_CFG:
        return today
    try:
        conn_cfg = DEIODB_CFG[instance]
        conn = glt.db.conn(**conn_cfg)
        cmd = """
            SELECT MAX(tradedate) as date
            FROM positions limit 1
        """
        tradedate = conn.frame_query(cmd)
        if tradedate.shape[0] > 0:
            tradedate = tradedate['date'].iloc[0].date()
    finally:
        conn.close()
    return tradedate

def query_current_securities(today):
    securities = None
    try:
        conn = glt.db.conn(**DEIODB_CFG['securities'])
        cmd = """
            SELECT id FROM (
                SELECT id, expdate FROM futures 
                UNION ALL 
                SELECT id, expdate FROM options 
                UNION ALL
                SELECT id, expdate FROM warrants 
                UNION ALL
                SELECT id, expdate from bonds
            ) as contracts 
            WHERE expdate>='%s'
        """ % today
        securities = conn.frame_query(cmd)['id'].values
    finally:
        conn.close()
    return securities

def query_holidays(instance):
    holidays = None
    if instance not in DEIODB_CFG:
        return holidays
    try:
        conn = glt.db.conn(**RES01_CFG)
        holidays_table = HOLIDAY_TABLES[instance]
        holidays = conn.frame_query("select date from %s" % holidays_table)['date'].values
    finally:
        conn.close()
    return holidays

def find_yesterday(today, instance, holidays):
    if holidays is None:
        return None
    yday = None
    yday = today - dt.timedelta(1)
    while yday in holidays or yday.weekday() > 4:
        yday -= dt.timedelta(1)
    return yday

def query_date_position(mydate, instance):
    pos = None
    if instance not in DEIODB_CFG:
        return pos
    deio_conn_cfg = DEIODB_CFG[instance]
    deio_instance = deio_conn_cfg['db']
    valid_securities = query_current_securities(mydate)
    try:
        conn = glt.db.conn(**RES01_CFG)    
        cmd = """
            SELECT securityid, position
            FROM eod.positions
            WHERE tradedate='%s' 
            AND instance='%s'
            AND securityid in %s
        """ % (mydate, deio_instance, tuple(valid_securities))
        pos = conn.frame_query(cmd)
        pos.index = pos.securityid
        pos = pos['position']
    finally:
        conn.close()
    return pos

def query_date_filled(mydate, instance):
    filled = None
    if instance not in DEIODB_CFG:
        return filled
    deio_conn_cfg = DEIODB_CFG[instance]
    deio_instance = deio_conn_cfg['db']
    try:
        conn = glt.db.conn(**deio_conn_cfg)
        cmd = """
            SELECT securityid, SUM(qty*(3-2*side)) as position
            FROM %s.fills
            WHERE tradedate='%s'
            AND NOT (filltype=2 or (exchtradeid='' and price=0))
            GROUP BY securityid
        """ % (deio_instance, mydate)
        filled = conn.frame_query(cmd)
        filled.index = filled.securityid
        filled = filled['position']
    finally:
        conn.close()
    return filled

if __name__ == '__main__':
    datestr = sys.argv[1]
    instance = sys.argv[2]
    day_offset = int(sys.argv[3])

    holidays = query_holidays(instance)

    if holidays is None:
        sys.exit('instance', instance, 'unknown. no holidays table found. quitting')

    today = pd.to_datetime(datestr).date() - dt.timedelta(day_offset)

    if today in holidays or today.weekday() > 4:
        sys.exit('not saving positions for %s. non-trading day' % today)

    filled_position = query_date_filled(today, instance)
    #while filled_position.shape[0] == 0:
    #    print('no fills found for', today)
    #    today = find_yesterday(today, instance, holidays)
    #    filled_position = query_date_filled(today, instance)
        
    yday = find_yesterday(today, instance, holidays)

    my_position = query_date_position(yday, instance)

    deio_instance = DEIODB_CFG[instance]['db']
    print(today, yday, deio_instance)

    for secid, secpos in my_position.add(filled_position, fill_value=0).items():
        try:
            conn = glt.db.conn(**RES01_CFG)
            cmd = """
                INSERT INTO eod.positions (tradedate, instance, securityid, position)
                VALUES ('%s', '%s', %d, %d)
                ON DUPLICATE KEY UPDATE position=%d
            """ % (today, deio_instance, secid, secpos, secpos)
            conn.execute(cmd)
        finally:
            conn.close()
