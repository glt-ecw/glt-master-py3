import pandas as pd
import numpy as np
import paramiko as miko
import sys


#init ssh and sft connections
def ssh_sftp_connections(server_name):
    ssh = miko.SSHClient()
    ssh.set_missing_host_key_policy(miko.AutoAddPolicy())
    server = '%s.glt-llc.com' % server_name
    ssh.connect(server, username='deio', password='fatmanblues')
    sftp = ssh.open_sftp()
    return (ssh, sftp)

#takes stdout generator from "netstat -rn" and converts to a ip, interface dataframe
def format_netstat_table(stdout):
    ip = []
    interface = []
    raw_list = []
    
    #strip and split stdout from exec_command
    for line in stdout:
        raw_list.append(line.rstrip().split(' '))
        
    #get the data you need from the list of lists
    #idx 0 is the ip, idx -1 is the interface
    for line in raw_list:
        try:
            # see if the first 2 chars in string convert to an int... if so its an ip addr
            tmp = int(line[0][:1])
            ip.append(line[0])
            interface.append(line[-1])
        except:
            #if first 2 chars are not int, discard line
            pass
            
        
    df = pd.DataFrame( {'ip' : ip, 'interface' : interface} )
    return df

#takes stdout from 'grep "orGatewayAddr" jpa##.jpa.cfg and returns a gw_num, ip dataframe
def format_gw_cfg(stdout):
    gw_num = []
    ip = []
    raw_list = []
    for line in stdout:
        raw_list.append(line.rstrip().split('='))

    for line in raw_list:
        #find last 'r' in 'orGatewayAddr' str and int() the rest of the string
        gw_num.append( int(line[0][line[0].rfind('r')+1:]) )
        ip.append( line[1] )

    df = pd.DataFrame( {'gw_num' : gw_num, 'ip': ip} )
    return df

#use the above functions so you only need to make one function call
#note: this will fail if DNS isnt working... check that first
def get_gw_table(box):
    try:
        print(box)
        ssh, sftp = ssh_sftp_connections(box)

        #netstat command and df
        command = 'netstat -rn'
        stdin, stdout, stderr = ssh.exec_command(command)
        netstat_df = format_netstat_table(stdout)

        #gw num command and df
        command = 'grep orGatewayAddr /glt/app/cfg/%s.jpa.cfg' % box
        stdin, stdout, stderr = ssh.exec_command(command)
        gw_num_df = format_gw_cfg(stdout)

        #join the two dfs and create new col
        df = gw_num_df.merge(netstat_df, how='inner')
        df['gw'] = df['ip'] + ':' + df['interface']
        ssh.close()
        sftp.close()
    except:
        print("error")
        return pd.DataFrame()
   
    return df[['gw_num', 'gw']]

def apply_gw(row):
    
    #this is poor coding... its using gw_dict from the main script.... i should pass this or something
    #get the correct gw table for the box youre looking at
    df = gw_dict[row['serverid']]
    
    # filter df where the connId in the gateway table == connId in row
    val = df.loc[df['gw_num']==row['conn_num'], 'gw'].values
    
    #return 'none' if you cant find the gateway, otherwise return the ip:interface string
    if len(val) > 0:
        val = val[0]
    else:
        val = 'none'
    return val

#create gateway tables
#boxes = ['jpa01', 'jpa02', 'jpa03', 'jpa05']
boxes = ['jpa01', 'jpa02', 'jpa03', 'jpa04', 'jpa05', 'jpa06']
servers = [89, 90, 91, 93, 94, 96]
gws = []; rel_servers = [];

for server, box in zip(servers, boxes):
    print(server, box)
    table = get_gw_table(box)
    if len(table):
        gws.append( table )
        rel_servers.append(server)

#servers = [89, 90, 91, 94]
#map to dict of gateway tables, keys are serverIds
gw_dict = {}
for i in range(len(rel_servers)):
    gw_dict[rel_servers[i]] = gws[i]


datestr = sys.argv[1]

gw_df = pd.DataFrame()
for key in list(gw_dict.keys()):
    d = gw_dict[key]
    d['server'] = key
    gw_df = pd.concat([gw_df, d])
    
gw_df.to_csv('/glt/storage/pcapmap/gateway_map.%s.csv'%datestr)
