
import sys
import os
import shutil
import pandas as pd

from io import StringIO

STRAT_IDX = 0
SECID_IDX = 1

def extract_ids(items):
    secid = int(items[SECID_IDX])
    stratid = int(items[STRAT_IDX])
    return stratid, secid

def extract_product(secid, rollmap):
    if secid not in rollmap:
        return None
    name = rollmap[secid]['shortname']
    if '-' in name:
        name = name.split('-')[0]
    return name[:-3]

def evaluate_strategy(expired_rollmap, rollmap, chunk):
    if not chunk[0][0]:
        return chunk, 0
    ids = [extract_ids(items) for items in chunk]
    secids = {id[1] for id in ids}
    if any(secid == -1 for secid in secids):
        return chunk
    products = {extract_product(secid, rollmap) for secid in secids}
    if None in products:
        products.remove(None)
    if len(products) > 1:
        return chunk, 0
    next_secids, expired_secids = {}, {}
    for secid in secids:
        if secid in expired_rollmap:
            expired_secids[secid] = expired_rollmap[secid]['next_secid']
        next_secids[secid] = rollmap.get(secid)
    new_chunk = []
    if not any(v is None for v in list(next_secids.values())) and expired_secids:
        for items in chunk:
            stratid, secid = extract_ids(items)
            items[SECID_IDX] = '%d' % rollmap.get(secid)['next_secid']
            new_chunk.append(items)
        return new_chunk, 1
    else:
        return chunk, 0

if __name__ == '__main__':
    expired_rollmap_filename = sys.argv[1]
    rollmap_filename = sys.argv[2]
    filename = sys.argv[3]

    expired_rollmap = pd.read_pickle(expired_rollmap_filename).T.to_dict()
    rollmap = pd.read_pickle(rollmap_filename).T.to_dict()

    #next_ids = rollmap['next_secid'].to_dict()
    #expired_next_ids = expired_rollmap['next_secid'].to_dict()

    with open(filename, 'r') as f:
        lines = f.read().split('\n')

    output = StringIO()
    
    roll_count = 0
    last_stratid = -1
    strategy_chunk = []
    for line in lines:
        if line and line[0] == '#':
            print(line, file=output)
            continue
        items = line.split(',')
        if len(items) < 2:
            stratid, secid = -1, -1
        else:
            stratid, secid = extract_ids(items)
        if stratid == -1 or stratid != last_stratid:
            if strategy_chunk:
                chunk, updated = evaluate_strategy(expired_rollmap, rollmap, strategy_chunk)
                for row in chunk:
                    print(','.join(row), file=output)
                roll_count += updated
            strategy_chunk = [items]
            last_stratid = stratid
        else:
            strategy_chunk.append(items)
    
    if roll_count > 0:
        print(filename, ': rolling')
        shutil.copy(filename, filename + '.bak')
        with open(filename, 'w') as f:
            print(output.getvalue(), file=f)
    else:
        print(filename, ': not rolling')
    
