
import sys
import numpy as np
import pandas as pd
import subprocess
import re
from time import sleep

from collections import defaultdict

from glt.md import omx
from glt.log_parsing \
    import jpx_ormsg_parse, jpx_add_conn_info, jpx_log_config, jpx_conn_regex

if __name__ == '__main__':

    datestr = sys.argv[1]

    output_filename = '/glt/ssd/hdf/orders.%s.h5' % datestr

    serverid_list = [
        #('jpx01', 46),
        #('jpx02', 47),
        ('jpa01', 89),
        ('jpa02', 90),
        ('jpa03', 91),
        ('jpa04', 93),
        ('jpa05', 94),
        ('jpa06', 96)
    ]

    raw = defaultdict(list)
    for server, serverid in serverid_list:
        filename = '/glt/storage/%s/ORMsgs.%s.%s.log.gz' % (server, datestr, server)
        try:
            ormsg_lines = subprocess.check_output(['gzip', '-cd', filename]).split('\n')
        except:
            print('file: %s does not exist' %filename)
            continue

        last_conn, last_gateway = None, None
        for line in ormsg_lines:
            if 'PARM' in line:
                results = jpx_conn_regex.findall(line)
                last_conn, last_gateway = results[0]
            else:
                for msg in jpx_log_config:
                    if msg in line:
                        s = jpx_log_config[msg]['regex']
                        result = s.findall(line)
                        if result:
                            parsed = jpx_add_conn_info(result[0], last_conn, last_gateway)
                            raw[msg].append((server, serverid) + parsed)

    for msg in jpx_log_config:
        params = jpx_log_config[msg]
        cols = params['cols']
        int_cols = params['int_cols']
        float_cols = params['float_cols']
        del_cols = params['del_cols']
        dat = pd.DataFrame(raw[msg], columns=['server', 'serverid'] + cols)
        omx.add_time_labels(dat)
        if int_cols is not None:
            dat[int_cols] = dat[int_cols].astype(np.int64)
        if float_cols is not None:
            dat[float_cols] = dat[float_cols].astype(np.float64)
        if del_cols is not None:
            for col in del_cols:
                del dat[col]

        if 'err_msg' in dat.columns:
            dat.loc[dat['err_msg']=='', 'err_msg'] = np.nan
        #print(dat.shape)
        #print(dat.serverid.unique())
        #print(dat.head(10).to_string())
        hdf_config = {
            'mode': 'a',
            'format': 't',
            'complevel': 9,
            'complib': 'blosc',
            'data_columns': dat.columns
        }
        dat.sort_values(by='time').to_hdf(output_filename, msg.lower(), **hdf_config)
        sleep(0.2)
