
import sys
import os
import pandas as pd
import datetime as dt
import subprocess
import json

import glt.db

from collections import deque

from glt.misc import send_gmail

res01_db_config = {
    'host': '10.1.21.201',
    'user': 'gltreader',
    'pw': 'gltreader',
    'db': 'GLTRESULTS'
}

dbserver01_db_config = {
    'host': '10.1.31.202',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'SECURITIES'
}

stop_conditions = {
    'bid': {
        'passive': ['bidlte'],
        'aggressive': ['offerlte'],
        'both': ['bidlte', 'offerlte'],
    },
    'ask': {
        'passive': ['offergte'],
        'aggressive': ['bidgte'],
        'both': ['offergte', 'bidgte']
    }
}

price_offsets = {
    'bidlte': 0,
    'offergte': 0,
    'offerlte': 1,
    'bidgte': 1
}

def to_datedb(date):
    return date.isoformat().replace('-', '_')

def retrieve_defs(today, contract, which_month):
    defs = None
    try:
        conn = glt.db.conn(**dbserver01_db_config)
        cmd = (
            "select id, shortname, security from futures_expiries "
            "where expiry >= '%s' and security = '%s' "
            "order by expiry"
        ) % (today, contract.upper())
        defs = conn.frame_query(cmd)
    finally:
        conn.close()
    return defs.iloc[which_month]

def retrieve_settle(today, secid):
    settle = None
    try:
        yday = today - dt.timedelta(1)
        conn = glt.db.conn(**res01_db_config)
        dbs = conn.frame_query("show databases")['Database'].values
        while to_datedb(yday) not in dbs:
            yday -= dt.timedelta(1)
        conn.execute('use %s' % to_datedb(yday))
        cmd = "select prc as px_last from jpx_settles where securityid=%d" % secid
        settle = conn.frame_query(cmd)
    finally:
        conn.close()
    return settle

def set_ordertype(max_timer_orders):
    count = [0]
    def which_order():
        count[0] += 1
        if count[0] > max_timer_orders:
            return 'preopen'
        else:
            return 'timer'
    return which_order

def place_orders(bigtick, bidprc, askprc, conditions, 
        n=0, cross=False, reverse=False, shift=False):
    i = 0
    orders = deque()
    mybidprc, myaskprc = bidprc, askprc
    while i < n:
        for condition in stop_conditions['bid'][conditions['bid']]:
            offset = price_offsets[condition]
            orders.append((bidprc + bigtick*offset, condition))
            i += 1
        for condition in stop_conditions['ask'][conditions['ask']]:
            offset = price_offsets[condition]
            orders.append((askprc - bigtick*offset, condition))
            i += 1
        if cross:
            bidprc += bigtick
            askprc -= bigtick
        else:
            bidprc -= bigtick
            askprc += bigtick
    best_bid, best_ask, worst_bid, worst_ask = 4*[-1]
    if n >= 4:
        if conditions['bid'] == 'both':
            bbi, bai, wbi, wai = 0, 2, -4, -2
        else:
            bbi, bai, wbi, wai = 0, 1, -2, -1
        best_bid = orders[bbi][0] + bigtick
        best_ask = orders[bai][0] - bigtick
        worst_bid = orders[wbi][0] - bigtick
        worst_ask = orders[wai][0] + bigtick
    if reverse:
        orders.reverse()
    if shift:
        orders.rotate(shift)
    return orders, best_bid, best_ask, worst_bid, worst_ask


if __name__ == '__main__':
    config_file = sys.argv[1]
    product = sys.argv[2]
    which_month = int(sys.argv[3])
    which_print, skip = sys.argv[4].split('-')
    which_print = int(which_print)
    skip = int(skip)

    with open(config_file, 'r') as f:
        all_param = json.load(f)

        product_config = all_param['productConfig']
        from_center = all_param['fromCenter']

        overnight_filenames = all_param['overnightFilenames']

        # how many orders are going to be timer vs preopen?
        n_timer = all_param['nTimer']
        
        # how many orders designated to each phase?
        phases = all_param['phases']

    # phase 1 and 2 are non-crossing orders near center price
    # phase 3 and 4 are crossing orders near center price
    # phase 5 is remaining orders extending from where phase 1 and 2 left off
    today = dt.date.today()

    if product not in product_config:
        sys.exit('product does not exist in map. quitting')

    defs = retrieve_defs(today, product, which_month)
    secid = defs['id']
    contract = defs['shortname'].lower()

    bigtick, fmt = product_config[product]

    print('###', secid, contract, end=', ')
    print('tick width:', bigtick)
    
    # is the product nikkei? if not, read from the db
    if product in overnight_filenames:
        filename = overnight_filenames[product]
        settles = pd.read_csv(filename)
        settles['date'] = pd.to_datetime(settles['date'])
        settles['px_last'] = ((settles['px_last']/bigtick).round()*bigtick).astype(int)

        yday = dt.datetime.now().date() - dt.timedelta(1)

        while yday.weekday() in (5, 6):
            yday -= dt.timedelta(1)

        dat = settles.loc[settles['date']==yday]
        #dat = settles.iloc[[-1]]
    else:
        #defs = retrieve_defs(today, contract)
        #secid = defs['id']
        dat = retrieve_settle(today, secid)

    if not dat.shape[0]:
        email_specs = {
            'username': 'kak@glt-llc.com',
            'password': 'upsideupside',
            'recipients': ['kak@glt-llc.com'],
            'subject': 'stopMonSprinter.cfg using yesterday FAILED!',
            'message': '<pre style="font-size: 1.5em">%s</pre>' % settles.to_string()
        }
        send_gmail(**email_specs)
        prc = settles['px_last'].iloc[-1]
    else:
        prc = dat['px_last'].iloc[0]

    which_order = set_ordertype(n_timer)

    prices = []
    purge_keys = [
        'bidCondition', 'askCondition', 'fromWhichPhase', 'useBestMarket'
    ]
    order_count = 0
    for i, phase in enumerate(phases):
        conditions = {
            'bid': phase['bidCondition'],
            'ask': phase['askCondition']
        }
        prev_phase = phase['fromWhichPhase']
        if prev_phase > 0:
            best_bid, best_ask, worst_bid, worst_ask = prices[prev_phase-1]
            if phase['useBestMarket']:
                bidprc = best_bid
                askprc = best_ask
            else:   
                bidprc = worst_bid
                askprc = worst_ask
        else:
            bidprc = prc - bigtick * from_center
            askprc = prc + bigtick * from_center
        for key in purge_keys:
            del phase[key]
        
        phase_dat = place_orders(bigtick, bidprc, askprc, conditions, **phase)
        phase_orders, best_bid, best_ask, worst_bid, worst_ask = phase_dat

        prices.append([best_bid, best_ask, worst_bid, worst_ask])

        print('### phase', i+1)
        while phase_orders:
            oprc, condition = phase_orders.popleft()
            order_type = which_order()
            if order_count % skip == which_print:
                print('%s,%s,%s,%s' % (contract, fmt % oprc, condition, order_type))
            order_count += 1
