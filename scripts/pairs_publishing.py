import sys
import json
import glt.db

from glt.publishing.deio_pairs import DeioPairsPublisher
from glt.log import create_logger, generate_log_filename


if __name__ == '__main__':
    cfg_filename = sys.argv[1]

    with open(cfg_filename, 'r') as f:
        cfg = json.load(f)

    app_name = cfg['publisherName']
    model_id = cfg['modelID']
    md_sub_iface_addr = cfg['mdFwdChannel']
    fill_sub_iface_addr = cfg['fillFwdChannel']
    pub_iface_addr = cfg['pubChannel']

    # logging
    logging_cfg = cfg['logging']
    log_filename = generate_log_filename(logging_cfg['filenameFormat'])
    logger = create_logger(app_name, log_filename, debug=logging_cfg['debug'])

    # create models
    pairs = []
    for model_cfg in cfg['models']:
        logger.info(model_cfg)
        exec_cmd = 'from %s import %s as Model' % (model_cfg['module'], model_cfg['object'])
        logger.info(exec_cmd)
        exec(exec_cmd)
        model = Model(**model_cfg['parameters'])
        logger.info('created %s' % model)
        pairs.append(model)
    
    # start publisher
    pairs_publisher = DeioPairsPublisher(
        md_sub_iface_addr, fill_sub_iface_addr, pub_iface_addr, 
        pairs, model_id,
        price_multiplier=100.0, pub_interval=5, logger=logger
    )
    pairs_publisher.start_publishing()

    while True:
        try:
            pairs_publisher.evaluate_models()
        except KeyboardInterrupt:
            logger.warning('KeyboardInterrupt! quitting!')
            break


