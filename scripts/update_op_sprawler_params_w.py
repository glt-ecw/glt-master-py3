import numpy as np
import pandas as pd
import datetime as datetime
print('numpy', np.version.version)
print('pandas', pd.version.version)
import glt.db as db
import sys

#########
ManualAdjust = False
#########

def daterange(date, to=None, step=datetime.timedelta(days=1)):

    dates = []
    
    if to is None:
        condition = lambda d: True
    else:
        condition = lambda d: (d <= to)
    
    if isinstance(step, int):
        # By default, integers are interpreted in days. For more granular
        # steps, use a `datetime.timedelta()` instance.
        step = datetime.timedelta(days=step)
    elif isinstance(step, str):
        # If the string
        if re.match(r'^(\d+)$', str(step)):
            step = datetime.timedelta(days=int(step))
        else:
            try:
                step = delta(step)
            except ValueError:
                pass
    
    if not isinstance(step, datetime.timedelta):
        raise TypeError('Invalid step value: %r' % (step,))
    
    # The main generation loop.
    while condition(date):
        dates.append(date)
        #yield date
        date += step
    return dates

def find_wte(dte):
    if 0 <= dte < 7:
        return 1
    elif 7 <= dte < 14:
        return 2
    elif 14 <= dte:
        return 3
    else:
        return 0
    
def ungroupbyprc(prc_arry):
    minprcs = []
    for prc_tup in prc_arry:
        minp, maxp = prc_tup.split(",")
        minprcs.append(int(maxp[:-1]))
    
    return minprcs

DBSERVER01_CONN = {
    'host': 'dbserver01',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'DEIO_W'
}

SEC_CONN = {
    'host': 'dbserver01',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'SECURITIES'
}

SCRIPT_DIR = '/home/deio/christine/sprawler_params/options/'

#datestr = sys.argv[1]

#date_db = datestr[:4]+'-'+datestr[4:6]+'-'+datestr[6:8]

#DBSERVER01 = "10.1.31.202"

new_params = pd.read_csv(SCRIPT_DIR + 'new_params.csv', usecols=['wte','prc_range','side','minqty','streakerqty'])
new_params['adjustedPriceUpperLimit'] = ungroupbyprc(new_params.prc_range.values)
new_params['db_side'] = [1 if side == -1 else 2 for side in new_params.side.values]
del new_params['side']    
#curr_params = pd.read_csv('/home/deio/christine/curr_params.csv', usecols=['wte','prc_range','side','minqty','streakerqty'])
#curr_params['adjustedPriceUpperLimit'] = ungroupbyprc(curr_params.prc_range.values)

streaker_dict = dict(list(zip(list(range(1, 4)), [new_params.loc[new_params.wte == wte].streakerqty.values for wte in range(1, 4)])))
minqty_dict = dict(list(zip(list(range(1, 4)), [new_params.loc[new_params.wte == wte].minqty.values for wte in range(1, 4)])))

StreakerByWTE = {
    1 : streaker_dict[1],
    2 : streaker_dict[2],
    3 : streaker_dict[3]
}

MinqtyByWTE = {
    1 : minqty_dict[1],
    2 : minqty_dict[2],
    3 : minqty_dict[3]
}

ParamsByWTE = {
    1 : tuple(zip(StreakerByWTE[1], MinqtyByWTE[1])),
    2 : tuple(zip(StreakerByWTE[2], MinqtyByWTE[2])),
    3 : tuple(zip(StreakerByWTE[3], MinqtyByWTE[3])),
}

streakerqty, minqty, upperpx = 3 * [0]
datestr = str(datetime.date.today())

#### Manually adjust sprawler params with working size

dat = pd.read_csv('/home/deio/karl/w01.kospiSprinterOrders.cfg', header=None, skiprows=2, error_bad_lines=False)
dat.columns = ['op','side','price','qty','type','user','bck_qty']
options = dat.loc[dat.price < 100]
bids = options.loc[options.side=='bid']
offers = options.loc[options.side=='offer']

qty_map_array=[]
adjustedPriceUpperLimits = [2.0, 3.0, 4.0, 6.0, 8.0, 11.0, 21.0, 31.0]
prc_ranges = [(0.01, 0.01), (0.02, 0.02), (0.03, 0.03), (0.04, 0.05), (0.06, 0.07), (0.08, 0.10), (0.11, 0.20), (0.21, 0.30)]

for side, df in zip([1, 2], [bids, offers]):
    for adjustedPriceUpperLimit, prc_range in zip(adjustedPriceUpperLimits, prc_ranges):
        (low, high) = prc_range
        qty = df.loc[df.price.between(low, high)].qty.mean()
        qty_map_array.append([side, adjustedPriceUpperLimit, qty])

qty_map = pd.DataFrame(qty_map_array, columns = ['side','price','qty']).fillna(0)

######################################################

try:
    sec = db.conn(**SEC_CONN)
    #find dte:
    find_dte_cmd = "select expiry from options_expiries where expiry>='" + datestr + "' and shortname like 'KOO%' order by expiry limit 1"
    
    next_expiry = sec.frame_query(find_dte_cmd)      
    dates2expiry = daterange(datetime.date.today(), to = next_expiry.expiry.values[0], step=datetime.timedelta(days=1))
    dte = len(np.unique(dates2expiry)) - 1
    wte = find_wte(dte)

    if ManualAdjust == True:
        updated_params = new_params.merge(qty_map, how='left', left_on=['db_side','adjustedPriceUpperLimit'], right_on=['side','price'])
        updated_params['minLeanWPos'] = updated_params['minqty'] + updated_params['qty']
        params = updated_params.loc[updated_params.wte == wte].dropna()[['db_side','adjustedPriceUpperLimit','streakerqty','minLeanWPos']].values
    else:
        params = new_params.loc[new_params.wte == wte].dropna()[['db_side','adjustedPriceUpperLimit','streakerqty','minqty']].values

    for param in params:

        db_side, adjustedPriceUpperLimit, streakerThreshold, minLeanWPos = param
        #print db_side, adjustedPriceUpperLimit, streakerThreshold, minLeanWPos
        update_cmd = """
            update sprawlerParamsByPrice
            set streakerThreshold=%d, minLeanQty=%d
            where adjustedPriceUpperLimit=%f and side=%d
        """ % (streakerThreshold, int(minLeanWPos), adjustedPriceUpperLimit / 100., db_side)
        try:
            conn = db.conn(**DBSERVER01_CONN)
            conn.execute(update_cmd)
        except:
            continue

finally:
    print("dte: ", dte)
    print("wte: ", wte)
    print(new_params.loc[new_params.wte == wte].to_string())
    print(conn.frame_query("select * from sprawlerParamsByPrice"))   
    conn.close()
    sec.close()
