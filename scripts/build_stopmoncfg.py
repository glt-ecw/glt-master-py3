
import sys
import os
import pandas as pd
import datetime as dt
import subprocess

import glt.db

from collections import deque

from glt.misc import send_gmail

product_config = {
    'NK': (10, '%d'),
    'JGBL': (0.01, '%0.2f')
}

res01_db_config = {
    'host': '10.1.21.201',
    'user': 'gltreader',
    'pw': 'gltreader',
    'db': 'GLTRESULTS'
}

dbserver01_db_config = {
    'host': '10.1.31.202',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'SECURITIES'
}

def to_datedb(date):
    return date.isoformat().replace('-', '_')

def retrieve_defs(today, contract, which_month):
    defs = None
    try:
        conn = glt.db.conn(**dbserver01_db_config)
        cmd = (
            "select id, shortname, security from futures_expiries "
            "where expiry >= '%s' and security = '%s' "
            "order by expiry"
        ) % (today, contract.upper())
        defs = conn.frame_query(cmd)
    finally:
        conn.close()
    return defs.iloc[which_month]

def retrieve_settle(today, secid):
    settle = None
    try:
        yday = today - dt.timedelta(1)
        conn = glt.db.conn(**res01_db_config)
        dbs = conn.frame_query("show databases")['Database'].values
        while to_datedb(yday) not in dbs:
            yday -= dt.timedelta(1)
        conn.execute('use %s' % to_datedb(yday))
        cmd = "select prc as px_last from jpx_settles where securityid=%d" % secid
        settle = conn.frame_query(cmd)
    finally:
        conn.close()
    return settle

def set_ordertype(max_timer_orders):
    count = [0]
    def which_order():
        count[0] += 1
        if count[0] > max_timer_orders:
            return 'preopen'
        else:
            return 'timer'
    return which_order

if __name__ == '__main__':
    nk_overnight_filename = sys.argv[1]
    n = int(sys.argv[2])
    cross = int(sys.argv[3])
    start_orders = int(sys.argv[4])
    max_timers = int(sys.argv[5])
    start_server, num_servers = (int(x) for x in sys.argv[6].split('-'))
    #contract = sys.argv[7].lower()
    product = sys.argv[7].upper()
    which_month = int(sys.argv[8])

    if start_server >= num_servers:
        sys.exit(1)

    today = dt.date.today()
    # figure out the tick width
    #product = contract[:-3]

    if product not in product_config:
        sys.exit('product does not exist in map. quitting')

    defs = retrieve_defs(today, product, which_month)
    secid = defs['id']
    contract = defs['shortname'].lower()

    bigtick, fmt = product_config[product]

    print('###', secid, contract, end=', ')
    print('tick width:', bigtick)
    
    # is the product nikkei? if not, read from the db
    if product == 'NK':
        settles = pd.read_csv(nk_overnight_filename)
        settles['date'] = pd.to_datetime(settles['date'])
        settles['px_last'] = ((settles['px_last']/bigtick).round()*bigtick).astype(int)

        yday = dt.datetime.now().date() - dt.timedelta(1)

        while yday.weekday() in (5, 6):
            yday -= dt.timedelta(1)

        #dat = settles.loc[settles['date']==yday]
        dat = settles.iloc[[-1]]
    else:
        #defs = retrieve_defs(today, contract)
        #secid = defs['id']
        dat = retrieve_settle(today, secid)

    if not dat.shape[0]:
        email_specs = {
            'username': 'kak@glt-llc.com',
            'password': 'upsideupside',
            'recipients': ['kak@glt-llc.com'],
            'subject': 'stopMonSprinter.cfg using yesterday FAILED!',
            'message': '<pre style="font-size: 1.5em">%s</pre>' % settles.to_string()
        }
        send_gmail(**email_specs)
        prc = settles['px_last'].iloc[-1]
        #sys.exit(1)
    else:
        prc = dat['px_last'].iloc[0]

    topbidprc = prc + cross*bigtick
    topaskprc = prc - cross*bigtick
   
    start_ticks = -2 
    start_at = start_ticks*bigtick

    cross_at = cross*bigtick
    #cross_orders = cross - (start_at)//10
    cross_orders = cross - start_ticks

    remaining_at = cross_at - start_at
    remaining_orders = max(0, n-start_orders-cross_orders)

    which_order = set_ordertype(max_timers)

    p_one_ordersplit = start_orders//num_servers
    server_count, p_one_count = 0, 0
    # phase 1
    print("# phase 1")
    p_one_deqs = deque()
    for i in range(start_orders):
        if not p_one_deqs or p_one_count >= p_one_ordersplit:
            p_one_count = 0
            p_one_deqs.appendleft(deque())
            server_count += 1
        if server_count > num_servers:
            p_one_deqs.popleft()
            break
        p_one_deq = p_one_deqs[0]
        p_one_count += 1
        # start appending to the head
        p_one_deq.appendleft('#')
        p_one_deq.appendleft('%s,%s,%s,%s' % (contract, fmt % (prc - (start_at - (i-1)*bigtick)), 'bidgte', which_order()))
        p_one_deq.appendleft('%s,%s,%s,%s' % (contract, fmt % (prc - (start_at - i*bigtick)), 'offergte', which_order()))
        p_one_deq.appendleft('%s,%s,%s,%s' % (contract, fmt % (prc + start_at - (i-1)*bigtick), 'offerlte', which_order()))
        p_one_deq.appendleft('%s,%s,%s,%s' % (contract, fmt % (prc + start_at - i*bigtick), 'bidlte', which_order()))

    p_one_deqs.rotate(start_server)

    p_one_list = []
    for p_one_deq in p_one_deqs:
        while p_one_deq:
            item = p_one_deq.popleft()
            p_one_list.append(item)
            print(item)

    #print("# phase1a")
    #for item in p_one_list:
    #    if 'timer' in item:
    #        print(item.replace('timer', 'preopen'))
    #    elif item == '#':
    #        print(item)

    # phase 2
    print("# phase 2")
    for i in range(cross_orders-1, -1, -1):
        print('%s,%s,%s,%s' % (contract, fmt % (prc + (cross-i)*bigtick), 'bidlte', which_order()))
        print('%s,%s,%s,%s' % (contract, fmt % (prc + (cross-i+1)*bigtick), 'offerlte', which_order()))
        print('%s,%s,%s,%s' % (contract, fmt % (prc - (cross-i)*bigtick), 'offergte', which_order()))
        print('%s,%s,%s,%s' % (contract, fmt % (prc - (cross-i+1)*bigtick), 'bidgte', which_order()))
        print('#')
    # phase 3

    print("# phase 3")
    for i in range(remaining_orders):
        print('%s,%s,%s,%s' % (contract, fmt % (prc + start_at - (start_orders+i)*bigtick), 'bidlte', which_order()))
        print('%s,%s,%s,%s' % (contract, fmt % (prc + start_at - (start_orders+i-1)*bigtick), 'offerlte', which_order()))
        print('%s,%s,%s,%s' % (contract, fmt % (prc - (start_at - (start_orders+i)*bigtick)), 'offergte', which_order()))
        print('%s,%s,%s,%s' % (contract, fmt % (prc - (start_at - (start_orders+i-1)*bigtick)), 'bidgte', which_order()))
        print('#')

    #old_output = ','.join(['nku15', str(topbidprc), str(n), str(topaskprc), str(n)])
    #filename = os.path.join(cfgdir, 'stopMonSprinter.cfg')
    #with open(filename, 'w') as f:
    #    f.write(output + '\n')
