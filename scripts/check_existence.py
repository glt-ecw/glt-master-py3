
import sys
import os
import psutil

from subprocess import Popen

if __name__ == '__main__':
    keep_alive_filename = sys.argv[1]

    with open(keep_alive_filename, 'r') as f:
        keep_alive = {os.path.basename(l): l for l in f.read().split('\n')[:-1]}
        for proc in psutil.process_iter():
            try:
                cwd = proc.cwd()
                name = proc.cmdline()[-1]
                if name.endswith('.sh'):
                    name = os.path.basename(name)
                    if name in keep_alive:
                        print('still alive!', name)
                        del keep_alive[name]
            except psutil.AccessDenied:
                pass
        for key, process in list(keep_alive.items()):
            proc = Popen([process], shell=True)
        
