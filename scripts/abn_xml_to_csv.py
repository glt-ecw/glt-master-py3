
import sys
import os
import glob
import datetime as dt
import pandas as pd
import csv

from zipfile import ZipFile, BadZipfile
import xml.etree.ElementTree as ET

import glt.db

abn_dir = '/glt/storage/abn'

bad_filename = os.path.join(abn_dir, 'BADFILES')

"""
DB_CONFIG = {
    'host': '10.1.31.202',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'clearing'
}
"""

DB_CONFIG = {
    'host': '10.125.0.200',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'clearing'
}

CSV_CONFIG = {
    'index': False,
    'quoting': csv.QUOTE_NONNUMERIC
}

fill_columns = [
    'TicketNumber', 'TransactionDate', 'TimeStamp', 'ListingExchange',
    'Symbol', 'Expiry', 'BuySellCode', 
    'TransactionPrice', 'TransactionQuantity', 'TransactionType',
    'OpenCloseCode', 'OppositePartyCode', 
    'ExecTraderId', 'ProductGroupCode', 'ProcessingDate'
]

max_nodes = len(fill_columns)
fill_column_idx = {col: i for i, col in enumerate(fill_columns)}

def empty_values(max_nodes):
    return max_nodes*[None]

def to_mysql_date(datestr):
    return pd.to_datetime(datestr).date().isoformat()

if __name__ == '__main__':
    datestr = sys.argv[1]
    output_filename = sys.argv[2]

    try:
        conn = glt.db.conn(**DB_CONFIG)
        accounts = conn.frame_query("select * from accounts")
        #accounts.index = accounts['name']
        accounts.index = accounts['accountid']
        accounts_map = accounts['id'].to_dict()
    finally:
        conn.close()

    raw_fills = []
    bad_files = []
    for filename in glob.glob(os.path.join(abn_dir, datestr + '*TRANL*')):
        try:
            with ZipFile(filename, 'r') as zipf:
                for xml_file in zipf.namelist():
                    xml_items = xml_file.split('-')
                    account_num, batch_id = xml_items[1], xml_items[-1][:-4]
                    
                    if account_num not in accounts_map:
                        print('could not find account %s in db. quitting' % account_num)
                        sys.exit(1)

                    info_vals = [accounts_map[account_num], batch_id]
                    fill_tree = ET.fromstring(zipf.read(xml_file)).iter()
                    node_count = 0
                    values = empty_values(max_nodes)
                    for node in fill_tree:
                        tag = node.tag
                        if node_count >= max_nodes:
                            raw_fills.append(info_vals + values)
                            values = empty_values(max_nodes)
                            node_count = 0
                        elif tag in fill_column_idx:
                            i = fill_column_idx[tag]
                            values[i] = node.text
                            node_count += 1
        except BadZipfile:
            bad_files.append(filename)

    # handle the bad files
    with open(bad_filename, 'r') as badfile:
        current_bad_filenames = set(badfile.read().split('\n'))

    with open(bad_filename, 'a') as badfile:
        for bad_filename in bad_files:
            if bad_filename not in current_bad_filenames:
                print('bad zipfile: %s. adding to BADFILES.' % filename)
                print(bad_filename, file=badfile)
            else:
                print('bad zipfile: %s. already in BADFILES!' % filename)
    
    # now process csv
    df_cols = [
        'account_id', 'batch_id', 'id', 'date', 'time', 'exchange', 
        'security', 'expiry', 'side', 'price', 'qty', 'fill_type',
        'open_close_code', 'opposite_party', 'exec_trader_id',
        'product_group', 'processing_date'
    ]

    fills = pd.DataFrame(raw_fills, columns=df_cols)
    timestamp_col = fills['date'].apply(to_mysql_date) + ' ' + fills['time']
    fills.insert(4, 'timestamp', timestamp_col)
    del fills['time']
    print(fills.to_string())
    fills.to_csv(output_filename, **CSV_CONFIG)

