
import sys
import os
import glob
import paramiko
import datetime as dt
import subprocess

from time import sleep
from _thread import error as ThreadError

MAX_INTERVAL = 60 # seconds

def calc_sleep_time(right_now):
    return MAX_INTERVAL - (right_now.minute * 60 + right_now.second)%MAX_INTERVAL

def get_remote_file(sftp, remote_filename, local_filename):
    try:
        sftp.get(remote_filename, local_filename)
    except ThreadError:
        print('failed to start a new thread for sftp? chilling out')
        sleep(10)

def create_local_filename(old_basename, local_dir='.'):
    basename = str(old_basename).translate(None, ' ()[]')
    return os.path.join(local_dir, basename)

hostname = '91.213.201.123'
port = 22
#user = '1812'
user = '2822'
password = ''

local_dir = '/glt/storage/abn'
remote_dir = 'outgoing'

bad_filename = os.path.join(local_dir, 'BADFILES')

datestr = sys.argv[1]

if __name__ == '__main__':
    rsa_private_key = '/home/deio/.ssh/id_rsa'
    private_key = paramiko.RSAKey.from_private_key_file(rsa_private_key)

    try:
        print('connecting to abn.')
        transport = paramiko.Transport((hostname, port))
        #transport.connect(username = user, pw = password)
        transport.connect(username = user, pkey = private_key)

        print('connected. checking for new files.')
        current_filenames = None
        update_filenames = True
        #while True:
        sftp = paramiko.SFTPClient.from_transport(transport)
        sftp_filenames = sftp.listdir(remote_dir)

        # check for any bad zip files
        bad_filenames = []
        with open(bad_filename, 'r') as badfile:
            filenames = (l for l in badfile.read().split('\n') if l)
            for filename in filenames:
                item = filename, filename.split('-')[-1]
                bad_filenames.append(item)

        for filename, suffix in bad_filenames:
            print('bad filename: %s. fetching again' % filename)
            found_files = [x for x in sftp_filenames if suffix in x]
            for remote_basename in found_files:
                remote_filename = os.path.join(remote_dir, remote_basename)
                local_filename = create_local_filename(remote_basename, local_dir)
                get_remote_file(sftp, remote_filename, local_filename)

        if bad_filenames:
            with open(bad_filename, 'w') as badfile:
                pass

        # have we added files to the directory?                    
        if update_filenames:
            current_filenames = glob.glob(os.path.join(local_dir, '*'))
            print('new file count:', len(current_filenames))
            update_filenames = False

        # check remote directory for any new files
        for remote_basename in sftp_filenames:
            remote_filename = os.path.join(remote_dir, remote_basename)
            local_filename = create_local_filename(remote_basename, local_dir)
            if local_filename not in current_filenames:
                print(local_filename, 'does not exist')
                get_remote_file(sftp, remote_filename, local_filename)
                update_filenames = True
                # insert net liq data to db
                if '-POSL-' in local_filename:
                    try:
                        subprocess.call('/glt/research/bin/insert_abn_data.sh %s'%datestr)
                    except:
                        print('error running insert_abn_data.sh')
                sleep(2)

        sleep_time = calc_sleep_time(dt.datetime.now())
        print(dt.datetime.now(), 'sleeping for', sleep_time)
        sftp.close()
        sleep(sleep_time)
    finally:
        print('disconnecting.')
        sftp.close()
        transport.close()


