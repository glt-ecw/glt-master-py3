import zipfile
import xml.etree.ElementTree as ET
import sys
import numpy as np
import pandas as pd
import glob
from pandas.io import sql
from sqlalchemy import create_engine, MetaData, Table, select
import sys
import subprocess
import datetime as dt
import os
import glt.db
import smtplib
import base64
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from io import StringIO as cStringIO
import io
import pandas.io.formats.format as pf #excel as pf
#import pandas.core.format as pf
#import pandas.formats.format as pf
import contextlib

old_stdout = sys.stdout
StringCatcher = io.StringIO()
sys.stdout = StringCatcher

conn_defn = {
    'host': '10.125.0.200',
    'user': 'gltloader',
    'pw': 'gltloader',
    'db': 'pnl'
}

#conn = glt.db.conn(**conn_defn)

#email sign in and send function
def sendEmail(toList,msgSubject,message):
    if str(type(toList)) == "<type 'str'>":
        toList = [toList]

    msg = MIMEMultipart()
    #msg['From'] = 'G.E. Depalma <ged@glt-llc.com>'
    from_email = 'greenlight@glt-llc.com'
    secret_password = 'upsideupside'

    msg['From'] = 'GLT <%s>' % from_email
    msg['Subject'] = msgSubject

    msg.attach(MIMEText(message,'html'))

    server = smtplib.SMTP()
    server.connect('smtp.gmail.com',587)
    server.ehlo()
    server.starttls()
    server.login(from_email, secret_password)
    server.sendmail(from_email, toList, msg.as_string()) #your email
    server.close()
    return


def float_format(x):
    return '%0.0f' % x

#Used for email formatting
@contextlib.contextmanager
def custom_formatting():
    orig_float_format = pd.options.display.float_format
    orig_int_format = pf.IntArrayFormatter

    pd.options.display.float_format = '{:0,.0f}'.format
    class IntArrayFormatter(pf.GenericArrayFormatter):
        def _format_strings(self):
            formatter = self.formatter or '{:,d}'.format
            fmt_values = [formatter(x) for x in self.values]
            return fmt_values
    pf.IntArrayFormatter = IntArrayFormatter
    yield
    pd.options.display.float_format = orig_float_format
    pf.IntArrayFormatter = orig_int_format

    
#If there is a deposit recorded, this function is used for the email formatting
def DEP(x):
    if x.empty:
        return ""
    elif ~x.empty:
        print("Deposits")
        print("-" * (max(pnl_calc_dep['Deposits'].apply(len)) + 6))
        with custom_formatting():
            print((dep.to_string(col_space=0,header=False, index=False)))
        print("")

#If there is a withdrawal recorded, this funciton is used for the email formatting
def WIT(x):
    if x.empty:
        return ""
    else:
        print("Withdrawals")
        print("-" * (max(pnl_calc_wit['Withdrawals'].apply(len)) + 7))
        with custom_formatting():
            print(wit.to_string(col_space=0, header=False, index=False))
            
            
            
#Function finds the cash and pnl difference from the previous trading day
def diff(x):
    with zipfile.PyZipFile(x, 'r') as zf:
        xml_txt = None
        for obj in zf.filelist:
            with zf.open(obj.filename) as f:
                xml_txt = f.read()
            if xml_txt is not None:
                break
    root = ET.fromstring(xml_txt)
    
    d = os.path.basename(x)[:8]
    try:
        conn = glt.db.conn(**conn_defn)
        dates = conn.frame_query("select distinct(date) from abn_netliq where date<= %s order by date desc limit 2;" %d)
        today, yday = pd.to_datetime(dates.values[:, 0]).strftime('%Y%m%d')
        #sett_dates = conn.frame_query("select distinct(date) from abn_settlement where date<= %s order by date desc limit 2;" %x)
        #cmd = "select * from abn_netliq where date in ('%s', '%s')" % (today, yday)
        abn_netliq1 = conn.frame_query("select * from abn_netliq where date in ('%s', '%s')" % (today, yday))
        abn_settlement1 = conn.frame_query("select * from abn_settlement where date in ('%s', '%s')" % (today, yday))
    finally:
        conn.close()
    
    abn_netliq1['date'] = pd.to_datetime(abn_netliq1['date'])
    abn_settlement1['date'] = pd.to_datetime(abn_settlement1['date'])
    revenue_terms = [
        'COST OF FUTURE',
        'COST OF Futures',
        'COST OF Options'
    ]
    deposit_terms = [
        'CASH WITHDRAWAL',
        'CASH DEPOSIT'
    ]
    expense_terms = [
        'FEE',
        'INTEREST',
        'COST OF PAYMENTS'
        'COST OF Others'
    ]
    revenue_bool = abn_netliq1['description'].str.contains('|'.join(revenue_terms), na=False)
    deposit_bool = abn_netliq1['description'].str.contains('|'.join(deposit_terms), na=False)
    expense_bool = abn_netliq1['description'].str.contains('|'.join(expense_terms), na=False)
    other_bool = ~(revenue_bool|expense_bool|deposit_bool)
    
    abn_netliq2 = abn_netliq1[revenue_bool]
    abn_netliq2 = abn_netliq2.rename(columns = lambda x: x.replace('value', 'rev'))
    del abn_netliq2['seqnum']
    
    abn_netliq3 = abn_netliq1[deposit_bool]
    abn_netliq3 = abn_netliq3.rename(columns = lambda x: x.replace('value', 'D/W'))
    del abn_netliq3['seqnum']
    
    abn_netliq4 = abn_netliq1[expense_bool]
    abn_netliq4 = abn_netliq4.rename(columns = lambda x: x.replace('value', 'expense'))
    abn_netliq5 = abn_netliq1[other_bool]
    abn_netliq5 = abn_netliq5.rename(columns = lambda x: x.replace('value', 'other'))
        
    merged = pd.merge(abn_netliq1, abn_netliq2, on=['date', 'description', 'currency', 'product'], how='left')
    merged1 = pd.merge(merged, abn_netliq3, on=['date', 'description', 'currency', 'product'], how='left')
    merged2 = pd.merge(merged1, abn_netliq4, on=['date','description', 'currency', 'product', 'seqnum'], how='left')
    merged3 = pd.merge(merged2, abn_netliq5, on=['date','description', 'currency', 'product', 'seqnum'], how='left')

    merged3['revenue'] = merged3['rev'].replace([None], [0])
    merged3['W/D'] = merged3['D/W'].replace([None], [0])
    merged3['expenses'] = merged3['expense'].replace([None], [0])
    merged3['others'] = merged3['other'].replace([None], [0])
    
    del merged3['rev']
    del merged3['D/W']
    del merged3['expense']
    del merged3['other']
    
    cashnew = merged3.groupby(['date', 'product', 'currency']).sum().reset_index()
    abn_settlement1['adjustment'] = (abn_settlement1['theo'] - abn_settlement1['settle']) * abn_settlement1['position'] * abn_settlement1['tradingUnit']
    settlementnew = abn_settlement1.groupby(['date', 'product', 'currency'])['adjustment'].sum().reset_index()
    
    merged5 = pd.merge(cashnew, settlementnew, on = ['date', 'product', 'currency'], how='left')
    merged5['adjustment'] = merged5['adjustment'].fillna(0)


    merged_new = merged5.groupby(['date', 'currency'])['W/D', 'expenses', 'revenue', 'value', 'others', 'adjustment'].sum().reset_index()
    
    today = merged_new.loc[merged_new['date'] == d]
    yesterday = merged_new.loc[merged_new['date'] != d]
    testing = pd.merge(today, yesterday, on='currency', how='left', suffixes=('_today', '_yesterday'))
    testing['deposits_withdrawals'] = testing['W/D_today'] - testing['W/D_yesterday']
    #testing['dw_corr'] = -1*(testing['deposits_withdrawals'])
    testing['rev'] = (testing['revenue_today'] - testing['revenue_yesterday']) # + testing['dw_corr']
    testing['expense'] = (testing['expenses_today'] - testing['expenses_yesterday'])
    testing['others'] = testing['others_today'] - testing['others_yesterday']
    testing['adjustment'] = testing['adjustment_today'] - testing['adjustment_yesterday']
    testing['currency'] = [str(x) for x in testing['currency']]
    testing['date'] = testing['date_today']
    testing['account_id'] = np.nan
    testing['account_id'] = testing['account_id'].replace([None], [""])
    
    
    
    return testing.loc[:, ['date', 'date_yesterday', 'account_id', 'currency', 'rev', 'deposits_withdrawals', 'expense', 'others', 'adjustment']]

#Finds the latest date in database, and inserts all data dated after the latest date. 
#The email later is sent with the values that are being inserted into the database.
datestr = sys.argv[1]
filenames = glob.glob('/glt/storage/abn/*%s**-1812-C1812-POSL-*'%datestr)
filenames.sort()

#try:
#    conn = glt.db.conn(**conn_defn)
#    date = conn.frame_query("select max(date) from abn_pnl")
#    
#finally:
#    conn.close()
    
#datenew = pd.to_datetime(str(date.values[0, 0])).strftime('%Y%m%d')
#dates = map(lambda x: os.path.basename(x)[:8], filenames)
#newdat = filter(lambda x: x >= datenew, dates)
#filename = filter(lambda x: any(d in x for d in newdat), filenames)[:1]

data = []
for filename in filenames:
    dat = diff(filename)
    if len(dat):
        data.append(dat)
if len(data):
    pnl_calc = pd.DataFrame().append(data)
    try:
        conn = glt.db.conn(**conn_defn)
        inserted_data = conn.frame_query('select * from abn_pnl where date=%s'%datestr)
        if not len(inserted_data):
            ## no pnl info for given date...
            for i, row in pnl_calc.iterrows():
                output = 'insert into abn_pnl %s' % (str(tuple(pnl_calc.columns)).replace('\'', ''), )
                output2 = ' values%s' % (str(tuple(row.values)), )
                OP = output + output2
                conn.execute(OP)

    finally:   
        conn.close()
        
#Manipulating the numbers being added to the database into email form    
pnl_calc['Revenue'] = pnl_calc['rev'] + pnl_calc['adjustment']
pnl_calc['Revenue'] = pnl_calc['Revenue'].round()
pnl_calc['Expense'] = pnl_calc['expense']
pnl_calc['Expense'] = pnl_calc['Expense'].round()
pnl_calc['Net'] = pnl_calc['Revenue'] + pnl_calc['Expense']
pnl_calc['empty'] = np.nan
pnl_calc['empty'] = pnl_calc['empty'].replace([None], [0])
pnl_calc2 = pnl_calc.loc[:, ['Revenue', 'currency', 'Expense', 'Net', 'deposits_withdrawals', 'empty']]


pnl_calc3 = pnl_calc2.loc[:, ['Revenue', 'Expense', 'Net', 'empty']]
pnl_calc3 = pnl_calc3.loc[~(pnl_calc3==0).all(axis=1)]
pnl_calc_dep = pnl_calc2.loc[pnl_calc2['deposits_withdrawals'] > 0]
pnl_calc_dep = pnl_calc_dep.rename(columns = lambda x: x.replace('deposits_withdrawals', 'Deposits'))
pnl_calc_dep = pnl_calc_dep.loc[~(pnl_calc_dep == 0).all(axis=1)]
pnl_calc_wit = pnl_calc2.loc[pnl_calc2['deposits_withdrawals'] < 0]
pnl_calc_wit = pnl_calc_wit.rename(columns = lambda x: x.replace('deposits_withdrawals', 'Withdrawals'))
pnl_calc_wit = pnl_calc_wit.loc[~(pnl_calc_wit == 0).all(axis=1)]

merged8 = pd.merge(pnl_calc2, pnl_calc3, on = ['Revenue', 'Expense', 'Net', 'empty'], how='right')
#merged7 = pd.merge(merged6, pnl_calc_dep, on = ['Revenue', 'currency', 'Expense', 'Net', 'empty'], how='left')
#merged8 = pd.merge(merged7, pnl_calc_wit, on = ['Revenue', 'currency', 'Expense', 'Net', 'empty'], how='left')
deposit = pnl_calc_dep.loc[:, ['currency', 'Deposits']]
withdrawal = pnl_calc_wit.loc[:, ['currency', 'Withdrawals']]
deposit = deposit[pd.notnull(deposit['Deposits'])]
withdrawal = withdrawal[pd.notnull(withdrawal['Withdrawals'])]    
    
    
    
pnlcalc = merged8.loc[:, ['Revenue', 'currency', 'Expense', 'Net', 'empty']]
merged8['Revenue'] = merged8['Revenue'].apply(float_format)
merged8['Expense'] = merged8['Expense'].apply(float_format)
merged8['Net'] = merged8['Net'].apply(float_format)
pnl_calc_dep['Deposits'] = pnl_calc_dep['Deposits'].apply(float_format)
#merged8['currency'] = merged8['currency'].apply(float_format)
#deposit['Deposits'] = deposit['Deposits'].map(str) + " " + deposit['currency']
pnl_calc_wit['Withdrawals'] = pnl_calc_wit['Withdrawals'].apply(float_format)
#withdrawal['Withdrawals'] = withdrawal['Withdrawals'].map(str) + " " + withdrawal['currency']
pnlcalc = pnlcalc.loc[:, ['Revenue', 'currency', 'empty','empty', 'Expense', 'currency', 'empty', 'empty', 'Net', 'currency']]
pnlcalc['empty'] = pnlcalc['empty'].replace([0], [""])


dep = deposit.loc[:, ['Deposits', 'currency']]
wit = withdrawal.loc[:, ['Withdrawals', 'currency']]

def cash(x):
    with zipfile.PyZipFile(x, 'r') as zf:
        xml_txt = None
        for obj in zf.filelist:
            with zf.open(obj.filename) as f:
                xml_txt = f.read()
            if xml_txt is not None:
                break
    root = ET.fromstring(xml_txt)
    
    d = os.path.basename(x)[:8]
    try:
        conn = glt.db.conn(**conn_defn)
        abn_netliq1 = conn.frame_query("select * from abn_netliq where date = %s;" %d)
        abn_settlement1 = conn.frame_query("select * from abn_settlement where date = %s;" %d)
    finally:
        conn.close()

    abn_netliq1['date'] = pd.to_datetime(abn_netliq1['date'])
    abn_settlement1['date'] = pd.to_datetime(abn_settlement1['date'])

    cashnew = abn_netliq1.groupby(['product', 'currency']).sum().reset_index()
    abn_settlement1['adjustment'] = (abn_settlement1['theo'] - abn_settlement1['settle']) * abn_settlement1['position'] * abn_settlement1['tradingUnit']
    settlementnew = abn_settlement1.groupby(['product', 'currency'])['adjustment'].sum().reset_index()


    merged5 = pd.merge(cashnew, settlementnew, on = ['product', 'currency'], how='left')
    merged5['adjustment'] = merged5['adjustment'].fillna(0)

    merged_new = merged5.groupby('currency')['value', 'adjustment'].sum().reset_index()

    cash = merged_new
    cash['cash'] = cash['value']
    cash = cash.loc[:, ['cash', 'adjustment', 'currency']]

    
    cash['cash'] = cash['cash'].round()
    cash['net'] = cash['cash'] + cash['adjustment']
    cash['empty'] = np.nan
    cash['empty'] = cash['empty'].replace([None], [0])
    cash = cash.loc[:, ['cash', 'adjustment', 'net', 'currency', 'empty']]
    
    return cash

data = []
for filename2 in filenames[:1]:
    dat = cash(filename2)
    data.append(dat)
    
cash = pd.DataFrame().append(data)


cash1 = cash.loc[:, ['cash', 'adjustment', 'net', 'empty']]
cash1 = cash1.loc[~(cash1 == 0).all(axis=1)]
cash2 = pd.merge(cash, cash1, on = ['cash', 'adjustment', 'net', 'empty'], how='right')
cash2['empty'] = cash2['empty'].replace([0], [""])
cash2 = cash2.loc[:, ['cash', 'currency', 'empty', 'empty', 'adjustment', 'currency', 'empty', 'empty', 'net', 'currency']]
cash1['cash'] = cash1['cash'].apply(float_format)
cash1['adjustment'] = cash1['adjustment'].apply(float_format)
cash1['net'] = cash1['net'].apply(float_format)

pnlcalc[''] = ""
pnlcalc.index = pnlcalc['']
del pnlcalc['']

cash2[''] = ""
cash2.index = cash2['']
del cash2['']


#new_dat = ''.join(newdat[:1])
#new_dat = '-'.join([new_dat[:4], new_dat[4:6], new_dat[6:]])
date = '-'.join([datestr[:4], datestr[4:6], datestr[6:]])

#final email format
print('Date: %s' %date)
print("")
print(" " "Revenue" + " " * ((max(merged8['Revenue'].apply(len)) - 7) + 12) + "Expenses" + " " * (max(merged8['Expense'].apply(len) - 8) + 11) + "PNL")
print(" " + "-" * (max(merged8['Revenue'].apply(len)) +7) + "     " + "-" * max(merged8['Expense'].apply(len) + 6) + "     " + "-" * max(merged8['Net'].apply(len) + 7))
with custom_formatting():
    print((pnlcalc.to_string(col_space=0, header=False)))
print("")
print(" " "Cash" + " " * ((max(cash1['cash'].apply(len)) - 4) + 12) + "Theo Adjust" + " " * ((max(cash1['adjustment'].apply(len)) - 10) + 11) + "Adjusted Cash")
print(" " + "-" * (max(cash1['cash'].apply(len)) + 7) + "     " + "-" * max(cash1['adjustment'].apply(len) + 7) + "     " + "-" * max(cash1['net'].apply(len) + 7))
with custom_formatting():
    print((cash2.to_string(col_space=0, header=False)))
print("")
DEP(dep)
WIT(wit)

test_list = ['cke@glt-llc.com']

toList = [
    #'kak@glt-llc.com',
    #'ged@glt-llc.com',
    #'jvh@gracehall.com', 'jgehrke76@hotmail.com', 
    #'mmcdonald@midlandmetalproducts.com', 
    'ecw@glt-llc.com', 'baw@glt-llc.com',
    'jff@glt-llc.com', 'cke@glt-llc.com',
    'ars@glt-llc.com'
] #recipient email list

sys.stdout = old_stdout
output = StringCatcher.getvalue()

sendEmail(toList,"Grace Hall Asia PNL (%s)" %(date),"<pre>" + output + "</pre>")
#sendEmail(test_list,"Grace Hall Asia PNL (%s)" %(date),"<pre>" + output + "</pre>")

