
import sys
import os
import subprocess

from glt.misc import send_gmail

tail = subprocess.Popen(['tail', '-n', '20', '/var/log/sfptpd_stats.txt'], stdout=subprocess.PIPE)
grep = subprocess.Popen(['grep', '-E', '\[ptp-gm->phc[0-9]'], stdin=tail.stdout, stdout=subprocess.PIPE)

output = grep.communicate()[0].split('\n')[:-1]

if not output:
    recipients = ['kak@glt-llc.com', 'ecw@glt-llc.com']
    send_gmail('kak@glt-llc.com', 'upsideupside', recipients, 'AUTOMATED: ptp not running on jpxcap01', '')

