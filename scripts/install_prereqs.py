import os
import imp
import glob
import subprocess


conda_libraries = [
    'gcc',
    'mysql',
    'bzip2',
    'boost',
    'numpy',
    'scipy',
    'pandas',
    'scikit-learn',
    'cython'
]

pip_libraries = [
    'cvxpy',
    'scapy'
]

etc_libraries = [
    ('mysql', 'mysql-connector-python-2.1.4')
]


def update_conda(real_call=False, show_cmd=True):
    cmd = ['conda', 'update', 'conda']
    if show_cmd:
        print(' '.join(cmd))
    if real_call:
        subprocess.call(cmd)


def decompress(filename, real_call=False, show_cmd=True):
    cmd = None
    if filename.endswith('tar.gz'):
        cmd = ['tar', 'xzvf', filename]
    if cmd is not None:
        if show_cmd:
            print(' '.join(cmd))
        if real_call:
            subprocess.call(cmd)


def install_cwdir_library(real_call=False, show_cmd=True):
    cmd = ['python', 'setup.py', 'install', '--record', 'files.txt']
    if show_cmd:
        print(' '.join(cmd))
    if real_call:
        subprocess.call(cmd)


def install_libraries(etc_path, real_call=False, show_cmd=True):
    conda_cmd = ['conda', 'install'] + conda_libraries
    pip_cmd = ['pip', 'install'] + pip_libraries
    if show_cmd:
        print(' '.join(conda_cmd))
        print(' '.join(pip_cmd))
    if real_call:
        subprocess.call(conda_cmd)
        subprocess.call(pip_cmd)

    cwd = os.getcwd()
    os.chdir(etc_path)
    for libname, libdir in etc_libraries:
        try:
            imp.find_module(libname)
            print('python module "%s" already exists. moving on' % libname)
            continue
        except ImportError:
            print('missing %s. install from %s' % (libname, libdir))
        contents = glob.glob('%s*' % libdir)
        if not contents:
            raise ValueError('library: %s not found in %s' % (libdir, etc_path))
        if libdir not in contents:
            filename = contents[0]
            decompress(filename, real_call=real_call)
        os.chdir(libdir)
        install_cwdir_library(real_call=real_call)
        os.chdir('..')
    os.chdir(cwd)


if __name__ == '__main__':
    glt_path = os.environ['GLT_MASTER_PATH']
    etc_path = os.path.join(glt_path, 'etc')

    update_conda(real_call=True)

    install_libraries(etc_path, real_call=True)
