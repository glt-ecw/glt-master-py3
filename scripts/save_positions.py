import sys
import os
import pandas as pd
import datetime as dt
import json
import glt.db


# e.g: python save_positions.py 20161102 DEIO_W


def read_config(json_filename, instance):
    cfg, param = {}, {}
    with open(json_filename, 'r') as f:
         cfg = json.load(f)
    if instance not in cfg:
        raise KeyError('%s does not exist in save_positions.py config')
    cfg = cfg[instance]
    param['user_pos'] = cfg['positionConnectionUser']
    param['user_fill'] = cfg['fillsConnectionUser']
    param['pos_table'] = cfg['positionTable']
    param['fill_table'] = cfg['fillsTable']
    return param

def find_max_date(conn, pos_table, instance, show_cmd=True):
    cmd = """
        SELECT max(tradedate) as date
        FROM %s
        WHERE instance='%s'
    """ % (pos_table, instance)
    if show_cmd:
        print(cmd)
    df = conn.frame_query(cmd)
    if len(df) == 0:
        raise ValueError('no data found')
    return df['date'].iloc[0].date()


def find_yesterday(today):
    if today.weekday() == 0:
        return today - dt.timedelta(3)
    else:
        return today - dt.timedelta(1)


def query_positions(conn, date, instance, pos_table):
    cmd = """
        SELECT securityid as secid, position
        FROM %s
        WHERE (
            tradedate='%s'
            AND
            instance='%s'
        )
    """ % (pos_table, date, instance)
    return conn.frame_query(cmd)


def query_fills(conn, date, instance, show_cmd=True):
    args = instance, date, instance, date
    cmd = """
        SELECT
            securityid as secid,
            SUM(qty) as volume,
            SUM(price*qty) as notional,
            SUM(qty*(3-2*side)) as filled,
            SUM(price*qty*(3-2*side)) as prcqty
        FROM (
            SELECT
                securityid,
                side,
                exchtradeid,
                filltype,
                price,
                qty
            FROM %s.fills
            WHERE (
                tradedate='%s'
                AND
                exchtradeid<>''
            )
            GROUP BY id, securityid, side, exchtradeid, filltype
            UNION ALL
            SELECT
                securityid,
                side,
                exchtradeid,
                filltype,
                price,
                qty
            FROM %s.fills
            WHERE (
                tradedate='%s'
                AND
                exchtradeid=''
                AND
                price<>0
            )
        ) as fills_tmp
        GROUP BY securityid

    """ % args
    if show_cmd:
        print(cmd)
    df = conn.frame_query(cmd)
    for col in ['volume', 'filled']:
        df[col] = df[col].round().astype(int)
    return df


def insert_pos_data(conn, table, date, instance, row,
                    real_insert=False, show_cmd=True):
    cmd = """
        INSERT INTO %s
        (tradedate, instance, securityid, position)
        VALUES('%s', '%s', %d, %d)
    """ % (table, date, instance, row[0], row[1])
    if show_cmd:
        print(cmd)
    if real_insert:
        conn.execute(cmd)


def insert_fill_data(conn, table, date, instance, row,
                     real_insert=False, show_cmd=True):
    cmd = """
        INSERT INTO %s
        (tradedate, instance, securityid, volume, notional, position, prcqty)
        VALUES('%s', '%s', %d, %d, %0.4f, %d, %0.4f)
    """ % ((table, date, instance) + row)
    if show_cmd:
        print(cmd)
    if real_insert:
        conn.execute(cmd)


def delete_table_contents(conn, table, date, instance,
                          real_delete=False, show_cmd=True):
    cmd = """
        DELETE FROM %s
        WHERE (
            tradedate='%s'
            AND
            instance='%s'
        )
    """ % (table, date, instance)
    if show_cmd:
        print(cmd)
    if real_delete:
        conn.execute(cmd)


def not_expired(conn, pos_table, date, show_cmd=True):
    secids = ', '.join(map(str, pos_table['secid'].values))
    cmd = """
        SELECT id as secid
        FROM SECURITIES.futures
        WHERE (
            id in (%s)
            AND
            expdate >= '%s'
        )
        UNION ALL
        SELECT id as secid
        FROM SECURITIES.options
        WHERE (
            id in (%s)
            AND
            expdate >= '%s'
        )
        UNION ALL
        SELECT id as secid
        FROM SECURITIES.warrants
        WHERE (
            id in (%s)
            AND
            expdate >= '%s'
        )
        UNION ALL
        SELECT id as secid
        FROM SECURITIES.bonds
        WHERE (
            id in (%s)
            AND
            expdate >= '%s'
        )
    """ % (4*(secids, date))
    if show_cmd:
        print(cmd)
    relevant_secids = conn.frame_query(cmd)['secid'].values
    return pos_table.loc[pos_table['secid'].isin(relevant_secids)]


if __name__ == '__main__':
    cfg_path = os.path.join(os.environ['GLT_MASTER_PATH'], 'cfg')
    mysql_cfg_filename = os.path.join(cfg_path, glt.db.CONFIG_BASENAME)
    mysql_config = glt.db.read_mysql_config(mysql_cfg_filename)

    tdate, prod = sys.argv[1:]

    script_cfg_filename = os.path.join(cfg_path, 'deio_eod.json')
    param = read_config(script_cfg_filename, prod)
    user_pos = param['user_pos']
    user_fill = param['user_fill']
    pos_table = param['pos_table']
    fill_table = param['fill_table']

    for user in [user_pos, user_fill]:
        if user not in mysql_config:
            print('%s does not exist in %s' % (user, mysql_cfg_filename))
            exit(1)

    real_execute = user_pos != 'rdeio'

    today = pd.to_datetime(tdate, format='%Y%m%d').date()

    if today.weekday() in (5, 6):
        raise ValueError('date=%s is not a work day. quitting')

    # establish connection
    conn_pos, conn_fill = None, None
    try:
        conn_pos = glt.db.MysqlConnection(**mysql_config[user_pos])
        conn_fill = glt.db.MysqlConnection(**mysql_config[user_fill])

        # compare max(tradedate) found in pos_table to what we think
        # yesterday's date is
        max_date = find_max_date(conn_pos, pos_table, prod)
        yday = find_yesterday(today)
        if yday != max_date:
            print('yday != max_date (%s != %s). quitting' % (yday, max_date))
            exit(1)

        pos = query_positions(conn_pos, yday, prod, pos_table)

        # remove expired contracts
        pos = not_expired(conn_pos, pos, today)

        fills = query_fills(conn_fill, today, prod)

        print(pos.to_string())
        print(fills.to_string())

        # add fill position to last positions
        pos = pd.merge(pos, fills, how='outer', on='secid').fillna(0)
        pos = pos.sort_values('secid')
        for col in ['position', 'volume', 'filled']:
            pos[col] = pos[col].round().astype(int)
        pos['eod'] = pos['position'] + pos['filled']
        print(pos.to_string())
        #pos = pos.loc[pos['eod'] != 0]

        # delete current contents of pos_table and fill_table
        delete_table_contents(conn_pos, pos_table, today, prod, real_delete=real_execute)
        delete_table_contents(conn_pos, fill_table, today, prod, real_delete=real_execute)

        # insert into pos_table
        for row in zip(*(pos[col] for col in ['secid', 'eod'])):
            insert_pos_data(conn_pos, pos_table, today, prod, row, real_insert=real_execute)

        # insert into fill_table
        for row in zip(*(fills[col] for col in fills)):
            insert_fill_data(conn_pos, fill_table, today, prod, row, real_insert=real_execute)

        # and finally commit
        conn_pos.commit()

    finally:
        if conn_pos is not None:
            print('closing connection')
            conn_pos.close()
        else:
            print('connection non-existent')
        if conn_fill is not None:
            print('closing connection')
            conn_fill.close()
        else:
            print('connection non-existent')