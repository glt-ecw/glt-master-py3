
import os
import sys
import numpy as np
import pandas as pd
import datetime as dt
import re
import glob

#print('numpy', np.version.version)
#print('pandas', pd.version.version)

from glt import tools, manuscraper as nkm, manuscraper_beta as nkmb
from combine_nk_cke import combine_nk

################################################
### Identify training data - good and bad


##############################################
### INPUTS
datestr = sys.argv[1]
date = '%s-%s-%s'%(datestr[:4],datestr[4:6],datestr[6:])
##############################################

nkdf = combine_nk(datestr)
if nkdf is None:
    print("Issue found while building md book. Skipping book")
    sys.exit(0)

turnqty, turntick = 50, 10 ## Could modify so you record all turnqtys and filter later?
time_to_turn = 500 #ms

turn_cols = 'turn_direction', 'turn_price', 'prev_tick'
for col in turn_cols:
    if col in nkdf.columns:
        del nkdf[col]

nkdf = nkdf.join(tools.find_turns_with_previous_tick(nkdf, turnqty, turntick))
nkdf = nkdf.rename(columns={x:'prev_' + x for x in ('turn_direction','turn_price')})

### Find hard turns, weakqtymultiplier, from kak's prev
weak_qty_multiplier = 1
turn_data = nkm.find_turns(nkdf, turnqty, 10)
weak_turn_data = nkm.find_weak_turns(nkdf, turnqty, weak_qty_multiplier)
nkdf['turn_direction'] = turn_data[:, 0] + weak_turn_data[:, 0]
nkdf['turn_price'] = turn_data[:, 1] + weak_turn_data[:, 1]
additional = nkm.additional_turn(nkdf, 1)
nkdf['additional_turn_direction'] = additional[:, 0]
nkdf['additional_turn_ahead'] = additional[:, 1]
nkdf['additional_turn_price'] = additional[:, 2]
nkdf['direction'] = nkdf['turn_direction']

nkdf['sweeping'] = 0
nkdf.loc[nkdf['bidprc0']>nkdf['askprc0'], 'sweeping'] = 1

### Load Avol eventx
#avol_file='/glt/storage/txt/parsed_pcap/nkavol/trades.nkavol.mnk.%s.csv.gz'%datestr
avol_file='/glt/ssd/csv/trades.nkavol.mnk.%s.csv'%datestr

if not os.path.isfile(avol_file):
    print("No avol file found for %s"%datestr)
    sys.exit(0)
    
avol_events = pd.read_csv(avol_file, index_col=0)

pos_buffer_pct=0.20 #lean on 120% of working pos
avol_events['pos_buffer'] = ((1.0 + pos_buffer_pct) * avol_events['tradeqty_mnk']).astype(int)
buy_bool = avol_events['trade_nk'] == 1
avol_events.loc[buy_bool, 'tradeprc_even'] = (10*np.ceil(0.1*avol_events.loc[buy_bool, 'tradeprc_mnk'].values)).round().astype(np.int64)
avol_events.loc[~buy_bool, 'tradeprc_even'] = (10*np.floor(0.1*avol_events.loc[~buy_bool, 'tradeprc_mnk'].values)).round().astype(np.int64)
avol_events['t_st'] = (pd.to_datetime(avol_events['exch_ts_nk']) + pd.Timedelta(1, unit='ms')).astype(int)
avol_events['next_avol_event'] = avol_events['t_st'].shift(-1)
avol_events['next_avol_direction'] = avol_events['trade_nk'].shift(-1)

avol_cols=['exch_ts_nk','trade_nk','tradeprc_mnk','tradeprc_even','tradeqty_mnk','pos_buffer','t_st']
avol_trades = avol_events.loc[(avol_events.exch_ts_nk.between('%s 09:00:05'%date, '%s 15:00:00'%date))&(avol_events.dt<2000), avol_cols].drop_duplicates('exch_ts_nk')

nkdf['score'] = 0
#['exch_ts_nk','trade_nk','tradeprc_mnk','tradeprc_even','tradeqty_mnk','pos_buffer','t_st','next_avol_event','next_avol_direction']
next_avol_ts = avol_trades.t_st.values
next_avol_directions = avol_trades.trade_nk.values
scores=[]

for (t, direction, price, evenprice, qty, minleanqty, t_st) in avol_trades.values:
    nkdf_slice=nkdf.loc[nkdf.time > pd.to_datetime(t_st)]
    next_avol_tts = next_avol_ts[next_avol_directions!=direction]
    next_avol_tts = next_avol_tts[next_avol_tts > t_st]

    next_avol_t = next_avol_tts[0] if len(next_avol_tts) else nkdf_slice.timestamp.values[-1]

    score_array = nkm.score_trades_getout(nkdf_slice, evenprice, minleanqty, direction, next_avol_t)
    if not score_array:
        print(t, direction, price, evenprice, qty, minleanqty, t_st) 
    score = pd.DataFrame(score_array, columns=['ix','triggerid','t0','t1','getout_price'])
    score['next_avol_t'] = pd.to_datetime(next_avol_t)
    score['direction'] = direction
    score['price'] = price
    score['evenprice'] = evenprice
    score['qty'] = qty
    score['minleanqty'] = minleanqty
    score['score'] = (score['t1'] - score['t0'])
    score['score'] = score['score'].values.astype(int)
    scores.append(score.drop_duplicates('t1'))

positive_scores = pd.DataFrame().append(scores)
positive_scores['triggerid'] = positive_scores['triggerid'].astype(int)
print('positive_trades', positive_scores.shape)
### isolate only volume events (manuscraper triggers) from nkdf
scores_array=[]
nkdf['score'] = np.NaN

for (triggerid, t0, t1, direction, price, evenprice, qty, minleanqty, getout_price, next_avol_t) in positive_scores.loc[:, ['triggerid','t0','t1','direction','price','evenprice','qty','minleanqty','getout_price','next_avol_t']].values:
    #print(triggerid, t0, t1, direction, price, evenprice, qty, minleanqty, getout_price, next_avol_t)
    trade_slice = nkdf.loc[nkdf.timestamp.between(t0, t1)]
    trade_slice.loc[np.logical_or(trade_slice.buyvolume!=0, trade_slice.sellvolume!=0), 'score'] = 0 ## only include markets with volume events
    trade_slice.loc[(trade_slice.timestamp==t1)&(trade_slice.triggerid==triggerid), 'score'] = 1
    trade_slice.loc[:, 'direction'] = direction
    trade_slice.loc[:, 'triggerid'] = triggerid
    trade_slice.loc[:, 'price'] = price
    trade_slice.loc[:, 'evenprice'] = evenprice
    trade_slice.loc[:, 'qty'] = qty
    trade_slice.loc[:, 'minleanqty'] = minleanqty
    trade_slice.loc[:, 'getout_price'] = getout_price
    trade_slice.loc[:, 'next_avol_t'] = next_avol_t
    #trade_slice.dropna(subset=['score'], inplace=True)
    scores_array.append(trade_slice)
    
    
allscores = pd.DataFrame().append(scores_array)
print("#pos events: ", allscores.loc[allscores.score==1].shape)
print("#neg events: ", allscores.loc[(allscores.score==0)&(np.logical_or(allscores.buyvolume!=0, allscores.sellvolume!=0))].shape)

"""
#####
## Filtering training data
#####
min_streak_qty = 0
min_streak_ms = 0
min_trade_size = 0
buys = trigger_candidates_by_side(nkdf, 'buy', min_streak_ms, min_streak_qty, min_trade_size, 0, False)
buys['side'] = 'buy'
sells = trigger_candidates_by_side(nkdf, 'sell', min_streak_ms, min_streak_qty, min_trade_size, 0, False)
sells['side'] = 'sell'
#all_trades = pd.concat([buys, sells])

print(datestr, dt.datetime.now(), 'buys')
print(buys['score'].value_counts())
print(buys['score_hard'].value_counts())
print(datestr, dt.datetime.now(), 'sells')
print(sells['score'].value_counts())
print(sells['score_hard'].value_counts())
"""

filename = '.'.join([datestr, 'csv'])
#filepath = '/glt/storage/results/manuscraper/training_data/20161108_getout_jpaonly/%s'%filename
filepath = '/glt/storage/results/manuscraper/training_data/20161215_getout_jpaonly/%s'%filename
allscores.to_csv(filepath)
os.system('gzip %s'%filepath)

