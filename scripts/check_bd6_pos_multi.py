import pandas as pd
import numpy as np
import random
import sys, os
import datetime
import time
import argparse
import csv
import collections, itertools
import os
import subprocess
import glob
import glt.db as db
from struct_converter import apply_fill_struct
import smtplib
import base64
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

## Suppress warnings ##
import warnings
warnings.filterwarnings('ignore')
from io import StringIO as cStringIO
import io

old_stdout = sys.stdout
StringCatcher = io.StringIO()
sys.stdout = StringCatcher

weight_dict = {
    "MNK": 10,
    "SNK": 2,
    "NK" : 1,
    "NK4": 1,
    "TP" : 1,
    "MTP": 10,
    "JGBL":1,
    "SMJBL":10, 
    "SJGBL":10, 
    "SXU":1,
    "STW" : 1,
}

contract_categories = {
    "MNK": "NK",
    "SNK": "NK",
    "NK" : "NK",
    "NK4": "NK4",
    "TPX": "TP",
    "TP" : "TP",
    "MTP": "TP",
    "JGBL":"JGB",
    "SMJBL":"JGB",
    "SJGBL":"JGB",
    "STW":"STW" 
}

DBSERVER01_CONN = {
    'host': '10.125.0.200',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'DEIO_JPA'
}

SEC_CONN = {
    'host': '10.125.0.200',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'SECURITIES'
}

def find_secids(secids):
    securities_str = str(tuple(secids)) if len(secids) > 1 else str("(%d)"%secids[0])    
    try:
        sec = db.conn(**SEC_CONN)
        security_info = sec.frame_query("select shortName, exchangeId, id from SECURITIES.securities where id in %s" %securities_str)  #
        security_info.columns = ['shortName','symbol','securityId']
        shortnames = security_info[security_info.securityId.isin(secids)]
    finally:
        sec.close()    
    #print shortnames
    return shortnames

def float2int(x):
    return int(np.round(x*100.))


def sendEmail(toList,msgSubject,message):
        
    if str(type(toList)) == "<type 'str'>":
        toList = [toList]

    msg = MIMEMultipart()
    msg['From'] = 'C.K. Eun <myaccount@gmail.com>'
    msg['Subject'] = msgSubject
    
    msg.attach(MIMEText(message,'html'))        

    server = smtplib.SMTP()
    server.connect('smtp.gmail.com',587)
    server.ehlo()
    server.starttls()
    server.login('cke@glt-llc.com','nwdOxxqa')    
    server.sendmail('cke@glt-llc.com', toList, msg.as_string())
    server.close()   

def find_sec2series():
    sec2seriesmap = pd.read_csv('/glt/storage/scratch/series2secId.map',engine='c')
    sec2series_dict = dict(list(zip(sec2seriesmap.secId.values, sec2seriesmap.series.values)))
    series2secId_dict = dict(list(zip(sec2seriesmap.series.values, sec2seriesmap.secId.values)))
    series2shortName_dict = dict(list(zip(sec2seriesmap.series.values, sec2seriesmap.shortName.values)))
    sec2shortName_dict = dict(list(zip(sec2seriesmap.secId.values, sec2seriesmap.shortName.values)))

    return sec2series_dict, series2secId_dict, series2shortName_dict, sec2shortName_dict

#############
#############

## Suppress warnings ##
#import warnings
#warnings.filterwarnings('ignore')
#####################################
## Format database numbers ##
pd.options.display.float_format = '{:.2f}'.format
#####################################
email_list=None
## Settings ##
datestr = sys.argv[1]

date_db = datestr[:4]+'-'+datestr[4:6]+'-'+datestr[6:8]
print("Date: ", datestr, "\n")

if len(sys.argv) > 3:
    datestr1 = sys.argv[2]
    date_db1 = datestr1[:4]+'-'+datestr1[4:6]+'-'+datestr1[6:8]
    session = 1
    email_list = sys.argv[3]
    print(date_db, date_db1)
elif len(sys.argv) == 3:
    email_list = sys.argv[2]
    date_db1 = date_db
    session = 0
else:
    date_db1 = date_db
    session = 0

session_dict = {
    0: [date_db + ' 07:45:00', date_db + ' 15:30:00'], 
    1: [date_db + ' 15:30:00', date_db1 + ' 03:30:00']
               }
#st = date_db + ' 09:00:00'
#et = date_db + ' 15:30:00'

st, et = session_dict[session]


##PCAP RECONCILE

try:
    conn = db.conn(**DBSERVER01_CONN)
    qwid2secId = conn.frame_query(("select distinct id, exchOrderId, side, securityId from orders where tradeDate = '%s' and exchOrderId != ''")%date_db)
    qwid2secId['qwid'] = ["%s"%quad for quad in qwid2secId.exchOrderId.values]
finally:
    conn.close()

qwid2secId_dict = dict(list(zip(qwid2secId.qwid.values, qwid2secId.id.values)))

series2secmap = pd.read_csv('/glt/storage/scratch/series2secId.map').drop_duplicates()

fill_files = glob.glob('/glt/storage/txt/fills/*%s.*.csv'%datestr)
fills = [] 
for fill_file in fill_files:
    with open(fill_file) as f:
        content = f.readlines()
    for line in content:
        #if "BD" in line:
        datestr, timestamp, msg_t, pktlen, msg = line.split(',')
        msg_t, parsed_msg = apply_fill_struct(msg)
        fills.append([datestr, timestamp, msg_t, pktlen] + parsed_msg.split(", "))
#fill_cols = ['datestr', 'time', 'msg_t', 'pktlen', 'deal_no', 'qwid', 's01', 's02', 's03', 's04', 's05', 's06', 's07', 'price', 'qty', 'side', 'exch_info',""]
fill_cols = ['datestr', 'time', 'msg_t', 'pktlen', 'deal_no', 'qwid', 'series', 'price', 'qty', 'side', 'exch_info', ""]
allfills = pd.DataFrame(fills, columns = fill_cols)

#allfills['series'] = ['%s,%s,%s,%s,%s,%s,%s'%(s0,s1,s2,s3,s4,s5,s6) for [s0,s1,s2,s3,s4,s5,s6] in allfills.loc[:, ['s01', 's02', 's03', 's04', 's05', 's06', 's07']].values]
allfills['deioid'] = 1 #allfills['exch_info'].str[5:13].astype(int)
allfills['time'] = pd.to_datetime(allfills.time.values)
allfills['id'] = 1 #[int(exch[5:13]) for exch in allfills.exch_info.values]
allfills['qwid'] = [x.lstrip()[1:-1] for x in allfills.qwid.values]
allfills['qty'] = allfills.qty.values.astype(int)
nkfills = allfills.loc[:, ['id','server','time','price','qty','qwid','series','side','deal_no']].drop_duplicates(['deal_no','qwid'])
exchserver_trades = nkfills.merge(series2secmap, on='series', how='left')
exchserver_trades['securityId'] = exchserver_trades['secId']
exchserver_trades['id'] = [qwid2secId_dict[x] if x in qwid2secId_dict else "wtf" for x in exchserver_trades.qwid.values]
exchserver_trades.to_csv('/glt/storage/txt/fills/%s.fills.txt'%datestr)

if len(exchserver_trades):
    exchserver_trades['dealprice'] = exchserver_trades['price'].astype(int) / 100.
    exchserver_trades['side'] = exchserver_trades['side'].astype(int)
    exchserver_trades["qty"] = exchserver_trades.qty.astype(int)
    exchserver_trades['t'] = exchserver_trades['time'].values.astype('<M8[m]')
    exchserver_trades_relcols = exchserver_trades.loc[:, ['time','shortName','side','qty','dealprice']]
    topix_bool = ["TP" in x for x in exchserver_trades_relcols.shortName.values]
    exchserver_trades_relcols["shortName"][topix_bool] = exchserver_trades_relcols.shortName.str.replace("TPX","TP")
    exchserver_trades_relcols["dealprice"][topix_bool] = exchserver_trades_relcols["dealprice"]
    tw_bool = [("TW" in x) or ("SJGB" in x) for x in exchserver_trades_relcols.shortName.values]
    snk_bool = ["SNK" in x for x in exchserver_trades_relcols.shortName.values]
    nk_bool = [("NK" in x and "SNK" not in x) for x in exchserver_trades_relcols.shortName.values]
    exchserver_trades_relcols["dealprice"][nk_bool] = exchserver_trades_relcols['dealprice'] / 100.
    exchserver_trades_relcols["dealprice"][tw_bool] = exchserver_trades_relcols['dealprice'] * 100.
    trades_t = exchserver_trades_relcols.loc[exchserver_trades_relcols.time.between(st, et)]

    grouped_exch_trades = pd.DataFrame({'tot_qty' : trades_t.groupby(['shortName','side','dealprice'])['qty'].sum()}).reset_index()
    grouped_exch_trades['weight_name'] = [weight_dict[shortName[:-3]] if shortName[:-3] in weight_dict else 1 for shortName in grouped_exch_trades.shortName.values]
    grouped_exch_trades['weight'] = [-1.0 * weight_name if side == 2 else 1.0 * weight_name for side, weight_name in zip(grouped_exch_trades.side.values, grouped_exch_trades.weight_name.values)]
    grouped_exch_trades['position'] = [qtydiff / (1.0*weight) for weight, qtydiff in zip(grouped_exch_trades.weight.values, grouped_exch_trades.tot_qty.values)]


    trades_tplus = exchserver_trades_relcols.loc[exchserver_trades_relcols.time > et]
    grouped_exch_trades_plus = pd.DataFrame({'tot_qty' : trades_tplus.groupby(['shortName','side','dealprice'])['qty'].sum()}).reset_index()
    grouped_exch_trades_plus['weight_name'] = [weight_dict[shortName[:-3]] if shortName[:-3] in weight_dict else 1 for shortName in grouped_exch_trades_plus.shortName.values]
    grouped_exch_trades_plus['weight'] = [-1.0 * weight_name if side == 2 else 1.0 * weight_name for side, weight_name in zip(grouped_exch_trades_plus.side.values, grouped_exch_trades_plus.weight_name.values)]
    grouped_exch_trades_plus['position'] = [qtydiff / (1.0*weight) for weight, qtydiff in zip(grouped_exch_trades_plus.weight.values, grouped_exch_trades_plus.tot_qty.values)]

else:
    grouped_exch_trades=pd.DataFrame()
    grouped_exch_trades_plus=pd.DataFrame()

###################################################ei
####Grab trades from deio db
####################################################

#os.system("/glt/research/bin/get_defs.sh")
try:
    conn = db.conn(**DBSERVER01_CONN)
    jpx_trades = conn.frame_query(("select * from fills where price != 0 and exchTradeId != '' and fillType != 2 and tradeDate = '%s'")%(date_db))
    #jpx_trades = conn.frame_query(("select * from fills where price != 0 and exchTradeId != '' and fillType != 2 and tradeDate = '%s' and fillTime between '%s' and '%s'")%(date_db, st, et))
    #jpx_trades = conn.frame_query(("select * from fills where price != 0 and fillType != 2 and tradeDate = '%s' and fillTime between '%s' and '%s'")%(date_db, st, et))

finally:
    conn.close()

deio_trades = jpx_trades.loc[:, ['id','securityId','side','qty','price','fillTime','usFillTime']]
if len(deio_trades):
    secids = deio_trades.securityId.unique()
    secid_db = find_secids(secids)
    df_deio = pd.merge(jpx_trades, secid_db, left_on='securityId', right_on='securityId', how='left')
    
    df_deio['dealprice'] = df_deio['price']
    convert_float_bool = df_deio.shortName.str[:-3].isin(["SJGBL","SMJBL", "JGBL", "TP", "MTP", "SXU", "STW"])
    df_deio['dealprice'][convert_float_bool] = df_deio.price.apply(float2int) #map(butthole, df_deio.price.values) #[int(x*100) if x % 1 > 0 else int(x) for x in df_deio.price.values]
    mini_jgbs = df_deio.shortName.str[:-3].isin(["SJGBL","SMJBL"])
    df_deio['dealprice'][mini_jgbs] = df_deio.dealprice.values * 10
    df_deio['dealprice'] = df_deio['dealprice'].values.astype(int)
    df_deio['qty'] = df_deio.qty.astype(int)
    
    df_deio['t'] = df_deio['fillTime'].values.astype('<M8[m]')
    
    df_deio_t = df_deio.loc[df_deio['fillTime'].between(st, et)]
    df_deio_tplus = df_deio.loc[df_deio['fillTime'] > et]

    grouped_df = pd.DataFrame({'tot_qty' : df_deio_t.groupby(['shortName','side','dealprice'])['qty'].sum()}).reset_index()
    grouped_df['weight_name'] = [weight_dict[shortName[:-3]] if shortName[:-3] in weight_dict else 1 for shortName in grouped_df.shortName.values]
    grouped_df['weight'] = [-1.0 * weight_name if side == 2 else 1.0 * weight_name for side, weight_name in zip(grouped_df.side.values, grouped_df.weight_name.values)]
    grouped_df['position'] = [qtydiff / (1.0* weight) for weight, qtydiff in zip(grouped_df.weight.values, grouped_df.tot_qty.values)]

    grouped_df_plus = pd.DataFrame({'tot_qty' : df_deio_tplus.groupby(['shortName','side','dealprice'])['qty'].sum()}).reset_index()
    grouped_df_plus['weight_name'] = [weight_dict[shortName[:-3]] if shortName[:-3] in weight_dict else 1 for shortName in grouped_df_plus.shortName.values]
    grouped_df_plus['weight'] = [-1.0 * weight_name if side == 2 else 1.0 * weight_name for side, weight_name in zip(grouped_df_plus.side.values, grouped_df_plus.weight_name.values)]
    grouped_df_plus['position'] = [qtydiff / (1.0* weight) for weight, qtydiff in zip(grouped_df_plus.weight.values, grouped_df_plus.tot_qty.values)]

else:
    grouped_df = pd.DataFrame()
    grouped_df_plus = pd.DataFrame()

empty_df_bool = not len(grouped_df) or not len(grouped_exch_trades)
empty_df_plus_bool = not len(grouped_df_plus) or not len(grouped_exch_trades_plus)

if empty_df_bool and empty_df_plus_bool: # no trades for T or T+1 session
    if len(grouped_exch_trades) < 1:
        print("No fills found in bd6.\n")
    elif len(grouped_df) < 1:
        print("No fills found in deio\n")
    else:
        print("Something broke, stupid.")

elif empty_df_bool and not empty_df_plus_bool: # only T+1 session
    print("T+1 Session\n")

    final_plus= grouped_df_plus.merge(grouped_exch_trades_plus, suffixes=('_deio','_bd6'), how='outer', on=["shortName","side","dealprice"]).fillna(0)
    final_plus['qtydiff'] = final_plus['position_deio'].astype(float) - final_plus['position_bd6'].astype(float)
    final_plus['contract_category'] = [contract_categories[x[:-3]] if x[:-3] in contract_categories else "" for x in final_plus.shortName.values]

    print("Net buys & sells for deio: ")
    net_deio = pd.DataFrame({'net' : final_plus.groupby(['side']).position_deio.sum()}).reset_index()
    print(net_deio.to_string(), "\n")

    print("Total buys & sells deio: \n", final_plus.groupby(['shortName','side']).position_deio.sum().reset_index(), "\n")

    print("Overall position by contract: \n", final_plus.groupby(['contract_category']).position_deio.sum(), "\n")
    print("Overall position deio: \n", final_plus.position_deio.sum(), "\n")

    print()

    print("Net buys & sells for bd6: ")
    net_bd6 = pd.DataFrame({'net' : final_plus.groupby(['side']).position_bd6.sum()}).reset_index()
    print(net_bd6.to_string(), "\n")
    print("Total buys & sells bd6: \n", final_plus.groupby(['shortName','side']).position_bd6.sum().reset_index(),"\n")

    print("Overall position by contract: \n", final_plus.groupby(['contract_category']).position_bd6.sum(), "\n")
    print("Overall position bd6: \n", final_plus.position_bd6.sum(), "\n")

    missing = final_plus[final_plus['qtydiff'] !=0]
    print("Difference in fills by security, side, volume, & price. \n")
    print(missing[["shortName","side","dealprice","position_deio","position_bd6","qtydiff"]].to_string(), "\n\n")
    unique_missing_bd6 = []
    unique_missing_deio = []


else: #both T and T+1 session
    final= grouped_df.merge(grouped_exch_trades, suffixes=('_deio','_bd6'), how='outer', on=["shortName","side","dealprice"]).fillna(0)
    final['qtydiff'] = final['position_deio'].astype(float) - final['position_bd6'].astype(float)
    final['contract_category'] = [contract_categories[x[:-3]] if x[:-3] in contract_categories else "" for x in final.shortName.values]

    print("Net buys & sells for deio: ")
    net_deio = pd.DataFrame({'net' : final.groupby(['side']).position_deio.sum()}).reset_index()
    print(net_deio.to_string(), "\n")

    print("Total buys & sells deio: \n", final.groupby(['shortName','side']).position_deio.sum().reset_index(), "\n")

    print("Overall position by contract: \n", final.groupby(['contract_category']).position_deio.sum(), "\n")
    print("Overall position deio: \n", final.position_deio.sum(), "\n")

    print()

    print("Net buys & sells for bd6: ")
    net_bd6 = pd.DataFrame({'net' : final.groupby(['side']).position_bd6.sum()}).reset_index()
    print(net_bd6.to_string(), "\n")
    print("Total buys & sells bd6: \n", final.groupby(['shortName','side']).position_bd6.sum().reset_index(),"\n")

    print("Overall position by contract: \n", final.groupby(['contract_category']).position_bd6.sum(), "\n")
    print("Overall position bd6: \n", final.position_bd6.sum(), "\n")

    missing = final[final['qtydiff'] !=0]
    print("Difference in fills by security, side, volume, & price. \n")
    print(missing[["shortName","side","dealprice","position_deio","position_bd6","qtydiff"]].to_string(), "\n\n")
    unique_missing_bd6 = []
    unique_missing_deio = []


    print("T+1 Session\n")

    final_plus= grouped_df_plus.merge(grouped_exch_trades_plus, suffixes=('_deio','_bd6'), how='outer', on=["shortName","side","dealprice"]).fillna(0)
    final_plus['qtydiff'] = final_plus['position_deio'].astype(float) - final_plus['position_bd6'].astype(float)
    final_plus['contract_category'] = [contract_categories[x[:-3]] if x[:-3] in contract_categories else "" for x in final_plus.shortName.values]

    print("Net buys & sells for deio: ")
    net_deio = pd.DataFrame({'net' : final_plus.groupby(['side']).position_deio.sum()}).reset_index()
    print(net_deio.to_string(), "\n")

    print("Total buys & sells deio: \n", final_plus.groupby(['shortName','side']).position_deio.sum().reset_index(), "\n")

    print("Overall position by contract: \n", final_plus.groupby(['contract_category']).position_deio.sum(), "\n")
    print("Overall position deio: \n", final_plus.position_deio.sum(), "\n")

    print()

    print("Net buys & sells for bd6: ")
    net_bd6 = pd.DataFrame({'net' : final_plus.groupby(['side']).position_bd6.sum()}).reset_index()
    print(net_bd6.to_string(), "\n")
    print("Total buys & sells bd6: \n", final_plus.groupby(['shortName','side']).position_bd6.sum().reset_index(),"\n")

    print("Overall position by contract: \n", final_plus.groupby(['contract_category']).position_bd6.sum(), "\n")
    print("Overall position bd6: \n", final_plus.position_bd6.sum(), "\n")

    missing = final_plus[final_plus['qtydiff'] !=0]
    print("Difference in fills by security, side, volume, & price. \n")
    print(missing[["shortName","side","dealprice","position_deio","position_bd6","qtydiff"]].to_string(), "\n\n")
    unique_missing_bd6 = []
    unique_missing_deio = []

toList = []
if email_list is not None:
    toList.append(email_list)
else:
    toList.append('cke@glt-llc.com') 

toList = ['cke@glt-llc.com']
#toList = ['perftimes@glt-llc.com']
sys.stdout = old_stdout
output = StringCatcher.getvalue()

sendEmail(toList,"JPX Position check","<pre>" + output + "</pre>")

