import pandas as pd
import numpy as np
import random
import sys, os
import glt.db as db

DBSERVER01_CONN = {
    'host': '10.125.0.200',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'DEIO_JPA'
}

SEC_CONN = {
    'host': '10.125.0.200',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'SECURITIES'
}

SERIES2SECID_FILENAME = '/glt/storage/scratch/series2secId.map'

def find_secids(secids):
    try:
        sec = db.conn(**SEC_CONN)
        security_info = sec.frame_query(("select shortName, exchangeId, id from SECURITIES.securities where id in %s")  % str(tuple(secids))) #
        security_info.columns = ['shortName','symbol','securityId']
        shortnames = security_info[security_info.securityId.isin(secids)]
    finally:
        sec.close()    

    return shortnames

def stringify(row):
    return "[" + ','.join(map(str, row)) + "]"
    
def add_series(series_list):
    return series_list.iloc[:, 1:8].apply(stringify, axis=1)


datestr = sys.argv[1]
date_db = datestr[:4]+'-'+datestr[4:6]+'-'+datestr[6:8]

try:
    conn = db.conn(**DBSERVER01_CONN)
    unique_secIds = conn.frame_query(("select distinct securityId from orders where tradeDate = '%s'") %date_db)
    
    
finally:
    conn.close()

secId_defs = find_secids(unique_secIds.securityId.values)

try:
    jpx_spreads_list = pd.read_csv('/glt/storage/scratch/jpa_spread_defs.csv', skiprows=2, engine='c', error_bad_lines=False, warn_bad_lines=True, header=None)
    jpx_spreads_list.columns = ['symbol','1','2','3','4','5','6','7','8','9','10','11','12','13','14']
    jpx_spreads_list['series'] = add_series(jpx_spreads_list)
except:
    jpx_spreads_list = pd.DataFrame()

try:
    sgx_spreads_list = pd.read_csv('/glt/storage/scratch/sgx_spread_defs.csv', skiprows=2, engine='c', error_bad_lines=False, warn_bad_lines=True, header=None)
    sgx_spreads_list.columns = ['symbol','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15']
    sgx_spreads_list['series'] = add_series(sgx_spreads_list)
except:
    sgx_spreads_list = pd.DataFrame()

spreads_list = pd.concat([jpx_spreads_list, sgx_spreads_list])
spreads_list = spreads_list.loc[:, ['symbol','series']]

try:
    #jpx_series_list = pd.read_csv('/glt/storage/scratch/jpxInstDefs.csv', skiprows=2, engine='c', error_bad_lines=False, warn_bad_lines=True, header=None)
    jpx_series_list = pd.read_csv('/glt/storage/scratch/jpaInstDefs.csv', skiprows=2, engine='c', error_bad_lines=False, warn_bad_lines=True, header=None)
except:
    jpx_series_list = pd.DataFrame()
    
try:
    sgx_series_list = pd.read_csv('/glt/storage/scratch/sgxInstDefs.csv', skiprows=2, engine='c', error_bad_lines=False, warn_bad_lines=True, header=None)
except:
    sgx_series_list = pd.DataFrame()
    
series_list = pd.concat([jpx_series_list, sgx_series_list], axis=0)
if len(series_list):
    series_list.columns = ['symbol','1','2','3','4','5','6','7','8','9','10']
    series_list['series'] = add_series(series_list)
    series_list = series_list.loc[:, ['symbol','series']]
else:
    series_list = pd.DataFrame()

series_list = pd.concat([series_list, spreads_list])
#print series_list.loc[series_list.symbol.str[:7]=='FUT_JGB'].to_string()


existing_contracts = pd.read_csv(SERIES2SECID_FILENAME, engine='c')
print(existing_contracts.tail(20))



for shortName, symbol, secid in secId_defs.values:
    if shortName not in existing_contracts.shortName.values:
        new_line = series_list.loc[series_list.symbol == symbol]
        series = new_line.series.values[0] if len(new_line) else ""
        #add line
        with open(SERIES2SECID_FILENAME, "a") as myfile:
            myfile.write(("\"%s\",\"%s\",%d\n") %(shortName, series, secid))   



