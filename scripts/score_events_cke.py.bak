from __future__ import print_function
import os
import sys
import numpy as np
import pandas as pd
import datetime as dt
import re
import glob

print('numpy', np.version.version)
print('pandas', pd.version.version)

from glt import tools, manuscraper as nkm

from combine_nk_cke import combine_nk

def trigger_candidates_by_side(nkdf, side, streaker_ms, min_streaker_qty=0, min_trade_size=0, before_tick=0, mkt_only=False):
    from numpy import logical_and, logical_or, logical_not, sign
    # set up side variables
    mkt_side = 'bid' if side == 'buy' else 'ask'
    opp_side = 'bid' if side == 'sell' else 'ask'
    opp_mkt = opp_side + 'prc0'
    tradeprc_side = side + 'price'
    volume_side = side + 'volume'
    score_side = side + 'score' + ('_mkt' if mkt_only else '')
    streak_side = 'streak_' + side + 's_' + str(streaker_ms)

    # filter by minimum thresholds
    # Only include buy/sell signals in training data?
    states = nkdf.loc[logical_and(nkdf[score_side].abs()>before_tick, nkdf[volume_side]>min_trade_size)]
    #states = nkdf.loc[np.logical_and(nkdf[volume_side]>min_trade_size, nkdf['acc_%sqty_%dms'%(side, streaker_ms)])]

    states = states.loc[logical_and(states['direction']==0, states['sweeping']==0)]
    states['score'] = sign(states[score_side])
    states.loc[states['score']<0, 'score'] = 0
    states['score_hard'] = sign(states[score_side + '_hard'])
    states.loc[states['score_hard']<0, 'score_hard'] = 0

    # find events
    if side == 'buy':
        trade_bool = states[tradeprc_side]>=states[opp_mkt]
    else:
        trade_bool = states[tradeprc_side]<=states[opp_mkt]

    # separate positives and negatives
    positive_bool = states['score']>0
    pos = states.loc[logical_and(trade_bool, positive_bool)]
    raw_neg = states.loc[logical_and(logical_not(positive_bool), trade_bool)]

    pos_unique_ticks = pos['unique_tick'].unique()
    neg_bool = nkm.no_shared_unique_ticks(raw_neg, pos_unique_ticks).astype(bool)
    neg = raw_neg[neg_bool]

    # drop duplicates
    nodupe_pos = pos.drop_duplicates('unique_tick', take_last=True)
    nodupe_neg = neg.drop_duplicates('unique_tick', take_last=True)
    return nodupe_pos.append(nodupe_neg).sort('timestamp')

################################################
### Identify training data - good and bad


##############################################
### INPUTS
datestr = sys.argv[1]
##############################################

nkdf = combine_nk(datestr)
if nkdf is None:
    print("Issue found while building md book. Skipping book")
    sys.exit(0)

turnqty, turntick = 200, 10 ## Could modify so you record all turnqtys and filter later?
time_to_turn = 500 #ms

turn_cols = 'turn_direction', 'turn_price', 'prev_tick'
for col in turn_cols:
    if col in nkdf.columns:
        del nkdf[col]

nkdf = nkdf.join(tools.find_turns_with_previous_tick(nkdf, turnqty, turntick))
nkdf = nkdf.rename(columns={x:'prev_' + x for x in ('turn_direction','turn_price')})

### Find hard turns, weakqtymultiplier, from kak's prev
weak_qty_multiplier = 2
turn_data = nkm.find_turns(nkdf, turnqty, 5)
weak_turn_data = nkm.find_weak_turns(nkdf, turnqty, weak_qty_multiplier)
nkdf['turn_direction'] = turn_data[:, 0] + weak_turn_data[:, 0]
nkdf['turn_price'] = turn_data[:, 1] + weak_turn_data[:, 1]
additional = nkm.additional_turn(nkdf, 1)
nkdf['additional_turn_direction'] = additional[:, 0]
nkdf['additional_turn_ahead'] = additional[:, 1]
nkdf['additional_turn_price'] = additional[:, 2]
nkdf['direction'] = nkdf['turn_direction']

nkdf['sweeping'] = 0
nkdf.loc[nkdf['bidprc0']>nkdf['askprc0'], 'sweeping'] = 1

ticked_again = True
score_test = nkm.score_trades(nkdf, time_to_turn * 1e6, not ticked_again)
score_test_hard = nkm.score_trades(nkdf, time_to_turn * 1e6, ticked_again)

nkdf['buyscore'] = score_test[:, 0]
nkdf['sellscore'] = score_test[:, 1]
nkdf['buyscore_hard'] = score_test_hard[:, 0]
nkdf['sellscore_hard'] = score_test_hard[:, 1]

#####
## Filtering training data
#####
min_streak_qty = 0
min_streak_ms = 0
min_trade_size = 0
buys = trigger_candidates_by_side(nkdf, 'buy', min_streak_ms, min_streak_qty, min_trade_size, 0, False)
buys['side'] = 'buy'
sells = trigger_candidates_by_side(nkdf, 'sell', min_streak_ms, min_streak_qty, min_trade_size, 0, False)
sells['side'] = 'sell'
#all_trades = pd.concat([buys, sells])

print(datestr, dt.datetime.now(), 'buys')
print(buys['score'].value_counts())
print(buys['score_hard'].value_counts())
print(datestr, dt.datetime.now(), 'sells')
print(sells['score'].value_counts())
print(sells['score_hard'].value_counts())

#filename = "%d.%d.%d.%d.%d.%d.%d.%d.%d.h5" %(weakturnqty, streaker_ms, min_dev, gain, turnqty, turntick, accqty_threshold, time_to_turn, minvolume)

filename = '.'.join([datestr, 'out', 'h5'])
filepath = '/glt/storage/results/manuscraper/training_data/20161114_jpaonly'
print('placing tables in', filename)

#output = pd.HDFStore(filepath + '/' + filename, 'w')
output = pd.HDFStore(filepath + '/' + filename)
output['buys'] = buys
output['sells'] = sells
output['market'] = nkdf
#output["s_%s" %datestr] = all_trades
#output[datestr] = all_trades
print(output.keys())
output.close()
#output['buys'] = buys
#output['sells'] = sells
#output['buys_reset'] = None
#output['sells_reset'] = None
## prev_tick: last tick where book was not crossed and there was a turn
## turn_dt: time from end of market tick to "significant" market turn/turn
##

print(datestr, dt.datetime.now(), 'done')
"""
all_turns = nkdf.ix[nkdf['turn_direction'].values!=0, ['time', 'turn_direction', 'turn_price', 'prev_tick', 'unique_tick']]
all_turns.index = all_turns.prev_tick
turn_times = all_turns.time.astype(int).to_dict()

## calculate time diffs using dictionary of non zero turn directions
nkdf['turn_dt'] = 0
nkdf.loc[nkdf.unique_tick.isin(turn_times), 'turn_dt'] = 1e-6*(nkdf.unique_tick.apply(lambda x: turn_times[x] if x in turn_times else 0) - nkdf['time'].astype(int))

###############################3333333333333
### CUTTING OUT THE BORING SHIT
### HOPEFULLY NOT IMPORTANT
###################################333333333

itercount = 0
ignore_ticks = []

all_turns['tick_before_prev'] = all_turns.unique_tick.shift(1).fillna(0).astype(int)
#print(all_turns.head(100)[['tick_before_prev','prev_tick','time']].to_string())

for idx, row in all_turns.iterrows():
    if itercount == 0:
        itercount += 1
        continue

    start_tick, end_tick = row[['tick_before_prev', 'prev_tick']]
    #print(start_tick, end_tick)
    ignore_ticks.extend(range(start_tick, end_tick))
    #print(start_tick, end_tick)
    itercount += 1

ignore_ticks = np.array(ignore_ticks, dtype=np.int64)

prev_upticks = all_turns.loc[all_turns.turn_direction>0, 'prev_tick']
prev_downticks = all_turns.loc[all_turns.turn_direction<0, 'prev_tick']

under_dt = nkdf.turn_dt.between(1, time_to_turn)

acc_buys = (nkdf['acc_buyqty_%dms'%streaker_ms].values>accqty_threshold) & (nkdf.buyvolume.values>0) & (nkdf.buyprice.values>=nkdf.askprc0.values)
acc_sells = (nkdf['acc_sellqty_%dms'%streaker_ms].values>accqty_threshold) & (nkdf.sellvolume.values>0) & (nkdf.sellprice.values<=nkdf.bidprc0.values)

in_prev_upticks = nkdf.unique_tick.isin(prev_upticks)
in_prev_downticks = nkdf.unique_tick.isin(prev_downticks)
#prev_upticks = all_turns.loc[all_turns.turn_direction>0, 'prev_tick']
#prev_downticks = all_turns.loc[all_turns.turn_direction<0, 'prev_tick']
in_ignore_ticks = nkdf.unique_tick.isin(ignore_ticks)

########################################
### the GOOD shit

good_uptick_sig = nkdf.loc[acc_buys & in_prev_upticks & under_dt].drop_duplicates('time', take_last=True)
### drop duplicates and take first to get the earliest possible signal
good_uptick_sig = good_uptick_sig.drop_duplicates('unique_tick')
good_uptick_sig['tick_direction'] = 1
good_downtick_sig = nkdf.loc[acc_sells & in_prev_downticks & under_dt].drop_duplicates('time', take_last=True)
### drop duplicates and take first to get the earliest possible signal
good_downtick_sig = good_downtick_sig.drop_duplicates('unique_tick')
good_downtick_sig['tick_direction'] = -1

good_signals = pd.concat([good_uptick_sig, good_downtick_sig])
good_signals['score'] = 1
print("No. of positive outcomes: ", good_signals.shape[0])

bad_uptick_sig = nkdf.loc[acc_buys & in_ignore_ticks].drop_duplicates('unique_tick', take_last=False)
bad_uptick_sig['tick_direction'] = 1
bad_downtick_sig = nkdf.loc[acc_sells & in_ignore_ticks].drop_duplicates('unique_tick', take_last=False)
bad_downtick_sig['tick_direction'] = -1

bad_signals = pd.concat([bad_uptick_sig, bad_downtick_sig])
bad_signals['score'] = 0
print("No. of negative outcomes: ", bad_signals.shape[0])

training_data = pd.concat([good_signals, bad_signals])
"""
#results_dir = '/glt/storage/results/manuscraper/training_data/'
#training_data.to_csv("%s%s.%d.%d.%d.%d.%d.%d.%d.%d.csv" %(results_dir, datestr, weakturnqty, streaker_ms, min_dev, gain, turnqty, turntick, accqty_threshold, time_to_turn))

