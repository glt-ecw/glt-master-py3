import sys
import os
import pandas as pd
import numpy as np
import glt.db
import re
import glob
import json

from collections import defaultdict, deque, namedtuple

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

pd.options.mode.chained_assignment = None


pid_search = re.compile(r'DERIV.([0-9]+).log')
port_search = re.compile(r'CONNECTION_PARMS\|.*remotePort\|([0-9]{1,5})')
orderid_search = re.compile(r'T0[78][0-9]{4}.*TCHODR.*([0-9]{10})        X')


def retrieve_krx_sprints(tdate, filename):
    from glt.md.standard import read_krx_csv
    gz_filename = filename + '.gz'
    if os.path.exists(filename):
        book = read_krx_csv(filename, raw=True)
    else:
        book = read_krx_csv(gz_filename, raw=True)
    preopen = book.loc[(book['status'] == 'PreOpen') & (book['msg'] == 'B6')]

    tot_qty_cols = 'tot_bidqty', 'tot_askqty'

    for col in tot_qty_cols:
        diff_col = col + '_diff'
        preopen[diff_col] = preopen.groupby('symbol')[col].diff()
        nulls = preopen[diff_col].isnull()
        preopen.loc[nulls, diff_col] = preopen.loc[nulls, col]
        preopen[diff_col] = preopen[diff_col].round().astype(int)

    sprint_end_time = preopen.loc[(preopen['tot_bidqty_diff'] < 0) | (preopen['tot_askqty_diff'] < 0), 'time'].iloc[0]
    rel_col = [
        'time', 'msg', 'exch_time', 'chan', 'symbol',
        'tot_bidqty_diff', 'tot_askqty_diff',
        'tot_bidpar', 'tot_askpar', 'tradeprc'
    ]
    sprints = preopen.loc[preopen['time'] < sprint_end_time, rel_col]
    after_open_time = pd.to_datetime(tdate + 'T090500')
    a3 = book.loc[(book['time'].between(sprint_end_time, after_open_time)) & (book['msg'] == 'A3'), rel_col]
    a3 = a3.drop_duplicates('symbol', keep='first')
    for col in tot_qty_cols:
        diff_col = col + '_diff'
        a3[diff_col] = a3[diff_col].fillna(0).astype(int)
    return sprints.append(a3).sort_values('exch_time')


def retrieve_our_sprints(tradedate, server):
    cmd = """
        SELECT eventtime, useventtime, id, securityid, side, price, qty
        FROM DEIO_W.orders
        WHERE (
            tradedate='%s'
            AND
            serverid=%d
            AND
            orderstatus=10
            AND
            eventtime<='%s 08:00:00'
        )
    """ % (tradedate, server, tradedate)
    enqueued_orders = glt.db.autoclose_query(cmd)

    if len(enqueued_orders) == 0:
        return None
    
    # check that the orders actually were accepted from the exchange
    cmd = """
        SELECT eventtime, useventtime, id, securityid, side, price, qty
        FROM DEIO_W.orders
        WHERE (
            tradedate='%s'
            AND
            orderstatus=2
            AND
            id in (%s)
        )
    """ % (tradedate, ','.join(map(str, enqueued_orders['id'].unique())))
    accepted_orders = glt.db.autoclose_query(cmd).drop_duplicates('id', keep='first')
    del enqueued_orders
    if len(accepted_orders) == 0:
        return None

    cmd = """
        SELECT id
        FROM DEIO_W.orders
        WHERE (
            tradedate='%s'
            AND
            orderstatus in (3, 6, 9)
            AND
            id in (%s)
            AND
            eventtime<='%s 08:00:00'
        )
    """ % (tradedate, ','.join(map(str, accepted_orders['id'].unique())), tradedate)

    canceled_orders = set(glt.db.autoclose_query(cmd)['id'].unique())
    accepted_orders = accepted_orders.loc[~accepted_orders['id'].isin(canceled_orders)]
    accepted_orders['price'] = accepted_orders['price'].round(2)
    accepted_orders['qty'] = accepted_orders['qty'].round().astype(int)

    cmd = """
        SELECT id as securityid, exchangeid as symbol
        FROM SECURITIES.securities
        WHERE id in (%s)
    """ % (','.join(map(str, accepted_orders['securityid'].unique())),)
    secs = glt.db.autoclose_query(cmd)

    accepted_orders.sort_values('id', inplace=True)
    merged = pd.merge(accepted_orders, secs, on='securityid', how='left')
    return merged.rename(index=merged['id'])


OrderQty = namedtuple('OrderQty', ['orderid', 'side', 'qty'])
OrderPID = namedtuple('OrderPID', ['orderid', 'pid'])


class SprinterTracker:
    def __init__(self):
        self.pid_qtys = defaultdict(deque)
        self.qtys = defaultdict(deque)
        self.orderids = defaultdict(list)
        self.is_unique = defaultdict(bool)
        self.ready = False

    def add_order(self, orderid, pid, side, qty):
        self.orderids[pid].append(OrderQty(orderid, side, qty))

    def remove(self, side, qty, debug=False):
        if not self.ready:
            raise Exception('SprinterTracker has not sorted quantities by pid!')
        if (side, qty) not in self.qtys:
            if debug:
                print('we are here? side=%d, qty=%d' % (side, qty))
            return None
        o = self.qtys[(side, qty)].popleft()
        if not self.qtys[(side, qty)]:
            if debug:
                print('deleting key=(%d, %d) from self.qtys' % (side, qty))
            del self.qtys[(side, qty)]
        if not self.pid_qtys[o.pid]:
            if debug:
                print('deleting pid=%d from self.pid_qtys' % o.pid)
            del self.pid_qtys[o.pid]
        else:
            top = self.pid_qtys[o.pid].popleft()
            if debug:
                print('new top from pid=%d: %s' % (o.pid, top))
            self.qtys[(top.side, top.qty)].append(OrderPID(top.orderid, o.pid))
        return o

    def prepare(self):
        if self.ready:
            return
        for pid, orderids in self.orderids.items():
            orderids.sort(key=lambda x: x.orderid)
            q = deque(orderids)
            self.pid_qtys[pid] = q
            top = q.popleft()
            self.qtys[(top.side, top.qty)].append(OrderPID(top.orderid, pid))
        self.ready = True

    def has(self, side, qty):
        return (side, qty) in self.qtys


def mark_our_orders(sprints, tdate, server, ormsgs_search):
    tradedate = pd.to_datetime(tdate).date()
    enqueued_orders = retrieve_our_sprints(tradedate, server)

    if enqueued_orders is None:
        sprints['our_order'] = 0
        sprints['pid'] = -1
        sprints['our_price'] = 0.0
        return sprints

    orderid_pid_map = find_order_pids(ormsgs_search)
    order_prices = enqueued_orders['price'].to_dict()

    our_orders = {}
    for symbol, orderid, side, qty in zip(*(enqueued_orders[col] for col in ['symbol', 'id', 'side', 'qty'])):
        if symbol not in our_orders:
            our_orders[symbol] = SprinterTracker()
        if orderid not in orderid_pid_map:
            print('concerning... orderid=%d not in orderid_pid_map' % orderid)
            continue
        our_orders[symbol].add_order(orderid, orderid_pid_map[orderid], side, qty)

    for _, tracker in our_orders.items():
        tracker.prepare()

    thats_us = np.zeros(len(sprints),
                        dtype=[('orderid', np.int64), ('pid', np.int64), ('price', np.float64)])
    thats_us['pid'][:] = -1
    for i, (symbol, bidqty, askqty) in enumerate(
            zip(*(sprints[col] for col in ['symbol', 'tot_bidqty_diff', 'tot_askqty_diff']))):
        if bidqty > 0:
            if symbol in our_orders and our_orders[symbol].has(1, bidqty):
                our_bid = our_orders[symbol]
                o = our_bid.remove(1, bidqty)
                thats_us[i]['orderid'] = o.orderid
                thats_us[i]['pid'] = o.pid
                thats_us[i]['price'] = order_prices[o.orderid]
        else:
            if symbol in our_orders and our_orders[symbol].has(2, askqty):
                our_offer = our_orders[symbol]
                o = our_offer.remove(2, askqty)
                thats_us[i]['orderid'] = o.orderid
                thats_us[i]['pid'] = o.pid
                thats_us[i]['price'] = order_prices[o.orderid]

    sprints['our_order'] = thats_us['orderid']
    sprints['pid'] = thats_us['pid']
    sprints['our_price'] = thats_us['price']
    return sprints


def find_order_pids(ormsg_filename_search):
    ormsgs_filenames = glob.glob(ormsg_filename_search)
    all_orderids = {0: -1}
    for filename in ormsgs_filenames:
        pid = int(pid_search.findall(filename)[0])
        with open(filename, 'r') as f:
            output = f.read()
        port = int(port_search.findall(output)[0])
        #orderids = map(lambda x: [int(x), pid], orderid_search.findall(output))
        orderids = [[int(x), port] for x in orderid_search.findall(output)]
        all_orderids.update(orderids)
    return all_orderids


def retrieve_sprints(tdate, book_filename, ormsg_filepath, server=None):
    if server is None:
        raise ValueError('need server value')
    sprints = retrieve_krx_sprints(tdate, book_filename)
    return mark_our_orders(sprints, tdate, server, ormsg_filepath)


def check_increasing_orderids(us):
    pids = us['pid'].unique()
    symbols = us['symbol'].unique()
    pids.sort()

    print('double checking whether order ids are increasing per (pid, symbol) combo')
    pid_errors = 0
    for pid in pids:
        for symbol in symbols:
            orderids = us.loc[(us['pid'] == pid) & (us['symbol'] == symbol), 'our_order'].values
            if len(orderids) == 0:
                continue
            if (np.diff(orderids) < 0).sum() > 0:
                print(pid, symbol, orderids)
                pid_errors += 1
    print('found %d errors' % pid_errors)
    print()


def competitive_contracts_bid(not_us, min_size, how_many):
    print('finding the top %d contracts where bid order size placed >=%d' % (how_many, min_size))
    top_contracts = not_us.loc[not_us['tot_bidqty_diff'] >= min_size].groupby('symbol')['pid'].count().sort_values(
        ascending=False)
    return top_contracts.iloc[:how_many]


if __name__ == '__main__':
    cfg_filename, tdate, output_filename = sys.argv[1:]

    with open(cfg_filename, 'r') as f:
        param = json.load(f)

    cxl_param = param['canceling']
    server = cxl_param['serverID']
    book_filename = cxl_param['bookFilenameFormat'] % tdate
    ormsg_filename_search = cxl_param['ORMsgFilenameSearchString'] % tdate
    sprints_filename = cxl_param['sprintersEveryoneFilenameFormat'] % tdate
    us_filename = cxl_param['sprintersUsFilenameFormat'] % tdate

    sprints = retrieve_sprints(tdate, book_filename, ormsg_filename_search, server=server)
    sprints['total_orders'] = np.arange(len(sprints)) + 1
    sprints['side'] = np.where(sprints['tot_bidqty_diff'] > 0, 'bid', 'offer')

    # the market
    not_us = sprints.loc[sprints['our_order'] <= 0]

    # us
    us = sprints.loc[sprints['our_order'] > 0]
    us['place'] = np.where(us['tot_bidqty_diff'] > 0, us['tot_bidpar'], us['tot_askpar'])
    us['true_place'] = us['place'] - us.groupby(['symbol', 'side']).cumcount()

    best_orders = us.groupby(['symbol', 'side', 'our_price'])[['our_order', 'pid']].nth(0).reset_index().sort_values(
        ['pid', 'our_order'])

    redundant = us.loc[~us['our_order'].isin(best_orders['our_order'])]
    #redundant.head()

    # let's generate some useful info about our sprinters...
    num_pids = len(us['pid'].unique())
    us['pid_idx'] = us.groupby(['side', 'pid']).cumcount() + 1
    us['order_count'] = np.arange(len(us)) + 1
    us['pid_order_count'] = us.groupby('pid').cumcount() + 1
    us['chan_order_count'] = us.groupby(['chan', 'pid']).cumcount() + 1

    sprints.to_csv(sprints_filename)
    us.to_csv(us_filename)

    with open(output_filename, 'w') as f:
        f.write('\n'.join(map(str, redundant['our_order'].values)))
        print('written file: %s' % output_filename)
    print('done')
