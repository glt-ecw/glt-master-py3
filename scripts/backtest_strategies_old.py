import sys
import json
import datetime as dt
from pandas import to_datetime, to_timedelta

from glt.md.standard import MDSource, which_md
from glt.backtesting import MatchingEngine


config_keys = [
    'matchingEngines',
    'strategies'
]

matching_engine_config_keys = [
    'secids',
    'assumptions'
]

matching_engine_config_time_keys = [
    'timeZoneOffset',
    'start',
    'end'
]

matching_engine_config_assumptions_keys = [
    'orderEntryLatency',
    'lookForwardTime',
    'useExchangeTime'
]

strategy_config_keys = [
    'objectName',
    'parameters'
]

hindsight_config_keys = [
    'lookForwardTime'
]

matching_engine_attributes = [
    'latency',
    #'assume_all_filled',
    #'orders',
    #'matches',
    #'misses'
]

strategy_attributes = [
    'valid_md',
    'evaluate_md',
    'has_orders',
    'place_order',
    'remove_order'
]


def check_attr(obj, attr):
    return 'yes' if hasattr(obj, attr) else 'no'


def now():
    return '%s:' % dt.datetime.now()


def process_md(me):
    me.read_md()


def read_config(config_filename):
    with open(config_filename, 'r') as config_file:
        config = json.load(config_file)

    for key in config_keys:
        if key not in config:
            raise KeyError('"%s" not present in config. please verify' % key)

    matching_engine_configs = config['matchingEngines']
    strategies_config = config['strategies']

    secids = set()
    me_configs = {}
    for config in matching_engine_configs:
        me_id = config['ID']
        for key in matching_engine_config_keys:
            if key not in config:
                raise KeyError('"%s" not present in matchingEngine config ID=%d. please verify' % (key, me_id))

        assumptions_config = config['assumptions']
        for key in matching_engine_config_assumptions_keys:
            if key not in assumptions_config:
                raise KeyError('"%s" not present in matchingEngine(ID=%d):assumptions config. please verify' % (key, me_id))

        # change some string timedeltas to ints
        secids.update(config['secids'])
        assumptions_config['latency'] = to_timedelta(assumptions_config['orderEntryLatency']).value
        assumptions_config['look_forward_time'] = to_timedelta(assumptions_config['lookForwardTime']).value
        config['me_id'] = me_id
        if me_id in me_configs:
            raise KeyError('duplicate matchingEngine(ID=%d). please verify' % me_id)
        me_configs[me_id] = config

    for i, strategy_config in enumerate(strategies_config):
        for key in strategy_config_keys:
            if key not in strategy_config:
                raise KeyError('"%s" not present in strategy config #%d. please verify' % (key, i))

    return list(secids), me_configs, strategies_config


def init_global_strategy(strategy_config):
    strategy_object = strategy_config['objectName']
    strategy_parameters = strategy_config['parameters']

    Strategy = globals()[strategy_object]

    strategies = []

    for me_id in strategy_config['useMatchingEngineIDs']:
        print(now(), 'initializing %s for me_id=%d' % (strategy_object, me_id))
        print(now(), 'parameters: %s' % strategy_parameters)

        strategy = Strategy(**strategy_parameters)

        print(now(), 'checking parameters:')
        for param in sorted(strategy_parameters):
            print(now(), '    %s... %s' % (param, check_attr(strategy, param)))
        print(now(), 'done.')

        # run diagnostics
        print(now(), 'running strategy diagnostics for %s' % strategy)
        print(now(), 'checking required attributes:')
        for attr in strategy_attributes:
            print(now(), '    %s... %s' % (attr, check_attr(strategy, attr)))
        print(now(), 'done.')

        # assign matching engine
        strategy.use_matching_engine(me_id)

        strategies.append(strategy)

    return strategies


if __name__ == '__main__':
    exchange, config_filename = sys.argv[1:]
    md_type = which_md(exchange)

    try:
        print(now(), 'reading config at', config_filename)
        with open(config_filename, 'r') as f:
            print(f.read(), '\n')
        secids, me_configs, strategies_config = read_config(config_filename)
    except Exception as e:
        print(now(), 'ERROR in reading %s' % config_filename)
        print(now(), 'error message: %s' % e)
        print(now(), 'quitting')
        exit(1)

    # initialize MDSource
    md_src = MDSource()
    md_src.initialize(secids)

    # import strategy objects
    if not strategies_config:
        print(now(), 'no strategies to run. quitting')
        exit(1)

    strategy_objects = ', '.join(set([x['objectName'] for x in strategies_config]))

    import_cmd = 'from glt.backtesting import %s' % strategy_objects
    print(now(), import_cmd)
    try:
        exec(import_cmd)
    except ImportError:
        print('cannot import %s from glt.backtesting. please check that objects exist in latest glt install' % strategy_objects)
        exit(1)

    # initialize matching engines
    me_map = {}
    for me_id, me_config in me_configs.items():
        me_assumptions = me_config['assumptions']
        me = MatchingEngine(
            me_config['secids'],
            me_id=me_id,
            look_forward_time=me_assumptions['look_forward_time'],
            latency=me_assumptions['latency'],
            use_exch_time=me_assumptions['useExchangeTime']
        )
        me.ref_md_from(md_src)
        me_map[me_id] = me

        print(now(), 'checking matching engine attributes for ID=%d' % me_id)
        for attr in matching_engine_attributes:
            print(now(), '    %s... %s' % (attr, getattr(me, attr)))
        print(now(), 'done.')

    # now set up strategies
    strategies = []
    for strat_list in map(init_global_strategy, strategies_config):
        for strategy in strat_list:
            if strategy.is_matching_engine_assigned():
                # reference strategies from md_src's market data
                strategy.ref_md_from(md_src)
                me_map[strategy.matching_engine_id()].add_strategy(strategy)
                strategies.append(strategy)

    # start reading file for backtesting
    print(now(), 'now reading market data from stdin')
    reader = iter(sys.stdin)
    next(reader)

    mes = [me for me in list(me_map.values()) if len(me.strategies)]

    i = 0
    try:
        for i, line in enumerate(reader):
            # matching engine must parse the data
            md_src.parse_csv_md(line[:-1], md_type)
            # strategy benefits by just reading matching engine's data;
            # it only has to validate the market data
            list(map(process_md, mes))
    except KeyboardInterrupt:
        print('KeyboardInterrupt! quitting')
    finally:
        print(now(), 'calling .gc() on all strategy objects')
        list(map(lambda strategy: strategy.gc(), strategies))

    print(now(), 'done. lines read:   %d' % i)
