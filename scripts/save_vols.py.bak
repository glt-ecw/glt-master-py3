import sys
import os
import pandas as pd
import datetime as dt
import json
import glt.db


# e.g: python save_vols.py 20161114 DEIO_W


def read_config(json_filename, instance):
    cfg, param = {}, {}
    with open(json_filename, 'r') as f:
         cfg = json.load(f)
    if instance not in cfg:
        raise KeyError('%s does not exist in save_vols.py config')
    cfg = cfg[instance]
    param['user'] = cfg['universalReadWriteConnectionUser']
    param['vols_table'] = cfg['volsTable']
    return param


def query_vols(conn, instance, date, show_cmd=True):
    cmd = """
        SELECT
            securityid as secid,
            rawvolatility as raw,
            volatility as model,
            synthetic as roll
        FROM %s.optionsPricingModelData
    """ % instance
    if show_cmd:
        print cmd
    vols = conn.frame_query(cmd).fillna(0)
    vols = vols.sort_values('secid')
    return not_expired(conn, vols, date, show_cmd=show_cmd)


def delete_table_contents(conn, table, date,
                          real_delete=False, show_cmd=True):
    cmd = """
        DELETE FROM %s
        WHERE (
            tradedate='%s'
        )
    """ % (table, date)
    if show_cmd:
        print cmd
    if real_delete:
        conn.execute(cmd)


def not_expired(conn, vols_table, date, show_cmd=True):
    secids = ', '.join(map(str, vols_table['secid'].values))
    cmd = """
        SELECT id as secid
        FROM SECURITIES.options
        WHERE (
            id in (%s)
            AND
            expdate >= '%s'
        )
    """ % (secids, date)
    if show_cmd:
        print cmd
    relevant_secids = conn.frame_query(cmd)['secid'].values
    return vols_table.loc[vols_table['secid'].isin(relevant_secids)]


def insert_vol_data(conn, table, date, row,
                     real_insert=False, show_cmd=True):
    cmd = """
        INSERT INTO %s
        (tradedate, securityid, raw_volatility, model_volatility, roll)
        VALUES('%s', %d, %0.10f, %0.10f, %0.10f)
    """ % ((table, date) + row)
    if show_cmd:
        print cmd
    if real_insert:
        conn.execute(cmd)


if __name__ == '__main__':
    cfg_path = os.path.join(os.environ['GLT_MASTER_PATH'], 'cfg')
    mysql_cfg_filename = os.path.join(cfg_path, glt.db.CONFIG_BASENAME)
    mysql_config = glt.db.read_mysql_config(mysql_cfg_filename)

    tdate, prod = sys.argv[1:]

    script_cfg_filename = os.path.join(cfg_path, 'deio_eod.json')
    param = read_config(script_cfg_filename, prod)
    user = param['user']
    vols_table = param['vols_table']

    if user not in mysql_config:
        print '%s does not exist in %s' % (user, mysql_cfg_filename)
        exit(1)

    real_execute = user != 'rdeio'

    today = pd.to_datetime(tdate, format='%Y%m%d').date()

    # establish connection
    conn = None
    try:
        conn = glt.db.MysqlConnection(**mysql_config[user])
        # from %s.optionsPricingModelData
        vols = query_vols(conn, prod, today)

        # delete current contents of pos_table and fill_table
        delete_table_contents(conn, vols_table, today, real_delete=real_execute)

        # insert into vols_table
        for row in zip(*(vols[col] for col in vols)):
            insert_vol_data(conn, vols_table, today, row, real_insert=real_execute)

        # and finally commit
        conn.commit()

    finally:
        if conn is not None:
            print 'closing connection'
            conn.close()
        else:
            print 'connection non-existent'
