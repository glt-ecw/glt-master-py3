
import sys
import os
import pandas as pd

from dateutil.relativedelta import relativedelta
from collections import defaultdict

from glt import db

pd.options.mode.chained_assignment = None

MCODES = '0FGHJKMNQUVXZ'
MIDX_MAP = {m: i for m, i in zip(MCODES, list(range(len(MCODES))))}
QUARTERLIES = 'HMUZ'

QUARTERLY_EXCEPTIONS = ['MNK', 'SNK']

def find_next_expiry(suffix):
    month = MIDX_MAP[suffix[0]]
    year = int(suffix[1:])
    s_month = (month%12)+1
    q_month = ((month/3)%4+1)*3
    s_year = year
    q_year = year
    if s_month < month:
        s_year += 1
    if q_month < month:
        q_year += 1
    return '%s%d' % (MCODES[s_month], s_year), '%s%d' % (MCODES[q_month], q_year)

def find_next_possibles(name):
    if '_' in name:
        first, second = name.split('_')
    else:
        first, second = name, None
    product = first[:-3]
    suffix_s, suffix_q = find_next_expiry(first[-3:])
    if second is None:
        next_s = product + suffix_s
        next_q = product + suffix_q
    else:
        next_s = product + suffix_s + '_' + second
        next_q = product + suffix_q + '_' + second
    return next_s, next_q

def is_quarterly(month):
    return month in QUARTERLIES

def find_next_outright(names_map, name):
    is_exception = any(name[:len(p)]==p for p in QUARTERLY_EXCEPTIONS)
    next_s, next_q = find_next_possibles(name)
    next_s_secid = names_map.get(next_s)
    next_q_secid = names_map.get(next_q)
    if next_s_secid is None:
        return next_q_secid
    elif is_quarterly(name[-3]) and is_exception:
        return next_q_secid
    else:
        return next_s_secid

def find_next_spread(names_map, front, back):
    is_front_q = is_quarterly(front[-3])
    is_back_q = is_quarterly(back[-3])
    next_front_s, next_front_q = find_next_possibles(front)
    next_back_s, next_back_q = find_next_possibles(back)
    is_exception = any(front[:len(p)]==p for p in QUARTERLY_EXCEPTIONS)
    if is_front_q and is_back_q:
        next_front = next_front_q
        next_back = next_back_q
    elif next_front_s == back:
        if is_front_q and is_exception:
            next_front = next_front_s
            next_back = next_back_q
        else:
            next_front = next_front_s
            next_back = next_back_s
    else:
        next_front = next_front_s
        next_back = back
    return names_map.get('%s-%s' % (next_front, next_back))

if __name__ == '__main__':
    datestr = sys.argv[1]
    output_dir = sys.argv[2]

    date = pd.to_datetime(datestr).date()
    month_back = date - relativedelta(months=1)

    print(date, month_back)

    securities = None
    try:
        conn = db.conn('10.1.31.202', 'deio', '!w3alth!', 'SECURITIES')
        cmd = """
            SELECT 
                securities.id as secid, 
                shortname, 
                contractdefid as cd, 
                expdate 
            FROM (
                SELECT id, expdate 
                FROM futures 
                UNION ALL 
                SELECT id, expdate 
                FROM futureSpreads 
                UNION ALL
                SELECT id, expdate 
                FROM options
            ) AS defs 
            LEFT JOIN securities 
            ON (defs.id=securities.id) 
            WHERE expdate >= '%s' 
            AND shortname NOT IN ('KOSPI200', 'KOSYNTH')
        """ % month_back
        securities = conn.frame_query(cmd)
    finally:
        conn.close()

    if securities is None:
        print('failed to retrieve securities')
        sys.exit(1)

    securities = securities.loc[~securities['shortname'].isnull()]
    securities['expdate'] = pd.to_datetime(securities['expdate'])

    secs_list = []
    for cd in securities['cd'].unique():
        secs = securities.loc[securities['cd']==cd]

        secs['secid'] = secs['secid'].round().astype(int)
        secs.index = secs['secid']
        secids_map = secs['shortname'].to_dict()

        names = secs.ix[:, ['secid', 'shortname']]
        names.index = names['shortname']
        names_map = names['secid'].to_dict()

        del names

        #expired = securities.loc[securities['expdate']<date]
        next_secids = []
        ignore_ids = []
        for idx, row in secs.iterrows():
            name = row['shortname']
            cd = row['cd']
            if '-' in name:
                front, back = name.split('-')
                # hack for names like NIY-NIYH16_0C
                if len(front) != len(back):
                    ignore_ids.append(row['secid'])
                    continue
                next_id = find_next_spread(names_map, front, back)
            else:
                next_id = find_next_outright(names_map, name)
            next_secids.append(next_id)
        secs = secs.loc[~secs.secid.isin(ignore_ids)]
        secs['next_secid'] = next_secids
        secs['next_shortname'] = secs['next_secid'].apply(lambda x: secids_map.get(x))

        secs = secs.dropna()
        if len(secs):
            secs_list.append(secs)

    securities = pd.DataFrame().append(secs_list)
    output_filename = os.path.join(output_dir, 'deio_rollmap.all.p')
    securities.to_pickle(output_filename)

    output_filename = os.path.join(output_dir, 'deio_rollmap.expired.p')
    expired = securities.loc[securities['expdate']<date]
    expired.to_pickle(output_filename)
     
