import numpy as np
import pandas as pd
import datetime
#print 'numpy', np.version.version
import glt.db as db
import sys
import glob

####
# To setup email
import smtplib
import base64
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

## Suppress warnings ##
import warnings
warnings.filterwarnings('ignore')
from io import StringIO as cStringIO
import io

def sendEmail(toList,msgSubject,message):

    if str(type(toList)) == "<type 'str'>":
        toList = [toList]

    msg = MIMEMultipart()
    msg['From'] = 'C.K. Eun <myaccount@gmail.com>'
    msg['Subject'] = msgSubject

    msg.attach(MIMEText(message,'html'))

    server = smtplib.SMTP()
    server.connect('smtp.gmail.com',587)
    server.ehlo()
    server.starttls()
    server.login('cke@glt-llc.com','nueyak77')
    server.sendmail('cke@glt-llc.com', toList, msg.as_string())
    server.close()

## Settings ##
debug = False
datestr = sys.argv[1]
if len(sys.argv) > 2:
    print(sys.argv[2])
    debug = True

if not debug:
    old_stdout = sys.stdout
    StringCatcher = io.StringIO()
    sys.stdout = StringCatcher

DBSERVER01_CONN = {
    'host': 'sql01.glt-llc.com',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'DEIO_JPA'
}

SEC_CONN = {
    'host': 'dbserver01',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'SECURITIES'
}
"""
qty_multiplier={
    515131: 1,
    515115: 10,
    123196: 5
}
"""

qty_multiplier={
    515116: 1,
    515132: 10,
    123197: 5
}


datestr = sys.argv[1]
date_db = datestr[:4]+'-'+datestr[4:6]+'-'+datestr[6:8]

try:
    conn = db.conn(**DBSERVER01_CONN)
    fills = conn.frame_query("select * from fills where tradeDate='%s' order by fillTime, usFillTime"%date_db)
except:
    print("Fills not found")
finally:
    conn.close()

sniper = fills.loc[fills.orderType==11]
manuscraper = fills.loc[fills.orderType==17]
manual = fills.loc[fills.orderType==1]

agg_manuscraper = manuscraper.groupby(['fillTime','side','price','orderType','securityId'])['qty'].sum().reset_index()
agg_sniper = sniper.groupby(['fillTime','side','price','orderType','securityId'])['qty'].sum().reset_index()
agg_manual = manual.groupby(['fillTime','side','price','orderType','securityId'])['qty'].sum().reset_index()

#print(agg_manuscraper.shape, agg_sniper.shape, agg_manual.shape)

agg = pd.concat([agg_sniper, agg_manuscraper, agg_manual]).sort(['fillTime','orderType'])
agg = agg.loc[:, ['fillTime','orderType','securityId','side','price','qty']]
agg = agg.loc[agg.securityId.isin(list(qty_multiplier.keys()))]

agg['qty_mnk'] = agg['qty'] * agg['securityId'].map(qty_multiplier)

curr_pos=0; getin_qty=0; ave_price=0; getout_qty=0; ave_getoutprice=0;
getout_t=None
record=True
results=[]
for t, typ, secid, side, price, qty in agg.loc[:, ['fillTime','orderType','securityId','side','price','qty_mnk']].values:
    if getout_t==None:
        getout_t = t#.value
        getin_side = side  
    if typ==11:
        
        if side != getin_side and curr_pos > 20:#there was a sniper going the other way...
            curr_pos -= qty
            getout_qty += qty
            getout_price = price
            getout_side = side
            ave_getoutprice += price * qty
            getout_t = t
            record=False
            #print "ms/manual trade out"
            #print "curr pos: %d, qty: %d, getout_qty: %d, getoutprice: %d, getout_side:%d, aver getouprice paid: %f\n" %(curr_pos, qty, getout_qty, getout_price, getout_side, ave_getoutprice / (1.0*getout_qty))        
            if curr_pos <= 0:
                if record==False:
                    results.append([t, trigsecid, getin_side, ave_price / (1.0*getin_qty), getin_qty, ave_getoutprice / (1.0*getout_qty), getout_qty])
                    record=True
                #print "pos crossed zero. resetting"
                curr_pos = 0
                getin_qty = 0
                getout_qty = 0
                ave_price = 0
                ave_getoutprice = 0            

        else:
            if side != getin_side or ((t.value - getout_t.value) > 5e9): #not a great way to do this 
                if record==False:
                    results.append([t, trigsecid, getin_side, ave_price / (1.0*getin_qty), getin_qty, ave_getoutprice / (1.0*getout_qty), getout_qty])
                    record=True
                #print "sniping diff side. resetting", t, getout_t
                #new sniper trade going oother way, reset
                curr_pos = 0
                getin_qty = 0
                getout_qty = 0
                ave_price = 0
                ave_getoutprice = 0
                getout_t = t
            #sniper get in trade:
            trigsecid = secid
            curr_pos += qty
            getin_qty += qty
            getin_price = price #get in price\
            getin_side = side
            ave_price += price * qty 
            getin_t = t
            #print "sniper trade in"
            #print "curr pos: %d, qty: %d, getin_qty: %d, getinprice: %d, getin_side:%d, aver price paid: %f\n" %(curr_pos, qty, getin_qty, getin_price, getin_side, ave_price / (1.0*getin_qty))
    elif curr_pos and typ in [1, 17] and side != getin_side:
        curr_pos -= qty
        getout_qty += qty
        getout_price = price
        getout_side = side
        ave_getoutprice += price * qty
        getout_t = t
        record=False
        #print "ms/manual trade out"
        #print "curr pos: %d, qty: %d, getout_qty: %d, getoutprice: %d, getout_side:%d, aver getouprice paid: %f\n" %(curr_pos, qty, getout_qty, getout_price, getout_side, ave_getoutprice / (1.0*getout_qty))        
        if curr_pos <= 0:
            if record==False:
                results.append([t, trigsecid, getin_side, ave_price / (1.0*getin_qty), getin_qty, ave_getoutprice / (1.0*getout_qty), getout_qty])
                record=True
            #print "pos crossed zero. resetting"
            curr_pos = 0
            getin_qty = 0
            getout_qty = 0
            ave_price = 0
            ave_getoutprice = 0
    else:
        #print "Doin't know"
        #print typ, secid, side, price, qty, "\n"
        continue
    
df = pd.DataFrame(results, columns=['t','trigger_contract','side','getin_price','getin_qty','getout_price','getout_qty'])
df['qty_diff'] = df['getin_qty'] - df['getout_qty']
df.loc[df.side==1, 'ticks'] = (df['getout_price'] - df['getin_price'])/10.
df.loc[df.side==2, 'ticks'] = (df['getin_price'] - df['getout_price'])/10.

df['score'] = df['ticks'].apply(np.sign)

#print df.to_string()

int_cols = ['getin_price','getin_qty','getout_price','getout_qty','qty_diff','score']
for col in int_cols:
    #print(col)
    df[col] = df[col].astype(int)

df['ticks'] = df['ticks'].map(lambda x: '{0:.2}'.format(x))

print("SUMMARY OF MANUSCRAPER GET OUT TRADES\n")   
print(df.groupby('trigger_contract').score.value_counts())
print("\n")
print("LIST OF MANUSCRAPER GET OUT TRADES\n")   
print(df.to_string())

toList = ['cke@glt-llc.com']
#toList = ['perftimes@glt-llc.com']
if not debug:
    sys.stdout = old_stdout
    output = StringCatcher.getvalue()

    sendEmail(toList,"Manuscraper Get out performance","<font face='courier' size='3'><pre>" + output + "</pre></font>")



