import numpy as np
import pandas as pd
import datetime as datetime
print('numpy', np.version.version)
print('pandas', pd.version.version)
import glt.db as db
import sys, os

DBSERVER01_CONN = {
    'host': 'dbserver01',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'DEIO_JPX'
}

date = sys.argv[1]

datestr = date[:4]+'-'+date[4:6]+'-'+date[6:8]

DBSERVER01 = "10.1.31.202"
retry_msg = "'NK timer sprinter with retry'"

sprinter_ids, sprints = None, None

os.system('/glt/research/bin/parse_ormsgs.sh ' + datestr)
orlogfile = "/glt/ssd/hdf/orders."+datestr+".h5"

sprints = None
if os.path.isfile(orlogfile):
    with pd.HDFStore(orlogfile, 'r') as store:
        if '/mo41' in list(store.keys()):
            sprints = store['/mo41']
else:
    print(("no order log file for ", datestr))

if sprints is None:
    print("No sprinters for %s" %date)
    #sys.exit('boo!')
else:
    sprinter_ids = sprints.loc[sprints.err_msg != "Illegal transaction at this time"].sort('time').drop_duplicates('serverid')
    if len(sprinter_ids):
        sprinter_ids['usSprintTime'] = [pd.to_datetime(t).microsecond for t in sprinter_ids.time.values]
        sprinter_ids['SprintTime'] = [pd.to_datetime(t).time() for t in sprinter_ids.time.values]
        sprinter_ids['SprintTime'] = ["'" + str(t).split(".")[0] + "'" for t in sprinter_ids.SprintTime.values]

        db_entries = sprinter_ids.drop_duplicates('deioid').loc[:, ['serverid', 'acktime', 'gateway', 'SprintTime', 'usSprintTime']]            

        for array in db_entries.values:
            server, acktime, gateway, sprintTime, usSprintTime = array
            updatedSprintTime = sprintTime
            usUpdatedSprintTime = usSprintTime
            print(server, acktime, gateway, sprintTime, usSprintTime)

            try:
                update_cmd = """
                    insert into first_accepted_sprinter_times (datestr, server, acktime, gateway, sprintTime, usSprintTime, updatedSprintTime, usUpdatedSprintTime)
                    values('%s', %d, %d, '%s', %s, %d, %s, %d)
                    on duplicate key update
                    acktime=%d, gateway='%s', sprintTime=%s, usSprintTime=%d, updatedSprintTime=%s, usUpdatedSprintTime=%d
                """ % (datestr, server, acktime, gateway, sprintTime, usSprintTime, updatedSprintTime, usUpdatedSprintTime, acktime, gateway, 
                       sprintTime, usSprintTime, updatedSprintTime, usUpdatedSprintTime)

                conn = db.conn(**DBSERVER01_CONN)
                conn.execute(update_cmd)

            finally:
                conn.close()

