
import sys
import os
import datetime as dt

import numpy as np
import pandas as pd

import glt.db

DEIO_USERID = 7

RES01_CFG = {
    'host': '10.1.31.200',
    'user': 'rdeio',
    'pw': 'fatmanblues',
    'db': 'eod'
}

DEIODB_CFG = {
    'jpx': {
        'host': '10.1.31.200',
        'user': 'deio',
        'pw': '!w3alth!',
        'db': 'DEIO_JPX'
    },
    'w': {
        'host': '10.1.31.200',
        'user': 'deio',
        'pw': '!w3alth!',
        'db': 'DEIO_W'
    },
    'bk': {
        'host': '10.130.33.103',
        'user': 'deio',
        'pw': '!w3alth!',
        'db': 'DEIO_BK'
    },
    'securities': {
        'host': '10.1.31.200',
        'user': 'rdeio',
        'pw': 'fatmanblues',
        'db': 'SECURITIES'
    }
}

def query_current_securities(today):
    securities = None
    try:
        conn = glt.db.conn(**DEIODB_CFG['securities'])
        cmd = """
            SELECT id FROM (
                SELECT id, expdate FROM futures 
                UNION ALL 
                SELECT id, expdate FROM options 
                UNION ALL
                SELECT id, expdate FROM warrants 
                UNION ALL
                SELECT id, expdate from bonds
            ) as contracts 
            WHERE expdate>='%s'
        """ % today
        securities = conn.frame_query(cmd)['id'].values
    finally:
        conn.close()
    return securities

def query_yesterday(today, instance):
    if instance not in DEIODB_CFG:
        return None
    yday = None
    try:
        conn = glt.db.conn(**RES01_CFG)
        yday = today - dt.timedelta(1)
        while yday.weekday() > 4:
            yday -= dt.timedelta(1)
    finally:
        conn.close()
    return yday

def query_my_position(instance):
    positions = None
    if instance not in DEIODB_CFG:
        return positions
    deio_conn_cfg = DEIODB_CFG[instance]
    deio_instance = deio_conn_cfg['db']
    valid_securities = query_current_securities(today)
    try:
        conn = glt.db.conn(**RES01_CFG)    
        yday = query_yesterday(today, instance)
        cmd = """
            SELECT securityid, position
            FROM eod.positions
            WHERE tradedate=(select max(tradedate) from positions) 
            AND instance='%s'
            AND securityid in %s
        """ % (deio_instance, tuple(valid_securities))
        curr = conn.frame_query(cmd)
        curr.index = curr.securityid
        curr = curr['position']
    finally:
        conn.close()
    return curr

def query_fills(today, instance):
    curr = None
    if instance not in DEIODB_CFG:
        return curr
    deio_conn_cfg = DEIODB_CFG[instance]
    deio_instance = deio_conn_cfg['db']
    try:
        conn = glt.db.conn(**deio_conn_cfg)
        cmd = """
            SELECT securityid, userid, SUM(qty*(3-2*side)) as position
            FROM %s.fills
            WHERE tradedate='%s'
            AND NOT (filltype=2 or (exchtradeid='' and price=0))
            GROUP BY securityid, userid
        """ % (deio_instance, today)
        curr = conn.frame_query(cmd)
        #curr.index = curr.securityid
        #curr = curr['position']
    finally:
        conn.close()
    return curr

if __name__ == '__main__':
    today = dt.date.today()
    yday = today - dt.timedelta(1)

    instance = sys.argv[1]

    my_position = query_my_position(instance)

    if my_position is None:
        sys.exit('unknown instance', instance)

    try:
        conn_cfg = DEIODB_CFG[instance]
        conn = glt.db.conn(**conn_cfg)
        del_cmd = """
            DELETE FROM positions 
            WHERE tradedate='%s'
        """ % yday
        print(dt.datetime.now(), 'deleting deio positions from', yday)
        conn.execute(del_cmd)
        print(dt.datetime.now(), 'now adding what I know to deio positions')
        for secid, secpos in my_position.items():
            if secpos == 0:
                continue
            cmd = """
                INSERT INTO positions (tradedate, securityid, userid, instance, position) 
                VALUES('%s', %d, %d, '%s', %d)
                ON DUPLICATE KEY UPDATE position=%d
            """ % (yday, secid, DEIO_USERID, instance.upper(), secpos, secpos)
            conn.execute(cmd)

        # any fills from today?
        filled_position = query_fills(today, instance)
        print(filled_position.to_string())
        if filled_position is None or filled_position.shape[0] == 0:
            print(dt.datetime.now(), 'no fills to adjust for')
        else:
            print(dt.datetime.now(), 'adjusting positions for today using fills')
            del_cmd = """
                DELETE FROM positions 
                WHERE tradedate='%s'
            """ % today
            print(dt.datetime.now(), 'deleting deio positions from', today)
            conn.execute(del_cmd)
            for idx, row in filled_position.iterrows():
                secid, userid, secpos = row
                if secpos == 0:
                    continue
                cmd = """
                    INSERT INTO positions (tradedate, securityid, userid, instance, position) 
                    VALUES('%s', %d, %d, '%s', %d)
                    ON DUPLICATE KEY UPDATE position=%d
                """ % (today, secid, userid, instance.upper(), secpos, secpos)
                conn.execute(cmd)
    finally:
        conn.close()
