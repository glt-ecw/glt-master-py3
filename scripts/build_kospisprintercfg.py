
import sys
import os
import json
import datetime as dt
import numpy as np
import pandas as pd

from random import uniform

import glt.db

from scipy.interpolate import interp1d

from glt.math import bs

# general definitions
ASSUMED_FUT_MOVE_PCT = 0.01
MAX_QTY = 1000
RAND_PCT = 0.05
ALTQTY_PCT = 0.5

# futures definitions
FUT_TICK_WIDTH = 0.05
FUT_START_TICKS_AWAY = 7
SKIP_TIGHT_LEVELS = True
FUT_QTY = 26

# options definitions
INTEREST_RATE = 0.015
BIZDAYS = 260.0
LOW_DT = 0.3
HIGH_DT = 0.7
DU = 0.25

XTRA_BOOST = 1.0
BID_PREMIUM_BOOST = 1.05

def randqty(qty, pct, cap=MAX_QTY):
    return int(min((1+uniform(-pct, pct))*qty, cap))

def floor(num, precision):
    return pow(10, -precision) * np.floor(pow(10, precision) * num)

def ceil(num, precision):
    return pow(10, -precision) * np.ceil(pow(10, precision) * num)

def retrieve_cmekospi(filename):
    return pd.read_csv(filename).iloc[-1]['px_last']

def retrieve_settles(res01_conn, today):
    yday = today - dt.timedelta(1)
    vols = None
    try:
        conn = glt.db.conn(**res01_conn)
        # fetch vols
        cmd = "select date from krx_holidays"
        holidays = conn.frame_query(cmd)['date'].values
        while yday in holidays or yday.weekday() > 4:
            yday -= dt.timedelta(1)
        ydb = yday.isoformat().replace('-', '_')
        conn.execute('use %s' % ydb)
        vols = conn.frame_query("select securityid, vol, roll from eod_krxvols")
        vols.index = vols['securityid']
    finally:
        conn.close()
    return vols

def retrieve_defs(dbserver01_conn):
    options, futures = None, None
    try:
        # first options
        conn = glt.db.conn(**dbserver01_conn)
        cmd = (
            "select * from options_expiries where expiry >= '%s' and "
            "shortname regexp '^KOO'"
        ) % dt.date.today()
        options = conn.frame_query(cmd)
        options.index = options['id']
        del options['id']
        # now front-month futures
        cmd = (
            "select * from futures_expiries where expiry >= '%s' and "
            "shortname regexp '^KO' order by expiry limit 2"
        ) % dt.date.today()
        futures = conn.frame_query(cmd)
        futures.index = futures['id']
        del futures['id']
    finally:
        conn.close()
    return options, futures

def fmt_cfg(secid, side, prc, qty, sprinter_type, user, altqty):
    sidestr = 'bid' if side == 1 else 'offer'
    order = secid, sidestr, prc, qty, sprinter_type, user, altqty
    return '%d,%s,%0.2f,%d,%s,%s,%d' % order

if __name__ == '__main__':
    cme_filename = sys.argv[1]
    json_config = sys.argv[2]
    instance = sys.argv[3]
    vol_center = np.float64(sys.argv[4])

    with open(json_config, 'r') as f:
        all_param = json.load(f)
        if instance not in all_param:
            raise KeyError('%s non-existent in %s' % (instance, json_config))
        param = all_param[instance]
        delta_bidqty_map = {k: v for k, v in all_param['delta_bidqty_map']}
        delta_askqty_map = {k: v for k, v in all_param['delta_askqty_map']}
        delta_qty_map = [None, delta_bidqty_map, delta_askqty_map]
        # sql connection parameters
        dbserver01_conn = all_param['dbserver01_conn']
        res01_conn = all_param['res01_conn']

    # initialize some parameters
    orders_available = param['maxOrders']
    sprinter_type = param['sprinterType']
    first_side = param['firstSide']
    num_futures = param['numFutures']
    num_options = param['numOptions']
    num_options_bids = param['numOptionsBids']
    num_options_asks = param['numOptionsAsks']
    offer_order_ratio = param['offerOrderRatio']
    qty_multiplier = param['qtyMultiplier']
    user_futures = param['userFutures']
    user_options = param['userOptions']
    ignore_call_bids = param['ignoreCallBids']
    ignore_call_asks = param['ignoreCallAsks']
    ignore_put_bids = param['ignorePutBids']
    ignore_put_asks = param['ignorePutAsks']
    min_delta = param['minDelta']
    max_delta = param['maxDelta']

    if num_options > orders_available:
        raise ValueError('numOptions > maxOrders. fix json file')
    
    if num_futures > orders_available:
        raise ValueError('numFutures > maxOrders. fix json file')

    options_left = num_options
    futures_left = num_futures

    # start processing here
    today = dt.date.today()

    futprc = retrieve_cmekospi(cme_filename)

    vols = retrieve_settles(res01_conn, today)
    options_defs, futures_defs = retrieve_defs(dbserver01_conn)

    dat = options_defs.join(vols)
    front_month = dat['expiry'].min()
    dat = dat.loc[dat['expiry']==front_month]
    
    dte, cdte = 1, 1
    while today + dt.timedelta(cdte) < front_month:
        sometime = today + dt.timedelta(cdte)
        if sometime.weekday() < 5:
            dte += 1
        cdte += 1
    dte += 1

    if dte > 15:
        lo_dte = LOW_DT
        xtra = 0.60
    elif dte > 10:
        lo_dte = LOW_DT
        xtra = 0.8
    elif dte == 3:
        lo_dte = 0.5
        xtra = 2.0
    elif dte == 2:
        lo_dte = 0.7
        xtra = 3.0
    elif dte == 1:
        lo_dte = 0.9
        xtra = 5.0
    else:
        lo_dte = LOW_DT
        xtra = XTRA_BOOST

    hi_dte = HIGH_DT

    lo_dt = lo_dte / BIZDAYS
    hi_dt = hi_dte / BIZDAYS
    
    dat['dte'] = dte
    dat['yte'] = dat['dte'] / BIZDAYS

    # take out zero vol options?
    dat = dat.loc[dat['vol']>0]

    dat['futprc'] = futprc
    dat['u'] = dat['futprc'] + dat['roll']
    dat['rate'] = INTEREST_RATE

    shortname_map = dat['shortname'].to_dict()

    opt_cols = ['strike', 'u', 'vol', 'yte', 'rate', 'op']
    deltas = {secid: bs.delta(*row[opt_cols].values) for secid, row in dat.iterrows()}
    dat['delta'] = pd.Series(deltas)
    dat['abs_delta'] = dat['delta'].abs()
    dat = dat.sort(['abs_delta', 'op'], ascending=[True, False])

    dat = dat.loc[dat['abs_delta']>min_delta]

    # REMIX
    options_bid_orders, options_ask_orders = [], []
    best_bids, best_asks = {}, {}
    for secid, row in dat.iterrows():
        x, u, v, t, r, op = row[opt_cols]
        # adjust centering
        v += vol_center
        lo_t = t + lo_dt
        hi_t = t + hi_dt
        if op < 0:
            lo_u = u + DU
            hi_u = u - DU
        else:
            lo_u = u - DU
            hi_u = u + DU
        # find delta range of max futures move allowed
        min_u = (1 - ASSUMED_FUT_MOVE_PCT) * u
        max_u = (1 + ASSUMED_FUT_MOVE_PCT) * u
        lo_delta = abs(bs.delta(x, min_u, v, lo_t, r, op))
        hi_delta = abs(bs.delta(x, max_u, v, lo_t, r, op))
        if lo_delta > max_delta and hi_delta > max_delta:
            continue
        # now compute orders and corresponding sizes
        size_map = delta_qty_map[1]
        bidprc = bs.tv(x, lo_u, v, lo_t, r, op)
        best_key = op, floor(bidprc, 2)
        if best_key in best_bids:
            #if (not ignore_call_bids and op == 1) or (not ignore_put_bids and op == -1):
            if best_bids[best_key] in options_bid_orders:
                options_bid_orders.pop(best_bids[best_key])
        best_bids[best_key] = len(options_bid_orders)
        # exception for expiration week orders
        # place multiple under an arbitrary tick threshold
        st_bidprc = bidprc
        # compute bid prices
        premium_boost = 1.0
        seed_u, iu = 2*[lo_u]
        min_bidprc = 0.02 #if bidprc > 0.05 else 0.01
        last_rdelta = 0
        while bidprc > min_bidprc and iu < max_u and iu > min_u:
            iu = bs.iu(x, v, lo_t, r, op, bidprc, seed_u, 0.001, 100)
            delta_i = abs(bs.delta(x, iu, v, lo_t, r, op))
            if delta_i <= max_delta:
                rdelta = round(delta_i, 2)
                qty = xtra * qty_multiplier * premium_boost * size_map[rdelta]
                rqty = randqty(qty, RAND_PCT)
                altqty = rqty * ALTQTY_PCT
                rprc = floor(bidprc, 2)
                order = secid, 1, rprc, rqty, sprinter_type, user_options, altqty
                if (not ignore_call_bids and op == 1) or (not ignore_put_bids and op == -1):
                    options_bid_orders.append(order)
                if rdelta != last_rdelta:
                    premium_boost *= BID_PREMIUM_BOOST
                    last_rdelta = rdelta
            bidprc -= 0.01
            seed_u = iu
        # now compute offer prices
        size_map = delta_qty_map[2]
        askprc = bs.tv(x, hi_u, v, hi_t, r, op)
        best_key = op, floor(askprc, 2)
        if best_key in best_asks:
            askprc += 0.01
            best_key = op, floor(askprc, 2)
        best_asks[best_key] = len(options_ask_orders)
        # exception for expiration week orders
        seed_u, iu = 2*[hi_u]
        delta_i = abs(bs.delta(x, iu, v, hi_t, r, op))
        min_u = (1 - ASSUMED_FUT_MOVE_PCT * offer_order_ratio) * u
        max_u = (1 + ASSUMED_FUT_MOVE_PCT * offer_order_ratio) * u
        while iu < max_u and iu > min_u:
            iu = bs.iu(x, v, hi_t, r, op, askprc, seed_u, 0.001, 100)
            delta_i = abs(bs.delta(x, iu, v, hi_t, r, op))
            if delta_i > max_delta:
                break
            if askprc >= 0.02:
                rdelta = round(delta_i, 2)
                qty = xtra * qty_multiplier * size_map[rdelta]
                rqty = randqty(qty, RAND_PCT)
                altqty = rqty * ALTQTY_PCT
                rprc = floor(askprc, 2)
                order = secid, 2, rprc, rqty, sprinter_type, user_options, altqty
                if (not ignore_call_asks and op == 1) or (not ignore_put_asks and op == -1):
                    options_ask_orders.append(order)
            askprc += 0.01
   
    # now which orders do we want to place?
    orders = {
        'bids':  {
            'count': num_options_bids,
            'data': options_bid_orders[::-1]
        },
        'asks': {
            'count': num_options_asks,
            'data': options_ask_orders[::-1]
        }
    }
    keys = 'bids', 'asks'
    if first_side == 2:
        keys = keys[::-1]

    print('### options orders available', options_left) 
    for key in keys:
        print('### options', key)
        data = orders[key]['data']
        count = orders[key]['count']
        while data and options_left > 0 and count > 0:
            row = data.pop()
            print(fmt_cfg(*row))
            count -= 1
            options_left -= 1

    # now do some futures
    futures_left = orders_available - num_options + options_left
    futures_left = min(num_futures, futures_left)
    
    bidticks_away = FUT_START_TICKS_AWAY
    askticks_away = FUT_START_TICKS_AWAY
    fut_id = futures_defs.index[0]
    fut_name = futures_defs.loc[fut_id, 'shortname']
    print('###', 'alternating', fut_id, fut_name, end=', ')
    print('orders remaining:', futures_left)
    while futures_left > 0:
        bidprc = futprc - bidticks_away * FUT_TICK_WIDTH
        askprc = futprc + askticks_away * FUT_TICK_WIDTH
        for side, prc in enumerate((bidprc, askprc), 1):
            if futures_left <= 0:
                break
            qty = randqty(FUT_QTY, RAND_PCT)
            altqty = qty * ALTQTY_PCT
            order = fut_id, side, prc, qty, sprinter_type, user_futures, altqty
            print(fmt_cfg(*order))
            futures_left -= 1
        bidticks_away += 1
        askticks_away += 1
