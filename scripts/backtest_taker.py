import sys
import json
import datetime as dt
from pandas import to_datetime, to_timedelta

from glt.backtesting import MarketTakingMatchingEngine as MatchingEngine
#from glt.backtesting import MatchingEngine
from glt.backtesting import Hindsight


config_keys = [
    'matchingEngine',
    'strategies',
    'hindsight'
]

matching_engine_config_keys = [
    'secids',
    'assumptions'
]

matching_engine_config_time_keys = [
    'timeZoneOffset',
    'start',
    'end'
]

matching_engine_config_assumptions_keys = [
    'orderEntryLatency',
    'fullOrderFilled'
]

strategy_config_keys = [
    'objectName',
    'parameters'
]

hindsight_config_keys = [
    'lookForwardTime'
]

matching_engine_attributes = [
    'latency',
    #'assume_all_filled',
    #'orders',
    #'matches',
    #'misses'
]

hindsight_attributes = [
    'look_forward',
    'orders'
]

taker_attributes = [
    'valid_md',
    'evaluate_md',
    'has_orders',
    'place_order',
    'remove_order'
]


def check_attr(obj, attr):
    return 'yes' if hasattr(obj, attr) else 'no'


def now():
    return '%s:' % dt.datetime.now()


def read_config(config_filename):
    with open(config_filename, 'r') as config_file:
        config = json.load(config_file)

    for key in config_keys:
        if key not in config:
            raise KeyError('"%s" not present in config. please verify' % key)

    matching_engine_config = config['matchingEngine']
    strategies_config = config['strategies']
    hindsight_config = config['hindsight']

    for key in matching_engine_config_keys:
        if key not in matching_engine_config:
            raise KeyError('"%s" not present in matchingEngine config. please verify' % key)

    #me_assumptions_config = matching_engine_config['time']
    #for key in matching_engine_config_time_keys:
    #    if key not in me_assumptions_config:
    #        raise KeyError('"%s" not present in matchingEngine:time config. please verify' % key)

    me_assumptions_config = matching_engine_config['assumptions']
    for key in matching_engine_config_assumptions_keys:
        if key not in me_assumptions_config:
            raise KeyError('"%s" not present in matchingEngine:assumptions config. please verify' % key)

    for i, strategy_config in enumerate(strategies_config):
        for key in strategy_config_keys:
            if key not in strategy_config:
                raise KeyError('"%s" not present in strategy config #%d. please verify' % (key, i))

    for key in hindsight_config_keys:
        if key not in hindsight_config:
            raise KeyError('"%s" not present in hindsight config. please verify' % key)

    # change some string timedeltas to ints
    matching_engine_config['assumptions']['latency'] = to_timedelta(matching_engine_config['assumptions']['orderEntryLatency']).value
    hindsight_config['look_forward'] = to_timedelta(hindsight_config['lookForwardTime']).value
    return matching_engine_config, strategies_config, hindsight_config


def initialize_taker(strategy_config, Taker):
    strategy_object = strategy_config['objectName']
    strategy_parameters = strategy_config['parameters']

    print(now(), 'initializing %s' % strategy_object)
    print(now(), 'parameters: %s' % strategy_parameters)

    taker = Taker(**strategy_parameters)

    print(now(), 'checking parameters:')
    for param in sorted(strategy_parameters):
        print(now(), '    %s... %s' % (param, check_attr(taker, param)))
    print(now(), 'done.')

    # run diagnostics
    print(now(), 'running strategy diagnostics for %s' % taker)
    print(now(), 'checking required attributes:')
    for attr in taker_attributes:
        print(now(), '    %s... %s' % (attr, check_attr(taker, attr)))
    print(now(), 'done.')

    return taker


class DummyStrategy:
    def __init__(self, **kwargs):
        pass


if __name__ == '__main__':
    config_filename = sys.argv[1]
    try:
        matching_engine_config, strategies_config, hindsight_config = read_config(config_filename)
    except Exception as e:
        print(now(), 'ERROR in reading %s' % config_filename)
        print(now(), 'error message: %s' % e)
        print(now(), 'quitting')
        exit(1)

    strategy_objects = ', '.join(set([x['objectName'] for x in strategies_config]))

    Taker = DummyStrategy

    print(now(), 'from glt.backtesting import %s' % strategy_objects)
    try:
        exec('from glt.backtesting import %s as Taker' % strategy_objects)
    except ImportError:
        print('cannot import %s from glt.backtesting. please check that objects exist in latest glt install' % strategy_objects)
        exit(1)

    takers = [initialize_taker(config, Taker) for config in strategies_config]

    # initialize matching engine
    me_assumptions = matching_engine_config['assumptions']
    me = MatchingEngine(
        matching_engine_config['secids'],
        latency=me_assumptions['latency'],
        assume_all_filled=me_assumptions['fullOrderFilled']
    )

    print(now(), 'checking matching engine attributes')
    for attr in matching_engine_attributes:
        print(now(), '    %s... %s' % (attr, getattr(me, attr)))
    print(now(), 'done.')

    # initialize hindsight
    hindsight = Hindsight(
        matching_engine_config['secids'],
        look_forward=hindsight_config['look_forward']
    )

    print(now(), 'checking hindsight attributes')
    for attr in hindsight_attributes:
        print(now(), '    %s... %s' % (attr, getattr(hindsight, attr)))
    print(now(), 'done.')

    # start reading file for backtesting
    print(now(), 'now reading market data from stdin')
    reader = iter(sys.stdin)
    next(reader)

    # reference taker from me's market data
    list(map(lambda taker: taker.ref_md_from(me), takers))
    hindsight.ref_md_from(me)

    trigger_count, fill_count, evaluated_count, miss_count = 4*[0]
    kill_all = False
    try:
        for i, line in enumerate(reader):
            # matching engine must parse the data
            me.read_md(line[:-1])
            # taker benefits by just reading matching engine's data;
            # it only has to validate the market data
            for taker in takers:
                taker.update_valid_md()
                taker.evaluate_md()
                while taker.has_orders():
                    order = taker.remove_order()
                    me.recv_order(order)
                    if not order.is_valid():
                        print('invalid order: %s. quitting' % order)
                        kill_all = True
                        break
                    trigger_count += 1

            if kill_all:
                break

            while me.has_matches():
                order = me.remove_matched()
                hindsight.recv_order(order)
                fill_count += 1

            if hindsight.copy_md():
                hindsight.evaluate_orders()

                while hindsight.has_evaluated():
                    order = hindsight.remove_evaluated()
                    evaluated_count += 1
                    print(now(), order)
    except KeyboardInterrupt:
        print('KeyboardInterrupt! quitting')
    finally:
        print(now(), 'calling .gc() on all strategy objects')
        list(map(lambda taker: taker.gc(), takers))

    while me.has_misses():
        order = me.remove_missed()
        miss_count += 1
        print(now(), order)

    print(now(), 'done. lines read:   %d' % i)
    print(now(), 'number of triggers: %d' % trigger_count)
    print(now(), 'number of fills:    %d' % fill_count)
    print(now(), 'number of misses:   %d' % miss_count)
    print(now(), 'number evaluated:   %d' % evaluated_count)
