
import sys
import os
import numpy as np
import pandas as pd
import subprocess
import re
from time import sleep
import glob

if __name__ == '__main__':
    pcap_dir = "/glt/ssd/pcap/temp/"
    pcap_stor_dir = "/glt/storage/pcap/"
    parser_dir = "/glt/research/bin/"
    #parser_dir = "/glt/research/etc/go/src/pcap_parser/md_parser.go.20160803"
    #output_dir = "/glt/ssd/csv/"
    output_dir = "/glt/storage/csv/"
    
    datestr = sys.argv[1]
    pcap_stor_names = []
    pcapnames = glob.glob("%sethMonitor[A|B]*%s*.pcap"%(pcap_dir, datestr))
    print(pcapnames)
    if not len(pcapnames):
        pcap_stor_names = glob.glob("%sethMonitor[A|B]*%s*.pcap.gz"%(pcap_stor_dir, datestr))
        if len(pcap_stor_names):
            for pname in pcap_stor_names:
                os.system("gunzip %s"%pname)
        
            pcapnames = glob.glob("%sethMonitor[A|B]*%s*.pcap"%(pcap_stor_dir, datestr))

    for pcapname in pcapnames:
        parser_line = "%smdparser -md -pcap %s -id %s -dir %s"%(parser_dir, pcapname, "parsed", output_dir)
        print(parser_line)
        try:
            os.system(parser_line)
        except:
            print("Parsed file may already exist?")
        finally:
            print("finished parsing %s pcap"%pcapname)
    
    os.system("gzip %s*%s*.csv"%(output_dir, datestr ))
    
    if len(pcap_stor_names):
        os.system("gzip %s*%s*.pcap"%(pcap_stor_dir, datestr))

