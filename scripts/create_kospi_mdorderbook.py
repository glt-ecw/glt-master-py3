import sys
import os
import subprocess
import json
import numpy as np
import pandas as pd
import datetime as dt

from glt.md.krx import reformat_krx_md_generator
from glt.db import autoclose_query


def query_kospi_secids(df, show_cmd=True):
    if 'issuecode' not in df:
        raise ValueError('"issuecode" not in columns')
    cmd = """
        SELECT id, exchangeid
        FROM SECURITIES.securities
        WHERE (
            exchangeid in (%s)
            AND
            exchangeid regexp '^KR4[123]01.*'
        )
    """ % (', '.join(['\'%s\'' % x for x in df['issuecode'].unique()]),)
    if show_cmd:
        print(cmd)
    return autoclose_query(cmd)


def query_front_kospi(secids, tdate, show_cmd=True):
    today = pd.to_datetime(tdate).date()
    cmd = """
        SELECT id, expiry, isoption
        FROM (
            SELECT id, expdate as expiry, 0 as isoption
            FROM SECURITIES.futures
            WHERE (
                expdate>='%s'
                AND
                id in (%s)
            )
            UNION ALL
            SELECT id, expdate as expiry, 1 as isoption
            FROM SECURITIES.options
            WHERE (
                expdate>='%s'
                AND
                id in (%s)
            )
        ) as futopt
        ORDER BY expiry
    """ % (today, secids, today, secids)
    if show_cmd:
        print(cmd)
    contracts = autoclose_query(cmd)
    isoption = contracts['isoption'] == 1
    options_expiry = contracts.loc[isoption, 'expiry'].min()
    futures_expiry = contracts.loc[~isoption, 'expiry'].min()
    relevant_options = isoption & (contracts['expiry'] == options_expiry)
    relevant_futures = (~isoption) & (contracts['expiry'] == futures_expiry)
    return contracts.loc[relevant_options | relevant_futures]


def read_msg_csv(tdate, msg, filename_fmt, show_cmd=True):
    filename = filename_fmt % (msg, tdate)
    read_csv_options = {
        'engine': 'c'
    }
    dat = pd.read_csv(filename, **read_csv_options)

    ko_defs = query_kospi_secids(dat, show_cmd=show_cmd)

    secids = ', '.join(map(str, ko_defs['id'].values))

    front = query_front_kospi(secids, tdate, show_cmd=show_cmd)
    front = pd.merge(front, ko_defs, on='id', how='left')

    select = dat['issuecode'].isin(front['exchangeid'].values)
    if 'bidqty0' in dat:
        select &= (dat['bidqty0'] != 0) & (dat['askqty0'] != 0)

    return dat.loc[select], front


def order_book_validation(data, test_data):
    check_start = dt.datetime.now()
    print(check_start, 'checking order books...', end=' ')
    book_col_sstr = set(['bidq', 'askq', 'bidp', 'askp'])
    book_cols = [x for x in data.columns if any(sc in x for sc in book_col_sstr)]

    data_book_only = data['msg_int'] > 5
    test_data_book_only = test_data['topic'].str.contains('md[BG].*', regex=True)

    data_trim = data.loc[data_book_only, book_cols]
    data_trim.loc[:, [x for x in data_trim.columns if 'prc' in x]] = \
        (data_trim.loc[:, [x for x in data_trim.columns if 'prc' in x]] * 100).round().astype(int)
    test_data_trim = test_data.loc[test_data_book_only, book_cols]

    data_trim.index = data.loc[data_book_only, 'time'].astype(int)
    test_data_trim.index = test_data.loc[test_data_book_only, 'time']

    data_trim = data_trim.sort_index()
    test_data_trim = test_data_trim.sort_index()

    if len(data_trim) != len(test_data_trim) or (data_trim.values - test_data_trim.values).sum() != 0:
        print('failed!', end=' ')
    else:
        print('passed!', end=' ')
    print('dt=%s' % (dt.datetime.now() - check_start))


def a3_validation(test_data):
    check_start = dt.datetime.now()
    print(check_start, 'checking a3 message processing...', end=' ')
    toplevel_cols = [
        'topic', 'secid', 'seqnum', 'time',
        'buyprice', 'buyvolume', 'sellprice', 'sellvolume'
    ]
    two_level_col = [
        'bidqty0', 'bidpar0', 'bidprc0', 'askprc0', 'askqty0', 'askpar0',
        'bidqty1', 'bidpar1', 'bidprc1', 'askprc1', 'askqty1', 'askpar1',
    ]
    toplevel_cols += two_level_col

    uniq_secids = test_data['secid'].unique()

    test_data_trim = test_data.loc[:, toplevel_cols]

    for col in two_level_col:
        test_data_trim['prev_%s' % col] = np.nan

    for secid in uniq_secids:
        sel_secid = test_data['secid'] == secid
        contract = test_data.loc[sel_secid, toplevel_cols]
        for col in two_level_col:
            test_data_trim.loc[sel_secid, 'prev_%s' % col] = contract[col].shift(1)

    for col in two_level_col:
        test_data_trim['prev_%s' % col] = test_data_trim['prev_%s' % col].fillna(0).astype(int)

    a3_check = test_data_trim.loc[test_data_trim['topic'].str.contains('mdA3')]

    buys_invalid = (a3_check['buyvolume'] > 0) & (
    (a3_check['buyprice'] == a3_check['askprc0']) | (a3_check['askprc0'] != a3_check['prev_askprc1']))
    sells_invalid = (a3_check['sellvolume'] > 0) & (
    (a3_check['sellprice'] == a3_check['bidprc0']) | (a3_check['bidprc0'] != a3_check['prev_bidprc1']))

    if len(a3_check.loc[buys_invalid | sells_invalid]):
        print('failed', end=' ')
    else:
        print('passed', end=' ')
    print('dt=%s' % (dt.datetime.now() - check_start))


def format_data_columns(data, defs):
    id_map = defs['id'].to_dict()
    data['secid'] = data['issuecode'].map(id_map)

    msg_int_map = {
        'A3': 3,
        'B6': 6,
        'G7': 7
    }

    data['msg_int'] = data['msg'].map(msg_int_map)

    del_cols = [
        'timestr', 'issuecode', 'msg'
    ]

    for col in del_cols:
        if col in data:
            del data[col]

    for col in [x for x in data.columns if 'qty' in x or 'par' in x]:
        data[col] = data[col].fillna(0).round().astype(np.int64)

    for col in [x for x in data.columns if 'prc' in x]:
        data[col] = data[col].fillna(0)


def read_config(config_filename):
    param = {}
    with open(config_filename, 'r') as config_file:
        config = json.load(config_file)

    param['input_basename_fmt'] = config['rawCSVBasenameFormat']
    param['output_filename_fmt'] = config['kospiFrontMonthFilenameFormat']
    param['show_cmd'] = config['showSQLCommands']
    param['time_format'] = config['timeFormat']
    return param


if __name__ == '__main__':
    cfg_path = os.path.join(os.environ['GLT_MASTER_PATH'], 'cfg')

    input_filepath, tdate = sys.argv[1:]

    script_cfg_filename = os.path.join(cfg_path, 'krx.md.json')
    param = read_config(script_cfg_filename)

    input_basename_fmt = param['input_basename_fmt']
    output_filename_fmt = param['output_filename_fmt']
    show_cmd = param['show_cmd']
    time_format = param['time_format']

    input_filename_fmt = os.path.join(input_filepath, input_basename_fmt)

    print(dt.datetime.now(), 'reading csv files')

    data, defs = [], []
    for msg in ['a3', 'b6', 'g7']:
        msg_dat, msg_defs = read_msg_csv(tdate, msg, input_filename_fmt, show_cmd=show_cmd)
        data.append(msg_dat)
        defs.append(msg_defs)

    data = pd.DataFrame().append(data)
    data['time'] = pd.to_datetime(data['timestr'], format=time_format)
    data = data.sort_values('time')
    data.index = np.arange(len(data), dtype=np.int64)

    defs = pd.DataFrame().append(defs)
    defs = defs.drop_duplicates('id').sort_values('exchangeid')
    defs.index = defs['exchangeid']

    format_data_columns(data, defs)

    output_filename = output_filename_fmt % tdate
    print(dt.datetime.now(), 'finished processing raw market data. writing to %s' % output_filename)

    with open(output_filename, 'w') as f:
        for line in reformat_krx_md_generator(data):
            f.write(line)
    print(dt.datetime.now(), 'finished writing. validating output')
    test_data = pd.read_csv(output_filename)

    print(dt.datetime.now(), 'pd.read_csv(%s) finished' % output_filename)

    order_book_validation(data, test_data)
    a3_validation(test_data)

    print(dt.datetime.now(), 'done. now gzipping')

    subprocess.call(['gzip', '-f', output_filename])