from struct import Struct, unpack
#import numpy as np
#import pandas as pd

import sys
import binascii

# helpful functions for unpacking bytes
def unpack_it(struct, bytes, st):
    size = struct.size
    data = struct.unpack(bytes[st:st + size])
    return size, data

def unpack_formatted(struct, bytes, st, label, formatter):
    size, data = unpack_it(struct, bytes, st)
    
    if formatter == 'QUAD_WORD':
        chars = [('%x' % d).zfill(2) for d in data]
        formatted = '[%s:%s]' % (''.join(chars[:4]), ''.join(chars[4:]))
    else:
        formatted = formatter % data
    return size, {label: formatted}

def unpack_value(struct, bytes, st, label):
    size, data = unpack_it(struct, bytes, st)
    return size, {label: data[0]}

def unpack_items(struct, bytes, st, items):
    size, data = unpack_it(struct, bytes, st)
    output = {}
    for item, dat in zip(items, data):
        output[item] = dat
    return size, output


# helpful closures
def StructNull(endian_bytes):
    struct = Struct(endian_bytes)
    
    def unpack_func(bytes, st):
        return struct.size, {}

    return unpack_func


def StructFormatted(name, endian_bytes, fmt):
    struct = Struct(endian_bytes)
    
    def unpack_func(bytes, st):
        return unpack_formatted(struct, bytes, st, name, fmt)
    
    return unpack_func


def StructItems(items, endian_bytes):
    struct = Struct(endian_bytes)
    
    def unpack_func(bytes, st):
        return unpack_items(struct, bytes, st, items)
    
    return unpack_func


def StructValue(label, endian_bytes):
    struct = Struct(endian_bytes)
    
    def unpack_func(bytes, st):
        return unpack_value(struct, bytes, st, label)
    
    return unpack_func

execution_timestamp_items = [
    'seconds',
    'ns'
]

matchid_items = [
     'execution_event_nbr_u',
     'match_group_nbr_u',
     'match_item_nbr_u'
 ]


### find the defs here: https://docs.python.org/3/library/struct.html

### BD6 struct defs
###################
j_trading_code = StructFormatted('trading_code', '<2s5s5s', '[%s,%s,%s]')
j_series = StructFormatted('series', '<BBBBHHi', '[%d,%d,%d,%d,%d,%d,%d]')
j_give_up = StructFormatted('give_up', '<2s5s1s', '[%s,%s,%s]')
j_quad_word = StructFormatted('quad_word', '<8B', 'QUAD_WORD')
j_sequence_number_i = StructValue('sequence_number_i', '<i')
j_trade_number_i = StructValue('trade_number_i', '<i')
j_deal_price_i = StructValue('deal_price_i', '<i')
j_trade_quantity_i = StructValue('trade_quantity_i', '<q')
j_account = StructFormatted('account', '<2s5s16s1s', '[%s,%s,%s,%s]')
j_customer_info_s = StructFormatted('customer_info_s', '<15s', '[%s]')
j_bought_or_sold_c = StructValue('bought_or_sold_c', '<B')
j_deal_source_c = StructValue('deal_source_c', '<B')
j_open_close_req_c = StructValue('open_close_req_c', '<B')
j_trade_type_c = StructValue('trade_type_c', '<B')
j_le_state_c = StructValue('le_state_c', '<B')
j_user_code = StructFormatted('user_code', '<2s5s5s', '[%s,%s,%s]')
j_created_date_s = StructFormatted('created_date_s', '<8s', '[%s]')
j_created_time_s = StructFormatted('created_time_s', '<6s', '[%s]')
j_asof_date_s = StructFormatted('asof_date_s', '<8s', '[%s]')
j_asof_time_s = StructFormatted('asof_time_s', '<6s', '[%s]')
j_modified_date_s = StructFormatted('modified_date_s', '<8s', '[%s]')
j_modified_time_s = StructFormatted('modified_time_s', '<6s', '[%s]')
j_trade_state_c = StructFormatted('trade_state_c', '<B', '[%d]')
j_attention_c = StructFormatted('attention_c', '<B', '[%d]')
j_deal_number_i = StructValue('deal_number_i', '<i')
j_global_deal_no_u = StructValue('global_deal_no_u', '<I')
j_orig_trade_number_i = StructFormatted('orig_trade_number_i', '<i', '[%d]')
j_OrigSeries = StructFormatted('OrigSeries', '<BBBBHHi', '[%d,%d,%d,%d,%d,%d,%d]')
#fillercrap_2_s = StructFormatted('fillercrap_2_s', '<2s', '[%s]')
j_exchange_info_s = StructFormatted('exchange_info_s', '<cc20s10s', '%s%s%s%s')
#exchange_info_s = StructFormatted('exchange_info_s', '<32s', '%s')
j_big_attention_u = StructFormatted('big_attention_u', '<I', '[%d]')
j_clearing_date_s = StructFormatted('clearing_date_s', '<8s', '[%s]')
j_ExecutionTimestamp = StructItems(execution_timestamp_items, '<Ii')
j_trade_venue_c = StructFormatted('trade_venue_c', '<B', '[%d]')
j_instance_c = StructFormatted('instance_c', '<B', '[%d]')
j_exch_order_type_n = StructFormatted('exch_order_type_n', '<H', '[%d]')
j_Party = StructFormatted('Party', '<8s', '[%s]')
j_trade_rep_code_n = StructFormatted('trade_rep_code_n', '<H', '[%d]')
j_state_number_n = StructFormatted('state_number_n', '<H', '[%d]')
j_MatchId = StructItems(matchid_items, '<QII')

#########################
### BD4 struct defs
#########################
trading_code = StructFormatted('trading_code', '<2s5s5s', '[%s,%s,%s]')
series = StructFormatted('series', '<BBBBHHi', '[%d,%d,%d,%d,%d,%d,%d]')
account = StructFormatted('account', '<20s', '[%s]')
user_code = StructFormatted('user_code', '<2s5s5s', '[%s,%s,%s]')
countersigncode = StructFormatted('countersigncode', '<2s5s5s', '[%s,%s,%s]')
newseries = StructFormatted('newseries', '<BBBBHHi', '[%d,%d,%d,%d,%d,%d,%d]')
party = StructFormatted('party', '<8s', '[%s]')
posaccount = StructFormatted('posaccount', '<20s', '[%s]')
origseries = StructFormatted('origseries', '<BBBBHHi', '[%d,%d,%d,%d,%d,%d,%d]')
comboseries = StructFormatted('comboseries', '<BBBBHHi', '[%d,%d,%d,%d,%d,%d,%d]')
matchseqnbr = StructFormatted('matchseqnbr', '<IIIHH', '[%d,%d,%d,%d,%d]')
sequence_number_i = StructValue('sequence_number_i', '<i')
trade_number_i = StructValue('trade_number_i', '<i')
orig_trade_number_i = StructValue('orig_trade_number_i', '<i')
deal_price_i = StructValue('deal_price_i', '<i')
trade_quantity_i = StructValue('trade_quantity_i', '<i')
deal_number_i = StructValue('deal_number_i', '<i')
global_deal_no_u = StructValue('global_deal_no_u', '<i')
ext_seq_nbr_i = StructValue('ext_seq_nbr_i', '<i')
ext_status_i = StructValue('ext_status_i', '<i')
rem_quantity_i = StructValue('rem_quantity_i', '<i')
quantity_i = StructValue('quantity_i', '<i')
quad_word = StructFormatted('quad_word', '<8B', 'QUAD_WORD')
ext_trade_number_i = StructValue('ext_trade_number_i', '<I')
orig_ext_trade_number_i = StructValue('orig_ext_trade_number_i', '<I')
residual_i = StructValue('residual_i', '<i')
combo_deal_price_i = StructValue('combo_deal_price_i', '<i')
created_date_s = StructFormatted('created_date_s', '<8s', '[%s]')
created_time_s = StructFormatted('created_time_s', '<6s', '[%s]')
asof_date_s = StructFormatted('asof_date_s', '<8s', '[%s]')
asof_time_s = StructFormatted('asof_time_s', '<6s', '[%s]')
modified_date_s = StructFormatted('modified_date_s', '<8s', '[%s]')
modified_time_s = StructFormatted('modified_time_s', '<6s', '[%s]')
customer_info_s = StructFormatted('customer_info_s', '<15s', '[%s]')
clearing_date_s = StructFormatted('clearing_date_s', '<8s', '[%s]')
passthrough_s = StructFormatted('passthrough_s', '<32s', '[%s]')
ex_client_s = StructFormatted('ex_client_s', '<10s', '[%s]')
filler_2_s = StructFormatted('fillercrap_2_s', '<2s', '[%s]')
reserved_2_s = StructFormatted('reserved_2_s', '<2s', '[%s]')
orig_trade_type_c = StructValue('orig_trade_type_c', '<B')
bought_or_sold_c = StructValue('bought_or_sold_c', '<B')
deal_source_c = StructValue('deal_source_c', '<B')
open_close_req_c = StructFormatted('open_close_req_c', '<B', '[%d]')
open_close_c = StructFormatted('open_close_c', '<B', '[%d]')
trade_type_c = StructFormatted('trade_type_c', '<B', '[%d]')
filler_1_s = StructFormatted('fillercrap_1_s', '<1s', '[%s]')
trade_state_c = StructFormatted('trade_state_c', '<B', '[%d]')
attention_c = StructFormatted('attention_c', '<B', '[%d]')
account_type_c = StructFormatted('account_type_c', '<B', '[%d]')
instigant_c = StructFormatted('instigant_c', '<B', '[%d]')
cab_price_ind_c = StructFormatted('cab_price_ind_c', '<B', '[%d]')
ext_trade_fee_type_c = StructFormatted('ext_trade_fee_type_c', '<1s', '[%s]')
nbr_held_q = StructValue('nbr_held_q', '<q')
nbr_written_q = StructValue('nbr_written_q', '<q')
total_held_q = StructValue('total_held_q', '<q')
total_written_q = StructValue('total_written_q', '<q')
commission_i = StructValue('commission_i', '<i')
give_up = StructFormatted('give_up', '<8s', '[%s]')
give_up_number_i = StructValue('give_up_number_i', '<i')
give_up_state_c = StructValue('give_up_state_c', '<B')
le_state_c = StructValue('le_state_c', '<B')
instance_c = StructValue('instance_c', '<B')
trade_venue_c = StructValue('instance_c', '<B')
big_attention_u = StructFormatted('big_attention_u', '<I', '[%d]')

unpacking_map = {
    3: [
        'CL_TRADE_BASE_API', 
        [
            j_trading_code,
            j_series,
            j_give_up,
            j_quad_word,
            j_sequence_number_i,
            j_trade_number_i,
            j_deal_price_i,
            j_trade_quantity_i,
            j_account,
            j_customer_info_s,
            j_bought_or_sold_c,
            j_deal_source_c,
            j_open_close_req_c,
            j_trade_type_c,
            j_le_state_c,
            j_user_code,
            j_created_date_s,
            j_created_time_s,
            j_asof_date_s,
            j_asof_time_s,
            j_modified_date_s,
            j_modified_time_s,
            j_trade_state_c,
            j_attention_c,
            j_deal_number_i,
            j_global_deal_no_u,
            j_orig_trade_number_i,
            j_OrigSeries,
            j_exchange_info_s,
            j_big_attention_u,
            j_clearing_date_s,
            j_ExecutionTimestamp,
            j_trade_venue_c, 
            j_instance_c,
            j_exch_order_type_n,
            j_Party,
            j_trade_rep_code_n,
            j_state_number_n,
            j_MatchId,
        ]
    ],
    1: [
        'CL_TRADE_BASE_API',
        [
            trading_code,
            series,
            account,
            user_code,
            countersigncode,
            newseries,
            party,
            posaccount,
            origseries,
            comboseries,
            matchseqnbr,
            sequence_number_i,
            trade_number_i,
            orig_trade_number_i,
            deal_price_i,
            trade_quantity_i,
            deal_number_i,
            global_deal_no_u,
            ext_seq_nbr_i,
            ext_status_i,
            rem_quantity_i,
            quantity_i,
            quad_word,
            ext_trade_number_i,
            orig_ext_trade_number_i,
            residual_i,
            combo_deal_price_i,
            created_date_s,
            created_time_s,
            asof_date_s,
            asof_time_s,
            modified_date_s,
            modified_time_s,
            customer_info_s,
            clearing_date_s,
            passthrough_s,
            ex_client_s,
            filler_2_s,
            reserved_2_s,
            orig_trade_type_c,
            bought_or_sold_c,
            deal_source_c,
            open_close_req_c,
            open_close_c,
            trade_type_c,
            filler_1_s,
            trade_state_c,
            attention_c,
            account_type_c,
            instigant_c,
            cab_price_ind_c,
            ext_trade_fee_type_c,
            nbr_held_q,
            nbr_written_q,
            total_held_q,
            total_written_q,
            commission_i,
            give_up,
            give_up_number_i,
            give_up_state_c,
            le_state_c,
            instance_c,
            trade_venue_c,
            big_attention_u,
        ]
    ]

}

def unpack_bytes(bytes, unpack_list):
    s_idx = 0
    i, n = 0, len(bytes)
    values = {}
    while i < n and s_idx < len(unpack_list):
        unpack_func = unpack_list[s_idx]
        size, data = unpack_func(bytes, i)
        values.update(data)
        s_idx += 1
        i += size
    return values

def unpack_bytes_test(bytes, unpack_list):
    s_idx = 0
    i, n = 0, len(bytes)
    values = {}
    print(unpack_list)
    while i < n:
        print((n, i, s_idx))
        unpack_func = unpack_list[s_idx]
        size, data = unpack_func(bytes, i)
        print(data)
        values.update(data)
        s_idx += 1
        i += size
    return values

info_keys = {
    'bd6': ('deal_number_i', 'quad_word', 'series', 'deal_price_i','trade_quantity_i','bought_or_sold_c', 'exchange_info_s'),
    'bd4': ('global_deal_no_u', 'quad_word', 'series', 'deal_price_i','trade_quantity_i','bought_or_sold_c', 'customer_info_s')
}

def apply_fill_struct(hex_str):
    parsed_str = ""
    if "424406" in hex_str:
        msg_t = "BD6"
        msg = binascii.unhexlify(hex_str.strip())
        i = 8 
        struct_type, struct_size = unpack('<HH', msg[i:i+4])
        #print struct_type, struct_size
        #if struct_type not in unpacking_map.keys() or unpacking_map[struct_type] is None:
        #    print "struct_type: %d not found." %struct_type
        #    return "NA",", , , , , , ,"
       
        i += 4 
        struct_size -= 4
        try: 
            struct_name, unpack_list = unpacking_map[struct_type]
            #print struct_name, unpack_list
            unpacked = unpack_bytes(msg[i:i+struct_size], unpack_list)
            #print unpacked
        except:
            return "NA",", , , , , , ,"
        for item in info_keys['bd6']:
            if item in list(unpacked.keys()):
                if item in ("exchange_info_s"):
                    #exch_info = unpacked[item].replace(" ", "")[-21:]
                    parsed_str += "%s, "%unpacked[item].replace(" ", "")
                    #parsed_str += "%s, "%exch_info
                    #print parsed_str
                else:
                    parsed_str += "%s, "%str(unpacked[item])
            else:
                parsed_str += "%s, "%''

    elif "424404" in hex_str:
        msg_t = "BD4"
        msg = binascii.unhexlify(hex_str.strip())
        i = 4
        struct_type = 1
        struct_name, unpack_list = unpacking_map[struct_type]
        unpacked = unpack_bytes(msg[i:], unpack_list)
        #print unpacked
        for item in info_keys['bd4']:
            if item in list(unpacked.keys()):
                if item in ("customer_info_s"):
                    #cust_info = unpacked[item].replace(" ", "")[5:13]
                    parsed_str += "%s, "%unpacked[item].replace(" ", "")
                    #parsed_str += "%s, "%cust_info
                    #print parsed_str
                else:
                    parsed_str += "%s, "%str(unpacked[item])
            else:
                parsed_str += "%s, "%''
    else:
        return "NA",", , , , , , ,"

    return msg_t, parsed_str
