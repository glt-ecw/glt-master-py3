import sys
import json
import datetime as dt
import pandas as pd


BIZDAYS = 260.0


"""
{
    "publisherName": "test publisher wuat",
    "modelID": 2,
    "mdFwdChannel": "10.127.33.121;239.127.101.105:10500",
    "pubChannel": "10.127.33.121;239.127.101.106:10600",
    "logging": {
        "filenameFormat": "/tmp/pairs_publishing.%s.log",
        "debug": false
    },
    "models": [
        {
            "module": "glt.modeling.spread_pairs.kospi_options",
            "object": "KospiOptionSpreadEvaluatorV1",
            "parameters": {
                "secid_a": 584602,
                "secid_b": 584905,
                "secid_u": 574602,
                "interval": 200000000,
                "option_type": 1,
                "tte": 0.08076923076923077,
                "interest_rate": 0.015,
                "strike_over": 315.0,
                "strike_under": 317.5,
                "u_distance": 0.50,
                "half_life": 600e9,
                "price_multiplier": 100.0,
                "refit_interval": 10000000000,
                "poly_n": 1,
                "residual_threshold": 0.003,
                "pickle_filename": "/tmp/test_kospi1.p"
            }
        },
        {
            "module": "glt.modeling.spread_pairs.kospi_options",
            "object": "KospiOptionSpreadEvaluatorV1",
            "parameters": {
                "secid_a": 584397,
                "secid_b": 584549,
                "secid_u": 574602,
                "interval": 200000000,
                "option_type": 1,
                "tte": 0.08076923076923077,
                "interest_rate": 0.015,
                "strike_over": 307.5,
                "strike_under": 310.0,
                "u_distance": 0.50,
                "half_life": 600e9,
                "price_multiplier": 100.0,
                "refit_interval": 10000000000,
                "poly_n": 1,
                "residual_threshold": 0.003,
                "pickle_filename": "/tmp/test_kospi2.p"
            }
        }
    ]
}


"""




if __name__ == '__main__':
    cfg_builder_filename, cfg_filename = sys.argv[1:]
    tdate = dt.datetime.today().strftime('%Y%m%d')
    print('creating config for', tdate)

    with open(cfg_builder_filename, 'r') as f:
        cfg_builder = json.load(f)

    h5_filename = cfg_builder['hdfFilenameFormat'] % tdate
    del cfg_builder['hdfFilenameFormat']

    base_parameters = cfg_builder['baseParameters']
    del cfg_builder['baseParameters']

    num_pairs = cfg_builder['numberOfPairs']
    del cfg_builder['numberOfPairs']

    min_tick = cfg_builder['minTick']
    del cfg_builder['minTick']

    if len(sys.argv) > 2:
        center_price = sys.argv[2]

    with pd.HDFStore(h5_filename, 'r') as h5:
        md_info = h5['info']
    ko_info = md_info.loc[md_info['shortname'].str.contains(r'^KO')&(md_info['dte']>1.1)]
    # futures
    futures = ko_info.loc[ko_info['op']==0].sort_values('expiry').iloc[0]
    secid_u = futures['secid']

    # options
    options = ko_info.loc[(ko_info['op']!=0)&(ko_info['minprc']>min_tick)]
    options = options.loc[options['dte']==options['dte'].min()]

    tte = (options.iloc[0]['dte'] - 1) / BIZDAYS

    # append model parameters to list
    pairs = []

    # starting with the calls
    calls = options.loc[options['op']>0].sort_values('strike', ascending=False)
    for i in range(1, len(calls)):
        if i > num_pairs:
            print('hit num_pairs=%d' % num_pairs)
            break
        secid_a, strike_over = calls.iloc[i][['secid', 'strike']]
        secid_b, strike_under = calls.iloc[i-1][['secid', 'strike']]
        if strike_under - strike_over > 2.51:
            print('skipping call pair:', strike_over, strike_under)
            print('and quitting...')
            break
        pair = base_parameters.copy()
        model = pair['parameters'].copy()
        model['secid_a'] = secid_a
        model['secid_b'] = secid_b
        model['secid_u'] = secid_u
        model['option_type'] = 1
        model['tte'] = tte
        model['strike_over'] = strike_over
        model['strike_under'] = strike_under
        model['pickle_filename'] = '/tmp/kospi_spreads.%s.%d.%d.p' % (tdate, secid_a, secid_b)
        pair['parameters'] = model
        pairs.append(pair)

    puts = options.loc[options['op']<0].sort_values('strike', ascending=True)
    for i in range(1, len(puts)):
        if i > num_pairs:
            print('hit num_pairs=%d' % num_pairs)
            break
        secid_a, strike_over = puts.iloc[i][['secid', 'strike']]
        secid_b, strike_under = puts.iloc[i-1][['secid', 'strike']]
        if strike_over - strike_under > 2.51:
            print('skipping put pair:', strike_over, strike_under)
            print('and quitting...')
            break
        pair = base_parameters.copy()
        model = pair['parameters'].copy()
        model['secid_a'] = secid_a
        model['secid_b'] = secid_b
        model['secid_u'] = secid_u
        model['option_type'] = -1
        model['tte'] = tte
        model['strike_over'] = strike_over
        model['strike_under'] = strike_under
        model['pickle_filename'] = '/tmp/kospi_spreads.%s.%d.%d.p' % (tdate, secid_a, secid_b)
        pair['parameters'] = model
        pairs.append(pair)

    cfg_builder['models'] = pairs

    with open(cfg_filename, 'w') as f:
        json.dump(cfg_builder, f, sort_keys=True, indent=4, separators=(',', ': '))
