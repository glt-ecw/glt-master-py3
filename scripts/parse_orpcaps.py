import pandas as pd

import numpy as np
import random
import sys, os
from datetime import datetime

import time
import argparse
import csv
import collections, itertools
from itertools import groupby

import subprocess
from pandas import DataFrame, Series
from pandas import Timestamp
import matplotlib.pyplot as plt
import glob

datestr = sys.argv[1]
ormap_fname = '/glt/storage/pcapmap/%s.orgatewaymap'%datestr
if os.path.isfile(ormap_fname): 
    orport_map = pd.read_csv(ormap_fname)
    orport_map.columns = 'order','server','ip','port'
    orport_map.drop_duplicates(subset=['server','ip','port'], inplace=True)
    orport_map['port'] = orport_map['port'].values.astype(int)
    orport_map['server'] = orport_map['server'].values.astype(int)
    print(orport_map)
else:
    print("Or map does not exist. exiting")
    sys.exit()

directory = '/glt/storage/csv'
pcaps = glob.glob('/glt/ssd/pcap/temp/eth*[A|B].%s*.pcap'%datestr)
storage_pcaps = glob.glob('/glt/storage/pcap/eth*[A.temp|B].%s*.pcap.gz'%datestr)
parse_path = '/glt/research/etc/go/src/pcap_parser'
print(pcaps)

parsed = glob.glob('/glt/storage/csv/*%s*.csv*'%datestr)
if parsed:
    os.system('rm /glt/storage/csv/*%s*.8*.csv*'%datestr)

if pcaps:
    for pcap in pcaps:
        print(pcap)
        for port in orport_map.port.unique():
            print(pcap, port)
            os.system('%s/pcap_orparser -orders -pcap %s -oeport %d -dir %s' %(parse_path, pcap, port, directory))

elif storage_pcaps:
    os.system("gunzip /glt/storage/pcap/eth*%s*.pcap.gz"%datestr)
    gunzipped_storage_pcaps = glob.glob('/glt/storage/pcap/eth*[A.temp|B].*%s*.pcap'%datestr)

    for pcap in gunzipped_storage_pcaps:
       for port in orport_map.port.unique():
           print(pcap, port)
           os.system('%s/pcap_orparser -orders -pcap %s -oeport %d -dir %s' %(parse_path, pcap, port, directory))
    
    os.system("gzip /glt/storage/pcap/eth*%s*"%datestr)
    
os.system("gzip -f /glt/storage/csv/eth*%s*.csv"%datestr)

