import pandas as pd
import numpy as np
import sys
import os
import subprocess

datestr = sys.argv[1]

parsed = []
servers=['jpa01','jpa02','jpa03','jpa04','jpa05','jpa06']

for server in servers:
    print(server)
    orfile = '/glt/storage/%s/ORMsgs.%s.%s.log.gz'%(server, datestr, server)
    if os.path.isfile(orfile):
        cmd = subprocess.Popen('zgrep CONNECTION_PARMS %s' %(orfile), shell=True, stdout=subprocess.PIPE)
        for line in cmd.stdout:
            if 'localPort' in line:
                values = line.split('|')
                parsed.append([server, int(values[5]), values[11], int(values[13]), int(values[15][:-2])])

columns = 'server','connid','ip','port','srcport'
res = pd.DataFrame(parsed, columns=columns)
res.to_csv('/glt/storage/pcapmap/connid_map.%s.csv'%datestr)
    
