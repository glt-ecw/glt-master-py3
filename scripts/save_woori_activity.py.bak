import sys
import os
import re
import json
import pandas as pd
import glt.db


# e.g. python compute_woori_pnl.py /glt/ssd/txt/layout_011991_20160907.txt eod.woori_activity rdeio


PREVIOUS_OPEN_POSITION = 'Previous Open Position'

prefix_search_str = r'^(...)'

non_trade_line_search = re.compile(
    (
        r'('
        r'Page [0-9]+ of [0-9]+|Commission|Total|Trade Confirmation|NH Foundation bldg|Account Status|Fax:'
        r'|Yoido-dong'
        r')'
    )
)

ptval_prefix_map = {
    '101': 500000,
    '105': 100000,
    '201': 500000,
    '301': 500000,
    '205': 100000,
    '305': 100000,
    '165': 1000000,
    '167': 1000000,
    '175': 10000
}

trade_summary_cols = ['qty_intraday', 'traded_notional', 'fees']

ordered_merged_cols = [
    'exchangeid',
    'qty_open', 'price_open', 'cash_notional_open',
    'qty_close', 'price_close', 'cash_notional_close',
    'qty_intraday', 'traded_notional', 'fees', 'cash_pnl'
]

def read_config(json_filename):
    cfg, param = {}, {}
    with open(json_filename, 'r') as f:
         cfg = json.load(f)
    mysql_param = cfg['mysql']
    param['user'] = mysql_param['user']
    param['woori_table'] = mysql_param['table']
    return param


def is_relevant_line(line):
    return len(non_trade_line_search.findall(line)) == 0 and len(line) > 2


def find_side(direction):
    if direction == 'Long':
        return 1
    else:
        return -1


def parse_number(str_val, value_type):
    return value_type(str_val.replace(',', ''))


def format_trade_data(str_size, str_price, str_amount, str_fees):
    size = parse_number(str_size, int)
    price = parse_number(str_price, float)
    amount = abs(parse_number(str_amount, int))
    fees = -int(parse_number(str_fees, int))
    return [size, price, amount, fees]


def parse_previous_position(lines):
    heading_search = re.compile(r'\[[A-Za-z ]+\]')
    read_prev = False
    prev_pos = []
    last_heading = None
    for line in lines:
        heading = heading_search.findall(line)
        if heading:
            last_heading = heading[0]
        if not read_prev and last_heading == '[Previous Open Position]':
            read_prev = True
        elif read_prev:
            if last_heading != '[Previous Open Position]' and (heading_search.findall(line) or 'Total' in line):
                break
            elif heading or 'Series' in line:
                continue
            else:
                items = line.strip().split()
                data = [
                    items[0],
                    find_side(items[3]),
                    parse_number(items[4], int),
                    parse_number(items[5], float)
                ]
                prev_pos.append(data)
    prev_pos = pd.DataFrame(prev_pos, columns=['exchangeid', 'side', 'size', 'price_open'])
    prev_pos['qty_open'] = prev_pos['side'] * prev_pos['size']
    return prev_pos.loc[:, ['exchangeid', 'qty_open', 'price_open']]


def parse_current_position(lines):
    heading_search = re.compile(r'\[[A-Za-z ]+\]')
    read_curr = False
    curr_pos = []
    last_heading = None
    for line in lines:
        heading = heading_search.findall(line)
        if heading:
            last_heading = heading[0]
        if not read_curr and last_heading == '[Open Position]':
            read_curr = True
        elif read_curr:
            if last_heading != '[Open Position]' and (heading_search.findall(line) or 'Total' in line):
                break
            elif heading or 'Series' in line:
                continue
            else:
                items = line.strip().split()
                data = [
                    items[0],
                    find_side(items[3]),
                    parse_number(items[4], int),
                    parse_number(items[5], float),
                    abs(parse_number(items[6], int))
                ]
                curr_pos.append(data)
    curr_pos = pd.DataFrame(curr_pos, columns=['exchangeid', 'side', 'size', 'price_close', 'amount'])
    curr_pos['qty_close'] = curr_pos['side'] * curr_pos['size']
    curr_pos['point_value'] = curr_pos['exchangeid'].str.extract(prefix_search_str, expand=False).map(ptval_prefix_map)
    curr_pos['cash_notional_close'] = curr_pos['point_value'] * curr_pos['qty_close'] * curr_pos['price_close']
    curr_pos['cash_notional_close'] = curr_pos['cash_notional_close'].round().astype(int)
    ptvals = curr_pos.groupby('exchangeid').first()['point_value'].to_dict()
    return curr_pos.loc[:, ['exchangeid', 'qty_close', 'price_close', 'cash_notional_close']], ptvals


def check_for_expiration(items):
    new_items = []
    expired_str = 'optionExpired'
    for item in items:
        if expired_str in item:
            other_items = filter(lambda x: len(x), item.split(expired_str))
            num_other_items = len(other_items)
            if num_other_items == 0:
                new_items.append(item)
                continue
            elif num_other_items > 2:
                raise ValueError('%s item: %s, num_other_items>2' % (expired_str, item))
            elif item.endswith(expired_str):
                new_items.append(other_items[0])
                new_items.append(expired_str)
            elif item.beginswith(expired_str):
                new_items.append(expired_str)
                new_items.append(other_items[0])
            else:
                new_items.append(other_items[0])
                new_items.append(expired_str)
                new_items.append(other_items[1])
        else:
            new_items.append(item)
    return new_items, len(new_items)


def parse_trades(lines):
    trade_heading_search = re.compile(r'\[(Futures|Option) Trade\]')
    other_heading_search = re.compile(r'\[[A-Za-z][A-Za-z ]+\]')
    read_trades = False
    last_heading, exchangeid, direction, condition, data = 5 * [None]
    trades = []
    for line in lines:
        heading = trade_heading_search.findall(line)
        if heading:
            last_heading = heading[0]
        if not read_trades and heading:
            read_trades = True
        elif read_trades:
            if not heading and other_heading_search.findall(line):
                read_trades = False
            elif not non_trade_line_search.findall(line) and 'Trade' not in line:
                items = line.strip().split()
                num_items = len(items)
                if num_items == 0:
                    continue
                elif last_heading == 'Futures':
                    if num_items == 9:
                        exchangeid = items[0]
                        condition = items[2]
                        direction = find_side(items[3])
                        data = format_trade_data(items[4], items[5], items[6], items[8])
                    elif num_items == 7:
                        condition = items[0]
                        direction = find_side(items[1])
                        data = format_trade_data(items[2], items[3], items[4], items[6])
                    elif num_items == 6:
                        direction = find_side(items[0])
                        data = format_trade_data(items[1], items[2], items[3], items[5])
                    else:
                        data = format_trade_data(items[0], items[1], items[2], items[4])
                elif last_heading == 'Option':
                    items, num_items = check_for_expiration(items)
                    if num_items == 8:
                        exchangeid = items[0]
                        condition = items[2]
                        direction = find_side(items[3])
                        data = format_trade_data(items[4], items[5], items[6], items[7])
                    elif num_items == 6:
                        condition = items[0]
                        direction = find_side(items[1])
                        data = format_trade_data(items[2], items[3], items[4], items[5])
                    elif num_items == 5:
                        direction = find_side(items[0])
                        data = format_trade_data(items[1], items[2], items[3], items[4])
                    else:
                        data = format_trade_data(items[0], items[1], items[2], items[3])
                trades.append([last_heading, exchangeid, condition, direction] + data)
    # put the list of trade data in a DataFrame
    columns = [
        'contract_type', 'exchangeid', 'condition',
        'side', 'size', 'price', 'amount', 'fees'
    ]
    trades = pd.DataFrame(trades, columns=columns)
    trades['qty_intraday'] = trades['side'] * trades['size']
    trades['traded_notional'] = trades['side'] * trades['amount']
    trades['point_value'] = trades['exchangeid'].str.extract(prefix_search_str, expand=False).map(ptval_prefix_map)
    ptvals = trades.groupby('exchangeid').first()['point_value'].to_dict()

    # summary by exchangeid
    if len(trades) > 0:
        trade_summary = trades.groupby('exchangeid').sum()[trade_summary_cols]
        return trades, trade_summary, ptvals
    else:
        return None, None, {}


def delete_contents(conn, table, date, real_delete=False, show_cmd=True):
    cmd = """
        DELETE FROM %s WHERE date='%s'

    """ % (table, date)
    if show_cmd:
        print cmd
    if real_delete:
        conn.execute(cmd)


def insert_contract_data(conn, table, date, row,
                     real_insert=False, show_cmd=True):
    cmd = """
        INSERT INTO %s
        (
            date, exchangeid,
            qty_open, price_open, cash_notional_open,
            qty_close, price_close, cash_notional_close,
            qty_intraday, traded_notional, fees, cash_pnl
        )
        VALUES(
            '%s', '%s',
            %d, %0.4f, %d,
            %d, %0.4f, %d,
            %d, %d, %d, %d
        )
    """ % ((table, date) + row)
    if show_cmd:
        print cmd
    if real_insert:
        conn.execute(cmd)


def query_settles(conn, table, date):
    cmd = """
        SELECT date, exchangeid, price_close
        FROM %s
        WHERE date = (SELECT MAX(date) FROM %s where date<'%s')
    """ % (table, table, date)
    settles = conn.frame_query(cmd)
    if len(settles):
        return settles
    else:
        return None

if __name__ == '__main__':
    pd.options.mode.chained_assignment = None

    cfg_path = os.path.join(os.environ['GLT_MASTER_PATH'], 'cfg')
    script_cfg_filename = os.path.join(cfg_path, 'woori_eod.json')

    statement_filename = sys.argv[1]
    if not os.path.exists(statement_filename):
        print '%s does not exist. quitting' % statement_filename
        exit(1)

    param = read_config(script_cfg_filename)
    user = param['user']
    woori_table = param['woori_table']

    real_execute = user != 'rdeio'

    tdate = re.findall(r'_([0-9]{8}).txt', statement_filename)[0]
    today = pd.to_datetime(tdate, format='%Y%m%d').date()

    cfg_path = os.path.join(os.environ['GLT_MASTER_PATH'], 'cfg')
    mysql_cfg_filename = os.path.join(cfg_path, glt.db.CONFIG_BASENAME)
    config = glt.db.read_mysql_config(mysql_cfg_filename)

    # query for last statement's settles
    conn = None
    try:
        conn = glt.db.MysqlConnection(**config[user])

        delete_contents(conn, woori_table, today, real_delete=real_execute)
        conn.commit()

        settles = query_settles(conn, woori_table, today)

    finally:
        if conn is not None:
            conn.close()
            print 'closing connection'
        else:
            print 'connection non-existent'

    with open(statement_filename, 'r') as f:
        out = f.read()
    lines = out.split('\n')
    for i, line in enumerate(lines):
        if PREVIOUS_OPEN_POSITION in line or 'Trade]' in line:
            break
    lines = filter(is_relevant_line, lines[i:])

    # parse incoming position
    prev_pos = parse_previous_position(lines)

    # parse today's fills
    trades, trade_summary, ptvals = parse_trades(lines)

    # parse current position
    curr_pos, pos_ptvals = parse_current_position(lines)

    # what are the point values of each of the contracts we traded?
    ptvals.update(pos_ptvals)

    # add point values to prev_pos and compute notional
    prev_pos['point_value'] = prev_pos['exchangeid'].map(ptvals)
    if settles is not None:
        print 'settles??'
        print settles.to_string()
        tmp_index = pd.Series(settles.index).values
        settles.index = settles['exchangeid']
        settles_map = settles['price_close'].to_dict()
        settles.index = tmp_index
        prev_pos['price_open'] = prev_pos['exchangeid'].map(settles_map)
        if len(prev_pos.loc[prev_pos['price_open'].isnull()]):
            print 'NaN detected in price_open'
            print prev_pos.to_string()
            print 'price map'
            print settles.to_string()
            print 'skipping statement'
            exit(0)
    prev_pos['cash_notional_open'] = prev_pos['point_value'] * prev_pos['qty_open'] * prev_pos['price_open']
    print prev_pos.to_string()
    prev_pos['cash_notional_open'] = prev_pos['cash_notional_open'].round().astype(int)
    del prev_pos['point_value']

    merged = pd.merge(prev_pos, curr_pos, how='outer', on='exchangeid').fillna(0)

    if trade_summary is not None:
        merged = pd.merge(merged, trade_summary.reset_index(), how='outer', on='exchangeid')
        merged = merged.fillna(0)
    else:
        for col in trade_summary_cols:
            merged[col] = 0

    if len(merged) != len(merged['exchangeid'].unique()):
        raise ValueError('len(merged) != len(merged.exchangeid.unique())')

    int_cols = [
        'qty_open', 'qty_close',
        'cash_notional_open', 'cash_notional_close',
        'qty_intraday', 'traded_notional', 'fees'
    ]
    for col in int_cols:
        merged[col] = merged[col].round().astype(int)

    merged.index = merged['exchangeid']

    if trade_summary is not None:
        merged['traded_notional'] = trade_summary['traded_notional']
        merged['traded_notional'] = merged['traded_notional'].fillna(0).round().astype(int)
    merged['cash_pnl'] = merged['cash_notional_close'] - merged['cash_notional_open'] - merged['traded_notional']
    merged = merged.sort_index().loc[:, ordered_merged_cols]
    merged.index = range(len(merged))

    # now insert tables
    conn = None
    try:
        conn = glt.db.MysqlConnection(**config[user])

        for row in zip(*(merged[col].values for col in merged)):
            insert_contract_data(conn, woori_table, today, row, real_insert=real_execute)
        conn.commit()

    finally:
        if conn is not None:
            conn.close()
            print 'closing connection'
        else:
            print 'connection non-existent'

    print merged.to_string()
    print
    print merged['cash_pnl'].sum()
