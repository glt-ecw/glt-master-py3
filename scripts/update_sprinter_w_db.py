import numpy as np
import pandas as pd
import datetime
print('numpy', np.version.version)
print('pandas', pd.version.version)
import glt.db as db
import sys
import glob

DBSERVER01_CONN = {
    'host': 'dbserver01',
    'user': 'deio',
    'pw': '!w3alth!',
    'db': 'DEIO_W'
}

datestr = sys.argv[1]
date_db = datestr[:4]+'-'+datestr[4:6]+'-'+datestr[6:8]

### find qpos
stor_dir = '/glt/storage/txt/'
qpos_files = glob.glob(stor_dir + "qpos.*" + datestr + "*")
#print qpos_files

qpos_array = []
for qfile in qpos_files:
    with open(qfile, 'r') as f:
        filelines = f.read().split('\n')
        for line in filelines:
            msg = line.split("|")[-1].split(",")
            qpos_array.append([find_ix_substring(keyword, item) for keyword, item in zip(["id: ", "mdSeqNum: ", "qtyInFrontOfUs: "], msg)])

if len(qpos_array):
    qpos_df = pd.DataFrame(qpos_array, columns = ['id', 'mdSeqNum', 'qpos']).dropna()
    qpos_df['id'] = [int(x) for x in qpos_df.id.values]
    qpos_df['qpos'] = [int(x) for x in qpos_df.qpos.values]
else:
    print("No qpos file")

##############
#OnOpen sprints
burst_msg = 'KWU-KWU timer burst sprinter'
retry_msg = 'KWU-KWU timer sprinter with retry'

try:
    conn = db.conn(**DBSERVER01_CONN)

    burst_triggers = conn.frame_query(
        ("select distinct id, eventTime, usEventTime from orders where message like '%s%%' and orderStatus = 1 and tradedate='%s'") % (burst_msg, date_db)
    )
    
    if len(burst_triggers):
        burst_trig_tups = tuple(list(burst_triggers.id.values.astype(int)))

        burst_ids = conn.frame_query(
            ("select * from orders where (triggerId in %s or id in %s) and orderStatus in (1, 2) and tradedate='%s'") % (burst_trig_tups, burst_trig_tups, date_db)
        )
              
        burst_sprints = burst_ids.loc[burst_ids.orderStatus == 1]
        burst_sprints = burst_sprints.rename(columns={"eventTime": "sprintTime", "usEventTime": "usSprintTime"})

        burst_first_accepts = burst_ids.loc[burst_ids.orderStatus == 2]
        burst_first_accepts = burst_first_accepts.sort(['eventTime','usEventTime']).drop_duplicates(['id', 'securityId', 'serverId'])[['id', 'securityId', 'serverId', 'eventTime', 'usEventTime']]
        burst_first_accepts['sprinterType'] = 'burst'

        burst_first_accepts = burst_first_accepts.merge(burst_sprints.loc[:, ["id", "sprintTime", "usSprintTime"]], on='id', how='left')
    else:
        burst_first_accepts = pd.DataFrame()
        
    retry_triggers = conn.frame_query(
        ("select id, serverId, eventTime, usEventTime from orders where message like '%s%%' and orderStatus = 1 and tradedate='%s'") % (retry_msg, date_db)
    )

    retry_triggers = retry_triggers.rename(columns={"eventTime": "sprintTime", "usEventTime": "usSprintTime"})
    if len(retry_triggers) > 1:
        retry_ids = conn.frame_query(
            ("select * from orders where orderStatus = 2 and id in %s and tradedate='%s' "
             " order by eventTime, usEventTime") % (tuple(retry_triggers.drop_duplicates('serverId').id.unique().astype(int)), date_db)
            )

        retry_first_accepts = retry_ids.sort(['eventTime','usEventTime']).drop_duplicates(['id', 'securityId', 'serverId'])[['id', 'securityId', 'serverId', 'eventTime', 'usEventTime']]
        retry_first_accepts['sprinterType'] = 'retry'
        retry_first_accepts = retry_first_accepts.merge(retry_triggers.loc[:, ["id", "sprintTime", "usSprintTime"]], on='id', how='left')
    elif len(retry_triggers) == 1:
        retry_ids = conn.frame_query(
            ("select * from orders where orderStatus = 2 and id = %s and tradedate='%s' "
             " order by eventTime, usEventTime") % (retry_triggers.drop_duplicates('serverId').id.unique().astype(int)[0], date_db)
            )

        retry_first_accepts = retry_ids.sort(['eventTime','usEventTime']).drop_duplicates(['id', 'securityId', 'serverId'])[['id', 'securityId', 'serverId', 'eventTime', 'usEventTime']]
        retry_first_accepts['sprinterType'] = 'retry'
        retry_first_accepts = retry_first_accepts.merge(retry_triggers.loc[:, ["id", "sprintTime", "usSprintTime"]], on='id', how='left')
    else:
        retry_first_accepts = pd.DataFrame()
        
    sprints = pd.concat([burst_first_accepts, retry_first_accepts])
    if len(sprints):
        sprinted_orders = sprints.merge(qpos_df, how='left', on='id')
        sprinted_orders = sprinted_orders.rename(columns={"eventTime": "acceptTime", "usEventTime": "usAcceptTime"})
    else:
        sprinted_orders = pd.DataFrame()
finally:
    conn.close()

if len(accepted_orders):
    accepted_orders = sprinted_orders.merge(qpos_df, on='id', how='left')
    status = int(len(sprinted_orders) > 0)
    sprint_times = sprinted_orders.drop_duplicates(['serverId', 'sprinterType'])[['serverId', 'sprinterType', "sprintTime", "usSprintTime", "acceptTime", "usAcceptTime", "qpos"]]

    sprint_times['sprintTime'] = ["'" + str(pd.to_datetime(timestr).time()) + "'" for timestr in sprint_times.sprintTime.values]
    sprint_times['acceptTime'] = ["'" + str(pd.to_datetime(timestr).time()) + "'" for timestr in sprint_times.acceptTime.values]

    for array in sprint_times.values:
        qpos = None
        server, sprinterType, sprintTime, usSprintTime, acceptTime, usAcceptTime, qpos = array
        print(datestr, server, sprintTime, usSprintTime, acceptTime, usAcceptTime, qpos)

        try:
            insert_cmd = """
                insert into sprinter_times (datestr, server, sprinterType, sprintTime, usSprintTime, status, acceptTime, usAcceptTime, qpos)
                values(%s, %d, %s, %s, %d, %d, %s, %d, %d)
            """ % ("'" + date_db + "'", server, "'"+sprinterType+"'", sprintTime, usSprintTime, status, acceptTime, usAcceptTime, qpos)

            conn = db.conn(**DBSERVER01_CONN)
            conn.execute(insert_cmd)

        finally:
            conn.close()
else:
    print("No onOpen sprints found for %s\n" %date_db)

#############
#PreOpen sprints

try:
    conn = db.conn(**DBSERVER01_CONN)

    sprinter_ids = conn.frame_query(
        ("select * from orders where orderStatus = 1 and eventTime < '%s 08:00:01'"
         " and tradeDate = '%s' order by eventTime, usEventTime") %(date_db, date_db)
    )

    accepted_orders = conn.frame_query(
        ("select distinct id from orders where orderStatus = 2 and id in %s and tradedate='%s' "
         " order by eventTime, usEventTime") % (tuple(sprinter_ids.id.unique().astype(int)), date_db)
        )
finally:
    conn.close()
    
if len(sprinter_ids):
    status = int(len(accepted_orders) > 0)

    sprinter_ids = sprinter_ids.merge(qpos_df, on='id', how='left')
    sprint_times = sprinter_ids.dropna(subset=['qpos']).drop_duplicates('serverId')[['serverId','eventTime','usEventTime','qpos']]
    sprint_times['eventTime'] = ["'" + str(timestr.time()) + "'" for timestr in sprint_times.eventTime.values]

    for array in sprint_times.values:
        server, sprintTime, usSprintTime, qpos = array
        acceptTime = sprintTime
        usAcceptTime = usSprintTime
        print(datestr, server, sprintTime, usSprintTime, acceptTime, usAcceptTime, qpos)
        sprinterType = "'preopen'"
        try:
            update_cmd = """
                insert into sprinter_times (datestr, server, sprinterType, sprintTime, usSprintTime, status, acceptTime, usAcceptTime, qpos)
                values(%s, %d, %s, %s, %d, %d, %s, %d, %d)
                on duplicate key update
                sprintTime=%s, usSprintTime=%d, status=%d, acceptTime=%s, usAcceptTime=%d, qpos=%d
            """ % ("'" + date_db + "'", server, sprinterType, sprintTime, usSprintTime, status, acceptTime, usAcceptTime, qpos, sprintTime, usSprintTime, status, acceptTime, usAcceptTime, qpos)

            conn = db.conn(**DBSERVER01_CONN)
            conn.execute(update_cmd)

        finally:
            conn.close()
else:
    print("No sprints for %s" %date_db)

