import sys
import os
import json

from collections import deque


if __name__ == '__main__':
    cfg_filename, sprinter_filename, revised_filename = sys.argv[1:]

    with open(cfg_filename, 'r') as f:
        cfg = json.load(f)
    num_pids = cfg['numberOfPids']
    num_sacrificial = cfg['numberOfSacrificialPids']

    if num_sacrificial == 0:
        print('no sacrificial pids... quitting.')
        #sys.exit(0)

    with open(sprinter_filename, 'r') as f:
        lines = [x for x in f.read().split('\n') if x and '#' not in x]

    num_orders = len(lines)
    orders_per_pid = num_orders / num_pids
    print('read', num_orders, 'orders for', num_pids, 'pids...', orders_per_pid, 'per pid')

    order_queues = []
    for i in range(num_pids + num_sacrificial):
        order_queues.append(deque())
    print(order_queues)
    prev_spec = None
    which_pid, which_sacrificial = 0, 0
    contenders = deque()
    for i, line in enumerate(lines):
        items = line.split(',')
        qty = int(items[3])
        alt_qty = int(items[6])
        spec = ','.join(items[:3])
        other = ','.join(items[4:6])
        print('reading', line, 'spec', spec, 'qty', qty)
        if spec == prev_spec:
            if contenders and contenders[-1][0] == spec:
                contenders.pop()
            for which_sacrificial in range(num_sacrificial):
                idx = which_sacrificial % num_sacrificial
                q = order_queues[idx]
                if not q or q[-1] != line:
                    dqty = num_pids + idx
                    redundant_line = ','.join([spec, str(qty-dqty), other, str(alt_qty-dqty)]) 
                    print('adding', spec, 'to', idx, 'as', redundant_line)
                    q.append(redundant_line)
                    #which_sacrificial += 1
        else:
            pid = i % num_pids
            contenders.append((spec, qty, other, alt_qty, pid))
        q = order_queues[num_sacrificial + which_pid % num_pids]
        q.append(line)
        which_pid += 1
        prev_spec = spec

    for q in order_queues:
        print(len(q))

    which_sacrificial = 0
    while any([len(x) != orders_per_pid for x in order_queues[:num_sacrificial]]):
        idx = which_sacrificial % num_sacrificial
        q = order_queues[idx]
        if len(q) != orders_per_pid:
            spec, qty, other, alt_qty, pid = contenders.popleft()
            dqty = (num_pids - pid) + idx
            redundant_line = ','.join([spec, str(qty-dqty), other, str(alt_qty-dqty)]) 
            print('adding remaining', spec, 'to', idx, 'from pid', pid, 'as', redundant_line)
            q.append(redundant_line)
        which_sacrificial += 1

    for q in order_queues:
        print(len(q))

    print('writing to', revised_filename)
    with open(revised_filename, 'w') as f:
        which_pid = 0
        while any([len(x) > 0 for x in order_queues]):
            idx = which_pid % (num_sacrificial + num_pids)
            if idx < num_sacrificial:
                f.write('# order below might not succeed\n')
            line = order_queues[idx].popleft()
            f.write(line)
            f.write('\n')
            which_pid += 1
