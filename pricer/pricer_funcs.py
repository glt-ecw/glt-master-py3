import sys
import glt.db as db
import glt.math.bs as bs
import datetime
import json
import numpy as np
import pandas as pd
import dateutil.rrule as dr
import dateutil.relativedelta as drel


class SpreadPricer(object):
    def __init__(self, db_conn, cfg_name=None):
        self.db = db_conn
        #load from cfg
        cfg = self.load_cfg(cfg_name)
        self.front_month = cfg['front_month_fut']
        self.db_name = cfg['db_name']
        self.opt_stem = cfg['opt_stem']
        self.r = cfg['ir']
        self.time_offset = pd.Timedelta(cfg['time_offset'])

        #do time/date shit
        now = datetime.datetime.utcnow() + self.time_offset
        if now.hour < 5:
            self.today = now.date() - pd.Timedelta('1D')
            self.yesterday = now.date() - pd.Timedelta('2D')
        else:
            self.today = now.date()
            self.yesterday = now.date() - pd.Timedelta('1D')
        
        #month codes
        self.monthToUnderlying = {'F':0, 'G':0, 'H':0, 'J':1, 'K':1, 'M':1, 'N':2, 'Q':2, 'U':2, 'V':3, 'X':3, 'Z':3}
        self.symToMonth = {'F':1, 'G':2, 'H':3, 'J':4, 'K':5, 'M':6, 'N':7, 'Q':8, 'U':9, 'V':10, 'X':11, 'Z':12}
        self.monthToSym = {val : key for key, val in self.symToMonth.items()}
        self.nones = {'1':'EW1', '2':'EW2', '4':'EW4', 'EF':'EW', 'EG':'', 'EH':'', 'EJ':'', 'EK':'', 'EM':'', 'EN':'', 'EQ':'', 'EU':'', 'EV':'', 'EX':'', 'EZ':''}


    def load_cfg(self, name=None):
        if name is None:
            filename = 'pricer_cfg.json'
        else:
            filename = name
        with open(filename, 'r') as f:
            cfg = json.load(f)
        return cfg
        

    def getFutPrice(self):
        query = '''
            SELECT expDate, shortName, topBidPx, topOfferPx
            FROM %s.MD
            WHERE shortName="%s";
            ''' % (self.db_name, self.front_month)
        self.futures = self.db.frame_query(query)
        self.spot_price = self.futures['topBidPx'].values[0]
        return self.spot_price

    def doContract(self, con):
        mo = con.upper()
        if mo in self.symToMonth and self.symToMonth[mo] >= self.today.month:
            short = self.opt_stem + mo + self.today.strftime('%y') + '_'
        elif mo in self.symToMonth and self.symToMonth[mo] < self.today.month:
            short = self.opt_stem + mo + (self.today + datetime.timedelta(years=1)).strftime('%y') + '_'
        #else handle weeklies
        return short

    def doPricer(self, short, futuresPrice):
        #get opt type
        if 'P' in short:
            optType = -1
        elif 'C' in short:
            optType = 1
        else:
            print("Unknown option type dingus try again")
            return

        returnvalues = np.zeros(4)

        #get data from optionsPricingModelData tbl
        sqlcommand = '''SELECT tv, tvAdjust, futureMidPrice, delta, volatility as vol, yearsToExpiration as tte 
        FROM %s.optionsPricingModelData 
        WHERE eventTime > "%s 17:00:00" AND shortName = "%s";''' % (self.db_name, self.yesterday, short)
        self.db.execute(sqlcommand)
        temp = self.db.fetchall()[0]
        
        #chop up short to get the strike, get inputs
        start, end = 7, -1
        if short[start:end].isdigit():
            strike = float(short[start:end])
        vol = float(temp[4])
        tte = float(temp[5])
        price = bs.tv(strike, futuresPrice, vol, tte, self.r, optType)

        #fill out return array
        returnvalues[0] = price + float(temp[1]) #px + adj
        lst = [x.lstrip() for x in short.split('_')]
        returnvalues[1] = bs.delta(strike, futuresPrice, vol, tte, self.r, optType)
        returnvalues[2] = bs.vega(strike, futuresPrice, vol, tte, self.r) / 100.
        returnvalues[3] = price
        
        return returnvalues

    def doStrategy(self, lstStrat, futSpreads = [0,-7.925,0], printOut=True):
        '''lstStrat has to have a blank or useless value at index 0 bc ash writes shitty code
            lstStrat = ['', 
            returns: list - [TV, Delta, Vega, UnAdjTV]'''

        try:
            if float(lstStrat[len(lstStrat)-1]):
                strat = (lstStrat[len(lstStrat)-2]).upper()
                futuresPrice = float(lstStrat[len(lstStrat)-1])
        except:
            strat = (lstStrat[len(lstStrat)-1]).upper()
            futuresPrice = setUnderlying(strat)

        arg1 = lstStrat[1].upper()
        arg2 = lstStrat[2].upper()
        over = ''

        if (strat=='P' or strat=='C'):
            total = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + strat, futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
        elif strat=='PS':
            calFlag = 0
            if (arg1 in self.symToMonth or arg1 in self.nones) and (arg2 in self.symToMonth or arg2 in self.nones):
                #calendar
                v1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]] )
                if len(lstStrat) == 7:
                    #calendar with different strikes
                    calFlag = 1
                    v2 = self.doPricer( self.doContract(lstStrat[2]) + str(lstStrat[4]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg2]] )
                else:
                    v2 = self.doPricer( self.doContract(lstStrat[2]) + str(lstStrat[3]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg2]] )
            elif (arg1 in self.symToMonth or arg1 in self.nones) and not (arg2 in self.symToMonth or arg2 in self.nones):
                #not a calendar
                v1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]] )
                v2 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]] )
            if v1[0] > v2[0]:
                total = v1-v2
                if calFlag:
                    over = '%s over' % lstStrat[1].upper()
            else:
                total = v2-v1
                if calFlag:
                    over = '%s over' % lstStrat[2].upper()
        elif strat=='CS':
            calFlag = 0
            if (arg1 in self.symToMonth or arg1 in self.nones) and (arg2 in self.symToMonth or arg2 in self.nones):
                #calendar
                o1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]] )
                if len(lstStrat) == 7:
                    #calendar with different strikes
                    calFlag = 1
                    o2 = self.doPricer( self.doContract(lstStrat[2]) + str(lstStrat[4]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg2]] )
                else:
                    o2 = self.doPricer( self.doContract(lstStrat[2]) + str(lstStrat[3]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg2]] )
            elif (arg1 in self.symToMonth or arg1 in self.nones) and not (arg2 in self.symToMonth or arg2 in self.nones):
                #not a calendar
                o1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]] )
                o2 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]] )
            if o1[0] > o2[0]:
                total = o1-o2
                if calFlag:
                    over = '%s over' % lstStrat[1].upper()
            else:
                total = o2-o1
                if calFlag:
                    over = '%s over' % lstStrat[2].upper()
        elif strat=='RR' or strat=='R':
            strat = 'RR'
            p = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]] )
            c = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]] )
            if p[0] > c[0]:
                total = p-c
                over = "PUT over"
            else:
                total = c-p
                over = "CALL over"
        elif strat=='PF':
            o1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o2 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o3 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[4]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            total = o1+o3-(2*o2)
            if total[0] > 0:
                over = 'WINGS over'
            else:
                total = total * -1
                over = 'MIDDLE over'
        elif strat=='CF':
            o1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o2 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o3 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[4]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            total = o1+o3-(2*o2)
            if total[0] > 0:
                over = 'WINGS over'
            else:
                total = total * -1
                over = 'MIDDLE over'
        elif strat=='S':
            o1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o2 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            total = o1+o2
        elif strat=='SN':
            o1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o2 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            total = o1+o2
        elif strat=='PC':
            o1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o2 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o3 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[4]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o4 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[5]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            total = (o1 + o4) - (o2 + o3)
            price = total[0]
            delta = total[1]
            vega = total[2]
        elif strat=='CC':
            o1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o2 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o3 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[4]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o4 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[5]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            total = (o1 + o4) - (o2 + o3)
        elif strat=='PT':
                o1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
                o2 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
                o3 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[4]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
                total = (o1 + o2) - (o3)
                if total[0] > 0:
                        over = "2 over"
                else:
                        total = total*-1
                        over = "1 over"
        elif strat=='CT':
                o1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
                o2 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
                o3 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[4]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
                total = o1 - (o2 + o3)
                if total[0] > 0:
                        over = "1 over"
                else:
                        total = total*-1
                        over = "2 over"
        elif strat == 'SPF':
            o1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o2 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o3 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[4]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            total = o1 + o3 - o2
            if total[0] > 1:
                over = 'WINGS over'
            else:
                over = 'MIDDLE over'
                total = total * -1
        elif strat == 'SCF':
            o1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o2 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o3 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[4]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            total = o1 + o3 - o2
            if total[0] > 1:
                over = 'WINGS over'
            else:
                over = 'MIDDLE over'
                total = total * -1
        elif strat == 'CVP':
            o1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o2 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o3 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[4]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o4 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[5]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            total = (o2-o1) - (o3-o4)
            if total[0] > 0:
                over = 'PS over'
            else:
                total = total * -1
                over = 'CS over'
        elif strat == 'CSP':
            o1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o2 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o3 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[4]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            total = (o1-o2) - o3
            if total[0] > 0:
                over = 'CS over'
            else:
                total = total * -1
                over = 'PUT over'
        elif strat == 'PSC':
            o1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o2 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o3 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[4]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            total = (o2-o1) - o3
            if total[0] > 0:
                over = 'PS over'
            else:
                total = total * -1
                over = 'CALL over'
        elif strat=='IF':
            o1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o2 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o3 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o4 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[4]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            total = (o2 + o3) - (o1 + o4)
            price = total[0]
            delta = total[1]
            vega = total[2]
        elif strat=='IC':
            o1 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o2 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + 'P', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o3 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[4]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            o4 = self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[5]) + 'C', futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            total = (o2 + o3) - (o1 + o4)
            price = total[0]
            delta = total[1]
            vega = total[2]
        elif len(strat) > 2:
            opt = strat[0]
            leg1 = strat[1]
            leg2 = strat[2:]
            calFlag = 0
            if opt == 'C':
                if len(lstStrat) == 7:
                    #if calendar ratio
                    calFlag = 1
                    o1 = float(leg1) * self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + opt, futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
                    o2 = float(leg2) * self.doPricer( self.doContract(lstStrat[2]) + str(lstStrat[4]) + opt, futuresPrice + futSpreads[self.monthToUnderlying[arg2]])
                else:
                    o1 = float(leg1) * self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + opt, futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
                    o2 = float(leg2) * self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + opt, futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            elif opt == 'P':
                if len(lstStrat) == 7:
                    #if calendar ratio
                    calFlag = 1
                    o2 = float(leg2) * self.doPricer( self.doContract(lstStrat[2]) + str(lstStrat[4]) + opt, futuresPrice + futSpreads[self.monthToUnderlying[arg2]])
                    o1 = float(leg1) * self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + opt, futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
                else:
                    o2 = float(leg2) * self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[2]) + opt, futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
                    o1 = float(leg1) * self.doPricer( self.doContract(lstStrat[1]) + str(lstStrat[3]) + opt, futuresPrice + futSpreads[self.monthToUnderlying[arg1]])
            if o1[0] > o2[0]:
                total = o1-o2
                if calFlag:
                    over = "%s BIG %s leg over" % (leg1, lstStrat[1].upper())
                else:
                    over = "%s BIG leg over" % leg1
            else:
                total = o2-o1
                if calFlag:
                    over = "%s LITTLE %s leg over" % (leg2, lstStrat[2].upper())
                else:
                    over = "%s LITTLE leg over" % leg2

        if printOut:
            if over != '':
                print(over)
            print("Unadjusted: %0.3f" % total[3])
            print("%0.3f D: %0.2f V: %0.2f" % (total[0],total[1],total[2]))

        total = list(total)
        total.append(over)
        return total

