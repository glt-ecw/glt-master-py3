from tkinter import Tk, Text, BOTH, W, N, E, S, LEFT, Entry, END, StringVar, SUNKEN, HORIZONTAL, CENTER, Menu, Toplevel, PhotoImage, Image
from tkinter.ttk import Frame, Style, Button, Label, Separator, Treeview, Scrollbar
from pricer_funcs import SpreadPricer
import glt.db as db
import json
import datetime


class Pricer(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent

        #load cfg
        cfg = self.load_cfg()
        self.rta_db = cfg['rta_name']
        self.tick_incr = cfg['tick_incr']

        #limited to 3 fut spreads right now...
        if 'future_spreads' in cfg:
            self.futSpread = [0.]    
            for spread in cfg['future_spreads']:
                self.futSpread.append(spread)
        else:
            self.futSpread = [0.,0.,0.,0.]

        #set broker field
        if 'broker' in cfg:
            self.broker = cfg['broker']
        else:
            self.broker = ''

        #TODO use the mysql.json
        # conn for pricer table
        self.db = db.conn(host='10.125.0.200', db=self.rta_db, user='deio', pw='!w3alth!')
        # conn for tvs
        theo_conn = db.conn(host='10.125.0.200', db=cfg['db_name'], user='rdeio', pw='fatmanblues')
        
        self.p2 = SpreadPricer(theo_conn)
        self.today = self.p2.today
        self.time_offset = self.p2.time_offset

        if len(self.db.frame_query('''SHOW TABLES IN %s LIKE "pricer_%s"''' % (self.rta_db, self.today.strftime('%Y%m%d')))) == 0:
            self.db.execute('''CREATE TABLE %s.pricer_%s 
                (id BINARY(16), eventTime TIME, strategy VARCHAR(50), future FLOAT, TV FLOAT, 
                unadj FLOAT, vega FLOAT, delta FLOAT, mktB FLOAT, mktA FLOAT, qty INT(11), bid VARCHAR(50), ask VARCHAR(50), 
                over VARCHAR(50), broker VARCHAR(50), 
                PRIMARY KEY (strategy, future, broker))''' % (self.rta_db, self.today.strftime('%Y%m%d')))

        self.initUI()
        
    def load_cfg(self, name=None):
        if name is None:
            filename = 'pricer_cfg.json'
        else:
            filename = name
        with open(filename, 'r') as f:
            cfg = json.load(f)
        return cfg 

    def save_spreads_cfg(self, name=None):
        if name is None:
            filename = 'pricer_cfg.json'
        else:
            filename = name
        cfg = self.load_cfg(filename)
        cfg['future_spreads'] = self.futSpread[1:]  # to remove the starting 0 value
        with open(filename, 'w') as f:
            json.dump(cfg, f, indent=4, separators=(',', ' : '))
 
    #####################
    # actions on events #
    #####################
    def callback(self, button):
        if button == 'ok':
            #call pricing shit
            strat = self.areaStrat.get().split(' ')
            strat = [x for x in strat if x!='']
            strat.insert(0, '')
            strat.append(float(self.areaFuture.get()))
            
            #dont go through pricing if you are moving fut without anything in strat cell
            if len(strat) > 2:
                pricing = self.p2.doStrategy( strat, self.futSpread, printOut=False)
                
                self.tv.set('%0.3f' % pricing[0])
                self.unadjTv.set('%0.3f' % pricing[3])
                self.delta.set('%0.2f' % pricing[1])
                self.vega.set('%0.2f' % pricing[2])
                self.over.set(pricing[4])
                
                strStrat = ' '.join(strat[:-1]).upper()
                self.areaStrat.delete(0, END)
                self.areaStrat.insert(0, strStrat)
                
        elif button == 'new':
            self.areaStrat.delete(0, END)
            self.areaFuture.delete(0, END)
            self.areaFuture.insert(0, str(self.p2.getFutPrice()))
            self.tv.set('')
            self.unadjTv.set('')
            self.delta.set('')
            self.vega.set('')
            self.over.set('')
            self.areaBid.delete(0, END)
            self.areaAsk.delete(0, END)
            self.areaBroker.delete(0, END)
            self.areaBroker.insert(0, self.broker)
            
            #if there was a row selected in the tree, turn it off and put the focus to the areaStrat
            if self.savedRow:
                self.selection = self.tree.selection_remove(self.selection)
                self.savedRow = False
            self.areaStrat.focus_set()
            
        elif button == 'save':
            #check if row exists
            query = '''SELECT * 
                FROM %s.pricer_%s 
                WHERE strategy="%s" and future=%s and broker="%s";''' % (
                self.rta_db, self.today.strftime('%Y%m%d'), self.areaStrat.get().lstrip(), 
                self.areaFuture.get(), self.areaBroker.get()
                )
            exists = self.db.frame_query(query)

            #db insert/update
            if len(exists) == 0:
                insert = '''INSERT INTO %s.pricer_%s 
                    (id, eventTime, strategy, future, bid, tv, unadj, ask, vega, 
                    delta, over, broker) 
                    VALUES (15, "%s", "%s", %s, "%s", %s, %s, "%s", "%s", %s, "%s", "%s" )''' % (
                    self.dbValues()
                    )
                self.db.execute(insert)
            else:
                update = '''UPDATE %s.pricer_%s 
                    SET eventTime="%s", strategy="%s", future=%s, bid="%s", tv=%s, unadj=%s, ask="%s", vega=%s, delta=%s, over="%s", broker="%s", qty=0
                    WHERE strategy="%s" and future=%s;''' % (
                    self.dbValues() + (self.areaStrat.get().lstrip(), self.areaFuture.get())
                    )
                self.db.execute(update)

            #if this is a saved row: update self.selection to that row, go throw all elements of that item and update
            #if not: insert row into tree and set self.selection to the new row so you can update it if need be
            #vals = (t, self.areaStrat.get().lstrip(), self.areaFuture.get(), self.tv.get(), self.unadjTv.get(), self.vega.get(), self.delta.get(), self.over.get())
            vals = self.dbValues()[2:]
            if self.savedRow:
                for i in range(len(self.columns)):
                    self.tree.set(self.selection, column=self.columns[i], value=vals[i])
                #sort tree items to put the updated row as the most recent row by timestamp
                #for col in self.columns:
                self.tree.heading('Time', text='Time', command=lambda: self.tree_sort(self.tree, col, True))
            else:
                self.selection = self.tree.insert('', 0, values=vals)
                self.items.append( self.selection )
            
            self.savedRow=True
            self.tree.selection_set(self.selection)
            
            self.treeSort()
        elif button == 'del':
            self.treeSort()
        elif button == 'refresh':
            query = '''SELECT eventTime, strategy, future, bid, TV, unadj, ask, vega, delta, over, broker
                FROM %s.pricer_%s
                ORDER BY eventTime DESC''' % (self.rta_db, self.today.strftime('%Y%m%d'))
            result = self.db.frame_query(query)
            
            data = []
            for i in range(len(result['eventTime'])):
                strat = result['strategy'][i].split(' ')
                strat.insert(0, '')
                strat.append(result['future'][i])
                newValue = self.p2.doStrategy( strat, self.futSpread, printOut=False)
                data.append([result['eventTime'][i], result['strategy'][i], '%0.2f' % result['future'][i], result['bid'][i],'%0.3f' % newValue[0], '%0.3f' % newValue[3], 
                    result['ask'][i],'%0.2f' % newValue[2], '%0.2f' % newValue[1], newValue[4], result['broker'][i]])
            data.sort(key=lambda t: t[0])

            #for child in self.tree.get_children():
            for child in self.items:
                self.tree.delete(child)
            self.items = []
            #add new data as children
            for row in data:
                self.items.append( self.tree.insert('', 0, values=row, text=row) )
                
            self.savedRow = False
            if len(self.entrySearch.get()) > 0:
                self.search(self.entrySearch.get())
            self.callback('new')
            
        elif button == 'search':
            if len(self.entrySearch.get()) > 0:
                self.search(self.entrySearch.get())
        
    def search(self, pattern, item=''):
        for child in self.items:
            self.tree.detach(child)
        i=0
        for child in self.items:
            text = self.tree.item(child, 'text')
            if text.lower().find(pattern.lower()) != -1:
                self.tree.reattach(child, '' , i)
                i += 1

                    
    def dbValues(self):
        #create values for INSERT statement
        t = (datetime.datetime.utcnow() + self.time_offset).strftime('%H:%M:%S')
        bid = self.areaBid.get()
        ask = self.areaAsk.get()
        res = (self.rta_db, self.today.strftime('%Y%m%d'), t, self.areaStrat.get().lstrip(), 
                self.areaFuture.get(), bid, self.tv.get(), self.unadjTv.get(), 
                ask, self.vega.get(), self.delta.get(), self.over.get(), 
                self.areaBroker.get())
        return res

    def treeSort(self):
        l = [(self.tree.set(k, 'Time'), k) for k in self.tree.get_children('')]
        l.sort(key=lambda t: t[0], reverse=True)
        
        for index, (val, k) in enumerate(l):
            self.tree.move(k, '', index)
    
    def futureSpreads(self):
        self.futSpreadPopup = futureSpreadsPopup(self.parent, self.futSpread)
        self.parent.wait_window(self.futSpreadPopup.top)
        if self.futSpreadPopup.spread1:
            self.futSpread = [0.,float(self.futSpreadPopup.spread1),float(self.futSpreadPopup.spread2), float(self.futSpreadPopup.spread3)]
            
    ###################
    # initialize  GUI #
    ###################
    def initUI(self):
        buttonRow = 0
        titleRow = 2
        outputRow = 4
        self.items = []
        
        self.parent.title("Pricer")
        self.style = Style()
        self.style.theme_use("alt")
        self.style.configure('Treeview', takefocus=False)
        self.pack(fill=BOTH, expand=1)
        
        #this handles the cells on resize
        bigWeight = 5
        smallWeight = 1
        self.columnconfigure(0, pad=0, weight=bigWeight)
        self.columnconfigure(1, pad=0, weight=smallWeight)
        self.columnconfigure(2, pad=0, weight=smallWeight)
        self.columnconfigure(3, pad=0, weight=smallWeight)
        self.columnconfigure(4, pad=0, weight=smallWeight)
        self.columnconfigure(5, pad=0, weight=smallWeight)
        self.columnconfigure(6, pad=0, weight=smallWeight)
        self.columnconfigure(7, pad=0, weight=smallWeight)
        #self.columnconfigure(8, pad=0, weight=smallWeight)
        self.rowconfigure(7, pad=0, weight=1)
        
        #buttons 
        #NEW, SAVE, DEL in row buttonRow(0)
        #OK in row 5
        #REFRESH in row x
        btnOk = Button(self, text="OK", command=lambda: self.callback('ok'))
        btnOk.grid(row=5, column=7)
        btnNew = Button(self, text="New", command=lambda: self.callback('new'))
        btnNew.grid(row=buttonRow, column=0, sticky=W)
        btnSave = Button(self, text="Save", command=lambda: self.callback('save'))
        btnSave.grid(row=buttonRow, column=0,sticky=N)
        btnDelete = Button(self, text="Del", command=lambda: self.callback('del'))
        btnDelete.grid(row=buttonRow, column=0, sticky=E)
        btnRefresh = Button(self, text='Refresh', command=lambda: self.callback('refresh'))
        btnRefresh.grid(row=8, column=5, sticky=N, pady=5)
        
        #title labels
        lbl = Label(self, text="Strategy")
        lbl.grid(row=titleRow, column=0, pady=0, padx=0)
        lbl2 = Label(self, text="Future")
        lbl2.grid(row=titleRow, column=1, pady=0, padx=0)
        lbl7 = Label(self, text="Bid")
        lbl7.grid(row=titleRow, column=2, pady=0, padx=0)
        lbl3 = Label(self, text="TV")
        lbl3.grid(row=titleRow, column=3, pady=0, padx=0)
        lbl4 = Label(self, text="UnAdjTV")
        lbl4.grid(row=titleRow, column=4, pady=0, padx=0)
        lbl8 = Label(self, text="Ask")
        lbl8.grid(row=titleRow, column=5, pady=0, padx=0)
        lbl5 = Label(self, text="Vega")
        lbl5.grid(row=titleRow, column=6, pady=0, padx=0)
        lbl6 = Label(self, text="Delta")
        lbl6.grid(row=titleRow, column=7, pady=0, padx=0)
        lbl8 = Label(self, text="Broker")
        lbl8.grid(row=titleRow, column=8, pady=0, padx=0)
        
        #output labels
        self.tv = StringVar()
        self.lblTV = Label(self, textvariable=self.tv)
        self.lblTV.grid(row=outputRow, column=3, pady=0, padx=0)
        
        self.unadjTv = StringVar()
        self.lblUnAdjTV = Label(self, textvariable=self.unadjTv)
        self.lblUnAdjTV.grid(row=outputRow, column=4, pady=0, padx=0)
        
        self.vega = StringVar()
        self.lblVega = Label(self, textvariable=self.vega)
        self.lblVega.grid(row=outputRow, column=6, pady=0, padx=0)
        
        self.delta = StringVar()
        self.lblDelta = Label(self, textvariable=self.delta)
        self.lblDelta.grid(row=outputRow, column=7, pady=0, padx=0)
        
        #table, scrollbar and keystroke Binding
        self.columns = ('Time', 'Strategy', 'FuturePx', 'Bid', 'TV', 'unAdjTV', 'Ask', 'Vega', 'Delta', 'Over', 'Broker')
        self.tree = Treeview(self, columns=self.columns, show='headings', height=12, selectmode='browse')
        self.sbY = Scrollbar(self, orient='vertical', command=self.tree.yview)

        self.tree.configure(yscrollcommand=self.sbY.set)
        self.tree.bind("<<TreeviewSelect>>", self.treeDblClick)
        for col in self.columns:
            self.tree.heading(col, text=col)
            if col == 'Time':
                self.tree.column(col, width=20, anchor='center')
            elif col == 'Strategy':
                self.tree.column(col, width=80, anchor='center')
            else:
                self.tree.column(col, width=50, anchor='center')
        self.tree.grid(row=7, columnspan=9, sticky=N+S+E+W, pady=10, padx=5)
        self.sbY.grid(row=7, column=9, stick=N+S, pady=10)
        self.selection = ''
        self.savedRow = False
        
        #keystroke binding
        self.parent.bind("<MouseWheel>", self._on_mousewheel)
        self.parent.bind("<Control-n>", lambda x: self.callback('new'))
        self.parent.bind("<Return>", lambda x: self.callback('ok'))
        self.parent.bind("<KP_Enter>", lambda x: self.callback('ok'))
        self.parent.bind("<F5>", lambda x: self.callback('refresh'))
        
        #Entry boxes in row3
        self.areaStrat = Entry(self)
        self.areaStrat.grid(row=outputRow, column=0, padx=5, sticky=E+W)
        
        self.areaFuture = Entry(self)
        self.areaFuture.configure(width=15, justify=CENTER)
        self.areaFuture.grid(row=outputRow, column=1)
        self.areaFuture.insert(0, '0')
        
        self.areaBid = Entry(self)
        self.areaBid.configure(width=8, justify=CENTER)
        self.areaBid.grid(row=outputRow, column=2, sticky=N)
        
        self.areaAsk = Entry(self)
        self.areaAsk.configure(width=8, justify=CENTER)
        self.areaAsk.grid(row=outputRow, column=5, sticky=N)
        
        self.areaBroker = Entry(self)
        self.areaBroker.configure(width=8, justify=CENTER)
        self.areaBroker.grid(row=outputRow, column=8, sticky=N)

        #separator and other shit
        Separator(self,orient=HORIZONTAL).grid(row=3, columnspan=9, sticky=E+W)
        Separator(self,orient=HORIZONTAL).grid(row=1, columnspan=9, sticky=E+W)
        self.over = StringVar()
        lblOver = Label(self, textvariable=self.over)
        lblOver.grid(row=5, column=0, pady=0, padx=0)
        
        #menu bar
        menubar = Menu(self.parent)
        self.parent.config(menu=menubar)
        
        settingsMenu = Menu(menubar, tearoff=0)
        settingsMenu.add_command(label="Future Spreads", command=self.futureSpreads)
        menubar.add_cascade(label='Settings', menu=settingsMenu)
        
        #search bar
        self.entrySearch = Entry(self)
        self.entrySearch.grid(row=8, columnspan=1, sticky=N+W+E, padx=10)
        btnSearch = Button(self, text="Search", command=lambda: self.callback('search'))
        btnSearch.grid(row=8, column=0, sticky=E+N, padx=5)
        
        #for testing:
        testing = False
        if testing:
            t = datetime.datetime.now().time().strftime('%H:%M:%S')
            vals = (t, 'N 1975 C', '1975', '1.12', '1.1', '2.34', '.46', '')
            self.tree.insert('', 0, values=vals, tags=('ttk',))
            vals = (t, 'Q 1975 C', '1975', '2.12', '2.1', '3.34', '.46', '')
            self.tree.insert('', 0, values=vals, tags=('ttk',))
            vals = (t, 'U 1975 C', '1975', '3.12', '3.1', '3.34', '.46', '')
            self.tree.insert('', 0, values=vals, tags=('ttk',))
        
        newOrder = (btnNew, self.areaStrat, self.areaFuture, self.areaBid, self.areaAsk, self.areaBroker, btnOk,  btnSave, btnDelete, self.entrySearch, btnSearch, btnRefresh)
        for widget in newOrder:
            widget.lift()
        
        #set focus on strat area
        self.areaStrat.focus_set()

    #################
    #    EVENTS     #
    #################
    def treeSelect(self, event):
        if not self.savedRow:
            self.tree.focus('')
            
    def _on_mousewheel(self, event):
        self.tree.yview_scroll(-1*(event.delta/60), "units")
            
    def treeDblClick(self, event):
        #second part keeps away an error when ctrl-n is pressed when row is highlighted
        if hasattr(self.tree.selection(), '__iter__') and len(self.tree.selection()) > 0:
            self.selection = self.tree.selection()[0]
            item = self.tree.item(self.selection)
            vals =  item['values']
            
            #vals is in the order of the tree columns 
            self.tv.set(vals[4])
            self.unadjTv.set(vals[5])
            self.delta.set(vals[8])
            self.vega.set(vals[7])
            self.over.set(vals[9])
            
            self.areaBid.delete(0, END)
            self.areaBid.insert(0, vals[3])
            self.areaAsk.delete(0, END)
            self.areaAsk.insert(0, vals[6])
            
            self.areaStrat.delete(0, END)
            self.areaStrat.insert(0, vals[1])
            self.areaFuture.delete(0, END)
            self.areaFuture.insert(0, vals[2])
            
            self.areaBroker.delete(0, END)
            self.areaBroker.insert(0, vals[10])
            
            self.areaStrat.focus_set()
            self.savedRow=True
        
    def keyPressed(self, event):
        if event.keysym == 'Up':
            #then tick fut
            currFut = float(self.areaFuture.get())
            self.areaFuture.delete(0, END)
            self.areaFuture.insert(0, str(currFut + self.tick_incr))
            self.callback('ok')
        elif event.keysym == 'Down':
            #then tick fut
            currFut = float(self.areaFuture.get())
            self.areaFuture.delete(0, END)
            self.areaFuture.insert(0, str(currFut - self.tick_incr))
            self.callback('ok')
            
class futureSpreadsPopup(Frame):
    def __init__(self, parent, spreads):
        self.style = Style()
        self.style.theme_use("alt")
        self.spread1 = None

        top = self.top = Toplevel(parent)
        label0 = Label(top, text='Future Spreads')
        label0.grid(row=0, column=0, pady=0, padx=30)
        
        label1 = Label(top, text='Spread 1:')
        label1.grid(row=1, column=0, pady=5, padx=0, sticky=E)
        self.entry1 = Entry(top)
        self.entry1.grid(row=1, column=1, padx=5)
        self.entry1.insert(0, str(spreads[1]))
        
        label2 = Label(top, text='Spread 2:')
        label2.grid(row=2, column=0, pady=5, padx=0, sticky=E)
        self.entry2 = Entry(top)
        self.entry2.grid(row=2, column=1, padx=5)
        self.entry2.insert(0, str(spreads[2]))
        
        label3 = Label(top, text='Spread 3:')
        label3.grid(row=3, column=0, pady=5, padx=0, sticky=E)
        self.entry3 = Entry(top)
        self.entry3.grid(row=3, column=1, padx=5)
        self.entry3.insert(0, str(spreads[3]))
        
        button = Button(top, text='Set', command=self.cleanup)
        button.grid(row=4, column=0, padx=30)
    
    def cleanup(self):
        self.spread1=self.entry1.get()
        self.spread2=self.entry2.get()
        self.spread3=self.entry3.get()

        self.top.destroy()
            
def main():
    root = Tk()
    root.geometry("850x410+300+300")
    app = Pricer(root)
    root.bind_all('<Key>', app.keyPressed)
    
    root.mainloop()
    app.save_spreads_cfg()

if __name__ == '__main__':
    main()

