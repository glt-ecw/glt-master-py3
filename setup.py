import sys
import os
import subprocess


# to run: python setup.py install --record files.txt

if __name__ == '__main__':
    glt_path = os.environ['GLT_MASTER_PATH']

    clean_only = len(sys.argv) > 1 and sys.argv[1] == 'clean'

    if clean_only:
        cwd = os.path.dirname(os.path.realpath(__file__))
        for root, dirs, files in os.walk(cwd):
            for file in files:
                if file.endswith('.c') or file.endswith('.cpp'):
                    filename = '/'.join([root, file])
                    print('removing', filename)
                    os.remove(filename)
        ext_modules = []
        exit(0)

    # before installing anything, run scripts/install_prereqs.py
    # in current python environment
    #subprocess.call(['python', os.path.join(glt_path, 'scripts/install_prereqs.py')])

    # now install
    from numpy import get_include as np_get_include
    from distutils.core import setup, Extension
    from Cython.Build import cythonize
    from Cython.Distutils import build_ext

    os.environ['CC'] = os.environ['CONDA_CC']
    os.environ['CXX'] = os.environ['CONDA_CXX']
    os.environ['ARCHFLAGS'] = '-arch x86_64'

    cpp11_compile_args = ['-std=c++11']

    np_include = np_get_include()
    md_helpers_path = os.path.join(glt_path, 'projects/cpp/md_helpers')

    pyx_ext = [
        Extension(
            'math.stats', 
            sources = [
                'glt/math/stats.pyx'
            ], 
            language = 'c++', 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'math.kalman', 
            sources = [
                'glt/math/kalman.pyx'
            ], 
            language = 'c++', 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'math.bezier', 
            sources= [
                'glt/math/bezier.pyx'
            ], 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'math.bs', 
            sources = [
                'glt/math/bs.pyx'
            ], 
            language='c++', 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'math.cvx', 
            sources = [
                'glt/math/cvx.pyx'
            ], 
            include_dirs = [
                np_include
            ]
        ),
        Extension(
            'md.bonds', 
            sources = [
                'glt/md/bonds.pyx'
            ],
            language = 'c++', 
            include_dirs = [
                np_include
            ], 
        ),
        Extension(
            'md.krx', 
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/md/krx.pyx'
            ], 
            include_dirs = [
                md_helpers_path,
                np_include
            ],
        ),
        Extension(
            'md.omx', 
            sources = [
                'glt/md/omx.pyx'
            ],
            language='c++', 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'md.itch', 
            sources = [
                'glt/md/itch.pyx'
            ], 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'md.standard', 
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                os.path.join(md_helpers_path, 'options_handling.cpp'),
                'glt/md/standard.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ), 
        Extension(
            'oe.standard', 
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                #os.path.join(md_helpers_path, 'options_handling.cpp'),
                'glt/oe/standard.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ), 
        Extension(
            'backtesting.base',
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                os.path.join(md_helpers_path, 'match_simulation.cpp'),
                'glt/backtesting/base.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'backtesting.strategies',
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/backtesting/strategies.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'backtesting.sprawler',
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                os.path.join(md_helpers_path, 'options_handling.cpp'),
                'glt/backtesting/sprawler.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'backtesting.pairs',
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/backtesting/pairs.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'backtesting.options',
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/backtesting/options.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'backtesting.ko_sniper',
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/backtesting/ko_sniper.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'backtesting.tpx_sniper',
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/backtesting/tpx_sniper.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ), 
        Extension(
            'backtesting.cke_sandbox',
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/backtesting/cke_sandbox.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'backtesting.ars_sandbox',
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/backtesting/ars_sandbox.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'backtesting.scraper_qty',
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/backtesting/scraper_qty.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'backtesting.scraper_leave_in',
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/backtesting/scraper_leave_in.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'tools', 
            sources = [
                'glt/tools.pyx'
            ], 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'whiteboard', 
            sources = [
                'glt/whiteboard.pyx'
            ], 
            language = 'c++', 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'deio', 
            sources = [
                'glt/deio.pyx'
            ], 
            language = 'c++', 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'structures', 
            sources = [
                'glt/structures.pyx'
            ], 
            language = 'c++', 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'manuscraper', 
            sources = [
                'glt/manuscraper.pyx'
            ], 
            language = 'c++', 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'manuscraper_beta', 
            sources = [
                'glt/manuscraper_beta.pyx'
            ], 
            language='c++', 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'math.helpful_functions', 
            sources = [
                'glt/math/helpful_functions.pyx'
            ], 
            language = 'c++', 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'modeling.structs', 
            sources = [
                'glt/modeling/structs.pyx'
            ], 
            language = 'c++', 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'modeling.option_filters', 
            sources = [
                'glt/modeling/option_filters.pyx'
            ], 
            language = 'c++', 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'modeling.filter_base', 
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/modeling/filter_base.pyx'
            ],
            language = 'c++', 
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'modeling.filter_options', 
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/modeling/filter_options.pyx'
            ],
            language = 'c++', 
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'modeling.rawStrike_cython', 
            sources = [
                'glt/modeling/rawStrike_cython.pyx'
            ], 
            language = 'c++', 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'modeling.option_skews', 
            sources = [
                'glt/modeling/option_skews.pyx'
            ], 
            language = 'c++', 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'modeling.volpaths', 
            sources = [
                'glt/modeling/volpaths.pyx'
            ], 
            language = 'c++', 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'modeling.simulations', 
            sources = [
                'glt/modeling/simulations.pyx'
            ], 
            language = 'c++', 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'modeling.spread_beta', 
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/modeling/spread_beta.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'modeling.spread_pairs.base', 
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/modeling/spread_pairs/base.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'modeling.spread_pairs.relf', 
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/modeling/spread_pairs/relf.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'modeling.spread_pairs.topix', 
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/modeling/spread_pairs/topix.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'modeling.spread_pairs.kospi_options', 
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                os.path.join(md_helpers_path, 'options_handling.cpp'),
                'glt/modeling/spread_pairs/kospi_options.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'modeling.kospi_spreads', 
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                os.path.join(md_helpers_path, 'options_handling.cpp'),
                'glt/modeling/kospi_spreads.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'modeling.tv_adjust', 
            sources = [
                'glt/modeling/tv_adjust.pyx'
            ], 
            language='c++', 
            include_dirs = [
                np_include
            ],
        ),
        Extension(
            'listening.base',
            sources = [
                'glt/listening/base.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'listening.md',
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/listening/md.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'listening.oe',
            sources = [
                'glt/listening/oe.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'publishing.deio_pairs',
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/publishing/deio_pairs.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'publishing.modelpub',
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                'glt/publishing/modelpub.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        ),
        Extension(
            'fut_sprawler_tools',
            sources = [
                'glt/fut_sprawler_tools.pyx'
            ],
            language = 'c++',
            include_dirs = [
                np_include
            ]
        ),
         Extension(
            'sprawler_tools',
            sources = [
                'glt/sprawler_tools.pyx'
            ],
            language = 'c++',
            include_dirs = [
                np_include
            ]
        ), 
        Extension(
            'sandbox.karl.kospi_vol_studies',
            sources = [
                os.path.join(md_helpers_path, 'book_building.cpp'),
                os.path.join(md_helpers_path, 'options_handling.cpp'),
                'glt/sandbox/karl/kospi_vol_studies.pyx'
            ],
            include_dirs = [
                md_helpers_path,
                np_include
            ]
        )
 
    ]

    ext_modules = cythonize(pyx_ext)

    setup(name='glt-master',
        version='0.1.0',
        packages=[
            'glt', 
            'glt.backtesting',
            'glt.listening',
            'glt.math', 
            'glt.md', 
            'glt.modeling', 
            'glt.modeling.spread_pairs',
            'glt.pcap', 
            'glt.publishing',
            'glt.oe',
            'glt.sandbox',
            'glt.sandbox.karl'
        ],
        package_data={
            'glt': [
                '*.pxd',
                'backtesting/*.pxd', 
                'listening/*.pxd',
                'math/*.pxd', 
                'md/*.pxd', 
                'md/*.pkl', 
                'modeling/*.pxd', 
                'modeling/spread_pairs/*.pxd',
                'pcap/*.pxd', 
                'publishing/*.pxd',
                'oe/*.pxd',
                'sandbox/*.pxd',
                'sandbox/karl/*.pxd'
            ]
        },
        ext_package='glt',
        ext_modules=ext_modules,
        cmdclass = {'build_ext': build_ext},
    )




