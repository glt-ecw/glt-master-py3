from libc.stdint cimport *


cdef extern from "resampling.h" nogil:
    cdef struct Sample:
        uint64_t time, num_measurements
        double value
    cdef cppclass MeanValueResampler:
        MeanValueResampler()
        bint Initialized() const
        bint Recording() const
        void Init(uint64_t t_interval)
        void Record(uint64_t time, double value)
        uint64_t LastRecordedTime() const
        uint64_t NumSamples() const
        bint Empty() const
        void ForceSamplesTo(uint64_t time)
        void PopFront(uint64_t number)
        void PopFrontTo(uint64_t time)
        Sample PopFrontSample()
        int8_t CopyValuesToArray(double *dst_arr, uint64_t size)
        int8_t CopySamplesToArray(Sample *dst_arr, uint64_t size)
        uint64_t FirstSampleTime() const
        uint64_t LastSampleTime() const
    cdef cppclass MVRWithinBounds:
        MVRWithinBounds()
        void Init(double t_distance, double t_epsilon, uint64_t interval)
        void Record(double time, double value)
        void ForceSamplesTo(uint64_t time, bint trim)
        void ForceSamplesAround(uint64_t start_time, uint64_t end_time, bint trim)
        void PopFront(uint64_t number)
        void PopFrontTo(uint64_t time)
        uint64_t NumSamples() const
        double MinVal() const
        double MaxVal() const
        int8_t CopyValuesToArray(double *dst_arr, uint64_t size)
        int8_t CopySamplesToArray(Sample *dst_arr, uint64_t size)
        uint64_t FirstSampleTime() const
        uint64_t LastSampleTime() const
    cdef double CalculateEntropy(uint64_t time, uint64_t final_time, double half_life)
    cdef void CalculateMVREntropy(MeanValueResampler *mvr, double half_life, double *dst_arr, uint64_t size)
    cdef void CalculateMVRDiffAndCopyValuesToArray(MeanValueResampler *mvr_left, MeanValueResampler *mvr_right, double *dst_arr, uint64_t size)
    cdef double CalculateMVREntropyWeightedValue(MeanValueResampler *mvr, double half_life)
