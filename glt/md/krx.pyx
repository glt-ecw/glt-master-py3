# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: cdivision=True
# cython: wraparound=False
from libc.stdlib cimport malloc, free
from libc.stdint cimport int16_t, int32_t, int64_t, uint16_t, uint32_t, uint64_t
from libc.stdio cimport sprintf
from libc.string cimport strlen
from libcpp.map cimport map as map_
from numpy cimport ndarray
from glt.md.standard cimport BookBuilder, MDOrderBook, BookLevel, PrintMDOrderBookColumns
from glt.tools cimport BBO

from numpy import zeros, float64, int64, int32, uint64, array
from pandas import DataFrame


cdef uint16_t KRX_NUM_LEVELS = 5
cdef uint16_t KRX_MESSAGE_ATHREE = 3
cdef uint16_t KRX_MESSAGE_BSIX = 6
cdef uint16_t KRX_MESSAGE_GSEVEN = 7
cdef uint16_t AFTER_MSG_TOPIC = 4


cpdef double wmid(long bidprc, long bidqty, long askprc, long askqty) nogil:
    return 1.0 * (bidprc * askqty + askprc * bidqty) / (bidqty + askqty)


cdef bint is_krx_trade(int64_t msg_type) nogil:
    return msg_type != KRX_MESSAGE_BSIX


cdef bint is_krx_book(int64_t msg_type) nogil:
    return msg_type != KRX_MESSAGE_ATHREE


cdef void fill_topic_msg(int64_t msg_type, MDOrderBook *md) nogil:
    if msg_type == KRX_MESSAGE_ATHREE:
        md.topic[2] = 'A'
        md.topic[3] = '3'
    elif msg_type == KRX_MESSAGE_BSIX:
        md.topic[2] = 'B'
        md.topic[3] = '6'
    if msg_type == KRX_MESSAGE_GSEVEN:
        md.topic[2] = 'G'
        md.topic[3] = '7'


cdef void fill_topic_unknown_trade(int32_t tradeprc, int32_t tradeqty, MDOrderBook *md):
    cdef:
        uint16_t i
        char *topic = <char*>malloc(8 * sizeof(char))
    sprintf(topic, ';%d@%d', tradeqty, tradeprc)
    for 0 <= i < strlen(topic):
        md.topic[i + AFTER_MSG_TOPIC] = topic[i]
    free(topic)
    
    
cdef void clear_topic(MDOrderBook *md):
    cdef uint16_t i = AFTER_MSG_TOPIC
    while i < md.topic.size() and md.topic[i] != '_':
        md.topic[i] = ' '
        i += 1


def reformat_krx_md_generator(data):
    cdef:
        bint unknown_trade = False
        int32_t[:] tradeprcs, tradeqtys
        int32_t[:, :] bidprcs, bidqtys, askprcs, askqtys, bidpars, askpars
        uint64_t i, j, n = len(data), secid, msg
        uint64_t[:] secids, msgs, times
        map_[uint64_t, BookBuilder] builders
        map_[uint64_t, int32_t] last_msg
        BookBuilder *builder
        MDOrderBook *md
        BookLevel *bid
        BookLevel *ask
        bytes line
        uint64_t seqnum = 1

    secids =  data['secid'].values.astype(uint64)
    msgs = data['msg_int'].values.astype(uint64)
    times = data['time'].astype(int64).values.astype(uint64)
    tradeprcs = (data['tradeprc']*100).round().values.astype(int32)
    tradeqtys = data['tradeqty'].values.astype(int32)
    bidprcs = (data[filter(lambda x: 'bidprc' in x, data.columns)]*100).round().values.astype(int32)
    askprcs = (data[filter(lambda x: 'askprc' in x, data.columns)]*100).round().values.astype(int32)
    bidqtys = data[filter(lambda x: 'bidqty' in x, data.columns)].values.astype(int32)
    askqtys = data[filter(lambda x: 'askqty' in x, data.columns)].values.astype(int32)
    bidpars = data[filter(lambda x: 'bidpar' in x, data.columns)].values.astype(int32)
    askpars = data[filter(lambda x: 'askpar' in x, data.columns)].values.astype(int32)
    
    line = PrintMDOrderBookColumns(KRX_NUM_LEVELS)
    yield line
    for 0 <= i < n:
        msg = msgs[i]
        secid = secids[i]
        if builders.find(secid) == builders.end():
            builders[secid].Init(secid)
            last_msg[secid] = -1
        builder = &(builders[secid])
        md = &(builder.md)
        
        md.seqnum = seqnum
        md.time = times[i]
        fill_topic_msg(msg, md)
        
        md.buyprice = 0
        md.buyvolume = 0
        md.sellprice = 0
        md.sellvolume = 0
        
        if is_krx_trade(msg):
            if last_msg[secid] > 0:
                bid = &(md.bids[0])
                ask = &(md.asks[0])
                if tradeprcs[i] == ask.price:
                    md.buyprice = tradeprcs[i]
                    md.buyvolume = tradeqtys[i]
                elif tradeprcs[i] == bid.price:
                    md.sellprice = tradeprcs[i]
                    md.sellvolume = tradeqtys[i]
                else:
                    fill_topic_unknown_trade(tradeprcs[i], tradeqtys[i], md)
                    unknown_trade = True
                if msg == KRX_MESSAGE_ATHREE:
                    if unknown_trade:
                        line = md.ToCSV()
                        clear_topic(md)
                        unknown_trade = False
                        yield line
                    else:
                        if last_msg[secid] != KRX_MESSAGE_ATHREE:
                            builder.bids.Clear()
                            builder.asks.Clear()
                            j = 0
                            while j < KRX_NUM_LEVELS and md.bids[j].qty > 0:
                                builder.bids.Add(md.bids[j].price, md.bids[j].qty, md.bids[j].participants)
                                j += 1
                            j = 0
                            while j < KRX_NUM_LEVELS and md.asks[j].qty > 0:
                                builder.asks.Add(md.asks[j].price, md.asks[j].qty, md.asks[j].participants)
                                j += 1
                        if md.buyvolume > 0:
                            builder.asks.Remove(ask.price, ask.qty, True, ask.participants)
                        else:
                            builder.bids.Remove(bid.price, bid.qty, True, bid.participants)
                        builder.FillMDOrderBook()
                        line = md.ToCSV()
                        yield line

        if is_krx_book(msg):
            for 0 <= j < KRX_NUM_LEVELS:
                bid = &(md.bids[j])
                bid.price = bidprcs[i, j]
                bid.qty = bidqtys[i, j]
                bid.participants = bidpars[i, j]
                ask = &(md.asks[j])
                ask.price = askprcs[i, j]
                ask.qty = askqtys[i, j]
                ask.participants = askpars[i, j]
            line = md.ToCSV()
            if unknown_trade:
                clear_topic(md)
                unknown_trade = False
            yield line
        
        last_msg[secid] = msg
        seqnum += 1


def wmid_array(df):
    cdef:
        long tlen = df.shape[0], i
        long[:] bidprc = df.bidprc0.values
        long[:] askprc = df.askprc0.values
        long[:] bidqty = df.bidqty0.values
        long[:] askqty = df.askqty0.values
        double[:] results = zeros(tlen, dtype=float64)

    for i in range(tlen):
        results[i] = wmid(bidprc[i], bidqty[i], askprc[i], askqty[i])
    return results


def msg_sequence_numbers(df):
    cdef:
        long tlen = df.shape[0], i, code
        dict msg_counts = {}
        long[:] codes = df.code_int.values
        long[:] results = zeros(tlen, dtype=long)

    for i in range(tlen):
        code = codes[i]
        if code not in msg_counts:
            msg_counts[code] = -1
        msg_counts[code] += 1
        results[i] = msg_counts[code]

    return results

    
cdef class KRXMDHandler:
    cdef:
        BBO bbo
        int levels
        
    def __cinit__(self, int levels):
        self.bbo = BBO.__new__(BBO, levels)
        self.levels = levels

    cdef long trade_direction(self, long tradeprice):
        cdef:
            long[:] best = self.bbo.best
        if tradeprice >= best[2]:
            return 1
        elif tradeprice <= best[1]:
            return 2
        else:
            return 0
        
    cdef void update_b6(self, long[:] bidprices, long[:] bidqtys, long[:] askprices, long[:] askqtys):
        self.bbo.clear_book()
        self.bbo.update_book_array(1, bidprices, bidqtys)
        self.bbo.update_book_array(2, askprices, askqtys)
    
    cdef long update_g7(self, long[:] bidprices, long[:] bidqtys, long[:] askprices, long[:] askqtys, long tradeprice, long tradeqty):
        cdef long tradeside = self.trade_direction(tradeprice)
        self.bbo.clear_book()
        if bidqtys[0] > 0:
            self.bbo.update_book_array(1, bidprices, bidqtys)
        if askqtys[0] > 0:
            self.bbo.update_book_array(2, askprices, askqtys)
        return tradeside
    
    cdef long update_a3(self, long tradeprice, long tradeqty):
        cdef long tradeside = self.trade_direction(tradeprice)
        self.bbo.update_book_by_nocheck(2 if tradeside == 1 else 1, tradeprice, -1 * tradeqty)
        return tradeside
    
    cdef void write_trades(self, long[:, :] output, long i, long tradeidx, long tradeside, long tradeprice, long tradeqty):
        output[i, tradeidx] = tradeprice
        output[i, tradeidx + 1] = tradeqty
    
    cdef void write_output(self, long[:, :] output, long i, long tradeside, long tradeprice, long tradeqty):
        cdef:
            long bidprice = 0, askprice = 0
            int j = 0, n = self.levels
            int askidx = 2*n, tradeidx
            BBO bbo = self.bbo
        for j in range(n):
            bidprice, askprice = bbo.bidprices[j], bbo.askprices[j]
            output[i, 2*j + 1] = bidprice
            output[i, 2*j + 2] = bbo.at(1, bidprice)
            output[i, 2*j + askidx + 1] = askprice
            output[i, 2*j + askidx + 2] = bbo.at(2, askprice)
        if tradeside:
            tradeidx = 4*n+1 + (2 if tradeside == 2 else 0)
            self.write_trades(output, i, tradeidx, tradeside, tradeprice, tradeqty)
            
def a3_book_building(df, a3_open=True):
    cdef:
        long tlen = df.shape[0], i, count = 0
        int levels = 5
        
        long[:] df_index = df.index.values
        long[:] msgs = df.msg_int.values
        str[:] timestrs = df.timestr.values
        
        long[:, :] bidprices = df.loc[:, ['bidprc' + str(i) for i in range(levels)]].values
        long[:, :] bidqtys = df.loc[:, ['bidqty' + str(i) for i in range(levels)]].values
        long[:, :] askprices = df.loc[:, ['askprc' + str(i) for i in range(levels)]].values
        long[:, :] askqtys = df.loc[:, ['askqty' + str(i) for i in range(levels)]].values
        
        long[:] tradeprices = df.tradeprc.values
        long[:] tradeqtys = df.tradeqty.values
        
        long tradeprice = 0, tradeqty = 0, tradeside = 0, buyside = 0, sellside = 0
        
        KRXMDHandler md_handler = KRXMDHandler(levels)
        
        bint market_open
        
        ndarray output = zeros((tlen, levels * 4 + 5), dtype=long)
        list newcol = [ba + pq + str(i) for ba in ('bid', 'ask') for i in range(levels) for pq in ('prc', 'qty')] + ['buyprice', 'buyvolume', 'sellprice', 'sellvolume']
    
    market_open = not a3_open
    for 0 <= i < tlen:
        if not market_open:
            if msgs[i] == 0:
                market_open = True
                continue
            elif msgs[i] == 1:
                market_open = True
        if msgs[i] == 0:
            tradeprice, tradeqty = tradeprices[i], tradeqtys[i]
            tradeside = md_handler.update_a3(tradeprice, tradeqty)
            md_handler.write_output(output, i, tradeside, tradeprice, tradeqty)
            output[i, 0] = df_index[i]
        elif msgs[i] == 1:
            tradeprice, tradeqty = tradeprices[i], tradeqtys[i]
            tradeside = md_handler.update_g7(bidprices[i], bidqtys[i], askprices[i], askqtys[i], tradeprice, tradeqty)
            md_handler.write_output(output, i, tradeside, tradeprice, tradeqty)
            output[i, 0] = df_index[i]
        elif msgs[i] == 2:
            md_handler.update_b6(bidprices[i], bidqtys[i], askprices[i], askqtys[i])
            
    return DataFrame(output[output[:, 0]!=0, 1:], index=output[output[:, 0]!=0, 0], columns=newcol)

