# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True

from libc.math cimport NAN
from numpy cimport ndarray

import os

from collections import namedtuple
from glob import glob
from numpy import zeros, int64, float64, fromfile, concatenate
from warnings import warn


cdef uint16_t MD_JPX = 1
cdef uint16_t MD_KRX = 2
cdef uint16_t MD_DEPRECATED_JPX = 3
cdef uint16_t JPX_STOP_PARSING_AT = 10
cdef uint16_t KRX_STOP_PARSING_AT = 10
cdef uint16_t DEPRECATED_JPX_STOP_PARSING_AT = 7
cdef uint16_t TOPIC_TYPE_MSG = 2
cdef double ALMOST_ONE_HUNDRED_PCT = 0.99999

cdef double KRX_PRICE_MULTIPLIER = 100.0
cdef double JPA_PRICE_MULTIPLIER = 10000.0

cdef str WARNING_STR_FMT_CROSSED_BOOK = 'book crossed; secid=%d, seqnum=%d'


cdef list sample_dtype = [ 
    ('time', '<u8'),
    ('num_measurements', '<u8'),
    ('value', '<f8')
]


cdef void copy_mdorderbook(MDOrderBook *src, MDOrderBook *copy, uint64_t num_levels=MAX_DEPTH) nogil:
    cdef uint16_t i
    copy.packet_len = src.packet_len
    copy.eop = src.eop
    for 0 <= i < TOPIC_SIZE:
        copy.topic[i] = src.topic[i]
    copy.secid = src.secid
    copy.seqnum = src.seqnum
    copy.time = src.time
    copy.exch_time = src.exch_time
    copy.buyprice = src.buyprice
    copy.buyvolume = src.buyvolume
    copy.sellprice = src.sellprice
    copy.sellvolume = src.sellvolume
    if num_levels > MAX_DEPTH:
        num_levels = MAX_DEPTH
    for 0 <= i < num_levels:
        copy.bids[i].price = src.bids[i].price
        copy.bids[i].qty = src.bids[i].qty
        copy.bids[i].participants = src.bids[i].participants
        copy.asks[i].price = src.asks[i].price
        copy.asks[i].qty = src.asks[i].qty
        copy.asks[i].participants = src.asks[i].participants
    copy.is_open = src.is_open
    copy.bid_avol_qty = src.bid_avol_qty
    copy.ask_avol_qty = src.ask_avol_qty


cdef double calc_wmid(double bidprc, int64_t bidqty, double askprc, int64_t askqty, double mintick) nogil:
    cdef int64_t total_qty = bidqty + askqty
    if total_qty == 0:
        return NAN
    elif askprc - bidprc < mintick:
        return (bidprc * <double>askqty + askprc * <double>bidqty) / <double>total_qty
    else:
        return 0.5 * (bidprc + askprc)
    

cdef double calc_wmid_from_mdorderbook(MDOrderBook *md, double price_multiplier, double mintick) nogil:
    cdef:
        double bidprc = <double>md.bids[0].price / price_multiplier
        double askprc = <double>md.asks[0].price / price_multiplier
    return calc_wmid(bidprc, md.bids[0].qty, askprc, md.asks[0].qty, mintick)


cdef double calc_imbalance(int32_t bidqty, int32_t askqty) nogil:
    return <double>(bidqty - askqty) / (bidqty + askqty)


cdef void copy_values_to_memoryview(MVRWithinBounds *mvr, double[:] output, uint64_t size_output) nogil:
    mvr.CopyValuesToArray(&(output[0]), size_output)


cdef void calc_mvr_entropy(MeanValueResampler *mvr, double half_life, double[:] w, uint64_t size_w) nogil:
    CalculateMVREntropy(mvr, half_life, &(w[0]), size_w)


cdef void calc_mvr_diff(MeanValueResampler *mvr_left, MeanValueResampler *mvr_right, double[:] output, uint64_t size_output) nogil:
    CalculateMVRDiffAndCopyValuesToArray(mvr_left, mvr_right, &(output[0]), size_output)


cdef double calc_mvr_entropy_weighted_value(MeanValueResampler *mvr, double half_life) nogil:
    return CalculateMVREntropyWeightedValue(mvr, half_life)


def which_md(exchange):
    if exchange.lower() in ('jpx', 'jpa'):
        return MD_JPX
    elif exchange.lower() == 'krx':
        return MD_KRX
    elif exchange.lower() in ('deprecated_jpx', 'deprecated_jpa'):
        msg = 'using deprecated_jpx. you should probably start using \'jpx\' for simulations'
        warn(msg, DeprecationWarning)
        return MD_DEPRECATED_JPX
    else:
        raise ValueError('please enter valid exchange (KRX or JPX)')


cdef str mdorderbook_repr(MDOrderBook *md, uint16_t book_depth=1):
    cdef:
        BookLevel *bid = &(md.bids[0])
        BookLevel *ask = &(md.asks[0])
        BookLevel *bid2 = &(md.bids[1])
        BookLevel *ask2 = &(md.asks[1])

    """
    old code:
    cdef:
        BookLevel *bid = &(md.bids[0])
        BookLevel *ask = &(md.asks[0])

    return '%d/%d %d(%d)x%d(%d)' % (bid.price, ask.price, bid.qty, bid.participants, ask.qty, ask.participants)
    """
    if book_depth == 1:
        book = '%d/%d %d(%d)x%d(%d)' % (bid.price, ask.price, bid.qty, bid.participants, ask.qty, ask.participants)
    elif book_depth == 2:
        book =  '%d/%d %d(%d):%d(%d)x%d(%d):%d(%d)' % (bid.price, ask.price, bid2.qty, bid2.participants,
                            bid.qty, bid.participants, ask.qty, ask.participants, ask2.qty, ask2.participants)
    else:
        book = 'book_depth variable not a 1 or 2, fix or add a condition to mdorderbook_repr'
    return book


def find_my_dtype(uint16_t num_levels=5):
    """find_my_dtype(num_levels=5)

Construct a numpy dtype list which resembles MDOrderBook, which is the
basis of all standard market data used within GLT. Specify how many levels
of the market data order book the data has using num_levels.

Parameters
----------
num_levels : int
    Number of order book levels for market data (i.e. the size of each bid
    and ask vectors in MDOrderBook (see glt/md/standard.pyx source for
    more information)

    """
    if num_levels == 0:
        raise ValueError('num_levels must be >0')
    my_dtype = [
        ('pkt_len', '<i2'),
        ('eop', '<i2'),
        ('channel', '<u8'),
        ('msg_idx', '<u2'),
        ('topic', 'S16'),
        ('secid', '<u8'),
        ('seqnum', '<u8'),
        ('time', '<u8'),
        ('exch_time', '<u8'),
        ('buyprice', '<i4'),
        ('buyvolume', '<i4'),
        ('sellprice', '<i4'),
        ('sellvolume', '<i4')
    ]
    for i in xrange(num_levels):
        my_dtype.append(('bidprc%d' % i, '<i4'))
        my_dtype.append(('bidqty%d' % i, '<i4'))
        my_dtype.append(('bidpar%d' % i, '<i4'))
    for i in xrange(num_levels):
        my_dtype.append(('askprc%d' % i, '<i4'))
        my_dtype.append(('askqty%d' % i, '<i4'))
        my_dtype.append(('askpar%d' % i, '<i4'))
    return my_dtype


def read_md_dat(str filepath, str identifier, uint16_t num_levels=5):
    """read_md_dat(filepath, identifier, num_levels=5)

Read series of binary files (with extension .dat) from a specified directory.
Also assumed is a unique identifier, which is each dat file's prefix. Each dat
filename is followed by digits indicating the order in which the files should
be read (e.g. identifier=md, first dat filename=md.00001.dat). These binary
files assume a fixed structure of bytes, so using numpy's fromfile works
efficiently as long as you specify the proper dtype. The underlying function
for determining the appropriate dtype is find_my_dtype. Each dat file is read
into a numpy array and saved in a list, which is then concatenated together
into one array.

Parameters
----------
filepath : str
    The filepath to read the dat files
    (e.g. /glt/ssd/backtesting/20161025/books)
identifier : str
    The unique identifier/prefix to locate all relevant dat files in
    the specified filepath (e.g. identifier=md will find all files which
    match filepath/md.*.dat)
num_levels : int
    Number of order book levels for market data used to build numpy's
    array dtype. See find_my_dtype for more information.

    """
    filenames = glob(os.path.join(filepath, '%s.*.dat' % identifier))
    filenames.sort()
    data = []
    my_dtype = find_my_dtype(num_levels=num_levels)
    for filename in filenames:
        data.append(fromfile(filename, dtype=my_dtype))
    return concatenate(data)


cdef dict renamed_krx_columns = {
    '#pktNum': 'pkt_num',
    'ts': 'timestr',
    'tsInt': 'time',
    'exchTsInt': 'exch_time',
    'channel': 'chan',
    'seqNum': 'seqnum',
    'msgType': 'msg',
    'securityId': 'secid',
    'mdStatus': 'status',
    'exchMdStatus': 'exch_status',
    'tradePrice': 'tradeprc',
    'tradeQty': 'tradeqty',
    'tradeAggressorSide': 'tradedir',
    'totalVolumeTraded': 'tot_volume',
    'totalBidQty': 'tot_bidqty',
    'totalOfferQty': 'tot_askqty',
    'totalBidNumPart': 'tot_bidpar',
    'totalOfferNumPart': 'tot_askpar',
    'indicPrice': 'indicative',
    'bPx0': 'bidprc0',
    'bPx1': 'bidprc1',
    'bPx2': 'bidprc2',
    'bPx3': 'bidprc3',
    'bPx4': 'bidprc4',
    'oPx0': 'askprc0',
    'oPx1': 'askprc1',
    'oPx2': 'askprc2',
    'oPx3': 'askprc3',
    'oPx4': 'askprc4',
    'bQty0': 'bidqty0',
    'bQty1': 'bidqty1',
    'bQty2': 'bidqty2',
    'bQty3': 'bidqty3',
    'bQty4': 'bidqty4',
    'oQty0': 'askqty0',
    'oQty1': 'askqty1',
    'oQty2': 'askqty2',
    'oQty3': 'askqty3',
    'oQty4': 'askqty4',
    'bNP0': 'bidpar0',
    'bNP1': 'bidpar1',
    'bNP2': 'bidpar2',
    'bNP3': 'bidpar3',
    'bNP4': 'bidpar4',
    'oNP0': 'askpar0',
    'oNP1': 'askpar1',
    'oNP2': 'askpar2',
    'oNP3': 'askpar3',
    'oNP4': 'askpar4'
}


def krx_price_multiplier():
    return KRX_PRICE_MULTIPLIER


def read_krx_csv(filename, buysell=False, raw=False, time_adjust='9h', open_only=False,
                 start_time=None, end_time=None, use_int_prices=False, chunksize=20000):
    """read_krx_csv(filename, buysell=False, raw=False, time_adjust='9h', open_only=False,
                    start_time=None, end_time=None, use_int_prices=False, chunksize=20000)

Read CSV-formatted KRX market data from Bruce's krxmdpcap. Column names are
renamed to familiar format (time, exch_time, seqnum, bidprc0, bidqty0, etc).

Parameters
----------
filename : str
    CSV filepath. It may take the form of krxbooks.*.csv or krx.book.*.csv.
    It may also be gzipped; no unzipping necessary if it is.
buysell : bool
    If you want separate buy/sell columns for trades (buyprice, buyvolume,
    sellprice, sellvolume), set to True. Default is False.
raw : bool
    If you want all columns available, set to True. Default is False.
time_adjust : str
    String-formatted timedelta to adjust datetime of time and exch_time
    columns. Default is 9h because time and exch_time are written in UTC.
open_only : bool
    Result will only have market-data status == Open. Default is False.
start_time : str, unicode, datetime
    If set, file will only be read after this time (uses chunking). Default is None.
end_time : str, unicode, datetime
    If set, file will only be read until this time (uses chunking). Default is None.
use_int_prices : bool
    If True, convert prices (raw data provides float data) to integer-based
    with arbitrary multiplier 100. Use krx_price_multiplier() to verify. 
    Default is False.
chunksize : int
    Only applicable if end_time is not None; specifies the size of each chunk
    read per iteration until end_time is reached in the file. Default is 20000.
    """

    from pandas import read_csv, to_datetime, to_timedelta, DataFrame
    time_cols = [
        'time', 'exch_time'
    ]
    if start_time is not None or end_time is not None:
        if type(start_time) in (str, unicode):
            start_time = to_datetime(start_time)
        if type(end_time) in (str, unicode):
            end_time = to_datetime(end_time)
        df = []
        for dat in read_csv(filename, chunksize=chunksize):
            dat.rename(columns=renamed_krx_columns, inplace=True)
            # convert some time columns
            for col in time_cols:
                dat[col] = to_datetime(dat[col]) + to_timedelta(time_adjust)
            if start_time is None or len(dat.loc[dat['time']>=start_time]) > 0:
                df.append(dat)
            if end_time is not None and len(dat.loc[dat['time']<=end_time]) == 0:
                break
        df = DataFrame().append(df)
    else:
        df = read_csv(filename)
        df.rename(columns=renamed_krx_columns, inplace=True)
        for col in time_cols:
            df[col] = to_datetime(df[col]) + to_timedelta(time_adjust)
    # delete some columns
    del_cols = [
        'timestr'
    ]
    for col in del_cols:
        del df[col]
    df.loc[df['tradedir']=='U', 'tradedir'] = '-'
    # do we want to keep the raw columns?
    if not raw:
        del_raw_cols = [
            'pkt_num', 'exch_status', 'chan'
        ]
        for col in del_raw_cols:
            del df[col]
    # convert prices to int
    if use_int_prices:
        for col in filter(lambda x: 'prc' in x, df.columns) + ['indicative']:
            df[col] = (df[col] * KRX_PRICE_MULTIPLIER).fillna(0).round().astype(int)
    # how about buy-sell columns?
    if buysell:
        buysell_cols = [
            'buyprice', 'buyvolume', 'sellprice', 'sellvolume'
        ]
        for col in buysell_cols:
            df[col] = 0
        buy_trade = df['tradedir']=='B'
        sell_trade = df['tradedir']=='O'
        df.loc[buy_trade, 'buyprice'] = df.loc[buy_trade, 'tradeprc']
        df.loc[buy_trade, 'buyvolume'] = df.loc[buy_trade, 'tradeqty']
        df.loc[sell_trade, 'sellprice'] = df.loc[sell_trade, 'tradeprc']
        df.loc[sell_trade, 'sellvolume'] = df.loc[sell_trade, 'tradeqty']
    if open_only:
        df = df.loc[df['status']=='Open']
    return df


cdef dict renamed_jpa_columns = {
    '#pktNum': 'pkt_num',
    'ts': 'timestr',
    'tsInt': 'time',
    'exchTsInt': 'exch_time',
    'channel': 'chan',
    'seqNum': 'seqnum',
    'msgType': 'msg',
    'securityId': 'secid',
    'mdStatus': 'status',
    'exchMdStatus': 'exch_status',
    'tradePrice': 'tradeprc',
    'tradeQty': 'tradeqty',
    'tradeAggressorSide': 'tradedir',
    'totalVolumeTraded': 'tot_volume',
    'totalBidQty': 'tot_bidqty',
    'totalOfferQty': 'tot_askqty',
    'totalBidNumPart': 'tot_bidpar',
    'totalOfferNumPart': 'tot_askpar',
    'indicPrice': 'indicative',
    'bPx0': 'bidprc0',
    'bPx1': 'bidprc1',
    'bPx2': 'bidprc2',
    'bPx3': 'bidprc3',
    'bPx4': 'bidprc4',
    'oPx0': 'askprc0',
    'oPx1': 'askprc1',
    'oPx2': 'askprc2',
    'oPx3': 'askprc3',
    'oPx4': 'askprc4',
    'bQty0': 'bidqty0',
    'bQty1': 'bidqty1',
    'bQty2': 'bidqty2',
    'bQty3': 'bidqty3',
    'bQty4': 'bidqty4',
    'oQty0': 'askqty0',
    'oQty1': 'askqty1',
    'oQty2': 'askqty2',
    'oQty3': 'askqty3',
    'oQty4': 'askqty4',
    'bNP0': 'bidpar0',
    'bNP1': 'bidpar1',
    'bNP2': 'bidpar2',
    'bNP3': 'bidpar3',
    'bNP4': 'bidpar4',
    'oNP0': 'askpar0',
    'oNP1': 'askpar1',
    'oNP2': 'askpar2',
    'oNP3': 'askpar3',
    'oNP4': 'askpar4',
    'bidAvolQty': 'avol_bidqty',
    'offerAvolQty': 'avol_askqty',
    'endOfPacket': 'eop',
    'pktLen': 'pkt_len'
}


def jpa_price_multiplier():
    return JPA_PRICE_MULTIPLIER


def read_jpa_csv(filename, buysell=False, raw=False, time_adjust='9h', open_only=False,
                 start_time=None, end_time=None, use_int_prices=False, chunksize=20000):
    """read_jpa_csv(filename, buysell=False, raw=False, time_adjust='9h', open_only=False,
                    start_time=None, end_time=None, use_int_prices=False, chunksize=20000)

Read CSV-formatted JPA market data from Bruce's jpamdpcap. Column names are
renamed to familiar format (time, exch_time, seqnum, bidprc0, bidqty0, etc).

Parameters
----------
filename : str
    CSV filepath. It may take the form of jpabooks.*.csv or jpa.book.*.csv.
    It may also be gzipped; no unzipping necessary if it is.
buysell : bool
    If you want separate buy/sell columns for trades (buyprice, buyvolume,
    sellprice, sellvolume), set to True. Default is False.
raw : bool
    If you want all columns available, set to True. Default is False.
time_adjust : str
    String-formatted timedelta to adjust datetime of time and exch_time
    columns. Default is 9h because time and exch_time are written in UTC.
open_only : bool
    Result will only have market-data status == Open. Default is False.
use_int_prices : bool
    If True, convert prices (raw data provides float data) to integer-based
    with arbitrary multiplier 10000. Use jpa_price_multiplier() to verify. 
    Default is False.
start_time : str, unicode, datetime
    If set, file will only be read after this time (uses chunking). Default is None.
end_time : str, unicode, datetime
    If set, file will only be read until this time (uses chunking). Default is None.
chunksize : int
    Only applicable if end_time is not None; specifies the size of each chunk
    read per iteration until end_time is reached in the file. Default is 20000.
    """

    from pandas import read_csv, to_datetime, to_timedelta, DataFrame
    time_cols = [
        'time', 'exch_time'
    ]
    if start_time is not None or end_time is not None:
        if type(start_time) in (str, unicode):
            start_time = to_datetime(start_time)
        if type(end_time) in (str, unicode):
            end_time = to_datetime(end_time)
        df = []
        for dat in read_csv(filename, chunksize=chunksize):
            dat.rename(columns=renamed_jpa_columns, inplace=True)
            # convert some time columns
            for col in time_cols:
                dat[col] = to_datetime(dat[col]) + to_timedelta(time_adjust)
            if start_time is None or len(dat.loc[dat['time']>=start_time]) > 0:
                if start_time is None:
                    df.append(dat)
                elif end_time is not None:
                    df.append(dat.loc[(dat['time']>=start_time)&(dat['time']<=end_time)])
                else:
                    df.append(dat.loc[(dat['time']>=start_time)])
            if end_time is not None and len(dat.loc[dat['time']<=end_time]) == 0:
                break
        df = DataFrame().append(df)
    else:
        df = read_csv(filename)
        df.rename(columns=renamed_jpa_columns, inplace=True)
        for col in time_cols:
            df[col] = to_datetime(df[col]) + to_timedelta(time_adjust)
    # delete some columns
    del_cols = [
        'timestr'
    ]
    for col in del_cols:
        del df[col]
    df.loc[df['tradedir']=='U', 'tradedir'] = '-'
    # split pkt_num with interface column
    #pkt_iface = df['pkt_num'].str.extract(r'^(.+)(.)$', expand=False)
    # woops had to level up karls regex.  [0-9]+ = any digits, [AB]? = one or none A or B, [12]? = one or none 1 or 2
    pkt_iface = df['pkt_num'].str.extract(r'^([0-9]+)([AB]?[12]?)$', expand=False)
    df['pkt_num'] = pkt_iface[0].astype(int)
    df['iface'] = pkt_iface[1]
    df['eop'] = df['eop'].astype(bool)
    # do we want to keep the raw columns?
    if not raw:
        del_raw_cols = [
            'exch_status', 'chan'
        ]
        for col in del_raw_cols:
            del df[col]
    # convert prices to int
    if use_int_prices:
        for col in filter(lambda x: 'prc' in x, df.columns) + ['indicative']:
            df[col] = (df[col] * JPA_PRICE_MULTIPLIER).fillna(0).round().astype(int)
    # how about buy-sell columns?
    if buysell:
        buysell_cols = [
            'buyprice', 'buyvolume', 'sellprice', 'sellvolume'
        ]
        for col in buysell_cols:
            df[col] = 0
        buy_trade = df['tradedir']=='B'
        sell_trade = df['tradedir']=='O'
        df.loc[buy_trade, 'buyprice'] = df.loc[buy_trade, 'tradeprc']
        df.loc[buy_trade, 'buyvolume'] = df.loc[buy_trade, 'tradeqty']
        df.loc[sell_trade, 'sellprice'] = df.loc[sell_trade, 'tradeprc']
        df.loc[sell_trade, 'sellvolume'] = df.loc[sell_trade, 'tradeqty']
    if open_only:
        df = df.loc[df['status']=='Open']
    return df


def find_last_market(trades, market, str timecol, list columns):
    cdef:
        size_t n_trades = len(trades), n_market = len(market), i, j = 0, k, num_columns = len(columns)
        int64_t[:] trades_exch_time = trades[timecol].values
        int64_t[:, :] market_arr = market[columns].values
        ndarray output = zeros((n_trades, num_columns), dtype=int64)

    for 0 <= i < n_trades:
        while j < n_market and market_arr[j, 0] <= trades_exch_time[i]:
            j += 1
        j -= 1
        for 0 <= k < num_columns:
            output[i, k] = market_arr[j, k]

    return output


def find_following_trades(trades_a, trades_b, str timecol, int64_t max_trade_count=5, int64_t start_after=0, int64_t terminate_after=1000000, int64_t minqty=0):
    cdef:
        bint correct_price
        double[:] pcts_b = trades_b['tradepct'].values
        int64_t trade_count = 0
        int64_t[:] times_a = trades_a[timecol].values, times_b = trades_b[timecol].values
        int64_t[:] index_a = trades_a.index.values, index_b = trades_b.index.values
        int64_t[:] side_a = trades_a['trade'].values, side_b = trades_b['trade'].values
        int64_t[:] price_a = trades_a['tradeprc'].values, price_b = trades_b['tradeprc'].values
        int64_t[:] qtys_b = trades_b['tradeqty'].values
        uint64_t n_a = len(trades_a), n_b = len(trades_b), i=0, j=0, k=0, idx
        ndarray output = zeros((n_a*max_trade_count, 3), dtype=int64)

    if trade_count <= 0:
        trade_count = 1
    output[:, 0] = -1

    for 0 <= i < n_a:
        trade_count = 0
        count = 0
        while j < n_b and times_a[i] >= times_b[j]:
            j += 1

        if j == n_b:
            continue

        idx = j
        uniq_b_val = 0
        while idx < n_b and trade_count < max_trade_count and times_b[idx] - times_a[i] > start_after and times_b[idx] - times_a[i] < terminate_after:
            correct_price = (side_a[i] > 0 and price_a[i] >= price_b[idx]) or (side_a[i] < 0 and price_a[i] <= price_b[idx])
            if side_a[i] == side_b[idx] and correct_price and qtys_b[idx]>minqty:
                output[k, 0] = index_a[i]
                output[k, 1] = index_b[idx]
                trade_count += 1
                k += 1
                if price_a[i] == price_b[idx] and pcts_b[idx] > ALMOST_ONE_HUNDRED_PCT:
                    break
            idx += 1
    return output


PyBookLevel = namedtuple('PyBookLevel', ['price', 'qty', 'participants'])


cdef class PyMDOrderBook:
    def __init__(self):
        self.bids = None
        self.asks = None
        self.md_ptr = NULL

    def __repr__(self):
        return (
            'PyMDOrderBook{secid=%d, seqnum=%d, time=%d, '
            'trade=(%d, %d, %d, %d), top_bid=(%d, %d, %d), top_ask=(%d, %d, %d)}'
        ) % (
            self.secid, self.seqnum, self.time, 
            self.buyprice, self.buyvolume, self.sellprice, self.sellvolume,
            self.bids[0].price, self.bids[0].qty, self.bids[0].participants,
            self.asks[0].price, self.asks[0].qty, self.asks[0].participants
        )

    cdef void copy(self, MDOrderBook *md, bint parse_topic=False):
        cdef:
            list topic_elements
            uint64_t i, n_levels = md.bids.size()
            BookLevel *bid
            BookLevel *ask
        self.packet_len = md.packet_len
        self.eop = md.eop
        self.msg_idx = md.msg_idx
        self.secid = md.secid
        self.seqnum = md.seqnum
        self.time = md.time
        self.exch_time = md.exch_time
        self.channel = md.channel
        self.buyprice = md.buyprice
        self.buyvolume = md.buyvolume
        self.sellprice = md.sellprice
        self.sellvolume = md.sellvolume
        self.is_open = md.is_open
        self.bid_avol_qty = md.bid_avol_qty
        self.ask_avol_qty = md.ask_avol_qty
        if self.bids is None or self.asks is None:
            self.bids = []
            self.asks = []
            for 0 <= i < n_levels:
                bid = &(md.bids[i])
                ask = &(md.asks[i])
                self.bids.append(PyBookLevel(
                    price=bid.price, qty=bid.qty, participants=bid.participants
                ))
                self.asks.append(PyBookLevel(
                    price=ask.price, qty=ask.qty, participants=ask.participants
                ))
        if parse_topic:
            topic_elements = []
            for 0 <= i < md.topic.size():
                topic_elements.append(chr(md.topic[i]))
            self.topic = ''.join(topic_elements)
        # and finally, copy the ptr
        self.md_ptr = md


cdef class MDSource:
    cdef void _parse_csv_begin(self, const char* line, uint16_t line_len, uint16_t md_type) nogil:
        cdef MDOrderBook *md = &(self.md)
        if md_type == MD_JPX:
            md.ParseJPXCSV(line, line_len, 0, JPX_STOP_PARSING_AT)
        elif md_type == MD_KRX:
            md.ParseKRXCSV(line, line_len, 0, KRX_STOP_PARSING_AT)
        elif md_type == MD_DEPRECATED_JPX:
            md.ParseDeprecatedJPXCSV(line, line_len, 0, DEPRECATED_JPX_STOP_PARSING_AT)
            
    cdef void _parse_csv_end(self, const char* line, uint16_t line_len, uint16_t md_type) nogil:
        cdef MDOrderBook *md = &(self.md)
        if md_type == MD_JPX:
            md.ParseJPXCSV(line, line_len, JPX_STOP_PARSING_AT, 255)
        elif md_type == MD_KRX:
            md.ParseKRXCSV(line, line_len, KRX_STOP_PARSING_AT, 255)
        elif md_type == MD_DEPRECATED_JPX:
            md.ParseDeprecatedJPXCSV(line, line_len, DEPRECATED_JPX_STOP_PARSING_AT, 255)
        else:
            return
        if md.bids[0].qty == 0 or md.asks[0].qty == 0:
            self.valid_md = False
        
            
    def initialize(self, list secids):
        cdef:
            map[uint64_t, MDOrderBook] new_map
            uint64_t secid
            MDOrderBook *md = &(self.md)

        self.md_map = new_map
        md.NullifyByteArray()
        self.valid_md = False
        for secid in secids:
            self.md_map[secid].NullifyByteArray()
        self.md_ptr = &(self.md)

    def parse_csv_md(self, bytes line, uint16_t md_type):
        cdef:
            MDOrderBook *md = &(self.md)
            uint16_t line_len = len(line)
            bint valid_market
        self._parse_csv_begin(line, line_len, md_type)
        self._update_valid_md()
        if self.valid_md:
            self._parse_csv_end(line, line_len, md_type)
            # need to check for crossing book
            if md.bids[0].qty > 0 and md.asks[0].qty > 0 and md.bids[0].price >= md.asks[0].price:
                self.valid_md = False
                #warn(WARNING_STR_FMT_CROSSED_BOOK % (md.secid, md.seqnum), RuntimeWarning)
                return
            copy_mdorderbook(md, &(self.md_map[md.secid]))

    def ref_md_from(self, MDSource src):
        self.md_ptr = src.md_ptr

    def copy_pymd_ptr(self, PyMDOrderBook pymd):
        self.md_ptr = pymd.md_ptr

    cdef void _update_valid_md(self) nogil:
        self.valid_md = self.md_map.find(self.md_ptr.secid) != self.md_map.end()

    def update_valid_md(self):
        self._update_valid_md()

