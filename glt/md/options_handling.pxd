from libc.stdint cimport *
from glt.md.resampling cimport Sample, MeanValueResampler


cdef extern from "options_handling.h" nogil:
    cdef:
        const double ALMOST_ZERO, M_1_SQRT2PI
        const double DEFAULT_TICK_THRESHOLD
        const uint64_t DEFAULT_ITERATIONS
        double NormalCDF(double x)
        double NormalPDF(double x)
        double CalculateD1(double strike, double tte, double u, double vol)
        double CalculateD2(double strike, double tte, double u, double vol)
        double NormalCDFD1(int16_t option_type, double strike, double tte, double u, double vol)
        double NormalCDFD2(int16_t option_type, double strike, double tte, double u, double vol)
        double NPVAdjust(double tte, double interest_rate)
        double CalculateTV(int16_t option_type, double strike, double tte, double interest_rate, double u, double vol)
        double CalculateDelta(int16_t option_type, double strike, double tte, double interest_rate, double u, double vol)
        double CalculateGamma(double strike, double tte, double interest_rate, double u, double vol)
        double CalculateVega(double strike, double tte, double interest_rate, double u, double vol)
        double CalculateImpliedVol(int16_t option_type, double strike, double tte, double interest_rate, double u, double price, double seed_vol, double tick_threshold, uint64_t iterations)
    cdef struct OptionImpliedValues:
        double moneyness, vol, delta, gamma, vega
    cdef cppclass Option:
        OptionImpliedValues implied
        Option()
        void Init(int16_t t_option_type, double t_strike, double t_tte, double t_interest_rate)
        bint Initialized() const
        void CalculateImpliedValues(double price, double u)
        double Moneyness() const
        double ImpliedVol() const
    cdef cppclass OptionPair:
        double u_bid, u_ask
        Option call_bid, call_ask, put_bid, put_ask
        OptionPair()
        void Init(double t_strike, double t_tte, double t_interest_rate)
        bint Initialized() const
        void UpdateUnderlying(double t_u_bid, double t_u_ask)
        int16_t WhichOTM() const
        void CalculateOTMImpliedValues(double bid_price, double ask_price, bint as_market_taker)
        OptionImpliedValues* ImpliedBidPtr()
        OptionImpliedValues* ImpliedAskPtr()
    cdef cppclass OptionResampler:
        OptionImpliedValues implied
        OptionResampler()
        void Init(int16_t t_option_type, double t_strike, double t_tte, double t_interest_rate, uint64_t resampling_interval)
        void Record(uint64_t time, double price, double u)
        void ForceSamplesAround(uint64_t start_time, uint64_t end_time)
        uint64_t NumSamples() const
        int8_t CopyPricesToArray(double *dst_arr, uint64_t size)
        int8_t CopyVolsToArray(double *dst_arr, uint64_t size)
        int8_t CopyPriceSamplesToArray(Sample *dst_arr, uint64_t size)
        int8_t CopyVolSamplesToArray(Sample *dst_arr, uint64_t size)
        uint64_t FirstSampleTime() const
        uint64_t LastSampleTime() const
        MeanValueResampler* PriceMVRPtr()
        MeanValueResampler* VolMVRPtr()
