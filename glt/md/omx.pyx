from __future__ import print_function

cimport cython
from numpy cimport ndarray
from libc.math cimport sqrt, exp, pow
from numpy import zeros, float64, int64, uint64, NaN, array as nparray, logical_and, logical_or
from pandas import DataFrame, Series, to_datetime

import os
import pickle

from glt.tools cimport *

cdef inline double _max(double a, double b): return a if a >= b else b
cdef inline double _min(double a, double b): return a if a <= b else b

# bd1 deal sources
cdef long DEALSRC_AUTO = 1
cdef long DEALSRC_OPEN_ALLOCATION = 20

# bo2 reasons
cdef long REASON_DELETE = 1
cdef long REASON_STOPPED = 2
cdef long REASON_DEAL = 3
cdef long REASON_INACTIVE = 4
cdef long REASON_CHANGE = 5
cdef long REASON_ADD = 6
cdef long REASON_MOD_MKT = 7
cdef long REASON_MOD_PRICE = 8
cdef long REASON_SYSTERM_DELETE = 9
cdef long REASON_PROXY_DELETE = 10
cdef long REASON_RECALCULATED = 11
cdef long REASON_ACTIVATED_STOP = 12
cdef long REASON_HV_RECALC = 13
cdef long REASON_LIMIT_CHANGE_DEL = 15
cdef long REASON_SYSTEM_DEL_DAY = 19
cdef long REASON_ISS_DEL = 20
cdef long REASON_ISS_INACTIVATE = 21
cdef long REASON_INACTIVATE_PURGE = 23
cdef long REASON_ENTERED_CENTRAL_INACTIVE_ORDER = 29
cdef long REASON_RELOAD_INTRADAY = 30
cdef long REASON_NO_LONGER_VALID = 33
cdef long REASON_AUCTION_DELETE = 34
cdef long REASON_COMBO_AUCTION_DELETE = 44

# date constants
cdef long START_YEAR = 1989
cdef long DAY_ANDMASK = 31
cdef long MONTH_ANDMASK = 480
cdef long YEAR_ANDMASK = 32256

# series constant labels
cdef str SERIES_COUNTRY = "s0"
cdef str SERIES_MARKET = "s1"
cdef str SERIES_INSTRUMENT_GROUP = "s2"
cdef str SERIES_MODIFIER = "s3"
cdef str SERIES_COMMODITY = "s4"
cdef str SERIES_EXPIRY = "s5"
cdef str SERIES_STRIKE = "s6"

# series identifiers (s0 through s4)
# ose
cdef str BIG_DJIA_OUTRIGHT = "96,1,11,0,1"
cdef str BIG_NKVI_OUTRIGHT = "96,2,11,0,1"
cdef str BIG_JGB_OUTRIGHT = "96,29,11,0,27"
cdef str BIG_JGB_SPREAD_3MO = "96,29,151,0,27"
cdef str MINI_JGB_OUTRIGHT = "96,29,11,0,28"
cdef str BIG_TOPIX_OUTRIGHT = "96,22,11,0,30"
cdef str BIG_TOPIX_SPREAD_3MO = "96,22,101,0,30"
cdef str MINI_TOPIX_OUTRIGHT = "96,22,11,0,31"
cdef str BIG_NIKKEI_OUTRIGHT = "96,30,11,0,50"
cdef str BIG_NIKKEI_SPREAD_3MO = "96,30,101,0,50"
cdef str BIG_NIKKEI_SPREAD_6MO = "96,30,102,0,50"
cdef str BIG_NIKKEI_SPREAD_9MO = "96,30,103,0,50"
cdef str BIG_NIKKEI_SPREAD_12MO = "96,30,104,0,50"
cdef str MINI_NIKKEI_OUTRIGHT = "96,30,11,0,40"
cdef str MINI_NIKKEI_SPREAD_1MO = "96,30,101,0,40"
cdef str MINI_NIKKEI_SPREAD_2MO = "96,30,102,0,40"
cdef str MINI_NIKKEI_SPREAD_3MO = "96,30,103,0,40"
cdef str MINI_NIKKEI_SPREAD_4MO = "96,30,104,0,40"
cdef str BIG_NIKKEI400_OUTRIGHT = "96,22,11,0,39"
# sgx
cdef str SGX_NIKKEI_OUTRIGHT = "83,153,50,0,202"


# omniframe stuff
cdef long OMNI_END_BYTES = 6
cdef int BD1_HEADER_BYTES = 44+6
cdef int BD1_PAR_BYTES = 24
cdef int BD1_MAXSIZE = 51 # dealsrc == 1
cdef long BO2_MSGSIZE = 62

cdef long larger(long x, long y) nogil:
    if x > y:
        return x
    else:
        return y

cdef struct AssumedBD1:
    int participants
    int msgs

@cython.boundscheck(False)
cdef AssumedBD1 bd1_guess(long omnilen, long throttle_participants=0) nogil:
    cdef:
        long size = omnilen - OMNI_END_BYTES
        long i = 0, maxiter = 1 + size / (BD1_HEADER_BYTES + 2 * BD1_PAR_BYTES)
        AssumedBD1 assumed
    #for i in range(1, maxiter):
    if maxiter > 1:
        for i in range(2, maxiter):
            if (size - i * BD1_HEADER_BYTES) % BD1_PAR_BYTES == 0 and (size - i * BD1_HEADER_BYTES) / (i * BD1_PAR_BYTES) <= BD1_MAXSIZE:
                assumed.msgs = i
                #assumed.participants = (size - i * BD1_HEADER_BYTES) / BD1_PAR_BYTES - i
                assumed.participants = larger(0, (size - i * BD1_HEADER_BYTES) / BD1_PAR_BYTES - i - throttle_participants)
                return assumed
    assumed.msgs = -1
    assumed.participants = -1
    return assumed

@cython.boundscheck(False)
@cython.wraparound(False)
cdef void fast_bd1_guesses(long tlen, long[:] omniframes, long[:] omnilens, long[:] expiries, 
                           long[:] sides, long[:] tradeqtys, long[:] numitems, long[:] dealsrcs, long[:, :] results,
                           long throttle_qty) nogil:
    cdef:
        long i = 0, omniframe = 0, ignore_omniframe = 0, expiry = 0, side = 0
        long participants = 0, real_tradeqty = 0, my_tradeqty = 0, my_prevqty = 0, count = 0
        AssumedBD1 assumed
        bint processing = False
    
    for i in range(tlen):
        if not processing and dealsrcs[i] != DEALSRC_AUTO:
            ignore_omniframe = omniframes[i]
        if omniframes[i] == ignore_omniframe:
            continue
        if omniframes[i] != omniframe:
            ignore_omniframe = 0
            count = 1
            expiry, side = expiries[i], sides[i]
            assumed = bd1_guess(omnilens[i], throttle_qty)
            participants = larger(0, assumed.participants - numitems[i] + 1)
            if participants <= 0:
                ignore_omniframe = omniframes[i]
                continue
            real_tradeqty = tradeqtys[i]
            my_tradeqty = real_tradeqty + participants
            my_prevqty = 0
            processing = True
        else:
            if dealsrcs[i] != DEALSRC_AUTO or expiries[i] != expiry or sides[i] != side:
                real_tradeqty, my_tradeqty = 0, 0
                results[i, 3] = expiry
                results[i, 4] = side
                results[i, 5] = -1 * participants
                expiry, side = expiries[i], sides[i]
                if dealsrcs[i] != DEALSRC_AUTO:
                    ignore_omniframe = omniframes[i]
                    processing = False
                    continue
            participants = larger(0, participants - numitems[i] + 1)
            real_tradeqty += tradeqtys[i]
            my_prevqty = my_tradeqty
            my_tradeqty = real_tradeqty + participants
            if participants == 0:
                count = 0
                processing = False
            else:
                count += 1
        results[i, 0] = expiry #bd1_total
        results[i, 1] = participants
        results[i, 2] = my_tradeqty - my_prevqty
        results[i, 6] = count
        omniframe = omniframes[i]
    return

def bd1_guesses(df, long throttle_qty):
    cdef:
        long tlen = df.shape[0]
        # omniframe information
        long[:] omniframes = df.omniframe.values
        long[:] omnilens = df.omnilen.values
        # contract information
        long[:] expiries = df.expiry.values
        # bd1 information
        long[:] sides = df.side.values
        long[:] tradeqtys = df.tradeqty.values
        long[:] numitems = df.numitems.values
        long[:] dealsrcs = df.dealsrc.values
        
        ndarray[long, ndim=2] results = zeros((tlen, 7), dtype=long)
    
    # optimized for nogil
    fast_bd1_guesses(tlen, omniframes, omnilens, expiries, sides, tradeqtys, numitems, dealsrcs, results, throttle_qty)
    return DataFrame(results, index=df.index, columns=['bd1_expiry', 'bd1_par', 'mytradeqty', 'adj_expiry', 'adj_side', 'adj_qty', 'assumed_idx'])


# arbitrary contract defs
#cdef dict contract_defs = {
#    BIG_DJIA_OUTRIGHT: {
#        "num": 10,
#        "long": "Big DJIA Outright",
#        "short": "djia"
#    },
#    BIG_NKVI_OUTRIGHT: {
#        "num": 20,
#        "long": "Big Nikkei VIX Outright",
#        "short": "nkv"
#    },
#    BIG_JGB_OUTRIGHT: {
#        "num": 290,
#        "long": "Big JGB Outright",
#        "short": "jgbl"
#    },
#    BIG_JGB_SPREAD_3MO: {
#        "num": 291,
#        "long": "Big JGB Spread 3mo",
#        "short": "jgbl-sp3"
#    },
#    MINI_JGB_OUTRIGHT: {
#        "num": 280,
#        "long": "Mini JGB Outright",
#        "short": "jgbs"
#    },
#    BIG_TOPIX_OUTRIGHT: {
#        "num": 220,
#        "long": "Big TOPIX Outright",
#        "short": "tp"
#    },
#    BIG_TOPIX_SPREAD_3MO: {
#        "num": 221,
#        "long": "Big TOPIX Spread 3mo",
#        "short": "tp-sp3"
#    },
#    MINI_TOPIX_OUTRIGHT: {
#        "num": 230,
#        "long": "Mini TOPIX Outright",
#        "short": "mtp"
#    },
#    MINI_NIKKEI_OUTRIGHT: {
#        "num": 310,
#        "long": "Mini Nikkei Outright",
#        "short": "mnk"
#    },
#    MINI_NIKKEI_SPREAD_1MO: {
#        "num": 311,
#        "long": "Mini Nikkei Spread 1mo",
#        "short": "mnk-sp1"
#    },
#    MINI_NIKKEI_SPREAD_2MO: {
#        "num": 312,
#        "long": "Mini Nikkei Spread 2mo",
#        "short": "mnk-sp2"
#    },
#    MINI_NIKKEI_SPREAD_3MO: {
#        "num": 313,
#        "long": "Mini Nikkei Spread 3mo",
#        "short": "mnk-sp3"
#    },
#    MINI_NIKKEI_SPREAD_4MO: {
#        "num": 314,
#        "long": "Mini Nikkei Spread 4mo",
#        "short": "mnk-sp4"
#    },
#    BIG_NIKKEI_OUTRIGHT: {
#        "num": 300,
#        "long": "Big Nikkei Outright",
#        "short": "nk"
#    },
#    BIG_NIKKEI_SPREAD_3MO: {
#        "num": 301,
#        "long": "Big Nikkei Spread 3mo",
#        "short": "nk-sp3"
#    },
#    BIG_NIKKEI_SPREAD_6MO: {
#        "num": 302,
#        "long": "Big Nikkei Spread 6mo",
#        "short": "nk-sp6"
#    },
#    BIG_NIKKEI_SPREAD_9MO: {
#        "num": 303,
#        "long": "Big Nikkei Spread 9mo",
#        "short": "nk-sp9"
#    },
#    BIG_NIKKEI_SPREAD_12MO: {
#        "num": 304,
#        "long": "Big Nikkei Spread 12mo",
#        "short": "nk-sp12"
#    },
#    BIG_NIKKEI400_OUTRIGHT: {
#        "num": 390,
#        "long": "Big Nikkei 400 Outright",
#        "short": "nk4"
#    },
#    # sgx
#    SGX_NIKKEI_OUTRIGHT: {
#        "num": 1530,
#        "long": "SGX Nikkei Outright",
#        "short": "snk"
#    }
#}

cdef dict series_map, contract_defs = {}

def load_series_map():
    global series_map
    pkl_filenames = 'omx_series_map.pkl', 'sgx_series_map.pkl'
    for filename in pkl_filenames:
        # Added 'rb' to make this a byte object rather than str object --ec
        series_map = pickle.load(open(os.path.join(os.path.dirname(__file__), filename), 'rb'))
        #series_map = pickle.load(open(os.path.join(os.path.dirname(__file__), filename), 'r'))
        for k, v in series_map.items():
            contract_defs[k] = { 'num': int(k.replace(',', '')), 'prefix': v }
    return

def what():
    #return series_map
    return contract_defs

msg_int_dict = {
    'MA42': -1,
    'BO2': 0,
    'BD1': 1,
    'BI41': 41
}

passive_int_dict = { # keep reversed to check against orders traded away in bo2s
    'Bid': 2,
    'Offer': 1,
    'Unknown': 0
}

def get_contract_defs():
    return contract_defs

@cython.boundscheck(False)
@cython.wraparound(False)
cdef str stringify(tuple items, str delim):
    cdef:
        long tlen = len(items), i
        str result = ""
    for i in range(tlen):
        result += str(items[i])
        if i < tlen-1:
            result += delim
    return result

def add_contract_labels(df):
    cdef:
        long tlen = df.shape[0], i #, contract_count = 0
        long[:] s0 = df.s0.values
        long[:] s1 = df.s1.values
        long[:] s2 = df.s2.values
        long[:] s3 = df.s3.values
        long[:] s4 = df.s4.values
        long[:] s5 = df.s5.values # expiration date info
        long[:] s6 = df.s6.values # strike
        
        dict contracts = {}
        str key
        
        ndarray[long, ndim=2] results = zeros((tlen, 2), dtype=long)

    for i in range(tlen):
        key = stringify((s0[i], s1[i], s2[i], s3[i], s4[i]), ",") #, s5[i], s6[i]
        if key not in contract_defs:
            results[i, 0] = -1
        else:
            results[i, 0] = contract_defs[key]['num']
        results[i, 1] = find_expiry(s5[i])
        
    df['contract'] = results[:, 0]
    df['expiry'] = results[:, 1]
    df['strike'] = s6
    
    #print('added contract, expiry, and strike')
    return #df

def add_time_labels(df):
    # patch for missing microseconds
    timestr_bool = df.timestr.str.len() < 16
    df.loc[timestr_bool, 'timestr'] = df['timestr'].apply(lambda x: x+'.000000')
    df['time'] = to_datetime(df['timestr'].values, format='%Y%m%dT%H%M%S.%f')
    df['timestamp'] = df['time'].astype(long)
    return

def qwid_ints(df):
    return df['qwid'].apply(lambda x: long(x[1:-1].replace(':', ''), 16)).astype(uint64).astype(int64)

def prepare_raw_data(df):
    df['msg_int'] = df['msg'].apply(lambda x: msg_int_dict[x])
    for col in df.columns:
        if df[col].dtype == float64:
            df[col] = df[col].fillna(0).round().astype(long)
        elif col == 'bd1aggressor':
            df.loc[df['msg_int']!=1, col] = 'Unknown'
    # ma42 handling
    ending_ma42_bool = logical_and(df['msg_int']==-1, df['packet']==-1)
    df['ma42_end'] = 0
    df.loc[ending_ma42_bool, 'ma42_end'] = 1
    df.loc[ending_ma42_bool, 'qwid'] = '[0:0]'
    # add more columns
    df['bd1passive_int'] = df['bd1aggressor'].apply(lambda x: passive_int_dict[x])
    df['qwid_int'] = qwid_ints(df)

    add_time_labels(df)
    add_contract_labels(df)
    # return the DataFrame
    return #df

def add_str_series(df):
    cdef:
        long tlen = df.shape[0], i #, contract_count = 0 
        long[:] s0 = df.s0.values
        long[:] s1 = df.s1.values
        long[:] s2 = df.s2.values
        long[:] s3 = df.s3.values
        long[:] s4 = df.s4.values
        long[:] s5 = df.s5.values # expiration date info
        long[:] s6 = df.s6.values # strike
    
        str key 
    
        list str_series = []

    for i in range(tlen):
        key = '[' + stringify((s0[i], s1[i], s2[i], s3[i], s4[i], s5[i], s6[i]), ",") + ']' 
        str_series.append(key)
    
    df['str_series'] = nparray(str_series)
    
    return 0

cdef long find_long_expiry(long seriesval):
    cdef:
        long year = ((seriesval & YEAR_ANDMASK) >> 9) + START_YEAR
        long month = (seriesval & MONTH_ANDMASK) >> 5
        long day = seriesval & DAY_ANDMASK
    return 10000*year + 100*month + day

def find_expiry(long seriesval, bint string_format=False):
    cdef:
        long expiry = find_long_expiry(seriesval)
        long month, year
    if string_format:
        month = (expiry/100)%100
        year = (expiry/10000)%100
        return get_monthcode(month) + str(year)
    else:
        return expiry

def find_shortname(long s0, long s1, long s2, long s3, long s4, long s5):
    """
    Parameters
    ----------
    s0 : long
        Series value 0
    s1 : long
        Series value 1
    s2 : long
        Series value 2
    s3 : long
        Series value 3
    s4 : long
        Series value 4
    s5 : long
        Series value 5

    Returns
    -------
    shortname : string (e.g. NKH15)

    """
    cdef str key = stringify((s0, s1, s2, s3, s4), ",")
    if key in contract_defs:
        return contract_defs[key]["short"].upper() + find_expiry(s5, True)
    else:
        return "DK" + find_expiry(s5, True)

def gateway_winners(df):
    cdef:
        long[:] timestamps = df.timestamp.values
        long[:] gateways = df.gateway_int.values
        long[:] seqnums = df.seqnum.values
        long[:] counters = df.bd1counter.values
        long[:] msgs = df.msg_int.values
        long[:] contracts = df.expiry.values
        long tlen = df.shape[0], i, msg, seqnum, counter, contract, gateway
        
        #dict bo2_counter = {}, bd1_counter = {}
        dict msg_counter = {}
        dict msg_wintime = {}
        dict msg_accounting = {}
        tuple key
        
        ndarray[long, ndim=2] results = zeros((tlen, 2), dtype=long)
        
    for i in range(tlen):
        contract = contracts[i]
        msg = msgs[i]
        seqnum = seqnums[i]
        counter = counters[i]
        gateway = gateways[i]
        if msg == 0:
            key = (contract, msg, seqnum)
        elif msg == 1:
            key = (contract, msg, seqnum, counter)
        else:
            continue
        
        if key not in msg_counter:
            msg_counter[key] = -1
            msg_wintime[key] = timestamps[i]
            msg_accounting[key] = set()
            
        # only record if not in msg_accounting
        if gateway not in msg_accounting[key]:
            msg_counter[key] += 1
            results[i, 0] = msg_counter[key]
            results[i, 1] = timestamps[i] - msg_wintime[key]
        
        # sucks but must be done here
        msg_accounting[key].add(gateway)
        
    # clean up
    #del msg_counter
    #del msg_wintime
    #del msg_accounting
        
    return results


cdef class OMXMDHandler:
    cdef:
        BBO bbo
        dict unresolved, tradeqtys, qwid_tradeqtys, qwid_tradeprices, assumed_accounting
        int levels
        
    def __cinit__(self, int levels):
        cdef int i
        self.bbo = BBO.__new__(BBO, levels)
        self.unresolved = {}
        self.tradeqtys = {}
        self.qwid_tradeqtys = {}
        self.qwid_tradeprices = {}
        self.assumed_accounting = {}
        for i in range(2):
            self.unresolved[i+1] = {}
            self.tradeqtys[i+1] = {}
            self.qwid_tradeqtys[i+1] = {}
            self.qwid_tradeprices[i+1] = {}
            self.assumed_accounting[i+1] = {}
        self.levels = levels
        
    cdef long clear_book(self):
        cdef int i = 0
        for i in range(2):
            self.unresolved[i+1].clear()
            self.tradeqtys[i+1].clear()
            self.assumed_accounting[i+1].clear()
        self.bbo.clear_book()

    cdef long update_book(self, long side, long price, long qty):
        cdef long newqty = self.bbo.update_book_by(side, price, qty)
        if newqty < 0:
            self.add_to_unresolved(side, price, newqty)
    
    cdef void update_book_nocheck(self, long side, long price, long qty):
        cdef long newqty = self.bbo.update_book_by_nocheck(side, price, qty)
        if newqty < 0:
            self.add_to_unresolved(side, price, newqty)
    
    cdef long reconcile_bo2_trade(self, long side, long price, long qtydiff):
        cdef:
            dict tradeqtys = self.tradeqtys[side]
            long tradeqty = 0
        if price not in tradeqtys or (price in tradeqtys and tradeqtys[price] <= 0):
            tradeqty = self.modify_book_from_trade(side, price, -1 * qtydiff)
        self.reconcile_trade_info(side, price, qtydiff)
        return tradeqty
    
    cdef long reconcile_bd1_trade(self, long side, long price, long itemqty):
        cdef:
            long tradeqty = 0, prev_tradeqty = 0
            dict tradeqtys = self.tradeqtys[side]
        if price not in tradeqtys or (price in tradeqtys and tradeqtys[price] >= 0):
            tradeqty = self.modify_book_from_trade(side, price, itemqty)
        prev_tradeqty = self.reconcile_trade_info(side, price, itemqty)
        if prev_tradeqty < 0 and tradeqtys[price] > 0:
            tradeqty = self.modify_book_from_trade(side, price, tradeqtys[price])
        return tradeqty
    
    cdef void reconcile_bd1_trades_post_auction(self, side):
        cdef dict tradeqtys = self.tradeqtys[side]
        for price in tradeqtys:
            self.update_book(side, price, -1 * tradeqtys[price])
    
    cdef long modify_book_from_trade(self, long side, long price, long tradeqty):
        self.update_book(side, price, -1 * tradeqty)
        return tradeqty
    
    cdef void add_to_unresolved(self, long side, long price, long neg_qty):
        cdef dict unresolved = self.unresolved[side]
        if price not in unresolved:
            unresolved[price] = neg_qty
        else:
            unresolved[price] += neg_qty
    
    cdef void update_unresolved(self, long side, long price, long qty):
        cdef:
            dict unresolved = self.unresolved[side]
            long qtychange
        if price not in unresolved:
            self.update_book(side, price, qty)
        else:
            qtychange = unresolved[price] + qty
            if qtychange > 0:
                self.update_book(side, price, qtychange)
                del unresolved[price]
            elif qtychange == 0:
                del unresolved[price]
            else:
                unresolved[price] = qtychange
    
    cdef void add_bo2(self, long side, long price, long qtydiff):
        if self.bbo.has(side, price):
            self.update_book_nocheck(side, price, qtydiff)
        else:
            self.update_unresolved(side, price, qtydiff)
            
    cdef void delete_bo2(self, long side, long price, long qtydiff):
        self.update_book(side, price, qtydiff)
            
    cdef long reconcile_trade_info(self, long side, long price, long qty):
        cdef:
            long prev_tradeqty = 0
            dict tradeqtys = self.tradeqtys[side]
        if price not in tradeqtys:
            tradeqtys[price] = qty
        else:
            prev_tradeqty = tradeqtys[price]
            tradeqtys[price] += qty
        return prev_tradeqty
    
    cdef void reconcile_auction_trade_info(self, long qwid, long side, long price, long qty, bint purge_zeros):
        cdef dict qwid_tradeqtys = self.qwid_tradeqtys[side], qwid_tradeprices = self.qwid_tradeprices[side]
        if qwid not in qwid_tradeqtys:
            qwid_tradeqtys[qwid] = qty
            qwid_tradeprices[qwid] = price
        else:
            qwid_tradeqtys[qwid] += qty
            if purge_zeros and qwid_tradeqtys[qwid] == 0:
                del qwid_tradeqtys[qwid]
                del qwid_tradeprices[qwid]
    
    cdef bint is_bo2_auction_settled(self):
        if len(self.qwid_tradeqtys[1]) == 0 and len(self.qwid_tradeqtys[2]) == 0:
            #self.qwid_tradeqtys.clear()
            #self.qwid_tradeprices.clear()
            return True
        else:
            return False
    
    cdef tuple settle_auction(self, bint use_bo2s=True):
        cdef:
            dict qwid_tradeqtys = self.qwid_tradeqtys, qwid_tradeprices = self.qwid_tradeprices
            list remove_qwids = []
            long side, qwid, price, qty, tradesum = 0, lastbuyprice = 0, lastsellprice = 0, buyvolume = 0, sellvolume = 0
        for side in qwid_tradeqtys:
            for qwid in qwid_tradeqtys[side]:
                price, qty = qwid_tradeprices[side][qwid], qwid_tradeqtys[side][qwid]
                if qty < 0:
                    if use_bo2s:
                        self.reconcile_trade_info(side, price, qty)
                        tradesum -= qty
                        if side == 1:
                            lastsellprice = price
                            sellvolume -= qty
                        else:
                            lastbuyprice = price
                            buyvolume -= qty
                    else:
                        self.add_bo2(side, price, -1 * qty)
                        print('adding back into the book', qwid, side, price, qty)
                    remove_qwids.append((side, qwid))
                elif qty == 0:
                    remove_qwids.append((side, qwid))
        for side, qwid in remove_qwids:
            del qwid_tradeqtys[side][qwid]
            del qwid_tradeprices[side][qwid]
        return tradesum, lastbuyprice, buyvolume, lastsellprice, sellvolume
    
    cdef bint is_crossed(self):
        return self.bbo.is_crossed()

    cdef void write_output_buysell(self, long[:] output, long buyprice, long buyvolume, long sellprice, long sellvolume, long idx, bint use_crossed):
        cdef:
            int i, n = self.levels
            int askidx = 2*n, tradeidx = 4*n+1
            long bid, ask #, bidqty, askqty
            BBO bbo = self.bbo
        for i in range(n):
            bid, ask = bbo.bidprices[i], bbo.askprices[i]
            output[2*i + 1] = bid
            output[2*i + 2] = bbo.at(1, bid)
            output[2*i + 1 + askidx] = ask
            output[2*i + 2 + askidx] = bbo.at(2, ask)
        if buyvolume > 0 or sellvolume > 0:
            output[tradeidx] = buyprice
            output[tradeidx + 1] = buyvolume
            output[tradeidx + 2] = sellprice
            output[tradeidx + 3] = sellvolume
        if use_crossed or not self.is_crossed():
            output[0] = idx

    cdef void write_output(self, long[:] output, long tradeside, long tradeprice, long tradeqty, long idx, bint use_crossed):
        if tradeside == 0:
            self.write_output_buysell(output, 0, 0, 0, 0, idx, use_crossed)
        elif tradeside == 1:
            self.write_output_buysell(output, 0, 0, tradeprice, tradeqty, idx, use_crossed)
        elif tradeside == 2:
            self.write_output_buysell(output, tradeprice, tradeqty, 0, 0, idx, use_crossed)

    cdef void show_info(self):
        print('tradeqtys')
        print([(prc, qty) for prc, qty in self.tradeqtys[1].items() if qty != 0])
        print([(prc, qty) for prc, qty in self.tradeqtys[2].items() if qty != 0])
        print('unresolved')
        print([(prc, qty) for prc, qty in self.unresolved[1].items() if qty != 0])
        print([(prc, qty) for prc, qty in self.unresolved[2].items() if qty != 0])

    cdef tuple settle_auction_with_trade(self, long side, long price, long itemqty, bint use_bo2s=True):
        cdef:
            long tradesum, lastbuyprice, lastsellprice, buyvolume, sellvolume
            long tradeqty
        tradesum, lastbuyprice, lastsellprice, buyvolume, sellvolume = self.settle_auction(use_bo2s)
        tradeqty = self.reconcile_bd1_trade(side, price, itemqty)
        if tradeqty > 0:
            if side == 1:
                lastsellprice = price
                sellvolume += tradeqty
            elif side == 2:
                lastbuyprice = price
                buyvolume += tradeqty
        return tradesum, lastbuyprice, buyvolume, lastsellprice, sellvolume

    cdef list reconcile_bd1_assumed_trade(self, long side, long price, long tradeqty, long itemqty):
        cdef:
            list tradeqtys = self.bbo.update_assumed_tradeqty(side, price, tradeqty, itemqty)
            long dummyprice, dummyqty
        dummyprice, dummyqty = tradeqtys.pop()
        if dummyqty < 0:
            #print('well look what we have here', dummyqty, price, tradeqty, itemqty)
            self.add_to_unresolved(side, price, -1 * tradeqty)
        return tradeqtys
    
    cdef void update_assumed_accounting(self, long side, long tradeprice, long tradeqty, bint is_assumed):
        cdef:
            long direction = -1 if is_assumed else 1
            dict assumed_accounting = self.assumed_accounting[side]
        if tradeprice not in assumed_accounting:
            assumed_accounting[tradeprice] = direction * tradeqty
        else:
            assumed_accounting[tradeprice] += direction * tradeqty
    
    cdef void clear_assumed_accounting(self):
        cdef:
            long side
            dict assumed_accounting = self.assumed_accounting
        for side in assumed_accounting:
            assumed_accounting[side].clear()
            
    cdef bint assumed_accounting_is_empty(self):
        return not (self.assumed_accounting[1] or self.assumed_accounting[2])
            
    cdef tuple check_assumed_accounting(self):
        cdef:
            long side, tradeprice, tradeqty
            dict assumed_accounting = self.assumed_accounting
        for side in assumed_accounting:
            for tradeprice, tradeqty in assumed_accounting[side].items():
                if tradeqty != 0:
                    return False, side, tradeprice
        self.clear_assumed_accounting()
        return True, 0, 0
    
    # this will take over check_assumed_accounting
    cdef tuple resolve_assumed_accounting(self):
        cdef:
            long side, itemprice, itemqty, tradeside = 0, tradeprice = 0, tradeqty = 0
            long rbuyprice = 0, rbuyvolume = 0, rsellprice = 0, rsellvolume = 0
            dict assumed_accounting = self.assumed_accounting
        for side in assumed_accounting:
            for itemprice, itemqty in assumed_accounting[side].items():
                if itemqty > 0:
                    self.modify_book_from_trade(side, itemprice, itemqty)
                    tradeside, tradeprice, tradeqty = side, itemprice, itemqty
                elif itemqty < 0:
                    self.add_bo2(side, itemprice, -1 * itemqty)
                    if side == 2:
                        rbuyprice = itemprice
                        rbuyvolume += itemqty
                    else:
                        rsellprice = itemprice
                        rsellvolume += itemqty
        self.clear_assumed_accounting()
        return tradeside, tradeprice, tradeqty, rbuyprice, rbuyvolume, rsellprice, rsellvolume
    
    cdef tuple reverse_bd1_assumption(self, long rside, list tradeqtys, long reversed_tradeqty):
        cdef:
            long rprice, rqty, passive_side = 2 if rside == 1 else 1, itemprice, itemqty, rvolume = 0
            list reversal
        rprice, rqty, reversal = self.bbo.reverse_assumed_tradeqty(rside, tradeqtys, reversed_tradeqty)
        if rqty == -1:
            print('tradeqtys is empty? interesting...')
            return 0, -1, 0, -1
        elif rqty == -2:
            print('well something is fucked... reversed_tradeqty was not used up')
            return 0, -2, 0, -2
        while reversal:
            itemprice, itemqty = reversal.pop()
            self.add_bo2(passive_side, itemprice, itemqty)
            self.update_assumed_accounting(passive_side, itemprice, itemqty, False)
        #self.add_bo2(passive_side, rprice, rqty)
        #self.update_assumed_accounting(passive_side, rprice, rqty, False)
        if rside == 1:
            return rprice, rqty, 0, 0, reversal
        else:
            return 0, 0, rprice, rqty, reversal
        
    cdef void write_output_adjusted(self, long[:] output, long tradeside, long tradeprice, long tradeqty, long idx, bint use_crossed, 
                                    long adj_buyprice, long adj_buyvolume, long adj_sellprice, long adj_sellvolume):
        cdef adj_idx = 4*self.levels + 5
        self.write_output(output, tradeside, tradeprice, tradeqty, idx, use_crossed)
        output[adj_idx] = adj_buyprice
        output[adj_idx + 1] = adj_buyvolume
        output[adj_idx + 2] = adj_sellprice
        output[adj_idx + 3] = adj_sellvolume
    

@cython.wraparound(False)
@cython.boundscheck(False)
cdef list ma42_cleanup(long tlen, long[:] times, long[:] msgs, long[:] seqnums, long[:] ma42_end):
    cdef:
        long i = 0, j = 0, ma42_finished = 0, ma42_seqnum = 0        
        list queued_bo2s = [], results = []
        
    for i in range(tlen):
        if msgs[i] == -1:
            ma42_finished = ma42_end[i]
            if ma42_finished:
                #print('okay we are finished for now', i, ma42_seqnum, queued_bo2s)
                while queued_bo2s:
                    j = queued_bo2s.pop()
                    if times[j] <= times[i] and seqnums[j] > ma42_seqnum:
                        results.append(j)
            else:
                ma42_seqnum = seqnums[i]
        elif msgs[i] == 0 and not ma42_finished:
            queued_bo2s.append(i)
            
    #results.sort()
    return results
            
@cython.boundscheck(False)
def book_building(df, str exch, long bd1_expiry, bint bo2_only=True, bint debug_mode=False):
    cdef:
        long tlen = df.shape[0], i, j, count = 0
        long qty = 0, price = 0, side = 0, tradeqty = 0, buyvolume = 0, sellvolume = 0, lastbuyprice = 0, lastsellprice = 0
        long ma42_finished = 0
        
        unsigned long qwid
        
        long[:] times = df.timestamp.values
        long[:] msgs = df.msg_int.values
        unsigned long[:] qwids = df.qwid_int.astype(uint64).values
        long[:] sides = df.side.values
        long[:] prices = df.price.values
        long[:] seqnums = df.seqnum.values
        long[:] expiries = df.expiry.values
        long[:] df_index = df.index.values

        # bd1 arrays
        long[:] itemnumbers = df.itemnumber.values
        long[:] dealsrcs = df.dealsrc.values
        long[:] itemqtys = df.itemqty.values
        long[:] bd1passives = df.bd1passive_int.values

        # bo2 arrays
        long[:] qtydiffs = df.qtydiff.values
        long[:] reasons = df.reason.values
        long[:] ma42_end = df.ma42_end.values

        # for checking shit
        str[:] timestrs = df.timestr.values

        bint market_open = False, bo2_auction_settled = False
        bint is_ose = exch == 'jpx', is_sgx = exch == 'sgx'
        
        int levels = 10
        OMXMDHandler md_handler = OMXMDHandler.__new__(OMXMDHandler, levels)

        ndarray[long, ndim=2] output = zeros((tlen, levels * 4 + 5), dtype=long)
        list tradecol = ['buyprice', 'buyvolume', 'sellprice', 'sellvolume']
        list newcol = [ba + pq + str(i) for ba in ('bid', 'ask') for i in range(levels) for pq in ('prc', 'qty')] + tradecol

        long mytradesum = 0, bd1sum = 0
        
        #list ma42_cleanup_idx = ma42_cleanup(tlen, times, msgs, seqnums, ma42_end)
            
    if not is_ose and not is_sgx:
        print(exch, 'not recognized as valid omx exchange. quitting')
        return 0

    #if ma42_cleanup_idx:
    #    print('need to clean up following messages between ma42s', ma42_cleanup_idx)
    #    print('cannot handle now. quitting')
    #    return 0
    
    for i in range(tlen):
        tradeqty = 0
        
        # An ma42 arrived
        if msgs[i] == -1:
            if not ma42_end[i] and ma42_finished:
                md_handler.clear_book()
            md_handler.update_book(sides[i], prices[i], qtydiffs[i])
            ma42_finished = ma42_end[i]
            if not ma42_finished:
                md_handler.write_output(output[i], 0, 0, 0, df_index[i], False)
        # A bo2 arrived
        if msgs[i] == 0: # and (ma42_finished):
            if reasons[i] == REASON_DEAL:
                if not (market_open and bo2_auction_settled):
                    qwid, side, price, qty = qwids[i], sides[i], prices[i], qtydiffs[i]
                    md_handler.reconcile_auction_trade_info(qwid, side, price, qty, True)
                    md_handler.update_book_nocheck(side, price, qty)
                    bo2_auction_settled = md_handler.is_bo2_auction_settled()
                    #if bo2_auction_settled:
                    md_handler.write_output(output[i], 0, 0, 0, df_index[i], False)
                    md_handler.reconcile_bd1_trades_post_auction(1)
                    md_handler.reconcile_bd1_trades_post_auction(2)
                else:
                    side, price, qty = sides[i], prices[i], qtydiffs[i]
                    tradeqty = md_handler.reconcile_bo2_trade(side, price, qty)
                    md_handler.write_output(output[i], side, price, tradeqty, df_index[i], False)
                    mytradesum += tradeqty
                    count += 1
            elif qtydiffs[i] > 0:    
                md_handler.add_bo2(sides[i], prices[i], qtydiffs[i])
                if market_open:
                    md_handler.write_output(output[i], 0, 0, 0, df_index[i], False)
                    count += 1
            elif qtydiffs[i] < 0:
                if not market_open:
                    md_handler.update_book_nocheck(sides[i], prices[i], qtydiffs[i])
                else:
                    side, price, qty = sides[i], prices[i], qtydiffs[i]
                    md_handler.delete_bo2(side, price, qty)
                    md_handler.write_output(output[i], 0, 0, 0, df_index[i], False)
                    count += 1
        # A bd1 arrived
        elif msgs[i] == 1:
            if not market_open and expiries[i] == bd1_expiry:
                if dealsrcs[i] == DEALSRC_OPEN_ALLOCATION:
                    md_handler.reconcile_auction_trade_info(qwids[i], sides[i], prices[i], itemqtys[i], False)
                elif dealsrcs[i] == DEALSRC_AUTO:
                    if (is_ose and itemnumbers[i] == 1) or (is_sgx and itemnumbers[i]%2 == 1):
                        side, price, qty = bd1passives[i], prices[i], itemqtys[i]
                        mytradesum, lastbuyprice, buyvolume, lastsellprice, sellvolume = md_handler.settle_auction_with_trade(side, price, qty, True)
                    else:
                        mytradesum, lastbuyprice, buyvolume, lastsellprice, sellvolume = md_handler.settle_auction(True)
                    #mytradesum, lastbuyprice, buyvolume, lastsellprice, sellvolume = md_handler.settle_auction()
                    market_open = True
                    bo2_auction_settled = md_handler.is_bo2_auction_settled()
                    if debug_mode:
                        print('debug mode', 'bo2_auction_settled', bo2_auction_settled)
                        print('debug mode', md_handler.qwid_tradeqtys)
                    #if bo2_auction_settled:
                    md_handler.write_output_buysell(output[i], lastbuyprice, buyvolume, lastsellprice, sellvolume, df_index[i], False)
            elif not bo2_only and dealsrcs[i] == DEALSRC_AUTO and ((is_ose and itemnumbers[i] == 1) or (is_sgx and itemnumbers[i]%2 == 1)):
                if expiries[i] == bd1_expiry:
                    side, price, qty = bd1passives[i], prices[i], itemqtys[i]
                    tradeqty = md_handler.reconcile_bd1_trade(side, price, qty)
                    mytradesum += tradeqty
                    bd1sum += itemqtys[i]
                    md_handler.write_output(output[i], side, price, tradeqty, df_index[i], False)
                    count += 1

    bookdf = DataFrame(output[output[:, 0]!=0, 1:], index=output[output[:, 0]!=0, 0], columns=newcol)
    bookdf.insert(0, 'timestr', df.loc[bookdf.index, 'timestr'])
    bookdf.insert(1, 'timestamp', df.loc[bookdf.index, 'timestamp'])
    bookdf.insert(2, 'omniframe', df.loc[bookdf.index, 'omniframe'])
    bookdf.insert(3, 'omnilen', df.loc[bookdf.index, 'omnilen'])
    #bookdf.insert(3, 'msg', df.loc[bookdf.index, 'msg'])
    bookdf.insert(4, 'msg_int', df.loc[bookdf.index, 'msg_int'])
    bookdf.insert(5, 'seqnum', df.loc[bookdf.index, 'seqnum'])
    #bookdf.insert(6, 'bd1segment', df.loc[bookdf.index, 'bd1segment'])
    #bookdf.insert(7, 'bd1counter', df.loc[bookdf.index, 'bd1counter'])
    #bookdf.insert(8, 'bd1numitems', df.loc[bookdf.index, 'numitems'])
    bookdf.insert(6, 'bo2reason', df.loc[bookdf.index, 'reason'])
    bookdf.insert(7, 'bo2side', df.loc[bookdf.index, 'side'])
    bookdf.insert(8, 'bo2price', df.loc[bookdf.index, 'price'])
    bookdf.insert(9, 'qwid_int', df.loc[bookdf.index, 'qwid_int'])
    bookdf.loc[bookdf['msg_int']==1, 'bo2side'] = 0

    #print('finished', mytradesum, bd1sum)
    if debug_mode:
        md_handler.show_info()
        print('bo2 only?', bo2_only)
    return bookdf #DataFrame(output[output[:, 0]!=0, 1:], index=output[output[:, 0]!=0, 0], columns=newcol)

def book_building_assumed(df, str exch, long bd1_expiry, bint debug_mode=False):
    cdef:
        long tlen = df.shape[0], i, j, count = 0
        long qty = 0, price = 0, side = 0, buyvolume = 0, sellvolume = 0, lastbuyprice = 0, lastsellprice = 0
        long tradeqty = 0, assumed_qty = 0, tradeprice = 0, tradeside = 0, rside, passive_side, rprice, rqty, adj_expiry, last_assumed_idx = 0
        long rbuyprice, rbuyvolume, rsellprice, rsellvolume, rbuyprice_add, rbuyvolume_add, rsellprice_add, rsellvolume_add
        long ma42_finished = 0
        
        unsigned long qwid
        
        long[:] times = df.timestamp.values
        long[:] msgs = df.msg_int.values
        unsigned long[:] qwids = df.qwid_int.values
        long[:] sides = df.side.values
        long[:] prices = df.price.values
        long[:] seqnums = df.seqnum.values
        long[:] expiries = df.expiry.values
        long[:] df_index = df.index.values

        # bd1 arrays
        long[:] itemnumbers = df.itemnumber.values
        long[:] dealsrcs = df.dealsrc.values
        long[:] itemqtys = df.itemqty.values
        long[:] bd1passives = df.bd1passive_int.values
        
        # bd1 assumption arrays
        long[:, :] bd1_assumed = df.loc[:, ('bd1_expiry', 'bd1_par', 'mytradeqty', 'adj_expiry', 'adj_side', 'adj_qty', 'assumed_idx')].values

        # bo2 arrays
        long[:] qtydiffs = df.qtydiff.values
        long[:] reasons = df.reason.values
        long[:] ma42_end = df.ma42_end.values

        # for checking shit
        str[:] timestrs = df.timestr.values

        bint market_open = False, bo2_auction_settled = False
        bint is_ose = exch == 'jpx', is_sgx = exch == 'sgx'
        
        int levels = 10
        OMXMDHandler md_handler = OMXMDHandler.__new__(OMXMDHandler, levels)

        ndarray[long, ndim=2] output = zeros((tlen, levels * 4 + 9), dtype=long)
        list tradecol =  ['buyprice', 'buyvolume', 'sellprice', 'sellvolume', 'rbuyprice', 'rbuyvolume', 'rsellprice', 'rsellvolume']
        list newcol = [ba + pq + str(i) for ba in ('bid', 'ask') for i in range(levels) for pq in ('prc', 'qty')] + tradecol

        long mytradesum = 0, bd1sum = 0
        
        list ma42_cleanup_idx, assumed_tradeqtys, last_assumed = [], reversal_trades
        
        bint assumed_accounting_passed = True
        
    if not is_ose and not is_sgx:
        print(exch, 'not recognized as valid omx exchange. quitting')
        return 0
    
    ma42_cleanup_idx = ma42_cleanup(tlen, times, msgs, seqnums, ma42_end)
    if ma42_cleanup_idx:
        print('need to clean up following messages between ma42s', ma42_cleanup_idx)
        print('cannot handle now. quitting')
        return 0
    
    for i in range(tlen):
        tradeqty = 0
        
        # An ma42 arrived
        if msgs[i] == -1:
            if not ma42_end[i] and ma42_finished:
                md_handler.clear_book()
            md_handler.update_book(sides[i], prices[i], qtydiffs[i])
            ma42_finished = ma42_end[i]
            if not ma42_finished:
                md_handler.write_output(output[i], 0, 0, 0, df_index[i], False)
        # A bo2 arrived
        elif msgs[i] == 0:
            if reasons[i] == REASON_DEAL:
                if not (market_open and bo2_auction_settled):
                    qwid, side, price, qty = qwids[i], sides[i], prices[i], qtydiffs[i]
                    md_handler.reconcile_auction_trade_info(qwid, side, price, qty, True)
                    md_handler.update_book_nocheck(side, price, qty)
                    bo2_auction_settled = md_handler.is_bo2_auction_settled()
                    md_handler.write_output(output[i], 0, 0, 0, df_index[i], False)
                    md_handler.reconcile_bd1_trades_post_auction(1)
                    md_handler.reconcile_bd1_trades_post_auction(2)
                else:
                    continue
            elif qtydiffs[i] > 0:    
                md_handler.add_bo2(sides[i], prices[i], qtydiffs[i])
                if market_open:
                    md_handler.write_output(output[i], 0, 0, 0, df_index[i], True)
                    count += 1
            elif qtydiffs[i] < 0:
                if not market_open:
                    md_handler.update_book_nocheck(sides[i], prices[i], qtydiffs[i])
                else:
                    side, price, qty = sides[i], prices[i], qtydiffs[i]
                    md_handler.delete_bo2(side, price, qty)
                    md_handler.write_output(output[i], 0, 0, 0, df_index[i], True)
                    count += 1
        # A bd1 arrived
        elif msgs[i] == 1:
            side, price, qty, adj_expiry, rexpiry = bd1passives[i], prices[i], itemqtys[i], bd1_assumed[i, 0], bd1_assumed[i, 3]
            if not market_open and expiries[i] == bd1_expiry:
                if dealsrcs[i] == DEALSRC_OPEN_ALLOCATION:
                    md_handler.reconcile_auction_trade_info(qwids[i], sides[i], price, qty, False)
                elif dealsrcs[i] == DEALSRC_AUTO:
                    if (is_ose and itemnumbers[i] == 1) or (is_sgx and itemnumbers[i]%2 == 1):
                        mytradesum, lastbuyprice, buyvolume, lastsellprice, sellvolume = md_handler.settle_auction_with_trade(side, price, qty, False)
                    else:
                        mytradesum, lastbuyprice, buyvolume, lastsellprice, sellvolume = md_handler.settle_auction(False)
                    market_open = True
                    bo2_auction_settled = md_handler.is_bo2_auction_settled()
                    md_handler.write_output_buysell(output[i], lastbuyprice, buyvolume, lastsellprice, sellvolume, df_index[i], True)
            elif adj_expiry > 0 or rexpiry > 0:
                # nasty code to handle assumed book fills
                rqty, last_assumed_idx = bd1_assumed[i, 5], bd1_assumed[i, 6]
                rbuyprice, rbuyvolume, rsellprice, rsellvolume = 0, 0, 0, 0
                if adj_expiry == bd1_expiry:
                    md_handler.update_assumed_accounting(side, price, qty, False)
                    assumed_qty = bd1_assumed[i, 2]
                    assumed_tradeqtys = md_handler.reconcile_bd1_assumed_trade(side, price, assumed_qty, expiries[i])
                    for tradeprice, tradeqty in assumed_tradeqtys:
                        md_handler.update_assumed_accounting(side, tradeprice, tradeqty, True)
                        bd1sum += tradeqty
                    #if len(assumed_tradeqtys) > 1:
                    #    print(timestrs[i], seqnums[i], 'BOOK SWEEP', side, tradeprice, tradeqty, assumed_tradeqtys)
                    md_handler.write_output(output[i], side, tradeprice, tradeqty, df_index[i], True)
                    last_assumed.extend(assumed_tradeqtys)
                else:
                    tradeprice, tradeqty = 0, 0
                # now handle adjustments
                if rexpiry == bd1_expiry and rqty != 0:
                    if not last_assumed:
                        print(timestrs[i], 'SOMETHING SERIOUSLY WRONG FOLKS! BREAK')
                        break
                    rside = bd1_assumed[i, 4]
                    rbuyprice, rbuyvolume, rsellprice, rsellvolume, reversal_trades = md_handler.reverse_bd1_assumption(rside, last_assumed, rqty)
                    if rbuyvolume < 0 or rsellvolume < 0:
                        print(timestrs[i], 'INCAPABLE OF REVERSING? BREAK')
                        break
                    rbuyvolume *= -1
                    rsellvolume *= -1
                    md_handler.write_output_adjusted(output[i], side, tradeprice, tradeqty, df_index[i], True, rbuyprice, rbuyvolume, rsellprice, rsellvolume)
                # cleanup
                if last_assumed_idx == 0 and not md_handler.assumed_accounting_is_empty():
                    tradeside, tradeprice, tradeqty, rbuyprice_add, rbuyvolume_add, rsellprice_add, rsellvolume_add = md_handler.resolve_assumed_accounting()
                    if rbuyvolume_add != 0:
                        rbuyvolume += rbuyvolume_add
                        rbuyprice = rbuyprice_add
                    if rsellvolume_add != 0:
                        rsellvolume += rsellvolume_add
                        rsellprice = rsellprice_add
                    last_assumed = []
                    md_handler.write_output_adjusted(output[i], tradeside, tradeprice, tradeqty, df_index[i], True, rbuyprice, rbuyvolume, rsellprice, rsellvolume)
            elif expiries[i] == bd1_expiry and dealsrcs[i] == DEALSRC_AUTO and ((is_ose and itemnumbers[i] == 1) or (is_sgx and itemnumbers[i]%2 == 1)):
                tradeqty = md_handler.reconcile_bd1_trade(side, price, qty)
                mytradesum += tradeqty
                bd1sum += itemqtys[i]
                md_handler.write_output(output[i], side, price, tradeqty, df_index[i], True)
                count += 1
 
    bookdf = DataFrame(output[output[:, 0]!=0, 1:], index=output[output[:, 0]!=0, 0], columns=newcol)
    bookdf.insert(0, 'timestr', df.loc[bookdf.index, 'timestr'])
    bookdf.insert(1, 'omniframe', df.loc[bookdf.index, 'omniframe'])
    bookdf.insert(2, 'omnilen', df.loc[bookdf.index, 'omnilen'])
    bookdf.insert(3, 'msg', df.loc[bookdf.index, 'msg'])
    bookdf.insert(4, 'msg_int', df.loc[bookdf.index, 'msg_int'])
    bookdf.insert(5, 'seqnum', df.loc[bookdf.index, 'seqnum'])
    bookdf.insert(6, 'bd1segment', df.loc[bookdf.index, 'bd1segment'])
    bookdf.insert(7, 'bd1counter', df.loc[bookdf.index, 'bd1counter'])
    bookdf.insert(8, 'bd1numitems', df.loc[bookdf.index, 'numitems'])
    bookdf.insert(9, 'bo2reason', df.loc[bookdf.index, 'reason'])
    bookdf.insert(10, 'bo2side', df.loc[bookdf.index, 'side'])
    bookdf.loc[bookdf['msg_int']==1, 'bo2side'] = 0
    bookdf.insert(11, 'timestamp', df.loc[bookdf.index, 'timestamp'])

    if debug_mode:
        md_handler.show_info()
    return bookdf

def nk_score_sniper(df, long[:, :] triggers, long lookforward_window, long qty_threshold, long volume_threshold, 
                    bint write_timediffs=False):
    """
    nk_score_sniper(df, long[:, :] triggers, long lookforward_window, long qty_threshold, long volume_threshold)

    Score sniper triggers in fired contract vs triggered contract. Restricted to one-to-one relationship
    of trigger contract to fired contract. Plans to open up comparisons for cross-market fires and options in
    future iterations.

    Parameters
    ----------
    df : DataFrame
        DataFrame of fired contract
    triggers : ndarray[long, ndim=2]
        Array of size (n, 3) with columns representing: timestamp, fire price, direction
    lookforward_window : long
        Amount of time to look into the future in nanoseconds (to match units of the timestamp column in df)
    qty_threshold : long
        Market quantity required to qualify a signal as a positive event (bid or offered by at least this amount)
    volume_threshold : long
        Quantity required to qualify a signal as a positive event (trading by at least this amount)
    write_timediffs : bint
        Instead of timestamps, write time difference in nanoseconds from the trigger time

    Returns
    -------
    results : DataFrame
        mnk_state column:   0 := tradeable market is currently at the trigger price
                            1 := already bid or offered at the trigger price
                            2 := no market; too wide
                            3 := trigger price is through the tradeable market
    """

    cdef:
        long dflen = df.shape[0], tlen = triggers.shape[0], i = 0, j = 0, triggertime = 0, triggerprice = 0, direction = 0, sideidx = 0, oppidx = 0, jb = 0
        long[:] times = df.timestamp.values
        long[:, :] mktprcs = df.loc[:, ('bidprc0', 'askprc0')].values
        long[:, :] mktqtys = df.loc[:, ('bidqty0', 'askqty0')].values
        long[:, :] tradeprices = df.loc[:, ('buyprice', 'sellprice')].values
        long[:, :] tradeqtys = df.loc[:, ('buyvolume', 'sellvolume')].values
        long[:, :] accqtys = df.loc[:, ('acc_buyqty', 'acc_sellqty')].values
        
        bint already_bid = False, wide_market = False, thru_market = False
        
        list newcol = ['mnk_state', 'mnk_bidqty', 'mnk_bidprc', 'mnk_askprc', 'mnk_askqty', 
                       'mnk_tradetime_first', 'mnk_tradeqty_first', 'mnk_tradetime_max', 'mnk_tradeqty_max', 
                       'mnk_turntime_first', 'mnk_turnqty_first', 'mnk_turntime_max', 'mnk_turnqty_max', 
                       'mnk_ticktime', 'mnk_tickprice']
        ndarray[long, ndim=2] results = zeros((tlen, 15), dtype=long)
        
    for i in range(tlen):
        triggertime = triggers[i, 0]
        triggerprice = triggers[i, 1]
        direction = triggers[i, 2]
        if direction == 1:
            sideidx = 0
            oppidx = 1
        else:
            sideidx = 1
            oppidx = 0
        if j >= tlen:
            j = tlen - 1
        while j > 0 and times[j] > triggertime:
            j -= 1
        while j < dflen:
            if times[j] >= triggertime and times[j] - triggertime < lookforward_window:
                # check right before
                if j > 0 and results[i, 0] == 0 and times[j-1] <= triggertime:
                    jb = j-1
                    already_bid = direction * mktprcs[jb, sideidx] >= direction * triggerprice
                    wide_market = direction * triggerprice < direction * mktprcs[jb, oppidx] and direction * triggerprice > direction * mktprcs[jb, sideidx]
                    thru_market = direction * triggerprice > direction * mktprcs[jb, oppidx]
                    # write market
                    results[i, 1] = mktqtys[jb, 0]
                    results[i, 2] = mktprcs[jb, 0]
                    results[i, 3] = mktprcs[jb, 1]
                    results[i, 4] = mktqtys[jb, 1]
                    if already_bid:
                        results[i, 0] = 1
                    elif wide_market:
                        results[i, 0] = 2
                    elif thru_market:
                        results[i, 0] = 3                        
                if results[i, 9] == 0 and accqtys[j, sideidx] >= volume_threshold and tradeprices[j, sideidx] == triggerprice:
                    if results[i, 5] == 0:
                        if write_timediffs:
                            results[i, 5] = times[j] - triggertime
                        else:
                            results[i, 5] = times[j]
                        results[i, 6] = accqtys[j, sideidx]
                    if accqtys[j, sideidx] > results[i, 8]:
                        if write_timediffs:
                            results[i, 7] = times[j] - triggertime
                        else:
                            results[i, 7] = times[j]
                        results[i, 8] = accqtys[j, sideidx]
                if results[i, 13] == 0 and mktqtys[j, sideidx] >= qty_threshold and mktprcs[j, sideidx] == triggerprice:
                    if results[i, 9] == 0:
                        if write_timediffs:
                            results[i, 9] = times[j] - triggertime
                        else:
                            results[i, 9] = times[j]
                        results[i, 10] = mktqtys[j, sideidx]
                    if mktqtys[j, sideidx] > results[i, 12]:
                        if write_timediffs:
                            results[i, 11] = times[j] - triggertime
                        else:
                            results[i, 11] = times[j]
                        results[i, 12] = mktqtys[j, sideidx]
                if direction * mktprcs[j, sideidx] > direction * triggerprice:
                    if write_timediffs:
                        results[i, 13] = times[j] - triggertime
                    else:
                        results[i, 13] = times[j]
                    results[i, 14] = mktprcs[j, sideidx]
                    break
            elif times[j] - triggertime > lookforward_window:
                break
            j += 1
    
    return DataFrame(results, columns=newcol)

