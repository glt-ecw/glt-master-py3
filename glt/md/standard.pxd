from libc.stdint cimport *
from libcpp.map cimport map
from glt.md.book_building cimport *
from glt.md.moving_average cimport *
from glt.md.options_handling cimport *
from glt.md.resampling cimport *
from glt.md.trade_handling cimport *


cdef str mdorderbook_repr(MDOrderBook *md, uint16_t book_depth=*)

cdef list sample_dtype 


cdef void copy_mdorderbook(MDOrderBook *src, MDOrderBook *copy, uint64_t num_levels=*) nogil


cdef double calc_wmid(double bidprc, int64_t bidqty, double askprc, int64_t askqty, double mintick) nogil


cdef double calc_wmid_from_mdorderbook(MDOrderBook *md, double price_multiplier, double mintick) nogil


cdef double calc_imbalance(int32_t bidqty, int32_t askqty) nogil


cdef void copy_values_to_memoryview(MVRWithinBounds *mvr, double[:] output, uint64_t size_output) nogil


cdef void calc_mvr_entropy(MeanValueResampler *mvr, double half_life, double[:] w, uint64_t size_w) nogil


cdef void calc_mvr_diff(MeanValueResampler *mvr_left, MeanValueResampler *mvr_right, double[:] output, uint64_t size_output) nogil


cdef double calc_mvr_entropy_weighted_value(MeanValueResampler *mvr, double half_life) nogil


cdef class PyMDOrderBook:
    cdef:
        MDOrderBook *md_ptr
        readonly:
            uint16_t packet_len, eop, msg_idx
            str topic
            uint64_t secid, seqnum, time, exch_time, channel
            int32_t buyprice, buyvolume, sellprice, sellvolume, indicative
            int32_t bid_avol_qty, ask_avol_qty
            bint is_open
            list bids, asks

        void copy(self, MDOrderBook *md, bint parse_topic=*)


cdef class MDSource:
    cdef:
        MDOrderBook md
        MDOrderBook *md_ptr
        map[uint64_t, MDOrderBook] md_map
        readonly bint valid_md
        void _update_valid_md(self) nogil
    
        void _parse_csv_begin(self, const char* line, uint16_t line_len, uint16_t md_type) nogil
        void _parse_csv_end(self, const char* line, uint16_t line_len, uint16_t md_type) nogil
