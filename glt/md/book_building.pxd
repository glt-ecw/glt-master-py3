from libc.stdint cimport int16_t, int32_t, int64_t, uint16_t, uint32_t, uint64_t
from libcpp.vector cimport vector
from libcpp.string cimport string


cdef extern from "book_building.h" nogil:
    cdef:
        const uint16_t MAX_DEPTH
        const uint16_t TOPIC_SIZE
        string PrintMDOrderBookColumns(uint16_t depth)
    cdef cppclass BookLevel:
        int32_t price, qty, participants
    cdef cppclass MDOrderBook:
        uint16_t packet_len, eop, msg_idx
        vector[char] topic
        uint64_t secid, seqnum, time, exch_time, channel
        int32_t buyprice, buyvolume, sellprice, sellvolume
        int32_t bid_avol_qty, ask_avol_qty
        bint is_open
        vector[BookLevel] bids
        vector[BookLevel] asks
        MDOrderBook()
        void Init(uint16_t)
        void NullifyByteArray()
        void ParseDeprecatedJPXCSV(const char*, uint16_t, uint16_t, uint16_t)
        void ParseKRXCSV(const char*, uint16_t, uint16_t, uint16_t)
        void ParseJPXCSV(const char*, uint16_t, uint16_t, uint16_t)
        bint HasByteArray() const
        string ToCSV() const
    cdef cppclass BookDepth:
        uint16_t side_, depth_
        vector[BookLevel] levels
        BookDepth(uint16_t)
        BookDepth()
        void Init(uint16_t)
        void Add(int32_t, int32_t, int32_t)
        void Remove(int32_t, int32_t, bint, int32_t)
        void Sort()
        void Clear()
        uint64_t Size() const
        void FillMDOrderBook(vector[BookLevel]&)
    cdef cppclass BookBuilder:
        MDOrderBook md
        BookDepth bids, asks
        bint initialized
        BookBuilder()
        void Init(uint64_t)
        void ResetBooks(bint)
        bint HasBooks() const
        void RemoveParticipant(uint16_t, int32_t, int32_t)
        void ModifyDown(uint16_t, int32_t, int32_t)
        void FillMDOrderBook()
