cimport cython

from libc.math cimport pow as cpow, round as cround, fabs as cfabs
from numpy cimport ndarray
from numpy import datetime64, timedelta64, float64, int64, zeros
from pandas import to_datetime
from datetime import datetime
from dateutil.relativedelta import relativedelta

@cython.boundscheck(False)
cdef double polysum(double a, long n) nogil:
    cdef:
        long i
        double mysum = 0
    for i in range(1, n+1):
        mysum += cpow(a, i)
    return mysum

cdef double dpolysum(double a, long n) nogil:
    cdef:
        long i
        double mysum = 1.0
    for i in range(1, n+1):
        mysum += (i + 1) * cpow(a, i)
    return mysum

@cython.boundscheck(False)
cdef double polysum_cash(double a, long n) nogil:
    cdef:
        long i
        double mysum = 1
    for i in range(1, n+1):
        mysum += cpow(a, i)
    return mysum

cdef double npv(double time, double coupon, double payments, double rate) nogil:
    cdef:
        double cfreq = 1/payments
        double a = 1/(1 + rate*cfreq)
        long n = <long>cround(time/cfreq)
    return 100*(cpow(a, n) + coupon*cfreq*polysum(a, n))

cdef double dnpv(double time, double coupon, double payments, double rate) nogil:
    cdef:
        double cfreq = 1/payments
        double a = 1/(1 + rate*cfreq)
        double da_dy = 1/(cpow(1 + rate*cfreq, 2))
        long n = <long>cround(time/cfreq)

    #return <double>100*n*(-cfreq*a*a)*(cpow(a, n-1)+coupon*cfreq*dpolysum(a, n-1))
    return <double>( 100.0 * -1.0 * cfreq * da_dy ) * ( n*cpow(a, n-1) + coupon*cfreq*dpolysum(a, n-1) )

cdef double dnpv_old(double time, double coupon, double payments, double rate) nogil:
    cdef:
        double cfreq = 1/payments
        double a = 1/(1 + rate*cfreq)
        long n = <long>cround(time/cfreq)
    return <double>n*100*(cpow(a, n-1)+coupon*cfreq*polysum(a, n-1))

@cython.boundscheck(False)
cdef double irr(double time, double coupon, double payments, double prc, double start) nogil:
    #irr: internal rate of return 
    cdef:
        double rate = start, temp_rate, deriv, tolerance = 1e-6
        int iterations = 100, i
    for i in range(iterations):
        if rate > 0:
            #deriv = dnpv(time, coupon, payments, rate)
            deriv = dnpv_old(time, coupon, payments, rate)
            temp_rate = rate-(prc-npv(time, coupon, payments, rate))/deriv
            if cfabs(temp_rate-rate) < tolerance:
                break
            rate = temp_rate
    return rate

def find_next_coupon(now, maturity):
    next_date = to_datetime(datetime(now.year-1, maturity.month, maturity.day))
    while next_date < now:
        next_date += relativedelta(months=6)
    return next_date

@cython.cdivision(True)
cdef double npv_cash(double days_ctm, double days_ntc, double coupon, double payments, double rate) nogil:
    cdef:
        double cfreq = 1 / payments
        double ctm = (days_ctm - 1) / (365 / payments), ntc = (days_ntc + 1) / (365 / payments)
        long n = <long>cround(ctm)
        double a = 1 / (1 + rate*cfreq)
        double prc = 100*(cpow(a, n) + coupon*cfreq*polysum_cash(a, n))
    return prc / (1 + rate*cfreq*ntc)

@cython.cdivision(True)
cdef double dnpv_cash(double days_ctm, double days_ntc, double coupon, double payments, double rate) nogil:
    cdef:
        double cfreq = 1 / payments
        double ctm = (days_ctm - 1) / (365 / payments), ntc = (days_ntc + 1) / (365 / payments)
        long n = <long>cround(ctm)
        double a = 1 / (1 + rate*cfreq)
        double prc = <long>n*100*(cpow(a, n-1) + coupon*cfreq*polysum_cash(a, n-1))
    return prc / (1 + rate*cfreq*ntc)

@cython.boundscheck(False)
cdef double irr_cash(double days_ctm, double days_ntc, double coupon, double payments, double prc, double start) nogil:
    cdef:
        double rate = start, temp_rate, deriv, tolerance = 1e-6
        int iterations = 100, i
    for i in range(iterations):
        if rate > 0:
            deriv = dnpv_cash(days_ctm, days_ntc, coupon, payments, rate)
            temp_rate = rate-(prc-npv_cash(days_ctm, days_ntc, coupon, payments, rate))/deriv
            if cfabs(temp_rate-rate) < tolerance:
                break
            rate = temp_rate
    return rate

@cython.cdivision(True)
cdef double shitty_yield(double ytm, double coupon, double payments, double prc) nogil:
    return (coupon + ((1 - prc/100)/ytm))/(1 + prc/100)*payments

@cython.cdivision(True)
cdef double shitty_prc(double ytm, double coupon, double payments, double rate) nogil:
    return 100 * (rate*ytm - payments * (coupon*ytm + 1)) / (-2 - rate*ytm)

cdef double shitty_yield_cash(double days_ctm, double days_ntc, double coupon, double payments, double prc) nogil:
    cdef double ytm = (days_ntc + days_ctm)/365
    return shitty_yield(ytm, coupon, payments, prc)

cdef double shitty_prc_cash(double days_ctm, double days_ntc, double coupon, double payments, double rate) nogil:
    cdef double ytm = (days_ntc + days_ctm)/365
    return shitty_prc(ytm, coupon, payments, rate)

@cython.boundscheck(False)
cdef void calculate_yields_cash_fast(double[:] prices, long n, double days_ctm, double days_ntc, double coupon, double payments, double start, double[:] yields, double[:] bpvs) nogil:
    cdef:
        long i
    for i in range(n):
        if prices[i] > 0:
            yields[i] = irr_cash(days_ctm, days_ntc, coupon, payments, prices[i], start)
            bpvs[i] = yields[i] - irr_cash(days_ctm, days_ntc, coupon, payments, prices[i] + 0.01, start)
    return

@cython.boundscheck(False)
cdef void calculate_yields_cash_shitty_fast(double[:] prices, long n, double days_ctm, double days_ntc, double coupon, double payments, double[:] yields, double[:] bpvs) nogil:
    cdef:
        long i
    for i in range(n):
        if prices[i] > 0:
            yields[i] = shitty_yield_cash(days_ctm, days_ntc, coupon, payments, prices[i])
            bpvs[i] = yields[i] - shitty_yield_cash(days_ctm, days_ntc, coupon, payments, prices[i] + 0.01)
    return

def calculate_yields_cash(ndarray prices, defs, calc_type=None):
    cdef:
        long n = prices.shape[0]
        list parameters = ['coupon', 'ctm', 'ntc']
        double coupon, days_ctm, days_ntc, payments = 2, start = 0.05
        ndarray yields = zeros(n, dtype=float64)
        ndarray bpvs = zeros(n, dtype=float64)
        
    coupon, days_ctm, days_ntc = defs[parameters]
    
    if not calc_type:
        calculate_yields_cash_fast(prices, n, days_ctm, days_ntc, coupon, payments, start, yields, bpvs)
    elif calc_type == 'shitty':
        calculate_yields_cash_shitty_fast(prices, n, days_ctm, days_ntc, coupon, payments, yields, bpvs)
    else:
        print('please define calc_type. leave blank if you want correct calculations')
        return
    return yields, bpvs
       
@cython.boundscheck(False)
cdef void calculate_yields_deriv_fast(double[:] prices, long n, double time, double coupon, double payments, double start, double[:] yields, double[:] bpvs) nogil:
    cdef:
        long i
    for i in range(n):
        if prices[i] > 0:
            yields[i] = irr(time, coupon, payments, prices[i], start)
            bpvs[i] = yields[i] - irr(time, coupon, payments, prices[i] + 0.01, start)
    return

@cython.boundscheck(False)
cdef void calculate_yields_deriv_shitty_fast(double[:] prices, long n, double time, double coupon, double payments, double[:] yields, double[:] bpvs) nogil:
    cdef:
        long i
    for i in range(n):
        if prices[i] > 0:
            yields[i] = shitty_yield(time, coupon, payments, prices[i])
            bpvs[i] = yields[i] - shitty_yield(time, coupon, payments, prices[i] + 0.01)
    return

def calculate_yields_deriv(ndarray prices, defs, calc_type=None):
    cdef:
        long n = prices.shape[0]
        list parameters = ['coupon', 'time']
        double coupon, time, payments = 2, start = 0.05
        ndarray yields = zeros(n, dtype=float64)
        ndarray bpvs = zeros(n, dtype=float64)
        
    coupon, time = defs[parameters]
    
    if not calc_type:
        print "using new things"
        calculate_yields_deriv_fast(prices, n, time, coupon, payments, start, yields, bpvs)
    elif calc_type == 'shitty':
        calculate_yields_deriv_shitty_fast(prices, n, time, coupon, payments, yields, bpvs)
    else:
        print('please define calc_type. leave blank if you want correct calculations')
        return
    return yields, bpvs
