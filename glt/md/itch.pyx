# distutils: language=c++
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from libc.math cimport NAN, isnan
from libcpp.map cimport map
from numpy cimport ndarray

from numpy import empty, float64

def fill_prices_qtys(md, dict msg_intmap):
    cdef:
        size_t i, n = md.shape[0]
        long oid, price, qty, msg, a_msg, e_msg, d_msg
        long[:] oids = md['oid'].values
        long[:] prices = md['price'].values
        long[:] qtys = md['qty'].values
        long[:] msgs = md['msg'].values
        map[long, long] order_prices, order_qtys
        ndarray output = empty((n, 2), dtype=float64)
    a_msg = msg_intmap['A']
    e_msg = msg_intmap['E']
    d_msg = msg_intmap['D']
    output[:, 0] = prices
    output[:, 1] = qtys
    for 0 <= i < n:
        oid = oids[i]
        price = prices[i]
        qty = qtys[i]
        msg = msgs[i]
        if msg == a_msg:
            order_prices[oid] = price
            order_qtys[oid] = qty
        elif msg == e_msg:
            output[i, 0] = order_prices[oid]
            order_qtys[oid] -= qty
        elif msg == d_msg:
            output[i, 0] = order_prices[oid]
            output[i, 1] = order_qtys[oid]
    return output
