from libc.stdint cimport int16_t, int32_t, int64_t, uint16_t, uint32_t, uint64_t


cdef extern from "moving_average.h" nogil:
    cdef cppclass MovingAverageWithEntropy:
        MovingAverageWithEntropy()
        void Init(uint64_t, double)
        void AddData(uint64_t, double)
        double Value() const
        void Clear()
        uint64_t TimeWindow() const
        uint64_t Size() const
        bint IsSaturated() const
    cdef cppclass MAWEManagerINT64:
        MAWEManagerINT64()
        void Init(uint64_t, double)
        void AddData(uint64_t, uint64_t, double)
        double Value(uint64_t) const
        void Clear()
    cdef cppclass MAWEManagerUINT64:
        MAWEManagerUINT64()
        void Init(uint64_t, double)
        void AddData(int64_t, uint64_t, double)
        double Value(int64_t) const
        void Clear()
