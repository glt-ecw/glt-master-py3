from libc.stdint cimport int16_t, int32_t, int64_t, uint16_t, uint32_t, uint64_t


cdef extern from "trade_handling.h" nogil:
    cdef cppclass VolumeAccumulator:
        VolumeAccumulator()
        void Init(uint64_t)
        int32_t AddTrade(uint64_t, int32_t)
        void RemoveTrades(uint64_t)
        int32_t Qty() const
        void Clear()
        uint64_t TimeWindow() const
        uint32_t Size() const
