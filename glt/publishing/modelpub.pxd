# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True

from libc.stdint cimport *
from libcpp.unordered_map cimport unordered_map
from numpy cimport ndarray


cdef class OPMSharedArray:
    cdef:
        unordered_map[uint64_t, uint64_t] secid_map
        readonly:
            list secids
            uint64_t n_secids
            double tte
            list pub_adjust_idx, pub_full_idx
        public:
            ndarray values
