from libc.stdint cimport *

cdef extern from "deio_publishing.h" nogil:
    cdef cppclass DeioPairPacker:
        DeioPairPacker()
        void Init(uint8_t version, uint16_t model_id, uint16_t num_spreads)
        void InitValuesAt(uint16_t index, uint8_t spread_type, 
                          uint32_t securityid_a, uint32_t securityid_b, uint32_t securityid_u)
        void UpdateValuesAt(uint16_t index, double spread_value, double ref_u, double spread_value_du, double improve_u_offset)
        uint64_t Prepare()
        char* ByteArray() const
        uint64_t NumBytes() const
