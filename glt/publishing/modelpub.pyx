# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True

from cython.operator cimport dereference
from libc.math cimport NAN, sqrt
from glt.md.standard cimport MDOrderBook, PyMDOrderBook
from glt.listening.md cimport DeioMDFWDReader, DeioMDReceiver
#from glt.listening.oe cimport DeioFill, DeioFillFWDReader, DeioFillReceiver
from numpy cimport ndarray

#from glt.net import MulticastSocket
from threading import Thread
from time import sleep
from numpy import zeros


cdef uint64_t SHARED_ARR_SIZE = 15

cdef uint64_t SHARED_ARR_POSITION = 0 
cdef uint64_t SHARED_ARR_VOL = 1 
cdef uint64_t SHARED_ARR_TTE = 2 
cdef uint64_t SHARED_ARR_MONEYNESS = 3 
cdef uint64_t SHARED_ARR_VEGA = 4 
cdef uint64_t SHARED_ARR_ADJ = 5 
cdef uint64_t SHARED_ARR_K = 6 
cdef uint64_t SHARED_ARR_IS_SM = 7 
cdef uint64_t SHARED_ARR_CALL_POSITION = 8 
cdef uint64_t SHARED_ARR_PUT_POSITION = 9 
cdef uint64_t SHARED_ARR_CALL_DELTA = 10
cdef uint64_t SHARED_ARR_PUT_DELTA = 11
cdef uint64_t SHARED_ARR_CALL_ADJUST = 12
cdef uint64_t SHARED_ARR_PUT_ADJUST = 13
cdef uint64_t SHARED_ARR_VP = 14


cdef void _fill_black_scholes_at(double[:, :] values, uint64_t i, 
        double vol, double vp, double moneyness, double vega,
        double call_delta, double put_delta) nogil:
    values[i, SHARED_ARR_VOL] = vol
    values[i, SHARED_ARR_VP] = vp
    values[i, SHARED_ARR_MONEYNESS] = moneyness
    values[i, SHARED_ARR_VEGA] = vega
    values[i, SHARED_ARR_CALL_DELTA] = call_delta
    values[i, SHARED_ARR_PUT_DELTA] = put_delta


cdef void _modify_position_at(double[:, :] values, uint64_t i, double position) nogil: 
    values[i, SHARED_ARR_POSITION] += position


cdef class OPMSharedArray:
    def __init__(self, list secids, double tte):
        cdef uint64_t secid, i
        self.secids = secids
        self.n_secids = len(self.secids)
        for 0 <= i < self.n_secids:
            self.secid_map[self.secids[i]] = i
        self.tte = tte
        self.values = zeros((self.n_secids, SHARED_ARR_SIZE), dtype='<f8')
        self.values[:, SHARED_ARR_TTE] = tte
        self.pub_adjust_idx = [
            SHARED_ARR_CALL_ADJUST, SHARED_ARR_PUT_ADJUST
        ]
        self.pub_full_idx = [
            SHARED_ARR_VOL, SHARED_ARR_VP, SHARED_ARR_CALL_ADJUST, SHARED_ARR_PUT_ADJUST
        ]
        self.values[:, SHARED_ARR_POSITION] = 0
        self.values[:, SHARED_ARR_VOL] = NAN
        self.values[:, SHARED_ARR_VP] = 0
        self.values[:, SHARED_ARR_MONEYNESS] = NAN
        self.values[:, SHARED_ARR_VEGA] = NAN
        self.values[:, SHARED_ARR_CALL_ADJUST] = 0
        self.values[:, SHARED_ARR_PUT_ADJUST] = 0

    def set_strikes(self, dict secid_to_strike):
        cdef:
            uint64_t secid
            double strike
        for secid, strike in secid_to_strike.iteritems():
            if self.secid_map.find(secid) == self.secid_map.end():
                raise KeyError('cannot find secid=%d in secid_map. known secids: %s' % (secid, self.secids))
            self.values[self.secid_map[secid], SHARED_ARR_K] = strike

    def modify_position_at(self, uint64_t secid, double position):
        if self.secid_map.find(secid) == self.secid_map.end():
            raise KeyError('cannot find secid=%d in secid_map. known secids: %s' % (secid, self.secids))
        _modify_position_at(self.values, self.secid_map[secid], position)

    def copy_adjustment_to_call_put(self):
        self.values[:, SHARED_ARR_CALL_ADJUST] = self.values[:, SHARED_ARR_ADJ]
        self.values[:, SHARED_ARR_PUT_ADJUST] = self.values[:, SHARED_ARR_ADJ]

    def set_black_scholes_at(self, uint64_t secid, 
            double vol, double vp, double moneyness=NAN, double vega=NAN, 
            double call_delta=NAN, double put_delta=NAN):
        if self.secid_map.find(secid) == self.secid_map.end():
            raise KeyError('cannot find secid=%d in secid_map. known secids: %s' % (secid, self.secids))
        _fill_black_scholes_at(self.values, self.secid_map[secid], vol, vp, moneyness, vega, call_delta, put_delta)

    #def set_position_adjust_at(self, uint64_t secid,
    #        double call_adjust, double put_adjust, double call_position=NAN, 
    #        double put_position=NAN):
    #    if self.secid_map.find(secid) == self.secid_map.end():
    #        raise KeyError('cannot find secid=%d in secid_map. known secids: %s' % (secid, self.secids))
    #    _fill_position_adjust_at(self.values, self.secid_map[secid], call_adjust, put_adjust, 
    #        call_position, put_position)

    def get_pub_full(self):
        return self.values[:, self.pub_full_idx]

    def get_pub_adjust(self):
        return self.values[:, self.pub_adjust_idx]
