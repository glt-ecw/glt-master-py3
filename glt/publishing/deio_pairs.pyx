# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True

from cython.operator cimport dereference
from libc.stdint cimport *
from glt.md.standard cimport MDOrderBook, PyMDOrderBook
from glt.listening.md cimport DeioMDFWDReader, DeioMDReceiver
from glt.listening.oe cimport DeioFill, DeioFillFWDReader, DeioFillReceiver
from glt.modeling.spread_pairs.base cimport SpreadPairModel
from glt.publishing.deio_publishing cimport DeioPairPacker
from glt.net import MulticastSocket
from threading import Thread
from time import sleep


cdef uint8_t DEIO_PAIRS_PUBLISHER_VERSION = 1

cdef int64_t MIN_PUBLISH_INTERVAL_IN_SECONDS = 10


cdef class DeioPairsPublisher:
    """
    DeioPairsPublisher listens to market data and recomputes the values of
    a list of SpreadPairModel objects. In a separate thread (not using
    multiprocessing), DeioPairsPublisher listens to a Deio market data
    forwarding channel and determines which models to update based on
    the contract this market data corresponds to.

    For now, it will not publish to the publishing address pub_addr (SEE
    TODO). 
    """
    cdef:
        double price_multiplier
        uint16_t model_id
        int64_t pub_interval
        uint32_t n_models
        dict secid_model_map, model_index_map
        list models, byte_list
        MDOrderBook *md_ptr
        DeioMDReceiver md_receiver
        DeioFill *fill_ptr
        DeioFillReceiver fill_receiver
        publisher, thread, logger
        DeioPairPacker packer

    def __init__(self, md_sub_iface_addr, fill_sub_iface_addr, pub_iface_addr, list models, 
                 uint16_t model_id, 
                 double price_multiplier=1.0, 
                 int64_t pub_interval=MIN_PUBLISH_INTERVAL_IN_SECONDS, logger=None):
        cdef:
            SpreadPairModel model
            uint64_t i, k, n_models = len(models)
        # logger
        self.logger = logger
        self.log_info('md_sub_iface_addr: %s' % md_sub_iface_addr)
        self.log_info('fill_sub_iface_addr: %s' % fill_sub_iface_addr)
        self.log_info('pub_iface_addr: %s' % pub_iface_addr)
        # md_receiver
        self.price_multiplier = price_multiplier
        md_sub_iface, md_sub_addr = md_sub_iface_addr.split(';')
        self.md_receiver = DeioMDReceiver(md_sub_iface, md_sub_addr, price_multiplier=price_multiplier)
        self.md_ptr = &(self.md_receiver.reader.md)
        # fill_receiver
        fill_sub_iface, fill_sub_addr = fill_sub_iface_addr.split(';')
        self.fill_receiver = DeioFillReceiver(fill_sub_iface, fill_sub_addr)
        self.fill_ptr = &(self.fill_receiver.reader.fill)
        # publisher
        pub_iface, pub_addr = pub_iface_addr.split(';')
        self.publisher = MulticastSocket(pub_iface, publish=True, addr=pub_addr)
        # make look_up lists for models
        self.secid_model_map = {}
        self.model_index_map = {}
        self.models = models
        self.n_models = n_models
        # initialize byte packer object
        self.model_id = model_id
        self.packer.Init(DEIO_PAIRS_PUBLISHER_VERSION, self.model_id, n_models)
        self.byte_list = [0] * self.packer.NumBytes()
        for k, model in enumerate(self.models):
            model.assign_logger(self.logger)
            self.packer.InitValuesAt(k, model.spread_type, model.secid_a, model.secid_b, model.secid_u)
            if model.key() in self.model_index_map:
                existing = self.models[self.model_index_map[model.key()]]
                error = (
                    'model: %s conflicts with existing: %s; same key: %d'
                ) % (model, existing, model.key())
                self.log_error(error)
                raise KeyError(error)
            self.model_index_map[model.key()] = k
            for 0 <= i < model.secids.size():
                if model.secids[i] not in self.secid_model_map:
                    self.secid_model_map[model.secids[i]] = []
                self.secid_model_map[model.secids[i]].append(model)
        if pub_interval < MIN_PUBLISH_INTERVAL_IN_SECONDS:
            pub_interval = MIN_PUBLISH_INTERVAL_IN_SECONDS
        self.pub_interval = pub_interval
        # and let's start the listening process
        self.md_receiver.listen()
        self.log_info('md_receiver.listen() called')
        self.fill_receiver.listen()
        self.log_info('fill_receiver.listen() called')
        self.thread = None

    def log_info(self, msg):
        if self.logger:
            self.logger.info('%s: %s' % (self, msg))

    def log_error(self, msg):
        if self.logger:
            self.logger.error('%s: %s' % (self, msg))

    def log_warning(self, msg):
        if self.logger:
            self.logger.warning('%s: %s' % (self, msg))

    def log_debug(self, msg):
        if self.logger:
            self.logger.debug('%s: %s' % (self, msg))

    def evaluate_models(self):
        cdef:
            DeioMDReceiver md_receiver = self.md_receiver
            DeioFillReceiver fill_receiver = self.fill_receiver
            MDOrderBook *md = self.md_ptr
            DeioFill *fill = self.fill_ptr
            uint64_t k
            SpreadPairModel model
            PyMDOrderBook pymd
            list processed_md_msgs = []
            list processed_fill_msgs = []
        # fills first
        while fill_receiver.process_next_msg():
            if fill_receiver._ignore_fill():
                continue
            # TODO: handle each fill like md below
            if fill.secid in self.secid_model_map:
                for model in self.secid_model_map[fill.secid]:
                    if not model.process_fills:
                        continue
                    k = self.model_index_map[model.key()]
                    model._copy_fill(fill)
                    model.evaluate_fills()
                    self.packer.UpdateValuesAt(
                        k, 
                        model.spread_value / self.price_multiplier, 
                        model.ref_u / self.price_multiplier, 
                        model.spread_value_du / self.price_multiplier,
                        model.improve_u_offset / self.price_multiplier
                    )
                processed_fill_msgs.append((fill.secid, fill.fwd_seqnum, fill.side, fill.qty, fill.price, fill.order_type))
        # then md
        while md_receiver.process_next_msg():
            if not md_receiver._use_md():
                continue
            if md.secid in self.secid_model_map:
                for model in self.secid_model_map[md.secid]:
                    k = self.model_index_map[model.key()]
                    model._copy_md(md)
                    model.evaluate_md()
                    self.packer.UpdateValuesAt(
                        k, 
                        model.spread_value / self.price_multiplier, 
                        model.ref_u / self.price_multiplier, 
                        model.spread_value_du / self.price_multiplier,
                        model.improve_u_offset / self.price_multiplier
                    )
                pymd = PyMDOrderBook()
                pymd.copy(md)
                processed_md_msgs.append(pymd)
        for msg in processed_fill_msgs:
            self.log_debug('FILL: secid=%d, fwd_seqnum=%d, side=%d, %d @ %0.4f, type=%d' % msg)
        for pymd in processed_md_msgs:
            self.log_debug(pymd)

    def send(self):
        cdef:
            SpreadPairModel model
            list byte_list = self.byte_list
            uint64_t i
            char* byte_arr = self.packer.ByteArray()
            uint64_t seqnum
        seqnum = self.packer.Prepare()
        for 0 <= i < self.packer.NumBytes():
            byte_list[i] = <uint8_t>dereference(byte_arr + i)
        self.publisher.send_bytes(byte_list)
        for model in self.models:
            self.log_info(
                (
                    'model_id=%d, seqnum=%d; spread value for %s is '
                    'spread_value=%0.5f, ref_u=%0.5f, spread_value_du=%0.5f, '
                    'improve_u_offset=%0.5f'
                ) % (
                    self.model_id, seqnum, 
                    model, 
                    model.spread_value / self.price_multiplier, 
                    model.ref_u / self.price_multiplier, 
                    model.spread_value_du / self.price_multiplier,
                    model.improve_u_offset / self.price_multiplier
                )
            )

    def run(self):
        while True:
            self.send()
            sleep(self.pub_interval)

    def start_publishing(self, double sleep_by=0.5):
        self.thread = Thread(target=self.run)
        self.thread.daemon = True
        self.thread.start()
        sleep(sleep_by)
