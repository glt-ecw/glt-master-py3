cimport cython
from numpy cimport ndarray
from numpy import zeros, float64
from numpy import abs, unique, sort, where, array, minimum
from pandas import to_datetime

cdef struct updateSprawlerPosition: #updateSprawlerResult
    long pos
    long ordersize
    long qty_filled ## is this necessary? ##

cdef struct FinalSprawlerResult:
    long ordersize
    long qty_filled
    long qty_won
    long qty_lost
    long qty_scratch

cdef struct updateSprawlerScratch:
    long qty_scratch
    long qty_lost

cdef struct WillScratch:
    bint will_scratch
    long minqty_trigs
    long streaker_trigs
    long streakerpct_trigs
    

cdef struct Order:
    long price
    long initial
    long working
    long filled
    long curr_pos    

cdef Order update_traded_prcs(dict trade_record, long price, long volume, Order order, long direction, long pop_price_bool):
    
    cdef:
        long old_vol
        ndarray prices
        
    if price in trade_record.keys():
        old_vol = abs(trade_record[price])
    else:
        old_vol=0
        
    if volume < order.working:
        trade_record[price] = (direction) * (old_vol + volume)
        order.curr_pos += (direction * volume)
        order.working -= abs(volume)
    else:
        trade_record[price] += direction * order.working
        order.curr_pos += (direction) * order.working
        order.working = 0        

    order.filled = trade_record[price]
    if pop_price_bool == 1:
        trade_record.pop(price, None)
        prices = sort(trade_record.keys())
        
        # get rid of irrelevant prices 
        # e.g., bids higher than last sell price,
        #       offers lower than last buy price.
        if direction == 1:
            prices = prices[prices < price]
        else:
            prices = prices[prices > price]
            
        # recreate trade_record dict with updated prices.
        # should be a ref by sharing so do not have to return dict specifically
        trade_record = dict(zip(prices, zeros(len(prices))))
    
    return order 


cdef updateSprawlerScratch find_scratch_event(long i, long scratchdirection, long mktdelay, Order order, double[:] timediffs, 
                                             long[:] mktprcs, long[:] mktqtys, long[:] tradevolumes, long[:] tradeprices, long traded_price):
    
    cdef:
        updateSprawlerScratch result
        double acc_time=0
        long remqty=0, j=0
        long last_prc = mktprcs[i]
        long last_qty = mktqtys[i]
        long ix_max = len(timediffs) - 1
        long qty_scratch = 0, qty_lost = 0, curr_market = 0
        long curr_pos = order.curr_pos
        long working = order.working    
        
    if i < ix_max:
        j = i + 1
    else:
        j = i #the next mkt book update
        # take into acct if next book update has a trade event
        curr_pos += (-1 * scratchdirection) * tradevolumes[i]

    
    # aggregate traded volumes during mktdelay
    while (j < ix_max and i < ix_max):
        acc_time += timediffs[j]
        
        if tradeprices[i] == traded_price:
            curr_pos += (-1 * scratchdirection) * tradevolumes[i]
            
        if acc_time >= mktdelay:
            break
        else:
            
            i+=1
            j+=1
                
    if abs(curr_pos) > (abs(order.curr_pos) + abs(working)):
        #can not have more on than was originally working
        curr_pos =  (abs(order.curr_pos) + abs(working))
        
    if scratchdirection == -1:
        # no market to scratch
        curr_market = 0 if mktprcs[j] < traded_price else mktqtys[j]
        
    if scratchdirection == 1:
        curr_market = 0 if mktprcs[j] > traded_price else mktqtys[j]
    
    remqty = curr_market - abs(curr_pos)
    
    ### market moves in your favor, scratch all remqty 
    if ((scratchdirection == -1) and (mktprcs[j] > traded_price)) or ((scratchdirection == 1) and (mktprcs[j] < traded_price)):
        remqty = 0

    if remqty < 0: #could not scratch all of pos
        qty_lost = abs(remqty)
        qty_scratch = curr_market
    else:
        qty_lost = 0
        qty_scratch = abs(curr_pos)


    result.qty_scratch = qty_scratch
    result.qty_lost = qty_lost

    return result

cdef WillScratch should_sprawler_scratch(Order order, long mktqty, long minqty, long accqty, long streakerqty, double accpct, double streakerpct) nogil: 
    
    cdef:
        WillScratch scratch_decision;
        long minqty_trigs = 0, streaker_trigs = 0, streakerpct_trigs = 0;
        long ordersize = order.curr_pos + order.working
        
    if mktqty <= (minqty + ordersize) or (accqty >= streakerqty and accpct >= streakerpct):
        scratch_decision.will_scratch = 1
        if (mktqty <= (minqty + ordersize)) and not (accqty >= streakerqty and accpct >= streakerpct):
            scratch_decision.minqty_trigs = 1
        elif (accqty >= streakerqty and accpct >= streakerpct) and not (mktqty <= (minqty + ordersize)):
            if (accqty >= streakerqty) and not (accpct >= streakerpct):
                scratch_decision.streaker_trigs = 1
            elif (accpct >= streakerpct) and not (accqty >= streakerqty):
                scratch_decision.streakerpct_trigs = 1
            else:
                scratch_decision.streaker_trigs = 1
                scratch_decision.streakerpct_trigs = 1
        else:
            scratch_decision.minqty_trigs = 1
            if (accqty >= streakerqty) and not (accpct >= streakerpct):
                scratch_decision.streaker_trigs = 1
            elif (accpct >= streakerpct) and not (accqty >= streakerqty):
                scratch_decision.streakerpct_trigs = 1
            else:
                scratch_decision.streaker_trigs = 1
                scratch_decision.streakerpct_trigs = 1
    else:
        scratch_decision.will_scratch = 0
    
    return scratch_decision


@cython.boundscheck(False)
cdef list eval_sprawler_pnl(long scratchdirection, long code_int, long minqty, long streakerqty, double streakerpct, 
                                           long time_threshold, long[:] tradevolumes, long[:] tradeprices, long[:] delta_mktqty, 
                                           long[:] turn_dirs, long[:] times, double[:] timediffs, long[:] mktprcs, long[:] mktqtys, long[:] oppprcs, 
                                           long[:] oppqtys, long[:] accqtys, double[:] accpcts, int trade_size, bint print_yes, bint print_yes2, double shrink_pct):
    
    cdef:
        long i=0, tick_direction = 0, scratch_time = 0, queue_pos = 0, initial_pos = 0;
        long minqty_trigs = 0, streaker_trigs = 0, streakerpct_trigs = 0;
        long curr_pos=0, n=len(mktprcs)
        long mktdelay=5; #ms
        #long mktdelay=20; #ms
        long num_trades = 0, vol_traded = 0;
        long start_i = 0;
        long last_traded_price = 0
        long ticksize = 1;
        
        ndarray unique_prices = unique(mktprcs);
        ndarray prices;

        bint will_scratch = False;
        bint trade_started = False;
        bint qty_shrink = False;
        double success = 0, count_turns = 0;
        
        updateSprawlerScratch scratchresult
        updateSprawlerPosition result;
        WillScratch scratch_decision;
        Order order
        
        dict traded_prices;
        long qty_won=0, qty_lost=0, qty_scratch=0, qty_working=0, qty_filled=0
        list results =[]
        
        
    # grab first tradeprice with nonzero volume
    if scratchdirection == -1: #bidside
        prices = unique_prices[unique_prices <= array(tradeprices)[array(tradevolumes) > 0][0]]
        if len(prices):        
            start_i = where(array(mktprcs) == max(prices))[0][0]
    elif scratchdirection == 1: #offer side
        prices = unique_prices[unique_prices >= array(tradeprices)[array(tradevolumes) > 0][0]]
        if len(prices):
            start_i = where(array(mktprcs) == min(prices))[0][0]
            
    traded_prices = dict(zip(prices, zeros(len(prices))))
    queue_pos = 0
    order.price = tradeprices[i]
    order.initial = trade_size
    order.filled = 0
    order.curr_pos = 0
    order.working = trade_size
    
    for i in range(start_i - 1, n):
        ## book is thin, wait till it fills in
        if not trade_started and (mktqtys[i] < (order.working + minqty)) and (mktprcs[i] in traded_prices.keys()): 
            if print_yes:
                print to_datetime(times[i]), "Book too thin mktqty: %d and tradesize: %d and minqty: %f" %(mktqtys[i], trade_size, minqty), " Removed price %d from traded_prices\n" %mktprcs[i]
                print traded_prices
            ## cases where you had made a tick on the working order but thins out... forced to cancel it.
            traded_price = mktprcs[i]
            order = update_traded_prcs(traded_prices, traded_price, 0, order, -1 * scratchdirection, 1)
            qty_filled = abs(qty_lost) + abs(qty_scratch) + abs(qty_won)
            
            results.append([to_datetime(times[i]), scratchdirection, code_int, minqty, streakerqty, streakerpct, traded_price, qty_working, 
                            qty_filled, qty_scratch, qty_won, qty_lost, minqty_trigs, streaker_trigs, streakerpct_trigs])
            qty_filled = 0; qty_scratch = 0; qty_won = 0; qty_lost = 0; minqty_trigs = 0; streaker_trigs = 0; streakerpct_trigs = 0;            
            order.curr_pos = 0
            order.working = trade_size                 
            continue
            
        if not len(traded_prices) and not curr_pos:
            #print "No more prices to trade... exiting..\n"
            break
            
        # trade has started... 
        if tradevolumes[i]: # getting filled on bid
            if print_yes:
                print "\nThere was a trade for prc ", tradeprices[i], " and volume ", tradevolumes[i], "\n"
            traded_volume = tradevolumes[i]
            traded_price = tradeprices[i]
                        
            if traded_price not in traded_prices.keys():
                ## scenario 1. that price has already traded, move on.
                if print_yes:
                    print "Price: %d Already traded \n" %traded_price
                trade_started = False
                continue
                            
            elif (mktprcs[i] not in traded_prices.keys()) or (mktqtys[i] < (order.working + minqty)):
                ## scenario 2: no market for traded price. book has thinned? if pos on, hit/lift, bid/offer
                scratchresult = find_scratch_event(i, scratchdirection, mktdelay, order, timediffs, mktprcs, mktqtys, tradevolumes, tradeprices, traded_price)    
                qty_lost = scratchresult.qty_lost
                qty_scratch = scratchresult.qty_scratch
                order = update_traded_prcs(traded_prices, traded_price, traded_volume, order, -1*scratchdirection, 1)
                
                qty_filled = abs(qty_lost) + abs(qty_scratch) + abs(qty_won)
                
                if qty_scratch:
                    minqty_trigs += 1
                    
                trade_started = False
                
                if print_yes:
                    print "Not enough size for minqty. Cancelling order for price.", traded_price, "for mkt ", sort([mktprcs[i], oppprcs[i]]), [mktqtys[i], oppqtys[i]]#, " remaining qty: ", 
                    print "the qty lost is ", qty_lost, " and qty scratched is ", qty_scratch, " and qty working ", qty_working, " and qty filled ", qty_filled
                    
                results.append([to_datetime(times[i]), scratchdirection, code_int, minqty, streakerqty, streakerpct, last_traded_price, qty_working, 
                                qty_filled, qty_scratch, qty_won, qty_lost, minqty_trigs, streaker_trigs, streakerpct_trigs])
                
                ## reset variables
                curr_pos = 0; qty_filled = 0; qty_scratch = 0; qty_won = 0; qty_lost = 0; minqty_trigs = 0; streaker_trigs = 0; streakerpct_trigs = 0;
                order.curr_pos = 0
                order.working = trade_size     
                continue
                
            elif turn_dirs[i] != ticksize * scratchdirection:
                ## scenario 3: getting filled on order
                last_traded_price = traded_price                
                trade_started = True
                order = update_traded_prcs(traded_prices, traded_price, traded_volume, order, -1 * scratchdirection, 0)
                curr_pos = order.curr_pos
                #ordersize = sprawler.ordersize
                qty_filled = order.filled
                if print_yes:
                    print "Getting filled on order. Price: ", traded_price, " Qty: ", traded_volume, num_trades, vol_traded
                    print "Traded_prc dict updated with: ", traded_price, traded_volume, traded_prices
                    print "The current market is: ", [mktprcs[i], oppprcs[i]], [mktqtys[i], oppqtys[i]], " remaining qty: " 
                    print "At %s for price: %d, The current pos: %d and qty_remaining: \n"%(to_datetime(times[i]), last_traded_price, curr_pos)
                    
        if trade_started:
            # get out of curr pos
            if turn_dirs[i] == -1 * ticksize * scratchdirection:
                qty_won += order.curr_pos
                order.curr_pos = 0
                trade_started = False
                #update pos & traded_pos
                if print_yes:                
                    print "Result: Made a tick at ", to_datetime(times[i]), " because market went "
                    print "The current market is: ", sort([mktprcs[i], oppprcs[i]]), [mktqtys[i], oppqtys[i]], " remaining qty: "
                    print "Updated dictionary: ", traded_prices, curr_pos, qty_won
                results.append([to_datetime(times[i]), scratchdirection, code_int, minqty, streakerqty, streakerpct, last_traded_price, qty_working, 
                                qty_filled, qty_scratch, qty_won, qty_lost, minqty_trigs, streaker_trigs, streakerpct_trigs])
                
            elif turn_dirs[i] == ticksize * scratchdirection:                
                scratch_decision = should_sprawler_scratch(order, mktqtys[i], minqty, accqtys[i], streakerqty, accpcts[i], streakerpct)
                scratchresult = find_scratch_event(i, scratchdirection, mktdelay, order, timediffs, mktprcs, mktqtys, tradevolumes, tradeprices, traded_price)
                order.curr_pos = 0
                qty_lost = scratchresult.qty_lost
                qty_scratch = scratchresult.qty_scratch
                if qty_scratch:
                    minqty_trigs += scratch_decision.minqty_trigs
                    streaker_trigs += scratch_decision.streaker_trigs
                    streakerpct_trigs += scratch_decision.streakerpct_trigs
                    
                order = update_traded_prcs(traded_prices, traded_price, traded_volume, order, -1 * scratchdirection, 0)
                qty_filled = abs(qty_lost) + abs(qty_scratch) + abs(qty_won)
                trade_started = False
                if print_yes: 
                    print "Result: SCRATCHED at ", to_datetime(times[i]), " because market went "
                    print "The current market is: ", [mktprcs[i], oppprcs[i]], [mktqtys[i], oppqtys[i]], " remaining qty: ",  
                    print "At %s, %d, the current result is: (1) won %d, (2) lost %d, (3) scratched %d, (4) working %d, (5) filled %d \n"%(to_datetime(times[i]), last_traded_price, qty_won, qty_lost, qty_scratch, qty_working, qty_filled)
                    print "The current tally: (1) will_scratch %d, (2) minqty_trigs %d, (3) streaker_trigs %d, (4) streakerpct_trigs %d \n" %(will_scratch, minqty_trigs, streaker_trigs, streakerpct_trigs), "\n\n"        
                    
                results.append([to_datetime(times[i]), scratchdirection, code_int, minqty, streakerqty, streakerpct, last_traded_price, qty_working, 
                                qty_filled, qty_scratch, qty_won, qty_lost, minqty_trigs, streaker_trigs, streakerpct_trigs])
                curr_pos = 0; qty_filled = 0; qty_scratch = 0; qty_won = 0; qty_lost = 0; minqty_trigs = 0; streaker_trigs = 0; streakerpct_trigs = 0;
                order.working = trade_size
                order.curr_pos = 0

            else:    
                scratch_decision = should_sprawler_scratch(order, mktqtys[i], minqty, accqtys[i], streakerqty, accpcts[i], streakerpct)
                will_scratch = scratch_decision.will_scratch
                
                if will_scratch: 
                    if print_yes: 
                        if mktqtys[i] <= (minqty + trade_size):
                            print "Sprawler will scratch at ", to_datetime(times[i]), " for price of ", traded_price, " because the minqty: ", minqty, " > ", mktqtys[i]
                            print "The current market is: ", [mktprcs[i], oppprcs[i]], [mktqtys[i], oppqtys[i]], " remaining qty: "
                        elif (accqtys[i] >= streakerqty and accpcts[i] >= streakerpct):
                            print "Sprawler will scratch at ", to_datetime(times[i]), " for price of ", traded_price, " because the streakerqty: ", streakerqty, " > " , accqtys[i], " or streakerpct: ", streakerpct, " > ", accpcts[i]
                            print "The current market is: ", [mktprcs[i], oppprcs[i]], [mktqtys[i], oppqtys[i]], " remaining qty: "
                        print "the curr pos is ", curr_pos, traded_price, last_traded_price                    
                    
                    
                    scratchresult = find_scratch_event(i, scratchdirection, mktdelay, order, timediffs, mktprcs, mktqtys, tradevolumes, tradeprices, traded_price)
                    qty_lost = scratchresult.qty_lost
                    qty_scratch = scratchresult.qty_scratch
                    if qty_scratch:
                        minqty_trigs += scratch_decision.minqty_trigs
                        streaker_trigs += scratch_decision.streaker_trigs
                        streakerpct_trigs += scratch_decision.streakerpct_trigs
                        
                    order = update_traded_prcs(traded_prices, traded_price, 0, order, -1*scratchdirection, 1)
                    qty_filled = abs(qty_lost) + abs(qty_scratch) + abs(qty_won)
                    trade_started = False
                    
                    if print_yes: 
                        print "At %s, %d, the current result is: (1) won %d, (2) lost %d, (3) scratched %d, (4) working %d, (5) filled %d \n"%(to_datetime(times[i]), last_traded_price, qty_won, qty_lost, qty_scratch, qty_working, qty_filled)
                        print "The current tally: (1) will_scratch %d, (2) minqty_trigs %d, (3) streaker_trigs %d, (4) streakerpct_trigs %d \n" %(will_scratch, minqty_trigs, streaker_trigs, streakerpct_trigs), "\n\n"        
                        print "The current market is: ", [mktprcs[i], oppprcs[i]], [mktqtys[i], oppqtys[i]], " remaining qty: "
                    
                    results.append([to_datetime(times[i]), scratchdirection, code_int, minqty, streakerqty, streakerpct, last_traded_price, qty_working, 
                                    qty_filled, qty_scratch, qty_won, qty_lost, minqty_trigs, streaker_trigs, streakerpct_trigs])
                    curr_pos = 0; qty_filled = 0; qty_scratch = 0; qty_won = 0; qty_lost = 0; minqty_trigs = 0; streaker_trigs = 0; streakerpct_trigs = 0;
                    order.working = trade_size
                    order.curr_pos = 0            
                else:
                    continue
        else:
            continue
    
    if order.curr_pos:
        qty_scratch += order.curr_pos

        results.append([to_datetime(times[i]), scratchdirection, code_int, minqty, streakerqty, streakerpct, last_traded_price, qty_working, 
                        qty_filled, qty_scratch, qty_won, qty_lost, minqty_trigs, streaker_trigs, streakerpct_trigs])    

    return results

def eval_sprawler_pnl_grid(df, long side, long trade_size, ndarray minqtys, ndarray streakerqtys, ndarray streakerpcts, long time_threshold, long code_int, double shrink_pct):
    cdef:
        long i, j, n = df.shape[0], minqty, streakerqty
        long n_minqtys = minqtys.shape[0], n_streakerqtys = streakerqtys.shape[0], n_streakerpcts = streakerpcts.shape[0]
        long[:] times = df.timestamp.values
        double[:] timediffs = df.timediff.values
        double streakerpct
        long scratchdirection
        
        # turn data
        long[:] bid_turns = df.bid_turn.values
        long[:] ask_turns = df.ask_turn.values
        long[:] delta_bidqty0 = df.delta_bidqty0.values
        long[:] delta_askqty0 = df.delta_askqty0.values

        # market data
        long[:] bidprcs = df.bidprc0.values
        long[:] bidqtys = df.bidqty0.values
        long[:] askprcs = df.askprc0.values
        long[:] askqtys = df.askqty0.values
        #long[:] ordersizes = df.trade_size.values
        
        # accumulated volume data
        long[:] buyprices = df.buyprice.values
        long[:] sellprices = df.sellprice.values
        long[:] buyvolumes = df.buyvolume.values
        long[:] sellvolumes = df.sellvolume.values
        long[:] acc_buyqtys = df.acc_buyqty.values
        double[:] acc_buypcts = df.acc_buypct.values
        long[:] acc_sellqtys = df.acc_sellqty.values
        double[:] acc_sellpcts = df.acc_sellpct.values
        
        # results
        #SprawlerResult bidresult, askresult
        # results
        list result_i=[], result=[]

    for streakerqty in streakerqtys:
        for streakerpct in streakerpcts:
            for minqty in minqtys:
                #print side, code_int, minqty, streakerqty, streakerpct
                if side == -1:
                    result_i = eval_sprawler_pnl(side, code_int, minqty, streakerqty, streakerpct, time_threshold, sellvolumes, sellprices, delta_bidqty0,
                                  bid_turns, times, timediffs, bidprcs, bidqtys, askprcs, askqtys, acc_sellqtys, acc_sellpcts, trade_size, 0, 0, shrink_pct)
                elif side == 1:
                    result_i = eval_sprawler_pnl(side, code_int, minqty, streakerqty, streakerpct, time_threshold, buyvolumes, buyprices, delta_askqty0,
                                  ask_turns, times, timediffs, askprcs, askqtys, bidprcs, bidqtys, acc_buyqtys, acc_buypcts, trade_size, 0, 0, shrink_pct)
                else:
                    print "Ain't no side given."
                    return

                result.append(result_i)

    return result


def assign_trade_volumes(df):
    """
    0: buyvolume
    1: buyprice
    2: sellvolume
    3: sellprice

    """

    cdef:
        long tlen = df.shape[0], i = 0, j = 0
        long[:] bidprcs = df.bidprc0.values
        long[:] askprcs = df.askprc0.values
        long[:] tradevolumes = df.tradeQty.values
        long[:] tradeprices = df.tradeprice.values

        long buyprice, sellprice

        ndarray[long, ndim=2] trades = zeros((tlen, 4), dtype=long)

    for i in range(tlen):    
        if tradevolumes[i] == 0:
            continue
        else:
            if tradeprices[i] >= askprcs[i]: #buys
                trades[i, 0] = tradeprices[i]
                trades[i, 1] = tradevolumes[i]                
            elif tradeprices[i] <= bidprcs[i]: #sells
                trades[i, 2] = tradeprices[i]
                trades[i, 3] = tradevolumes[i]
            else:
                continue
    
    return trades


def compute_streaker(df, long window_size, long sym_int):
    """
    streaker_sides(nkdf, double window_size, int sym_int)

    all: 0
    nk: 1
    mnk: 2
    snk: 3

    results:
        0 := buys_at_bid
        1 := buys_above_bid
        2 := sells_at_ask
        3 := sells_below_ask
        4 := buys_vs_ask
        5 := sells_vs_bid
        6 := vwap_buys
        7 := vwap_sells
    """
    cdef:
        long tlen = df.shape[0], i = 0, j = 0
        long[:] times = df.timestamp.values
        long[:] buyprices = df.buyprice.values
        long[:] sellprices = df.sellprice.values
        long[:] buyvolumes = df.buyvolume.values
        long[:] sellvolumes = df.sellvolume.values
        #long[:] sym_ints = df.sym_int.values

        long buyprice, sellprice
        long prcqty_buys, prcqty_sells, qty_buys, qty_sells, prcqty_tot, qty_tot
        long count_buys, count_sells
        ndarray[long, ndim=2] streaker_results = zeros((tlen, 4), dtype=long)
        ndarray[double, ndim=2] vwap_results = zeros((tlen, 3), dtype=float64)

    for i in range(tlen):
        j = i

        prcqty_buys, prcqty_sells, qty_buys, qty_sells = 0, 0, 0, 0
        prcqty_tot, qty_tot = 0, 0
        count_buys, count_sells = 0, 0

        #if buyvolumes[i] > 0 or sellvolumes[i] > 0:
        buyprice, sellprice = buyprices[i], sellprices[i]
        # climb back to find the start of the window
        while j >= 0 and times[i] - times[j] <= window_size:
            # keep summing trades?
            #if buyvolumes[j] > 0 and buyprices[j] == buyprice:
            if buyvolumes[j] > 0:
                qty_buys += buyvolumes[j]
                prcqty_buys += buyvolumes[j] * buyprices[j]
                count_buys += 1
            #if sellvolumes[j] > 0 and sellprices[j] == sellprice:
            if sellvolumes[j] > 0:
                prcqty_sells += sellvolumes[j] * sellprices[j]
                qty_sells += sellvolumes[j]
                count_sells += 1
            j -= 1

        streaker_results[i, 0] = qty_buys
        streaker_results[i, 1] = qty_sells
        streaker_results[i, 2] = count_buys
        streaker_results[i, 3] = count_sells
        
        if qty_buys > 0:
            vwap_results[i, 0] = <double>prcqty_buys / <double>qty_buys
            prcqty_tot += prcqty_buys
            qty_tot += qty_buys
        if qty_sells > 0:
            vwap_results[i, 1] = <double>prcqty_sells / <double>qty_sells
            prcqty_tot += prcqty_sells
            qty_tot += qty_sells
        if qty_tot > 0:
            vwap_results[i, 2] = <double>prcqty_tot / <double>qty_tot
            
            
    return streaker_results, vwap_results


