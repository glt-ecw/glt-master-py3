cimport cython
from glt.math.stats import *

from numpy cimport ndarray
from numpy import float64, int64, zeros, logical_and, logical_or, unique, sign, around

from pandas import DataFrame, to_datetime

from libc.math cimport sqrt, exp, pow

cdef inline double _max(double a, double b): return a if a >= b else b
cdef inline double _min(double a, double b): return a if a <= b else b

cdef long INTMIN = -2147483648
cdef long INTMAX = 2147483647

cdef struct MarketLevel:
    long price
    long qty

@cython.boundscheck(False)
cdef void clear_marketlevel_arrays(MarketLevel[:, :] bids, MarketLevel[:, :] asks, int levels, int num_symbols) nogil:
    cdef int i, j
    for i in range(levels):
        for j in range(1, num_symbols+1):
            bids[i, j].price = INTMIN
            asks[i, j].price = INTMAX
            bids[i, j].qty = 0
            asks[i, j].qty = 0
    return

def book_building(df, int levels, int num_symbols):
    cdef:
        int j, askidx = 2*levels, tradeidx = 4*levels
        long tlen = df.shape[0], i, sym, sym_i, bidprice, askprice, buyvolume, sellvolume
        
        list tradecol = ['buyprice', 'buyvolume', 'sellprice', 'sellvolume']
        list newcol = [ba + pq + str(i) for ba in ('bid', 'ask') for i in range(levels) for pq in ('prc', 'qty')] + tradecol
        
        long[:] syms = df.sym_int.values
        long[:, :] bidprcs = df.loc[:, ['bidprc' + str(j) for j in range(levels)]].values
        long[:, :] bidqtys = df.loc[:, ['bidqty' + str(j) for j in range(levels)]].values
        long[:, :] askprcs = df.loc[:, ['askprc' + str(j) for j in range(levels)]].values
        long[:, :] askqtys = df.loc[:, ['askqty' + str(j) for j in range(levels)]].values
        long[:, :] trades = df.loc[:, tradecol].values
        MarketLevel[:, :] bids = ndarray((levels, num_symbols+1), dtype=[('price', int64), ('qty', int64)])
        MarketLevel[:, :] asks = ndarray((levels, num_symbols+1), dtype=[('price', int64), ('qty', int64)])
        
        MarketLevel mkt
        
        list bid_list, ask_list
        dict bid_data = {}, ask_data = {}
        
        bint any_changes
        
        ndarray[long, ndim=2] results = zeros((tlen, (levels + 1) * 4), dtype=long)
        
    clear_marketlevel_arrays(bids, asks, levels, num_symbols)
    
    for i in range(tlen):
        any_changes = False
        sym = syms[i]
        buyvolume, sellvolume = trades[i, 1], trades[i, 3]
        for j in range(levels):
            if bidprcs[i, j] != bids[j, sym].price:
                bids[j, sym].price = bidprcs[i, j]
                any_changes = True
            if askprcs[i, j] != asks[j, sym].price:
                asks[j, sym].price = askprcs[i, j]
                any_changes = True
        if any_changes:
            bid_data.clear()
            ask_data.clear()
            for sym_i in range(1, num_symbols+1):
                if sym_i == sym:
                    for j in range(levels):
                        bids[j, sym].qty = bidqtys[i, j]
                        asks[j, sym].qty = askqtys[i, j]
                for j in range(levels):
                    mkt = bids[j, sym_i]
                    if mkt.price not in bid_data:
                        bid_data[mkt.price] = mkt.qty
                    else:
                        bid_data[mkt.price] += mkt.qty
                    mkt = asks[j, sym_i]
                    if mkt.price not in ask_data:
                        ask_data[mkt.price] = mkt.qty
                    else:
                        ask_data[mkt.price] += mkt.qty
            bid_list = bid_data.keys()
            bid_list.sort(reverse=True)
            ask_list = ask_data.keys()
            ask_list.sort()
        else:
            for j in range(levels):
                bid_data[bidprcs[i, j]] += bidqtys[i, j] - bids[j, sym].qty
                ask_data[askprcs[i, j]] += askqtys[i, j] - asks[j, sym].qty
                bids[j, sym].qty = bidqtys[i, j]
                asks[j, sym].qty = askqtys[i, j]
        for j in range(levels):
            bidprice, askprice = bid_list[j], ask_list[j]
            results[i, 2*j] = bidprice
            results[i, 2*j + 1] = bid_data[bidprice]
            results[i, 2*j + askidx] = askprice
            results[i, 2*j + 1 + askidx] = ask_data[askprice]
        if buyvolume > 0 or sellvolume > 0:
            results[i, tradeidx] = trades[i, 0]
            results[i, tradeidx + 1] = buyvolume
            results[i, tradeidx + 2] = trades[i, 2]
            results[i, tradeidx + 3] = sellvolume
    return DataFrame(results, index=df.index, columns=newcol)

def unique_side(nkdf, side):
    cdef:
        long tlen = nkdf.shape[0], i, tick = 0
        long[:] mktprc
        ndarray[long, ndim=1] results = zeros(tlen, dtype=long)   
    if side > 0:
        mktprc = nkdf.bidprc0.values #.astype(long)
    else:
        mktprc = nkdf.askprc0.values #.astype(long)
    for i in range(tlen):
        if i == 0:
            continue
        if mktprc[i] != mktprc[i-1]:
            tick += 1
        results[i] = tick
    return results

def sniper_streaker_sides(nkdf, long window_size, long sym_int, long tick_increment=1, 
                          long ignore_sym=0, long ignore_qty=0):
    """
    streaker_sides(nkdf, double window_size, int sym_int)
    
    all: 0
    nk: 1
    mnk: 2
    snk: 3
    
    results:
        0 := buys_at_bid
        1 := buys_above_bid
        2 := sells_at_ask
        3 := sells_below_ask
        4 := buys_vs_ask
        5 := sells_vs_bid
        6 := vwap_buys
        7 := vwap_sells
    """
    cdef:
        long tlen = nkdf.shape[0], i, j, window_start
        long[:] times = nkdf.timestamp.values
        long[:] bidprc = nkdf.bidprc0.values
        long[:] askprc = nkdf.askprc0.values
        long[:] buyprices = nkdf.buyprice.values
        long[:] sellprices = nkdf.sellprice.values
        long[:] buyvolumes = nkdf.buyvolume.values
        long[:] sellvolumes = nkdf.sellvolume.values
        long[:] sym_ints = nkdf.sym_int.values

        long bid_ref, ask_ref, buys_vs_ask, sells_vs_bid
        long prcqty_buys, prcqty_sells, qty_buys, qty_sells, prcqty_tot, qty_tot
        long count_buys, count_sells
        ndarray[long, ndim=2] streaker_results = zeros((tlen, 4), dtype=int64)
        ndarray[double, ndim=2] vwap_results = zeros((tlen, 3), dtype=float64)
        bint valid_bid, valid_ask

    for i in range(tlen):
        j = i
        buys_vs_ask, sells_vs_bid = 0, 0
        count_buys, count_sells = 0, 0
        prcqty_buys, prcqty_sells, qty_buys, qty_sells = 0, 0, 0, 0
        prcqty_tot, qty_tot = 0, 0

        bid_ref = bidprc[i]
        ask_ref = askprc[i]
        # e.g. if tick_increment == 10 and bid_ref == 17555, will modify bid_ref to 17550
        if bid_ref%tick_increment > 0:
            bid_ref -= bid_ref%tick_increment
        if ask_ref%tick_increment > 0:
            ask_ref += ask_ref%tick_increment
        
        valid_bid, valid_ask = True, True
        # climb back to find the start of the window
        while j>=0 and times[i]-times[j]<=window_size:
            if (sym_int>0 and sym_int!=sym_ints[j]) or (ignore_sym==sym_ints[j] and ignore_qty>0 and (buyvolumes[j]>=ignore_qty or sellvolumes[j]>=ignore_qty)):
                j -= 1
                continue
            # keep summing trades? don't do it if market is worse in past
            if bidprc[j]<bid_ref:
                valid_bid = False
            if askprc[j]>ask_ref:
                valid_ask = False

            # buying streak against or through offer
            if valid_ask and buyprices[j]>=ask_ref:
                buys_vs_ask += buyvolumes[j]
                prcqty_buys += buyvolumes[j] * buyprices[j]
                qty_buys += buyvolumes[j]
                if buyvolumes[j]:
                    count_buys += 1
            # selling streak against or through bid
            if valid_bid and sellprices[j]<=bid_ref:
                sells_vs_bid += sellvolumes[j]
                prcqty_sells += sellvolumes[j] * sellprices[j]
                qty_sells += sellvolumes[j]
                if sellvolumes[j]:
                    count_sells += 1
            j -= 1
        streaker_results[i, 0] = buys_vs_ask
        streaker_results[i, 1] = sells_vs_bid
        streaker_results[i, 2] = count_buys
        streaker_results[i, 3] = count_sells
        if qty_buys > 0:
            vwap_results[i, 0] = <double>prcqty_buys / <double>qty_buys
            prcqty_tot += prcqty_buys
            qty_tot += qty_buys
        if qty_sells > 0:
            vwap_results[i, 1] = <double>prcqty_sells / <double>qty_sells
            prcqty_tot += prcqty_sells
            qty_tot += qty_sells
        if qty_tot > 0:
            vwap_results[i, 2] = <double>prcqty_tot / <double>qty_tot
    return streaker_results, vwap_results


def ew_top_market(nkdf, double w, bint use_unique_qtys, double start_dev=1):
    cdef:
        long tlen = nkdf.shape[0], i, j
        long[:] bidprc0 = nkdf.bidprc0.values
        long[:] askprc0 = nkdf.askprc0.values
        long[:] bidprc1 = nkdf.bidprc1.values
        long[:] askprc1 = nkdf.askprc1.values
        long[:] bidqty0 = nkdf.bidqty0.values
        long[:] askqty0 = nkdf.askqty0.values
        long[:] bidqty1 = nkdf.bidqty1.values
        long[:] askqty1 = nkdf.askqty1.values
        ndarray[double, ndim=2] results = zeros((tlen, 12), dtype=float64)
        double bidqty0_mean, bidqty1_mean, askqty0_mean, askqty1_mean
        double bidqty0_var, bidqty1_var, askqty0_var, askqty1_var
   
    for i in range(tlen):
        if i == 0:
            results[i, 0] = <double>bidqty0[i] #weighted means
            results[i, 1] = <double>bidqty1[i] 
            results[i, 2] = <double>askqty0[i] 
            results[i, 3] = <double>askqty1[i] 
            results[i, 4] = start_dev * start_dev #weighted variance
            results[i, 5] = start_dev * start_dev 
            results[i, 6] = start_dev * start_dev 
            results[i, 7] = start_dev * start_dev 
            results[i, 8] = start_dev #zscore seed
            results[i, 9] = start_dev #zscore seed
            results[i, 10] = start_dev #zscore seed
            results[i, 11] = start_dev #zscore seed
            continue
        # is the market same as the previous?
        # bid
        if bidprc0[i] == bidprc0[i-1]:
            bidqty0_mean = results[i-1, 0]
            bidqty0_var = results[i-1, 4]
        elif bidprc0[i] == bidprc1[i-1]:
            bidqty0_mean = results[i-1, 1]
            bidqty0_var = results[i-1, 5]
        else:
            bidqty0_mean = <double>bidqty0[i]
            bidqty0_var = start_dev * start_dev

        if bidprc1[i] == bidprc1[i-1]:
            bidqty1_mean = results[i-1, 1]
            bidqty1_var = results[i-1, 5]
        elif bidprc1[i] == bidprc0[i-1]:
            bidqty1_mean = results[i-1, 0]
            bidqty1_var = results[i-1, 4]
        else:
            bidqty1_mean = <double>bidqty1[i]
            bidqty1_var = start_dev * start_dev

        # ask
        if askprc0[i] == askprc0[i-1]:
            askqty0_mean = results[i-1, 2]
            askqty0_var = results[i-1, 6]
        elif askprc0[i] == askprc1[i-1]:
            askqty0_mean = results[i-1, 3]
            askqty0_var = results[i-1, 7]
        else:
            askqty0_mean = <double>askqty0[i]
            askqty0_var = start_dev * start_dev

        if askprc1[i] == askprc1[i-1]:
            askqty1_mean = results[i-1, 3]
            askqty1_var = results[i-1, 7]
        elif askprc1[i] == askprc0[i-1]:
            askqty1_mean = results[i-1, 2]
            askqty1_var = results[i-1, 6]
        else:
            askqty1_mean = <double>askqty1[i]
            askqty1_var = start_dev * start_dev

        # save results; if different from previous book: update; else: keep last
        # _var are previous variances
        if (bidprc0[i] == bidprc0[i-1] and (not use_unique_qtys or (use_unique_qtys and bidqty0[i] != bidqty0[i-1]))):
            results[i, 0] = ew_mean(w, <double>bidqty0[i], bidqty0_mean)
            results[i, 4] = ew_var(w, <double>bidqty0[i], bidqty0_var, results[i, 0])
        else:
            results[i, 0] = bidqty0_mean
            results[i, 4] = bidqty0_var

        if (bidprc1[i] == bidprc1[i-1] and (not use_unique_qtys or (use_unique_qtys and bidqty1[i] != bidqty1[i-1]))):
            results[i, 1] = ew_mean(w, <double>bidqty1[i], bidqty1_mean)
            results[i, 5] = ew_var(w, <double>bidqty1[i], bidqty1_var, results[i, 1])
        else:
            results[i, 1] = bidqty1_mean
            results[i, 5] = bidqty1_var

        if (askprc0[i] == askprc0[i-1] and (not use_unique_qtys or (use_unique_qtys and askqty0[i] != askqty0[i-1]))):
            results[i, 2] = ew_mean(w, <double>askqty0[i], askqty0_mean)
            results[i, 6] = ew_var(w, <double>askqty0[i], askqty0_var, results[i, 2])
        else:
            results[i, 2] = askqty0_mean
            results[i, 6] = askqty0_var

        if (askprc1[i] == askprc1[i-1] and (not use_unique_qtys or (use_unique_qtys and askqty1[i] != askqty1[i-1]))):
            results[i, 3] = ew_mean(w, <double>askqty1[i], askqty1_mean)
            results[i, 7] = ew_var(w, <double>askqty1[i], askqty1_var, results[i, 3])
        else:
            results[i, 3] = askqty1_mean
            results[i, 7] = askqty1_var

        for j in range(4):
            results[i, j + 4] = _max(start_dev * start_dev, results[i, j + 4])

        results[i, 8] = 0 if results[i, 4] == 0 else (<double>bidqty0[i] - results[i, 0]) / sqrt(results[i, 4])
        results[i, 9] = 0 if results[i, 5] == 0 else (<double>bidqty1[i] - results[i, 1]) / sqrt(results[i, 5])
        results[i, 10] = 0 if results[i, 6] == 0 else (<double>askqty0[i] - results[i, 2]) / sqrt(results[i, 6])
        results[i, 11] = 0 if results[i, 7] == 0 else (<double>askqty1[i] - results[i, 3]) / sqrt(results[i, 7])

    return results



##############################################
#
# scoring
#
##############################################

def find_turns(df, long minqty, long tick):
    """
    find turns that turn with a minimum of minqty
    """
    cdef:
        long tlen = df.shape[0], i = 0, prev_bid = 0, prev_ask = 0
        long[:] bidqty = df.bidqty0.values
        long[:] bidprc = df.bidprc0.values
        long[:] askprc = df.askprc0.values
        long[:] askqty = df.askqty0.values
        ndarray[long, ndim=2] results = zeros([tlen, 2], dtype=long)
    
    for i in range(tlen):
        if i == 0:
            prev_bid = bidprc[i]
            prev_ask = askprc[i]
            continue
        elif (bidprc[i] == 0 and askprc[i] == 0) or (askprc[i] - bidprc[i]) > tick:
            continue
        elif bidprc[i] >= prev_ask and (bidqty[i] >= minqty or bidprc[i] - prev_ask > tick):
            if bidprc[i] - prev_bid > 0:
                results[i, 0] = bidprc[i] - prev_bid
                results[i, 1] = bidprc[i]
            prev_ask = askprc[i]
            prev_bid = bidprc[i]
        elif askprc[i] <= prev_bid and (askqty[i] >= minqty or prev_bid - askprc[i] > tick):
            if askprc[i] - prev_ask < 0:
                results[i, 0] = askprc[i] - prev_ask
                results[i, 1] = askprc[i]
            prev_bid = bidprc[i]
            prev_ask = askprc[i]
        elif askprc[i] <= prev_ask and not askprc[i] <= prev_bid:
            prev_ask = askprc[i]
        elif bidprc[i] >= prev_bid and not bidprc[i] >= prev_ask:
            prev_bid = bidprc[i]
    return results

def find_weak_turns(df, long minqty_base, long minqty_multiple):
    """
    weak turn is classified as the book is choice and the bid or offer retracts
    """
    cdef:
        long tlen = df.shape[0], i, minqty = minqty_base * minqty_multiple
        long[:] bidqty = df.bidqty0.values
        long[:] bidprc = df.bidprc0.values
        long[:] askprc = df.askprc0.values
        long[:] askqty = df.askqty0.values
        ndarray[long, ndim=2] results = zeros([tlen, 2], dtype=long)
    
    for i in range(tlen):
        if i == 0 or bidprc[i] >= askprc[i]:
            continue
        elif bidprc[i-1] == askprc[i-1]:
            if bidqty[i] >= minqty and askprc[i] > askprc[i-1]:
                results[i, 0] = askprc[i] - askprc[i-1]
                results[i, 1] = bidprc[i]
            elif askqty[i] >= minqty and bidprc[i] < bidprc[i-1]:
                results[i, 0] = bidprc[i] - bidprc[i-1]
                results[i, 1] = askprc[i]
    return results

def additional_turn(df, long min_ticks):
    """
    "min_ticks" : number of consecutive ticks in 1 direction 
    """
    cdef:
        long tlen = df.shape[0], i, j, tick_count
        long[:] times = df.timestamp.values
        long[:] turn_direction = df.turn_direction.values
        long[:] turn_price = df.turn_price.values
        ndarray[long, ndim=2] results = zeros([tlen, 3], dtype=long)
        
    for i in range(tlen):
        if turn_direction[i] == 0:
            continue
        tick_count = 0
        j = i + 1
        while j < tlen and tick_count < min_ticks:
            if (turn_direction[i] > 0 and turn_direction[j] > 0) or (turn_direction[i] < 0 and turn_direction[j] < 0):
                tick_count += 1
            elif (turn_direction[i] > 0 and turn_direction[j] < 0) or (turn_direction[i] < 0 and turn_direction[j] > 0):
                break
            j += 1
        if tick_count >= min_ticks:
            #if turn_direction[i] > 0:
            #else:
            #    results[i][0] = -1 * total_ticks
            
            results[i, 0] = turn_price[j - 1] - turn_price[i]
            results[i, 1] = times[j - 1] - times[i]
            results[i, 2] = turn_price[j - 1]
    return results

def score_trades(nkdf, long lookback_window, bint ticked_again):
    """
    score_turns(nkdf, long streaker_threshold, long qtylean_threshold, long time_ahead, bint hard_tick, bint overwrite_negatives)

    results:
        0 := buy score
        1 := sell score
        2 := market direction
    """
    cdef:
        long tlen = nkdf.shape[0], i, j, k, direction, buyscore, sellscore
        long[:] times = nkdf.timestamp.values
        long[:] turn_direction = nkdf.turn_direction.values
        long[:] turn_price = nkdf.turn_price.values
        long[:] additional_turn_direction = nkdf.additional_turn_direction.values
        long[:] unique_tick = nkdf.unique_tick.values
        long[:] buyprices = nkdf.buyprice.values
        long[:] buyvolume = nkdf.buyvolume.values
        long[:] sellprices = nkdf.sellprice.values
        long[:] sellvolume = nkdf.sellvolume.values
        ndarray[long, ndim=2] results = zeros([tlen, 7], dtype=long)

    for i in range(tlen):
        if (not ticked_again and turn_direction[i] > 0) or (ticked_again and additional_turn_direction[i] > 0):
            direction = 1
        elif (not ticked_again and turn_direction[i] < 0) or (ticked_again and additional_turn_direction[i] < 0):
            direction = -1
        else:
            continue
        j = i
        while j > 0 and times[i] - times[j] <= lookback_window:
            #if (direction > 0 and turn_direction[j] < 0) or (direction < 0 and turn_direction[j] > 0):
            if j < i and turn_direction[j] != 0:
                break
            buyscore, sellscore = 0, 0
            if buyvolume[j] > 0 and buyprices[j] == turn_price[i]: # == buyprices[j]:
                buyscore = direction * (times[i] - times[j])
            elif sellvolume[j] > 0 and sellprices[j] == turn_price[i]: #== sellprices[j]:
                sellscore = -1 * direction * (times[i] - times[j])
            if buyscore != 0 and results[j, 0] == 0:
                results[j, 0] = buyscore
                results[j, 1] = -1 * buyscore
            if sellscore != 0 and results[j, 1] == 0:
                results[j, 0] = -1 * sellscore
                results[j, 1] = sellscore
            j -= 1
    return results

def score_trades_beta(nkdf, int time_to_turn, bint ticked_again):
    """
    score_turns(nkdf, long streaker_threshold, long qtylean_threshold, long time_ahead, bint hard_tick, bint overwrite_negatives)

    results:
        0 := buy score
        1 := sell score
        2 := market direction
    """
    cdef:
        long tlen = nkdf.shape[0], i, direction
        long buyscore, sellscore
        long[:] times = nkdf.timestamp.values
        double[:] turn_times = nkdf.turn_dt.values
        long[:] turn_direction = nkdf.turn_direction.values
        long[:] turn_price = nkdf.turn_price.values
        long[:] additional_turn_direction = nkdf.additional_turn_direction.values
        long[:] unique_tick = nkdf.unique_tick.values
        long[:] buyprices = nkdf.buyprice.values
        long[:] buyvolume = nkdf.buyvolume.values
        long[:] sellprices = nkdf.sellprice.values
        long[:] sellvolume = nkdf.sellvolume.values
        ndarray[long, ndim=2] results = zeros([tlen, 2], dtype=long)

    for i in range(tlen):
        if (not ticked_again and turn_direction[i] > 0) or (ticked_again and additional_turn_direction[i] > 0):
            direction = 1
        elif (not ticked_again and turn_direction[i] < 0) or (ticked_again and additional_turn_direction[i] < 0):
            direction = -1
        else:
            continue
            
        buyscore, sellscore = 0, 0
        turn_time = around(turn_times[i])
        if turn_time <= time_to_turn:
            if buyvolume[i] > 0 and buyprices[i] == turn_price[i]: # == buyprices[j]:
                buyscore = direction * turn_time
            elif sellvolume[i] > 0 and sellprices[i] == turn_price[i]: #== sellprices[j]:
                sellscore = -1 * direction * turn_time
                
            if buyscore != 0 and results[i, 0] == 0:
                results[i, 0] = buyscore
                results[i, 1] = -1 * buyscore
            if sellscore != 0 and results[i, 1] == 0:
                results[i, 0] = -1 * sellscore
                results[i, 1] = sellscore
    return results


def score_market(nkdf, long lookback_window, bint ticked_again):
    """
    score_turns(nkdf, long streaker_threshold, long qtylean_threshold, long time_ahead, bint hard_tick, bint overwrite_negatives)

    results:
        0 := buy score
        1 := sell score
        2 := market direction
    """
    cdef:
        long tlen = nkdf.shape[0], i, j, k, direction, buyscore, sellscore, turn_price, additional_turn_price
        long[:] times = nkdf.timestamp.values
        long[:] turn_directions = nkdf.turn_direction.values
        long[:] turn_prices = nkdf.turn_price.values
        long[:] additional_turn_directions = nkdf.additional_turn_direction.values
        long[:] unique_ticks = nkdf.unique_tick.values
        long[:] bidprcs = nkdf.even_bidprc0.values
        long[:] askprcs = nkdf.even_askprc0.values
        ndarray[long, ndim=2] results = zeros([tlen, 7], dtype=long)

    for i in range(tlen):
        if (not ticked_again and turn_directions[i] > 0) or (ticked_again and additional_turn_directions[i] > 0):
            direction = 1
        elif (not ticked_again and turn_directions[i] < 0) or (ticked_again and additional_turn_directions[i] < 0):
            direction = -1
        else:
            continue
        #if turn_direction != 0:
        #    turn_price = turn_prices[i]
        #if additional_turn_direction != 0:
        #    additional_turn_price = additional_turn_prices[i]
        j = i
        while j > 0 and times[i] - times[j] <= lookback_window:
            if j < i and turn_directions[j] != 0:
                break
            buyscore, sellscore = 0, 0
            if direction == 1 and askprcs[j] <= turn_prices[i]:
                buyscore = direction * (times[i] - times[j])
            elif direction == -1 and bidprcs[j] >= turn_prices[i]:
                sellscore = -1 * direction * (times[i] - times[j])
            if buyscore != 0 and results[j, 0] == 0:
                results[j, 0] = buyscore
                results[j, 1] = -1 * buyscore
            if sellscore != 0 and results[j, 1] == 0:
                results[j, 0] = -1 * sellscore
                results[j, 1] = sellscore
            j -= 1
    return results

def score_trades_getout(nkdf, long price, long minleanqty, long direction, long next_avol_t):
    """
    score_turns(nkdf, long streaker_threshold, long qtylean_threshold, long time_ahead, bint hard_tick, bint overwrite_negatives)
    direction := 
        1 := long from price
        -1 := short from price
    results:
        0 := score
        1 := direction
        #2 := market direction

    # Find interval md interval of interest
    
    ## Start: 1 ms after trigger time
    ## Evaluate turn direction (if turns against us, mark POSITIVE EVENT & evaluate exit point.
    ## End: Earliest of the following times.
    ### After trigger time + 1ms...(triggered by volume events)
    ### 1. (lean market < buffer qty) and (zscore < 1 and book weakens and volume traded against book) ##lean market is weakening. get out of trade
    ### 2. lean market < buffer qty (wait for next volume event against book)
    ### 3. Market goes in our favor, take all positive events till you get to initial trade price
    ### 4. If inital trade is against us, trade up to 1 tick loss (treat as normal) OR if goes in favor, take positive event up to orig trade price
    ### 5. Next avol trade going the other way
    ## Mark interval between Start & End as NEGATIVE EVENTS.  (Randomly choose # from set)        
    """
    cdef:
        long tlen = nkdf.shape[0], i, j=0, ix, t0, t1, getout_price, buyscore, sellscore
        long[:] triggerids = nkdf.triggerid.values #need this to identify specific md event
        long[:] times = nkdf.timestamp.values
        long[:] turn_direction = nkdf.turn_direction.values
        long[:] turn_price = nkdf.turn_price.values
        long[:] additional_turn_direction = nkdf.additional_turn_direction.values
        long[:] unique_tick = nkdf.unique_tick.values
        long[:] buyprices = nkdf.buyprice.values
        long[:] buyvolumes = nkdf.buyvolume.values
        long[:] sellprices = nkdf.sellprice.values
        long[:] sellvolumes = nkdf.sellvolume.values
        long[:] bidqtys = nkdf.bidqty0.values
        long[:] bidprcs = nkdf.bidprc0.values 
        long[:] askqtys = nkdf.askqty0.values
        long[:] askprcs = nkdf.askprc0.values
        long[:] trade_volumes
        long curr_price = price
        double[:] bidzscores = nkdf.mov_2pct_bidqty0_10_z.values
        double[:] askzscores = nkdf.mov_2pct_askqty0_10_z.values        
        double zscore_thresh = 0.0, zscore = 0.0
        long size_thresh = 0, stop_trade = 0, tick_size=10
        
    zscore_thresh = bidzscores[0] if direction == 1 else askzscores[0]

    trade_volumes = buyvolumes if direction==-1 else sellvolumes
    trade_prices = buyprices if direction==-1 else sellprices
    leanqtys = bidqtys if direction == 1 else askqtys
    leanprcs = bidprcs if direction == 1 else askprcs    
    
    results = []
    prev_price = 0
    
    for i in range(tlen):
        price_array = [leanprcs[i]] if i == 0 else price_array
        curr_price = leanprcs[i]

        if times[i] > next_avol_t:
            if not results:
                results.append([i, triggerids[i], times[0], times[i], leanprcs[i]])
            return results    

        ## check if enough size on market or if market is against us
        market_for_bool = (direction == -1 and leanprcs[i] <= price_array[-1]) or (direction == 1 and leanprcs[i] >= price_array[-1])
        market_against_bool = (direction == -1 and leanprcs[i] > price_array[-1]) or (direction == 1 and leanprcs[i] < price_array[-1])

        size_thresh = 1 if leanqtys[i] > minleanqty and curr_price == price_array[-1] else size_thresh

        zscore = bidzscores[i] if direction == 1 else askzscores[i]

        if size_thresh:
            ## market is going in your favor
            if turn_direction[i] == 1 * tick_size * direction:
                ## check if price already in array
                if curr_price != price_array[-1]:
                    price_array.append(curr_price)
                    size_thresh = 0 #reset size_thresh
                else:
                    continue

            ## market has gone against you. Find exit point
            elif (turn_direction[i] == -1 * tick_size * direction) and market_against_bool:
                j = i - 1
                while (j > 0 and ((direction == -1 and leanprcs[j] >= price_array[-1]) or (direction == 1 and leanprcs[j] <= price_array[-1]))):
                    if trade_volumes[j] and leanqtys[j] >= minleanqty and price_array[-1] == leanprcs[j]:
                        if prev_price != leanprcs[j]: ## unique consec prices
                            ## found exit point
                            results.append([j, triggerids[j], times[0], times[j], leanprcs[j]])
                            prev_price = leanprcs[j]
                        price_array.pop()
                        break
                    else:
                        j-=1

                size_thresh = 0
            else:
                continue

        ## if initial book is weak, get out if zscore < -1
        elif not size_thresh:
        
            if trade_volumes[i] and times[i] > times[0] and zscore < 0 and zscore < zscore_thresh:
                if price_array[-1] == leanprcs[i]:
                    if prev_price != leanprcs[i]:
                        results.append([i, triggerids[i], times[0], times[i], leanprcs[i]])
                        prev_price = leanprcs[i]
                    price_array.pop()
                else:
                    continue

        if not len(price_array):
            return results

    return results 


def score_trades_getout_bookupdates(nkdf, long price, long minleanqty, long direction, long next_avol_t):
    """
    Same as above, but allows for get out triggers with book updates (not just trade events)
    """
    cdef:
        long tlen = nkdf.shape[0], i, j=0, ix, t0, t1, getout_price, buyscore, sellscore
        long[:] triggerids = nkdf.triggerid.values #need this to identify specific md event
        long[:] times = nkdf.timestamp.values
        long[:] turn_direction = nkdf.turn_direction.values
        long[:] turn_price = nkdf.turn_price.values
        long[:] additional_turn_direction = nkdf.additional_turn_direction.values
        long[:] unique_tick = nkdf.unique_tick.values
        long[:] buyprices = nkdf.buyprice.values
        long[:] buyvolumes = nkdf.buyvolume.values
        long[:] sellprices = nkdf.sellprice.values
        long[:] sellvolumes = nkdf.sellvolume.values
        long[:] bidqtys = nkdf.bidqty0.values
        long[:] bidprcs = nkdf.bidprc0.values 
        long[:] askqtys = nkdf.askqty0.values
        long[:] askprcs = nkdf.askprc0.values
        long[:] trade_volumes
        long curr_price = price
        double[:] bidzscores = nkdf.mov_5pct_bidqty0_100_z.values
        double[:] askzscores = nkdf.mov_5pct_askqty0_100_z.values        
        double zscore_thresh = 0.0, zscore = 0.0
        long size_thresh = 0, stop_trade = 0, tick_size=10
        
    zscore_thresh = bidzscores[0] if direction == 1 else askzscores[0]

    trade_volumes = buyvolumes if direction==-1 else sellvolumes
    trade_prices = buyprices if direction==-1 else sellprices
    leanqtys = bidqtys if direction == 1 else askqtys
    leanprcs = bidprcs if direction == 1 else askprcs    
    
    results = []
    prev_price = 0
    
    for i in range(tlen):
        price_array = [leanprcs[i]] if i == 0 else price_array
        curr_price = leanprcs[i]

        if times[i] > next_avol_t:
            if not results:
                results.append([i, triggerids[i], times[0], times[i], leanprcs[i]])
            return results    

        ## check if enough size on market or if market is against us
        market_for_bool = (direction == -1 and leanprcs[i] <= price_array[-1]) or (direction == 1 and leanprcs[i] >= price_array[-1])
        market_against_bool = (direction == -1 and leanprcs[i] > price_array[-1]) or (direction == 1 and leanprcs[i] < price_array[-1])

        size_thresh = 1 if leanqtys[i] > minleanqty and curr_price == price_array[-1] else size_thresh

        zscore = bidzscores[i] if direction == 1 else askzscores[i]

        if size_thresh:
            ## market is going in your favor
            if turn_direction[i] == 1 * tick_size * direction:
                ## check if price already in array
                if curr_price != price_array[-1]:
                    price_array.append(curr_price)
                    size_thresh = 0 #reset size_thresh
                else:
                    continue

            ## market has gone against you. Find exit point
            elif (turn_direction[i] == -1 * tick_size * direction) and market_against_bool:
                j = i - 1
                while (j > 0 and ((direction == -1 and leanprcs[j] >= price_array[-1]) or (direction == 1 and leanprcs[j] <= price_array[-1]))):
                    if leanqtys[j] >= minleanqty and price_array[-1] == leanprcs[j]:
                        if prev_price != leanprcs[j]: ## unique consec prices
                            ## found exit point
                            results.append([j, triggerids[j], times[0], times[j], leanprcs[j]])
                            prev_price = leanprcs[j]
                        price_array.pop()
                        break
                    else:
                        j-=1

                size_thresh = 0
            else:
                continue

        ## if initial book is weak, get out if zscore < -1
        elif not size_thresh:
            if trade_volumes[i] and times[i] > times[0] and zscore < 0 and zscore < zscore_thresh:
                if price_array[-1] == leanprcs[i]:
                    if prev_price != leanprcs[i]:
                        results.append([i, triggerids[i], times[0], times[i], leanprcs[i]])
                        prev_price = leanprcs[i]
                    price_array.pop()
                else:
                    continue

        if not len(price_array):
            return results

    return results 


def score_trades_tpx(md, long price, long minleanqty, long direction, long next_trig_t, long printbool):
    """
    score_turns(nkdf, long streaker_threshold, long qtylean_threshold, long time_ahead, bint hard_tick, bint overwrite_negatives)
    direction := 
        1 := long from price
        -1 := short from price
    results:
        0 := score
        1 := direction
        #2 := market direction

    # Find interval md interval of interest
    
    ## Start: 1 ms after trigger time
    ## End: Earliest of the following times.

    ### Evaluate turn direction (if turns against us, mark POSITIVE EVENT & evaluate exit point)
    ### After trigger time + 1ms...(triggered by volume events)
    ### 1. (lean market < buffer qty) and (zscore < 1 and book weakens and volume traded against book) ##lean market is weakening. get out of trade
    ### 2. lean market < buffer qty (wait for next volume event against book)
    ### 3. Market goes in our favor, take all positive events till you get to initial trade price
    ### 4. If inital trade is against us, trade up to 1 tick loss (treat as normal) OR if goes in favor, take positive event up to orig trade price
    ### 5. 

    ## Mark interval between Start & End as NEGATIVE EVENTS.  (Randomly choose # from set)        
    """
    cdef:
        long tlen = md.shape[0], i, j=0, ix, t0, t1, getout_price, buyscore, sellscore
        long[:] triggerids = md.triggerid.values
        long[:] times = md.timestamp.values
        long[:] turn_direction = md.turn_direction.values
        long[:] turn_price = md.turn_price.values
        #long[:] additional_turn_direction = md.additional_turn_direction.values
        #long[:] unique_tick = md.unique_tick.values
        long[:] buyprices = md.buyprice.values
        long[:] buyvolumes = md.buyvolume.values
        long[:] sellprices = md.sellprice.values
        long[:] sellvolumes = md.sellvolume.values
        long[:] bidqtys = md.bidqty0.values
        long[:] bidprcs = md.bidprc0.values 
        long[:] askqtys = md.askqty0.values
        long[:] askprcs = md.askprc0.values
        double[:] bidzscores = md.mov_10pct_bidqty0_5_z.values
        double[:] askzscores = md.mov_10pct_askqty0_5_z.values         
        long[:] trade_volumes        
        long curr_price = price
        double zscore_thresh = 0.0, zscore = 0.0
        long size_thresh = 0, stop_trade = 0, tick_size=5
        
    zscore_thresh = bidzscores[0] if direction == 1 else askzscores[0]
    trade_volumes = buyvolumes if direction==-1 else sellvolumes
    trade_prices = buyprices if direction==-1 else sellprices
    ## check lean mkt size
    leanqtys = askqtys if direction == -1 else bidqtys
    leanprcs = askprcs if direction == -1 else bidprcs    
    #print(to_datetime(times[0]), bidprcs[0], askprcs[0])
    results = []
    if printbool:
        print "curr_price:%d, minleanqty=%s, direction=%s, leanqtys[0]=%s, leanprcs[0]=%s"%(curr_price, minleanqty, direction, leanqtys[0], leanprcs[0])
    prev_price = 0
    
    for i in range(tlen):
        market = 'bidprc0:%d bidqty0:%d askprc0:%d askqty0:%d'%(bidprcs[i], bidqtys[i], askprcs[i], askqtys[i])
        price_array = [leanprcs[i]] if i == 0 else price_array
        curr_price = leanprcs[i]

        if times[i] > next_trig_t:
            if not results:
                results.append([i, triggerids[i], times[0], times[i], leanprcs[i]])
            if printbool:
                print('Next trig trigger at %s'%(to_datetime(times[i])), results, "\n")
            return results    

        ## check if enough size on market or if market is against us
        market_for_bool = (direction==-1 and leanprcs[i]<=price_array[-1]) or (direction==1 and leanprcs[i]>=price_array[-1])
        market_against_bool = (direction==-1 and leanprcs[i]>price_array[-1]) or (direction==1 and leanprcs[i]<price_array[-1])

        size_thresh = 1 if leanqtys[i] > minleanqty and curr_price == price_array[-1] else size_thresh

        zscore = bidzscores[i] if direction == 1 else askzscores[i]

        if size_thresh:
            ## market is going in your favor
            if turn_direction[i] == 1 * tick_size * direction:
                if printbool:
                    print('Market turned in favor', to_datetime(times[i]), curr_price, minleanqty, direction, market, "\n")
                ## check if price already in array
                if curr_price != price_array[-1]:
                    price_array.append(curr_price)
                    size_thresh = 0 #reset size_thresh
                    if printbool:
                        print('Added price %d'%(curr_price), to_datetime(times[i]), price_array, market, "\n")
                else:
                    continue

            ## market has gone against you. Find exit point
            elif (turn_direction[i] == -1 * tick_size * direction) and market_against_bool:
                if printbool:
                    print('Market turned against', to_datetime(times[i]), curr_price, minleanqty, direction, market, "\n")
                j = i - 1
                while (j > 0 and ((direction==-1 and leanprcs[j] >= price_array[-1]) or (direction==1 and leanprcs[j] <= price_array[-1]))):
                    if trade_volumes[j] and leanqtys[j] >= minleanqty and price_array[-1] == leanprcs[j]:
                        if prev_price != leanprcs[j]: ## unique consec prices
                            ## found exit point
                            results.append([j, triggerids[j], times[0], times[j], leanprcs[j]])
                            prev_price = leanprcs[j]
                        else:
                            if printbool:
                                print('Price: %d has already traded previously'%leanprcs[i])
                        price_array.pop()
                        if printbool:
                            print('Normal mode: Subtracted price %d'%(leanprcs[j]), to_datetime(times[i]), price_array, market, "\n")
                        break
                    else:
                        j-=1

                size_thresh = 0
            else:
                continue

        ## if initial book is weak, get out if zscore < -1
        elif not size_thresh:

            if trade_volumes[i] and times[i] > times[0] and zscore < 0 and zscore < zscore_thresh:
                if printbool:
                    print('Not enough size in market', to_datetime(times[i]), curr_price, leanprcs[i], price_array[-1], minleanqty, direction, market, "\n")
                if price_array[-1] == leanprcs[i]:
                    if prev_price != leanprcs[i]:
                        if printbool:
                            print('Price: %d has been added to results'%leanprcs[i], to_datetime(times[i]))
                        results.append([i, triggerids[i], times[0], times[i], leanprcs[i]])
                        prev_price = leanprcs[i]
                    else:
                        if printbool:
                            print('Price: %d has already traded previously'%leanprcs[i], to_datetime(times[i]),)
                    price_array.pop()
                    if printbool:
                        print('Small mode: Subtracted price %d'%(leanprcs[i]), to_datetime(times[i]), price_array, market, "\n")
                else:
                    continue

        if not len(price_array):
            if printbool:
                print('No more prices', to_datetime(times[i]), price_array, market, "\n")
            return results

    return results


def no_shared_unique_ticks(table, ndarray[long, ndim=1] unique_ticks_check):
    cdef:
        long tlen = table.shape[0], ulen = unique_ticks_check.shape[0], tick
        ndarray[long, ndim=1] unique_ticks = table.unique_tick.values.astype(long)
        ndarray[long, ndim=1] results = zeros(tlen, dtype=long)
        int i, j, is_uniq
        
    for i in range(tlen):
        is_uniq = 1
        tick = unique_ticks[i]
        for j in range(ulen):
            if tick == unique_ticks_check[j]:
                is_uniq = 0
                break
        results[i] = is_uniq
    return results

