# distutils: language=c++
# cython: boundscheck=False
# cython: cdivision=True
# cython: wraparound=False
from libc.stdlib cimport atoi, atof, atol
from libc.math cimport NAN, isnan, fabs
from glt.math.helpful_functions cimport *
from glt.modeling.structs cimport MarketState, WrappedMarketState
from glt.math cimport kalman

from time import time
from numpy import zeros, float64, int64, empty
from numpy cimport ndarray
from pandas import DataFrame

cdef long INTMIN = -2147483648
cdef long INTMAX = 2147483647


cdef double double_within(double value, double lower, double upper) nogil:
    return double_gte(value, lower) and double_lte(value, upper)


cdef size_t arrcycle_push(double[:] arr, size_t n, size_t head, double val) nogil:
    arr[head] = val
    if head+1 > n:
        return 0
    else:
        return head+1


def now():
    cdef double t = time()
    return <long>(1000000000*t)


cdef class SpotManager:
    def __cinit__(self, double tick_width, long min_size, long pause_window):
        self.tick_width = tick_width
        self.min_size = min_size
        self.pause_window = pause_window
        self.ts = 0
        self.seqnum = 0
        self.bid = 0
        self.ask = 0
        self.prc = 0
        self.bidqty = 0
        self.askqty = 0
        self.good = False
        self.wide = False
        self.last_good_ts = 0
        self.new_tick = True
        
    cdef void update_md(self, long ts, long seqnum, double bidprc, long bidqty, double askprc, long askqty) nogil:
        if not double_eq(bidprc, self.bid) or not double_eq(askprc, self.ask):
            self.good = False
            self.new_tick = True
        elif self.new_tick:
            self.new_tick = False
            
        self.seqnum = seqnum
        self.bid = bidprc
        self.ask = askprc
        self.prc = 0.5 * (bidprc + askprc)
        self.bidqty = bidqty
        self.askqty = askqty
        self.wide = double_gt(askprc - bidprc, self.tick_width)
        
        if not self.wide and bidqty >= self.min_size and askqty >= self.min_size:
            self.ts = ts
            if not self.good:
                self.last_good_ts = ts
            self.good = True
        else:
            self.good = False
            
    cdef bint _is_usable(self, long ts) nogil:
        return self.good and ts - self.last_good_ts > self.pause_window
    
    def is_usable(self, long ts):
        return self._is_usable(ts)
    
    cdef void _update_marketstate(self, long ts, long seqnum, MarketState *mkt) nogil:
        self.update_md(ts, seqnum, mkt.bidprc0, mkt.bidqty0, mkt.askprc0, mkt.askqty0)
    
    def update_marketstate(self, long ts, long seqnum, WrappedMarketState w):
        self._update_marketstate(ts, seqnum, w.mkt)
        
    def update_deio_md(self, long ts, tuple md):
        self.update_md(ts, md[2], md[14], <long>md[15], md[16], <long>md[17])

    def force_values(self, double price, long last_good_ts, bint good, bint new_tick):
        self.prc = price
        self.last_good_ts = last_good_ts
        self.new_tick = new_tick
        if self.new_tick:
            self.good = False
        else:
            self.good = good

    def as_dict(self):
        return {
            'bid': self.bid, 
            'ask': self.ask, 
            'bidqty': self.bidqty,
            'askqty': self.askqty,
            'mid': self.prc, 
            'wide': self.wide, 
            'ts': self.ts, 
            'seqnum': self.seqnum, 
            'good': self.good
        }


cdef class SuperSpotManager:
    def __cinit__(self, double tick_width, long min_size, long pause_window):
        self.tick_width = tick_width
        self.min_size = min_size
        self.pause_window = pause_window
        self.seqnum = 0
        self.bid = 0
        self.ask = 0
        self.prc = NAN
        self.bidqty = 0
        self.askqty = 0
        self.bidpar = 0
        self.askpar = 0
        self.good = False
        self.last_good_ts = 0
        self.last_turn_tradeprc = NAN
        self.last_turn_ts = 0
        
    cdef void update_md(self, long ts, long seqnum, double bidprc, long bidqty, double askprc, long askqty, 
            long bidpar, long askpar,
            double buyprice, long buyvolume, double sellprice, long sellvolume) nogil:
        cdef bint has_traded = False
        if not double_eq(bidprc, self.bid) or not double_eq(askprc, self.ask):
            self.good = False
            if buyvolume > 0:
                self.last_turn_tradeprc = buyprice
                has_traded = True
            if sellvolume > 0:
                self.last_turn_tradeprc = sellprice
                has_traded = True
            if has_traded:
                self.last_turn_ts = ts

        self.seqnum = seqnum
        self.bid = bidprc
        self.ask = askprc
        self.bidqty = bidqty
        self.askqty = askqty
        self.bidpar = bidpar
        self.askpar = askpar
        
        if double_lte(askprc - bidprc, self.tick_width) and bidqty >= self.min_size and askqty >= self.min_size:
            if not self.good:
                self.last_good_ts = ts
            self.good = True
        else:
            self.good = False

        self._update_usability(ts)

    cdef void _update_usability(self, long ts) nogil:
        if self.good and ts - self.last_good_ts > self.pause_window:
            self.prc = 0.5 * (self.bid + self.ask)
        else:
            self.prc = NAN
           
    def update_usability(self, long ts):
        self._update_usability(ts)
 
    cdef void _update_marketstate(self, long ts, long seqnum, MarketState *mkt) nogil:
        self.update_md(ts, seqnum, mkt.bidprc0, mkt.bidqty0, mkt.askprc0, mkt.askqty0, 
            mkt.bidpar0, mkt.askpar0, mkt.buyprice, mkt.buyvolume, mkt.sellprice, mkt.sellvolume)
    
    def update_marketstate(self, long ts, long seqnum, WrappedMarketState w):
        self._update_marketstate(ts, seqnum, w.mkt)
        
    def update_deio_md(self, long ts, tuple md, size_t st):
        self.update_md(ts, md[2], md[st+1], <long>md[st+2], md[st+3], <long>md[st+4], 0, 0, NAN, 0, NAN, 0)

    def force_price(self, double price):
        self.prc = price

    def as_dict(self):
        return {
            'bid': self.bid, 
            'ask': self.ask, 
            'bidqty': self.bidqty,
            'askqty': self.askqty,
            'mid': self.prc, 
            'last_good_ts': self.last_good_ts, 
            'seqnum': self.seqnum, 
            'good': self.good
        }


cdef class SyntheticRollManager:
    def __cinit__(self, model, double strike_boundary, double band, 
            double roll=NAN, size_t prev_n=0, double prev_threshold=5, double q=0.00001):
        cdef double k
        self.strike_boundary = strike_boundary
        
        self.q = q
        self.r = 1.0
        
        self.futprc = NAN
        self.last_futprc = NAN
        self.roll = NAN
        self.raw_roll = NAN
        self.option_prices = {}
        for _, rs in model.raw_option_dict.iteritems():
            k = rs.k
            if k not in self.option_prices:
                self.option_prices[k] = empty(2, dtype=float64)
                self.option_prices[k][:] = NAN
        self.sampled_strikes = set()
        self.kroll.value = NAN
        self._update_kroll(roll)
        
        self.prev_movement = empty(prev_n, dtype=float64)
        self.prev_movement[:] = NAN
        self.prev_n = prev_n
        self.prev_head = 0
        self.prev_threshold = prev_threshold
        self.prev_roll_diff = 0
        
        if prev_n > 0:
            self.small_band = 0.1 * band
        else:
            self.small_band = band
        self.large_band = band
        self.band = self.small_band
        self.has_new_data = False
        
    cdef double _get_roll(self) nogil:
        return self.roll
    
    cdef double _get_kroll(self) nogil:
        return self.kroll.value
    
    cdef double _get_last_futprc(self) nogil:
        return self.last_futprc
    
    cdef double _get_last_synthetic(self) nogil:
        return self._get_last_futprc() + self._get_kroll()

    def get_roll(self):
        return self._get_roll()
    
    def get_kroll(self):
        return self._get_kroll()
    
    def get_last_futprc(self):
        return self._get_last_futprc()
    
    def get_last_synthetic(self):
        return self._get_last_synthetic()
    
    def update_future(self, double price):
        cdef double center_price
        if double_eq(price, self.futprc):
            return
        if not isnan(self.futprc):
            self.last_futprc = self.futprc
        self.futprc = price
        if isnan(self.roll):
            center_price = price
        else:
            center_price = price + self.roll
        for x in self.sampled_strikes:
            self.option_prices[x][:] = NAN
        self.sampled_strikes = set()
        self.lower = center_price - self.strike_boundary
        self.upper = center_price + self.strike_boundary
        
    cdef void _update_option(self, double cp, double k, double bid, double ask):
        cdef:
            size_t i
            bint has_new_data
            double mid
            double[:] prcs
        if not double_within(k, self.lower, self.upper):
            self.has_new_data = False
            return
        self.sampled_strikes.add(k)
        prcs = self.option_prices[k]
        if cp < 0:
            i = 1
        else:
            i = 0
        mid = 0.5 * (bid + ask)
        has_new_data = not double_eq(mid, prcs[i])
        if has_new_data:
            prcs[i] = mid
            self._update_roll(k, prcs)
        self.has_new_data = has_new_data
        
    def update_option(self, rs):
        #cdef:
        #    double k = rs.k, mid
        #    size_t i = 0
        #    double[:] prcs
        #if not double_within(k, self.lower, self.upper):
        #    return
        #self.sampled_strikes.add(k)
        #prcs = self.option_prices[k]
        #if rs.cp < 0:
        #    i = 1
        #mid = 0.5 * (rs.top_bid + rs.top_ask)
        #if not double_eq(mid, prcs[i]):
        #    prcs[i] = mid
        #    self._update_roll(k, prcs)
        self._update_option(rs.cp, rs.k, rs.top_bid, rs.top_ask)
            
    cdef void _update_roll(self, double k, double[:] prcs) nogil:
        cdef:
            double total_movement = NAN, roll_diff
        if isnan(self.futprc) or isnan(prcs[0]) or isnan(prcs[1]):
            return
        self.raw_roll = k + prcs[0] - prcs[1] - self.futprc
        self._update_kroll(self.raw_roll)
        roll_diff = self.kroll.value - self.roll
        if isnan(self.roll):
            self.roll = self.kroll.value
        elif double_gt(roll_diff, self.band):
            self.roll = self.kroll.value
            self.prev_head = arrcycle_push(self.prev_movement, self.prev_n, self.prev_head, 1)
            self.fix_band()
            self.prev_roll_diff = roll_diff
        elif double_lte(self.prev_roll_diff, 0) and double_gt(roll_diff, 0):
            self.prev_head = arrcycle_push(self.prev_movement, self.prev_n, self.prev_head, 1)
            self.fix_band()
            self.prev_roll_diff = roll_diff
        elif double_lt(roll_diff, -self.band):
            self.roll = self.kroll.value
            self.prev_head = arrcycle_push(self.prev_movement, self.prev_n, self.prev_head, -1)
            self.fix_band()
            self.prev_roll_diff = roll_diff
        elif double_gte(self.prev_roll_diff, 0) and double_lt(roll_diff, 0):
            self.prev_head = arrcycle_push(self.prev_movement, self.prev_n, self.prev_head, -1)
            self.fix_band()
            self.prev_roll_diff = roll_diff
        
    cdef double fix_band(self) nogil:
        cdef:
            size_t i
            double total_movement = 0
        for 0 <= i < self.prev_n:
            total_movement += self.prev_movement[i]

        if double_lte(fabs(total_movement), self.prev_threshold):
            self.band = self.large_band
        else:
            self.band = self.small_band
    
    cdef void _update_kroll(self, double roll) nogil:
        if isnan(self.kroll.value):
            self.kroll.value = roll
        else:
            self.kroll = kalman.new_univariate_value(self.q, self.r, roll, self.kroll)


cdef class OrderBook:
    def __cinit__(self):
        self.clear()

    cdef void clear(self):
        self.bookqtys = {}
        self.prices = []

    cdef void modify(self, long price, long qty):
        if price not in self.bookqtys:
            self.prices.append(price)
            self.prices.sort()
        self.bookqtys[price] = qty

    cdef void remove(self, long price):
        cdef:
            list prices
            int i = 0 #, n
        if price in self.bookqtys:
            prices = self.prices
            del self.bookqtys[price]
            for i in range(len(prices)):
                if prices[i] == price:
                    prices.pop(i)
                    return

    cdef bint has(self, long price):
        return price in self.bookqtys

    cdef long at(self, long price):
        cdef dict bookqtys = self.bookqtys
        if price in bookqtys:
            return bookqtys[price]
        else:
            return 0

cdef class BBO:
    def __cinit__(self, int levels):
        self.bidprices = zeros(levels, dtype=long)
        self.askprices = zeros(levels, dtype=long)
        self.bidqtys = zeros(levels, dtype=long)
        self.askqtys = zeros(levels, dtype=long)
        self.bid_ob = OrderBook.__new__(OrderBook)
        self.ask_ob = OrderBook.__new__(OrderBook)
        self.best = zeros(3, dtype=long)
        self.best[1] = INTMAX
        self.best[2] = INTMIN
        self.levels = levels

    cdef void clear_book(self):
        cdef:
            int i = 0
            long[:] bidprices = self.bidprices, askprices = self.askprices
            long[:] bidqtys = self.bidqtys, askqtys = self.askqtys
        self.bid_ob.clear()
        self.ask_ob.clear()
        for i in range(self.levels):
            bidprices[i] = 0
            askprices[i] = 0
            bidqtys[i] = 0
            askqtys[i] = 0
        self.best[1] = INTMAX
        self.best[2] = INTMIN

    cdef void update_book_array(self, long side, long[:] prices, long[:] qtys):
        cdef:
            size_t n = self.levels
            OrderBook ob = self.bid_ob if side == 1 else self.ask_ob
            long best = self.best[1] if side == 1 else self.best[2]
            bint must_rebuild = best != prices[0]
        for 0 <= i < n:
            if qtys[i] > 0:
                ob.modify(prices[i], qtys[i])
            else:
                must_rebuild = True
        if (side == 1 and prices[0] < best) or (side == 2 and prices[0] > best):
            self.remove_irrelevant(side, prices[0], ob)
            must_rebuild = True
        if must_rebuild:
            self.rebuild_price_arrays()
        #self.rebuild_qty_arrays()
        self.best[side] = prices[0]

    cdef void remove_irrelevant(self, long side, long price, OrderBook ob):
        cdef:
            int i = 0, n, start, end
            long[:] prices = self.bidprices if side == 1 else self.askprices
            bint must_rebuild = False
        n = len(prices)
        while i < n and prices[i] != 0 and prices[i] != price:
            ob.remove(prices[i])
            i += 1

    cdef long update_book_by(self, long side, long price, long qty):
        cdef OrderBook ob = self.bid_ob if side == 1 else self.ask_ob
        if ob.has(price):
            return self.update_book_by_nocheck(side, price, qty)
        elif qty > 0:
            ob.modify(price, qty)
            self.rebuild_price_arrays()
        return qty

    cdef long update_book_by_nocheck(self, long side, long price, long qty):
        cdef:
            OrderBook ob = self.bid_ob if side == 1 else self.ask_ob
            long newqty = ob.at(price)
        newqty += qty
        if newqty <= 0:
            ob.remove(price)
            self.rebuild_price_arrays()
        else:
            ob.modify(price, newqty)
            #self.rebuild_qty_arrays()
        return newqty

    cdef bint has(self, long side, long price):
        cdef OrderBook ob = self.bid_ob if side == 1 else self.ask_ob
        return ob.has(price)

    cdef long at(self, long side, long price):
        cdef OrderBook ob = self.bid_ob if side == 1 else self.ask_ob
        return ob.at(price)

    cdef void rebuild_price_arrays(self):
        cdef:
            list bids, asks
            int bid_depth, ask_depth, levels = self.levels, i
            long[:] bidprices = self.bidprices, askprices = self.askprices
        bids = self.bid_ob.prices #_ptr()
        asks = self.ask_ob.prices #_ptr()
        bid_depth, ask_depth = len(bids), len(asks)
        bid_depth = levels if bid_depth > levels else bid_depth
        ask_depth = levels if ask_depth > levels else ask_depth
        for 0 <= i < levels:
            if i < bid_depth:
                bidprices[i] = bids[bid_depth-1-i]
            else:
                bidprices[i] = 0
            if i < ask_depth:
                askprices[i] = asks[i]
            else:
                askprices[i] = 0
        #self.rebuild_qty_arrays()

    cdef void rebuild_qty_arrays(self):
        cdef:
            int i
            long[:] bidprices = self.bidprices, askprices = self.askprices, bidqtys = self.bidqtys, askqtys = self.askqtys
            OrderBook bid_ob = self.bid_ob, ask_ob = self.ask_ob
        for i in range(self.levels):
            bidqtys[i] = bid_ob.at(bidprices[i])
            askqtys[i] = ask_ob.at(askprices[i])

    cdef bint is_crossed(self) nogil:
        return self.bidprices[0] >= self.askprices[0]

    cdef list update_assumed_tradeqty(self, long side, long price, long tradeqty, long itemqty):
        cdef:
            OrderBook ob = self.bid_ob if side == 1 else self.ask_ob
            long[:] prices = self.bidprices if side == 1 else self.askprices
            long topqty = ob.at(price), remaining_qty = tradeqty, newprice = price
            list tradeqtys = []
        if topqty == 0:
            tradeqtys.append((price, tradeqty))
            tradeqtys.append((0, -1))
            return tradeqtys
        topqty = ob.at(newprice) - remaining_qty
        while topqty <= 0:
            remaining_qty = -1 * topqty
            tradeqtys.append((price, ob.at(newprice)))
            ob.remove(newprice)
            self.rebuild_price_arrays()
            newprice = prices[0]
            topqty = ob.at(newprice) - remaining_qty
        if remaining_qty > 0:
            tradeqtys.append((newprice, remaining_qty))
        ob.modify(newprice, topqty)
        tradeqtys.append((0, 0))
        return tradeqtys
    
    cdef tuple reverse_assumed_tradeqty(self, long side, list tradeqtys, long reversed_tradeqty):
        cdef:
            long itemprice, itemqty, remaining_qty
            list reversal = []
        if not tradeqtys:
            return 0, -1, []
        remaining_qty = reversed_tradeqty
        while tradeqtys:
            itemprice, itemqty = tradeqtys.pop()
            if remaining_qty + itemqty > 0:
                itemqty = -1 * remaining_qty
            remaining_qty += itemqty
            reversal.append((itemprice, itemqty))
            if remaining_qty >= 0:
                if remaining_qty > 0:
                    tradeqtys.append((itemprice, -1 * remaining_qty))
                return itemprice, -1 * reversed_tradeqty, reversal
        return 0, -2, []

cdef long find_tick_before_turn(long start_idx, long turn_direction, long turn_price, long[:] uniq_ticks, long[:] bidprcs, long[:] askprcs):
    cdef:
        long j = start_idx
        long curr_uniq_tick = uniq_ticks[start_idx]
    
    # rewind to previous uniq tick
    while j > 0 and uniq_ticks[j] == curr_uniq_tick:
        j -= 1
    
    # now look for when turn_price was bid if direction < 0 or offered if direction > 0
    while j > 0:
        if bidprcs[j] < askprcs[j] and ((turn_direction > 0 and askprcs[j] <= turn_price) or (turn_direction < 0 and bidprcs[j] >= turn_price)):
            return uniq_ticks[j]
        j -= 1
        
    return uniq_ticks[j]

def df_drop_duplicates(df, unique_columns):
    from numpy import any
    upside_down_df = df.loc[df.index[::-1], unique_columns].diff().fillna(1)!=0
    nodupe_bool = any(upside_down_df.values, axis=1)[::-1]
    return df.loc[nodupe_bool]

def df_no_repeat(df, norepeat_col):
    from numpy import all, apply_over_axes
    norepeat = df.loc[:, norepeat_col].diff().fillna(1).values==0
    return df.loc[~apply_over_axes(all, norepeat, 1)[:, 0]]

def df_asof(sorted_ldf, sorted_rdf, lcol, rcol):
    cdef:
        long i, j, nl = sorted_ldf.shape[0], nr = sorted_rdf.shape[0]
        ndarray left = sorted_ldf[lcol].values
        ndarray right = sorted_rdf[rcol].values
        ndarray asof_index = zeros(nl, dtype=sorted_rdf.index.dtype)
        ndarray rdf_index = sorted_rdf.index.values
        ndarray output = zeros((nl, 1), dtype=left.dtype)
        
    for i in range(nl):
        while j < nr and left[i] > right[j]:
            j += 1
        output[i, 0] = left[i]
        if j == 0:
            asof_index[i] = rdf_index[0]
            #output[i, 1] = right[0]
        else:
            asof_index[i] = rdf_index[j-1]
            #output[i, 1] = right[j-1]
    result = DataFrame(output, index=asof_index, columns=[rcol + '_asof_' + lcol])
    result = result.join(sorted_rdf)
    #return DataFrame(output, columns=[lcol, rcol + '_asof_' + lcol])
    result.index = sorted_ldf.index
    return result

def find_turns(df, long minqty, long tick, long time_window=0):
    """
    find_turns(df, long minqty, long tick, long time_window=0)

    find turns that turn with a minimum of minqty
    """
    cdef:
        long tlen = df.shape[0], i = 0, j = 0, prev_bid = 0, prev_ask = 0
        long[:] times
        long[:] bidqty = df.bidqty0.values
        long[:] bidprc = df.bidprc0.values
        long[:] askprc = df.askprc0.values
        long[:] askqty = df.askqty0.values
        ndarray[long, ndim=2] results = zeros([tlen, 2], dtype=long)
        bint success = True

    if time_window > 0:
        times = df.time.values.astype(int64)

    for i in range(tlen):
        if i == 0:
            prev_bid = bidprc[i]
            prev_ask = askprc[i]
            continue
        elif (bidprc[i] == 0 and askprc[i] == 0) or (askprc[i] - bidprc[i]) > tick:
            continue
        elif bidprc[i] >= prev_ask and (bidqty[i] >= minqty or bidprc[i] - prev_ask > tick):
            if bidprc[i] - prev_bid > 0:
                success = True
                if time_window > 0:
                    j = i
                    while j < tlen and times[j] - times[i] < time_window:
                        if bidprc[j] - prev_ask > tick:
                            success = True
                            break
                        elif bidqty[j] < minqty:
                            success = False
                            break
                        j += 1
                if success: 
                    results[i, 0] = bidprc[i] - prev_bid
                    results[i, 1] = bidprc[i]
            prev_ask = askprc[i]
            prev_bid = bidprc[i]
        elif askprc[i] <= prev_bid and (askqty[i] >= minqty or prev_bid - askprc[i] > tick):
            if askprc[i] - prev_ask < 0:
                success = True
                if time_window > 0:
                    j = i
                    while j < tlen and times[j] - times[i] < time_window:
                        if prev_bid - askprc[j] > tick:
                            success = True
                            break
                        elif askqty[j] < minqty:
                            success = False
                            break
                        j += 1
                if success:
                    results[i, 0] = askprc[i] - prev_ask
                    results[i, 1] = askprc[i]
            prev_bid = bidprc[i]
            prev_ask = askprc[i]
        elif askprc[i] <= prev_ask and not askprc[i] <= prev_bid:
            prev_ask = askprc[i]
        elif bidprc[i] >= prev_bid and not bidprc[i] >= prev_ask:
            prev_bid = bidprc[i]
    return DataFrame(results, index=df.index, columns=('turn_direction', 'turn_price'))


def find_turns_with_previous_tick(df, long minqty, long tick):
    cdef:
        long tlen = df.shape[0], i = 0, j = 0, prev_bid = 0, prev_ask = 0
        long[:] bidqty = df.bidqty0.values
        long[:] bidprc = df.bidprc0.values
        long[:] askprc = df.askprc0.values
        long[:] askqty = df.askqty0.values
        long[:] uniq_ticks = df.unique_tick.values
        ndarray[long, ndim=2] results = zeros([tlen, 3], dtype=long)
        bint success = True

    for 0 <= i < tlen:
        if i == 0:
            prev_bid = bidprc[i]
            prev_ask = askprc[i]
            continue
        elif (bidprc[i] == 0 and askprc[i] == 0) or (askprc[i] - bidprc[i]) > tick:
            continue
        elif bidprc[i] >= prev_ask and (bidqty[i] >= minqty or bidprc[i] - prev_ask > tick):
            #went bid. turned for more than minqty or moved more than a tick
            if bidprc[i] - prev_bid > 0:
                results[i, 0] = bidprc[i] - prev_bid
                results[i, 1] = bidprc[i]
                results[i, 2] = find_tick_before_turn(i, results[i, 0], results[i, 1], uniq_ticks, bidprc, askprc)
            prev_ask = askprc[i]
            prev_bid = bidprc[i]
        elif askprc[i] <= prev_bid and (askqty[i] >= minqty or prev_bid - askprc[i] > tick):
            if askprc[i] - prev_ask < 0:
                results[i, 0] = askprc[i] - prev_ask
                results[i, 1] = askprc[i]
                results[i, 2] = find_tick_before_turn(i, results[i, 0], results[i, 1], uniq_ticks, bidprc, askprc)
            prev_bid = bidprc[i]
            prev_ask = askprc[i]
        elif askprc[i] <= prev_ask and not askprc[i] <= prev_bid:
            #offer improved but not through prev bid. gapped book?
            prev_ask = askprc[i]
        elif bidprc[i] >= prev_bid and not bidprc[i] >= prev_ask:
            #bid improved but not through prev offer. gapped book?
            prev_bid = bidprc[i]
            
    return DataFrame(results, index=df.index, columns=['turn_direction', 'turn_price', 'prev_tick'])



def unique_ticks(df):
    """
    unique_ticks(df)

    Enumerate unique bid/ask combinations in a time series

    Parameters
    ----------
    df : DataFrame
        DataFrame of evaluated contract

    Returns
    -------        
    enumerated ticks : ndarray[long, ndim=1]
    """
    cdef:
        long tlen = df.shape[0], i, tick = 0
        long[:] bidprc = df.bidprc0.values
        long[:] askprc = df.askprc0.values
        ndarray[long, ndim=1] results = zeros(tlen, dtype=long)

    fast_unique_ticks(tlen, bidprc, askprc, results)
    #for i in range(tlen):
    #    if i == 0:
    #        continue
    #    if bidprc[i] != bidprc[i-1] or askprc[i] != askprc[i-1]:
    #        tick += 1
    #    results[i] = tick
    return results

def sniper_streaker_sides(df, long window_size, long sym_int, long tick_increment=1, bint any_price=False):
    """
    streaker_sides(nkdf, double window_size, int sym_int)

    Aggregate volume events and corresponding average prices in a specific window of time.

    Parameters
    ----------
    df : DataFrame
        DataFrame of time series
    window_size : long
        Amount of time to sum trades from last market data message
    sym_int : long
        Which symbol to sum the trades for. Set to zero to sum everything
    tick_increment : long
        Sum trades versus specific ticks in the market by computing the modulo of this against the market prices
    any_price : bool
        Sum volume and average prices regardless of traded price. Set to False if traded volume must be relevant
        to the market

    Returns
    -------
    streaker_volumes : ndarray[long, ndim=2]
        columns:    0 := buy volume
                    1 := sell volume
                    2 := buy trade count
                    3 := sell trade count
    vwap : ndarray[long, ndim=2]
        columns:    0 := average buy price
                    1 := average sell price
                    2 := average trade price
    """
    cdef:
        long tlen = df.shape[0], i = 0, j = 0, window_start = 0
        long[:] times = df.timestamp.values
        long[:] bidprcs = df.bidprc0.values
        long[:] askprcs = df.askprc0.values
        long[:] buyprices = df.buyprice.values
        long[:] sellprices = df.sellprice.values
        long[:] buyvolumes = df.buyvolume.values
        long[:] sellvolumes = df.sellvolume.values
        long[:] sym_ints = df.sym_int.values

        long bid_ref, ask_ref, buys_vs_ask, sells_vs_bid
        long prcqty_buys, prcqty_sells, qty_buys, qty_sells, prcqty_tot, qty_tot
        long count_buys, count_sells
        ndarray[long, ndim=2] streaker_results = zeros((tlen, 4), dtype=long)
        ndarray[double, ndim=2] vwap_results = zeros((tlen, 3), dtype=float64)

        bint valid_bid, valid_ask

    for i in range(tlen):
        j = i
        buys_vs_ask, sells_vs_bid = 0, 0
        count_buys, count_sells = 0, 0
        prcqty_buys, prcqty_sells, qty_buys, qty_sells = 0, 0, 0, 0
        prcqty_tot, qty_tot = 0, 0

        bid_ref = bidprcs[i]
        ask_ref = askprcs[i]
        # e.g. if tick_increment == 10 and bid_ref == 17555, will modify bid_ref to 17550
        if bid_ref%tick_increment > 0:  # conditional not necessary
            bid_ref -= bid_ref%tick_increment
        if ask_ref%tick_increment > 0:
            ask_ref += ask_ref%tick_increment

        valid_bid, valid_ask = True, True
        # climb back to find the start of the window
        while j >= 0 and times[i] - times[j] <= window_size:
            if sym_int > 0 and sym_int != sym_ints[j]:
                j -= 1
                continue
            # keep summing trades? don't do it if market is worse in past
            if bidprcs[j] < bid_ref:
                valid_bid = False
            if askprcs[j] > ask_ref:
                valid_ask = False

            # buying streak against or through offer
            if valid_ask and buyvolumes[j] > 0 and (any_price or (not any_price and buyprices[j] >= ask_ref)):
                buys_vs_ask += buyvolumes[j]
                prcqty_buys += buyvolumes[j] * buyprices[j]
                qty_buys += buyvolumes[j]
                count_buys += 1
            # selling streak against or through bid
            if valid_bid and sellvolumes[j] > 0 and (any_price or (not any_price and sellprices[j] <= bid_ref)):
                sells_vs_bid += sellvolumes[j]
                prcqty_sells += sellvolumes[j] * sellprices[j]
                qty_sells += sellvolumes[j]
                count_sells += 1
            j -= 1
        streaker_results[i, 0] = buys_vs_ask
        streaker_results[i, 1] = sells_vs_bid
        streaker_results[i, 2] = count_buys
        streaker_results[i, 3] = count_sells
        if qty_buys > 0:
            #res[i][2] = prcqty_buys / qty_buys
            vwap_results[i, 0] = <double>prcqty_buys / <double>qty_buys
            prcqty_tot += prcqty_buys
            qty_tot += qty_buys
        if qty_sells > 0:
            #res[i][3] = prcqty_sells / qty_sells
            vwap_results[i, 1] = <double>prcqty_sells / <double>qty_sells
            prcqty_tot += prcqty_sells
            qty_tot += qty_sells
        if qty_tot > 0:
            vwap_results[i, 2] = <double>prcqty_tot / <double>qty_tot
    return streaker_results, vwap_results

def find_any_price(df):
    cdef:
        long n = df.shape[0], i
        long[:] bidprcs = df.bidprc0.values
        long[:] askprcs = df.askprc0.values
        long[:] buyprices = df.buyprice.values
        long[:] sellprices = df.sellprice.values
        ndarray results = zeros(n, dtype=long)

    for i in range(n):
        results[i] = buyprices[i] or sellprices[i] or bidprcs[i] or askprcs[i]

    return results

def deio_pub_reconciliation(df, new=False):
        
    cdef:
        ndarray[long, ndim=1] times = df['time'].values.astype(int64)
        ndarray[long, ndim=1] buyvolumes = df['buyvolume'].values
        ndarray[long, ndim=1] buyprices = df['buyprice'].values
        ndarray[long, ndim=1] sellvolumes = df['sellvolume'].values
        ndarray[long, ndim=1] sellprices = df['sellprice'].values
        ndarray[long, ndim=2] res = zeros( (len(df), 5), dtype=int64)
        ndarray[long, ndim=1] eops = df['eop_int'].values
        ndarray[str, ndim=1] msgs = df['msg'].values
        
        int buyvolume, buyprice, sellvolume, sellprice
        int i=0, j=0, n=df.shape[0]
    
    for i in range(0, n):
        buyvolume = buyvolumes[i]
        buyprice = buyprices[i]
        sellvolume = sellvolumes[i]
        sellprice = sellprices[i]
    
        if msgs[i] == 'E_TRADE':
            j = i - 1            
            if buyvolumes[i]:
                while j>=0 and times[j]==times[i] and buyprices[j]==buyprices[i] and msgs[j]=='E_TRADE':
                    buyvolume += buyvolumes[j]
                    res[j, 0] = 0
                    j-=1
            elif sellvolumes[i]:
                while j>=0 and times[j]==times[i] and sellprices[j]==sellprices[i] and msgs[j]=='E_TRADE':
                    sellvolume += sellvolumes[j]
                    res[j, 0] = 0
                    j-=1
                    
            res[i, 0] = 1
            res[i, 1] = buyvolume
            res[i, 2] = buyprice
            res[i, 3] = sellvolume
            res[i, 4] = sellprice
                        
        elif eops[i]==1:
            res[i, 0] = 1
        
        elif i < n and times[i+1] != times[i]:
            res[i, 0] = 1
            
        else:
            continue
            
    return res


#########################################################
#
#   model publisher helper functions
#
#########################################################

def extract_mdrepub_fill(msg):
    """
    msg -> (id, filltype, side, securityid, 
            userid, serverid, price, qty, 
            exchtradeid, ordertype)
    """
    cdef:
        int side = 1
        list items = msg.split(',')
    
    if items[4][0] == 'o':
        side = 2
    
    return (
        atoi(items[1]), atoi(items[3]), side, atol(items[5]), 
        atoi(items[6]), atoi(items[7]), atof(items[8]), atoi(items[9]), 
        items[10], items[11]
    )


def sum_trades(df, new=False):

    cdef:
        ndarray[long, ndim=1] exch_ts = df['exch_time'].values.astype(int64)
        ndarray[long, ndim=1] ts = df['time'].values.astype(int64)
        ndarray[long, ndim=1] buy_vol = df['buyvolume'].values
        ndarray[double, ndim=1] buy_prc = df['buyprice'].values
        ndarray[long, ndim=1] ask_vol = df['sellvolume'].values
        ndarray[double, ndim=1] ask_prc = df['sellprice'].values
        ndarray[long, ndim=1] seqnum = df['seqnum'].values
        ndarray[long, ndim=1] secid = df['secid'].values
        ndarray[long, ndim=2] res = zeros( (len(df), 2), dtype=int64)
        ndarray[long, ndim=1] zero = zeros(len(df), dtype=int64)
        int i=0, j=0, tlen=len(df), temp_qty = 0, do_something=0

    for 1 <= i < tlen-1:
        #find side of trade and set temp var "vol_col" to that side's volume and set do_something flag
        '''
        try:
            if ask_vol[i] > 0 and (exch_ts[i] != exch_ts[i+1] or ts[i] != ts[i+1] or ask_vol[i+1] == 0):
                pass
        except:
            print tlen
            print i
            print ts[i]
            print exch_ts[i]
            print ask_vol[i]
            print ts[i+1]
            print exch_ts[i+1]
            print ask_vol[i+1]'''
        if buy_vol[i] > 0 and (exch_ts[i] != exch_ts[i+1] or ts[i] != ts[i+1] or buy_prc[i] != buy_prc[i+1] or buy_vol[i+1] == 0):
            vol_col = buy_vol
            prc = buy_prc
            do_something = 1
        elif ask_vol[i] > 0 and (exch_ts[i] != exch_ts[i+1] or ts[i] != ts[i+1] or ask_prc[i] != ask_prc[i+1] or ask_vol[i+1] == 0):
            vol_col = ask_vol
            prc = ask_prc
            do_something = 1
        else:
            res[i,1] = seqnum[i]

        if do_something:
            j = i
            temp_qty=0
            #loop backwards until the index is a different event on the same security that is being summed
            while ts[i] == ts[j] and exch_ts[i] == exch_ts[j] and secid[i] == secid[j] and prc[i] == prc[j]:
                if secid[i] == secid[j]:
                    #sum volume
                    res[i,0] += vol_col[j]
                    res[i,1] = seqnum[j]
                j -= 1

            #move index fwd by 1 to avoid resetting a value to zero
            j += 1
            while j != i:
                #reset values related to the same event to zero
                if secid[j] == secid[i]:
                    res[j,0] = 0
                j+=1
            do_something = 0

    return res

