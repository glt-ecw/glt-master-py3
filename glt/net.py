import os
import json
import socket


def get_hosts():
    cfg_path = os.path.join(os.environ['GLT_MASTER_PATH'], 'cfg')
    hosts_cfg_filename = os.path.join(cfg_path, 'hosts.json')
    with open(hosts_cfg_filename, 'r') as f:
        return json.load(f)


def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


def open_multicast_socket(iface, publish=False, addr=None):
    sock = socket.socket(
        socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP
    )

    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 32)

    # send from a specific interface 
    sock.setsockopt(
        socket.SOL_IP,
        socket.IP_MULTICAST_IF,
        socket.inet_aton(iface)
    )

    if not publish:
        if addr is None:
            raise ValueError('must specify addr to listen!')
        ip, port = addr.split(':')
        port = int(port)

        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 1)

        sock.bind((ip, port))

        sock.setsockopt(
            socket.SOL_IP,
            socket.IP_ADD_MEMBERSHIP,
            socket.inet_aton(ip) + socket.inet_aton(iface)
        )
    return sock


class MulticastSocket:
    def __init__(self, iface, addr, publish=False):
        self.addr = addr
        self.iface = iface
        self.publish = publish
        self.sock = open_multicast_socket(
            iface, publish=publish, addr=addr
        )
        if self.publish:
            pub_ip, pub_port = self.addr.split(':')
            self.pub_ip_port = pub_ip, int(pub_port)

    def send_bytes(self, byte_list):
        if self.publish:
            return self.sock.sendto(bytearray(byte_list), self.pub_ip_port)
        raise ValueError('not configured to publish. publish=False')

    def close(self):
        self.sock.close()

    def send(self, data):
        if self.publish:
            return self.sock.sendto(data, self.pub_ip_port)
        raise ValueError('not configured to publish. publish=False')

    def recv(self, num_bytes=1024):
        if self.publish:
            raise ValueError('not configured to listen. publish=True')
        return self.sock.recv(num_bytes)
