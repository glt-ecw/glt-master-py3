cdef struct Data:
    long time
    long side
    long price
    long qty
    
cdef struct ArrayQueue:
    Data[:] data
    unsigned int head
    unsigned int length
    unsigned int alloced

cdef struct ArrayStack:
    Data[:] data
    unsigned int length
    unsigned int alloced

cdef Data[:] new_data_array(unsigned int size)
cdef ArrayQueue new_arrayqueue(unsigned int alloced)
cdef void resize_arrayqueue(ArrayQueue *queue)
cdef void push_arrayqueue(ArrayQueue *queue, long time, long side, long price, long qty)
cdef Data pop_arrayqueue(ArrayQueue *queue) nogil
cdef void clear_arrayqueue(ArrayQueue *queue) nogil

cdef ArrayStack new_arraystack(unsigned int alloced)
cdef void resize_arraystack(ArrayStack *queue)
cdef void push_arraystack(ArrayStack *stack, long time, long side, long price, long qty)
cdef Data pop_arraystack(ArrayStack *stack) nogil
cdef void clear_arraystack(ArrayStack *stack) nogil
