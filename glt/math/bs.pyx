# cython: boundscheck=False
# cython: cdivision=True
# cython: wraparound=False
from libc.math cimport exp as fexp, erf as cerf, log as flog, sqrt as fsqrt, fabs as cabs, M_2_SQRTPI, M_SQRT1_2

cdef double M_1_SQRT2PI = 0.5 * M_SQRT1_2 * M_2_SQRTPI
cdef double ALMOST_ZERO = 1e-10

cdef double ncdf(double x) nogil:
    return 0.5 + 0.5 * cerf(x * M_SQRT1_2)

cdef double npdf(double x) nogil:
    return M_1_SQRT2PI * fexp(-0.5*x*x)

cdef double d1(double x, double u, double v, double t) nogil:
    if t <= 0:
        t = ALMOST_ZERO
    if v <= 0:
        v = ALMOST_ZERO
    return (flog(u / x) + 0.5 * v * v * t) / (v * fsqrt(t))

cdef double d2(double x, double u, double v, double t) nogil:
    return d1(x, u, v, t) - v * fsqrt(t)

cpdef double tv(double x, double u, double v, double t, double r, double op) nogil:
    """tv(double x, double u, double v, double t, double r, double op) """
    return fexp(-r * t) * op * (u * ncdf(op * d1(x, u, v, t)) - x * ncdf(op * d2(x, u, v, t)))

cpdef double delta(double x, double u, double v, double t, double r, double op) nogil:
    """delta(double x, double u, double v, double t, double r, double op)"""
    return op * fexp(-r * t) * ncdf(op * d1(x, u, v, t))

cpdef double vega(double x, double u, double v, double t, double r) nogil:
    """vega(double x, double u, double v, double t, double r)"""
    return u * fexp(-r * t) * fsqrt(t) * npdf(d1(x, u, v, t))

cpdef double ivol(double x, double u, double t, double r, double op, double prc, double seed_vol, double tick_threshold, int iterations) nogil:
    """ivol(double x, double u, double t, double r, double op, double prc, double seed, double tick_threshold, double iterations)"""
    cdef:
        int i
        double v = seed_vol, diff
    
    if prc <= 0:
        return 0.0
    if seed_vol <= 0 or tick_threshold <= 0:
        return -1
    for i in range(iterations):
        diff = prc - tv(x, u, v, t, r, op)
        if cabs(diff) <= tick_threshold:
            return v
        v += diff / vega(x, u, v, t, r)
    return -1

cpdef double iu(double x, double v, double t, double r, double op, double prc, double seed_futprc, double tick_threshold, int iterations) nogil:
    cdef:
        int i
        double u = seed_futprc, diff

    if prc <= 0:
        return -1
    if seed_futprc <= 0 or tick_threshold <= 0:
        return -1
    for i in range(iterations):
        diff = prc - tv(x, u, v, t, r, op)
        if cabs(diff) <= tick_threshold:
            return u
        u += diff / delta(x, u, v, t, r, op)
    return -1

# for backwards compatibility with bsm_implieds users
def bs_tv(double u, double x, double t, double v, double r, double op):
    """bs_tv(double u, double x, double t, double v, double r, double op)"""
    return tv(x, u, v, t, r, op)

def delta_py(double u, double x, double t, double v, double r, double op):
    return delta(x, u, v, t, r, op)

def vega_py(double u, double x, double t, double v, double r, double op=0):
    return vega(x, u, v, t, r)

def implied_vol(double u, double prc, double x, double t, double r, double op):
    """implied_vol(double u, double prc, double x, double t, double r, double op)"""
    return ivol(x, u, t, r, op, prc, 1.0, 0.001, 20)

