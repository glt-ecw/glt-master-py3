cdef double ncdf(double x) nogil

cdef double npdf(double x) nogil

cdef double d1(double x, double u, double v, double t) nogil

cdef double d2(double x, double u, double v, double t) nogil

cpdef double tv(double x, double u, double v, double t, double r, double op) nogil

cpdef double delta(double x, double u, double v, double t, double r, double op) nogil

cpdef double vega(double x, double u, double v, double t, double r) nogil

cpdef double ivol(double x, double u, double t, double r, double op, double prc, double seed_vol, double tick_threshold, int iterations) nogil

cpdef double iu(double x, double v, double t, double r, double op, double prc, double seed_futprc, double tick_threshold, int iterations) nogil
