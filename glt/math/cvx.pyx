# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from libc.stdint cimport *
from libc.math cimport NAN
from glt.math.cpp cimport isnan
from numpy cimport ndarray

import cvxpy as cvx
import warnings

from numpy import sign, empty, dot, array, sqrt, power


SolverError = cvx.SolverError


def calc_residual_error(ndarray y_actual, ndarray y_pred, double stdevs=2, sample_weight=None):
    residuals = y_actual - y_pred
    if sample_weight is None:
        xbar = residuals.mean()
        error = stdevs * residuals.std()
    else:
        total_weight = sample_weight.sum()
        xbar = (sample_weight * residuals).sum() / total_weight
        error = stdevs * sqrt((sample_weight * power(residuals - xbar, 2)).sum() / total_weight)
    return xbar, error


def is_bad_fit(double mean, double error):
    return (mean < 0 and mean + error < 0) or (mean > 0 and mean - error > 0)


def calc_poly_A(xvals, uint64_t n):
    cdef:
        ndarray A = empty((len(xvals), n), dtype='<f8')
        uint64_t i
    for 0 <= i < n:
        A[:, i] = power(xvals, i+1)
    return A


cdef void _add_weights(double[:] w, double[:, :] A_dst, double[:] b_dst, uint64_t r, uint64_t c) nogil:
    cdef:
        uint64_t i, j

    for 0 <= i < r:
        for 0 <= j < c:
            A_dst[i, j] = w[i] * A_dst[i, j]
        b_dst[i] = w[i] * b_dst[i]


cdef class CVXLeastSquares:
    cdef:
        readonly:
            list constraints_
            ndarray coef_
            double intercept_
            prob
        public:
            x 
            list base_constraints
            ndarray mean, std

    def __init__(self, x=None, list base_constraints=None):
        self.x = x
        if base_constraints is None:
            self.base_constraints = []
        else:
            self.base_constraints = base_constraints
        self.mean = None
        self.std = None
        self.prob = None
        self.constraints_ = None
        self.coef_ = None
        self.intercept_ = NAN

    cdef void _add_constraint(self, constraint):
        self.base_constraints.append(constraint)

    def add_constraint(self, constraint):
        self._add_constraint(constraint)

    cdef void _calc_normalization_parameters(self, ndarray A):
        self.mean = A.mean(axis=0)
        self.std = A.std(axis=0)
        
    cdef ndarray _normalize(self, ndarray A):
        # standard normalization
        return (A - self.mean) / self.std
        
    def fit(self, ndarray A_src, ndarray b_src, ndarray sample_weight=None, 
            solver_type='ECOS', list more_constraints=None, **solver_options):
        cdef:
            uint64_t r, c
            ndarray A_norm, b, w
            error, objective, constraints, prob
            double val
        self._calc_normalization_parameters(A_src)
        r = A_src.shape[0]
        c = A_src.shape[1] + 1
        A_norm = empty((r, c), dtype='<f8')
        A_norm[:, 0] = 1
        A_norm[:, 1:] = self._normalize(A_src)
        if self.x is None:
            self.x = cvx.Variable(c)
        x = self.x

        if sample_weight is not None:
            w = sample_weight
            b = b_src.copy()
            _add_weights(w, A_norm, b, r, c)
        else:
            b = b_src

        error = cvx.sum_squares(A_norm*x - b)
        objective = cvx.Minimize(error)

        constraints = list(self.base_constraints)
        if more_constraints is not None:
            constraints.extend(more_constraints)
        self.constraints_ = constraints

        prob = cvx.Problem(objective, self.constraints_)
        prob.solve(solver=solver_type, **solver_options)

        val = prob.value
        self.prob = prob

        self._save_coef()
        
    def predict(self, ndarray A, ndarray coef=None, double intercept=NAN):
        if coef is None or isnan(intercept):
            return dot(A, self.coef_) + self.intercept_
        else:
            return dot(A, coef) + intercept
    
    cdef void _save_coef(self):
        coef = array(self.x.value)[:, 0]
        self.coef_ = coef[1:] / self.std
        self.intercept_ = coef[0] - (self.coef_ * self.mean).sum()
        

cdef class PolyFitOneFeature(CVXLeastSquares):
    cdef:
        uint64_t slope_dir, n
        public:
            left, right
    def __init__(self, uint64_t n=1):
        cdef:
            list base_constraints
            x

        if n < 1:
            warnings.warn('n < 1; minimum dimensions is 1')
            n = 1
        self.n = n

        x = cvx.Variable(self.n + 1)

        CVXLeastSquares.__init__(self, x=x)

    def calc_A(self, ndarray a):
        return calc_poly_A(a, self.n)
    
    def fit(self, ndarray a, ndarray b, ndarray sample_weight=None, solver_type='ECOS', **solver_options):
        cdef ndarray A = self.calc_A(a)
        CVXLeastSquares.fit(self, A, b, sample_weight=sample_weight, solver_type=solver_type, **solver_options)
        
    def predict(self, ndarray a, ndarray coef=None, double intercept=NAN):
        return CVXLeastSquares.predict(self, self.calc_A(a), coef=coef, intercept=intercept)
