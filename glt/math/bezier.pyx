# distutils: language=c++
# cython: boundscheck=False
# cython: cdivision=True
# cython: wraparound=False
from numpy cimport ndarray
from libc.math cimport pow as fpow, NAN, isnan

from numpy import array, zeros, empty, float64, dot, diag, identity
from numpy.linalg import inv, LinAlgError

from glt.tools import now

# new hotness
cdef void _compute_bern(size_t n, double[:] bern) nogil:
    cdef size_t i
    bern[0] = 1
    for 1 <= i <= n:
        bern[i] = bern[i-1] * (n-i+1) / i
        

cdef void _compute_t(double[:] vals, size_t n, double minval, double maxval, double[:] output) nogil:
    cdef:
        size_t i
        double c = 1.0 / (maxval - minval)
    for 0 <= i < n:
        output[i] = c * (vals[i] - minval)


cdef void _compute_tm(double[:] t, double[:] bern, size_t n, size_t degrees, double[:, :] output) nogil:
    cdef size_t i, j
    for 0 <= j <= degrees:
        for 0 <= i < n:
            output[i, j] = bern[j] * fpow(1 - t[i], degrees - j) * fpow(t[i], j)


def compute_tm(ndarray vals, size_t degrees, size_t n=0, double minval=NAN, double maxval=NAN):
    cdef:
        ndarray bern, t, TM
    if isnan(minval):
        minval = vals.min()
    if isnan(maxval):
        maxval = vals.max()
    if n == 0:
        n = len(vals)
    bern = empty(degrees+1, dtype=float64)
    t = empty(n, dtype=float64)
    TM = empty((n, degrees+1), dtype=float64)
    _compute_bern(degrees, bern)
    _compute_t(vals, n, minval, maxval, t)
    _compute_tm(t, bern, n, degrees, TM)
    return TM


cdef void _compute_tm_deriv(double[:] t, double[:] bern, size_t n, size_t degrees, double[:, :] output) nogil:
    cdef:
        size_t i, j
        double left, right
    for 0 <= j <= degrees:
        for 0 <= i < n:
            if j < degrees:
                left = -1.0 * <double>(degrees-j) * fpow(1-t[i], degrees-j-1) * fpow(t[i], j)
            else:
                left = 0
            if j > 0:
                right = fpow(1-t[i], degrees-j) * j *fpow(t[i], j-1)
            else:
                right = 0
            output[i, j] = bern[j] * (left + right)


def compute_tm_deriv(ndarray vals, size_t degrees, size_t n=0, double minval=NAN, double maxval=NAN):
    cdef:
        ndarray bern, t, TM_deriv
    if isnan(minval):
        minval = vals.min()
    if isnan(maxval):
        maxval = vals.max()
    if n == 0:
        n = len(vals)
    bern = empty(degrees+1, dtype=float64)
    t = empty(n, dtype=float64)
    TM_deriv = empty((n, degrees+1), dtype=float64)
    _compute_bern(degrees, bern)
    _compute_t(vals, n, minval, maxval, t)
    _compute_tm_deriv(t, bern, n, degrees, TM_deriv)
    return TM_deriv


cdef void _add_weighting(double[:, :] points, double[:] weighting, double[:, :] wpoints, size_t r, size_t c) nogil:
    cdef size_t i, j
    for 0 <= i < r:
        for 0 <= j < c:
            wpoints[i, j] = weighting[i] * points[i, j]


def weighted_points(ndarray points, ndarray weighting):
    cdef:
        size_t r, c
        ndarray wpoints
    r = points.shape[0]
    c = points.shape[1]
    wpoints = empty((r, c), dtype=float64)
    if weighting is None:
        wpoints[:] = points
    else:
        _add_weighting(points, weighting, wpoints, r, c)
    return wpoints

        
cdef class BezierFit:
    cdef:
        readonly degrees
        readonly ndarray M, coef, lastX
        readonly double xmin, xmax
        
    def __init__(self, size_t degrees):
        self.degrees = degrees
        self.xmin = NAN
        self.xmax = NAN
        self.coef = None
        self.lastX = None
        
    cpdef ndarray compute_X(self, ndarray vals, size_t n, double xmin=NAN, double xmax=NAN):
        if isnan(xmin):
            xmin = self.xmin
        if isnan(xmax):
            xmax = self.xmax
        return compute_tm(vals[:, 0], self.degrees, n=n, minval=xmin, maxval=xmax)
            
    def fit(self, ndarray points, ndarray weighting=None):
        cdef:
            ndarray X, W, WX, X_tpose, fit_matrix
            size_t num_points = len(points)
        self.xmin = points[:, 0].min()
        self.xmax = points[:, 0].max()
        X = self.compute_X(points, num_points)
        X_tpose = X.T
        WX = weighted_points(X, weighting) 
        fit_matrix = dot(inv(dot(X_tpose, WX)), X_tpose)
        self.coef = dot(fit_matrix, weighted_points(points, weighting))
    
    def transform(self, ndarray points):
        cdef:
            size_t num_points = len(points)
            ndarray X
        if self.coef is None:
            raise ValueError('no coefficients set. fit first')
        return dot(self.compute_X(points, num_points), self.coef)

    cpdef ndarray compute_X_deriv(self, ndarray vals, size_t n, double xmin=NAN, double xmax=NAN):
        cdef double dx
        if isnan(xmin):
            xmin = self.xmin
        if isnan(xmax):
            xmax = self.xmax
        dx = 1.0 / (xmax - xmin)
        return compute_tm_deriv(vals[:, 0], self.degrees, n=n, minval=xmin, maxval=xmax) * dx

    def transform_deriv(self, ndarray points):
        cdef:
            size_t num_points = len(points)
            ndarray X
        if self.coef is None:
            raise ValueError('no coefficients set. fit first')
        return dot(self.compute_X_deriv(points, num_points), self.coef)
    
    cpdef ndarray fit_transform(self, ndarray points, ndarray weighting=None):
        self.fit(points, weighting=weighting)
        return self.transform(points)

    cpdef ndarray fit_transform_deriv(self, ndarray points, ndarray weighting=None):
        self.fit(points, weighting=weighting)
        return self.transform_deriv(points)

    cpdef void update_degrees(self, size_t degrees):
        self.degrees = degrees

    cpdef void copyfrom(self, bezier_fit):
        self.degrees = bezier_fit.degrees
        self.coef = bezier_fit.coef
        self.xmin = bezier_fit.xmin
        self.xmax = bezier_fit.xmax


# old hotness
def t_arr(ndarray x_arr):
    cdef:
        long n = x_arr.shape[0], i
        double last_diff
        ndarray t
        
    if n <= 1:
        t = zeros(1, dtype=float64)
        return t
    
    t = zeros(n, dtype=float64)
 
    last_diff = x_arr[n-1] - x_arr[0]
    t[n-1] = 1
    for i in range(1, n-1):
        t[i] = (x_arr[i] - x_arr[0])/last_diff
    
    return t

cdef long factorial(long n) nogil:
    cdef:
        long result = 1, i
    
    for i in range(n):
        result *= n - i

    return result

cdef long comb(long n, long i) nogil:
    if i <= n:
        return factorial(n) / (factorial(i) * factorial(n-i))
    else:
        return 0

def outer_product(ndarray[double, ndim=1] vec1, ndarray[double, ndim=1] vec2):
    cdef:
        long n1 = len(vec1), n2 = len(vec2), i, j
        ndarray res = zeros((n1, n2), dtype=float64)
    
    for i in range(n1):
        for j in range(n2):
            res[i, j] = vec1[i] * vec2[j]
    
    return res

def bernstein_deriv(int i, int n, double t):
    return n * (bernstein_poly_deriv(i-1, n-1, t) - bernstein_poly_deriv(i, n-1, t))

def bernstein_poly_deriv(int i, int n, double t):
    return comb(n, i) * (t ** (n-i)) * (1-t) ** i

def bernstein_poly(long i, long n, ndarray[double, ndim=1] t):
    return comb(n, i) * (t ** i) * (1-t) ** (n-i)

def bezier_curve(ndarray[double,ndim=2] points, ndarray[double, ndim=1] t_ish):
    cdef:
        long n_points = len(points), degree, i, n_t = len(t_ish)
        ndarray t = t_arr(t_ish)
        ndarray curve = zeros((n_t, 2), dtype=float64)
        long c

    degree = n_points - 1

    for i in range(n_points):
        curve += outer_product(bernstein_poly(i, degree, t), points[i])
  
    return curve.T

cdef double bernstein_basis(long i, long n, long k) nogil:
    return fpow(-1, i-k) * comb(n, k) * comb(k, i)

def transpose(ndarray M):
    cdef:
        long x = M.shape[0], y = M.shape[1], i, j
        ndarray MT = zeros((y, x), dtype=float64)
    
    for i in range(x):
        for j in range(y):
            MT[j, i] = M[i, j]
    
    return MT
  
def find_control_pts(ndarray[double,ndim=1] dataX, ndarray[double,ndim=1] dataY, long degree, ndarray[double, ndim=1] w=empty(1)):
    cdef:
        long n = len(dataX), i, j
        bint weightFlag = False
        ndarray M = zeros((degree+1, degree+1), dtype=float64)
        ndarray ts = zeros((degree+1, n), dtype=float64)
        ndarray t = t_arr(dataX)
        ndarray W = zeros((n, n), dtype=float64)
        ndarray pts = zeros((degree+1, 2), dtype=float64)

    if len(w) > 1:
        weightFlag = True
        
    for j in range(degree+1):
        for i in range(degree+1):
            M[j, i] = bernstein_basis(i, degree, j)

    for j in range(n):
        # here we build W matrix if there is a w parameter
        if weightFlag:
            W[j, j] = w[j]
        for i in range(degree+1):
            ts[i, j] = fpow(t[j], i)
             

    invM = inv(M)
    if weightFlag:
        TtT = inv(dot(ts, dot(W, transpose(ts))))
        almostThere = dot(dot(dot(invM, TtT), ts), W)
    else:
        TtT = inv(dot(ts, transpose(ts)))
        almostThere = dot(dot(invM, TtT), ts)
    
    pts[:, 0] = dot(almostThere, dataX)
    pts[:, 1] = dot(almostThere, dataY)
    return pts

