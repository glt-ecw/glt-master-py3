cimport cython
from numpy cimport ndarray
from numpy import zeros, float64
from pandas import Series
from scipy.stats import *

from libc.math cimport sqrt, exp, pow

cdef list describe_data_columns = ['count', 'min', 'max', 'mean', 'variance', 'skewness', 'kurtosis']

def describe_data(data):
    cdef:
        int cnt
        tuple minmax
        double mean, var, skewness, kurtosis
        ndarray[double, ndim=1] desc = zeros(7, dtype=float64)
    """
    same input as scipy.stats.describe
    """
    cnt, minmax, mean, var, skewness, kurtosis = describe(data)
    desc[0] = <double>cnt
    desc[1] = <double>minmax[0]
    desc[2] = <double>minmax[1]
    desc[3] = mean
    desc[4] = var
    desc[5] = skewness
    desc[6] = kurtosis
    return Series(desc, index=describe_data_columns)
    
# fast stats
def mean(double[:] measurements):
    return mean_fast(measurements, measurements.shape[0])

def stdev(double[:] measurements):
    cdef:
        long n = measurements.shape[0]
        double mean = mean_fast(measurements, n)
    return stdev_fast(measurements, n, mean)

def skew_kurt(double[:] measurements):
    cdef:
        long n = measurements.shape[0]
        double mean = mean_fast(measurements, n)
        double stdev = stdev_fast(measurements, n, mean)
    return skew_kurt_fast(measurements, n, mean, stdev)

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef double mean_fast(double[:] measurements, int n):
    cdef:
        double summed = 0
    for i in range(n):
        summed += measurements[i]
    return summed / n

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef double stdev_fast(double[:] measurements, int n, double mean):
    cdef:
        double summed = 0
    for i in range(n):
        summed += pow(measurements[i] - mean, 2)
    return sqrt(summed / n)

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef ndarray[double, ndim=1] skew_kurt_fast(double[:] measurements, int n, double mean, double stdev):
    cdef:
        double skew_sum = 0, kurt_sum = 0, dev
        ndarray[double, ndim=1] results = zeros(2, dtype=float64)
    for i in range(n):
        dev = measurements[i] - mean
        skew_sum += pow(dev, 3)
        kurt_sum += pow(dev, 4)
    results[0] = skew_sum / (n * pow(stdev, 3))
    results[1] = kurt_sum / (n * pow(stdev, 4)) - 3
    return results

# exponentially weighted stats
@cython.cdivision(True)
cpdef double ew_mean(double w, double dat, double prev_mean) nogil:
    return w * dat + (1 - w) * prev_mean

@cython.cdivision(True)
cpdef double ew_var(double w, double dat, double prev_var, double mean) nogil:
    return w * pow(dat - mean, 2) + (1 - w) * prev_var

@cython.cdivision(True)
cpdef double ew_skew(double w, double dat, double prev_skew, double mean, double std) nogil:
    return w * pow((dat - mean) / std, 3) + (1 - w) * prev_skew

@cython.cdivision(True)
cpdef double ew_kurt(double w, double dat, double prev_kurt, double mean, double std) nogil:
    return w * pow((dat - mean) / std, 4) + (1 - w) * (prev_kurt + 3) - 3

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
def ew_desc_array(double w, double[:] data, double seed=0):
    """
    univariate_kalman_array(double gain, double[:] measurements, double seed)
    
    results:
        kalman filter of measurements array
    """
    cdef:
        long tlen = data.shape[0]
        ndarray[double, ndim=2] results = zeros([tlen, 5], dtype=float64)
        double start = 0, mean, var, std, dat
    if seed != 0:
        start = seed
    else:
        start = data[0]
        
    for i in range(tlen):
        dat = data[i]
        if i == 0:
            results[i][0] = start
            var = ew_var(w, dat, 0, start)
            std = sqrt(var)
            results[i][1] = var
            results[i][2] = std
            results[i][3] = 0 if std == 0 else ew_skew(w, dat, 0, start, std)
            results[i][3] = 0 if std == 0 else ew_kurt(w, dat, 0, start, std)
            continue
        mean = ew_mean(w, dat, results[i-1][0])
        var = ew_var(w, dat, results[i-1][1], mean)
        std = sqrt(var)
        results[i][0] = mean
        results[i][1] = var
        results[i][2] = std
        results[i][3] = ew_skew(w, dat, results[i-1][3], mean, std)
        results[i][4] = ew_kurt(w, dat, results[i-1][4], mean, std)
    return results

