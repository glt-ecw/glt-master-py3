cdef struct UnivariateNode:
    double value
    double K
    double P

cdef UnivariateNode new_univariate_value(double q, double r, double datum, UnivariateNode node) nogil
cdef void fast_update_univariate_array(double q, double r, long n, double[:] data, double[:] results, bint use_seed, double seed) nogil
