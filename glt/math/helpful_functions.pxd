cdef double EPSILON

cdef bint double_eq(double a, double b, double epsilon=*) nogil
cdef bint double_gt(double a, double b, double epsilon=*) nogil
cdef bint double_gte(double a, double b, double epsilon=*) nogil
cdef bint double_lt(double a, double b, double epsilon=*) nogil
cdef bint double_lte(double a, double b, double epsilon=*) nogil
