from libc.math cimport fabs

cdef double EPSILON = 1e-7

cdef bint double_eq(double a, double b, double epsilon=EPSILON) nogil:
    return fabs(a - b) <= epsilon

cdef bint double_gt(double a, double b, double epsilon=EPSILON) nogil:
    return a - b > epsilon

cdef bint double_gte(double a, double b, double epsilon=EPSILON) nogil:
    return double_gt(a, b, epsilon) or double_eq(a, b, epsilon)

cdef bint double_lt(double a, double b, double epsilon=EPSILON) nogil:
    return double_gt(b, a, epsilon)

cdef bint double_lte(double a, double b, double epsilon=EPSILON) nogil:
    return double_gte(b, a, epsilon)
