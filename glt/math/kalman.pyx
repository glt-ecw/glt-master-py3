from __future__ import print_function
cimport cython
from numpy cimport ndarray
from numpy import zeros, float64, int64

#cdef inline tuple univariate_filter(double q, double r, double dat, double prev, double p):
#    cdef double pnew = p, k, val = prev
#    pnew += q
#    k = pnew/(pnew + r)
#    val += k * (dat - prev)
#    pnew *= (1-k)
#    return val, pnew, k

#cdef struct UnivariateNode:
#    double value
#    double K
#    double P

@cython.cdivision(True)
cdef UnivariateNode new_univariate_value(double q, double r, double datum, UnivariateNode prev) nogil:
    cdef:
        double p = prev.P, k, #value = prev.value, prev = value
        UnivariateNode node
    p += q
    k = p/(p + r)
    node.value = prev.value + k * (datum - prev.value)
    p *= (1-k)
    node.P = p
    node.K = k
    return node

def univariate_update(double q, double r, double datum, double val, double p, double k):
    cdef:
        UnivariateNode node

    node.P = p
    node.K = k
    node.value = val
    node = new_univariate_value(q, r, datum, node)
    return node.value, node.P, node.K

@cython.boundscheck(False)
cdef void fast_update_univariate_array(double q, double r, long n, double[:] data, double[:] results, bint use_seed, double seed) nogil:
    cdef:
        long i
        UnivariateNode node

    if use_seed:
        node.value = seed
    else:
        node.value = data[0]
    results[0] = node.value
    node.P = 0
    
    for i in range(1, n):
        node = new_univariate_value(q, r, data[i], node)
        results[i] = node.value

def univariate_array(double q, double r, ndarray data, bint use_seed=False, double seed=0):
    cdef:
        long n = data.shape[0]
        ndarray results = zeros(n, dtype=float64)
        
    fast_update_univariate_array(q, r, n, data, results, use_seed, seed)
    return results
