
import sys
import os
import gzip
import glob
import datetime as dt

import numpy as np
import pandas as pd

invalid_dtypes = [np.uint64]

def write_data(df, label, h5_filename, cold_dir=None, is_hot=True):    
    if not is_hot:
        if cold_dir is None:
            raise AttributeError("yikes! you need to specify cold_dir if you're not saving h5 format")     
        else:
            csv_filename = cold_formatter(h5_filename, label, cold_dir)
            with gzip.open(csv_filename, 'wb') as f:
                df.to_csv(f)
    else:
        valid_columns = []
        for col in df.columns:
            if df[col].dtype not in invalid_dtypes:
                valid_columns.append(col)
        hdf_config = {
            'mode': 'a',
            'complevel': 9,
            'complib': 'blosc',
            'format': 't',
            'data_columns': valid_columns
        }
        if df.index.name is None:
            df.index.name = 'idx'
        df.to_hdf(h5_filename, label, **hdf_config)
    return

def cold_prefix_formatter(h5_filename, h5_ext='h5'):
    h5_base = os.path.basename(h5_filename)
    ext = '.' + h5_ext.replace('.', '')
    return h5_base[:h5_base.find(ext)]

def cold_formatter(h5_filename, label, cold_dir):
    cold_prefix = cold_prefix_formatter(h5_filename)
    cold_base = cold_prefix + '__' + label.replace('/', '') + '.csv.gz'
    return os.path.join(cold_dir, cold_base)

def hot_to_cold(h5_filename, cold_dir, debug=False):
    if debug:
        print(dt.datetime.now(), 'reading', h5_filename)
    with pd.HDFStore(h5_filename, 'r') as h5:
        for key in list(h5.keys()):
            #cold_filename = cold_formatter(h5_filename, key, cold_dir)
            if debug:
                print(dt.datetime.now(), 'saving', key, 'to', cold_filename)
            write_data(h5[key], key, h5_filename, cold_dir=cold_dir, is_hot=False)
            #with gzip.open(cold_filename, 'wb') as f:
            #    h5[key].to_csv(f)
    if debug:
        print(dt.datetime.now(), 'done')
        
def cold_to_hot(cold_dir, h5_filename, debug=False):
    h5_prefix = cold_prefix_formatter(h5_filename)
    cold_globber = os.path.join(cold_dir, h5_prefix + '*.csv.gz')
    for cold_filename in glob.glob(cold_globber):
        cold_base = os.path.basename(cold_filename)
        h5_prefix, suffix = cold_base.split('__')
        label = suffix[:suffix.find('.csv.gz')]
        write_data(pd.read_csv(cold_filename, compression='gzip', index_col=0), label, h5_filename, is_hot=True)
