# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from libc.stdint cimport *
from libc.math cimport round as fround, NAN, fabs
from libcpp.vector cimport vector as vector_
from libcpp.unordered_map cimport unordered_map as unordered_map_
from glt.math.cpp cimport isnan
from glt.md.standard cimport (
    MDOrderBook, copy_mdorderbook, calc_wmid_from_mdorderbook, calc_imbalance, Option, OptionPair, OptionImpliedValues
)
from numpy cimport ndarray

from numpy import empty

    
cdef bint hacked_fill_mdorderbook(uint64_t secid, uint64_t time, double bidprc, int64_t bidqty, double askprc, int64_t askqty, double price_multiplier, MDOrderBook *md):
    cdef:
        int32_t int_bidprc = <int32_t>fround(price_multiplier * bidprc)
        int32_t int_askprc = <int32_t>fround(price_multiplier * askprc)
    md.secid = secid
    md.time = time
    md.exch_time = time
    md.bids[0].price = int_bidprc
    md.bids[0].qty = <int32_t>bidqty
    md.asks[0].price = int_askprc
    md.asks[0].qty = <int32_t>askqty
    return md.bids[0].qty > 0 and md.asks[0].qty > 0


cdef class MDSampler:
    cdef:
        MDOrderBook md
        MDOrderBook *md_ptr
        readonly:
            int32_t min_qty, min_participants, min_tick_width
            double min_imbalance_change
            double imbalance
        
    def __init__(self, int32_t min_qty=0, int32_t min_participants=0, int32_t min_tick_width=0, double min_imbalance_change=0.0):
        self.min_qty = min_qty
        self.min_participants = min_participants
        self.min_tick_width = min_tick_width
        self.min_imbalance_change = min_imbalance_change
        
        self.imbalance = 0
        self.md_ptr = &(self.md)
        
    cdef void _read_md(self, MDOrderBook *md):
        cdef:
            double imbalance
        
        copy_mdorderbook(md, self.md_ptr)
        


cdef class HackedKospiSyntheticMonitor:
    cdef:
        unordered_map_[int64_t, double] secid_strike_map
        unordered_map_[int64_t, int64_t] secid_op_map
        readonly:
            uint64_t secid_fut
            double price_multiplier, gain
        public:
            double fut_price, syn_price, k, call, put
            double fut_bid, fut_ask, syn_bid, syn_ask
            double seed_roll, raw_roll, roll
            
    def __init__(self, uint64_t secid_fut, dict option_defs, double seed_roll=NAN, double gain=0.0):
        cdef:
            int64_t secid
            dict defs
            
        for secid, defs in option_defs.iteritems():
            self.secid_strike_map[secid] = defs['k']
            self.secid_op_map[secid] = defs['op']
            
        self.secid_fut = secid_fut
        self.price_multiplier = 100.0
        self.gain = gain
        self.fut_price = NAN
        self.syn_price = NAN
        self.k = NAN
        self.raw_roll = NAN
        self.seed_roll = seed_roll
        self.roll = seed_roll
        
    cdef bint _calc_roll(self, MDOrderBook *md):
        cdef:
            int64_t op
            double k, wmid
            
        if md.bids[0].qty == 0 or md.asks[0].qty == 0 or (md.secid != self.secid_fut and isnan(self.fut_price)):
            return False
        
        if md.secid == self.secid_fut:
            self.fut_price = calc_wmid_from_mdorderbook(md, self.price_multiplier, 0.05)
            self.fut_bid = md.bids[0].price / self.price_multiplier
            self.fut_ask = md.asks[0].price / self.price_multiplier
            if isnan(self.roll):
                self.syn_price = self.fut_price
                self.syn_bid = self.fut_bid
                self.syn_ask = self.syn_ask
            else:
                self.syn_price = self.roll + self.fut_price
                self.syn_bid = self.roll + self.fut_bid
                self.syn_ask = self.roll + self.fut_ask
            return False
        elif self.seed_roll == 0:
            return False
        
        wmid = calc_wmid_from_mdorderbook(md, self.price_multiplier, 0.01)

        k = self.secid_strike_map[md.secid]
        if fabs(k - self.syn_price) < 2.5 and isnan(self.k):
            self.k = k
            self.call = NAN
            self.put = NAN
            return False
        elif self.call - self.put > 1.875 or self.call - self.put < -1.875:
            self.k = NAN
            self.call = NAN
            self.put = NAN
            
        if k == self.k:
            op = self.secid_op_map[md.secid]
            if op > 0:
                if self.call == wmid:
                    return False
                self.call = wmid
            else:
                if self.put == wmid:
                    return False
                self.put = wmid
            
            self.raw_roll = self.k + self.call - self.put - self.fut_price
            if isnan(self.raw_roll):
                return False
            elif isnan(self.roll):
                self.roll = self.raw_roll
            else:
                self.roll = self.gain * self.raw_roll + (1-self.gain) * self.roll
            return True
        else:
            return False

        
        
cdef class OptionPairMonitor:
    cdef:
        uint64_t secid_call, secid_put
        OptionPair pair
        
    def __init__(self, double strike, double tte, double interest_rate):
        self.secid_call = 0
        self.secid_put = 0
        self.pair.Init(strike, tte, interest_rate)
        
    def add_call_secid(self, uint64_t secid):
        self.secid_call = secid
        
    def add_put_secid(self, uint64_t secid):
        self.secid_put = secid
        
    cdef int16_t _which_op(self) nogil:
        return self.pair.WhichOTM()
    
    cdef bint _is_otm(self, uint64_t secid) nogil:
        cdef int16_t otm_op = self.pair.WhichOTM()
        return (otm_op > 0 and secid == self.secid_call) or (otm_op < 0 and secid == self.secid_put)
    
    cdef void _calc_implied_values(self, uint64_t secid, double bid_price, double ask_price) nogil:
        if self._is_otm(secid):
            self.pair.CalculateOTMImpliedValues(bid_price, ask_price, False)
            
    cdef void _update_underlying(self, double u_bid, double u_ask):
        self.pair.UpdateUnderlying(u_bid, u_ask)
        
    cdef double _bidvol(self) nogil:
        cdef OptionImpliedValues *implied = self.pair.ImpliedBidPtr()
        if implied == NULL:
            return -1
        return implied.vol
    
    cdef double _askvol(self) nogil:
        cdef OptionImpliedValues *implied = self.pair.ImpliedAskPtr()
        if implied == NULL:
            return -1
        return implied.vol
    
    cdef double _u_bid(self) nogil:
        return self.pair.u_bid
    
    cdef double _u_ask(self) nogil:
        return self.pair.u_ask
    
    cdef double _delta(self) nogil:
        cdef OptionImpliedValues *implied = self.pair.ImpliedBidPtr()
        if implied == NULL:
            return NAN
        return implied.delta
    
    cdef double _vega(self) nogil:
        cdef OptionImpliedValues *implied = self.pair.ImpliedBidPtr()
        if implied == NULL:
            return NAN
        return implied.vega
        

#def calc_synthetic(md, info, uint64_t interval, uint64_t refit_interval, double price_multiplier,
#                    double tte, double interest_rate,
#                    uint32_t secid_fut,
#                    double gain, double seed_roll):

def filter_and_calc_synthetic(md, info, double price_multiplier, uint32_t secid_fut, double gain, double seed_roll, double tte, double interest_rate):
    cdef:
        uint64_t i, bid_j, ask_j
        # for info
        uint64_t n_info = len(info)
        int64_t[:] info_secids = info['secid'].values
        double[:] info_strikes = info['strike'].values
        int64_t[:] info_ops = info['op'].values
        dict option_defs = {}
        # for df
        uint64_t n = len(md)
        int64_t[:] times = md['time'].astype('<i8').values
        int64_t[:] secids = md['secid'].values
        double[:, :] bidprcs = md[filter(lambda c: 'bidprc' in c, md.columns)].values
        double[:, :] askprcs = md[filter(lambda c: 'askprc' in c, md.columns)].values
        int64_t[:, :] bidqtys = md[filter(lambda c: 'bidqty' in c, md.columns)].values
        int64_t[:, :] askqtys = md[filter(lambda c: 'askqty' in c, md.columns)].values
        int64_t[:, :] bidpars = md[filter(lambda c: 'bidpar' in c, md.columns)].values
        int64_t[:, :] askpars = md[filter(lambda c: 'askpar' in c, md.columns)].values
        MDOrderBook md_hack
        HackedKospiSyntheticMonitor synthetic_monitor
        # implied vols
        bint valid
        dict option_monitors = {}
        #double imbalance, pct_on_bid
        #uint64_t secid
        OptionPairMonitor op_monitor
        OptionImpliedValues *implied_bid
        OptionImpliedValues *implied_ask
        list output_dtype
        ndarray output
        uint64_t count = 0
        uint64_t MAX_DEPTH = 5
        
    output_dtype = [
        ('roll', '<f8'),
        ('u_bid', '<f8'),
        ('u_ask', '<f8'),
        ('bidprc', '<f8'),
        ('bidqty', '<f8'),
        ('bidpar', '<f8'),
        ('askprc', '<f8'),
        ('askqty', '<f8'),
        ('askpar', '<f8'),
        #('bid_j', '<i8'),
        #('ask_j', '<i8'),
        ('bidvol', '<f8'), 
        ('askvol', '<f8'), 
        ('wvol', '<f8'), 
        #('which', '<i8'), 
        #('u_bid', '<f8'), 
        #('u_ask', '<f8'), 
        ('delta', '<f8'), 
        ('vega', '<f8'), 
        ('imbalance', '<f8')
    ]
    output = empty(n, dtype=output_dtype)
    
    #output[:]['time'] = 0
    output[:]['roll'] = NAN
    output[:]['u_bid'] = NAN
    output[:]['u_ask'] = NAN
    output[:]['bidprc'] = NAN
    output[:]['bidqty'] = NAN
    output[:]['bidpar'] = NAN
    output[:]['askprc'] = NAN
    output[:]['askqty'] = NAN
    output[:]['askpar'] = NAN
    output[:]['bidvol'] = NAN
    output[:]['askvol'] = NAN
    output[:]['delta'] = NAN
    output[:]['vega'] = NAN
    output[:]['wvol'] = NAN
    #output[:]['bid_j'] = -1
    #output[:]['ask_j'] = -1
    output[:]['imbalance'] = NAN
        
    for 0 <= i < n_info:
        if info_secids[i] == secid_fut:
            continue
        option_defs[info_secids[i]] = { 'k': info_strikes[i], 'op': info_ops[i] }
        if info_strikes[i] not in option_monitors:
            option_monitors[info_strikes[i]] = OptionPairMonitor(info_strikes[i], tte, interest_rate)
        pair = option_monitors[info_strikes[i]]
        if info_ops[i] < 0:
            pair.add_put_secid(info_secids[i])
        else:
            pair.add_call_secid(info_secids[i])

    synthetic_monitor = HackedKospiSyntheticMonitor(secid_fut, option_defs, seed_roll=seed_roll, gain=gain)
    
    for 0 <= i < n:
        # bid side
        bid_j = 0
        while bid_j < MAX_DEPTH:
            if bidpars[i, bid_j] > 1:
                break
            bid_j += 1
        if bid_j == MAX_DEPTH:
            continue
        # ask side
        ask_j = 0
        while ask_j < MAX_DEPTH:
            if askpars[i, ask_j] > 1:
                break
            ask_j += 1
        if ask_j == MAX_DEPTH:
            continue
        
        valid = hacked_fill_mdorderbook(secids[i], times[i], bidprcs[i, bid_j], bidqtys[i, bid_j], askprcs[i, ask_j], askqtys[i, ask_j], price_multiplier, &md_hack)
        
        if not valid: # and md_hack.asks[0].price != 1:
            continue
            
        synthetic_monitor._calc_roll(&md_hack)
        
        if secids[i] == secid_fut:
            output[i]['roll'] = synthetic_monitor.roll
            output[i]['u_bid'] = synthetic_monitor.syn_bid
            output[i]['u_ask'] = synthetic_monitor.syn_ask
            for secid, op_monitor in option_monitors.iteritems():
                op_monitor._update_underlying(bidprcs[i, bid_j] + synthetic_monitor.roll, askprcs[i, ask_j] + synthetic_monitor.roll)
        elif secids[i] in option_defs:
            output[i]['bidprc'] = bidprcs[i, bid_j]
            output[i]['bidqty'] = bidqtys[i, bid_j]
            output[i]['bidpar'] = bidpars[i, bid_j]
            output[i]['askprc'] = askprcs[i, ask_j]
            output[i]['askqty'] = askqtys[i, ask_j]
            output[i]['askpar'] = askpars[i, ask_j]
            op_monitor = option_monitors[option_defs[secids[i]]['k']]
            if not op_monitor._is_otm(secids[i]): # or askprcs[i] < 0.015:
                continue
            op_monitor._calc_implied_values(secids[i], bidprcs[i, bid_j], askprcs[i, ask_j])
            output[i]['bidvol'] = op_monitor._bidvol()
            output[i]['askvol'] = op_monitor._askvol()
            if md_hack.asks[0].price - md_hack.bids[0].price > 1:
                imbalance = 0
            else:
                imbalance = calc_imbalance(md_hack.bids[0].qty, md_hack.asks[0].qty)
            output[i]['imbalance'] = imbalance
            output[i]['delta'] = op_monitor._delta()
            output[i]['vega'] = op_monitor._vega()
            pct_on_bid = 0.5 * (1 - imbalance)
            output[i]['wvol'] = pct_on_bid * op_monitor._bidvol() + (1 - pct_on_bid) * op_monitor._askvol() 
        
    print len(md), count
    
    return output
        
    
def calc_resampled_implied_data(otm_bidprc, otm_askprc, double[:] u_bids, double[:] u_asks, double tte, double interest_rate, bint as_market_taker):
    cdef:
        uint64_t n = len(otm_bidprc), i, j
        double[:] strikes = otm_bidprc.columns.values * 1e-2
        double[:, :] bidprcs = otm_bidprc.values
        double[:, :] askprcs = otm_askprc.values
        uint64_t n_strikes = len(strikes)
        double strike, u_bid, u_ask, bidprc, askprc
        int op
        vector_[OptionPair] option_pairs
        OptionPair option_pair
        OptionPair *option_pair_ptr
        OptionImpliedValues *implied_bid
        OptionImpliedValues *implied_ask
        ndarray bidvols, askvols, deltas, vegas, xdeltas
        
    bidvols = empty((n, n_strikes), dtype='<f8')
    askvols = empty((n, n_strikes), dtype='<f8')
    deltas = empty((n, n_strikes), dtype='<f8')
    vegas = empty((n, n_strikes), dtype='<f8')
    
    bidvols[:, :] = NAN
    askvols[:, :] = NAN
    deltas[:, :] = NAN
    vegas[:, :] = NAN
        
    for 0 <= j < n_strikes:
        option_pair.Init(strikes[j], tte, interest_rate)
        option_pairs.push_back(option_pair)
        
    for 0 <= i < n:
        u_bid = u_bids[i]
        u_ask = u_asks[i]
        if isnan(u_bid) or isnan(u_ask):
            continue
        for 0 <= j < n_strikes:
            strike = strikes[j]
            option_pair_ptr = &(option_pairs[j])
            if strike > u_ask:
                op = 1
            else:
                op = -1
            bidprc = bidprcs[i, j]
            askprc = askprcs[i, j]
            option_pair_ptr.UpdateUnderlying(u_bid, u_ask)
            option_pair_ptr.CalculateOTMImpliedValues(bidprc, askprc, as_market_taker)
            implied_bid = option_pair_ptr.ImpliedBidPtr()
            implied_ask = option_pair_ptr.ImpliedAskPtr()
            if not isnan(bidprc) and implied_bid != NULL:
                bidvols[i, j] = implied_bid.vol
                deltas[i, j] = implied_bid.delta
                vegas[i, j] = implied_bid.vega
            if not isnan(askprc) and implied_ask != NULL:
                askvols[i, j] = implied_ask.vol
            if bidvols[i, j] >= askvols[i, j]:
                bidvols[i, j] = NAN
                askvols[i, j] = NAN
                
    return bidvols, askvols, deltas, vegas
