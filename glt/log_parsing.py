import re

### JPX ###
jpx_conn_regex = re.compile((
    'CONNECTION_PARMS\|pid\|([0-9]+).*remoteAddr\|([0-9]{1,3}\.[0-9]{1,3}\.'
    '[0-9]{1,3}\.[0-9]{1,3}).*'))

jpx_log_config = {
    'MO31': {
        'regex': re.compile(
            ('^([0-9]{8}T[0-9]{6}\.[0-9]{6,9}).*(MO31).* deioId: ([0-9]+), connId: '
            '([0-9]+), timeDiff: ([0-9]+), series: (\[[0-9\, ]+\]), premium: '
            '([\-\.0-9]+), qty: ([0-9]+), side: ([0-9]+), block: ([0-9]+), timeValid: '
            '([0-9]+), orderType: ([0-9]+), exInfo: (\[[a -f0-9]+\]),.* qwId: '
            '(\[[a-f0-9:]+\]), errMsg: \[(.*)\], respTime:.*')
        ),
        'cols': [
            'timestr', 'msg', 'deioid', 'connid', 'acktime', 'series', 'price', 
            'qty', 'side', 'block', 'timevalid', 'ordertype', 'exchinfo', 'qwid', 'err_msg', 'conn_label', 
            'conn_num', 'gateway'
        ],
        'int_cols': [
            'deioid', 'acktime', 'side', 'qty', 'timevalid', 
            'ordertype', 'conn_num'
        ],
        'float_cols': ['price'],
        'del_cols': ['timestr']
    },
    'MO33': {
        'regex': re.compile(
            ('^([0-9]{8}T[0-9]{6}\.[0-9]{6,9}).*(MO33).* deioId: ([0-9]+), connId: '
            '([0-9]+), timeDiff: ([0-9]+), series: (\[[0-9\, ]+\]), qwId: '
            '(\[[a-f0-9:]+\]), side: ([0-9]+)], premium: ([\-\.0-9]+), qty: '
            '([0-9]+), deltaQty: ([0-9]+),.* qwId: (\[[a-f0-9:]+\]), errMsg: \[(.*)\], respTime:.*')
        ),
        'cols': [
            'timestr', 'msg', 'deioid', 'connid', 'acktime', 'series', 'qwid', 'side', 'price', 
            'qty', 'deltaqty', 'qwid_mod', 'err_msg', 'conn_label', 'conn_num', 'gateway'
        ],
        'int_cols': ['deioid', 'acktime', 'side', 'qty', 'conn_num'],
        'float_cols': ['price'],
        'del_cols': ['timestr']
    },
     'MO4': {
         'regex': re.compile(
            ('^([0-9]{8}T[0-9]{6}\.[0-9]{6,9}).*(MO4).* deioId: ([0-9]+), connId: '
            '([0-9]+), timeDiff: ([0-9]+), series: (\[[0-9\, ]+\]), qwId: '
            '(\[[a-f0-9:]+\]), side: ([0-9]+),.*')
        ),
        'cols': [
            'timestr', 'msg', 'deioid', 'connid', 'acktime', 'series', 'qwid', 'side',
            'conn_label', 'conn_num', 'gateway' 
        ],
        'int_cols': ['deioid', 'acktime', 'side', 'conn_num'],
        'float_cols': None,
        'del_cols': ['timestr']
    },
    'MO41': {
        'regex': re.compile(
            ('^([0-9]{8}T[0-9]{6}\.[0-9]{6,9}).*(MO41).* deioId: ([0-9]+), '
             'timeDiff: ([0-9]+),.* premium: ([\-\.0-9]+), qty: ([0-9]+), '
             'side: ([0-9]+), stopCondition: ([0-9]+), stopPremium: ([0-9]+)'
             ',.* timeValid: ([0-9]+), orderType: ([0-9]+),.* '
             'qwId: (\[[a-f0-9:]+\]), errMsg: \[(.*)\], respTime:.*'
             '\[(?:[A-F0-9]{2} ){98}([A-F0-9]{2} [A-F0-9]{2}).*\]\]$')
        ),
        'cols': [
            'timestr', 'msg', 'deioid', 'acktime', 'price', 'qty', 'side', 
            'stop_condition', 'stop_premium', 'timevalid', 'ordertype', 
            'qwid', 'err_msg', 'conn_label', 'conn_num', 'gateway'
        ],
        'int_cols': ['deioid', 'acktime', 'side', 'qty', 'conn_num'],
        'float_cols': ['price', 'stop_premium'],
        'del_cols': ['timestr']
    },
    'BD6': {
        'regex': re.compile(
            ('^([0-9]{8}T[0-9]{6}\.[0-9]{6,9}).*\[([A-Z0-9]+), numItems: '
             '([0-9]).*, series: (\[[0-9\, ]+\]), seqNum: ([0-9]+), serverId: ([0-9]+), deioId: ([0-9]+), '
             'qwId: (\[[a-f0-9\:]+\]), dealId: ([0-9]+), dealPrice: '
             '([\-\.0-9]+), dealQty: ([0-9]+), side: ([0-9]+), dealSrc: '
             '([0-9]+),.*\[(?:(?:[A-F0-9]{2} ){202}([A-F0-9]{2} [A-F0-9]{2}).*)\]')
        ),
        'cols': [
            'timestr', 'msg', 'num_items', 'series', 'seqnum', 'orig_serverid', 'deioid',
            'qwid', 'dealid', 'dealprice', 'dealqty', 'side', 'dealsrc', 
            'conn_label', 'conn_num', 'gateway'
        ],
        'int_cols': [
            'num_items', 'seqnum', 'orig_serverid', 'deioid', 'dealid', 
            'dealqty', 'side', 'dealsrc', 'conn_num'
        ],
        'float_cols': ['dealprice'],
        'del_cols': ['timestr']
    },
    'BO41': {
        'regex': re.compile(
            ('^([0-9]{8}T[0-9]{6}\.[0-9]{6,9}).*(BO41).* qwid: (\[[a-f0-9:]+\]).*')
        ),
        'cols': ['timestr', 'msg', 'qwid', 'conn_label', 'conn_num', 'gateway'],
        'int_cols': ['conn_num'],
        'float_cols': None,
        'del_cols': ['timestr', 'conn_label']
    }
}

def jpx_ormsg_parse(lines, msg):
    if msg not in config:
        raise ValueError
    s = jpx_log_config[msg]['regex']
    c = jpx_conn_regex
    last_conn, last_gateway = None, None
    for line in lines:
        if 'PARM' in line:
            results = c.findall(line)
            last_conn, last_gateway = results[0]
        else:
            result = s.findall(line)
            if result:
                yield add_conn_info(result[0], last_conn, last_gateway)

def jpx_add_conn_info(result, last_conn, last_gateway):
    begin = result
    end = result[-1].split()
    if len(end) > 1:
        try:
            conn = ''.join(chr(int(x, 16)) for x in end)
            begin = begin[:-1]
        except:
            conn='NA'
    else:
        conn = 'NA'
    if last_conn is None:
        return begin + (conn, '', '')
    else:
        return begin + (conn, last_conn, last_gateway)
