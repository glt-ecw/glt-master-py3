import smtplib
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def send_gmail(username, password, recipients, subject, message):
    if type(recipients) == str:
        recipients = [recipients]

    msg = MIMEMultipart()
    msg['From'] = username
    msg['To'] = ', '.join(recipients)
    msg['Subject'] = subject
    
    msg.attach(MIMEText(message, 'html'))        

    server = smtplib.SMTP()
    server.connect('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.login(username, password)    
    server.sendmail(username, recipients, msg.as_string())
    server.close()
