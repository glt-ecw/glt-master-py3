cimport cython
from numpy cimport ndarray
from numpy import zeros, float64, int64

cdef Data[:] new_data_array(unsigned int size):
    cdef Data[:] data = ndarray(size, dtype=[('time', int64), ('side', int64), ('price', int64), ('qty', int64)])
    return data

# ArrayQueue functions

cdef ArrayQueue new_arrayqueue(unsigned int alloced):
    cdef ArrayQueue queue
    if alloced <= 0:
        alloced = 16
    queue.data = new_data_array(alloced)
    queue.head = 0
    queue.length = 0
    queue.alloced = alloced
    return queue

cdef void resize_arrayqueue(ArrayQueue *queue):
    cdef:
        unsigned int alloced = queue.alloced, newsize = 2*alloced, j = 0, length = queue.length, head = queue.head
        Data[:] data = queue.data, newdata = new_data_array(newsize)
    for j in range(queue.length):
        newdata[j] = data[(head+j)%alloced]
    queue.head = 0
    queue.alloced = newsize
    queue.data = newdata
    del data
    return

cdef void push_arrayqueue(ArrayQueue *queue, long time, long side, long price, long qty):
    cdef:
        length = queue.length
        Data newdata
    if length == queue.alloced:
        resize_arrayqueue(queue)
    newdata.time = time
    newdata.side = side
    newdata.price = price
    newdata.qty = qty
    queue.data[(queue.head+length)%queue.alloced] = newdata
    queue.length += 1
    return

@cython.boundscheck(False)
cdef Data pop_arrayqueue(ArrayQueue *queue) nogil:
    cdef Data data
    if queue.length <= 0:
        data.side = 0
        data.qty = 0
        return data
    data = queue.data[queue.head]
    queue.head = (queue.head+1)%queue.alloced
    queue.length -= 1
    return data

@cython.boundscheck(False)
cdef void clear_arrayqueue(ArrayQueue *queue) nogil:
    queue.head = 0
    queue.length = 0

# ArrayStack functions

cdef ArrayStack new_arraystack(unsigned int alloced):
    cdef ArrayStack stack
    if alloced <= 0:
        alloced = 16
    stack.data = new_data_array(alloced)
    stack.length = 0
    stack.alloced = alloced
    return stack

cdef void resize_arraystack(ArrayStack *stack):
    cdef:
        unsigned int alloced = stack.alloced, newsize = 2*alloced, i = 0, length = stack.length
        Data[:] data = stack.data, newdata = new_data_array(newsize)
    for i in range(stack.length):
        newdata[i] = data[i]
    stack.alloced = newsize
    stack.data = newdata
    del data
    return

cdef void push_arraystack(ArrayStack *stack, long time, long side, long price, long qty):
    cdef:
        length = stack.length
        Data newdata
    if length == stack.alloced:
        resize_arraystack(stack)
    newdata.time = time
    newdata.side = side
    newdata.price = price
    newdata.qty = qty
    stack.data[length] = newdata
    stack.length += 1
    return

@cython.boundscheck(False)
cdef Data pop_arraystack(ArrayStack *stack) nogil:
    cdef Data data
    if stack.length <= 0:
        data.side = 0
        data.qty = 0
        return data
    data = stack.data[stack.length-1]
    stack.length -= 1
    return data

@cython.boundscheck(False)
cdef void clear_arraystack(ArrayStack *stack) nogil:
    stack.length = 0
