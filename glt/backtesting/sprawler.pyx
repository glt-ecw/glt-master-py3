# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from cython.operator cimport dereference, preincrement
from libc.stdint cimport *
from libc.math cimport NAN, fabs
from libcpp.vector cimport vector as vector_
from libcpp.unordered_map cimport unordered_map as map_
from glt.md.standard cimport BookLevel, MDOrderBook, VolumeAccumulator, copy_mdorderbook, Option 
from glt.backtesting.base cimport StrategyInterface, SimulatedOrderAck


cdef uint16_t SIDE_BID = 1
cdef uint16_t SIDE_ASK = 2

cdef uint64_t KRX_MAX_LEVELS = 5

cdef int32_t KRX_MINIMUM_PRICE = 100

cdef int32_t KOSPI_OPTION_MIN_TICK = 100
cdef int32_t KOSPI_FUTURE_MIN_TICK = 500

cdef int32_t OFF_MARKET_TICKS = 2


cdef struct OrderInfo:
    uint16_t side
    int32_t price
    int32_t order_qty
    int32_t prev_qty
    int32_t rem_qty


cdef struct SprintedOrderStatus:
    uint16_t side
    int32_t price
    int32_t qty
    int32_t remaining
    int32_t hedged
    int32_t futures_hedged


cdef struct OrderStatus:
    uint64_t secid
    uint16_t side
    int32_t price
    int32_t qty
    int32_t remaining


cdef struct Position:
    int32_t filled
    int32_t working


cdef OrderInfo new_order_info(int32_t price, uint16_t side, int32_t qty) nogil:
    cdef OrderInfo o
    o.price = price
    o.side = side
    o.order_qty = qty
    o.prev_qty = qty
    o.rem_qty = qty
    return o


cdef OrderStatus reset_order_status(uint64_t secid, int32_t price, uint16_t side, int32_t qty, OrderStatus *status) nogil:
    status.secid = secid
    status.side = side
    status.price = price
    status.qty = qty
    status.remaining = qty


cdef class KospiOptionQPOSValuatorV1(StrategyInterface):
    cdef:
        map_[uint64_t, OrderStatus] sprinted_orders, futures_orders, scratch_orders, scalp_orders
        map_[int32_t, uint64_t] bid_scalp_orders, ask_scalp_orders, bid_sprinter_orders, ask_sprinter_orders
        MDOrderBook option_md, futures_md
        Option option
        Position option_position, futures_position
        readonly:
            bint already_sprinted
            str name
            uint64_t option_secid, futures_secid
            double sprinter_bypass_pct
            int32_t bid_order_size, ask_order_size, min_tick
            int32_t num_bid_sprinters, num_ask_sprinters
            # options valuation
            int16_t option_type
            double strike, tte, interest_rate, delta
            double price_multiplier
            double option_mid, futures_mid
            # scratch dat
            uint64_t queued_bid_triggerid, queued_ask_triggerid
            int32_t queued_bid_price, queued_ask_price
            uint64_t last_bid_scratch_id, last_ask_scratch_id
            int32_t bid_cancel_threshold, ask_cancel_threshold

    def __init__(self, uint64_t option_secid, uint64_t futures_secid, 
            int16_t option_type, double strike, double tte, double interest_rate, 
            int32_t bid_order_size, int32_t ask_order_size, 
            int32_t bid_cancel_threshold, int32_t ask_cancel_threshold, name=''):
        self.option_secid = option_secid
        self.futures_secid = futures_secid
        self.name = str(name)
        self.initialize([option_secid, futures_secid])

        self.sprinter_bypass_pct = 1.0
        #self.max_order_size = 2000
        self.bid_order_size = bid_order_size
        self.ask_order_size = ask_order_size
        self.bid_cancel_threshold = bid_cancel_threshold
        self.ask_cancel_threshold = ask_cancel_threshold
        self.min_tick = KOSPI_OPTION_MIN_TICK
        self.num_bid_sprinters = 10
        self.num_ask_sprinters = 10
        self.already_sprinted = False

        self.option_position.filled = 0
        self.option_position.working = 0
        self.futures_position.filled = 0
        self.futures_position.working = 0

        self.option_type = option_type
        self.strike = strike
        self.tte = tte
        self.interest_rate = interest_rate
        self.option.Init(self.option_type, self.strike, self.tte, self.interest_rate)
        self.price_multiplier = 0.0001

        self.option_mid = NAN
        self.futures_mid = NAN
        self.delta = NAN

        self.queued_bid_triggerid = 0
        self.queued_bid_price = 0
        self.queued_ask_triggerid = 0
        self.queued_ask_price = 0
        self.last_bid_scratch_id = 0
        self.last_ask_scratch_id = 0

    def __repr__(self):
        return '%s{%s, opt=%d, fut=%d, name=%s}' % (self.__class__.__name__, hex(id(self)), self.option_secid, self.futures_secid, self.name)

    cdef void _recalc_delta(self) nogil:
        cdef:
            Option *option = &(self.option)
        option.CalculateImpliedValues(self.option_mid, self.futures_mid)
        self.delta = option.implied.delta
        
    cdef void _attempt_sprint(self):
        cdef:
            MDOrderBook *md = self.md_ptr
            uint64_t i, idx 
            int32_t order_price, order_qty
            bint past_minimum_bid = False
            uint64_t client_orderid

        # check that all book levels are filled... if the lowest bid is at a price of 1, it's okay
        for 0 <= i < KRX_MAX_LEVELS:
            if md.bids[i].qty == 0 and not past_minimum_bid:
                return
            if md.asks[i].qty == 0:
                return
            if md.bids[i].price == KRX_MINIMUM_PRICE:
                past_minimum_bid = True

        print self, 'sprinting at seqnum=%d, %d/%d (%dx%d)' % (md.seqnum, md.bids[0].price, md.asks[0].price, md.bids[0].qty, md.asks[0].qty)
        # okay, every level has qty... let's place some orders
        # start with the second price
        order_price = md.bids[1].price
        for 0 <= i < self.num_bid_sprinters:
            if order_price <= KRX_MINIMUM_PRICE:
                break
            if i >= KRX_MAX_LEVELS:
                idx = KRX_MAX_LEVELS - 1 
            else:
                idx = i
            if md.bids[idx].qty > self.bid_order_size:
                print self, 'placing sprinter... bidprc=%d, bidqty=%d' % (md.bids[idx].price, md.bids[idx].qty)
                self._place_sprint_order(order_price, SIDE_BID, self.bid_order_size)
            else:
                print self, 'not placing bid sprinter: qty (%d) <= self.bid_order_size (%d)' % (md.bids[idx].qty, self.bid_order_size)
            order_price -= self.min_tick

        order_price = md.asks[1].price
        for 0 <= i < self.num_ask_sprinters:
            if i >= KRX_MAX_LEVELS:
                idx = KRX_MAX_LEVELS - 1
            else:
                idx = i
            if md.asks[idx].qty > self.ask_order_size:
                print self, 'placing sprinter... askprc=%d, askqty=%d' % (md.asks[idx].price, md.asks[idx].qty)
                self._place_sprint_order(order_price, SIDE_ASK, self.ask_order_size)
            else:
                print self, 'not placing offer sprinter: qty (%d) <= self.ask_order_size (%d)' % (md.asks[idx].qty, self.ask_order_size)
            order_price += self.min_tick

        # let's never do this again
        print self, 'successful sprint! delta=%0.4f' % (self.delta, )
        self.already_sprinted = True

    cdef uint64_t _place_new_order(self, MDOrderBook *md, uint64_t secid, 
            int32_t price, uint16_t side, int32_t qty, str context, 
            map_[uint64_t, OrderStatus] *status_map):
        cdef uint64_t client_orderid
        client_orderid = self.place_limit_order(md.time, md.seqnum, secid, price, side, qty, context)
        reset_order_status(secid, price, side, qty, &(dereference(status_map)[client_orderid]))
        print self, (
            'LIMIT (NEW REQUEST): client_orderid=%d, secid=%d, seqnum=%d, price=%d, side=%d, qty=%d (%d)'
        ) % (client_orderid, secid, md.seqnum, price, side, qty, dereference(status_map)[client_orderid].qty)
        return client_orderid
        
    cdef uint64_t _place_sprint_order(self, int32_t price, uint16_t side, int32_t qty):
        cdef:
            MDOrderBook *md = self.md_ptr
            uint64_t client_orderid
        client_orderid = self.place_sprint_order(
            md.time, md.seqnum, self.option_secid, price, side, qty, 'sprinter order', self.sprinter_bypass_pct
        )
        reset_order_status(self.option_secid, price, side, qty, &(self.sprinted_orders[client_orderid]))
        if side == SIDE_BID:
            self.bid_sprinter_orders[price] = client_orderid
        else:
            self.ask_sprinter_orders[price] = client_orderid
        print self, (
            'SPRINTER (NEW REQUEST): client_orderid=%d, seqnum=%d, price=%d, side=%d, qty=%d (%d)'
        ) % (client_orderid, md.seqnum, price, side, qty, self.sprinted_orders[client_orderid].qty)
        return client_orderid
       
    cdef void _manage_option_orderstatus(self, OrderStatus *order_status, SimulatedOrderAck *ack, MDOrderBook *md, str which_type):
        cdef:
            int32_t working, qty_change
        qty_change = order_status.remaining - ack.remaining_qty
        if qty_change > 0:
            if order_status.side == SIDE_BID:
                self.option_position.filled += qty_change
            else:
                self.option_position.filled -= qty_change
            working = <int32_t>(self.delta * self.option_position.filled + self.futures_position.filled + self.futures_position.working)
            # process futures hedge??
            if working >= 1:
                context = 'hedge for strategy=%s, client_orderid=%d' % (self, ack.client_orderid)
                self._place_new_order(
                    md, self.futures_secid, self.futures_md.bids[0].price, SIDE_ASK, 
                    working, context, &(self.futures_orders)
                )
            elif working <= -1:
                context = 'hedge for strategy=%s, client_orderid=%d' % (self, ack.client_orderid)
                self._place_new_order(
                    md, self.futures_secid, self.futures_md.asks[0].price, SIDE_BID, 
                    -working, context, &(self.futures_orders)
                )
            self.futures_position.working -= working
            print self, (
                'LIMIT (%s; FILL): client_orderid=%d, price=%d, side=%d, '
                'fill_qty=%d, remaining=%d, option_position=%d, delta=%0.3f, '
                'option_exposure=%0.1f, '
                'futures.working=%d, futures.filled=%d'
            ) % (
                which_type, ack.client_orderid, order_status.price, order_status.side, 
                qty_change, ack.remaining_qty, self.option_position.filled, self.delta,
                self.delta * self.option_position.filled, 
                self.futures_position.working, self.futures_position.filled
            )
        else:
            print self, (
                'LIMIT (%s; ACCEPTED): client_orderid=%d, price=%d, side=%d, qty=%d'
            ) % (which_type, ack.client_orderid, order_status.price, order_status.side, order_status.qty)
        order_status.remaining = ack.remaining_qty
        

    cdef void _cancel_order(self, uint64_t client_orderid, map_[uint64_t, OrderStatus] *status_map, str otype) except *:
        cdef OrderStatus *order_status
        if status_map.find(client_orderid) == status_map.end():
            err = '%s cannot find %s client_orderid=%d in status_map. debug please' % (self, otype, client_orderid)
            raise ValueError(err)
        order_status = &(dereference(status_map)[client_orderid])
        print self, (
            'IGNORE %s ORDER: client_orderid=%d, price=%d, filled=%d'
        ) % (otype, client_orderid, order_status.price, order_status.qty - order_status.remaining)
        status_map.erase(client_orderid)

    def evaluate_md(self):
        cdef:
            MDOrderBook *md = NULL
            MDOrderBook *option_md = NULL
            MDOrderBook *futures_md = NULL
            SimulatedOrderAck ack
            uint64_t client_orderid
            int32_t qty_change
            OrderStatus *order_status = NULL
            int32_t working
            cdef int32_t scalp_price, order_size

        if not self.valid_md:
            return

        md = self.md_ptr
        if md.bids[0].qty == 0 or md.asks[0].qty == 0:
            return

        option_md = &(self.option_md)
        futures_md = &(self.futures_md)

        # let's evaluate some acks!
        while self._has_acks():
            ack = self._top_ack()
            if self.sprinted_orders.find(ack.client_orderid) != self.sprinted_orders.end():
                # sprinter...
                order_status = &(self.sprinted_orders[ack.client_orderid])
                self._manage_option_orderstatus(order_status, &ack, md, 'SPRINTER')
                if ack.remaining_qty == 0:
                    if order_status.side == SIDE_BID:
                        self.bid_sprinter_orders.erase(order_status.price)
                        print self, '>> get ready to scratch if qty <= %d' % self.bid_order_size
                        self.queued_ask_price = order_status.price
                        self.queued_ask_triggerid = ack.client_orderid
                        scalp_price = self.queued_ask_price + KOSPI_OPTION_MIN_TICK
                        client_orderid = self._place_new_order(
                            md, self.option_secid, scalp_price, SIDE_ASK, 
                            self.bid_order_size, 'scalp client_orderid=%d' % ack.client_orderid, &(self.scalp_orders)
                        )
                        self.ask_scalp_orders[scalp_price] = client_orderid
                        print self, '>> but for now, scalp!', client_orderid
                    else:
                        self.ask_sprinter_orders.erase(order_status.price)
                        print self, '>> get ready to scratch if qty <= %d' % self.ask_order_size
                        self.queued_bid_price = order_status.price
                        self.queued_bid_triggerid = ack.client_orderid
                        scalp_price = self.queued_bid_price - KOSPI_OPTION_MIN_TICK
                        client_orderid = self._place_new_order(
                            md, self.option_secid, scalp_price, SIDE_BID, 
                            self.ask_order_size, 'scalp client_orderid=%d' % ack.client_orderid, &(self.scalp_orders)
                        )
                        self.bid_scalp_orders[scalp_price] = client_orderid
                        print self, '>> but for now, scalp!', client_orderid
                    # finally remove from order map
                    self.sprinted_orders.erase(ack.client_orderid)
            elif self.scratch_orders.find(ack.client_orderid) != self.scratch_orders.end():
                order_status = &(self.scratch_orders[ack.client_orderid])
                self._manage_option_orderstatus(order_status, &ack, md, 'SCRATCH')
                order_status = NULL
            elif self.scalp_orders.find(ack.client_orderid) != self.scalp_orders.end():
                order_status = &(self.scalp_orders[ack.client_orderid])
                if ((order_status.side == SIDE_ASK and self.ask_scalp_orders.find(order_status.price) != self.ask_scalp_orders.end()) or
                    (order_status.side == SIDE_BID and self.bid_scalp_orders.find(order_status.price) != self.bid_scalp_orders.end())):
                    self._manage_option_orderstatus(order_status, &ack, md, 'SCALP')
                    if ack.remaining_qty == 0:
                        print self, 'NO NEED TO SCRATCH BRUH: client_orderid=%d' % (ack.client_orderid, )
                        if order_status.side == SIDE_ASK:
                            self.queued_ask_price = 0
                            self.queued_ask_triggerid = 0
                        else:
                            self.queued_bid_price = 0
                            self.queued_bid_triggerid = 0
                order_status = NULL
            elif self.futures_orders.find(ack.client_orderid) != self.futures_orders.end():
                order_status = &(self.futures_orders[ack.client_orderid])
                qty_change = order_status.remaining - ack.remaining_qty
                if qty_change > 0:
                    if order_status.side == SIDE_BID:
                        self.futures_position.filled += qty_change
                        self.futures_position.working -= qty_change
                    else:
                        self.futures_position.filled -= qty_change
                        self.futures_position.working += qty_change
                    print self, (
                        'LIMIT (FUTURES; FILL): client_orderid=%d, price=%d, side=%d, '
                        'fill_qty=%d, remaining=%d, option_position=%d, delta=%0.3f, '
                        'option_exposure=%0.1f, '
                        'futures.working=%d, futures.filled=%d'
                    ) % (
                        ack.client_orderid, order_status.price, order_status.side, 
                        qty_change, ack.remaining_qty, self.option_position.filled, self.delta,
                        self.delta * self.option_position.filled, 
                        self.futures_position.working, self.futures_position.filled
                    )
                else:
                    print self, (
                        'LIMIT (ACCEPTED): client_orderid=%d, price=%d, side=%d, qty=%d'
                    ) % (ack.client_orderid, order_status.price, order_status.side, order_status.qty)
                order_status.remaining = ack.remaining_qty
                order_status = NULL
            else:
                print self, 'some other order... client_orderid=%d' % (ack.client_orderid, )
            self._remove_ack()
        
        # now evaluate md     
        if md.secid == self.option_secid:
            if md.bids[0].price != option_md.bids[0].price or md.asks[0].price != option_md.asks[0].price:
                self.option_mid = self.price_multiplier * 0.5 * (md.bids[0].price + md.asks[0].price)
                copy_mdorderbook(md, option_md)
                self._recalc_delta()
            if not self.already_sprinted:
                self._attempt_sprint()
                return
            if md.asks[0].price - md.bids[0].price == KOSPI_OPTION_MIN_TICK:
                # TODO: should we cancel any retardo orders?
                # check sprinters first
                if (self.bid_sprinter_orders.find(md.bids[0].price) != self.bid_sprinter_orders.end() and
                        md.bids[0].qty <= self.bid_cancel_threshold):
                    client_orderid = self.bid_sprinter_orders[md.bids[0].price]
                    self._cancel_order(client_orderid, &(self.sprinted_orders), 'SPRINTER')
                    self.bid_sprinter_orders.erase(md.bids[0].price)
                    if self.option_position.filled > 0:
                        print self, 'position=%d. need to scratch (triggerid=%d)' % (self.option_position.filled, client_orderid)
                        self.queued_ask_price = md.bids[0].price
                        self.queued_ask_triggerid = client_orderid
                        print self, 'but for now, scalp!'
                        scalp_price = self.queued_ask_price + KOSPI_OPTION_MIN_TICK
                        order_size = self.option_position.filled
                        client_orderid = self._place_new_order(
                            md, self.option_secid, scalp_price, SIDE_ASK, 
                            order_size, 'scalp client_orderid=%d' % ack.client_orderid, &(self.scalp_orders)
                        )
                        self.ask_scalp_orders[scalp_price] = client_orderid
                if (self.ask_sprinter_orders.find(md.asks[0].price) != self.ask_sprinter_orders.end() and
                        md.asks[0].qty <= self.ask_cancel_threshold):
                    client_orderid = self.ask_sprinter_orders[md.asks[0].price]
                    self._cancel_order(client_orderid, &(self.sprinted_orders), 'SPRINTER')
                    self.ask_sprinter_orders.erase(md.asks[0].price)
                    if self.option_position.filled < 0:
                        print self, 'position=%d. need to scratch (triggerid=%d)' % (self.option_position.filled, client_orderid)
                        self.queued_bid_price = md.asks[0].price
                        self.queued_bid_triggerid = client_orderid
                        print self, 'but for now, scalp!'
                        scalp_price = self.queued_bid_price - KOSPI_OPTION_MIN_TICK
                        order_size = -self.option_position.filled
                        client_orderid = self._place_new_order(
                            md, self.option_secid, scalp_price, SIDE_BID, 
                            order_size, 'scalp client_orderid=%d' % ack.client_orderid, &(self.scalp_orders)
                        )
                        self.bid_scalp_orders[scalp_price] = client_orderid
                # any crazy scalps?
                #if self.bid_scalp_orders.find(md.bids[0].price) != self.bid_scalp_orders.end():
                #    if md.bids[0].qty <= self.bid_order_size:
                #        client_orderid = self.bid_scalp_orders[md.bids[0].price]
                #        print self, 'consider canceling bid scalp? client_orderid=%d' % client_orderid
                #elif (self.ask_sprinter_orders.find(md.asks[0].price) == self.ask_sprinter_orders.end() and
                #        md.bids[0].qty > self.bid_order_size and 
                #        self.option_position.filled < 0):
                #    print self, 'consider placing bid scalp? self.option_position.filled=%d, md.bids[0].price=%d' % (self.option_position.filled, md.bids[0].price)
                #if self.ask_scalp_orders.find(md.asks[0].price) != self.ask_scalp_orders.end():
                #    if md.asks[0].qty <= self.ask_order_size:
                #        client_orderid = self.ask_scalp_orders[md.asks[0].price]
                #        print self, 'consider canceling ask scalp? client_orderid=%d' % client_orderid
                #elif (self.bid_sprinter_orders.find(md.bids[0].price) == self.bid_sprinter_orders.end() and
                #        md.asks[0].qty > self.ask_order_size and 
                #        self.option_position.filled > 0):
                #    print self, 'consider placing ask scalp? self.option_position.filled=%d, md.asks[0].price=%d' % (self.option_position.filled, md.asks[0].price)
                # we gots to scratch??
                if self.queued_bid_triggerid > 0:
                    if (self.queued_bid_price < md.asks[0].price or
                            (self.queued_bid_price == md.asks[0].price and md.asks[0].qty <= self.ask_order_size)):
                        print self, 'SCRATCH EVENT! lift it', md.asks[0].price, md.asks[0].qty
                        # manage possible scalp
                        scalp_price = self.queued_bid_price - KOSPI_OPTION_MIN_TICK
                        if self.bid_scalp_orders.find(scalp_price) == self.bid_scalp_orders.end():
                            err = '%s no bid scalp order? debug please' % self
                            raise ValueError(err)
                        self._cancel_order(self.bid_scalp_orders[scalp_price], &(self.scalp_orders), 'SCALP')
                        self.bid_scalp_orders.erase(scalp_price)
                        if self.last_bid_scratch_id > 0:
                            self._cancel_order(self.last_bid_scratch_id, &(self.scratch_orders), 'SCRATCH')
                            self.last_bid_scratch_id = 0
                        order_size = -self.option_position.filled
                        self.last_bid_scratch_id = self._place_new_order(
                            md, self.option_secid, self.queued_bid_price, SIDE_BID, 
                            order_size, 'scratch client_orderid=%d' % self.queued_bid_triggerid, &(self.scratch_orders)
                        )
                        self.queued_bid_price = 0
                        self.queued_bid_triggerid = 0
                if self.queued_ask_triggerid > 0:
                    if (self.queued_ask_price > md.bids[0].price or
                        (self.queued_ask_price == md.bids[0].price and md.bids[0].qty <= self.bid_order_size)):
                        print self, 'SCRATCH EVENT! hit it', md.bids[0].price, md.bids[0].qty
                        # manage possible scalp
                        scalp_price = self.queued_ask_price + KOSPI_OPTION_MIN_TICK
                        if self.ask_scalp_orders.find(scalp_price) == self.ask_scalp_orders.end():
                            err = '%s no bid scalp order? debug please' % self
                            raise ValueError(err)
                        self._cancel_order(self.ask_scalp_orders[scalp_price], &(self.scalp_orders), 'SCALP')
                        self.ask_scalp_orders.erase(scalp_price)
                        if self.last_ask_scratch_id > 0:
                            self._cancel_order(self.last_ask_scratch_id, &(self.scratch_orders), 'SCRATCH')
                            self.last_ask_scratch_id = 0
                        order_size = self.option_position.filled
                        self.last_ask_scratch_id = self._place_new_order(
                            md, self.option_secid, self.queued_ask_price, SIDE_ASK, 
                            order_size, 'scratch client_orderid=%d' % self.queued_ask_triggerid, &(self.scratch_orders)
                        )
                        self.queued_ask_price = 0
                        self.queued_ask_triggerid = 0
        elif md.secid == self.futures_secid:
            if md.bids[0].price != futures_md.bids[0].price or md.asks[0].price != futures_md.asks[0].price:
                self.futures_mid = self.price_multiplier * 0.5 * (md.bids[0].price + md.asks[0].price)
                copy_mdorderbook(md, futures_md)
                self._recalc_delta()


cdef class KospiOptionQPOSValuatorV2(KospiOptionQPOSValuatorV1):
    def __init__(self, uint64_t option_secid, uint64_t futures_secid, 
            int16_t option_type, double strike, double tte, double interest_rate, name=''):
        KospiOptionQPOSValuatorV1.__init__(self, option_secid, futures_secid, 
            option_type, strike, tte, interest_rate, name=name)

#cdef class KRXSprawlerFeatureExtractV1(StrategyInterface):
cdef class SprawlerV1(StrategyInterface):
    """KRXSprawlerV1

    """
    cdef:
        VolumeAccumulator buytrades, selltrades
        bint reset_accumulators, already_sprinted
        # keep track of some variables
        int32_t buyprice, sellprice
        int32_t last_bidprc, last_askprc, last_bidqty, last_askqty
        double buypct, sellpct
        vector_[int32_t] min_leans, tradeqtys
        map_[int32_t, int32_t] bid_order_qtys, ask_order_qtys
        map_[uint64_t, OrderInfo] client_ids
        uint64_t num_min_leans, num_tradeqtys, bid_min_lean_i, ask_min_lean_i, buy_tradeqty_i, sell_tradeqty_i
        # readable in interpreter
        readonly:
            uint64_t secid, volume_accumulator_time_window, num_bid_sprinters, num_ask_sprinters
            #int32_t min_lean_start, min_lean_end, min_lean_dec
            #int32_t tradeqty_start, tradeqty_end, tradeqty_inc
            double tradepct_threshold, bid_sprinter_size_pct, ask_sprinter_size_pct, sprinter_bypass_pct
            int32_t max_order_size
            int32_t qty, min_tick
            str name

    def __init__(self, uint64_t secid, name='',
            list min_lean_thresholds=None, list tradeqty_thresholds=None, 
            double tradepct_threshold=0, uint64_t volume_accumulator_time_window=0,
            double sprinter_bypass_pct=1.0, uint64_t num_bid_sprinters=0, num_ask_sprinters=0, 
            double bid_sprinter_size_pct=0.2, double ask_sprinter_size_pct=0.2,
            int32_t max_order_size=1, int32_t min_tick=100):
        cdef:
            int32_t min_lean_start = min_lean_thresholds[0]
            int32_t min_lean_end = min_lean_thresholds[1]
            int32_t min_lean_dec = min_lean_thresholds[2]
            int32_t tradeqty_start = tradeqty_thresholds[0]
            int32_t tradeqty_end = tradeqty_thresholds[1]
            int32_t tradeqty_inc = tradeqty_thresholds[2]
            OrderInfo order_info
        # initialize using StrategyInterface's initialize method, which requires list of
        # secids
        self.initialize([secid])
        self.secid = secid
        self.name = str(name)
        # SprawlerV1 relevant behavioral initializations
        # starting with min lean qtys
        if min_lean_end < 0:
            min_lean_end = 0
        if min_lean_start <= min_lean_end:
            min_lean_start = min_lean_end + 1
        if min_lean_dec >= 0:
            min_lean_dec = min_lean_end - min_lean_start
        qty = min_lean_start
        while qty >= min_lean_end:
            self.min_leans.push_back(qty)
            qty += min_lean_dec
        self.num_min_leans = self.min_leans.size()
        self.bid_min_lean_i = 0
        self.ask_min_lean_i = 0
        print 'min_lean thresholds: start=%d, end=%d, dec=%d' % (min_lean_start, min_lean_end, min_lean_dec)
        # and now trade qtys
        if tradeqty_start <= 0:
            tradeqty_start = 1
        if tradeqty_end <= tradeqty_start:
            tradeqty_end = tradeqty_start + 1
        if tradeqty_inc <= 0:
            tradeqty_inc = tradeqty_end - tradeqty_start
        qty = tradeqty_start
        while qty <= tradeqty_end:
            self.tradeqtys.push_back(qty)
            qty += tradeqty_inc
        self.num_tradeqtys = self.tradeqtys.size()
        self.buy_tradeqty_i = 0
        self.sell_tradeqty_i = 0
        print 'tradeqty thresholds: start=%d, end=%d, inc=%d' % (tradeqty_start, tradeqty_end, tradeqty_inc)
        self.tradepct_threshold = tradepct_threshold
        self.volume_accumulator_time_window = volume_accumulator_time_window
        # sprinter config
        self.already_sprinted = False
        self.sprinter_bypass_pct = sprinter_bypass_pct
        self.num_bid_sprinters = num_bid_sprinters
        self.num_ask_sprinters = num_ask_sprinters
        self.bid_sprinter_size_pct = bid_sprinter_size_pct
        self.ask_sprinter_size_pct = ask_sprinter_size_pct
        if max_order_size <= 0:
            self.max_order_size = 1
        else:
            self.max_order_size = max_order_size
        # initialize VolumeAccumulator objects
        self.buytrades.Init(volume_accumulator_time_window)
        self.selltrades.Init(volume_accumulator_time_window)
        # reset some values to start
        self._clear_buytrades()
        self._clear_selltrades()
        self.reset_accumulators = False
        self.min_tick = min_tick

        #initialize id map
        order_info.price=0
        order_info.side=0
        order_info.order_qty=0
        order_info.prev_qty=0
        order_info.rem_qty=0
        self.client_ids[0] = order_info

    def __repr__(self):
        return '%s{name=%s}' % (self.__class__.__name__, self.name)

    cdef void _clear_buytrades(self) nogil:
        self.buytrades.Clear()
        self.buyprice = 0
        self.buypct = 0

    cdef void _clear_selltrades(self) nogil:
        self.selltrades.Clear()
        self.sellprice = 0
        self.sellpct = 0

    cdef void _save_market(self, MDOrderBook *md) nogil:
        self.last_bidprc = md.bids[0].price
        self.last_askprc = md.asks[0].price
        self.last_bidqty = md.bids[0].qty
        self.last_askqty = md.asks[0].qty

    #cdef void _compute_accpcts(self) nogil:
    #    # buy side
    #    if self.buytrades.Qty() > 0:
    #        if self.buyprice == self.askprc:
    #            self.buypct = (1.0 * self.buyqty) / (self.askqty + self.buyqty)
    #            #self.buyparpct = (1.0 * self.buypar) / (self.askpar + self.buypar)
    #        else:
    #            self.buypct = 1.0
    #            #self.buyparpct = 1.0
    #    else:
    #        self.buypct = 0.0
    #        self.buyparpct = 0.0
    #    # sell side
    #    if self.selltrades.Qty() > 0:
    #        if self.sellprc == self.bidprc:
    #            self.sellpct = (1.0 * self.sellqty) / (self.bidqty + self.sellqty)
    #            #self.sellparpct = (1.0 * self.sellpar) / (self.bidpar + self.sellpar)
    #        else:
    #            self.sellpct = 1.0
    #            #self.sellparpct = 1.0
    #    else:
    #        self.sellpct = 0.0
    #        #self.sellparpct = 0.0

    #cdef void _check_trigger(self) nogil:
    #    self.buy_trigger = self.buyqty >= self.tradeqty_threshold and \
    #                       self.buypct >= self.tradepct_threshold
    #    self.sell_trigger = self.sellqty >= self.tradeqty_threshold and \
    #                        self.sellpct >= self.tradepct_threshold
    #    if self.buy_trigger:
    #        self.sellprc = 0
    #    if self.sell_trigger:
    #        self.buyprc = 0
    #    self.trig_seqnum = self.last_seqnum

    cdef void _attempt_sprint(self, MDOrderBook *md):
        cdef:
            uint64_t i, idx
            int32_t order_bidprc, order_askprc, order_qty, last_order_qty
            bint past_minimum_bid = False
            OrderInfo order_info
        # self.place_ghostsprint_order(
        #   uint64_t time, uint64_t seqnum, uint64_t secid,
        #   int32_t price, uint16_t side, int32_t qty, str context, double bypass_market_by
        # )
        print 'need to sprint at seqnum=%d' % md.seqnum
        # check that all book levels are filled... if the lowest bid is at a price of 1, it's okay
        for 0 <= i < KRX_MAX_LEVELS:
            #print 'checking bidprc=%d, askprc=%d' % (md.bids[i].price, md.asks[i].price)
            if md.bids[i].qty == 0 and not past_minimum_bid:
                return
            if md.asks[i].qty == 0:
                return
            if md.bids[i].price == KRX_MINIMUM_PRICE:
                past_minimum_bid = True
        # okay, every level has qty... let's place some orders
        order_bidprc = md.bids[0].price
        order_askprc = md.asks[0].price
        last_order_qty = 0
        for 0 <= i < self.num_bid_sprinters:
            if order_bidprc < KRX_MINIMUM_PRICE:
                break
            if i >= KRX_MAX_LEVELS:
                idx = KRX_MAX_LEVELS - 1
            else:
                idx = i
            order_qty = <int32_t>(self.bid_sprinter_size_pct * <double>(md.bids[idx].qty))
            # let's force our bid order qtys to be increasing as we go up in price
            if order_qty < last_order_qty:
                order_qty = last_order_qty
            if order_qty > self.max_order_size:
                order_qty = self.max_order_size
            client_orderid = self.place_sprint_order(
                md.time, md.seqnum, self.secid, 
                order_bidprc, SIDE_BID, order_qty, "sprinter order", self.sprinter_bypass_pct
            )
            self.bid_order_qtys[order_bidprc] = order_qty
            #print 'woo, order_bidprc=%d, order_bidqty=%d, client_orderid=%d' % (order_bidprc, order_qty, client_orderid)
            order_info.price=order_bidprc
            order_info.side=SIDE_BID
            order_info.order_qty=order_qty
            order_info.prev_qty=order_qty
            order_info.rem_qty=order_qty

            self.client_ids[client_orderid] = order_info

            order_bidprc -= self.min_tick
            last_order_qty = order_qty

        print self.client_ids

        ## why is this here?
        last_order_qty = self.max_order_size

        for 0 <= i < self.num_ask_sprinters:
            if i >= KRX_MAX_LEVELS:
                idx = KRX_MAX_LEVELS - 1
            else:
                idx = i
            order_qty = <int32_t>(self.ask_sprinter_size_pct * <double>(md.asks[idx].qty))
            # let's force our ask order qtys to be decreasing as we go up in price
            if order_qty > last_order_qty:
                order_qty = last_order_qty
            if order_qty > self.max_order_size:
                order_qty = self.max_order_size
            client_orderid=self.place_sprint_order(
                md.time, md.seqnum, self.secid, 
                order_askprc, SIDE_ASK, order_qty, "sprinter order", self.sprinter_bypass_pct
            )
            self.ask_order_qtys[order_askprc] = order_qty
            #print 'woo, order_askprc=%d, order_askqty=%d, client_orderid=%d' % (order_askprc, order_qty, client_orderid)
            order_info.price=order_askprc
            order_info.side=SIDE_ASK
            order_info.order_qty=order_qty
            order_info.rem_qty=order_qty
            order_info.prev_qty=order_qty

            self.client_ids[client_orderid] = order_info
            #print self.client_ids

            order_askprc += self.min_tick
            last_order_qty = order_qty

        print self.client_ids
        # let's never do this again
        self.already_sprinted = True

    cdef bint _bid_order_exists(self, int32_t price) nogil:
        return self.bid_order_qtys.find(price) != self.bid_order_qtys.end()

    cdef bint _ask_order_exists(self, int32_t price) nogil:
        return self.ask_order_qtys.find(price) != self.ask_order_qtys.end()

    cdef void _remove_bid_order(self, int32_t price) nogil:
        self.bid_order_qtys.erase(price)

    cdef void _remove_ask_order(self, int32_t price) nogil:
        self.ask_order_qtys.erase(price)

    cdef int32_t _bid_order_qty_at(self, int32_t price) nogil:
        if self._bid_order_exists(price):
            return self.bid_order_qtys[price]
        else:
            return 0

    cdef int32_t _ask_order_qty_at(self, int32_t price) nogil:
        if self._ask_order_exists(price):
            return self.ask_order_qtys[price]
        else:
            return 0

    def evaluate_md(self):
        cdef:
            # pointers for ease of use
            MDOrderBook *md = self.md_ptr
            VolumeAccumulator *buytrades = &(self.buytrades)
            # market data stuff
            int32_t bidprc, bidqty, askprc, askqty
            int32_t buyprice, buyvolume, sellprice, sellvolume
            int32_t test_min_lean, test_tradeqty, order_qty
            int32_t fill_price=0, fill_qty=0, fill_side=0
            int32_t hedge_price=0, hedge_qty=0, hedge_side=0
            uint64_t time, client_orderid
            SimulatedOrderAck ack
            OrderInfo order_info

        if not self.valid_md:
            return

        # let's evaluate some acks!
        while self._has_acks():
            ack = self._top_ack()
            #if self.sprinted_orders.find(ack.client_orderid) != self.sprinted_orders.end():
            if self.client_ids.find(ack.client_orderid) != self.client_ids.end():
                #print 'cool! i acked client_orderid=%d, rem_qty=%d' % (ack.client_orderid, ack.remaining_qty)
                ## check if client_orderid is part of sprinted order array...
                self.client_ids[ack.client_orderid].prev_qty = self.client_ids[ack.client_orderid].rem_qty
                self.client_ids[ack.client_orderid].rem_qty = ack.remaining_qty
                #print self.client_ids[ack.client_orderid]
               
                fill_price = self.client_ids[ack.client_orderid].price
                fill_side = self.client_ids[ack.client_orderid].side
                fill_qty = self.client_ids[ack.client_orderid].prev_qty - self.client_ids[ack.client_orderid].rem_qty
                
                if fill_qty > 0:
                    if fill_side == 1:
                        hedge_price = fill_price + self.min_tick
                        hedge_side = 2
                        hedge_qty = fill_qty
                    elif fill_side == 2:
                        hedge_price = fill_price - self.min_tick
                        hedge_side = 1
                        hedge_qty = fill_qty
                    else:
                        print 'no fill side'
         
                    # place hedge orders
                    context = 'hedge order for id: %d' % (ack.client_orderid)
                    client_orderid = self.place_limit_order(md.time, md.seqnum, self.secid, hedge_price, hedge_side, hedge_qty, context)
                    #print 'throwing hedges', client_orderid,  md.seqnum, self.secid, hedge_price, hedge_side, hedge_qty, context
            else:
                print "ack id=%d does not exist in client_ids"%ack.client_orderid             
            
            self._remove_ack()

        time = md.time
        bidprc = md.bids[0].price
        askprc = md.asks[0].price
        bidqty = md.bids[0].qty
        askqty = md.asks[0].qty

        if bidprc != self.last_bidprc:
            self._clear_buytrades()

        if askprc != self.last_askprc:
            self._clear_selltrades()

        # KRX can give no price levels in their book messages, so let's
        # ignore them for now
        if bidqty == 0 or askqty == 0:
            #print 'SKIPPING seqnum=%d, %d/%d (%dx%d)' % (md.seqnum, bidprc, askprc, bidqty, askqty)
            return

        # we need to sprint our orders
        if not self.already_sprinted:
            self._attempt_sprint(md)
            return

        # did a level go away? remove the order from our map
        if bidprc < self.last_bidprc and self._bid_order_exists(self.last_bidprc):
            print 'removing bid order, seqnum=%d, price=%d' % (md.seqnum, self.last_bidprc)
            self._remove_bid_order(self.last_bidprc)
            self.bid_min_lean_i = 0
            self.sell_tradeqty_i = 0
        if askprc > self.last_askprc and self._ask_order_exists(self.last_askprc):
            print 'removing ask order, seqnum=%d, price=%d' % (md.seqnum, self.last_askprc)
            self._remove_ask_order(self.last_askprc)
            self.ask_min_lean_i = 0
            self.buy_tradeqty_i = 0

        # check bid order at the top level
        if self._bid_order_exists(bidprc):
            self._check_scratch_bid(md, bidprc)

        if self._ask_order_exists(askprc):
            self._check_scratch_ask(md, askprc)

        # finally save the last state of the market
        self._save_market(md)

    cdef int32_t _get_bid_min_lean(self, uint64_t i) nogil:
        if i >= self.num_min_leans:
            return self.min_leans[self.num_min_leans-1]
        else:
            return self.min_leans[i]

    cdef int32_t _get_ask_min_lean(self, uint64_t i) nogil:
        if i >= self.num_min_leans:
            return self.min_leans[self.num_min_leans-1]
        else:
            return self.min_leans[i]

    cdef int32_t _get_buy_tradeqty(self, uint64_t i) nogil:
        if i >= self.num_tradeqtys:
            return self.tradeqtys[self.num_tradeqtys-1]
        else:
            return self.tradeqtys[i]

    cdef int32_t _get_sell_tradeqty(self, uint64_t i) nogil:
        if i >= self.num_tradeqtys:
            return self.tradeqtys[self.num_tradeqtys-1]
        else:
            return self.tradeqtys[i]

    cdef void _check_scratch_bid(self, MDOrderBook *md, int32_t order_price):
        # self.place_ghostlimit_order(
        #   uint64_t time, uint64_t seqnum, uint64_t secid,
        #   int32_t price, uint16_t side, int32_t qty, str context
        # )
        cdef:
            int32_t order_qty = self._bid_order_qty_at(order_price)
            int32_t bidqty = md.bids[0].qty
            VolumeAccumulator *selltrades = &(self.selltrades)
            str context
        # check min lean
        while self.bid_min_lean_i < self.num_min_leans and bidqty < order_qty + self.min_leans[self.bid_min_lean_i]:
            context = (
                'cancel attempt for order side=1, price=%d, qty=%d: min_lean violation, '
                'seqnum=%d, mkt_qty=%d, min_lean=%d, volume=%d, tradeqty_threshold=%d, '
                'pct=%f, tradepct_threshold=%f'
            ) % (order_price, order_qty, md.seqnum, bidqty, self._get_bid_min_lean(self.bid_min_lean_i), 
                 selltrades.Qty(), self._get_sell_tradeqty(self.sell_tradeqty_i), selltrades.Qty() / (1.0 * (bidqty + selltrades.Qty())), self.tradepct_threshold)
            client_orderid = self.place_limit_order(md.time, md.seqnum, self.secid, order_price, SIDE_ASK, order_qty, context)
            #print 'awesome', client_orderid, order_price, order_qty, md.seqnum, bidqty, self._get_bid_min_lean(self.bid_min_lean_i)

            preincrement(self.bid_min_lean_i)
        # check volume events
        if md.sellvolume > 0:
            selltrades.AddTrade(md.time, md.sellvolume)
            while self.sell_tradeqty_i < self.num_tradeqtys and selltrades.Qty() > self.tradeqtys[self.sell_tradeqty_i] and selltrades.Qty() / (1.0 * (bidqty + selltrades.Qty())) > self.tradepct_threshold:
                context = (
                    'cancel attempt for order side=1, price=%d, qty=%d: tradeqty violation, '
                    'seqnum=%d, mkt_qty=%d, min_lean=%d, volume=%d, tradeqty_threshold=%d, '
                    'pct=%f, tradepct_threshold=%f'
                ) % (order_price, order_qty, md.seqnum, bidqty, self._get_bid_min_lean(self.bid_min_lean_i), 
                     selltrades.Qty(), self._get_sell_tradeqty(self.sell_tradeqty_i), selltrades.Qty() / (1.0 * (bidqty + selltrades.Qty())), self.tradepct_threshold)
                client_orderid = self.place_limit_order(md.time, md.seqnum, self.secid, order_price, SIDE_ASK, order_qty, context)
                #print 'awesome', client_orderid, order_price, order_qty, md.seqnum, bidqty, self._get_bid_min_lean(self.bid_min_lean_i)
                preincrement(self.sell_tradeqty_i)

    cdef void _check_scratch_ask(self, MDOrderBook *md, int32_t order_price):
        # self.place_ghostfak_order(
        #   uint64_t time, uint64_t seqnum, uint64_t secid,
        #   int32_t price, uint16_t side, int32_t qty, str context
        # )
        cdef:
            ### "ask order qty at" change to "ask order position at"
            int32_t order_qty = self._ask_order_qty_at(order_price)
            int32_t askqty = md.asks[0].qty
            VolumeAccumulator *buytrades = &(self.buytrades)
            str context
        # check min lean
        while self.ask_min_lean_i < self.num_min_leans and askqty < order_qty + self.min_leans[self.ask_min_lean_i]:
            context = (
                'cancel attempt for order side=2, price=%d, qty=%d: min_lean violation, '
                'seqnum=%d, mkt_qty=%d, min_lean=%d, volume=%d, tradeqty_threshold=%d, '
                'pct=%f, tradepct_threshold=%f'
            ) % (order_price, order_qty, md.seqnum, askqty, self._get_ask_min_lean(self.ask_min_lean_i), 
                 buytrades.Qty(), self._get_buy_tradeqty(self.buy_tradeqty_i), buytrades.Qty() / (1.0 * (askqty + buytrades.Qty())), self.tradepct_threshold)
            client_orderid = self.place_limit_order(md.time, md.seqnum, self.secid, order_price, SIDE_BID, order_qty, context)
            #print 'awesome', client_orderid, order_price, order_qty, md.seqnum, askqty, self._get_ask_min_lean(self.ask_min_lean_i)
            preincrement(self.ask_min_lean_i)
        # check volume events
        if md.buyvolume > 0:
            buytrades.AddTrade(md.time, md.buyvolume)
            while self.buy_tradeqty_i < self.num_tradeqtys and buytrades.Qty() > self.tradeqtys[self.buy_tradeqty_i] and buytrades.Qty() / (1.0 * (askqty + buytrades.Qty())) > self.tradepct_threshold:
                context = (
                    'cancel attempt for order side=2, price=%d, qty=%d: tradeqty violation, '
                    'seqnum=%d, mkt_qty=%d, min_lean=%d, volume=%d, tradeqty_threshold=%d, '
                    'pct=%f, tradepct_threshold=%f'
                ) % (order_price, order_qty, md.seqnum, askqty, self._get_ask_min_lean(self.ask_min_lean_i), 
                     buytrades.Qty(), self._get_buy_tradeqty(self.buy_tradeqty_i), buytrades.Qty() / (1.0 * (askqty + buytrades.Qty())), self.tradepct_threshold)
                client_orderid = self.place_limit_order(md.time, md.seqnum, self.secid, order_price, SIDE_BID, order_qty, context)
                #print 'awesome', client_orderid, order_price, order_qty, md.seqnum, askqty, self._get_ask_min_lean(self.ask_min_lean_i)
                preincrement(self.buy_tradeqty_i)
        

    @staticmethod
    def parse_context(df, inplace=True):
        pass

#        search_str = (
#            r'mkt_seqnum=([0-9]+), acc_(buy|sell)s={packets=([0-9]+), '
#            r'qty=([0-9]+), par=([0-9]+), \%qty=([\.0-9]+), \%par=([\.0-9]+)}, '
#            r'qty_imbalance={([\.\-0-9]+), ([\.\-0-9]+)}, '
#            r'par_imbalance={([\.\-0-9]+), ([\.\-0-9]+)}.*'
#        )
#        context = df['context'].str.extract(search_str, expand=True)
#
#        convert_cols = [
#            ('mkt_seqnum', int),
#            ('accside', None),
#            ('accpkt', int),
#            ('accqty', int),
#            ('accpar', int),
#            ('accpct', float),
#            ('accparpct', float),
#            ('qty_imb_prev', float),
#            ('qty_imb_now', float),
#            ('par_imb_prev', float),
#            ('par_imb_now', float)
#        ]
#        context.columns = map(lambda x: x[0], convert_cols)
#
#        if inplace:
#            for col, dtype in convert_cols:
#                if dtype is None:
#                    df[col] = context[col]
#                else:
#                    df[col] = context[col].astype(dtype)
#        else:
#            for col, dtype in convert_cols:
#                if dtype is not None:
#                    context[col] = context[col].astype(dtype)
#        return context

