# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from cython.operator cimport dereference, preincrement
from libc.stdint cimport *
from libcpp.unordered_map cimport unordered_map as unordered_map_
from glt.md.standard cimport BookLevel, MDOrderBook, VolumeAccumulator
from glt.backtesting.base cimport StrategyInterface, SimulatedOrderAck
from numpy import polyfit

cdef uint16_t SIDE_BID = 1
cdef uint16_t SIDE_ASK = 2

cdef class KospiFuturesSniper(StrategyInterface):
    cdef:
        VolumeAccumulator buytrades, selltrades
        bint reset_accumulators
        int32_t buyprice, sellprice
        double buypct, sellpct, prev_imbal_buy, prev_imbal_sell
        double prev_par_imbal_buy, prev_par_imbal_sell
        double prev_imbal_buy1, prev_imbal_sell1
        double prev_par_imbal_buy1, prev_par_imbal_sell1 
        int32_t last_bidprc, last_askprc, last_bidqty, last_askqty
        int32_t last_bidpar, last_askpar
        int32_t last_bidprc1, last_askprc1, last_bidqty1, last_askqty1
        int32_t last_bidpar1, last_askpar1 
        int32_t lastfire_price, lastfire_side
        int32_t multiple_fires, sfprice_id
        uint64_t prev_sftime 
        list par_bid_array, par_ask_array, time_bid_array, time_ask_array
        
        readonly:
            str name
            uint64_t futures_secid, volume_accumulator_time_window
            int32_t ordersize
            int32_t min_tradevolume
            uint16_t max_par_array_size
            dict existing_orders
            #float imbal_threshold
            
    def __init__(self, uint64_t futures_secid, name, ordersize, min_trade, uint64_t vol_acc_time_window, uint16_t max_par_array_size):
        self.futures_secid = futures_secid
        self.name = str(name)
        self.ordersize = ordersize
        self.min_tradevolume = min_trade
        #self.imbal_threshold=imbal_threshold
        self.max_par_array_size = max_par_array_size
        self.initialize([futures_secid])
        self.existing_orders = {}
        self.last_bidprc=0; self.last_askprc=0; self.last_bidqty=0; self.last_askqty=0
        self.last_bidpar=0; self.last_askpar=0; self.last_bidpar1=0; self.last_askpar1=0
        self.last_bidprc1=0; self.last_askprc1=0; self.last_bidqty1=0; self.last_askqty1=0
        self.lastfire_price=0
        self.lastfire_side=0
        self.prev_sftime=0
        self.prev_imbal_buy=0
        self.prev_imbal_sell=0
        self.prev_par_imbal_buy=0
        self.prev_par_imbal_sell=0
        self.prev_imbal_buy1=0
        self.prev_imbal_sell1=0
        self.prev_par_imbal_buy1=0
        self.prev_par_imbal_sell1=0
        self.multiple_fires=0
        self.sfprice_id=0
        self.par_bid_array = []
        self.par_ask_array = []
        self.time_bid_array = []
        self.time_ask_array = []

        self.volume_accumulator_time_window = vol_acc_time_window
        # initialize VolumeAccumulator objects
        self.buytrades.Init(vol_acc_time_window)
        self.selltrades.Init(vol_acc_time_window)
        # reset some values to start
        self._clear_buytrades()
        self._clear_selltrades()
        self.reset_accumulators = False
        #self.min_tick = min_tick

    def __repr__(self):
        return (
            '%s{%s, fut=%d, ordersize=%d, min_trade=%d, name=%s}'
        ) % (
            self.__class__.__name__, hex(id(self)), 
            self.futures_secid, self.ordersize, self.min_tradevolume, self.name
        )

    cdef void _clear_buytrades(self) nogil:
        self.buytrades.Clear()
        self.buyprice = 0
        self.buypct = 0

    cdef void _clear_selltrades(self) nogil:
        self.selltrades.Clear()
        self.sellprice = 0
        self.sellpct = 0

    cdef void _clear_par_bid_array(self):
        self.par_bid_array = []
        self.time_bid_array = []

    cdef void _clear_par_ask_array(self):
        self.par_ask_array = []
        self.time_ask_array = []

    cdef void _update_par_array(self, bidpar, askpar, time):
        
        self.par_bid_array.append(bidpar)
        self.par_ask_array.append(askpar)
        self.time_bid_array.append(time)
        self.time_ask_array.append(time)


        if len(self.par_bid_array) > self.max_par_array_size:
            self.par_bid_array = self.par_bid_array[1:]
            self.time_bid_array = self.time_bid_array[1:]

        if len(self.par_ask_array) > self.max_par_array_size:
            self.par_ask_array = self.par_ask_array[1:]
            self.time_ask_array = self.time_ask_array[1:]
        

    cdef void _save_market(self, MDOrderBook *md):
        self.last_bidprc = md.bids[0].price
        self.last_askprc = md.asks[0].price
        self.last_bidqty = md.bids[0].qty
        self.last_askqty = md.asks[0].qty
        self.last_bidpar = md.bids[0].participants
        self.last_askpar = md.asks[0].participants
        self.last_bidprc1 = md.bids[1].price
        self.last_askprc1 = md.asks[1].price
        self.last_bidqty1 = md.bids[1].qty
        self.last_askqty1 = md.asks[1].qty
        self.last_bidpar1 = md.bids[1].participants
        self.last_askpar1 = md.asks[1].participants

    cdef void _save_lastfire(self, int32_t side, int32_t tradeprice, uint64_t time):
        self.lastfire_side = side
        self.lastfire_price = tradeprice
        self.prev_sftime = time
 
    def evaluate_md(self):
        cdef:
            MDOrderBook *md = self.md_ptr
            VolumeAccumulator *buytrades = &(self.buytrades)
            VolumeAccumulator *selltrades = &(self.selltrades)
            SimulatedOrderAck ack
            uint64_t client_orderid
            # market data stuff
            uint64_t time, dt_sfprice=0, place_time, seqnum, secid
            int32_t bidprc, bidqty, askprc, askqty, bidpar, askpar
            int32_t bidprc1, bidqty1, askprc1, askqty1, bidpar1, askpar1
            int32_t buyprice, buyvolume, sellprice, sellvolume, accbuys, accsells      
            float imbal = 0.0, d_imbal_buy = 0.0, d_imbal_sell = 0.0
            float par_imbal = 0.0, d_par_imbal_buy = 0.0, d_par_imbal_sell = 0.0
            float d_par_buy = 0.0, d_par_sell = 0.0
            #second level
            float imbal1 = 0.0, d_imbal_buy1 = 0.0, d_imbal_sell1 = 0.0
            float par_imbal1 = 0.0, d_par_imbal_buy1 = 0.0, d_par_imbal_sell1 = 0.0
            float buypct=0.0, sellpct=0.0, accbuypct=0.0, accsellpct=0.0
            int16_t ticksize = 500
            bint samefireprice=False
            bint book_tight=True

        if not self.valid_md:
            return

        if md.bids[0].qty == 0 or md.asks[0].qty == 0:
            return
        
        time = md.time
        seqnum = md.seqnum
        secid = md.secid

        # let's evaluate some acks!
        while self._has_acks():
            ack = self._top_ack()
            self._remove_ack()
        
        bidprc = md.bids[0].price
        bidqty = md.bids[0].qty
        askprc = md.asks[0].price
        askqty = md.asks[0].qty
        bidpar = md.bids[0].participants
        askpar = md.asks[0].participants

        bidprc1 = md.bids[1].price
        bidqty1 = md.bids[1].qty
        askprc1 = md.asks[1].price
        askqty1 = md.asks[1].qty
        bidpar1 = md.bids[1].participants
        askpar1 = md.asks[1].participants

        if bidprc != self.last_bidprc:
            self._clear_buytrades()
            self._clear_par_bid_array() #reset slope of bid side participants

        if askprc != self.last_askprc:
            self._clear_selltrades()
            self._clear_par_ask_array() #reset slope of offer side participants

        buyprice = md.buyprice
        buyvolume = md.buyvolume
        sellprice = md.sellprice
        sellvolume = md.sellvolume

        if buyvolume:
            buytrades.AddTrade(time, buyvolume)

        if sellvolume:
            selltrades.AddTrade(time, sellvolume)
   
        buypct = 1.0 if buyprice != askprc else (1.0 * buyvolume / (buyvolume + askqty))
        sellpct = 1.0 if sellprice != bidprc else (1.0 * sellvolume / (sellvolume + bidqty))
        
        imbal = 1.0 * (bidqty - askqty) / (bidqty + askqty)
        d_imbal_buy = imbal - self.prev_imbal_buy ## curr imbal - prev imbal
        d_imbal_sell = (-1.0 * imbal) - self.prev_imbal_sell

        par_imbal = 1.0 * (bidpar - askpar) / (bidpar + askpar)
        d_par_imbal_buy = par_imbal - self.prev_par_imbal_buy ## curr imbal - prev imbal
        d_par_imbal_sell = (-1.0 * par_imbal) - self.prev_par_imbal_sell

        ## second level info
        imbal1 = 1.0 * (bidqty1 - askqty1) / (bidqty1 + askqty1)
        d_imbal_buy1 = imbal1 - self.prev_imbal_buy1 ## curr imbal - prev imbal
        d_imbal_sell1 = (-1.0 * imbal1) - self.prev_imbal_sell1

        par_imbal1 = 1.0 * (bidpar1 - askpar1) / (bidpar1 + askpar1)
        d_par_imbal_buy1 = par_imbal1 - self.prev_par_imbal_buy1 ## curr imbal - prev imbal
        d_par_imbal_sell1 = (-1.0 * par_imbal1) - self.prev_par_imbal_sell1
    
        self._update_par_array(bidpar, askpar, time)

        #d_par_buy = 1.0 * self.par_bid_array.sum() / self.max_par_array_size
        #d_par_sell = 1.0 * self.par_ask_array.sum() / self.max_par_array_size
        #d_par_buy = 0 if len(self.time_bid_array) <= 1 else polyfit(self.time_bid_array, self.par_bid_array, 1)[0] 
        #d_par_sell = 0 if len(self.time_ask_array) <= 1 else polyfit(self.time_ask_array, self.par_ask_array, 1)[0] 
        d_par_buy = 0 if len(self.time_bid_array) <= 1 else polyfit(range(len(self.time_bid_array)), self.par_bid_array, 1)[0] 
        d_par_sell = 0 if len(self.time_ask_array) <= 1 else polyfit(range(len(self.time_ask_array)), self.par_ask_array, 1)[0] 
        
        ## what if the whole level gets taken out in your favor? d_par_opp = arb large neg #
        d_par_buy = -100 if bidprc < self.last_bidprc else d_par_buy #sell trigger
        d_par_sell = -100 if askprc > self.last_askprc else d_par_sell #buy trigger

        accbuys = buytrades.Qty()
        accbuypct = buytrades.Qty() / (1.0 * (askqty + buytrades.Qty()))     
        accsells = selltrades.Qty()
        accsellpct = selltrades.Qty() / (1.0 * (bidqty + selltrades.Qty()))     

        gapped_book = (askprc - bidprc) > ticksize

        #if (buyvolume > self.min_tradevolume and imbal > self.imbal_threshold) or (sellvolume > self.min_tradevolume and imbal < -1.0 * self.imbal_threshold):
        if buyvolume > self.min_tradevolume:

            samefireprice = (self.lastfire_price == buyprice) & (self.lastfire_side == SIDE_BID)
            # oh boy! a trigger... record all the things...
        
            if samefireprice:
                self.multiple_fires += 1
                #print 'already fired side: %d, price: %d'%(self.lastfire_side, self.lastfire_price)
                dt_sfprice = (time - self.prev_sftime)# time since last fire price
            else:
                self.multiple_fires = 0 #reset
                dt_sfprice = 0
                self.sfprice_id += 1

            context_str = (
                 'time: %d, seqnum: %d, price: %d, side: %d, leanqty0: %d, oppqty0: %d, leanpar0: %d, opppar0: %d, '
                 'tradeqty: %d, tradepct: %f, acctradeqty: %d, acctradepct: %f, accoppqty: %d, accopppct: %f, '
                 'imbal: %f, prev_imbal: %f, d_imbal: %f, par_imbal: %f, d_par_imbal: %f, gapped_book: %d, '
                 'num_samefire: %d, dt_sfprice: %d, sfprice_id: %d, leanqty1: %d, oppqty1: %d, leanpar1: %d, opppar1: %d, '
                 'imbal1: %f, prev_imbal1: %f, d_imbal1: %f, par_imbal1: %f, d_par_imbal1: %f, d_par_lean: %f, d_par_opp: %f'
               % (time, seqnum, buyprice, SIDE_BID, bidqty, askqty, bidpar, askpar,
                    buyvolume, buypct, accbuys, accbuypct, accsells, accsellpct, 
                    imbal, self.prev_imbal_buy, d_imbal_buy, par_imbal, d_par_imbal_buy, 
                    gapped_book, self.multiple_fires, dt_sfprice, self.sfprice_id,
                    bidqty1, askqty1, bidpar1, askpar1, imbal1, self.prev_imbal_buy1, 
                    d_imbal_buy1, par_imbal1, d_par_imbal_buy1, d_par_buy, d_par_sell)
            )
            print ('placing fak order: price=%d, side=%d, placed at=%d, trigseqnum=%d, d_par_buy=%f, d_par_buy_array=%s, d_par_sell=%f, '
                   'd_par_sell_array=%s, qty_traded=%d, imbal=%f'
               % (buyprice, SIDE_BID, time, seqnum, d_par_buy, self.par_bid_array, d_par_sell, self.par_ask_array, buyvolume, imbal)
            )
            client_orderid = self.place_fak_order(time, seqnum, secid, buyprice, SIDE_BID, self.ordersize, context_str)

            # reset accumulator after fire             
            self._clear_buytrades()
            self._save_lastfire(SIDE_BID, buyprice, time)
        
        elif sellvolume > self.min_tradevolume:

            samefireprice = (self.lastfire_price == sellprice) & (self.lastfire_side == SIDE_ASK)
            # oh boy! a trigger... record all the things...
    
            if samefireprice:
                self.multiple_fires += 1
                dt_sfprice = (time - self.prev_sftime)# time since last fire price
                print 'already fired side: %d, price: %d'%(self.lastfire_side, self.lastfire_price)
                #self._clear_trades()
                #self.reset_accumulators=False
            else:
                self.multiple_fires = 0 #reset
                df_sfprice = 0
                self.sfprice_id += 1

            context_str = (
                 'time: %d, seqnum: %d, price: %d, side: %d, leanqty0: %d, oppqty0: %d, leanpar0: %d, opppar0: %d, '
                 'tradeqty: %d, tradepct: %f, acctradeqty: %d, acctradepct: %f, accoppqty: %d, accopppct: %f, '
                 'imbal: %f, prev_imbal: %f, d_imbal: %f, par_imbal: %f, d_par_imbal: %f, gapped_book: %d, '
                 'num_samefire: %d, dt_sfprice: %d, sfprice_id: %d, leanqty1: %d, oppqty1: %d, leanpar1: %d, opppar1: %d, '
                 'imbal1: %f, prev_imbal1: %f, d_imbal1: %f, par_imbal1: %f, d_par_imbal1: %f, d_par_lean: %f, d_par_opp: %f'
               % (time, seqnum, sellprice, SIDE_ASK, askqty, bidqty, askpar, bidpar,
                    sellvolume, sellpct, accsells, accsellpct, accbuys, accbuypct, 
                    -1.0 * imbal, self.prev_imbal_sell, d_imbal_sell, -1.0 * par_imbal, d_par_imbal_sell, 
                    gapped_book, self.multiple_fires, dt_sfprice, self.sfprice_id,
                    askqty1, bidqty1, askpar1, bidpar1, -1.0 * imbal1, self.prev_imbal_sell1, d_imbal_sell1,
                    -1.0 * par_imbal1, d_par_imbal_sell1, d_par_sell, d_par_buy)
            )
            print ('placing fak order: price=%d, side=%d, placed at=%d, trigseqnum=%d, d_par_buy=%f, d_par_buy_array=%s, d_par_sell=%f, '
                   'd_par_sell_array=%s, qty_traded=%d, imbal=%f'
                % (sellprice, SIDE_ASK, time, seqnum, d_par_buy, self.par_bid_array, d_par_sell, self.par_ask_array, sellvolume, -1.0 * imbal)
            )
            client_orderid = self.place_fak_order(time, seqnum, secid, sellprice, SIDE_ASK, self.ordersize, context_str)

            # reset accumulator after fire
            self._clear_selltrades()
            self._save_lastfire(SIDE_ASK, sellprice, time)

        #else (buyvolume > self.min_tradevolume and imbal < self.imbal_threshold) or (sellvolume > self.min_tradevolume and imbal > -1 * self.imbal_threshold):
        #    print 'imbal not met: seqnum=%d,  %f, buyvolume=%d, sellvolume=%d, tradeqty=%d' %(seqnum, imbal, buyvolume, sellvolume, tradeqty)
        
        self.prev_imbal_buy = imbal
        self.prev_imbal_sell = -1.0 * imbal
        self.prev_par_imbal_buy = par_imbal
        self.prev_par_imbal_sell = -1.0 * par_imbal

        self.prev_imbal_buy1 = imbal1
        self.prev_imbal_sell1 = -1.0 * imbal1
        self.prev_par_imbal_buy1 = par_imbal1
        self.prev_par_imbal_sell1 = -1.0 * par_imbal1

        self._save_market(md)

        return
