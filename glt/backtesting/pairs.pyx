# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from libc.stdint cimport int32_t, int64_t, uint16_t, uint32_t, uint64_t
from cython.operator cimport dereference, preincrement
from libcpp.vector cimport vector as vector_
from libcpp.unordered_map cimport unordered_map as map_
from glt.md.standard cimport BookLevel, MDOrderBook, VolumeAccumulator 
from glt.backtesting.base cimport StrategyInterface

cdef uint16_t SIDE_BID = 1
cdef uint16_t SIDE_ASK = 2

cdef uint64_t KRX_MAX_LEVELS = 5

cdef int32_t KRX_MINIMUM_PRICE = 1
cdef int32_t OFF_MARKET_TICKS = 2

cdef uint16_t DIFF_NOTHING = 0
cdef uint16_t DIFF_TRADE_PART = 1
cdef uint16_t DIFF_TRADE_FULL = 2
cdef uint16_t DIFF_TRADE_MULT = 3
cdef uint16_t DIFF_MOD_DOWN = 4
cdef uint16_t DIFF_MOD_UP = 5
cdef uint16_t DIFF_ADD = 6
cdef uint16_t DIFF_DELETE = 7


cdef struct StateDiff:
    int32_t qty, par
    uint16_t condition


cdef void update_state_diff(uint16_t side, int32_t tradeqty, BookLevel *curr, BookLevel *last, StateDiff *diff) nogil:
    if curr.price == last.price:
        diff.qty = curr.qty - last.qty
        diff.par = curr.participants - last.participants
    elif curr.price > last.price:
        if side == SIDE_BID:
            diff.qty = curr.qty
            diff.par = curr.participants
        else:
            diff.qty = -last.qty
            diff.par = -last.participants
    else:
        if side == SIDE_BID:
            diff.qty = -last.qty
            diff.par = -last.participants
        else:
            diff.qty = curr.qty
            diff.par = curr.participants
    # set condition
    if diff.par == 0:
        if diff.qty == 0:
            diff.condition = DIFF_NOTHING
        elif diff.qty > 0:
            diff.condition = DIFF_MOD_UP
        elif tradeqty > 0:
            diff.condition = DIFF_TRADE_PART
        else:
            diff.condition = DIFF_MOD_DOWN
    elif diff.par > 0:
        diff.condition = DIFF_ADD
    elif tradeqty > 0:
        if diff.par < -1:
            diff.condition = DIFF_TRADE_MULT
        else:
            diff.condition = DIFF_TRADE_FULL
    else:
        diff.condition = DIFF_DELETE


cdef bint is_trade(StateDiff *diff) nogil:
    return diff.condition == DIFF_TRADE_MULT \
        or diff.condition == DIFF_TRADE_FULL \
        or diff.condition == DIFF_TRADE_PART


cdef struct StateChange:
    int32_t part, full, mult, delete_qty, mod_down, total
    double part_pct, full_pct, mult_pct, delete_pct, mod_down_pct
    double imbalance


cdef void compute_state_change_pcts(StateChange *chg) nogil:
    chg.total = chg.part + chg.full + chg.mult + chg.delete_qty + chg.mod_down
    if chg.total == 0:
        chg.part_pct = 0
        chg.full_pct = 0
        chg.mult_pct = 0
        chg.delete_pct = 0
        chg.mod_down_pct = 0
    else:
        chg.part_pct = <double>chg.part / chg.total
        chg.full_pct = <double>chg.full / chg.total
        chg.mult_pct = <double>chg.mult / chg.total
        chg.delete_pct = <double>chg.delete_qty / chg.total
        chg.mod_down_pct = <double>chg.mod_down / chg.total
        

cdef void compute_state_change_imbalance(ContractMarketState state, StateChange *chg):
    cdef:
        int32_t qty, opp_qty
        double curr_imbalance
    if state.wide and chg.total != 0:
        chg.imbalance = -1
    elif chg.total == 0:
        chg.imbalance = 0
    else:
        chg.imbalance = chg.total / <double>(chg.total + state.bid.qty + state.ask.qty)
    

cdef void update_state_change(ContractMarketState state, TradeAccumulators acc_trades, 
                              VacuumAccumulators acc_vacuum, 
                              uint64_t side, StateChange *chg):
    cdef uint16_t opp_side
    if side == SIDE_BID:
        opp_side = SIDE_ASK
    else:
        opp_side = SIDE_BID
    chg.part = acc_trades._part_qty(opp_side)
    chg.full = acc_trades._full_qty(opp_side)
    chg.mult = acc_trades._mult_qty(opp_side)
    chg.delete_qty = acc_vacuum._delete_qty(side)
    chg.mod_down = acc_vacuum._mod_down_qty(side)
    compute_state_change_pcts(chg)
    compute_state_change_imbalance(state, chg)


cdef void clear_state_change(StateChange *chg) nogil:
    chg.part = 0
    chg.full = 0
    chg.mult = 0
    chg.delete_qty = 0
    chg.mod_down = 0
    chg.part_pct = 0
    chg.full_pct = 0
    chg.mult_pct = 0
    chg.delete_pct = 0
    chg.mod_down_pct = 0
    chg.imbalance = 0


cdef str condition_to_string(StateDiff *diff):
    cdef uint16_t c = diff.condition
    if c == DIFF_NOTHING:
        return 'NOTHING'
    elif c == DIFF_TRADE_PART:
        return 'TRADE_PART'
    elif c == DIFF_TRADE_FULL:
        return 'TRADE_FULL'
    elif c == DIFF_TRADE_MULT:
        return 'TRADE_MULT'
    elif c == DIFF_MOD_DOWN:
        return 'MOD_DOWN'
    elif c == DIFF_MOD_UP:
        return 'MOD_UP'
    elif c == DIFF_ADD:
        return 'ADD'
    elif c == DIFF_DELETE:
        return 'DELETE'
    else:
        return '???'


cdef class ContractMarketState:
    cdef:
        bint wide
        BookLevel bid, ask, last_bid, last_ask
        StateDiff dbid, dask
        uint64_t secid, seqnum
        double bid_imbalance, ask_imbalance
        readonly:
            int32_t order_turned_bid, order_turned_ask
   
    def __init__(self, uint64_t secid):
        self.secid = secid
        self.seqnum = 0
        self.bid.qty = 0
        self.ask.qty = 0
        self.dbid.qty = 0
        self.dask.qty = 0
        self.order_turned_bid = 0
        self.order_turned_ask = 0
        self.bid_imbalance = 0
        self.ask_imbalance = 0
        self.wide = False
        
    cdef void _update(self, MDOrderBook *md, int32_t min_tick) nogil:
        cdef:
            BookLevel *bid
            BookLevel *ask
            BookLevel *last_bid
            BookLevel *last_ask
        if md.secid != self.secid:
            return
        self.last_bid = self.bid
        self.last_ask = self.ask
        bid = &(md.bids[0])
        ask = &(md.asks[0])
        last_bid = &(self.last_bid)
        last_ask = &(self.last_ask)
        if bid.qty > 0 and ask.qty > 0:
            # compute bidqty and askqty diffs
            update_state_diff(SIDE_BID, md.sellvolume, bid, last_bid, &(self.dbid))
            update_state_diff(SIDE_ASK, md.buyvolume, ask, last_ask, &(self.dask))
            # is it wide?
            self.wide = ask.price - bid.price > min_tick
            # save current market state
            self.seqnum = md.seqnum
            self.bid = dereference(bid)
            self.ask = dereference(ask)
            if self.bid.price == self.last_ask.price:
                self.order_turned_bid = self.bid.qty + md.buyvolume
            else:
                self.order_turned_bid = 0
            if self.ask.price == self.last_bid.price:
                self.order_turned_ask = self.ask.qty + md.sellvolume
            else:
                self.order_turned_ask = 0
            # compute imbalance
            if self.wide:
                self.bid_imbalance = 0
                self.ask_imbalance = 0
            else:
                self.bid_imbalance = <double>(bid.qty - ask.qty)/(bid.qty + ask.qty)
                self.ask_imbalance = -1.0 * self.bid_imbalance

    cdef bint _changed(self) nogil:
        cdef:
            BookLevel *bid = &(self.bid)
            BookLevel *ask = &(self.ask)
            BookLevel *last_bid = &(self.last_bid)
            BookLevel *last_ask = &(self.last_ask)
        if bid.qty == 0 or ask.qty == 0:
            return False
        return bid.price != last_bid.price \
            or ask.price != last_ask.price \
            or bid.qty != last_bid.qty \
            or ask.qty != last_ask.qty \
            or bid.participants != last_bid.participants \
            or ask.participants != last_ask.participants

    cdef bint _new_top(self) nogil:
        return self.bid.price != self.last_bid.price \
            or self.ask.price != self.last_ask.price


cdef class TradeAccumulators:
    cdef:
        VolumeAccumulator buy_part, buy_full, buy_mult, sell_part, sell_full, sell_mult

    def __init__(self, uint64_t time_window):
        self.buy_part.Init(time_window)
        self.buy_full.Init(time_window)
        self.buy_mult.Init(time_window)
        self.sell_part.Init(time_window)
        self.sell_full.Init(time_window)
        self.sell_mult.Init(time_window)

    cdef void _add(self, uint16_t side, MDOrderBook *md, uint16_t condition) nogil:
        cdef:
            int32_t buy_part_qty = 0, buy_full_qty = 0, buy_mult_qty = 0
            int32_t sell_part_qty = 0, sell_full_qty = 0, sell_mult_qty = 0
        if side == SIDE_BID:
            if condition == DIFF_TRADE_PART:
                buy_part_qty = md.buyvolume
            elif condition == DIFF_TRADE_FULL:
                buy_full_qty = md.buyvolume
            else:
                buy_mult_qty = md.buyvolume
        elif side == SIDE_ASK:
            if condition == DIFF_TRADE_PART:
                sell_part_qty = md.sellvolume
            elif condition == DIFF_TRADE_FULL:
                sell_full_qty = md.sellvolume
            else:
                sell_mult_qty = md.sellvolume
        self.buy_part.AddTrade(md.time, buy_part_qty)
        self.buy_full.AddTrade(md.time, buy_full_qty)
        self.buy_mult.AddTrade(md.time, buy_mult_qty)
        self.sell_part.AddTrade(md.time, sell_part_qty)
        self.sell_full.AddTrade(md.time, sell_full_qty)
        self.sell_mult.AddTrade(md.time, sell_mult_qty)
        

    cdef void _add_nothing(self, MDOrderBook *md) nogil:
        self._add(0, md, DIFF_NOTHING)

    cdef int32_t _part_qty(self, uint16_t side) nogil:
        if side == SIDE_BID:
            return self.buy_part.Qty()
        else:
            return self.sell_part.Qty()

    cdef int32_t _full_qty(self, uint16_t side) nogil:
        if side == SIDE_BID:
            return self.buy_full.Qty()
        else:
            return self.sell_full.Qty()

    cdef int32_t _mult_qty(self, uint16_t side) nogil:
        if side == SIDE_BID:
            return self.buy_mult.Qty()
        else:
            return self.sell_mult.Qty()

    cdef void _reset(self) nogil:
        self.buy_part.Clear()
        self.buy_full.Clear()
        self.buy_mult.Clear()
        self.sell_part.Clear()
        self.sell_full.Clear()
        self.sell_mult.Clear()


cdef double compute_bid_imbalance(double bidqty, double askqty) nogil:
    return (bidqty - askqty) / (bidqty + askqty)


cdef class VacuumAccumulators:
    cdef:
        VolumeAccumulator bid_delete, ask_delete, bid_mod_down, ask_mod_down

    def __init__(self, uint64_t time_window):
        self.bid_delete.Init(time_window)
        self.ask_delete.Init(time_window)
        self.bid_mod_down.Init(time_window)
        self.ask_mod_down.Init(time_window)
        
    cdef void _add(self, MDOrderBook *md, StateDiff *dbid, StateDiff *dask) nogil:
        cdef:
            int32_t bid_delete_qty = 0, ask_delete_qty = 0
            int32_t bid_mod_down_qty = 0, ask_mod_down_qty = 0
        if dbid.condition == DIFF_DELETE:
            bid_delete_qty = -dbid.qty
        elif dask.condition == DIFF_DELETE:
            ask_delete_qty = -dask.qty
        elif dbid.condition == DIFF_MOD_DOWN:
            bid_mod_down_qty = -dbid.qty
        elif dask.condition == DIFF_MOD_DOWN:
            ask_mod_down_qty = -dask.qty
        self.bid_delete.AddTrade(md.time, bid_delete_qty)
        self.ask_delete.AddTrade(md.time, ask_delete_qty)
        self.bid_mod_down.AddTrade(md.time, bid_mod_down_qty)
        self.ask_mod_down.AddTrade(md.time, ask_mod_down_qty)

    cdef int32_t _delete_qty(self, uint16_t side) nogil:
        if side == SIDE_BID:
            return self.bid_delete.Qty()
        else:
            return self.ask_delete.Qty()

    cdef int32_t _mod_down_qty(self, uint16_t side) nogil:
        if side == SIDE_BID:
            return self.bid_mod_down.Qty()
        else:
            return self.ask_mod_down.Qty()

    cdef void _reset(self) nogil:
        self.bid_delete.Clear()
        self.ask_delete.Clear()
        self.bid_mod_down.Clear()
        self.ask_mod_down.Clear()


cdef class Christine(StrategyInterface):
    cdef:
        uint64_t secid

    def __init__(self, uint64_t secid):
        self.secid = secid
        self.initialize([secid])

    def evaluate_md(self):
        cdef:
            MDOrderBook *md = self.md_ptr
            BookLevel bid

        if not self.valid_md:
            return

        # do whatever you want with md
        #
        # cppclass MDOrderBook:
        #       uint16_t packet_len, eop, msg_idx
        #       vector[char] topic
        #       uint64_t secid, seqnum, time, exch_time, channel
        #       int32_t buyprice, buyvolume, sellprice, sellvolume
        #       vector[BookLevel] bids
        #       vector[BookLevel] asks
        bid = md.bids[0]
        print 'hey wow!', bid.price, bid.qty, bid.participants
        pass



cdef class FlipperDataCollect(StrategyInterface):
    """FlipperV1

    """
    cdef:
        TradeAccumulators acc_trades
        VacuumAccumulators acc_vacuums
        bint new_top_market, allow_bid_taker, allow_ask_taker
        uint64_t uniq_seqnum
        # keep track of some variables
        map_[int32_t, int32_t] bid_order_qtys, ask_order_qtys
        ContractMarketState state, immediate_state
        StateChange bid_chg, ask_chg
        double lowest_bid_imbalance, lowest_ask_imbalance
        # readable in interpreter
        readonly:
            uint64_t secid, accumulated_time_window
            int32_t min_tick, order_size
            str name
            double imbalance_threshold_to_place

    def __init__(self, uint64_t secid, name='', int32_t min_tick=0,
            uint64_t accumulated_time_window=0, double imbalance_threshold_to_place=0,
            int32_t order_size=1):
        # initialize using StrategyInterface's initialize method, which requires list of
        # secids
        self.initialize([secid])
        self.secid = secid
        self.name = str(name)
        self.min_tick = min_tick
        self.imbalance_threshold_to_place = imbalance_threshold_to_place
        if order_size <= 0:
            order_size = 1
        self.order_size = order_size
        # initialize ContractMarketState
        self.state = ContractMarketState(self.secid)
        self.immediate_state = ContractMarketState(self.secid)
        # initialize VolumeAccumulator objects
        self.acc_vacuums = VacuumAccumulators(accumulated_time_window)
        self.acc_trades = TradeAccumulators(accumulated_time_window)
        # reset some values to start
        clear_state_change(&(self.bid_chg))
        clear_state_change(&(self.ask_chg))
        self.new_top_market = False
        self.allow_bid_taker = False
        self.allow_bid_taker = False
        self.uniq_seqnum = 1
        self.lowest_bid_imbalance = imbalance_threshold_to_place
        self.lowest_ask_imbalance = imbalance_threshold_to_place

    def __repr__(self):
        return '%s{name=%s}' % (self.__class__.__name__, self.name)

    cdef void _save_market(self, MDOrderBook *md):
        self.state._update(md, self.min_tick)

    cdef void _compute_state_change(self):
        cdef:
            ContractMarketState state
            TradeAccumulators acc_trades
            VacuumAccumulators acc_vacuums

        state = self.state
        if state.wide:
            return

        acc_trades = self.acc_trades
        acc_vacuums = self.acc_vacuums
        update_state_change(state, acc_trades, acc_vacuums, SIDE_BID, &(self.bid_chg))
        update_state_change(state, acc_trades, acc_vacuums, SIDE_ASK, &(self.ask_chg))

    cdef str _context_builder(self):
        # only to be run after updated market state
        cdef:
            ContractMarketState state = self.state
            StateChange *bid_chg = &(self.bid_chg)
            StateChange *ask_chg = &(self.ask_chg)
            str context
        context = (
            'uniq_seqnum=%d, '
            #'%d/%d (%dx%d); '
            'turned for: (%d, %d) '
            'bid: ('
            'total=%d, imb_chg=%0.4f, '
            'delete_qty=%d (%0.4f), '
            'mod_down=%d (%0.4f), '
            'part=%d (%0.4f), '
            'full=%d (%0.4f), '
            'mult=%d (%0.4f), '
            'imb_curr=%0.4f, imb_lowest=%0.4f'
            '), ask: ('
            'total=%d, imb_chg=%0.4f, '
            'delete_qty=%d (%0.4f), '
            'mod_down=%d (%0.4f), '
            'part=%d (%0.4f), '
            'full=%d (%0.4f), '
            'mult=%d (%0.4f), '
            'imb_curr=%0.4f, imb_lowest=%0.4f'
            ')'
        ) % (
            self.uniq_seqnum, 
            #state.bid.price, state.ask.price, state.bid.qty, state.ask.qty,
            state.order_turned_bid, state.order_turned_ask,
            bid_chg.total, bid_chg.imbalance,
            bid_chg.delete_qty, bid_chg.delete_pct,
            bid_chg.mod_down, bid_chg.mod_down_pct,
            bid_chg.part, bid_chg.part_pct,
            bid_chg.full, bid_chg.full_pct,
            bid_chg.mult, bid_chg.mult_pct,
            state.bid_imbalance, self.lowest_bid_imbalance,
            ask_chg.total, ask_chg.imbalance,
            ask_chg.delete_qty, ask_chg.delete_pct,
            ask_chg.mod_down, ask_chg.mod_down_pct,
            ask_chg.part, ask_chg.part_pct,
            ask_chg.full, ask_chg.full_pct,
            ask_chg.mult, ask_chg.mult_pct,
            state.ask_imbalance, self.lowest_ask_imbalance
        )
        return context

    cdef void _handle_liquidity_removals(self, MDOrderBook *md):
        cdef:
            StateDiff *dbid = &(self.state.dbid)
            StateDiff *dask = &(self.state.dask)
        # handle trades
        if md.buyvolume > 0:
            self.acc_trades._add(SIDE_BID, md, dask.condition)
        elif md.sellvolume > 0:
            self.acc_trades._add(SIDE_ASK, md, dbid.condition)
        else:
            self.acc_trades._add_nothing(md)
        # and handle market deletes and mod down
        self.acc_vacuums._add(md, dbid, dask)
        
    cdef void _reset_accumulators(self):
        self.acc_trades._reset()
        self.acc_vacuums._reset()

    cdef void _place_bid_taker(self, MDOrderBook *md):
        # def place_ghostlimit_order(time, seqnum, secid, price, side, qty, context)
        if self.allow_bid_taker:
            self.place_ghostlimit_order(md.time, md.seqnum, self.secid, self.state.ask.price, 
                SIDE_BID, self.order_size, self._context_builder())
        #    print 'buy, seqnum=%d, lowest_bid_imbalance=%0.4f' % (md.seqnum, self.lowest_bid_imbalance),
        #else:
        #    print 'buy not allowed... seqnum=%d' % md.seqnum,
        #print self._context_builder()
        
    cdef void _place_ask_taker(self, MDOrderBook *md):
        # def place_ghostlimit_order(time, seqnum, secid, price, side, qty, context)
        if self.allow_ask_taker:
            self.place_ghostlimit_order(md.time, md.seqnum, self.secid, self.state.bid.price, 
                SIDE_ASK, self.order_size, self._context_builder())
        #    print 'sell, seqnum=%d, lowest_ask_imbalance=%0.4f' % (md.seqnum, self.lowest_ask_imbalance),
        #else:
        #    print 'sell not allowed... seqnum=%d' % md.seqnum,
        #print self._context_builder()

    cdef void _arm_bid_taker(self) nogil:
        self.allow_bid_taker = True

    cdef void _disarm_bid_taker(self) nogil:
        self.allow_bid_taker = False
        
    cdef void _arm_ask_taker(self) nogil:
        self.allow_ask_taker = True

    cdef void _disarm_ask_taker(self) nogil:
        self.allow_ask_taker = False
        
    def evaluate_md(self):
        cdef:
            # pointers for ease of use
            MDOrderBook *md = self.md_ptr
            ContractMarketState state

        # bypass invalid market data
        if not self.valid_md:
            return

        # save market; can't be bothered about unchanged top of book
        self.immediate_state._update(md, self.min_tick)
        if not self.immediate_state._changed():
            return

        # since the top market changed, save it
        state = self.state
        self._save_market(md)
        # should we reset accumulators on the next pass?
        self.new_top_market = state._new_top()
        if self.new_top_market or state.wide:
            self._disarm_bid_taker()
            self._disarm_ask_taker()
            self.lowest_bid_imbalance = self.imbalance_threshold_to_place
            self.lowest_ask_imbalance = self.imbalance_threshold_to_place
        else:
            if not self.allow_bid_taker and state.bid_imbalance < self.imbalance_threshold_to_place:
                self._arm_bid_taker()
                print 'bid re-armed at seqnum=%d, bid_imbalance=%0.4f' % (md.seqnum, state.bid_imbalance)
            if not self.allow_ask_taker and state.ask_imbalance < self.imbalance_threshold_to_place:
                self._arm_ask_taker()
                print 'ask re-armed at seqnum=%d, ask_imbalance=%0.4f' % (md.seqnum, state.ask_imbalance)

        # has any liquidity been removed from the market (trades, deletes, etc)?
        self._handle_liquidity_removals(md)

        # compute state changes from changes in liquidity
        self._compute_state_change()

        # TODO: handle order acks


        # TODO: place orders
        # place bid taker order?
        if self.ask_chg.total > 0 and state.bid_imbalance > self.imbalance_threshold_to_place:
            self._place_bid_taker(md)
        elif state.bid_imbalance < self.lowest_bid_imbalance:
            self.lowest_bid_imbalance = state.bid_imbalance
        # place ask taker order?
        if self.bid_chg.total > 0 and state.ask_imbalance > self.imbalance_threshold_to_place:
            self._place_ask_taker(md)
        elif state.ask_imbalance < self.lowest_ask_imbalance:
            self.lowest_ask_imbalance = state.ask_imbalance

        # print context for debugging
        #print self._context_builder()

        # did the top book change recently? we should:
        # 1) reset accumulators
        # 2) uptick unique market seqnum
        if self.new_top_market:
            self._reset_accumulators()
            self.uniq_seqnum += 1



    @staticmethod
    def parse_context(df, inplace=True):
        search_str = (
            r'uniq_seqnum=([0-9]+), '
            r'turned for: \(([0-9]+), ([0-9]+)\) '
            r'bid: \('
            r'total=([0-9]+), imb_chg=([\.\-0-9]+), '
            r'delete_qty=([0-9]+) \(([\.0-9]+)\), '
            r'mod_down=([0-9]+) \(([\.0-9]+)\), '
            r'part=([0-9]+) \(([\.0-9]+)\), '
            r'full=([0-9]+) \(([\.0-9]+)\), '
            r'mult=([0-9]+) \(([\.0-9]+)\), '
            r'imb_curr=([\-\.0-9]+), imb_lowest=([\-\.0-9]+)'
            r'\), ask: \('
            r'total=([0-9]+), imb_chg=([\.\-0-9]+), '
            r'delete_qty=([0-9]+) \(([\.0-9]+)\), '
            r'mod_down=([0-9]+) \(([\.0-9]+)\), '
            r'part=([0-9]+) \(([\.0-9]+)\), '
            r'full=([0-9]+) \(([\.0-9]+)\), '
            r'mult=([0-9]+) \(([\.0-9]+)\), '
            r'imb_curr=([\-\.0-9]+), imb_lowest=([\-\.0-9]+)'
            r'\)'
        )
        context = df['context'].str.extract(search_str, expand=True)

        convert_cols = [
            ('uniq_seqnum', int),
            ('bid_order_turn', int),
            ('ask_order_turn', int),
            ('bid_tot', int),
            ('bid_imb_chg', float),
            ('bid_delete', int),
            ('bid_delete_pct', float),
            ('bid_mod_down', int),
            ('bid_mod_down_pct', float),
            ('bid_part', int),
            ('bid_part_pct', float),
            ('bid_full', int),
            ('bid_full_pct', float),
            ('bid_mult', int),
            ('bid_mult_pct', float),
            ('bid_curr_imb', float),
            ('bid_lowest_imb', float),
            ('ask_tot', int),
            ('ask_imb_chg', float),
            ('ask_delete', int),
            ('ask_delete_pct', float),
            ('ask_mod_down', int),
            ('ask_mod_down_pct', float),
            ('ask_part', int),
            ('ask_part_pct', float),
            ('ask_full', int),
            ('ask_full_pct', float),
            ('ask_mult', int),
            ('ask_mult_pct', float),
            ('ask_curr_imb', float),
            ('ask_lowest_imb', float),
        ]
        context.columns = map(lambda x: x[0], convert_cols)

        if inplace:
            for col, dtype in convert_cols:
                if dtype is None:
                    df[col] = context[col]
                else:
                    df[col] = context[col].astype(dtype)
        else:
            for col, dtype in convert_cols:
                if dtype is not None:
                    context[col] = context[col].astype(dtype)
        return context

