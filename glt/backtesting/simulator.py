import sys
import json
import datetime as dt

from pandas import to_timedelta
from glt.md.standard import MDSource, which_md
from glt.backtesting import MatchingEngine
from glt.backtesting.base import StrategyInterface
from time import sleep


config_keys = [
    'matchingEngines',
    'strategies'
]

matching_engine_config_keys = [
    'secids',
    'assumptions'
]

matching_engine_config_time_keys = [
    'timeZoneOffset',
    'start',
    'end'
]

matching_engine_config_assumptions_keys = [
    'orderEntryLatency',
    'lookForwardTime',
    'useExchangeTime'
]

strategy_config_keys = [
    'objectName',
    'parameters'
]

hindsight_config_keys = [
    'lookForwardTime'
]

matching_engine_attributes = [
    'latency'
]


def check_attr(obj, attr):
    return 'yes' if hasattr(obj, attr) else 'no'


def now():
    return '%s:' % dt.datetime.now()


def process_md(me):
    me.read_md()


def read_config(config_filename):
    with open(config_filename, 'rb') as config_file:
        config = json.load(config_file)

    for key in config_keys:
        if key not in config:
            raise KeyError('"%s" not present in config. please verify' % key)

    secids, me_configs = digest_user_me_config(config['matchingEngines'])
    strategies_config = digest_user_strategy_config(config['strategies'])

    return secids, me_configs, strategies_config


def init_global_strategy(strategy_config):
    strategy_object = strategy_config['objectName']

    # attempt import
    exec('from glt.backtesting import %s' % strategy_object)

    strategy_parameters = strategy_config['parameters']

    Strategy = locals()[strategy_object]

    strategies = []

    for me_id in strategy_config['useMatchingEngineIDs']:
        print(now(), 'initializing %s for me_id=%d' % (strategy_object, me_id))
        print(now(), 'parameters: %s' % strategy_parameters)

        strategy = Strategy(**strategy_parameters)

        print(now(), 'checking parameters:')
        for param in sorted(strategy_parameters):
            print(now(), '    %s... %s' % (param, check_attr(strategy, param)))
        print(now(), 'done.')

        # run diagnostics
        #print now(), 'running strategy diagnostics for %s' % strategy
        #print now(), 'checking required attributes:'
        #for attr in strategy_attributes:
        #    print now(), '    %s... %s' % (attr, check_attr(strategy, attr))
        #print now(), 'done.'

        # assign matching engine
        strategy.use_matching_engine(me_id)

        strategies.append(strategy)

    return strategies


def digest_user_me_config(user_me_config):
    me_configs = {}
    secids = set()
    for config in user_me_config:
        me_id = config['ID']
        for key in matching_engine_config_keys:
            if key not in config:
                raise KeyError(
                    '"%s" not present in matchingEngine config ID=%d. please verify'
                    '' % (key, me_id)
                )

        assumptions_config = config['assumptions']
        for key in matching_engine_config_assumptions_keys:
            if key not in assumptions_config:
                raise KeyError(
                    '"%s" not present in matchingEngine(ID=%d):assumptions config. '
                    'please verify' % (key, me_id)
                )

        # change some string timedeltas to ints
        secids.update(config['secids'])
        assumptions_config['latency'] = to_timedelta(assumptions_config['orderEntryLatency']).value
        assumptions_config['look_forward_time'] = to_timedelta(assumptions_config['lookForwardTime']).value
        config['me_id'] = me_id
        if me_id in me_configs:
            raise KeyError('duplicate matchingEngine(ID=%d). please verify' % me_id)
        me_configs[me_id] = config
    return list(secids), me_configs


def digest_user_strategy_config(user_strategy_config):
    for i, strategy_config in enumerate(user_strategy_config):
        for key in strategy_config_keys:
            if key not in strategy_config:
                raise KeyError('"%s" not present in strategy config #%d. please verify' % (key, i))
    return user_strategy_config


class Simulator:
    def __init__(self, user_me_config):
        self.ready = False
        self.secids, self.me_configs = digest_user_me_config(user_me_config)
        # md_src
        self.__init_md_source()
        # matching engines
        self.__init_matching_engines()
        # strategies
        self.strategies = []
        # not ready yet...
        self.output_filename = None
        self.ready = False

    def __init_md_source(self):
        md_src = MDSource()
        md_src.initialize(self.secids)
        self.md_src = md_src
        
    def __init_matching_engines(self):
        me_configs = self.me_configs
        md_src = self.md_src
        me_map = {}
        for me_id, me_config in me_configs.items():
            me_assumptions = me_config['assumptions']
            me = MatchingEngine(
                me_config['secids'],
                me_id=me_id,
                look_forward_time=me_assumptions['look_forward_time'],
                latency=me_assumptions['latency'],
                use_exch_time=me_assumptions['useExchangeTime']
            )
            me.ref_md_from(md_src)
            me_map[me_id] = me

            print(now(), 'checking matching engine attributes for ID=%d' % me_id)
            for attr in matching_engine_attributes:
                print(now(), '    %s... %s' % (attr, getattr(me, attr)))
            print(now(), 'done.')
        self.me_map = me_map

    def init_strategy(self, StrategyObject, strategy_parameters, me_id=None):
        strategy = StrategyObject(**strategy_parameters)
        self.__add_strategy(strategy, me_id=me_id)
        
    def __add_strategy(self, strategy, me_id=None):
        me_map = self.me_map
        if me_id is None or me_id not in me_map:
            raise KeyError('me_id=%d not recognized. check your matching engine config')
        if not issubclass(strategy.__class__, StrategyInterface):
            raise ValueError('strategy class "%s" does not inherit StrategyInterface')
        md_src = self.md_src
        strategy.use_matching_engine(me_id)
        if strategy.is_matching_engine_assigned():
            strategy.ref_md_from(md_src)
            me_map[strategy.matching_engine_id()].add_strategy(strategy)
            self.strategies.append(strategy)

    def process_from(self, stream, exchange):
        if not self.ready:
            raise ValueError('simulator not ready. call .write_output_to(filename) and try again')
        next(stream)
        md_src = self.md_src
        if self.output_filename is None:
            num_lines = self.__parse_stream(stream, exchange)
        else:
            with open(self.output_filename, 'wb') as f:
                old_stdout = sys.stdout
                sys.stdout = f
                num_lines = self.__parse_stream(stream, exchange)
                sys.stdout = old_stdout
        return num_lines

    def __parse_stream(self, stream, exchange):
        mes = [me for me in list(self.me_map.values()) if len(me.strategies)]
        md_src = self.md_src
        md_type = which_md(exchange)
        try:
            for i, line in enumerate(stream):
                md_src.parse_csv_md(str.encode(line[:-1]), md_type)
                list(map(process_md, mes))
        finally:
            print(now(), 'calling .gc() on all strategy objects')
            list(map(lambda strategy: strategy.gc(), self.strategies))
        #if you are here looking for an UnboundLocalError... check your data file, is it empty?
        return i
        
    def write_output_to(self, filename):
        self.output_filename = filename
