# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from libc.stdint cimport int32_t, int64_t, uint16_t, uint32_t, uint64_t
from numpy cimport ndarray
from cython.operator cimport dereference
from libc.stdlib cimport malloc, free, atoi
from libcpp.map cimport map as map_
from libcpp.vector cimport vector as vector_
from glt.md.standard cimport MDSource, BookLevel, MDOrderBook, VolumeAccumulator, TOPIC_SIZE, copy_mdorderbook
from glt.backtesting.base cimport StrategyInterface
from glt.math.helpful_functions cimport double_gte, double_eq

import re
from pandas import to_datetime, to_timedelta, NaT, DataFrame, merge as df_merge, options as pd_options
from numpy import arange, float64, exp, dot, sqrt, empty, matmul, array
from numpy.linalg import norm


cdef uint16_t TOPIC_TYPE_MSG = 2
cdef uint16_t TOPIC_TYPE_MAGIC = 3

cdef char CHAR_NULL = 0
cdef char CHAR_BLANK = 32
cdef char CHAR_SLASH = 47

cdef uint64_t ONE_MICROSECOND = 1000
cdef uint64_t ONE_MILLISECOND = 1000 * ONE_MICROSECOND
cdef uint64_t ONE_SECOND = 1000 * ONE_MILLISECOND


cdef int32_t MINIMUM_QTY = 1


cdef struct MagicQuantities:
    int32_t bid, ask


cdef double calc_imbalance(int32_t bidqty, int32_t askqty, double direction) nogil:
    cdef double diff, total
    if bidqty + askqty == 0:
        return 0
    diff = direction * (bidqty - askqty)
    total = bidqty + askqty
    return diff / total


cdef void parse_topic_for_magic_quantities(vector_[char] *topic,
        char* bidqty_buf, char* askqty_buf, MagicQuantities *qty):
    cdef:
        uint16_t i = TOPIC_TYPE_MAGIC, loc = 0
    while (i < TOPIC_SIZE and dereference(topic)[i] != CHAR_SLASH):
        bidqty_buf[loc] = dereference(topic)[i]
        i += 1
        loc += 1
    bidqty_buf[loc] = CHAR_NULL
    i += 1
    loc = 0
    while (i < TOPIC_SIZE and dereference(topic)[i] != CHAR_BLANK):
        askqty_buf[loc] = dereference(topic)[i]
        i += 1
        loc += 1
    askqty_buf[loc] = CHAR_NULL
    qty.bid = atoi(bidqty_buf)
    qty.ask = atoi(askqty_buf)


def _regex_vol_to_dataframe(output_str, vol_type):

    if (vol_type.lower() == 'raw' or vol_type.lower() == 'smooth' or 
            vol_type.lower() == 'deriv' or vol_type.lower() == 'bid' or
            vol_type.lower() == 'ask' or vol_type.lower() == 'px' or
            vol_type.lower() == 'apx' or vol_type.lower() == 'bpx'):
        vol_type = vol_type.capitalize()
    else:
        raise ValueError('Incorrect vol_type parameter.  Choice is "raw", "smooth" or "deriv".')
    first_line = re.split('RawVol|SmoothVol', output_str, maxsplit=2)[1]
    n_strikes = len(re.findall(',', first_line)) - 3

    # \d are digits, + is >= 1, * is any/none, ? is 0/1 of preceeding

    #get df columns
    #stub = r' ([0-9\.]+)=-?[0-9\.]+,?'
    stub = r' ([0-9\.]+)=-?[0-9\.na]+,?'
    cols_str = stub * n_strikes
    strike_search = re.compile(cols_str)
    cols = ['ts', 'tte', 'spot', 'roll'] + list(strike_search.findall(first_line)[0])

    #get df data
    vol_str = r'%sVol{ts=([0-9]+), tte=([0-9\.na]+), spot=([0-9\.]+), roll=(-?[0-9\.na]+),' % vol_type
    stub = r' [0-9\.]+=(-?[0-9\.na]+),?' * n_strikes
    vol_str += stub
    vol_search = re.compile(vol_str)
    vols = vol_search.findall(output_str)

    df = DataFrame(vols, columns=cols, index=range(len(vols)))
    return df

def regex_vol_to_dataframe(filename, vol_type):
    #vol_type is either 'raw' or 'smooth' or 'deriv'
    with open(filename) as f:
        output = f.read()
    
    df_vols = _regex_vol_to_dataframe(output, vol_type)

    df_cols = df_vols.columns
    
    int_cols = ['ts']
    float_cols = filter(lambda x: x not in int_cols, df_cols)

    for col in int_cols:
        df_vols[col] = df_vols[col].astype(int)
    for col in float_cols:
        df_vols[col] = df_vols[col].astype(float64)
    df_vols['time'] = to_datetime(df_vols['ts'])

    return df_vols

def regex_mockorder_to_dataframe(filename):
    """regex_mockorder_to_dataframe(filename)

Read output of MockOrder print to stdout (MockOrder.__repr__) from a backtester
script. This assumes a standard output from MockOrder; if the output changes,
this function will be changed as well. Regex is used to search through the
output file to find the relevant MockOrder data and convert all columns to
their respective data types (mostly int values). Returns pandas DataFrame.

Parameters
----------
filename : str
    Filename of backtester's output file of MockOrders (and other data). Only
    MockOrder output will be extracted.

    """
    mockorder_search = re.compile(
        (
            r'MockOrder{secid=([0-9]+), strategy={(.*)}, '
            r'trig={ts=([0-9]+), seqnum=([0-9]+)}, '
            r'trade={ts=([0-9]+), in=([0-9]+), out=([0-9]+)}, '
            r'order={prc=([0-9]+), side=([12]), qty=([0-9]+)}, '
            r'match={id=([\-0-9]+) \(([0-9]+) @ ([0-9]+)\)}, '
            r'eval={dt=([0-9]+) \(([0-9]+) @ ([0-9]+)\), ticks={order=([\-0-9]+), match=([\-0-9]+)}}, '
            r'context={(.*)}}'
        )
    )
    eval_search = re.compile(r'number (of misses|evaluated):   ([0-9]+)')

    mockorder_columns = [
        'secid', 'strategy', 'trig_time', 'trig_seqnum', 'trade_time', 'seqnum_in', 'seqnum_out',
        'order_prc', 'order_side', 'order_qty', 'match_id', 'match_prc', 'match_qty',
        'eval_dt', 'eval_best_qty', 'eval_best_prc',  'order_ticks', 'match_ticks',
        'context'
    ]
    time_cols = [
        'trig_time', 'trade_time'
    ]
    dt_cols = [
        'eval_dt'
    ]
    str_cols = [
        'strategy',
        'context'
    ]

    int_cols = filter(lambda x: x not in str_cols, mockorder_columns)

    with open(filename, 'r') as f:
        out = f.read()

    if 'MockOrder' not in out and 'number of fills:' not in out:
        raise ValueError('incomplete output file %s. quitting' % filename)

    trades = DataFrame(mockorder_search.findall(out), columns=mockorder_columns)
    num_fills = sum(map(lambda x: int(x[1]), eval_search.findall(out)))

    if len(trades) != num_fills:
        raise ValueError('disagree on num_fills... len(trades)=%d, num_fills=%d' % (len(trades), num_fills))

    for col in int_cols:
        trades[col] = trades[col].astype(int)
    for col in time_cols:
        trades[col] = to_datetime(trades[col])
    for col in dt_cols:
        trades[col] = to_timedelta(trades[col])
    # change trade_time to NAT if there was no match
    trades.loc[trades['match_id']==-1, 'trade_time'] = NaT
    return trades


def _regex_simulatedorder_to_dataframe(output_str):
    simorder_search = re.compile(
        r'SimulatedOrder\{id=([0-9]+), client_orderid=([0-9]+), me_id=([\-]*[0-9]+), strategy=\{(.*)\}, '
        r'secid=([0-9]+), live=([01]), order_type=([A-Za-z0-9]+), seqnums=\(([0-9]+), ([0-9]+), ([0-9]+)\), '
        r'times=\(([0-9]+), ([0-9]+), ([0-9]+)\), price=([0-9]+), side=([12]), qty=([0-9]+), '
        r'rem_qty=([0-9]+), first_pos=([0-9]+), last_pos=([0-9]+), context=\{(.*)\}, num_fills=([0-9]+)\}'
    )

    orders_cols = [
        'order_id', 'client_order_id', 'me_id', 'strategy', 'secid', 'live', 'order_type',
        'trig_seqnum', 'place_seqnum', 'cancel_seqnum', 'trig_time', 'place_time', 'cancel_time',
        'order_prc', 'order_side', 'order_qty', 'rem_qty', 'first_pos', 'last_pos', 'context', 'num_fills'
    ]
    new_orders = DataFrame(simorder_search.findall(output_str), columns=orders_cols)
    new_orders['order_id'] = new_orders['order_id'].astype(int)
    return new_orders, new_orders.columns.tolist()


def _regex_simulatedfill_to_dataframe(output_str):
    simfill_search = re.compile(
        r'SimulatedFill\{id=([0-9]+), orderid=([0-9]+), secid=[0-9]+, seqnum=([0-9]+), time=([0-9]+), '
        r'price=([0-9]+), side=[12], qty=([0-9]+), last_position=([0-9]+), condition=([0-9]+), '
        r'eval_seqnum=([0-9]+), eval_dt=([0-9]+), eval_price=([\-0-9]+), eval_qty=([0-9]+), '
        r'match_ticks=([\-]*[0-9]+), order_ticks=([\-]*[0-9]+)\}'
    )

    fills_cols = [
        'match_id', 'order_id', 'match_seqnum', 'match_time',
        'match_prc', 'match_qty', 'last_position', 'condition',
        'eval_seqnum', 'eval_dt', 'eval_best_prc', 'eval_best_qty', 'match_ticks', 'order_ticks'
    ]
    new_fills = DataFrame(simfill_search.findall(output_str), columns=fills_cols)
    new_fills['order_id'] = new_fills['order_id'].astype(int)
    return new_fills, new_fills.columns.tolist()


def regex_simulation_to_dataframe(filename):
    """regex_simulation_to_dataframe(filename)

Read output of SimulatedOrder and SimulatedFill print to stdout from a backtester
script. This assumes a standard output from SimulatedOrder and SimulatedFill; if
the output changes, this function will be changed as well. Regex is used to
search through the output file to find the relevant MockOrder data and convert
all columns to their respective data types (mostly int values). Returns pandas
DataFrame.

Parameters
----------
filename : str
    Filename of backtester's output file of SimulatedOrder and SimulatedFill objects
    (and other data). Only SimulatedOrder and SimulatedFill will be extracted.
"""
    pd_options.mode.chained_assignment = None
    with open(filename) as f:
        output = f.read()
    orders, orders_cols = _regex_simulatedorder_to_dataframe(output)
    fills, fills_cols = _regex_simulatedfill_to_dataframe(output)

    merged = df_merge(fills, orders, on='order_id', how='left')
    not_filled = orders.loc[~orders['order_id'].isin(merged['order_id'].values)]

    all_cols = orders_cols + filter(lambda c: 'order_id'!=c, fills_cols)

    for col in merged:
        if col not in not_filled:
            if 'seqnum' in col or 'id' in col:
                not_filled[col] = -1
            else:
                not_filled[col] = 0
    data = merged.append(not_filled).loc[:, all_cols].sort_values(['order_id', 'match_id'])
    data.index = arange(len(data))

    int_cols = [
        'client_order_id', 'me_id', 'secid',
        'trig_seqnum', 'place_seqnum', 'cancel_seqnum',
        'trig_time', 'place_time', 'cancel_time',
        'order_prc', 'order_side', 'order_qty', 'rem_qty',
        'first_pos', 'last_pos', 'num_fills',
        'match_id', 'match_seqnum', 'match_time',
        'match_prc', 'match_qty', 'last_position', 'condition',
        'eval_seqnum', 'eval_dt', 'eval_best_prc', 'eval_best_qty',
        'match_ticks', 'order_ticks'
    ]
    bool_cols = [
        'live'
    ]
    time_cols = [
        'trig_time', 'place_time', 'cancel_time', 'match_time'
    ]
    for col in int_cols:
        if 'id' in col or 'seqnum' in col:
            data[col] = data[col].fillna(-1).astype(int)
        else:
            data[col] = data[col].fillna(0).astype(int)
    for col in time_cols:
        data[col] = to_datetime(data[col])
        data.loc[data[col].astype(int)==0, col] = NaT
    for col in bool_cols:
        data[col] = data[col].astype(int).astype(bool)
    data['eval_dt'] = to_timedelta(data['eval_dt'])

    mockorder_cols = [
        ('trade_time', 'match_time'),
        ('seqnum_in', 'place_seqnum'),
        ('seqnum_out', 'eval_seqnum'),
    ]
    for old_col, new_col in mockorder_cols:
        data[old_col] = data[new_col]
    pd_options.mode.chained_assignment = 'warn'
    return data


def simulation_config(filename):
    """simulation_config(filename)

Read config used for simulation (JSON format).

Parameters
----------
filename : str
    Filename of backtester's output file."""
    from StringIO import StringIO
    import json

    s = StringIO()
    read_cfg = False
    with open(filename, 'r') as f:
        for line in f:
            if 'reading config' in line:
                read_cfg = True
            elif 'from glt.backtesting import' in line:
                break
            elif read_cfg:
                s.write(line)
    s.seek(0)
    return json.load(s)


cdef class CVOLSlugger(StrategyInterface):
    """CVOLSlugger(secid)

    """
    cdef:
        VolumeAccumulator buytrades, selltrades, packet_cvol, buymsgs, sellmsgs
        MDOrderBook trig_md, fire_md
        bint reset_accumulators
        # readable in interpreter
        readonly:
            bint repeat_triggers, buy_trigger, sell_trigger
            int32_t buyprc, sellprc, buyqty, sellqty, buypar, sellpar
            int32_t bidprc, bidqty, bidpar, askprc, askqty, askpar
            int32_t tradeqty_threshold, last_trade_price, report_num_levels
            uint16_t ref_trig_side, last_trade_side
            uint64_t md_channel, time_window, last_seqnum, mkt_seqnum
            double buypct, sellpct, tradepct_threshold, buyparpct, sellparpct
            double qty_imbalance_before, qty_imbalance_after, par_imbalance_before, par_imbalance_after
            int32_t ref_trig_price
            str name
            # for placing orders
            uint64_t trig_seqnum, trig_secid, fire_secid

    def __init__(self, uint64_t secid, uint64_t fire_secid, uint64_t md_channel,
            uint64_t time_window=0, int32_t tradeqty_threshold=0,
            double tradepct_threshold=0, bint repeat_triggers=True, name='null'):
        # initialize using Taker's initialize method, which requires list of
        # secids
        self.initialize([secid])
        self.trig_secid = secid
        # CVOLSlugger relevant initializations
        self.md_channel = md_channel
        self.tradeqty_threshold = tradeqty_threshold
        self.tradepct_threshold = tradepct_threshold
        self.time_window = time_window
        self.repeat_triggers = repeat_triggers
        # initialize VolumeAccumulator objects
        self.buytrades.Init(time_window)
        self.selltrades.Init(time_window)
        self.buymsgs.Init(time_window)
        self.sellmsgs.Init(time_window)
        # we need to aggregate qty based on price and side and send this
        # quantity to the appropriate trade accumulator; time_window=100ms to
        # enable accumulation of anything in a packet
        self.packet_cvol.Init(100 * ONE_MILLISECOND)
        self._clear_trades()
        self.reset_accumulators = False
        self.bidprc = 0
        self.bidqty = 0
        self.bidpar = 0
        self.askprc = 0
        self.askqty = 0
        self.askpar = 0
        self.mkt_seqnum = 0
        # initialize reference prices (will not check if repeat_triggers == True)
        self.ref_trig_price = 0
        self.ref_trig_side = 0
        self.buy_trigger = False
        self.sell_trigger = False
        self.trig_seqnum = 0
        self.fire_secid = fire_secid
        self.name = str(name)
        # keep track of last trade price and side
        self.last_trade_price = 0
        self.last_trade_side = 0
        self.last_seqnum = 0
        self.qty_imbalance_before = 0
        self.qty_imbalance_after = 0
        self.par_imbalance_before = 0
        self.par_imbalance_after = 0
        self.report_num_levels = 2

    def __repr__(self):
        return 'CVOLSlugger{name=%s}' % self.name

    cdef void _clear_trades(self):
        self.buytrades.Clear()
        self.buymsgs.Clear()
        self.selltrades.Clear()
        self.sellmsgs.Clear()
        self.buyprc = 0
        self.sellprc = 0
        self._clear_qtys()

    cdef void _clear_qtys(self) nogil:
        self.buyqty = 0
        self.buypar = 0
        self.buypct = 0
        self.buyparpct = 0
        self.sellqty = 0
        self.sellpar = 0
        self.sellpct = 0
        self.sellparpct = 0
        self.buy_trigger = False
        self.sell_trigger = False

    cdef void _save_market(self, MDOrderBook *md) nogil:
        self.bidprc = md.bids[0].price
        self.askprc = md.asks[0].price
        self.bidqty = md.bids[0].qty
        self.askqty = md.asks[0].qty
        self.bidpar = md.bids[0].participants
        self.askpar = md.asks[0].participants

    cdef void _compute_accqtys(self, uint64_t exch_time):
        cdef:
            int32_t cvol_qty = self.packet_cvol.Qty()
            int32_t cvol_msgs = self.packet_cvol.Size()
        # take what we have from packet_cvol and add it to either of our trade
        # accumulators
        if self.last_trade_side == 1:
            self.buyprc = self.last_trade_price
            self.buyqty = self.buytrades.AddTrade(exch_time, cvol_qty)
            self.buypar = self.buymsgs.AddTrade(exch_time, cvol_msgs)
            self.sellqty = 0
        elif self.last_trade_side == 2:
            self.sellprc = self.last_trade_price
            self.buyqty = 0
            self.sellqty = self.selltrades.AddTrade(exch_time, cvol_qty)
            self.sellpar = self.sellmsgs.AddTrade(exch_time, cvol_msgs)
        self._compute_accpcts()
        self.packet_cvol.Clear()

    cdef void _compute_imbalance(self, double direction) nogil:
        if direction > 0:
            if self.buypct < 1.0:
                self.qty_imbalance_before = calc_imbalance(self.bidqty, self.askqty + self.buyqty, direction)
                self.qty_imbalance_after = calc_imbalance(self.bidqty, self.askqty, direction)
                self.par_imbalance_before = calc_imbalance(self.bidpar, self.askpar + self.buypar, direction)
                self.par_imbalance_after = calc_imbalance(self.bidpar, self.askpar, direction)
            else:
                self.qty_imbalance_before = calc_imbalance(self.bidqty, self.buyqty, direction)
                self.qty_imbalance_after = calc_imbalance(self.bidqty, 0, direction)
                self.par_imbalance_before = calc_imbalance(self.bidpar, self.buypar, direction)
                self.par_imbalance_after = calc_imbalance(self.bidpar, 0, direction)
        elif direction < 0:
            if self.sellpct < 1.0:
                self.qty_imbalance_before = calc_imbalance(self.bidqty + self.sellqty, self.askqty, direction)
                self.qty_imbalance_after = calc_imbalance(self.bidqty, self.askqty, direction)
                self.par_imbalance_before = calc_imbalance(self.bidpar + self.sellpar, self.askpar, direction)
                self.par_imbalance_after = calc_imbalance(self.bidpar, self.askpar, direction)
            else:
                self.qty_imbalance_before = calc_imbalance(self.sellqty, self.askqty, direction)
                self.qty_imbalance_after = calc_imbalance(0, self.askqty, direction)
                self.par_imbalance_before = calc_imbalance(self.sellpar, self.askpar, direction)
                self.par_imbalance_after = calc_imbalance(0, self.askpar, direction)

    cdef void _compute_accpcts(self) nogil:
        # buy side
        if self.buyqty > 0:
            if self.buyprc == self.askprc:
                self.buypct = (1.0 * self.buyqty) / (self.askqty + self.buyqty)
                self.buyparpct = (1.0 * self.buypar) / (self.askpar + self.buypar)
            else:
                self.buypct = 1.0
                self.buyparpct = 1.0
        else:
            self.buypct = 0.0
            self.buyparpct = 0.0
        # sell side
        if self.sellqty > 0:
            if self.sellprc == self.bidprc:
                self.sellpct = (1.0 * self.sellqty) / (self.bidqty + self.sellqty)
                self.sellparpct = (1.0 * self.sellpar) / (self.bidpar + self.sellpar)
            else:
                self.sellpct = 1.0
                self.sellparpct = 1.0
        else:
            self.sellpct = 0.0
            self.sellparpct = 0.0

    cdef void _check_trigger(self) nogil:
        self.buy_trigger = self.buyqty >= self.tradeqty_threshold and \
                           self.buypct >= self.tradepct_threshold
        self.sell_trigger = self.sellqty >= self.tradeqty_threshold and \
                            self.sellpct >= self.tradepct_threshold
        if self.buy_trigger:
            self.sellprc = 0
        if self.sell_trigger:
            self.buyprc = 0
        self.trig_seqnum = self.last_seqnum

    cdef void _check_fire(self, int32_t qty=MINIMUM_QTY, bint place_order=True):
        cdef:
            MDOrderBook *md = &(self.trig_md)
            MDOrderBook *fire_md = &(self.fire_md)   #new for context change
            bint has_trigger = False
            uint16_t side
            int32_t price
            str context
        self._compute_accqtys(md.exch_time)
        self._check_trigger()
        if self.buy_trigger:
            self._compute_imbalance(1)
            side = 1
            price = self.buyprc
            has_trigger = True
            #karls low-T context string
            context = (
                'mkt_seqnum=%d, '
                'acc_buys={packets=%d, qty=%d, par=%d, %%qty=%0.3f, %%par=%0.3f}, '
                'qty_imbalance={%0.3f, %0.3f}, par_imbalance={%0.3f, %0.3f}'
                '' % (
                    self.mkt_seqnum,
                    self.buytrades.Size(), self.buyqty, self.buypar, self.buypct, self.buyparpct,
                    self.qty_imbalance_before, self.qty_imbalance_after,
                    self.par_imbalance_before, self.par_imbalance_after
                )
            )

            #boss [ultra boss] context str
            #grab quantities, and prices from md object
            top_ask_q = (md.asks[0].qty + self.buyqty)
            top_bid_q = md.bids[0].qty
            top_ask = md.asks[0].price
            top_bid = md.bids[0].price

            # the zeros are eop and avol specific
            context_str = (
                    #'%.3f; %s; %s; %s; %s; %s; %s; %s; %s; %s; %s; md: %d/%d %d(%d):%d(%d)x%d(%d):%d(%d)'
                "{ 'pct' : %s, 'cvol_qty' : %s, 'top_bid' : %s, 'top_ask' : %s, 'top_bid_qty' : %s, 'top_ask_qty' : %s, " \
                "'bid_par' : %s, 'ask_par' : %s, 'avol_num' : %s, 'pkt_qty' : %s, 'target_bid' : %s, 'target_ask' : %s, " \
                "'target_bid_q1' : %s, 'target_bid_p1' : %s, 'target_bid_q0' : %s, 'target_bid_p0' : %s, " \
                "'target_ask_q0' : %s, 'target_ask_p0' : %s, 'target_ask_q1' : %s, 'target_ask_p1' : %s, 'side' : 'buy'}"
                % (
                        self.buypct, self.buyqty, top_bid, top_ask, top_bid_q, top_ask_q,
                        md.bids[0].participants, md.asks[0].participants, 0, self.buyqty,
                        fire_md.bids[0].price, fire_md.asks[0].price,
                        fire_md.bids[1].qty, fire_md.bids[1].participants, fire_md.bids[0].qty, fire_md.bids[0].participants,
                        fire_md.asks[0].qty, fire_md.asks[0].participants, fire_md.asks[1].qty, fire_md.asks[1].participants
                )
           ) 
        elif self.sell_trigger:
            self._compute_imbalance(-1)
            side = 2
            price = self.sellprc
            has_trigger = True
            #karls low-T context string
            context = (
                'mkt_seqnum=%d, '
                'acc_sells={packets=%d, qty=%d, par=%d, %%qty=%0.3f, %%par=%0.3f}, '
                'qty_imbalance={%0.3f, %0.3f}, par_imbalance={%0.3f, %0.3f}'
                ''  % (
                    self.mkt_seqnum,
                    self.selltrades.Size(), self.sellqty, self.sellpar, self.sellpct, self.sellparpct,
                    self.qty_imbalance_before, self.qty_imbalance_after,
                    self.par_imbalance_before, self.par_imbalance_after
                )
            )

            #grab quantities and prices form md object
            top_ask_q = md.asks[0].qty
            top_bid_q = (md.bids[0].qty + self.sellqty)
            top_ask = md.asks[0].price
            top_bid = md.bids[0].price

            context_str = (
                #'%.3f; %s; %s; %s; %s; %s; %s; %s; %s; %s; %s; md: %d/%d %d(%d):%d(%d)x%d(%d):%d(%d)'
                "{ 'pct' : %s, 'cvol_qty' : %s, 'top_bid' : %s, 'top_ask' : %s, 'top_bid_qty' : %s, 'top_ask_qty' : %s, " \
                "'bid_par' : %s, 'ask_par' : %s, 'avol_num' : %s, 'pkt_qty' : %s, 'target_bid' : %s, 'target_ask' : %s, " \
                "'target_bid_q1' : %s, 'target_bid_p1' : %s, 'target_bid_q0' : %s, 'target_bid_p0' : %s, " \
                "'target_ask_q0' : %s, 'target_ask_p0' : %s, 'target_ask_q1' : %s, 'target_ask_p1' : %s, 'side' : 'ask'}"
                % (
                        self.sellpct, self.sellqty, top_bid, top_ask, top_bid_q, top_ask_q,
                        md.bids[0].participants, md.asks[0].participants, 0, self.sellqty,
                        fire_md.bids[0].price, fire_md.asks[0].price,
                        fire_md.bids[1].qty, fire_md.bids[1].participants, fire_md.bids[0].qty, fire_md.bids[0].participants,
                        fire_md.asks[0].qty, fire_md.asks[0].participants, fire_md.asks[1].qty, fire_md.asks[1].participants
                )
            )


        if has_trigger and (self.repeat_triggers or \
                (price != self.ref_trig_price and side != self.ref_trig_side)):
            if place_order:
                # uses low-T context
                #self.place_ghostfak_order(
                #    md.time, self.trig_seqnum, self.fire_secid, price, side, qty, context
                #)
                #uses boss context_str
                self.place_ghostfak_order(
                    md.time, self.trig_seqnum, self.fire_secid, price, side, qty, context_str
                )
            if not self.repeat_triggers:
                self.ref_trig_price = price
                self.ref_trig_side = side

    def evaluate_md(self, bint place_order=True):
        cdef:
            MDOrderBook *md = self.md_ptr
            MDOrderBook *fire_md = &(self.fire_md) #new for context str
            VolumeAccumulator *buytrades = &(self.buytrades)
            VolumeAccumulator *selltrades = &(self.selltrades)
            VolumeAccumulator *packet_cvol = &(self.packet_cvol)
            bint new_market = False
            int32_t bidprc, bidqty, askprc, askqty
            int32_t buyprice, buyvolume, sellprice, sellvolume
            uint64_t time, exch_time, channel

        # check is_open flag to see if we should be placing orders
        if not md.is_open:
            return

        # parse market data using parent function
        time = md.time
        exch_time = md.exch_time
        channel = md.channel

        #set the variable for the target contract, new for context str
        if md.secid == self.trig_secid:
            copy_mdorderbook(md, &(self.trig_md), num_levels=self.report_num_levels)
        elif md.secid == self.fire_secid:
            copy_mdorderbook(md, fire_md, num_levels=self.report_num_levels)
            return
        
        # reset accumulators
        if (not self.repeat_triggers and (self.buy_trigger or self.sell_trigger)) or self.reset_accumulators:
            self._clear_trades()
            self.reset_accumulators = False

        if channel != self.md_channel:
            return

        if not self.valid_md:
            # even though the market data is not relevant market data, we need
            # to still evaluate whether we are at the end of packet in order
            # to save what we know as aggregate buy and sell quantities.
            # Multiple secids can come in one packet, but we still want to
            # evaluate match events per packet for the secid that we care about
            if packet_cvol.Qty() > 0:
                self._check_fire(place_order)
            return

        bidprc = md.bids[0].price
        askprc = md.asks[0].price
        bidqty = md.bids[0].qty
        askqty = md.asks[0].qty

        buyprice = md.buyprice
        buyvolume = md.buyvolume
        sellprice = md.sellprice
        sellvolume = md.sellvolume

        new_market = bidprc != self.bidprc or askprc != self.askprc

        if buyvolume > 0:
            self._save_market(md)
            # evaluate buys
            if self.last_trade_side != 1 or self.last_trade_price != buyprice:
                self._check_fire(place_order)
                self._clear_trades()
                packet_cvol.AddTrade(exch_time, buyvolume)
            elif md.eop:
                packet_cvol.AddTrade(exch_time, buyvolume)
                self.last_seqnum = md.seqnum
                self._check_fire(place_order)
            else:
                packet_cvol.AddTrade(exch_time, buyvolume)
            self.last_trade_side = 1
            self.last_trade_price = buyprice
        elif sellvolume > 0:
            self._save_market(md)
            # evaluate sells
            if self.last_trade_side != 2 or self.last_trade_price != sellprice:
                self._check_fire(place_order)
                self._clear_trades()
                packet_cvol.AddTrade(exch_time, sellvolume)
            elif md.eop:
                packet_cvol.AddTrade(exch_time, sellvolume)
                self.last_seqnum = md.seqnum
                self._check_fire(place_order)
            else:
                packet_cvol.AddTrade(exch_time, sellvolume)
            self.last_trade_side = 2
            self.last_trade_price = sellprice
        elif packet_cvol.Qty() > 0:
            self._check_fire(place_order)
            self._save_market(md)
        else:
            self._save_market(md)
        self.last_seqnum = md.seqnum

        # should we reset accumulators on next market data tick?
        if new_market:
            self.ref_trig_side = 0
            self.ref_trig_price = 0
            self.mkt_seqnum += 1
        self.reset_accumulators = self.reset_accumulators or new_market

    @staticmethod
    def parse_mockorder_context(df, inplace=True):
        search_str = (
            r'mkt_seqnum=([0-9]+), acc_(buy|sell)s={packets=([0-9]+), '
            r'qty=([0-9]+), par=([0-9]+), \%qty=([\.0-9]+), \%par=([\.0-9]+)}, '
            r'qty_imbalance={([\.\-0-9]+), ([\.\-0-9]+)}, '
            r'par_imbalance={([\.\-0-9]+), ([\.\-0-9]+)}.*'
        )
        context = df['context'].str.extract(search_str, expand=True)

        convert_cols = [
            ('mkt_seqnum', int),
            ('accside', None),
            ('accpkt', int),
            ('accqty', int),
            ('accpar', int),
            ('accpct', float),
            ('accparpct', float),
            ('qty_imb_prev', float),
            ('qty_imb_now', float),
            ('par_imb_prev', float),
            ('par_imb_now', float)
        ]
        context.columns = map(lambda x: x[0], convert_cols)

        if inplace:
            for col, dtype in convert_cols:
                if dtype is None:
                    df[col] = context[col]
                else:
                    df[col] = context[col].astype(dtype)
        else:
            for col, dtype in convert_cols:
                if dtype is not None:
                    context[col] = context[col].astype(dtype)
        return context




def parse_cvol_name(df):
    name = df['strategy'].str.extract('CVOLSlugger{name=(.+) (.+); (.+)}', expand=True)
    name.columns = ['shortname', 'kind', 'acc_type']
    for col in name:
        df[col] = name[col]



    

cdef class AVOLSluggerEOnly(StrategyInterface):
    """
    Overview: Strat inherits from StrategyInterface(MDSource).
        StrategyInterface inits an order through the place_ghostfak_order method
    """
    cdef:
        char *bidqty_buf
        char *askqty_buf
        MagicQuantities qtys
        MDOrderBook fire_md
        # readable by interpreter
        readonly:
            bint repeat_triggers, avol_last_pkt, avol_pkt
            uint64_t trig_secid, fire_secid, last_exch_time, last_pcap_time, report_num_levels
            int32_t tradeqty_threshold, avol_msg_count, buyvolume, sellvolume
            uint16_t prev_msg_idx, packet_len
            #map_[uint16_t, uint16_t] packet_len_triggers
            dict packet_len_triggers
            str name, context
            # attributes for new md parsing
            double avol_qty
            char msg_tag
            uint64_t top_bid, top_ask, avol_seqnum, top_bid_q, top_ask_q, buyprice, sellprice, prev_pcap_time, bid_par, ask_par
            bint avol

    def __init__(self, uint64_t trig_secid, uint64_t fire_secid, int32_t tradeqty_threshold,
                 list packet_len_triggers, bint repeat_triggers=False, context='old', name="null"):
        cdef uint16_t packet_len, msg_idx
        self.initialize([trig_secid, fire_secid])
        self.trig_secid = trig_secid
        self.fire_secid = fire_secid
        self.tradeqty_threshold = tradeqty_threshold
        self.packet_len_triggers = {}
        for packet_len, msg_idx in packet_len_triggers:
            self.packet_len_triggers[packet_len] = msg_idx
        self.name = str(name)
        self.repeat_triggers = repeat_triggers
        self.last_exch_time = 0
        self.last_pcap_time = 0
        self.avol_msg_count = 0
        self.bidqty_buf = <char*>malloc(8 * sizeof(char))
        self.askqty_buf = <char*>malloc(8 * sizeof(char))
        self.report_num_levels = 4
        self.avol_last_pkt = False
        self.avol_pkt = False
        self.avol_qty = 0
        self.avol_seqnum = 0
        self.prev_pcap_time = 0
        self.prev_msg_idx = 0
        self.buyvolume = 0
        self.sellvolume = 0
        self.context = str(context)
        self.packet_len = 0

    def __repr__(self):
        return 'AVOLSluggerEOnly: some stuff'

    def gc(self):
        """gc()
Free malloc'ed pointers (self.bidqty_buf, self.askqty_buf). This is up to the
user to call (backtester scripts should call this method automatically after
processing all market data).
        """
        print 'freeing self.bidqty_buf and self.askqty_buf'
        free(self.bidqty_buf)
        free(self.askqty_buf)

    #cdef bint _is_trigger(self, uint16_t packet_len, uint16_t msg_idx, char msg_tag) nogil:
    cdef bint _is_trigger(self, uint16_t packet_len, uint16_t msg_idx, char msg_tag):
        cdef:
            #bint exists = self.packet_len_triggers.find(packet_len) != self.packet_len_triggers.end()
            uint16_t mapped_msg_idx

        

        if packet_len in self.packet_len_triggers.keys():
            exists = True
        else:
            exists = False
        if not exists:
            return False

        print 'packet_len_triggers'
        print self.packet_len_triggers[packet_len]
        print packet_len, msg_idx, msg_tag

        #mapped_msg_idx = self.packet_len_triggers[packet_len]
        #return msg_idx == mapped_msg_idx and msg_tag == 'E'
        return True

    def evaluate_md_oldparser(self):
        """evaluate_md()

Evaluate market data from self.md_ptr (pointing to MDOrderBook object from
external MDSource) to determine whether we should fire on an assumed volume
event. Returns nothing, but it prepares orders (appends MockOrder to
self.orders using self.place_ghostfak_order function inherited by StrategyInterface).

        """
        cdef:
            MDOrderBook *md = self.md_ptr
            MDOrderBook *fire_md = &(self.fire_md)
            char msg_tag
            uint16_t packet_len, msg_idx
            uint64_t exch_time
            uint64_t pcap_time
            MagicQuantities *qtys = &(self.qtys)   #<---- this no longer works
            double buypct, sellpct
            uint64_t client_orderid = 0

        # absolutely necessary step: do we care about this market data?
        # self.valid_md == False if it isn't the market data md_ptr contains
        # is not our trig_secid
        if not self.valid_md:
            return

        if md.secid == self.fire_secid:
            copy_mdorderbook(md, fire_md, num_levels=self.report_num_levels)
            return
        packet_len = md.packet_len
        msg_idx = md.msg_idx
        msg_tag = md.topic[TOPIC_TYPE_MSG]
        exch_time = md.exch_time
        pcap_time = md.time
        bail = False 

        # Note about summing volume:
        # this will sum volume across trade events with one caveat
        # if there is a packet with NK and TP evnets to form a "full packet"
        # it will use the avol packet logic and use the top 26 par qty.
        self.buyvolume += md.buyvolume
        self.sellvolume += md.sellvolume

        # is this packet_len a triggerable packet_len? if so, let's fire!
        # we want to check packet_len, msg_idx == 1, and msg_tag == 'E'
        if not self._is_trigger(packet_len, msg_idx, msg_tag):
            # if its the first message, its not an avol so mark it so
            if msg_idx == 1:
                self.avol_pkt = False
                #this is an attempt to catch cases where the EOP packet of an avol is
                # not parsed for some reason (like its a security we dont care about)
                if self.last_exch_time != exch_time:
                    self.avol_last_pkt = False
            
            # if the previous packet was an avol and this pkt isnt and its the last
            # msg in the packet, call this a trigger because its the last packet in the 
            # trade event
            if self.avol_last_pkt and md.eop and not self.avol_pkt:
                self.avol_last_pkt = False
            else:
                return
        else:
            # if this is the start of a trade event (ie first avol) then start the qty count
            # for sanity the code should probalby come first
            if not self.avol_last_pkt:
                self.buyvolume = md.buyvolume
                self.sellvolume = md.sellvolume
            self.avol_last_pkt = True
            self.avol_pkt = True  

        self.prev_msg_idx = msg_idx

        # parse topic in a gruesome way for the first 26 participant summed qty
        parse_topic_for_magic_quantities(&(md.topic), self.bidqty_buf, self.askqty_buf, qtys)

        #juggling volumes:
        #if its and avol and prev packet was an avol theres a chance that there will be
        # no followup non-avol packet to nicely sum the qty, so we need to add the avol qty
        # and remove the row qty.... we need to back this out after we print it b/c there 
        #still might be a non-avol packet after this.  This should be the correct qty 
        # UP TO THIS POINT.
        if self.avol_pkt and self.avol_last_pkt:
            self.buyvolume = self.buyvolume - md.buyvolume + qtys.ask
            self.sellvolume = self.sellvolume - md.sellvolume + qtys.bid
                                                                                                   
        #check to make sure this packet time doesnt == last packet time
        if pcap_time != self.last_pcap_time:
            #mark sequential avol messages with number
            #in order to filter these out in the output use drop_duplicates on the 'trade_time' 
            #  column in the df from regex_to_df function
            if exch_time == self.last_exch_time:
                self.avol_msg_count += 1
            else:
                self.avol_msg_count = 0
            if md.buyvolume > 0:
                #grab quantities, and prices from md object
                top_ask_q = (md.asks[0].qty + md.buyvolume)
                top_bid_q = md.bids[0].qty
                top_ask = md.asks[0].price
                top_bid = md.bids[0].price
                #msg_type = str(bytearray(md.topic).decode()[:3])  #unpacking the hipster datatype "bytearray" into a string
                #calculate avol % (qtys object uses the Magic)
                buypct = <double>qtys.ask / top_ask_q
                print 'BUY!', md.channel, md.packet_len, md.seqnum, md.buyprice, md.buyvolume, qtys.ask, buypct, md.bid_avol_qty, md.ask_avol_qty
                #context_str = '%.3f; %s' % (buypct, qtys.ask)
                #add to context string: avol %, avol qty and top of book info, E msg index
                context_str = (
                    '%.3f; %s; %s; %s; %s; %s; %s; %s; %s; %s; %s; md: %d/%d %d(%d):%d(%d)x%d(%d):%d(%d); %s; %s'
                ) % (
                        buypct, qtys.ask, top_bid, top_ask, top_bid_q, top_ask_q,
                        md.bids[0].participants, md.asks[0].participants, msg_idx, self.avol_msg_count,
                        self.buyvolume,
                        fire_md.bids[0].price, fire_md.asks[0].price,
                        fire_md.bids[1].qty, fire_md.bids[1].participants, fire_md.bids[0].qty, fire_md.bids[0].participants,
                        fire_md.asks[0].qty, fire_md.asks[0].participants, fire_md.asks[1].qty, fire_md.asks[1].participants,
                        packet_len, chr(md.topic[0])
                )
                client_orderid = self.place_ghostfak_order(md.time, md.seqnum, self.fire_secid, md.buyprice, 1, 10, context_str)
            else:
                #grab quantities and prices form md object
                top_ask_q = md.asks[0].qty
                top_bid_q = (md.bids[0].qty + md.sellvolume)
                top_ask = md.asks[0].price
                top_bid = md.bids[0].price
                #msg_type = str(bytearray(md.topic).decode()[:3])  #unpacking the hipster datatype "bytearray" into a string
                #cacualate avol %
                sellpct = <double>qtys.bid / top_bid_q
                print 'SELL!', md.channel, md.packet_len, md.seqnum, md.sellprice, md.sellvolume, qtys.bid, sellpct, md.bid_avol_qty, md.ask_avol_qty
                context_str = (
                    '%.3f; %s; %s; %s; %s; %s; %s; %s; %s; %s; %s; md: %d/%d %d(%d):%d(%d)x%d(%d):%d(%d); %s; %s'
                ) % (
                        sellpct, qtys.bid, top_bid, top_ask, top_bid_q, top_ask_q,
                        md.bids[0].participants, md.asks[0].participants, msg_idx, self.avol_msg_count,
                        self.sellvolume,
                        fire_md.bids[0].price, fire_md.asks[0].price,
                        fire_md.bids[1].qty, fire_md.bids[1].participants, fire_md.bids[0].qty, fire_md.bids[0].participants,
                        fire_md.asks[0].qty, fire_md.asks[0].participants, fire_md.asks[1].qty, fire_md.asks[1].participants,
                        packet_len, chr(md.topic[0])
                     )
                client_orderid = self.place_ghostfak_order(md.time, md.seqnum, self.fire_secid, md.sellprice, 2, 10, context_str)
            self.last_exch_time = exch_time
            self.last_pcap_time = pcap_time

            #backing out the volume juggling from lines 782-785
            if self.avol_pkt and self.avol_last_pkt:
                self.buyvolume = self.buyvolume + md.buyvolume - qtys.ask
                self.sellvolume = self.sellvolume + md.sellvolume - qtys.bid

    def f1_calc(self, bid, ask, bidq1, bidq0, askq0, askq1, trade_qty, side):
        #behold the imbalance feature
        cdef:
            double arb_bid = 0, arb_ask = 0, even_bid = 0, even_ask = 0, n, d

        if (bid / 10000.0) % 10 != 0:
            arb_bid = bidq0
            even_bid = bidq1
            arb_ask = 0
            even_ask = askq0
        elif (ask / 10000.0) % 10 != 0:
            arb_bid = 0
            even_bid = bidq0
            arb_ask = askq0
            even_ask = askq1
            
        if side == 1 and arb_bid > 0 or side == 2 and arb_ask > 0:
            Q = 0
        else:
            Q = min(trade_qty * 10, max(arb_bid, arb_ask))

        n = even_bid + arb_bid - even_ask - arb_ask + (3-2*side) * Q
        d = even_bid + arb_bid + even_ask + arb_ask - Q

        if double_eq(d, 0):
            res = 0
        else:
            res = (3-2*side) * <double>n / d
        return res

    def f2_calc(self, trig_bidq, trig_askq, bid, ask, bidq1, bidq0, askq0, askq1, trade_qty, side):
        #behold the unhedged qty feature
        cdef:
            double arb_bid = 0, arb_ask = 0, even_bid = 0, even_ask = 0, n, d

        #get your mnk variables set
        if (bid / 10000.0) % 10 != 0:
            arb_bid = bidq0
            even_bid = bidq1
            arb_ask = 0
            even_ask = askq0
        elif (ask / 10000.0) % 10 != 0:
            arb_bid = 0
            even_bid = bidq0
            arb_ask = askq0
            even_ask = askq1
        
        if side == 1:
            target = arb_ask + even_ask
            #when do you send the trigger ask qty?  before or after trade_qty removed?
            trigger_remaining = trig_askq - trade_qty
            if arb_ask > 0:
                arb_qty = arb_ask
        else:
            target = even_bid + arb_bid
            trigger_remaining = trig_bidq - trade_qty
            if arb_bid > 0:
                arb_qty = arb_bid
        n = trade_qty * 10 - arb_qty
        d = target + trigger_remaining * 10 - min(trade_qty * 10, arb_qty)
        
        return <double>n / d

    def calc_probability(self, f1, f2):
        cdef:
            double c1 = 1.17, c2 = 1.061
            double p, probability
            bint res

        p = c1 * f1 + c2 * f2
        probability = 1.0 / (1 + exp(-p))
        #if double_gte(probability, 0.64):
        #    res = True
        #else:
        #    res = False

        return probability
      
    def vector_len(self, f1, f2):
        cdef:
            double c1 = 1.17, c2 = 1.061, length
            ndarray [double, ndim=2] rej, proj, proj_matrix, v = empty((2,1)), u = empty((2,1)), coef = empty((2,1))


        v = array([[f1], [f2]])  #the form [[],[]] is so the shape is 2,1
        coef = array([[-c1],[c2]]) 
        u = coef / norm(coef)
        proj_matrix = matmul(u, u.T)
        proj = matmul(proj_matrix, v)
        rej = v - proj
        length = norm(rej)

        return length

    def evaluate_md(self):
        cdef:
            MDOrderBook *md = self.md_ptr
            MDOrderBook *fire_md = &(self.fire_md)
            char msg_tag
            uint16_t packet_len, msg_idx = 0
            uint64_t exch_time, pcap_time
            double buypct, sellpct
            uint64_t client_orderid = 0
            bint reset = False 


        #neceesary step im told
        # self.valid_md == False if it isnt the market data md_ptr contains si not our trig sec_id
        if not self.valid_md:
            return

        if md.secid == self.fire_secid:
            copy_mdorderbook(md, fire_md, num_levels=self.report_num_levels)
            return

        packet_len = md.packet_len
        msg_tag = md.topic[0]
        exch_time = md.exch_time
        pcap_time = md.time
        time_delta = pcap_time - exch_time


        if pcap_time != self.prev_pcap_time:
            #new packet: reset prev_pkt_num and if its a D or E and pkt_len is correct, start avol process
            # T msgs dont show up so you dont need to check for them (but they effect the pkt size)
            self.prev_pcap_time = pcap_time
            
            #check if exch time is the same as last to see if these are all the same event
            if exch_time == self.last_exch_time and self.avol_last_pkt:
                self.avol_msg_count += 1
            else:
                self.avol_msg_count = 0
                self.last_exch_time = exch_time

            # only looking for D or E msgs bc bruce doest pass up T messages
            if msg_tag in ['D', 'E'] and packet_len in self.packet_len_triggers.keys():
                self.avol_seqnum = md.seqnum
                self.avol = True
                self.avol_qty = md.buyvolume + md.sellvolume #if D, then both will be zero
                self.packet_len = packet_len
                # keep summing across avol packets, or reset
                if not self.avol_last_pkt:
                    self.buyvolume = md.buyvolume
                    self.sellvolume = md.sellvolume
                else:
                    self.buyvolume += md.buyvolume
                    self.sellvolume += md.sellvolume
                #set your variables from the first message in the packet    
                self.top_ask = md.asks[0].price
                self.top_bid = md.bids[0].price
                self.msg_tag = msg_tag
                if md.buyvolume > 0:
                    self.top_ask_q = md.asks[0].qty + md.buyvolume
                    self.top_bid_q = md.bids[0].qty
                    self.buyprice = md.buyprice
                    self.ask_par = md.asks[0].participants + 1 #add the traded with par back in
                    self.bid_par = md.bids[0].participants 
                elif md.sellvolume > 0:
                    self.top_ask_q = md.asks[0].qty
                    self.top_bid_q = md.bids[0].qty + md.sellvolume
                    self.sellprice = md.sellprice
                    self.ask_par = md.asks[0].participants 
                    self.bid_par = md.bids[0].participants + 1 #add the traded with par back in
            else:
                #reset your vars if its not an avol packet
                self.avol = False
                self.buyvolume = 0
                self.sellvolume = 0
        elif msg_tag == 'E' and self.avol:
            # if you only get subsequent E messages after setting avol=True, then youre good
            self.buyvolume += md.buyvolume
            self.sellvolume += md.sellvolume
            if not md.eop:
                self.avol_qty += md.buyvolume + md.sellvolume #one of these will be zero
            elif md.eop:
                self.avol_qty += 1 #we dont "know" last qty while predicting avol qty
                self.avol_last_pkt = True
        elif self.avol:
            # if you get any other type of message after getting a D or E, its not an avol so reset vars
            self.avol = False
            self.avol_qty = 0
            self.avol_last_pkt = False
            self.buyvolume = 0
            self.sellvolume = 0

        #trigger check
        if self.avol and md.eop:
            #trigger!  yay, made it through the whole pkt with only E msgs after first msg!
            if md.buyvolume > 0:
                #buy trigger
                buypct = <double>self.avol_qty / self.top_ask_q
                f1 = self.f1_calc(
                    fire_md.bids[0].price, fire_md.asks[0].price, fire_md.bids[1].qty, fire_md.bids[0].qty,
                    fire_md.asks[0].qty, fire_md.asks[1].qty, self.avol_qty, 1
                    )
                f2 = self.f2_calc(
                    self.top_bid_q, self.top_ask_q,
                    fire_md.bids[0].price, fire_md.asks[0].price, fire_md.bids[1].qty, fire_md.bids[0].qty,
                    fire_md.asks[0].qty, fire_md.asks[1].qty, self.avol_qty, 1
                    )
                proba = self.calc_probability(f1, f2)
                v_len = self.vector_len(f1, f2)
                print 'BUY!', md.channel, md.packet_len, md.seqnum, md.buyprice, md.buyvolume, buypct, self.avol_qty
                print 'f1: %s, f2: %s' % (f1, f2)
                if self.context == 'old':
                    context_str = (
                        #'%.3f; %s; %s; %s; %s; %s; %s; %s; %s; %s; %s; md: %d/%d %d(%d):%d(%d)x%d(%d):%d(%d); %s; %s'
                        "{ 'pct' : %s, 'avol_qty' : %s, 'top_bid' : %s, 'top_ask' : %s, 'top_bid_qty' : %s, 'top_ask_qty' : %s, " \
                        "'bid_par' : %s, 'ask_par' : %s, 'avol_num' : %s, 'pkt_qty' : %s, 'target_bid' : %s, 'target_ask' : %s, " \
                        "'target_bid_q1' : %s, 'target_bid_p1' : %s, 'target_bid_q0' : %s, 'target_bid_p0' : %s, " \
                        "'target_ask_q0' : %s, 'target_ask_p0' : %s, 'target_ask_q1' : %s, 'target_ask_p1' : %s, " \
                        "'pkt_len' : %s, 'tag' : '%s', 'proba' : %s, 'vlen' : %s, 'side' : 'buy', 'tdiff' : %s}"
                    ) % (
                        buypct, self.avol_qty, self.top_bid, self.top_ask, self.top_bid_q, self.top_ask_q,
                        self.bid_par, self.ask_par, self.avol_msg_count, self.buyvolume,
                        fire_md.bids[0].price, fire_md.asks[0].price,
                        fire_md.bids[1].qty, fire_md.bids[1].participants, fire_md.bids[0].qty, fire_md.bids[0].participants,
                        fire_md.asks[0].qty, fire_md.asks[0].participants, fire_md.asks[1].qty, fire_md.asks[1].participants,
                        self.packet_len, chr(self.msg_tag), proba, v_len, time_delta
                    )
                else:
                    context_str = (
                        "{'proba' : %s, 'vlen' : %s, 'avol_num' : %s, 'pkt_len' : %s}"
                    ) % (proba, v_len, self.avol_msg_count, self.packet_len)
                #to change your order data, change this line
                client_orderid = self.place_ghostfak_order(md.time, self.avol_seqnum, self.fire_secid, self.buyprice, 1, 10, context_str)
            else:
                #sell trigger
                sellpct = <double>self.avol_qty / self.top_bid_q
                f1 = self.f1_calc(
                    fire_md.bids[0].price, fire_md.asks[0].price, fire_md.bids[1].qty, fire_md.bids[0].qty,
                    fire_md.asks[0].qty, fire_md.asks[1].qty, self.avol_qty, 2
                    )
                f2 = self.f2_calc(
                    self.top_bid_q, self.top_ask_q,
                    fire_md.bids[0].price, fire_md.asks[0].price, fire_md.bids[1].qty, fire_md.bids[0].qty,
                    fire_md.asks[0].qty, fire_md.asks[1].qty, self.avol_qty, 2
                    )
                proba = self.calc_probability(f1, f2)
                v_len = self.vector_len(f1, f2)
                print 'SELL!', md.channel, md.packet_len, md.seqnum, md.sellprice, md.sellvolume, sellpct, self.avol_qty
                print 'f1: %s, f2: %s' % (f1, f2)
                if self.context == 'old':
                    context_str = (
                        #'%.3f; %s; %s; %s; %s; %s; %s; %s; %s; %s; %s; md: %d/%d %d(%d):%d(%d)x%d(%d):%d(%d); %s; %s'
                        "{ 'pct' : %s, 'avol_qty' : %s, 'top_bid' : %s, 'top_ask' : %s, 'top_bid_qty' : %s, 'top_ask_qty' : %s, " \
                        "'bid_par' : %s, 'ask_par' : %s, 'avol_num' : %s, 'pkt_qty' : %s, 'target_bid' : %s, 'target_ask' : %s, " \
                        "'target_bid_q1' : %s, 'target_bid_p1' : %s, 'target_bid_q0' : %s, 'target_bid_p0' : %s, " \
                        "'target_ask_q0' : %s, 'target_ask_p0' : %s, 'target_ask_q1' : %s, 'target_ask_p1' : %s, " \
                        "'pkt_len' : %s, 'tag' : '%s', 'proba' : %s, 'vlen' : %s, 'side' : 'ask', 'tdiff' : %s}"
                    ) % (
                        sellpct, self.avol_qty, self.top_bid, self.top_ask, self.top_bid_q, self.top_ask_q,
                        self.bid_par, self.ask_par, self.avol_msg_count, self.sellvolume,
                        fire_md.bids[0].price, fire_md.asks[0].price,
                        fire_md.bids[1].qty, fire_md.bids[1].participants, fire_md.bids[0].qty, fire_md.bids[0].participants,
                        fire_md.asks[0].qty, fire_md.asks[0].participants, fire_md.asks[1].qty, fire_md.asks[1].participants,
                        packet_len, chr(self.msg_tag), proba, v_len, time_delta
                    )
                else:
                    context_str = (
                        "{'proba' : %s, 'vlen' : %s, 'avol_num' : %s, 'pkt_len' : %s}"
                    ) % (proba, v_len, self.avol_msg_count, self.packet_len)
                #to change your order data, change this line
                client_orderid = self.place_ghostfak_order(md.time, self.avol_seqnum, self.fire_secid, self.sellprice, 2, 10, context_str)

cdef class TimeDelta(StrategyInterface):
    cdef:
        char *bidqty_buf
        char *askqty_buf
        MDOrderBook fire_md
        readonly:
            uint64_t trig_secid, fire_secid, acc_volume, acc_pars, seqnum
            uint64_t report_num_levels, buyprice, sellprice, packet_num, packet_len, tick_num, prev_exch_time, packet_diff
            uint64_t me_diff, target_e_time, any_e_time, target_e_diff, any_e_diff
            int64_t t_diff, exch_time, pcap_time
            bint event_flag
            str side
    
    def __init__(self, uint64_t trig_secid, uint64_t fire_secid,  name='null'):
        self.bidqty_buf = <char*>malloc(8 * sizeof(char))
        self.askqty_buf = <char*>malloc(8 * sizeof(char))

        self.initialize([trig_secid, fire_secid])
        self.trig_secid = trig_secid
        self.fire_secid = fire_secid
        
        self.exch_time = 0
        self.pcap_time = 0
        self.prev_exch_time = 0
        self.target_e_time = 0
        self.any_e_time = 0
        self.packet_diff = 0
        self.target_e_diff = 0
        self.any_e_diff = 0
        self.me_diff = 0
        self.event_flag = False
        self.acc_volume = 0
        self.acc_pars = 0
        self.seqnum = 0
        self.t_diff = 0
        self.buyprice = 0
        self.sellprice = 0
        self.packet_num = 0
        self.packet_len = 0
        self.tick_num = 0

        self.side = ''

        self.report_num_levels = 4

    def __repr__(self):
        return 'TimeDelta: some stuff'

    def gc(self):
        """gc()
        Free malloc'ed pointers (self.bidqty_buf, self.askqty_buf). This is up to the
        user to call (backtester scripts should call this method automatically after
        processing all market data).
        """
        print 'freeing self.bidqty_buf and self.askqty_buf'
        free(self.bidqty_buf)
        free(self.askqty_buf)

    def evaluate_md(self):
        cdef:
            MDOrderBook *md = self.md_ptr
            MDOrderBook *fire_md = &(self.fire_md)

        # bail if md_ptr is not a secid we are interested in 
        #if not self.valid_md:
        #    return
        
        if md.secid == self.fire_secid:
            copy_mdorderbook(md, fire_md, num_levels=self.report_num_levels)
            if md.topic[0] in ['E'] and not self.event_flag:
                self.any_e_time = md.time
            return

        if self.trig_secid == md.secid and md.topic[0] in ['E', 'D'] and (not self.event_flag
                or (self.event_flag and self.exch_time != md.exch_time)):
            # beginning of new trade event AND end of old event, so clear current event
            if self.event_flag and self.exch_time != md.exch_time:
                self.event_flag = False
                self.fire_order()
            # beginning of trade event (only if target_secid is first trade msgs)
            self.packet_diff = md.time - self.pcap_time
            self.me_diff = md.exch_time - self.exch_time
            self.target_e_diff = md.exch_time - self.target_e_time
            self.any_e_diff = md.time - self.any_e_time
            if md.seqnum == 1214808:
                print 'BITCH', self.any_e_time
            #init trade variables
            self.t_diff = md.time - md.exch_time
            self.acc_volume = md.buyvolume + md.sellvolume
            self.acc_pars = 1
            self.seqnum = md.seqnum
            # only set these vars if its a E msg (no Ds)
            if md.topic[0] == 'E':
                self.event_flag = True
                self.exch_time = md.exch_time
                self.pcap_time = md.time
                self.target_e_time = md.exch_time
                self.any_e_time = md.time
            self.tick_num = 1
            self.packet_num = 1
            self.packet_len = md.packet_len
            #set side and trade price
            if md.buyvolume != 0:
                self.side = 'buy'
                self.buyprice = md.asks[0].price
            else:
                self.side = 'sell'
                self.sellprice = md.bids[0].price
        elif self.trig_secid == md.secid and md.topic[0] in ['E'] and self.event_flag \
                and self.exch_time == md.exch_time:
            # continuing trade event
            self.acc_volume += md.buyvolume + md.sellvolume
            self.acc_pars += 1
            #count pkt num
            if self.pcap_time != md.time:
                self.packet_num += 1
                self.pcap_time = md.time
            #count number of prices traded
            if self.side == 'buy' and md.buyprice != self.buyprice:
                self.tick_num += 1
                self.buyprice = md.asks[0].price
            elif self.side == 'sell' and md.sellprice != self.sellprice:
                if self.seqnum == 80389:
                    print md.bids[0].price, self.sellprice, md.seqnum
                self.tick_num += 1
                self.sellprice = md.bids[0].price
        elif self.event_flag:
            #event is over
            self.event_flag = False
            self.fire_order()
        elif self.pcap_time != md.time:
            if md.topic[0] in ['E'] and self.trig_secid == md.secid:
                self.exch_time = 9999999999
                self.pcap_time = 9999999999
                self.target_e_time = 9999999999
                #self.exch_time = md.exch_time
                #self.pcap_time = md.time
                #self.target_e_time = md.exch_time
            elif md.topic[0] in ['E']:
                self.any_e_time = md.time
                

    def fire_order(self):
        cdef:
            int side

        context = "{'side' : '%s', 'volume' : %s, 'tdiff' : %s, 'pars' : %s, 'seqnum' : %s, " \
            "'pkts' : %s, 'pkt_size' : %s, 'ticks' : %s, 'pkt_diff' : %s, 'me_diff' : %s}" % (
            self.side, self.acc_volume, self.t_diff, self.acc_pars, self.seqnum, self.packet_num,
            self.packet_len, self.tick_num, self.any_e_diff, self.target_e_diff #self.packet_diff, self.me_diff
            )
        if self.side == 'buy':
            side = 1
            price = self.sellprice
        else:
            side = 2
            price = self.buyprice
        print 'FUCK', self.seqnum, self.any_e_time, self.target_e_time
        client_orderid = self.place_ghostfak_order(self.exch_time, self.seqnum, self.fire_secid, self.sellprice, side, 1, context)

            
            

cdef class AVOLSweeps(AVOLSluggerEOnly):
    cdef:
        bint sweep
        uint64_t pkt_qty
    def __init__(self, uint64_t trig_secid, uint64_t fire_secid, int32_t tradeqty_threshold,
                 list packet_len_triggers, bint repeat_triggers=False, name="null", context='old'):
        AVOLSluggerEOnly.__init__( self, trig_secid, fire_secid, tradeqty_threshold,
                 packet_len_triggers, repeat_triggers, name, context)
        self.sweep = False
        self.pkt_qty = 0

    def evaluate_md(self):
        cdef:
            MDOrderBook *md = self.md_ptr
            MDOrderBook *fire_md = &(self.fire_md)
            char msg_tag
            uint16_t packet_len, msg_idx = 0
            uint64_t exch_time, pcap_time
            double buypct, sellpct
            uint64_t client_orderid = 0
            bint reset = False


        #neceesary step im told
        # self.valid_md == False if it isnt the market data md_ptr contains si not our trig sec_id
        if not self.valid_md:
            return

        if md.secid == self.fire_secid:
            copy_mdorderbook(md, fire_md, num_levels=self.report_num_levels)
            return

        packet_len = md.packet_len
        msg_tag = md.topic[0]
        exch_time = md.exch_time
        pcap_time = md.time


        if pcap_time != self.prev_pcap_time:
            #new packet: reset prev_pkt_num and if its a D or E and pkt_len is correct, start avol process
            # T msgs dont show up so you dont need to check for them (but they effect the pkt size)
            self.prev_pcap_time = pcap_time
            
            #check if exch time is the same as last to see if these are all the same event
            if exch_time == self.last_exch_time and self.avol_last_pkt:
                self.avol_msg_count += 1
            else:
                self.avol_msg_count = 0
                self.last_exch_time = exch_time

            # only looking for D or E msgs bc bruce doest pass up T messages
            if msg_tag in ['E'] and packet_len in self.packet_len_triggers.keys():
                self.avol_seqnum = md.seqnum
                self.avol = True
                self.sweep = False
                self.pkt_qty = md.buyvolume + md.sellvolume
                # keep summing across avol packets, or reset
                if not self.avol_last_pkt:
                    self.buyvolume = md.buyvolume
                    self.sellvolume = md.sellvolume
                else:
                    self.buyvolume += md.buyvolume
                    self.sellvolume += md.sellvolume
                #set your variables from the first message in the packet    
                self.msg_tag = msg_tag
                if md.buyvolume > 0:
                    self.buyprice = md.buyprice
                elif md.sellvolume > 0:
                    self.sellprice = md.sellprice

                if msg_tag == 'D':
                    print self.buyprice, self.sellprice, self.buyvolume, self.sellvolume
            else:
                #reset your vars if its not an avol packet
                self.sweep = False
                self.avol = False
                self.buyvolume = 0
                self.sellvolume = 0
        elif msg_tag == 'E' and self.avol:
            # if you only get subsequent E messages after setting avol=True, then youre good
            self.buyvolume += md.buyvolume
            self.sellvolume += md.sellvolume
            self.pkt_qty += md.buyvolume + md.sellvolume
            if md.buyvolume > 0 and self.buyprice != md.buyprice:
                self.sweep = True
                self.top_ask = md.asks[0].price
                self.top_bid = md.bids[0].price
                self.top_ask_q = md.asks[0].qty + md.buyvolume
                self.top_bid_q = md.bids[0].qty
                self.buyprice = md.buyprice
                self.ask_par = md.asks[0].participants + 1 #add the traded with par back in
                self.bid_par = md.bids[0].participants 
                self.avol_qty = md.buyvolume + md.sellvolume #one of these will be zero
            elif md.sellvolume > 0 and self.sellprice != md.sellprice:
                self.sweep = True
                self.top_ask = md.asks[0].price
                self.top_bid = md.bids[0].price
                self.top_ask_q = md.asks[0].qty
                self.top_bid_q = md.bids[0].qty + md.sellvolume
                self.sellprice = md.sellprice
                self.ask_par = md.asks[0].participants 
                self.bid_par = md.bids[0].participants + 1 #add the traded with par back in
                self.avol_qty = md.buyvolume + md.sellvolume #one of these will be zero
            #elif self.sweep and not md.eop:
            elif not md.eop:
                self.avol_qty += md.buyvolume + md.sellvolume #one of these will be zero
            #elif self.sweep and md.eop:
            elif md.eop:
                self.avol_qty += 1 #we dont "know" last qty while predicting avol qty
                self.avol_last_pkt = True
                self.buyvolume -= md.buyvolume -1 #-1 bc you only "know" the 1 lot traded
                self.sellvolume -= md.sellvolume -1
        elif self.avol:
            # if you get any other type of message after getting a D or E, its not an avol so reset vars
            self.sweep = False
            self.avol = False
            self.avol_qty = 0
            self.avol_last_pkt = False
            self.buyvolume = 0
            self.sellvolume = 0
            self.pkt_qty = 0

        #trigger check
        if self.avol and md.eop and self.sweep:
            #trigger!  yay, made it through the whole pkt with only E msgs after first msg!
            if md.buyvolume > 0:
                #buy trigger
                buypct = <double>self.avol_qty / self.top_ask_q
                print 'BUY!', md.channel, md.packet_len, md.seqnum, md.buyprice, md.buyvolume, buypct, self.avol_qty
                #you want the next level pct, qty etc
                context_str = (
                "{'pct':%.3f, 'qty':%s, 'event_qty':%s, 'pkt_qty' : %s, 'bid':%s, 'ask':%s, 'seq':%s, 'bidq':%s, 'askq':%s, 'pkt_num':%s, 'pkt_len':%s, " + \
                "'t_bid_px':%d, 't_ask_px':%d, 'tbid3':%s, 'tbid2':%s, 'tbid1':%s, 'tbid0':%s, 'task0':%s, 'task1':%s, 'task2':%s, 'task3':%s, 'side':'buy'}"
                ) % (
                    buypct, self.avol_qty, self.buyvolume, self.pkt_qty,
                    self.top_bid, self.top_ask, self.avol_seqnum, self.top_bid_q, self.top_ask_q,
                    self.avol_msg_count, packet_len,
                    fire_md.bids[0].price, fire_md.asks[0].price,
                    fire_md.bids[3].qty, fire_md.bids[2].qty, fire_md.bids[1].qty, fire_md.bids[0].qty, 
                    fire_md.asks[0].qty, fire_md.asks[1].qty, fire_md.asks[2].qty, fire_md.asks[3].qty, 
                )

                #to change your order data, change this line
                client_orderid = self.place_ghostfak_order(md.time, self.avol_seqnum, self.fire_secid, self.buyprice, 1, 1, context_str)
            else:
                #sell trigger
                sellpct = <double>self.avol_qty / self.top_bid_q
                print 'SELL!', md.channel, md.packet_len, md.seqnum, md.sellprice, md.sellvolume, sellpct, self.avol_qty
                context_str = (
                "{'pct':%.3f, 'qty':%s, 'event_qty': %s, 'pkt_qty' : %s, 'bid':%s, 'ask':%s, 'seq':%s, 'bidq':%s, 'askq':%s, 'pkt_num':%s, 'pkt_len':%s, " + \
                "'t_bid_px':%d, 't_ask_px':%d, 'tbid3':%s, 'tbid2':%s, 'tbid1':%s, 'tbid0':%s, 'task0':%s, 'task1':%s, 'task2':%s, 'task3':%s, 'side':'sell'}"
                ) % (
                    sellpct, self.avol_qty, self.sellvolume, self.pkt_qty,
                    self.top_bid, self.top_ask, self.avol_seqnum, self.top_bid_q, self.top_ask_q,
                    self.avol_msg_count, packet_len,
                    fire_md.bids[0].price, fire_md.asks[0].price,
                    fire_md.bids[3].qty, fire_md.bids[2].qty, fire_md.bids[1].qty, fire_md.bids[0].qty, 
                    fire_md.asks[0].qty, fire_md.asks[1].qty, fire_md.asks[2].qty, fire_md.asks[3].qty, 
                    )
                #to change your order data, change this line
                client_orderid = self.place_ghostfak_order(md.time, self.avol_seqnum, self.fire_secid, self.sellprice, 2, 1, context_str)



cdef class NKTPCVOLSlugger(StrategyInterface):
    cdef:
        readonly:
            CVOLSlugger slugger
            int32_t tp_bidprc, tp_askprc, last_tp_bidprc, last_tp_askprc
            int32_t mnk_bidprc, mnk_askprc, last_mnk_bidprc, last_mnk_askprc
            int32_t tp_ticks, check_zeros_tp, check_zeros_mnk
            str name
            uint64_t tp_secid, mnk_secid, nk_secid, trig_seqnum, last_trig_seqnum

    def __init__(self,dict slugger_parameters,uint64_t mnk_secid,uint64_t tp_secid, uint64_t nk_secid, name='null'):
        self.slugger = CVOLSlugger(**slugger_parameters)
        self.initialize([nk_secid, tp_secid, mnk_secid])
        self.tp_bidprc = 0
        self.tp_askprc = 0
        self.last_tp_bidprc = 0
        self.last_tp_askprc = 0
        self.last_mnk_bidprc = 0
        self.last_mnk_askprc = 0
        self.mnk_bidprc = 0
        self.mnk_askprc = 0
        self.trig_seqnum = 0
        self.last_trig_seqnum = 0
        self.check_zeros_tp = 0
        self.check_zeros_mnk = 0
        self.tp_ticks = 0

        self.tp_secid = tp_secid
        self.mnk_secid = mnk_secid
        self.nk_secid = nk_secid
        self.name = str(name)
    def __repr__(self):
        return 'TPcounter{name=%s}' %self.name


    cdef void _save_market(self, MDOrderBook *md):
        if md.secid==self.tp_secid:
            self.last_tp_bidprc = md.bids[0].price
            self.last_tp_askprc = md.asks[0].price
        elif md.secid == self.mnk_secid:
            self.last_mnk_bidprc = md.bids[0].price
            self.last_mnk_askprc = md.asks[0].price
#added in order to evaluate market data for CVOLSlugger
    def ref_md_from(self, MDSource src):
        print 'override Taker.ref_md_from'
        self.md_ptr = src.md_ptr
        self.slugger.ref_md_from(src)
        print 'my ptr vs src ptr', self.md_ptr == src.md_ptr
        print 'slugger ptr vs src ptr', self.slugger.md_ptr == src.md_ptr

#Need to update slugger to evaluate MD for CVOLSlugger
    cdef void _update_slugger_md(self):
        self.slugger.update_valid_md()
        self.slugger.evaluate_md(place_order=False)

    def evaluate_md(self):
        cdef:
            MDOrderBook *md = self.md_ptr
            int32_t side=0
            uint64_t time, exch_time
            bint new_market_tp = False, new_market_nk = False, new_market_mnk = False

        # evaluate md for CVOLSlugger object self.slugger
        self._update_slugger_md()

        # now I handle md here
        time = md.time
        exch_time = md.exch_time

        if not self.valid_md:
            return

        if md.secid == self.tp_secid:
            self.tp_bidprc = md.bids[0].price
            self.tp_askprc = md.asks[0].price
            new_market_tp = self.tp_bidprc != self.last_tp_bidprc or self.tp_askprc!=self.last_tp_askprc
        elif md.secid == self.mnk_secid:
            self.mnk_bidprc = md.bids[0].price
            self.mnk_askprc = md.asks[0].price
            new_market_mnk = self.mnk_bidprc != self.last_mnk_bidprc or self.mnk_askprc!=self.last_mnk_askprc



##I do not know how necessary this step is
##check_zeros variable set so we don't trigger when last prices are still 0 from initialization
        if new_market_tp:
            if self.last_tp_bidprc==0 or self.last_tp_askprc==0:
                self._save_market(md)
                self.check_zeros_tp = 1
        if new_market_mnk:
            if self.last_mnk_bidprc==0 or self.last_mnk_askprc==0:
                self._save_market(md)
                self.check_zeros_mnk = 1

##Counting TP ticks while MNK is stationary
##When the next Topix bid price is greater than the previous, tp_ticks will count 1 up
##When the next Topix ask price is less than the previous, tp_ticks will count 1 down
##When a new market in the Minis is recorded, the tp_ticks counter will reset to 0
        if new_market_tp and self.check_zeros_tp != 0:
            if self.tp_bidprc>self.last_tp_bidprc:
                self.tp_ticks+=1
                print 'TPticks_up',self.tp_ticks
            elif self.tp_askprc < self.last_tp_askprc:
                self.tp_ticks-=1
                print 'TPticks_down',self.tp_ticks
            self._save_market(md)
        if new_market_mnk and self.check_zeros_mnk != 0:
            self.tp_ticks=0
            print 'TPticks_reset',self.tp_ticks
            self._save_market(md)

#Object uses CVOL trig_seqnum and saves it as last_trig_seqnum after order placed so multiple orders are not fired on the same CVOL trig_seqnum
#Orders are fired in topix when CVOL has a buy or sell trigger for mnk
        self.trig_seqnum = self.slugger.trig_seqnum
        if self.trig_seqnum!=self.last_trig_seqnum:
            if self.slugger.buy_trigger:
#added CVOLSlugger context as well as the counted tp_ticks
                context = (
                    'mkt_seqnum=%d, '
                    'acc_buys={packets=%d, qty=%d, par=%d, %%qty=%0.3f, %%par=%0.3f}, '
                    'qty_imbalance={%0.3f, %0.3f}, par_imbalance={%0.3f, %0.3f}, '
                    'tp_ticks=%d'
                    '' % (
                        self.slugger.mkt_seqnum,
                        self.slugger.buytrades.Size(), self.slugger.buyqty, self.slugger.buypar, self.slugger.buypct, self.slugger.buyparpct,
                        self.slugger.qty_imbalance_before, self.slugger.qty_imbalance_after,
                        self.slugger.par_imbalance_before, self.slugger.par_imbalance_after,
                        self.tp_ticks
                    )
                )

                side = 1
                self.place_ghostfak_order(md.time,self.trig_seqnum,self.tp_secid,self.tp_askprc,side,1,context)
                self.last_trig_seqnum = self.trig_seqnum
            elif self.slugger.sell_trigger:
                context = (
                    'mkt_seqnum=%d, '
                    'acc_sells={packets=%d, qty=%d, par=%d, %%qty=%0.3f, %%par=%0.3f}, '
                    'qty_imbalance={%0.3f, %0.3f}, par_imbalance={%0.3f, %0.3f}, '
                    'tp_ticks=%d'
                    ''  % (
                        self.slugger.mkt_seqnum,
                        self.slugger.selltrades.Size(), self.slugger.sellqty, self.slugger.sellpar, self.slugger.sellpct, self.slugger.sellparpct,
                        self.slugger.qty_imbalance_before, self.slugger.qty_imbalance_after,
                        self.slugger.par_imbalance_before, self.slugger.par_imbalance_after,
                        self.tp_ticks
                    )
                )
                side = 2
                self.place_ghostfak_order(md.time,self.trig_seqnum,self.tp_secid,self.tp_bidprc,side,1,context)
                self.last_trig_seqnum = self.trig_seqnum

    @staticmethod
    def parse_mockorder_context(df, inplace=True):
        search_str = (
            r'mkt_seqnum=([0-9]+), acc_(buy|sell)s={packets=([0-9]+), '
            r'qty=([0-9]+), par=([0-9]+), \%qty=([\.0-9]+), \%par=([\.0-9]+)}, '
            r'qty_imbalance={([\.\-0-9]+), ([\.\-0-9]+)}, '
            r'par_imbalance={([\.\-0-9]+), ([\.\-0-9]+)}.*, '
            r'tp_ticks=(-?[-0-9]+)'
        )
        context = df['context'].str.extract(search_str, expand=True)

        convert_cols = [
            ('mkt_seqnum', int),
            ('accside', None),
            ('accpkt', int),
            ('accqty', int),
            ('accpar', int),
            ('accpct', float),
            ('accparpct', float),
            ('qty_imb_prev', float),
            ('qty_imb_now', float),
            ('par_imb_prev', float),
            ('par_imb_now', float),
            ('tp_ticks', int)
        ]
        context.columns = map(lambda x: x[0], convert_cols)

        if inplace:
            for col, dtype in convert_cols:
                if dtype is None:
                    df[col] = context[col]
                else:
                    df[col] = context[col].astype(dtype)
        else:
            for col, dtype in convert_cols:
                if dtype is not None:
                    context[col] = context[col].astype(dtype)
        return context
