# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True


cdef class Taker(MDSource):
    def gc(self):
        print 'nothing to garbage collect for', self.__class__.__name__
        pass

    def initialize(self, list secids):
        super(Taker, self).initialize(secids)
        self.orders = []

    def has_orders(self):
        return len(self.orders)

    def place_order(self, uint64_t time, uint64_t seqnum, uint64_t secid,
            int32_t price, uint16_t side, int32_t qty, str context, uint16_t book_depth=1):
        cdef MockOrder order = MockOrder(
            self, time, seqnum, secid, price, side, qty, context, book_depth)
        self.orders.append(order)

    def remove_order(self):
        return self.orders.pop()

    def __repr__(self):
        return '%s{no info}' % self.__class__.__name__


