# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from libc.stdint cimport int32_t, int64_t, uint16_t, uint32_t, uint64_t
from numpy cimport ndarray
from cython.operator cimport dereference
from libc.stdlib cimport malloc, free, atoi
from libcpp.map cimport map as map_
from libcpp.vector cimport vector as vector_
from glt.md.standard cimport MDSource, BookLevel, MDOrderBook, VolumeAccumulator, TOPIC_SIZE, copy_mdorderbook
from glt.backtesting.base cimport StrategyInterface

import re
from pandas import to_datetime, to_timedelta, NaT, DataFrame, merge as df_merge, options as pd_options
from numpy import arange, float64


cdef uint16_t TOPIC_TYPE_MSG = 2
cdef uint16_t TOPIC_TYPE_MAGIC = 3

cdef char CHAR_NULL = 0
cdef char CHAR_BLANK = 32
cdef char CHAR_SLASH = 47

cdef uint64_t ONE_MICROSECOND = 1000
cdef uint64_t ONE_MILLISECOND = 1000 * ONE_MICROSECOND
cdef uint64_t ONE_SECOND = 1000 * ONE_MILLISECOND


cdef int32_t MINIMUM_QTY = 1


cdef struct MagicQuantities:
    int32_t bid, ask


cdef double calc_imbalance(int32_t bidqty, int32_t askqty, double direction) nogil:
    cdef double diff, total
    if bidqty + askqty == 0:
        return 0
    diff = direction * (bidqty - askqty)
    total = bidqty + askqty
    return diff / total


cdef void parse_topic_for_magic_quantities(vector_[char] *topic,
        char* bidqty_buf, char* askqty_buf, MagicQuantities *qty):
    cdef:
        uint16_t i = TOPIC_TYPE_MAGIC, loc = 0
    while (i < TOPIC_SIZE and dereference(topic)[i] != CHAR_SLASH):
        bidqty_buf[loc] = dereference(topic)[i]
        i += 1
        loc += 1
    bidqty_buf[loc] = CHAR_NULL
    i += 1
    loc = 0
    while (i < TOPIC_SIZE and dereference(topic)[i] != CHAR_BLANK):
        askqty_buf[loc] = dereference(topic)[i]
        i += 1
        loc += 1
    askqty_buf[loc] = CHAR_NULL
    qty.bid = atoi(bidqty_buf)
    qty.ask = atoi(askqty_buf)


cdef class CVOLSlugger_TPX_old(StrategyInterface):
    """CVOLSlugger(secid)

    """
    cdef:
        VolumeAccumulator buytrades, selltrades, packet_cvol, buymsgs, sellmsgs
        MDOrderBook trig_md, fire_md
        bint reset_accumulators
        # readable in interpreter
        readonly:
            bint repeat_triggers, buy_trigger, sell_trigger
            int32_t buyprc, sellprc, buyqty, sellqty, buypar, sellpar
            int32_t bidprc, bidqty, bidpar, askprc, askqty, askpar
            int32_t buyvolume, sellvolume
            int32_t tradeqty_threshold, last_trade_price, report_num_levels
            uint16_t ref_trig_side, last_trade_side
            uint64_t md_channel, time_window, last_seqnum, mkt_seqnum
            double buypct, sellpct, tradepct_threshold, buyparpct, sellparpct
            double qty_imbalance_before, qty_imbalance_after, par_imbalance_before, par_imbalance_after
            int32_t ref_trig_price
            str name
            # for placing ordersself.buyprc = self.last_trade_price
            uint64_t trig_seqnum, trig_secid, fire_secid

    def __init__(self, uint64_t secid, uint64_t fire_secid, uint64_t md_channel,
            uint64_t time_window=0, int32_t tradeqty_threshold=0,
            double tradepct_threshold=0, bint repeat_triggers=True, name='null'):
        # initialize using Taker's initialize method, which requires list of
        # secids
        self.initialize([secid])
        self.trig_secid = secid
        # CVOLSlugger relevant initializations
        self.md_channel = md_channel
        self.tradeqty_threshold = tradeqty_threshold
        self.tradepct_threshold = tradepct_threshold
        self.time_window = time_window
        self.repeat_triggers = repeat_triggers
        # initialize VolumeAccumulator objects
        self.buytrades.Init(time_window)
        self.selltrades.Init(time_window)
        self.buymsgs.Init(time_window)
        self.sellmsgs.Init(time_window)
        # we need to aggregate qty based on price and side and send this
        # quantity to the appropriate trade accumulator; time_window=100ms to
        # enable accumulation of anything in a packet
        self.packet_cvol.Init(100 * ONE_MILLISECOND)
        self._clear_trades()
        self.reset_accumulators = False
        self.bidprc = 0
        self.bidqty = 0
        self.bidpar = 0
        self.askprc = 0
        self.askqty = 0
        self.askpar = 0
        self.sellvolume = 0
        self.buyvolume = 0
        self.mkt_seqnum = 0
        # initialize reference prices (will not check if repeat_triggers == True)
        self.ref_trig_price = 0
        self.ref_trig_side = 0
        self.buy_trigger = False
        self.sell_trigger = False
        self.trig_seqnum = 0
        self.fire_secid = fire_secid
        self.name = str(name)
        # keep track of last trade price and side
        self.last_trade_price = 0
        self.last_trade_side = 0
        self.last_seqnum = 0
        self.qty_imbalance_before = 0
        self.qty_imbalance_after = 0
        self.par_imbalance_before = 0
        self.par_imbalance_after = 0
        self.report_num_levels = 2

    def __repr__(self):
        return 'CVOLSlugger{name=%s}' % self.name

    cdef void _clear_trades(self):
        self.buytrades.Clear()
        self.buymsgs.Clear()
        self.selltrades.Clear()
        self.sellmsgs.Clear()
        self.buyprc = 0
        self.sellprc = 0
        self._clear_qtys()

    cdef void _clear_qtys(self) nogil:
        self.buyqty = 0
        self.buypar = 0
        self.buypct = 0
        self.buyparpct = 0
        self.sellqty = 0
        self.sellpar = 0
        self.sellpct = 0
        self.sellparpct = 0
        self.buy_trigger = False
        self.sell_trigger = False

    cdef void _save_market(self, MDOrderBook *md) nogil:
        self.bidprc = md.bids[0].price
        self.askprc = md.asks[0].price
        self.bidqty = md.bids[0].qty
        self.askqty = md.asks[0].qty
        self.bidpar = md.bids[0].participants
        self.askpar = md.asks[0].participants

    cdef void _compute_accqtys(self, uint64_t exch_time):
        cdef:
            int32_t cvol_qty = self.packet_cvol.Qty()
            int32_t cvol_msgs = self.packet_cvol.Size()
        # take what we have from packet_cvol and add it to either of our trade
        # accumulators
        #if self.last_trade_side == 1:
        if self.buyvolume > 0:
            self.buyprc = self.last_trade_price
            self.buyqty = self.buytrades.AddTrade(exch_time, cvol_qty)
            self.buypar = self.buymsgs.AddTrade(exch_time, cvol_msgs)
            self.sellqty = 0
            #print 'adding buy trade with cvolqty', cvol_qty, self.buyqty
        #elif self.last_trade_side == 2:
        elif self.sellvolume > 0:
            self.sellprc = self.last_trade_price
            self.buyqty = 0
            self.sellqty = self.selltrades.AddTrade(exch_time, cvol_qty)
            self.sellpar = self.sellmsgs.AddTrade(exch_time, cvol_msgs)
            #print 'adding sell trade with cvolqty', cvol_qty, self.sellqty
        self._compute_accpcts()
        self.packet_cvol.Clear()

    cdef void _compute_imbalance(self, double direction) nogil:
        if direction > 0:
            if self.buypct < 1.0:
                self.qty_imbalance_before = calc_imbalance(self.bidqty, self.askqty + self.buyqty, direction)
                self.qty_imbalance_after = calc_imbalance(self.bidqty, self.askqty, direction)
                self.par_imbalance_before = calc_imbalance(self.bidpar, self.askpar + self.buypar, direction)
                self.par_imbalance_after = calc_imbalance(self.bidpar, self.askpar, direction)
            else:
                self.qty_imbalance_before = calc_imbalance(self.bidqty, self.buyqty, direction)
                self.qty_imbalance_after = calc_imbalance(self.bidqty, 0, direction)
                self.par_imbalance_before = calc_imbalance(self.bidpar, self.buypar, direction)
                self.par_imbalance_after = calc_imbalance(self.bidpar, 0, direction)
        elif direction < 0:
            if self.sellpct < 1.0:
                self.qty_imbalance_before = calc_imbalance(self.bidqty + self.sellqty, self.askqty, direction)
                self.qty_imbalance_after = calc_imbalance(self.bidqty, self.askqty, direction)
                self.par_imbalance_before = calc_imbalance(self.bidpar + self.sellpar, self.askpar, direction)
                self.par_imbalance_after = calc_imbalance(self.bidpar, self.askpar, direction)
            else:
                self.qty_imbalance_before = calc_imbalance(self.sellqty, self.askqty, direction)
                self.qty_imbalance_after = calc_imbalance(0, self.askqty, direction)
                self.par_imbalance_before = calc_imbalance(self.sellpar, self.askpar, direction)
                self.par_imbalance_after = calc_imbalance(0, self.askpar, direction)

    cdef void _compute_accpcts(self) nogil:
        # buy side
        if self.buyqty > 0:
            if self.buyprc == self.askprc:
                self.buypct = (1.0 * self.buyqty) / (self.askqty + self.buyqty)
                self.buyparpct = (1.0 * self.buypar) / (self.askpar + self.buypar)
            else:
                self.buypct = 1.0
                self.buyparpct = 1.0
        else:
            self.buypct = 0.0
            self.buyparpct = 0.0
        # sell side
        if self.sellqty > 0:
            if self.sellprc == self.bidprc:
                self.sellpct = (1.0 * self.sellqty) / (self.bidqty + self.sellqty)
                self.sellparpct = (1.0 * self.sellpar) / (self.bidpar + self.sellpar)
            else:
                self.sellpct = 1.0
                self.sellparpct = 1.0
        else:
            self.sellpct = 0.0
            self.sellparpct = 0.0

    cdef void _check_trigger(self) nogil:
        self.buy_trigger = self.buyqty >= self.tradeqty_threshold and \
                           self.buypct >= self.tradepct_threshold
        self.sell_trigger = self.sellqty >= self.tradeqty_threshold and \
                            self.sellpct >= self.tradepct_threshold
        if self.buy_trigger:
            self.sellprc = 0
        if self.sell_trigger:
            self.buyprc = 0
        #self.trig_seqnum = self.last_seqnum

    cdef void _check_fire(self, int32_t qty=MINIMUM_QTY, bint place_order=True):
        cdef:
            MDOrderBook *md = &(self.trig_md)
            MDOrderBook *fire_md = &(self.fire_md)   #new for context change
            bint has_trigger = False
            uint16_t side
            int32_t price
            int32_t fire_price
            str context
        self._compute_accqtys(md.exch_time)
        self._check_trigger()
        if self.buy_trigger:
            self._compute_imbalance(1)
            side = 1
            price = self.buyprc
            fire_price = fire_md.asks[0].price ##buy the last known topix book
            has_trigger = True
            #karls low-T context string
            context = (
                'mkt_seqnum=%d, '
                'acc_buys={packets=%d, qty=%d, par=%d, %%qty=%0.3f, %%par=%0.3f}, '
                'qty_imbalance={%0.3f, %0.3f}, par_imbalance={%0.3f, %0.3f}'
                '' % (
                    self.mkt_seqnum,
                    self.buytrades.Size(), self.buyqty, self.buypar, self.buypct, self.buyparpct,
                    self.qty_imbalance_before, self.qty_imbalance_after,
                    self.par_imbalance_before, self.par_imbalance_after
                )
            )

            #boss context str
            #grab quantities, and prices from md object
            top_ask_q = (md.asks[0].qty + self.buyqty)
            top_bid_q = md.bids[0].qty
            top_ask = md.asks[0].price
            top_bid = md.bids[0].price

            # the zeros are eop and avol specific
            context_str = (
                    #'%.3f; %s; %s; %s; %s; %s; %s; %s; %s; %s; %s; md: %d/%d %d(%d):%d(%d)x%d(%d):%d(%d)'
                'pct : %s, acc_qty : %s, top_bid : %s, top_ask : %s, top_bid_qty : %s, top_ask_qty : %s, ' \
                'bid_par : %s, ask_par : %s, packet_len : %s, trade_qty : %s, target_bid : %s, target_ask : %s, ' \
                'target_bid_q1 : %s, target_bid_p1 : %s, target_bid_q0 : %s, target_bid_p0 : %s, ' \
                'target_ask_q0 : %s, target_ask_p0 : %s, target_ask_q1 : %s, target_ask_p1 : %s, side : buy'
                % (
                        self.buypct, self.buyqty, top_bid, top_ask, top_bid_q, top_ask_q,
                        md.bids[0].participants, md.asks[0].participants, md.packet_len, self.buyvolume,
                        fire_md.bids[0].price, fire_md.asks[0].price,
                        fire_md.bids[1].qty, fire_md.bids[1].participants, fire_md.bids[0].qty, fire_md.bids[0].participants,
                        fire_md.asks[0].qty, fire_md.asks[0].participants, fire_md.asks[1].qty, fire_md.asks[1].participants
                )
           )
        elif self.sell_trigger:
            self._compute_imbalance(-1)
            side = 2
            price = self.sellprc
            fire_price = fire_md.bids[0].price ##sell the last known topix book
            has_trigger = True
            #karls low-T context string
            context = (
                'mkt_seqnum=%d, '
                'acc_sells={packets=%d, qty=%d, par=%d, %%qty=%0.3f, %%par=%0.3f}, '
                'qty_imbalance={%0.3f, %0.3f}, par_imbalance={%0.3f, %0.3f}'
                ''  % (
                    self.mkt_seqnum,
                    self.selltrades.Size(), self.sellqty, self.sellpar, self.sellpct, self.sellparpct,
                    self.qty_imbalance_before, self.qty_imbalance_after,
                    self.par_imbalance_before, self.par_imbalance_after
                )
            )

            #grab quantities and prices form md object
            top_ask_q = md.asks[0].qty
            top_bid_q = (md.bids[0].qty + self.sellqty)
            top_ask = md.asks[0].price
            top_bid = md.bids[0].price

            context_str = (
                #'%.3f; %s; %s; %s; %s; %s; %s; %s; %s; %s; %s; md: %d/%d %d(%d):%d(%d)x%d(%d):%d(%d)'
                'pct : %s, acc_qty : %s, top_bid : %s, top_ask : %s, top_bid_qty : %s, top_ask_qty : %s, ' \
                'bid_par : %s, ask_par : %s, packet_len : %s, trade_qty : %s, target_bid : %s, target_ask : %s, ' \
                'target_bid_q1 : %s, target_bid_p1 : %s, target_bid_q0 : %s, target_bid_p0 : %s, ' \
                'target_ask_q0 : %s, target_ask_p0 : %s, target_ask_q1 : %s, target_ask_p1 : %s, side : sell'
                % (  
                        self.sellpct, self.sellqty, top_bid, top_ask, top_bid_q, top_ask_q,
                        md.bids[0].participants, md.asks[0].participants, md.packet_len, self.sellvolume,
                        fire_md.bids[0].price, fire_md.asks[0].price,
                        fire_md.bids[1].qty, fire_md.bids[1].participants, fire_md.bids[0].qty, fire_md.bids[0].participants,
                        fire_md.asks[0].qty, fire_md.asks[0].participants, fire_md.asks[1].qty, fire_md.asks[1].participants
                )    
            ) 

        if has_trigger and (self.repeat_triggers or \
                (price != self.ref_trig_price and side != self.ref_trig_side)):
            if place_order:
                # uses low-T context
                #self.place_ghostfak_order(
                #    md.time, self.trig_seqnum, self.fire_secid, price, side, qty, context
                #)
                #uses boss context_str
                self.place_ghostfak_order(
                    md.time, self.trig_seqnum, self.fire_secid, fire_price, side, qty, context_str
                )
            if not self.repeat_triggers:
                self.ref_trig_price = price
                self.ref_trig_side = side

    def evaluate_md(self, bint place_order=True):
        cdef:
            MDOrderBook *md = self.md_ptr
            MDOrderBook *fire_md = &(self.fire_md) #new for context str
            VolumeAccumulator *buytrades = &(self.buytrades)
            VolumeAccumulator *selltrades = &(self.selltrades)
            VolumeAccumulator *packet_cvol = &(self.packet_cvol)
            bint new_market = False
            int32_t bidprc, bidqty, askprc, askqty
            int32_t buyprice, buyvolume, sellprice, sellvolume
            uint64_t time, exch_time, channel

        # check is_open flag to see if we should be placing orders
        if not md.is_open:
            return

        # parse market data using parent function
        time = md.time
        exch_time = md.exch_time
        channel = md.channel

        #set the variable for the target contract, new for context str
        if md.secid == self.trig_secid:
            copy_mdorderbook(md, &(self.trig_md), num_levels=self.report_num_levels)

        if md.secid == self.fire_secid and self.fire_secid != self.trig_secid:
            copy_mdorderbook(md, fire_md, num_levels=self.report_num_levels)
            return

        elif md.secid == self.fire_secid and self.fire_secid == self.trig_secid:
            copy_mdorderbook(md, fire_md, num_levels=self.report_num_levels)
        
        # reset accumulators
        if (not self.repeat_triggers and (self.buy_trigger or self.sell_trigger)) or self.reset_accumulators:
            self._clear_trades()
            self.reset_accumulators = False

        if channel != self.md_channel:
            return

        if not self.valid_md:
            # even though the market data is not relevant market data, we need
            # to still evaluate whether we are at the end of packet in order
            # to save what we know as aggregate buy and sell quantities.
            # Multiple secids can come in one packet, but we still want to
            # evaluate match events per packet for the secid that we care about
            if packet_cvol.Qty() > 0:
                self._check_fire(place_order)
            return

        bidprc = md.bids[0].price
        askprc = md.asks[0].price
        bidqty = md.bids[0].qty
        askqty = md.asks[0].qty

        buyprice = md.buyprice
        buyvolume = md.buyvolume
        sellprice = md.sellprice
        sellvolume = md.sellvolume

        self.buyvolume = buyvolume
        self.sellvolume = sellvolume

        new_market = bidprc != self.bidprc or askprc != self.askprc

        self.trig_seqnum = md.seqnum

        if buyvolume > 0:
            
            self._save_market(md)
            # evaluate buys
            if self.last_trade_side != 1 or self.last_trade_price != buyprice:
                self._check_fire(place_order)
                self._clear_trades()
                packet_cvol.AddTrade(exch_time, buyvolume)
            elif md.eop:
                packet_cvol.AddTrade(exch_time, buyvolume)
                self.last_seqnum = md.seqnum
                self._check_fire(place_order)
            else:
                packet_cvol.AddTrade(exch_time, buyvolume)
            self.last_trade_side = 1
            self.last_trade_price = buyprice
        elif sellvolume > 0:
            self._save_market(md)
            # evaluate sells
            if self.last_trade_side != 2 or self.last_trade_price != sellprice:
                self._check_fire(place_order)
                self._clear_trades()
                packet_cvol.AddTrade(exch_time, sellvolume)
            elif md.eop:
                packet_cvol.AddTrade(exch_time, sellvolume)
                self.last_seqnum = md.seqnum
                self._check_fire(place_order)
            else:
                packet_cvol.AddTrade(exch_time, sellvolume)
            self.last_trade_side = 2
            self.last_trade_price = sellprice
        elif packet_cvol.Qty() > 0:
            self._check_fire(place_order)
            self._save_market(md)
        else:
            self._save_market(md)
        self.last_seqnum = md.seqnum

        # should we reset accumulators on next market data tick?
        if new_market:
            self.ref_trig_side = 0
            self.ref_trig_price = 0
            self.mkt_seqnum += 1
        self.reset_accumulators = self.reset_accumulators or new_market



cdef class CVOLSlugger_TPX(StrategyInterface):
    """CVOLSlugger(secid)

    """
    cdef:
        VolumeAccumulator buytrades, selltrades, packet_cvol, buymsgs, sellmsgs
        MDOrderBook trig_md, fire_md
        bint reset_accumulators
        # readable in interpreter
        readonly:
            bint repeat_triggers, buy_trigger, sell_trigger
            int32_t buyprc, sellprc, buyqty, sellqty, buypar, sellpar
            int32_t bidprc, bidqty, bidpar, askprc, askqty, askpar
            int32_t buyvolume, sellvolume
            int32_t tradeqty_threshold, last_trade_price, report_num_levels
            uint16_t ref_trig_side, last_trade_side
            uint64_t md_channel, time_window, last_seqnum, mkt_seqnum
            double buypct, sellpct, tradepct_threshold, buyparpct, sellparpct
            double qty_imbalance_before, qty_imbalance_after, par_imbalance_before, par_imbalance_after
            int32_t ref_trig_price, time_delta
            str name
            # for placing orders
            uint64_t trig_seqnum, trig_secid, fire_secid

    def __init__(self, uint64_t secid, uint64_t fire_secid, uint64_t md_channel,
            uint64_t time_window=0, int32_t tradeqty_threshold=0,
            double tradepct_threshold=0, bint repeat_triggers=True, name='null'):
        # initialize using Taker's initialize method, which requires list of
        # secids
        self.initialize([secid])
        self.trig_secid = secid
        # CVOLSlugger relevant initializations
        self.md_channel = md_channel
        self.tradeqty_threshold = tradeqty_threshold
        self.tradepct_threshold = tradepct_threshold
        self.time_window = time_window
        self.repeat_triggers = repeat_triggers
        self.time_delta = 0
        # initialize VolumeAccumulator objects
        self.buytrades.Init(time_window)
        self.selltrades.Init(time_window)
        self.buymsgs.Init(time_window)
        self.sellmsgs.Init(time_window)
        # we need to aggregate qty based on price and side and send this
        # quantity to the appropriate trade accumulator; time_window=100ms to
        # enable accumulation of anything in a packet
        self.packet_cvol.Init(100 * ONE_MILLISECOND)
        self._clear_trades()
        self.reset_accumulators = False
        self.bidprc = 0
        self.bidqty = 0
        self.bidpar = 0
        self.askprc = 0
        self.askqty = 0
        self.askpar = 0
        self.sellvolume = 0
        self.buyvolume = 0
        self.mkt_seqnum = 0
        # initialize reference prices (will not check if repeat_triggers == True)
        self.ref_trig_price = 0
        self.ref_trig_side = 0
        self.buy_trigger = False
        self.sell_trigger = False
        self.trig_seqnum = 0
        self.fire_secid = fire_secid
        self.name = str(name)
        # keep track of last trade price and side
        self.last_trade_price = 0
        self.last_trade_side = 0
        self.last_seqnum = 0
        self.qty_imbalancelf.buyprc = self.last_trade_price_before = 0
        self.qty_imbalance_after = 0
        self.par_imbalance_before = 0
        self.par_imbalance_after = 0
        self.report_num_levels = 2

    def __repr__(self):
        return 'CVOLSlugger{name=%s}' % self.name

    cdef void _clear_trades(self):
        self.buytrades.Clear()
        self.buymsgs.Clear()
        self.selltrades.Clear()
        self.sellmsgs.Clear()
        self.buyprc = 0
        self.sellprc = 0
        self._clear_qtys()

    cdef void _clear_qtys(self) nogil:
        self.buyqty = 0
        self.buypar = 0
        self.buypct = 0
        self.buyparpct = 0
        self.sellqty = 0
        self.sellpar = 0
        self.sellpct = 0
        self.sellparpct = 0
        self.buy_trigger = False
        self.sell_trigger = False

    cdef void _save_market(self, MDOrderBook *md) nogil:
        self.bidprc = md.bids[0].price
        self.askprc = md.asks[0].price
        self.bidqty = md.bids[0].qty
        self.askqty = md.asks[0].qty
        self.bidpar = md.bids[0].participants
        self.askpar = md.asks[0].participants

    cdef void _compute_accqtys(self, uint64_t exch_time):
        cdef:
            int32_t cvol_qty = self.packet_cvol.Qty()
            int32_t cvol_msgs = self.packet_cvol.Size()
        # take what we have from packet_cvol and add it to either of our trade
        # accumulators
        #if self.last_trade_side == 1:
        if self.buyvolume > 0:
            self.buyprc = self.last_trade_price
            self.buyqty = self.buytrades.AddTrade(exch_time, cvol_qty)
            self.buypar = self.buymsgs.AddTrade(exch_time, cvol_msgs)
            self.sellqty = 0
            #print 'adding buy trade with cvolqty', cvol_qty, self.buyqty
        #elif self.last_trade_side == 2:
        elif self.sellvolume > 0:
            self.sellprc = self.last_trade_price
            self.buyqty = 0
            self.sellqty = self.selltrades.AddTrade(exch_time, cvol_qty)
            self.sellpar = self.sellmsgs.AddTrade(exch_time, cvol_msgs)
            #print 'adding sell trade with cvolqty', cvol_qty, self.sellqty
        self._compute_accpcts()
        self.packet_cvol.Clear()

    cdef void _compute_imbalance(self, double direction) nogil:
        if direction > 0:
            if self.buypct < 1.0:
                self.qty_imbalance_before = calc_imbalance(self.bidqty, self.askqty + self.buyqty, direction)
                self.qty_imbalance_after = calc_imbalance(self.bidqty, self.askqty, direction)
                self.par_imbalance_before = calc_imbalance(self.bidpar, self.askpar + self.buypar, direction)
                self.par_imbalance_after = calc_imbalance(self.bidpar, self.askpar, direction)
            else:
                self.qty_imbalance_before = calc_imbalance(self.bidqty, self.buyqty, direction)
                self.qty_imbalance_after = calc_imbalance(self.bidqty, 0, direction)
                self.par_imbalance_before = calc_imbalance(self.bidpar, self.buypar, direction)
                self.par_imbalance_after = calc_imbalance(self.bidpar, 0, direction)
        elif direction < 0:
            if self.sellpct < 1.0:
                self.qty_imbalance_before = calc_imbalance(self.bidqty + self.sellqty, self.askqty, direction)
                self.qty_imbalance_after = calc_imbalance(self.bidqty, self.askqty, direction)
                self.par_imbalance_before = calc_imbalance(self.bidpar + self.sellpar, self.askpar, direction)
                self.par_imbalance_after = calc_imbalance(self.bidpar, self.askpar, direction)
            else:
                self.qty_imbalance_before = calc_imbalance(self.sellqty, self.askqty, direction)
                self.qty_imbalance_after = calc_imbalance(0, self.askqty, direction)
                self.par_imbalance_before = calc_imbalance(self.sellpar, self.askpar, direction)
                self.par_imbalance_after = calc_imbalance(0, self.askpar, direction)

    cdef void _compute_accpcts(self) nogil:
        # buy side
        if self.buyqty > 0:
            if self.buyprc == self.askprc:
                self.buypct = (1.0 * self.buyqty) / (self.askqty + self.buyqty)
                self.buyparpct = (1.0 * self.buypar) / (self.askpar + self.buypar)
            else:
                self.buypct = 1.0
                self.buyparpct = 1.0
        else:
            self.buypct = 0.0
            self.buyparpct = 0.0
        # sell side
        if self.sellqty > 0:
            if self.sellprc == self.bidprc:
                self.sellpct = (1.0 * self.sellqty) / (self.bidqty + self.sellqty)
                self.sellparpct = (1.0 * self.sellpar) / (self.bidpar + self.sellpar)
            else:
                self.sellpct = 1.0
                self.sellparpct = 1.0
        else:
            self.sellpct = 0.0
            self.sellparpct = 0.0

    cdef void _check_trigger(self) nogil:
        self.buy_trigger = self.buyqty >= self.tradeqty_threshold and \
                           self.buypct >= self.tradepct_threshold
        self.sell_trigger = self.sellqty >= self.tradeqty_threshold and \
                            self.sellpct >= self.tradepct_threshold
        if self.buy_trigger:
            self.sellprc = 0
        if self.sell_trigger:
            self.buyprc = 0
        #self.trig_seqnum = self.last_seqnum

    cdef void _check_fire(self, int32_t qty=MINIMUM_QTY, bint place_order=True):
        cdef:
            MDOrderBook *md = &(self.trig_md)
            MDOrderBook *fire_md = &(self.fire_md)   #new for context change
            bint has_trigger = False
            uint16_t side
            int32_t price
            int32_t fire_price
            str context
        self._compute_accqtys(md.exch_time)
        self._check_trigger()
        if self.buy_trigger:
            self._compute_imbalance(1)
            side = 1
            price = self.buyprc
            fire_price = fire_md.asks[0].price ##buy the last known topix book
            has_trigger = True
            #karls low-T context string
            context = (
                'mkt_seqnum=%d, '
                'acc_buys={packets=%d, qty=%d, par=%d, %%qty=%0.3f, %%par=%0.3f}, '
                'qty_imbalance={%0.3f, %0.3f}, par_imbalance={%0.3f, %0.3f}'
                '' % (
                    self.mkt_seqnum,
                    self.buytrades.Size(), self.buyqty, self.buypar, self.buypct, self.buyparpct,
                    self.qty_imbalance_before, self.qty_imbalance_after,
                    self.par_imbalance_before, self.par_imbalance_after
                )
            )

            #boss context str
            #grab quantities, and prices from md object
            top_ask_q = (md.asks[0].qty + self.buyqty)
            top_bid_q = md.bids[0].qty
            top_ask = md.asks[0].price
            top_bid = md.bids[0].price

            # the zeros are eop and avol specific
            context_str = (
                    #'%.3f; %s; %s; %s; %s; %s; %s; %s; %s; %s; %s; md: %d/%d %d(%d):%d(%d)x%d(%d):%d(%d)'
                'pct : %s, acc_qty : %s, top_bid : %s, top_ask : %s, top_bid_qty : %s, top_ask_qty : %s, ' \
                'bid_par : %s, ask_par : %s, packet_len : %s, trade_qty : %s, target_bid : %s, target_ask : %s, ' \
                'target_bid_q1 : %s, target_bid_p1 : %s, target_bid_q0 : %s, target_bid_p0 : %s, ' \
                'target_ask_q0 : %s, target_ask_p0 : %s, target_ask_q1 : %s, target_ask_p1 : %s, side : buy, ' \
                'md_time : %s, exch_time : %s, tdiff : %s'
                % (
                        self.buypct, self.buyqty, top_bid, top_ask, top_bid_q, top_ask_q,
                        md.bids[0].participants, md.asks[0].participants, md.packet_len, self.buyvolume,
                        fire_md.bids[0].price, fire_md.asks[0].price,
                        fire_md.bids[1].qty, fire_md.bids[1].participants, fire_md.bids[0].qty, fire_md.bids[0].participants,
                        fire_md.asks[0].qty, fire_md.asks[0].participants, fire_md.asks[1].qty, fire_md.asks[1].participants,
                        md.time, md.exch_time, self.time_delta
                )
           )
        elif self.sell_trigger:
            self._compute_imbalance(-1)
            side = 2
            price = self.sellprc
            fire_price = fire_md.bids[0].price ##sell the last known topix book
            has_trigger = True
            #karls low-T context string
            context = (
                'mkt_seqnum=%d, '
                'acc_sells={packets=%d, qty=%d, par=%d, %%qty=%0.3f, %%par=%0.3f}, '
                'qty_imbalance={%0.3f, %0.3f}, par_imbalance={%0.3f, %0.3f}'
                ''  % (
                    self.mkt_seqnum,
                    self.selltrades.Size(), self.sellqty, self.sellpar, self.sellpct, self.sellparpct,
                    self.qty_imbalance_before, self.qty_imbalance_after,
                    self.par_imbalance_before, self.par_imbalance_after
                )
            )

            #grab quantities and prices form md object
            top_ask_q = md.asks[0].qty
            top_bid_q = (md.bids[0].qty + self.sellqty)
            top_ask = md.asks[0].price
            top_bid = md.bids[0].price

            context_str = (
                #'%.3f; %s; %s; %s; %s; %s; %s; %s; %s; %s; %s; md: %d/%d %d(%d):%d(%d)x%d(%d):%d(%d)'
                'pct : %s, acc_qty : %s, top_bid : %s, top_ask : %s, top_bid_qty : %s, top_ask_qty : %s, ' \
                'bid_par : %s, ask_par : %s, packet_len : %s, trade_qty : %s, target_bid : %s, target_ask : %s, ' \
                'target_bid_q1 : %s, target_bid_p1 : %s, target_bid_q0 : %s, target_bid_p0 : %s, ' \
                'target_ask_q0 : %s, target_ask_p0 : %s, target_ask_q1 : %s, target_ask_p1 : %s, side : sell, ' \
                'md_time : %s, exch_time : %s, tdiff : %s'
                % (  
                        self.sellpct, self.sellqty, top_bid, top_ask, top_bid_q, top_ask_q,
                        md.bids[0].participants, md.asks[0].participants, md.packet_len, self.sellvolume,
                        fire_md.bids[0].price, fire_md.asks[0].price,
                        fire_md.bids[1].qty, fire_md.bids[1].participants, fire_md.bids[0].qty, fire_md.bids[0].participants,
                        fire_md.asks[0].qty, fire_md.asks[0].participants, fire_md.asks[1].qty, fire_md.asks[1].participants,
                        md.time, md.exch_time, self.time_delta
                )    
            ) 

        if has_trigger and (self.repeat_triggers or \
                (price != self.ref_trig_price and side != self.ref_trig_side)):
            if place_order:
                # uses low-T context
                #self.place_ghostfak_order(
                #    md.time, self.trig_seqnum, self.fire_secid, price, side, qty, context
                #)
                #uses boss context_str
                self.place_ghostfak_order(
                    md.time, self.trig_seqnum, self.fire_secid, fire_price, side, qty, context_str
                )
            if not self.repeat_triggers:
                self.ref_trig_price = price
                self.ref_trig_side = side

    def evaluate_md(self, bint place_order=True):
        cdef:
            MDOrderBook *md = self.md_ptr
            MDOrderBook *fire_md = &(self.fire_md) #new for context str
            VolumeAccumulator *buytrades = &(self.buytrades)
            VolumeAccumulator *selltrades = &(self.selltrades)
            VolumeAccumulator *packet_cvol = &(self.packet_cvol)
            bint new_market = False
            int32_t bidprc, bidqty, askprc, askqty
            int32_t buyprice, buyvolume, sellprice, sellvolume
            uint64_t time, exch_time, channel

        # check is_open flag to see if we should be placing orders
        if not md.is_open:
            return

        # parse market data using parent function
        time = md.time
        exch_time = md.exch_time
        channel = md.channel
        self.time_delta = time - exch_time

        #set the variable for the target contract, new for context str
        if md.secid == self.trig_secid:
            copy_mdorderbook(md, &(self.trig_md), num_levels=self.report_num_levels)

        if md.secid == self.fire_secid and self.fire_secid != self.trig_secid:
            copy_mdorderbook(md, fire_md, num_levels=self.report_num_levels)
            return

        elif md.secid == self.fire_secid and self.fire_secid == self.trig_secid:
            copy_mdorderbook(md, fire_md, num_levels=self.report_num_levels)
 
        #if md.secid == self.trig_secid:
        #    copy_mdorderbook(md, &(self.trig_md), num_levels=self.report_num_levels)
        #elif md.secid == self.fire_secid:
        #    copy_mdorderbook(md, fire_md, num_levels=self.report_num_levels)
        #    return
        
        # reset accumulators
        if (not self.repeat_triggers and (self.buy_trigger or self.sell_trigger)) or self.reset_accumulators:
            self._clear_trades()
            self.reset_accumulators = False

        if channel != self.md_channel:
            return

        if not self.valid_md:
            # even though the market data is not relevant market data, we need
            # to still evaluate whether we are at the end of packet in order
            # to save what we know as aggregate buy and sell quantities.
            # Multiple secids can come in one packet, but we still want to
            # evaluate match events per packet for the secid that we care about
            if packet_cvol.Qty() > 0:
                self._check_fire(place_order)
            return

        bidprc = md.bids[0].price
        askprc = md.asks[0].price
        bidqty = md.bids[0].qty
        askqty = md.asks[0].qty

        buyprice = md.buyprice
        buyvolume = md.buyvolume
        sellprice = md.sellprice
        sellvolume = md.sellvolume

        self.buyvolume = buyvolume
        self.sellvolume = sellvolume

        new_market = bidprc != self.bidprc or askprc != self.askprc

        self.trig_seqnum = md.seqnum

        if buyvolume > 0:
            
            self._save_market(md)
            # evaluate buys
            if self.last_trade_side != 1 or self.last_trade_price != buyprice:
                self._clear_trades()
                packet_cvol.AddTrade(exch_time, buyvolume)
                self._check_fire(place_order)
                #print "after check_fire"
                #print "BUY 1", md.seqnum, buyvolume, self.buyqty
            #elif md.eop:
            #    packet_cvol.AddTrade(exch_time, buyvolume)
            #    self.last_seqnum = md.seqnum
            #    self._check_fire(place_order)
            else:
                packet_cvol.AddTrade(exch_time, buyvolume)
                self._check_fire(place_order)
                #print "BUY 2", md.seqnum, buyvolume, self.buyqty
            self.last_trade_side = 1
            self.last_trade_price = buyprice
        elif sellvolume > 0:
            self._save_market(md)
            # evaluate sells
            if self.last_trade_side != 2 or self.last_trade_price != sellprice:
                self._clear_trades()
                packet_cvol.AddTrade(exch_time, sellvolume)
                self._check_fire(place_order)
                #print "after check_fire"
                #print "SELL 1", md.seqnum, sellvolume, self.sellqty
            #elif md.eop:
            #    packet_cvol.AddTrade(exch_time, sellvolume)
            #    self.last_seqnum = md.seqnum
            #    self._check_fire(place_order)
            else:
                packet_cvol.AddTrade(exch_time, sellvolume)
                self._check_fire(place_order)
                #print "SELL 2", md.seqnum, sellvolume, self.sellqty
            self.last_trade_side = 2
            self.last_trade_price = sellprice
        elif packet_cvol.Qty() > 0:
            self._check_fire(place_order)
            self._save_market(md)
            #print "CVOL_QTY", md.seqnum, self.packet_cvol.Qty()
        else:
            self._save_market(md)
        self.last_seqnum = md.seqnum

        # should we reset accumulators on next market data tick?
        if new_market:
            self.ref_trig_side = 0
            self.ref_trig_price = 0
            self.mkt_seqnum += 1
        self.reset_accumulators = self.reset_accumulators or new_market

    @staticmethod
    def parse_mockorder_context(df, inplace=True):
        search_str = (
            r'mkt_seqnum=([0-9]+), acc_(buy|sell)s={packets=([0-9]+), '
            r'qty=([0-9]+), par=([0-9]+), \%qty=([\.0-9]+), \%par=([\.0-9]+)}, '
            r'qty_imbalance={([\.\-0-9]+), ([\.\-0-9]+)}, '
            r'par_imbalance={([\.\-0-9]+), ([\.\-0-9]+)}.*'
        )
        context = df['context'].str.extract(search_str, expand=True)

        convert_cols = [
            ('mkt_seqnum', int),
            ('accside', None),
            ('accpkt', int),
            ('accqty', int),
            ('accpar', int),
            ('accpct', float),
            ('accparpct', float),
            ('qty_imb_prev', float),
            ('qty_imb_now', float),
            ('par_imb_prev', float),
            ('par_imb_now', float)
        ]
        context.columns = map(lambda x: x[0], convert_cols)

        if inplace:
            for col, dtype in convert_cols:
                if dtype is None:
                    df[col] = context[col]
                else:
                    df[col] = context[col].astype(dtype)
        else:
            for col, dtype in convert_cols:
                if dtype is not None:
                    context[col] = context[col].astype(dtype)
        return context




def parse_cvol_tpx_name(df):
    name = df['strategy'].str.extract('CVOLSlugger_TPX{name=(.+) (.+); (.+)}', expand=True)
    name.columns = ['shortname', 'kind', 'acc_type']
    for col in name:
        df[col] = name[col]



    

cdef class AVOLSluggerEOnly_TPX(StrategyInterface):
    """
    Overview: Strat inherits from StrategyInterface(MDSource).
        StrategyInterface inits an order through the place_ghostfak_order method
    """
    cdef:
        char *bidqty_buf
        char *askqty_buf
        MagicQuantities qtys
        MDOrderBook fire_md
        # readable by interpreter
        readonly:
            bint repeat_triggers, avol_last_pkt, avol_pkt
            uint64_t trig_secid, fire_secid, first_secid, last_exch_time, last_pcap_time, report_num_levels
            int32_t tradeqty_threshold, avol_msg_count, buyvolume, sellvolume
            uint16_t prev_msg_idx, trig_packet_len
            #map_[uint16_t, uint16_t] packet_len_triggers
            dict packet_len_triggers
            str name
            # attributes for new md parsing
            double avol_qty
            char msg_tag
            uint64_t top_bid, top_ask, avol_seqnum, top_bid_q, top_ask_q, buyprice, sellprice, prev_pcap_time, bid_par, ask_par
            bint avol
            bint update_after_avol
            uint64_t fire_time, fire_price
            int32_t fire_avol_seqnum, fire_side
            str fire_context_str

    def __init__(self, uint64_t trig_secid, uint64_t fire_secid, int32_t tradeqty_threshold,
                 list packet_len_triggers, bint repeat_triggers=False, name="null"):
        cdef uint16_t packet_len, msg_idx
        self.initialize([trig_secid, fire_secid])
        self.trig_secid = trig_secid
        self.fire_secid = fire_secid
        self.tradeqty_threshold = tradeqty_threshold
        self.packet_len_triggers = {}
        for packet_len, msg_idx in packet_len_triggers:
            self.packet_len_triggers[packet_len] = msg_idx
        self.name = str(name)
        self.repeat_triggers = repeat_triggers
        self.last_exch_time = 0
        self.last_pcap_time = 0
        self.avol_msg_count = 0
        self.bidqty_buf = <char*>malloc(8 * sizeof(char))
        self.askqty_buf = <char*>malloc(8 * sizeof(char))
        self.report_num_levels = 1
        self.avol_last_pkt = False
        self.avol_pkt = False
        self.avol_qty = 0
        self.avol_seqnum = 0
        self.prev_pcap_time = 0
        self.prev_msg_idx = 0
        self.buyvolume = 0
        self.sellvolume = 0
        self.update_after_avol = False
        self.fire_time = 0
        self.fire_avol_seqnum = 0
        self.fire_price = 0
        self.fire_side = 0
        self.fire_context_str = ''
        self.first_secid = 0
        self.trig_packet_len = 0

    def __repr__(self):
       return 'AVOLSluggerEOnly_TPX: some stuff'

    def gc(self):
        """gc()
Free malloc'ed pointers (self.bidqty_buf, self.askqty_buf). This is up to the
user to call (backtester scripts should call this method automatically after
processing all market data).
        """
        print 'freeing self.bidqty_buf and self.askqty_buf'
        free(self.bidqty_buf)
        free(self.askqty_buf)

    #cdef bint _is_trigger(self, uint16_t packet_len, uint16_t msg_idx, char msg_tag) nogil:
    cdef bint _is_trigger(self, uint16_t packet_len, uint16_t msg_idx, char msg_tag):
        cdef:
            #bint exists = self.packet_len_triggers.find(packet_len) != self.packet_len_triggers.end()
            uint16_t mapped_msg_idx

        

        if packet_len in self.packet_len_triggers.keys():
            exists = True
        else:
            exists = False
        if not exists:
            return False

        print 'packet_len_triggers'
        print self.packet_len_triggers[packet_len]
        print packet_len, msg_idx, msg_tag

        #mapped_msg_idx = self.packet_len_triggers[packet_len]
        #return msg_idx == mapped_msg_idx and msg_tag == 'E'
        return True

    def evaluate_md_oldparser(self):
        """evaluate_md()

Evaluate market data from self.md_ptr (pointing to MDOrderBook object from
external MDSource) to determine whether we should fire on an assumed volume
event. Returns nothing, but it prepares orders (appends MockOrder to
self.orders using self.place_ghostfak_order function inherited by StrategyInterface).

        """
        cdef:
            MDOrderBook *md = self.md_ptr
            MDOrderBook *fire_md = &(self.fire_md)
            char msg_tag
            uint16_t packet_len, msg_idx
            uint64_t exch_time
            uint64_t pcap_time
            MagicQuantities *qtys = &(self.qtys)   #<---- this no longer works
            double buypct, sellpct
            uint64_t client_orderid = 0

        # absolutely necessary step: do we care about this market data?
        # self.valid_md == False if it isn't the market data md_ptr contains
        # is not our trig_secid
        if not self.valid_md:
            return

        if md.secid == self.fire_secid:
            copy_mdorderbook(md, fire_md, num_levels=self.report_num_levels)
            return
        packet_len = md.packet_len
        msg_idx = md.msg_idx
        msg_tag = md.topic[TOPIC_TYPE_MSG]
        exch_time = md.exch_time
        pcap_time = md.time
        bail = False 

        # Note about summing volume:
        # this will sum volume across trade events with one caveat
        # if there is a packet with NK and TP evnets to form a "full packet"
        # it will use the avol packet logic and use the top 26 par qty.
        self.buyvolume += md.buyvolume
        self.sellvolume += md.sellvolume

        # is this packet_len a triggerable packet_len? if so, let's fire!
        # we want to check packet_len, msg_idx == 1, and msg_tag == 'E'
        if not self._is_trigger(packet_len, msg_idx, msg_tag):
            # if its the first message, its not an avol so mark it so
            if msg_idx == 1:
                self.avol_pkt = False
                #this is an attempt to catch cases where the EOP packet of an avol is
                # not parsed for some reason (like its a security we dont care about)
                if self.last_exch_time != exch_time:
                    self.avol_last_pkt = False
            
            # if the previous packet was an avol and this pkt isnt and its the last
            # msg in the packet, call this a trigger because its the last packet in the 
            # trade event
            if self.avol_last_pkt and md.eop and not self.avol_pkt:
                self.avol_last_pkt = False
            else:
                return
        else:
            # if this is the start of a trade event (ie first avol) then start the qty count
            # for sanity the code should probalby come first
            if not self.avol_last_pkt:
                self.buyvolume = md.buyvolume
                self.sellvolume = md.sellvolume
            self.avol_last_pkt = True
            self.avol_pkt = True  

        self.prev_msg_idx = msg_idx

        # parse topic in a gruesome way for the first 26 participant summed qty
        parse_topic_for_magic_quantities(&(md.topic), self.bidqty_buf, self.askqty_buf, qtys)

        #juggling volumes:
        #if its and avol and prev packet was an avol theres a chance that there will be
        # no followup non-avol packet to nicely sum the qty, so we need to add the avol qty
        # and remove the row qty.... we need to back this out after we print it b/c there 
        #still might be a non-avol packet after this.  This should be the correct qty 
        # UP TO THIS POINT.
        if self.avol_pkt and self.avol_last_pkt:
            self.buyvolume = self.buyvolume - md.buyvolume + qtys.ask
            self.sellvolume = self.sellvolume - md.sellvolume + qtys.bid
                                                                                                   
        #check to make sure this packet time doesnt == last packet time
        if pcap_time != self.last_pcap_time:
            #mark sequential avol messages with number
            #in order to filter these out in the output use drop_duplicates on the 'trade_time' 
            #  column in the df from regex_to_df function
            if exch_time == self.last_exch_time:
                self.avol_msg_count += 1
            else:
                self.avol_msg_count = 0
            if md.buyvolume > 0:
                #grab quantities, and prices from md object
                top_ask_q = (md.asks[0].qty + md.buyvolume)
                top_bid_q = md.bids[0].qty
                top_ask = md.asks[0].price
                top_bid = md.bids[0].price
                #msg_type = str(bytearray(md.topic).decode()[:3])  #unpacking the hipster datatype "bytearray" into a string
                #calculate avol % (qtys object uses the Magic)
                buypct = <double>qtys.ask / top_ask_q
                print 'BUY!', md.channel, md.packet_len, md.seqnum, md.buyprice, md.buyvolume, qtys.ask, buypct, md.bid_avol_qty, md.ask_avol_qty
                #context_str = '%.3f; %s' % (buypct, qtys.ask)
                #add to context string: avol %, avol qty and top of book info, E msg index
                context_str = (
                    '%.3f; %s; %s; %s; %s; %s; %s; %s; %s; %s; %s; md: %d/%d %d(%d):%d(%d)x%d(%d):%d(%d); %s; %s'
                ) % (
                        buypct, qtys.ask, top_bid, top_ask, top_bid_q, top_ask_q,
                        md.bids[0].participants, md.asks[0].participants, msg_idx, self.avol_msg_count,
                        self.buyvolume,
                        fire_md.bids[0].price, fire_md.asks[0].price,
                        fire_md.bids[1].qty, fire_md.bids[1].participants, fire_md.bids[0].qty, fire_md.bids[0].participants,
                        fire_md.asks[0].qty, fire_md.asks[0].participants, fire_md.asks[1].qty, fire_md.asks[1].participants,
                        packet_len, chr(md.topic[0])
                )
                client_orderid = self.place_ghostfak_order(md.time, md.seqnum, self.fire_secid, md.buyprice, 1, 10, context_str)
            else:
                #grab quantities and prices form md object
                top_ask_q = md.asks[0].qty
                top_bid_q = (md.bids[0].qty + md.sellvolume)
                top_ask = md.asks[0].price
                top_bid = md.bids[0].price
                #msg_type = str(bytearray(md.topic).decode()[:3])  #unpacking the hipster datatype "bytearray" into a string
                #cacualate avol %
                sellpct = <double>qtys.bid / top_bid_q
                print 'SELL!', md.channel, md.packet_len, md.seqnum, md.sellprice, md.sellvolume, qtys.bid, sellpct, md.bid_avol_qty, md.ask_avol_qty
                context_str = (
                    '%.3f; %s; %s; %s; %s; %s; %s; %s; %s; %s; %s; md: %d/%d %d(%d):%d(%d)x%d(%d):%d(%d); %s; %s'
                ) % (
                        sellpct, qtys.bid, top_bid, top_ask, top_bid_q, top_ask_q,
                        md.bids[0].participants, md.asks[0].participants, msg_idx, self.avol_msg_count,
                        self.sellvolume,
                        fire_md.bids[0].price, fire_md.asks[0].price,
                        fire_md.bids[1].qty, fire_md.bids[1].participants, fire_md.bids[0].qty, fire_md.bids[0].participants,
                        fire_md.asks[0].qty, fire_md.asks[0].participants, fire_md.asks[1].qty, fire_md.asks[1].participants,
                        packet_len, chr(md.topic[0])
                     )
                client_orderid = self.place_ghostfak_order(md.time, md.seqnum, self.fire_secid, md.sellprice, 2, 10, context_str)
            self.last_exch_time = exch_time
            self.last_pcap_time = pcap_time

            #backing out the volume juggling from lines 782-785
            if self.avol_pkt and self.avol_last_pkt:
                self.buyvolume = self.buyvolume + md.buyvolume - qtys.ask
                self.sellvolume = self.sellvolume + md.sellvolume - qtys.bid

    def evaluate_md(self):
        cdef:
            MDOrderBook *md = self.md_ptr
            MDOrderBook *fire_md = &(self.fire_md)
            char msg_tag
            uint16_t packet_len, msg_idx = 0
            uint64_t exch_time, pcap_time
            double buypct, sellpct
            uint64_t client_orderid = 0
            bint reset = False 
            float imbal = 0.0, par_imbal = 0.0


        #neceesary step im told
        # self.valid_md == False if it isnt the market data md_ptr contains si not our trig sec_id
        if not self.valid_md:
            return

        if md.secid == self.fire_secid and not self.update_after_avol:
            copy_mdorderbook(md, fire_md, num_levels=self.report_num_levels)
            return

        elif md.secid == self.fire_secid and self.update_after_avol:
            copy_mdorderbook(md, fire_md, num_levels=self.report_num_levels)
            client_orderid = self.place_ghostfak_order(self.fire_time, self.fire_avol_seqnum, self.fire_secid, self.fire_price, self.fire_side, 10, self.fire_context_str)
            self.update_after_avol = False

        packet_len = md.packet_len
        msg_tag = md.topic[0]
        exch_time = md.exch_time
        pcap_time = md.time


        if pcap_time != self.prev_pcap_time:
            #new packet: reset prev_pkt_num and if its a D or E and pkt_len is correct, start avol process
            # T msgs dont show up so you dont need to check for them (but they effect the pkt size)
            self.prev_pcap_time = pcap_time
             
            #check if exch time is the same as last to see if these are all the same event
            if exch_time == self.last_exch_time and self.avol_last_pkt:
                self.avol_msg_count += 1
            else:
                self.avol_msg_count = 0
                self.last_exch_time = exch_time

            # only looking for D or E msgs bc bruce doest pass up T messages
            if msg_tag in ['D', 'E'] and packet_len in self.packet_len_triggers.keys():
                self.avol_seqnum = md.seqnum
                self.avol = True
                self.avol_qty = md.buyvolume + md.sellvolume #if D, then both will be zero
                # keep summing across avol packets, or reset
                if not self.avol_last_pkt:
                    self.buyvolume = md.buyvolume
                    self.sellvolume = md.sellvolume
                else:
                    self.buyvolume += md.buyvolume
                    self.sellvolume += md.sellvolume
                #set your variables from the first message in the packet    
                self.top_ask = md.asks[0].price
                self.top_bid = md.bids[0].price
                self.msg_tag = msg_tag
                self.first_secid = md.secid
                self.trig_packet_len = packet_len

                if md.buyvolume > 0:
                    self.top_ask_q = md.asks[0].qty + md.buyvolume
                    self.top_bid_q = md.bids[0].qty
                    #self.buyprice = md.buyprice #for triggering off nk into nk/mnk
                    self.buyprice = fire_md.asks[0].price # for triggering off nk into sep contract
                    self.ask_par = md.asks[0].participants + 1 #add the traded with par back in
                    self.bid_par = md.bids[0].participants
                    imbal = 1.0 * (self.top_bid_q - self.top_ask_q) / (self.top_bid_q + self.top_ask_q)
                    par_imbal = 1.0 * (self.bid_par - self.ask_par) / (self.bid_par + self.ask_par)
                elif md.sellvolume > 0:
                    self.top_ask_q = md.asks[0].qty
                    self.top_bid_q = md.bids[0].qty + md.sellvolume
                    #self.sellprice = md.sellprice
                    self.sellprice = fire_md.bids[0].price # for triggering off nk into sep fire contract
                    self.ask_par = md.asks[0].participants 
                    self.bid_par = md.bids[0].participants + 1 #add the traded with par back in
                    imbal = -1.0 * (self.top_bid_q - self.top_ask_q) / (self.top_bid_q + self.top_ask_q)
                    par_imbal = -1.0 * (self.bid_par - self.ask_par) / (self.bid_par + self.ask_par)
 
            else:
                #reset your vars if its not an avol packet
                self.avol = False
                self.buyvolume = 0
                self.sellvolume = 0
                self.first_secid = 0
                self.trig_packet_len = 0
        elif msg_tag == 'E' and self.avol:
            # if you only get subsequent E messages after setting avol=True, then youre good
            self.buyvolume += md.buyvolume
            self.sellvolume += md.sellvolume
            if not md.eop:
                self.avol_qty += md.buyvolume + md.sellvolume #one of these will be zero
            elif md.eop:
                self.avol_qty += 1 #we dont "know" last qty while predicting avol qty
                self.avol_last_pkt = True
        elif self.avol:
            # if you get any other type of message after getting a D or E, its not an avol so reset vars
            self.avol = False
            self.avol_qty = 0
            self.avol_last_pkt = False
            self.buyvolume = 0
            self.sellvolume = 0
            self.first_secid = 0
            self.trig_packet_len = 0

        #trigger check
        if self.avol and md.eop:
            #trigger!  yay, made it through the whole pkt with only E msgs after first msg!
            if md.buyvolume > 0:
                #buy trigger
                buypct = <double>self.avol_qty / self.top_ask_q
                trig_imbal = 1.0 * (fire_md.bids[0].qty - fire_md.asks[0].qty) / (fire_md.bids[0].qty + fire_md.asks[0].qty)
                trig_par_imbal = 1.0 * (fire_md.bids[0].participants - fire_md.asks[0].participants) / (fire_md.bids[0].participants + fire_md.asks[0].participants)
                print 'BUY!', md.channel, md.packet_len, md.seqnum, 'avol_seqnum:', self.avol_seqnum, self.buyprice, md.buyvolume, buypct, self.avol_qty
                print md.time, 'trigger: ', self.trig_secid, md.seqnum, 'fireprice:', fire_md.bids[0].price, fire_md.asks[0].price, fire_md.seqnum
                context_str = (
                 'side: 1, tradepct: %f, avol_qty: %d, tradevolume: %d, leanpar0: %d, leanqty0: %d, leanprc0: %d, oppprc0: %d, oppqty0: %d, opppar0: %d, '
                 'trig_leanpar0: %d, trig_leanqty0: %d, trig_leanprc0: %d, trig_oppprc0: %d, trig_oppqty0: %d, trig_opppar0: %d, '
                 'imbal: %f, par_imbal: %f, trig_imbal: %f, trig_par_imbal: %f, first_secid: %d, msg_idx: %d, avol_msg_count: %d, packet_len: %d, msg_tag: %s, trig_secid: %d'
                % (buypct, self.avol_qty, self.buyvolume, self.bid_par, self.top_bid_q, self.top_bid, self.top_ask, self.top_ask_q, self.ask_par,
                   fire_md.bids[0].participants, fire_md.bids[0].qty, fire_md.bids[0].price, 
                   fire_md.asks[0].price, fire_md.asks[0].qty, fire_md.asks[0].participants,
                   imbal, par_imbal, trig_imbal, trig_par_imbal, self.first_secid, msg_idx, 
                   self.avol_msg_count, self.trig_packet_len, chr(self.msg_tag), self.trig_secid
                   )
                )
                #to change your order data, change this line
                #client_orderid = self.place_ghostfak_order(md.time, self.avol_seqnum, self.fire_secid, self.buyprice, 1, 10, context_str)
                self.fire_time = md.time
                self.fire_avol_seqnum = self.avol_seqnum
                self.fire_price = self.buyprice
                self.fire_context_str = context_str 
                self.fire_side = 1
                self.update_after_avol = True
            else:
                #sell trigger
                sellpct = <double>self.avol_qty / self.top_bid_q
                trig_imbal = -1.0 * (fire_md.bids[0].qty - fire_md.asks[0].qty) / (fire_md.bids[0].qty + fire_md.asks[0].qty)
                trig_par_imbal = -1.0 * (fire_md.bids[0].participants - fire_md.asks[0].participants) / (fire_md.bids[0].participants + fire_md.asks[0].participants)
                print 'SELL!', md.channel, md.packet_len, md.seqnum, 'avol_seqnum:', self.avol_seqnum, self.sellprice, md.sellvolume, sellpct, self.avol_qty
                print md.time, 'trigger: ', self.trig_secid, md.seqnum, 'fireprice:', fire_md.bids[0].price, fire_md.asks[0].price, fire_md.seqnum

                context_str = (
                 'side: -1, tradepct: %f, avol_qty: %d, tradevolume: %d, leanpar0: %d, leanqty0: %d, leanprc0: %d, oppprc0: %d, oppqty0: %d, opppar0: %d, '
                 'trig_leanpar0: %d, trig_leanqty0: %d, trig_leanprc0: %d, trig_oppprc0: %d, trig_oppqty0: %d, trig_opppar0: %d, '
                 'imbal: %f, par_imbal: %f, trig_imbal: %f, trig_par_imbal: %f, first_secid: %d, msg_idx: %d, avol_msg_count: %d, packet_len: %d, msg_tag: %s, trig_secid: %d'
                % (sellpct, self.avol_qty, self.sellvolume, self.ask_par, self.top_ask_q, self.top_ask, self.top_bid, self.top_bid_q, self.bid_par,
                   fire_md.asks[0].participants, fire_md.asks[0].qty, fire_md.asks[0].price, 
                   fire_md.bids[0].price, fire_md.bids[0].qty, fire_md.bids[0].participants,
                   imbal, par_imbal, trig_imbal, trig_par_imbal, self.first_secid, msg_idx, 
                   self.avol_msg_count, self.trig_packet_len, chr(self.msg_tag), self.trig_secid
                   )
                ) 
               #to change your order data, change this line
                #client_orderid = self.place_ghostfak_order(md.time, self.avol_seqnum, self.fire_secid, self.sellprice, 2, 10, context_str)
                self.fire_time = md.time
                self.fire_avol_seqnum = self.avol_seqnum
                self.fire_price = self.sellprice
                self.fire_context_str = context_str
                self.fire_side = 2
                self.update_after_avol = True
 


cdef class AVOLSweeps_TPX(AVOLSluggerEOnly_TPX):
    cdef:
        bint sweep
        uint64_t pkt_qty
        uint64_t trig_buyprice, trig_sellprice
    def __init__(self, uint64_t trig_secid, uint64_t fire_secid, int32_t tradeqty_threshold,
                 list packet_len_triggers, bint repeat_triggers=False, name="null"):
        AVOLSluggerEOnly_TPX.__init__( self, trig_secid, fire_secid, tradeqty_threshold,
                 packet_len_triggers, repeat_triggers, name)
        self.sweep = False
        self.pkt_qty = 0
        self.trig_buyprice = 0
        self.trig_sellprice = 0

    def evaluate_md(self):
        cdef:
            MDOrderBook *md = self.md_ptr
            MDOrderBook *fire_md = &(self.fire_md)
            char msg_tag
            uint16_t packet_len, msg_idx = 0
            uint64_t exch_time, pcap_time
            double buypct, sellpct
            uint64_t client_orderid = 0
            bint reset = False 
            float imbal = 0.0, par_imbal = 0.0


        #neceesary step im told
        # self.valid_md == False if it isnt the market data md_ptr contains si not our trig sec_id
        if not self.valid_md:
            return

        if md.secid == self.fire_secid and not self.update_after_avol:
            copy_mdorderbook(md, fire_md, num_levels=self.report_num_levels)
            return

        elif md.secid == self.fire_secid and self.update_after_avol:
            copy_mdorderbook(md, fire_md, num_levels=self.report_num_levels)
            client_orderid = self.place_ghostfak_order(self.fire_time, self.fire_avol_seqnum, self.fire_secid, self.fire_price, self.fire_side, 10, self.fire_context_str)
            self.update_after_avol = False

        packet_len = md.packet_len
        msg_tag = md.topic[0]
        exch_time = md.exch_time
        pcap_time = md.time


        if pcap_time != self.prev_pcap_time:
            #new packet: reset prev_pkt_num and if its a D or E and pkt_len is correct, start avol process
            # T msgs dont show up so you dont need to check for them (but they effect the pkt size)
            self.prev_pcap_time = pcap_time
             
            #check if exch time is the same as last to see if these are all the same event
            if exch_time == self.last_exch_time and self.avol_last_pkt:
                self.avol_msg_count += 1
            else:
                self.avol_msg_count = 0
                self.last_exch_time = exch_time

            # only looking for D or E msgs bc bruce doest pass up T messages
            if msg_tag in ['D', 'E'] and packet_len in self.packet_len_triggers.keys():
                self.avol_seqnum = md.seqnum
                self.avol = True
                self.sweep = False
                self.avol_qty = md.buyvolume + md.sellvolume #if D, then both will be zero
                # keep summing across avol packets, or reset
                if not self.avol_last_pkt:
                    self.buyvolume = md.buyvolume
                    self.sellvolume = md.sellvolume
                else:
                    self.buyvolume += md.buyvolume
                    self.sellvolume += md.sellvolume
                #set your variables from the first message in the packet    
                self.top_ask = md.asks[0].price
                self.top_bid = md.bids[0].price
                self.msg_tag = msg_tag
                self.first_secid = md.secid
                self.trig_packet_len = packet_len

                if md.buyvolume > 0:
                    self.top_ask_q = md.asks[0].qty + md.buyvolume
                    self.top_bid_q = md.bids[0].qty
                    self.trig_buyprice = md.buyprice #need this to compare to prev prices
                    #self.buyprice = md.buyprice #for triggering off nk into nk/mnk
                    self.buyprice = fire_md.asks[0].price # for triggering off nk into sep contract
                    self.ask_par = md.asks[0].participants + 1 #add the traded with par back in
                    self.bid_par = md.bids[0].participants
                    imbal = 1.0 * (self.top_bid_q - self.top_ask_q) / (self.top_bid_q + self.top_ask_q)
                    par_imbal = 1.0 * (self.bid_par - self.ask_par) / (self.bid_par + self.ask_par)
                elif md.sellvolume > 0:
                    self.top_ask_q = md.asks[0].qty
                    self.top_bid_q = md.bids[0].qty + md.sellvolume
                    self.trig_sellprice = md.sellprice
                    #self.sellprice = md.sellprice
                    self.sellprice = fire_md.bids[0].price # for triggering off nk into sep fire contract
                    self.ask_par = md.asks[0].participants 
                    self.bid_par = md.bids[0].participants + 1 #add the traded with par back in
                    imbal = -1.0 * (self.top_bid_q - self.top_ask_q) / (self.top_bid_q + self.top_ask_q)
                    par_imbal = -1.0 * (self.bid_par - self.ask_par) / (self.bid_par + self.ask_par)
 
                if msg_tag == 'D':
                    print self.buyprice, self.sellprice, self.buyvolume, self.sellvolume

            else:
                #reset your vars if its not an avol packet
                self.sweep = False
                self.avol = False
                self.buyvolume = 0
                self.sellvolume = 0
                self.first_secid = 0
                self.trig_packet_len = 0

        elif msg_tag == 'E' and self.avol:
            # if you only get subsequent E messages after setting avol=True, then youre good
            self.buyvolume += md.buyvolume
            self.sellvolume += md.sellvolume
            self.pkt_qty += md.buyvolume + md.sellvolume
            if md.buyvolume > 0 and self.trig_buyprice != md.buyprice:
                self.sweep = True 
                self.top_ask = md.asks[0].price
                self.top_bid = md.bids[0].price
                self.top_ask_q = md.asks[0].qty + md.buyvolume
                self.top_bid_q = md.bids[0].qty
                self.trig_buyprice = md.buyprice
                self.ask_par = md.asks[0].participants + 1 #add the traded with par back in
                self.bid_par = md.bids[0].participants 
                self.avol_qty = md.buyvolume + md.sellvolume #one of these will be zero
            elif md.sellvolume > 0 and self.trig_sellprice != md.sellprice:
                self.sweep = True 
                self.top_ask = md.asks[0].price
                self.top_bid = md.bids[0].price
                self.top_ask_q = md.asks[0].qty
                self.top_bid_q = md.bids[0].qty + md.sellvolume
                self.trig_sellprice = md.sellprice
                self.ask_par = md.asks[0].participants 
                self.bid_par = md.bids[0].participants + 1 #add the traded with par back in
                self.avol_qty = md.buyvolume + md.sellvolume #one of these will be zero
            elif not md.eop:
                self.avol_qty += md.buyvolume + md.sellvolume #one of these will be zero
            elif md.eop:
                self.avol_qty += 1 #we dont "know" last qty while predicting avol qty
                self.avol_last_pkt = True 
                self.buyvolume -= md.buyvolume -1 #-1 bc you only "know" the 1 lot traded
                self.sellvolume -= md.sellvolume -1
        elif self.avol:
            # if you get any other type of message after getting a D or E, its not an avol so reset vars
            self.sweep = False
            self.avol = False
            self.avol_qty = 0
            self.avol_last_pkt = False
            self.buyvolume = 0
            self.sellvolume = 0
            self.pkt_qty = 0
            self.first_secid = 0
            self.trig_packet_len = 0

        #trigger check
        if self.avol and md.eop and self.sweep:
            #trigger!  yay, made it through the whole pkt with only E msgs after first msg!
            if md.buyvolume > 0:
                #buy trigger
                buypct = <double>self.avol_qty / self.top_ask_q
                trig_imbal = 1.0 * (fire_md.bids[0].qty - fire_md.asks[0].qty) / (fire_md.bids[0].qty + fire_md.asks[0].qty)
                trig_par_imbal = 1.0 * (fire_md.bids[0].participants - fire_md.asks[0].participants) / (fire_md.bids[0].participants + fire_md.asks[0].participants)
                print 'BUY!', md.channel, md.packet_len, md.seqnum, 'avol_seqnum:', self.avol_seqnum, self.buyprice, md.buyvolume, buypct, self.avol_qty
                print md.time, 'trigger: ', self.trig_secid, md.seqnum, 'fireprice:', fire_md.bids[0].price, fire_md.asks[0].price, fire_md.seqnum
                context_str = (
                 'side: 1, tradepct: %f, avol_qty: %d, tradevolume: %d, leanpar0: %d, leanqty0: %d, leanprc0: %d, oppprc0: %d, oppqty0: %d, opppar0: %d, '
                 'trig_leanpar0: %d, trig_leanqty0: %d, trig_leanprc0: %d, trig_oppprc0: %d, trig_oppqty0: %d, trig_opppar0: %d, '
                 'imbal: %f, par_imbal: %f, trig_imbal: %f, trig_par_imbal: %f, first_secid: %d, msg_idx: %d, avol_msg_count: %d, packet_len: %d, msg_tag: %s, trig_secid: %d'
                % (buypct, self.pkt_qty, self.buyvolume, self.bid_par, self.top_bid_q, self.top_bid, self.top_ask, self.top_ask_q, self.ask_par,
                   fire_md.bids[0].participants, fire_md.bids[0].qty, fire_md.bids[0].price, 
                   fire_md.asks[0].price, fire_md.asks[0].qty, fire_md.asks[0].participants,
                   imbal, par_imbal, trig_imbal, trig_par_imbal, self.first_secid, msg_idx, 
                   self.avol_msg_count, self.trig_packet_len, chr(self.msg_tag), self.trig_secid
                   )
                )
                #to change your order data, change this line
                #client_orderid = self.place_ghostfak_order(md.time, self.avol_seqnum, self.fire_secid, self.buyprice, 1, 10, context_str)
                self.fire_time = md.time
                self.fire_avol_seqnum = self.avol_seqnum
                self.fire_price = self.buyprice
                self.fire_context_str = context_str 
                self.fire_side = 1
                self.update_after_avol = True
            else:
                #sell trigger
                sellpct = <double>self.avol_qty / self.top_bid_q
                trig_imbal = -1.0 * (fire_md.bids[0].qty - fire_md.asks[0].qty) / (fire_md.bids[0].qty + fire_md.asks[0].qty)
                trig_par_imbal = -1.0 * (fire_md.bids[0].participants - fire_md.asks[0].participants) / (fire_md.bids[0].participants + fire_md.asks[0].participants)
                print 'SELL!', md.channel, md.packet_len, md.seqnum, 'avol_seqnum:', self.avol_seqnum, self.sellprice, md.sellvolume, sellpct, self.avol_qty
                print md.time, 'trigger: ', self.trig_secid, md.seqnum, 'fireprice:', fire_md.bids[0].price, fire_md.asks[0].price, fire_md.seqnum

                context_str = (
                 'side: -1, tradepct: %f, avol_qty: %d, tradevolume: %d, leanpar0: %d, leanqty0: %d, leanprc0: %d, oppprc0: %d, oppqty0: %d, opppar0: %d, '
                 'trig_leanpar0: %d, trig_leanqty0: %d, trig_leanprc0: %d, trig_oppprc0: %d, trig_oppqty0: %d, trig_opppar0: %d, '
                 'imbal: %f, par_imbal: %f, trig_imbal: %f, trig_par_imbal: %f, first_secid: %d, msg_idx: %d, avol_msg_count: %d, packet_len: %d, msg_tag: %s, trig_secid: %d'
                % (sellpct, self.pkt_qty, self.sellvolume, self.ask_par, self.top_ask_q, self.top_ask, self.top_bid, self.top_bid_q, self.bid_par,
                   fire_md.asks[0].participants, fire_md.asks[0].qty, fire_md.asks[0].price, 
                   fire_md.bids[0].price, fire_md.bids[0].qty, fire_md.bids[0].participants,
                   imbal, par_imbal, trig_imbal, trig_par_imbal, self.first_secid, msg_idx, 
                   self.avol_msg_count, self.trig_packet_len, chr(self.msg_tag), self.trig_secid
                   )
                ) 
               #to change your order data, change this line
                #client_orderid = self.place_ghostfak_order(md.time, self.avol_seqnum, self.fire_secid, self.sellprice, 2, 10, context_str)
                self.fire_time = md.time
                self.fire_avol_seqnum = self.avol_seqnum
                self.fire_price = self.sellprice
                self.fire_context_str = context_str
                self.fire_side = 2
                self.update_after_avol = True

cdef class CVOLSlugger_ALL(StrategyInterface):
    """CVOLSlugger(secid)

    """
    cdef:
        VolumeAccumulator buytrades, selltrades, packet_cvol, buymsgs, sellmsgs
        MDOrderBook trig_md, fire_md
        bint reset_accumulators
        # readable in interpreter
        readonly:
            bint repeat_triggers, buy_trigger, sell_trigger
            int32_t buyprc, sellprc, buyqty, sellqty, buypar, sellpar
            int32_t bidprc, bidqty, bidpar, askprc, askqty, askpar
            int32_t buyvolume, sellvolume
            int32_t tradeqty_threshold, last_trade_price, report_num_levels
            uint16_t ref_trig_side, last_trade_side
            uint64_t md_channel, time_window, last_seqnum, mkt_seqnum
            double buypct, sellpct, tradepct_threshold, buyparpct, sellparpct
            double qty_imbalance_before, qty_imbalance_after, par_imbalance_before, par_imbalance_after
            int32_t ref_trig_price, time_delta
            str name
            # for placing orders
            uint64_t trig_seqnum, trig_secid, fire_secid

    def __init__(self, uint64_t secid, uint64_t fire_secid, uint64_t md_channel,
            uint64_t time_window=0, int32_t tradeqty_threshold=0,
            double tradepct_threshold=0, bint repeat_triggers=True, name='null'):
        # initialize using Taker's initialize method, which requires list of
        # secids
        self.initialize([secid])
        self.trig_secid = secid
        # CVOLSlugger relevant initializations
        self.md_channel = md_channel
        self.tradeqty_threshold = tradeqty_threshold
        self.tradepct_threshold = tradepct_threshold
        self.time_window = time_window
        self.repeat_triggers = repeat_triggers
        self.time_delta = 0
        # initialize VolumeAccumulator objects
        self.buytrades.Init(time_window)
        self.selltrades.Init(time_window)
        self.buymsgs.Init(time_window)
        self.sellmsgs.Init(time_window)
        # we need to aggregate qty based on price and side and send this
        # quantity to the appropriate trade accumulator; time_window=100ms to
        # enable accumulation of anything in a packet
        self.packet_cvol.Init(100 * ONE_MILLISECOND)
        self._clear_trades()
        self.reset_accumulators = False
        self.bidprc = 0
        self.bidqty = 0
        self.bidpar = 0
        self.askprc = 0
        self.askqty = 0
        self.askpar = 0
        self.sellvolume = 0
        self.buyvolume = 0
        self.mkt_seqnum = 0
        # initialize reference prices (will not check if repeat_triggers == True)
        self.ref_trig_price = 0
        self.ref_trig_side = 0
        self.buy_trigger = False
        self.sell_trigger = False
        self.trig_seqnum = 0
        self.fire_secid = fire_secid
        self.name = str(name)
        # keep track of last trade price and side
        self.last_trade_price = 0
        self.last_trade_side = 0
        self.last_seqnum = 0
        self.qty_imbalance_before = 0
        self.qty_imbalance_after = 0
        self.par_imbalance_before = 0
        self.par_imbalance_after = 0
        self.report_num_levels = 2

    def __repr__(self):
        return 'CVOLSlugger{name=%s}' % self.name

    cdef void _clear_trades(self):
        self.buytrades.Clear()
        self.buymsgs.Clear()
        self.selltrades.Clear()
        self.sellmsgs.Clear()
        self.buyprc = 0
        self.sellprc = 0
        self._clear_qtys()

    cdef void _clear_qtys(self) nogil:
        self.buyqty = 0
        self.buypar = 0
        self.buypct = 0
        self.buyparpct = 0
        self.sellqty = 0
        self.sellpar = 0
        self.sellpct = 0
        self.sellparpct = 0
        self.buy_trigger = False
        self.sell_trigger = False

    cdef void _save_market(self, MDOrderBook *md) nogil:
        self.bidprc = md.bids[0].price
        self.askprc = md.asks[0].price
        self.bidqty = md.bids[0].qty
        self.askqty = md.asks[0].qty
        self.bidpar = md.bids[0].participants
        self.askpar = md.asks[0].participants

    cdef void _compute_accqtys(self, uint64_t exch_time):
        cdef:
            int32_t cvol_qty = self.packet_cvol.Qty()
            int32_t cvol_msgs = self.packet_cvol.Size()
        # take what we have from packet_cvol and add it to either of our trade
        # accumulators
        if self.buyvolume > 0:
            self.buyprc = self.last_trade_price
            self.buyqty = self.buytrades.AddTrade(exch_time, cvol_qty)
            self.buypar = self.buymsgs.AddTrade(exch_time, cvol_msgs)
            self.sellqty = 0
        elif self.sellvolume > 0:
            self.sellprc = self.last_trade_price
            self.buyqty = 0
            self.sellqty = self.selltrades.AddTrade(exch_time, cvol_qty)
            self.sellpar = self.sellmsgs.AddTrade(exch_time, cvol_msgs)
        self._compute_accpcts()
        self.packet_cvol.Clear()

    cdef void _compute_accpcts(self):
        # buy side
        if self.buyqty > 0:
            if self.buyprc == self.askprc:
                self.buypct = (1.0 * self.buyqty) / (self.askqty + self.buyqty)
                self.buyparpct = (1.0 * self.buypar) / (self.askpar + self.buypar)
            else:
                self.buypct = 1.0
                self.buyparpct = 1.0
        else:
            self.buypct = 0.0
            self.buyparpct = 0.0
        # sell side
        if self.sellqty > 0:
            if self.sellprc == self.bidprc:
                self.sellpct = (1.0 * self.sellqty) / (self.bidqty + self.sellqty)
                self.sellparpct = (1.0 * self.sellpar) / (self.bidpar + self.sellpar)
            else:
                self.sellpct = 1.0
                self.sellparpct = 1.0
        else:
            self.sellpct = 0.0
            self.sellparpct = 0.0

    cdef void _check_trigger(self) nogil:
        self.buy_trigger = self.buyqty >= self.tradeqty_threshold and \
                           self.buypct >= self.tradepct_threshold
        self.sell_trigger = self.sellqty >= self.tradeqty_threshold and \
                            self.sellpct >= self.tradepct_threshold
        if self.buy_trigger:
            self.sellprc = 0
        if self.sell_trigger:
            self.buyprc = 0
        #self.trig_seqnum = self.last_seqnum

    cdef void _check_fire(self, int32_t qty=MINIMUM_QTY, bint place_order=True):
        cdef:
            MDOrderBook *md = &(self.trig_md)
            #MDOrderBook *fire_md = &(self.fire_md)   #new for context change
            bint has_trigger = False
            uint16_t side
            int32_t price
            str context
        self._compute_accqtys(md.exch_time)
        self._check_trigger()
        if self.buy_trigger:
            side = 1
            price = self.buyprc
            #fire_price = fire_md.asks[0].price ##buy the last known topix book
            has_trigger = True
            #grab quantities, and prices from md object
            top_ask_q = (md.asks[0].qty + self.buyqty)
            top_bid_q = md.bids[0].qty
            top_ask = md.asks[0].price
            top_bid = md.bids[0].price

            # the zeros are eop and avol specific
            context_str = (
                'pct : %s, acc_qty : %s, top_bid : %s, top_ask : %s, top_bid_qty : %s, top_ask_qty : %s, ' \
                'bid_par : %s, ask_par : %s, packet_len : %s, trade_qty : %s, target_bid : %s, target_ask : %s, ' \
                'target_bid_q1 : %s, target_bid_p1 : %s, target_bid_q0 : %s, target_bid_p0 : %s, ' \
                'target_ask_q0 : %s, target_ask_p0 : %s, target_ask_q1 : %s, target_ask_p1 : %s, side : buy, ' \
                'md_time : %s, exch_time : %s, tdiff : %s'
                % (
                        self.buypct, self.buyqty, top_bid, top_ask, top_bid_q, top_ask_q,
                        md.bids[0].participants, md.asks[0].participants, md.packet_len, self.buyvolume,
                        md.bids[0].price, md.asks[0].price,
                        md.bids[1].qty, md.bids[1].participants, md.bids[0].qty, md.bids[0].participants,
                        md.asks[0].qty, md.asks[0].participants, md.asks[1].qty, md.asks[1].participants,
                        md.time, md.exch_time, self.time_delta
                )
           )
        elif self.sell_trigger:
            side = 2
            price = self.sellprc
            #fire_price = fire_md.bids[0].price ##sell the last known topix book
            has_trigger = True
            #grab quantities and prices form md object
            top_ask_q = md.asks[0].qty
            top_bid_q = (md.bids[0].qty + self.sellqty)
            top_ask = md.asks[0].price
            top_bid = md.bids[0].price

            context_str = (
                'pct : %s, acc_qty : %s, top_bid : %s, top_ask : %s, top_bid_qty : %s, top_ask_qty : %s, ' \
                'bid_par : %s, ask_par : %s, packet_len : %s, trade_qty : %s, target_bid : %s, target_ask : %s, ' \
                'target_bid_q1 : %s, target_bid_p1 : %s, target_bid_q0 : %s, target_bid_p0 : %s, ' \
                'target_ask_q0 : %s, target_ask_p0 : %s, target_ask_q1 : %s, target_ask_p1 : %s, side : sell, ' \
                'md_time : %s, exch_time : %s, tdiff : %s'
                % (  
                        self.sellpct, self.sellqty, top_bid, top_ask, top_bid_q, top_ask_q,
                        md.bids[0].participants, md.asks[0].participants, md.packet_len, self.sellvolume,
                        md.bids[0].price, md.asks[0].price,
                        md.bids[1].qty, md.bids[1].participants, md.bids[0].qty, md.bids[0].participants,
                        md.asks[0].qty, md.asks[0].participants, md.asks[1].qty, md.asks[1].participants,
                        md.time, md.exch_time, self.time_delta
                )    
            ) 

        if has_trigger and (self.repeat_triggers or \
                (price != self.ref_trig_price and side != self.ref_trig_side)):
            if place_order:
                self.place_ghostfak_order(
                    md.time, self.trig_seqnum, self.fire_secid, price, side, qty, context_str
                )
            if not self.repeat_triggers:
                self.ref_trig_price = price
                self.ref_trig_side = side

    def evaluate_md(self, bint place_order=True):
        cdef:
            MDOrderBook *md = self.md_ptr
            #MDOrderBook *fire_md = &(self.fire_md) #new for context str
            VolumeAccumulator *buytrades = &(self.buytrades)
            VolumeAccumulator *selltrades = &(self.selltrades)
            VolumeAccumulator *packet_cvol = &(self.packet_cvol)
            bint new_market = False
            int32_t bidprc, bidqty, askprc, askqty
            int32_t buyprice, buyvolume, sellprice, sellvolume
            uint64_t time, exch_time, channel

        # check is_open flag to see if we should be placing orders
        if not md.is_open:
            return

        # parse market data using parent function
        time = md.time
        exch_time = md.exch_time
        channel = md.channel
        self.time_delta = time - exch_time

        #set the variable for the target contract, new for context str
        if md.secid == self.trig_secid:
            copy_mdorderbook(md, &(self.trig_md), num_levels=self.report_num_levels)
            
            #if md.secid == self.fire_secid:
            #    copy_mdorderbook(md, fire_md, num_levels=self.report_num_levels)
            #
            #    if self.fire_secid != self.trig_secid:
            #        return

        #elif md.secid == self.fire_secid:
        #    copy_mdorderbook(md, fire_md, num_levels=self.report_num_levels)
        #    return

        # reset accumulators
        if (not self.repeat_triggers and (self.buy_trigger or self.sell_trigger)) or self.reset_accumulators:
            self._clear_trades()
            self.reset_accumulators = False

        if channel != self.md_channel:
            return

        if not self.valid_md:
            # even though the market data is not relevant market data, we need
            # to still evaluate whether we are at the end of packet in order
            # to save what we know as aggregate buy and sell quantities.
            # Multiple secids can come in one packet, but we still want to
            # evaluate match events per packet for the secid that we care about
            if packet_cvol.Qty() > 0:
                self._check_fire(place_order)
            return

        bidprc = md.bids[0].price
        askprc = md.asks[0].price
        bidqty = md.bids[0].qty
        askqty = md.asks[0].qty

        buyprice = md.buyprice
        buyvolume = md.buyvolume
        sellprice = md.sellprice
        sellvolume = md.sellvolume

        self.buyvolume = buyvolume
        self.sellvolume = sellvolume

        new_market = bidprc != self.bidprc or askprc != self.askprc

        self.trig_seqnum = md.seqnum

        if buyvolume > 0: 
            self._save_market(md)
            # evaluate buys
            if self.last_trade_side != 1 or self.last_trade_price != buyprice:
                self._clear_trades()
                self.last_trade_side = 1
                self.last_trade_price = buyprice
                packet_cvol.AddTrade(exch_time, buyvolume)
                self._check_fire(place_order)
            elif md.eop:
                packet_cvol.AddTrade(exch_time, buyvolume)
                self.last_seqnum = md.seqnum
                self._check_fire(place_order)
            else:
                packet_cvol.AddTrade(exch_time, buyvolume)
                self._check_fire(place_order)
            #self.last_trade_side = 1
            #self.last_trade_price = buyprice

        elif sellvolume > 0: 
            self._save_market(md)
            # evaluate sells
            if self.last_trade_side != 2 or self.last_trade_price != sellprice:
                self._clear_trades()
                self.last_trade_side = 2
                self.last_trade_price = sellprice
                packet_cvol.AddTrade(exch_time, sellvolume)
                self._check_fire(place_order)
            elif md.eop:
                packet_cvol.AddTrade(exch_time, sellvolume)
                self.last_seqnum = md.seqnum
                self._check_fire(place_order)
            else:
                packet_cvol.AddTrade(exch_time, sellvolume)
                self._check_fire(place_order)
            #self.last_trade_side = 2
            #self.last_trade_price = sellprice

        elif packet_cvol.Qty() > 0: 
            self._check_fire(place_order)
            self._save_market(md)

        else:
            self._save_market(md)

        self.last_seqnum = md.seqnum

        # should we reset accumulators on next market data tick?
        if new_market:
            self.ref_trig_side = 0
            self.ref_trig_price = 0
            self.mkt_seqnum += 1 
        self.reset_accumulators = self.reset_accumulators or new_market
