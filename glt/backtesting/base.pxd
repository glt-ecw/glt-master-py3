from libc.stdint cimport *
from glt.md.standard cimport MDSource
from glt.backtesting.match_simulation cimport OrderQueuer, SimulatedOrderAck


cdef int32_t find_order_sign(uint16_t side) nogil


cdef class StrategyInterface(MDSource):
    cdef:
        OrderQueuer oq
        int64_t me_id
        bint me_assigned

        bint _has_acks(self) nogil
        void _remove_ack(self) nogil
        SimulatedOrderAck _top_ack(self) nogil
