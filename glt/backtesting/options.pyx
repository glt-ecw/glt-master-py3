# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True

from glt.backtesting.base cimport StrategyInterface
from glt.md.standard cimport MDOrderBook
from glt.modeling.filter_options cimport *
from glt.publishing.modelpub cimport OPMSharedArray
from numpy import nan


cdef class VolTest(StrategyInterface):
    cdef:
        OptionsFilterModel model 
        str name
        logger

    def __init__(self, shortname, expiry, respline_interval, px_tick_pair, spot_tick_incr, max_opt_width, 
            max_fut_width, ir, opt_min_qty_par, fut_min_qty_par, pause_window, 
            t_offset, min_bid, use_roll, underlying_secid=None, exch_time=True, 
            ts_start=None, name='None', logger='None'): 

        self.model = OptionsFilterModel(shortname, expiry, respline_interval, px_tick_pair, spot_tick_incr, max_opt_width,
                    max_fut_width, ir, opt_min_qty_par, fut_min_qty_par, pause_window, 
                    t_offset, min_bid, use_roll, underlying_secid, exch_time, 
                    ts_start, name)
        sec_list = self.model.securities.keys()
        self.initialize(sec_list)
        self.logger = logger
        self.logger.info('VolTest is done initializing')

    def __repr__(self):
        return '8==D %s' % self.name

    def evaluate_md(self):
        cdef:
            MDOrderBook *md = self.md_ptr
        

        # set tte in model if its not set (done on first book)
        if not self.model.tte_flag:
            self.model.set_tte(md)
        # necessary step im told
        if not self.valid_md or self.md_ptr.asks[0].qty == 0:
            return 

        self.model.evaluate_md(md)
        if self.model.snapshot:

            #calc top of book vols
            self.model.calculate_top_vols()

            # build vol part of the str
            vol_raw = ''
            vol_sm = ''
            vol_dy = ''
            extra = ''
            extra2 = ''
            extra3 = ''
            extra4 = ''
            extra5 = ''
            # there must be a better way to do this...
            for i in xrange(self.model.n_strikes):
                for sec in self.model.securities.itervalues():
                    if sec['idx'] == i and sec['type'] == 'opt' and sec['filter'].otm:
                        vol_raw += ' %s=%s,' % (sec['filter'].strike, sec['filter'].vol['mid'])
                        vol_sm += ' %s=%s,' % (sec['filter'].strike, sec['filter'].vol['smooth'])
                        vol_dy += ' %s=%.8f,' % (sec['filter'].strike, sec['filter'].vol['dy'])
                        extra += ' %s=%s,' % (sec['filter'].strike, sec['filter'].vol['bid'])
                        extra2 += ' %s=%s,' % (sec['filter'].strike, sec['filter'].vol['ask'])
                        #extra3 += ' %s=%s,' % (sec['filter'].strike, sec['filter'].md['mid'])
                        extra3 += ' %s=%s,' % (sec['filter'].strike, sec['filter'].vega)
                        extra4 += ' %s=%s,' % (sec['filter'].strike, sec['filter'].top_vol['bid'])
                        extra5 += ' %s=%s,' % (sec['filter'].strike, sec['filter'].top_vol['ask'])
                        break
            vol_raw = vol_raw.rstrip(',')
            vol_sm = vol_sm.rstrip(',')
            vol_dy = vol_dy.rstrip(',')
            extra = extra.rstrip(',')
            extra2 = extra2.rstrip(',')
            extra3 = extra3.rstrip(',')
            # header and footer
            raw_output = 'RawVol{ts=%s, tte=%.8f, spot=%s, roll=%.2f,' + vol_raw + '}'
            sm_output = 'SmoothVol{ts=%s, tte=%.8f, spot=%s, roll=%.2f,' + vol_sm + '}'
            dy_output = 'DerivVol{ts=%s, tte=%.8f, spot=%s, roll=%.2f,' + vol_dy + '}'
            extra = 'BidVol{ts=%s, tte=%.8f, spot=%s, roll=%.2f,' + extra + '}'
            extra2 = 'AskVol{ts=%s, tte=%.8f, spot=%s, roll=%.2f,' + extra2 + '}'
            extra3 = 'PxVol{ts=%s, tte=%.8f, spot=%s, roll=%.2f,' + extra3 + '}'
            extra4 = 'BpxVol{ts=%s, tte=%.8f, spot=%s, roll=%.2f,' + extra4 + '}'
            extra5 = 'ApxVol{ts=%s, tte=%.8f, spot=%s, roll=%.2f,' + extra5 + '}'

            print raw_output % (int(self.model.ts), self.model.tte, self.model.securities[self.model.fut_secid]['filter'].md['mid'], self.model.roll)
            print sm_output % (int(self.model.ts), self.model.tte, self.model.securities[self.model.fut_secid]['filter'].md['mid'], self.model.roll)
            print dy_output % (int(self.model.ts), self.model.tte, self.model.securities[self.model.fut_secid]['filter'].md['mid'], self.model.roll)
            print extra % (int(self.model.ts), self.model.tte, self.model.securities[self.model.fut_secid]['filter'].md['mid'], self.model.roll)
            print extra2 % (int(self.model.ts), self.model.tte, self.model.securities[self.model.fut_secid]['filter'].md['mid'], self.model.roll)
            print extra3 % (int(self.model.ts), self.model.tte, self.model.securities[self.model.fut_secid]['filter'].md['mid'], self.model.roll)
            print extra4 % (int(self.model.ts), self.model.tte, self.model.securities[self.model.fut_secid]['filter'].top['mid'], self.model.roll)
            print extra5 % (int(self.model.ts), self.model.tte, self.model.securities[self.model.fut_secid]['filter'].top['mid'], self.model.roll)


cdef class VolModel(StrategyInterface):
    cdef:
        OptionsFilterModel model
        OPMSharedArray shared_arr 
        str name
        logger
        count
        readonly:
            double yte
            double interest_rate
            uint64_t n_strikes
            

    def __init__(self, shortname, expiry, respline_interval, px_tick_pair, spot_tick_incr, max_opt_width, 
            max_fut_width, ir, opt_min_qty_par, fut_min_qty_par, pause_window, 
            t_offset, min_bid, use_roll, underlying_secid=None, exch_time=True, 
            ts_start=None, name='None', logger='None'): 

        self.model = OptionsFilterModel(shortname, expiry, respline_interval, px_tick_pair, spot_tick_incr, max_opt_width,
                    max_fut_width, ir, opt_min_qty_par, fut_min_qty_par, pause_window, 
                    t_offset, min_bid, use_roll, underlying_secid, exch_time, 
                    ts_start, name, logger)
        # get the secids from the model in order to listen to MD for them from MDSource
        sec_list = self.model.securities.keys()
        self.initialize(sec_list)
        #vars for karls modelpub
        self.yte = self.model.tte
        self.interest_rate = self.model.ir
        self.n_strikes = self.model.n_strikes
        self.name = str(name)
        self.logger = logger
        self.count = 0
        #self.set_sh_array()
        self.logger.info('VolModel (%s) is done initializing' % self.name)
        

    def __repr__(self):
        return '8==D %s' % self.name

    def set_sh_array(self):
        #fill up the shared array
        for sec in self.model.securities.itervalues():
            if sec['type'] == 'opt' and sec['filter'].otm:
                if sec['filter'].cp == 1:
                    secid = sec['filter'].secid
                else:
                    secid = self.model.put_to_call[sec['filter'].secid]
                vp = sec['filter'].vol['dy']
                vol = sec['filter'].vol['smooth']
                m = sec['filter'].m
                vega = sec['filter'].vega
                self.shared_arr.set_black_scholes_at(secid, vol, vp, m, vega) 
    

    def evaluate_md(self):
        cdef:
            MDOrderBook *md = self.md_ptr

        # set tte in model if its not set (done on first book)
        if not self.model.tte_flag:
            self.model.set_tte(md)
        # necessary step im told
        if not self.valid_md or self.md_ptr.asks[0].qty == 0:
            return 

        self.model.evaluate_md(md)
        if self.model.snapshot:

            # put values in sh array
            self.set_sh_array()

            # RawVol{ts= , spot= , roll= , secid/strike= }
            # build vol part of the str
            vol_raw = ''
            vol_sm = ''
            vol_dy = ''
            extra = ''
            extra2 = ''
            extra3 = ''
            # there must be a better way to do this...
            for i in xrange(self.model.n_strikes):
                for sec in self.model.securities.itervalues():
                    if sec['idx'] == i and sec['type'] == 'opt' and sec['filter'].otm:
                        vol_raw += ' %s=%s,' % (sec['filter'].strike, sec['filter'].vol['mid'])
                        vol_sm += ' %s=%s,' % (sec['filter'].strike, sec['filter'].vol['smooth'])
                        vol_dy += ' %s=%.8f,' % (sec['filter'].strike, sec['filter'].vol['dy'])
                        extra += ' %s=%s,' % (sec['filter'].strike, sec['filter'].vol['bid'])
                        extra2 += ' %s=%s,' % (sec['filter'].strike, sec['filter'].vol['ask'])
                        extra3 += ' %s=%s,' % (sec['filter'].strike, sec['filter'].md['mid'])
                        break
            vol_raw = vol_raw.rstrip(',')
            vol_sm = vol_sm.rstrip(',')
            vol_dy = vol_dy.rstrip(',')
            extra = extra.rstrip(',')
            extra2 = extra2.rstrip(',')
            extra3 = extra3.rstrip(',')
            # header and footer
            raw_output = '%sRawVol{ts=%s, spot=%s, roll=%.2f,' + vol_raw + '}'
            sm_output = '%sSmoothVol{ts=%s, spot=%s, roll=%.2f,' + vol_sm + '}'
            dy_output = '%sDerivVol{ts=%s, spot=%s, roll=%.2f,' + vol_dy + '}'
            extra = '%sBidVol{ts=%s, spot=%s, roll=%.2f,' + extra + '}'
            extra2 = '%sAskVol{ts=%s, spot=%s, roll=%.2f,' + extra2 + '}'
            extra3 = '%sPxVol{ts=%s, spot=%s, roll=%.2f,' + extra3 + '}'


            #print dy_output % (self.model.opt_contract, int(self.model.ts), self.model.securities[self.model.fut_secid]['filter'].md['mid'], self.model.roll)
            #print raw_output % (self.model.opt_contract, int(self.model.ts), self.model.securities[self.model.fut_secid]['filter'].md['mid'], self.model.roll)
            #print sm_output % (self.model.opt_contract, int(self.model.ts), self.model.securities[self.model.fut_secid]['filter'].md['mid'], self.model.roll)
            #print dy_output % (self.model.opt_contract, int(self.model.ts), self.model.securities[self.model.fut_secid]['filter'].md['mid'], self.model.roll)
            #print extra % (self.model.opt_contract, int(self.model.ts), self.model.securities[self.model.fut_secid]['filter'].md['mid'], self.model.roll)
            #print extra2 % (self.model.opt_contract, int(self.model.ts), self.model.securities[self.model.fut_secid]['filter'].md['mid'], self.model.roll)
            #print extra3 % (self.model.opt_contract, int(self.model.ts), self.model.securities[self.model.fut_secid]['filter'].md['mid'], self.model.roll)

    def update_parameters(self, param_dict):
        set_oldkeys = set(self.model.securities.keys())
        self.model.update_parameters(**param_dict)
        set_newkeys = set(self.model.securities.keys())
        if set_oldkeys != set_newkeys:
            # if any secids have changed re init MDSource
            sec_list = self.model.securities.keys()
            self.initialize(sec_list)

    def log_values(self):
        # RawVol{ts= , spot= , roll= , secid/strike= }
        # build vol part of the str
        vol_raw = ''
        vol_sm = ''
        vol_dy = ''
        extra = ''
        extra2 = ''
        extra3 = ''
        # there must be a better way to do this...
        for i in xrange(self.model.n_strikes):
            for sec in self.model.securities.itervalues():
                if sec['idx'] == i and sec['type'] == 'opt' and sec['filter'].otm:
                    vol_raw += ' %s=%s,' % (sec['filter'].strike, sec['filter'].vol['mid'])
                    vol_sm += ' %s=%s,' % (sec['filter'].strike, sec['filter'].vol['smooth'])
                    vol_dy += ' %s=%.8f,' % (sec['filter'].strike, sec['filter'].vol['dy'])
                    extra += ' %s=%s,' % (sec['filter'].strike, sec['filter'].vol['bid'])
                    extra2 += ' %s=%s,' % (sec['filter'].strike, sec['filter'].vol['ask'])
                    extra3 += ' %s=%s,' % (sec['filter'].strike, sec['filter'].md['mid'])
                    break
        vol_raw = vol_raw.rstrip(',')
        vol_sm = vol_sm.rstrip(',')
        vol_dy = vol_dy.rstrip(',')
        extra = extra.rstrip(',')
        extra2 = extra2.rstrip(',')
        extra3 = extra3.rstrip(',')
        # header and footer
        raw_output = '%sRawVol{ts=%s, spot=%s, roll=%.2f,' + vol_raw + '}'
        sm_output = '%sSmoothVol{ts=%s, spot=%s, roll=%.2f,' + vol_sm + '}'
        dy_output = '%sDerivVol{ts=%s, spot=%s, roll=%.2f,' + vol_dy + '}'
        extra = '%sBidVol{ts=%s, spot=%s, roll=%.2f,' + extra + '}'
        extra2 = '%sAskVol{ts=%s, spot=%s, roll=%.2f,' + extra2 + '}'
        extra3 = '%sPxVol{ts=%s, spot=%s, roll=%.2f,' + extra3 + '}'

        self.logger.debug(raw_output % (self.model.opt_contract, int(self.model.ts), self.model.securities[self.model.fut_secid]['filter'].md['mid'], self.model.roll))
        self.logger.debug(sm_output % (self.model.opt_contract, int(self.model.ts), self.model.securities[self.model.fut_secid]['filter'].md['mid'], self.model.roll))
        self.logger.debug(dy_output % (self.model.opt_contract, int(self.model.ts), self.model.securities[self.model.fut_secid]['filter'].md['mid'], self.model.roll))
    

    def dump_data(self):
        return self.model.dump_data()

    def option_secids(self):
        return self.model.call_secids

    def future_secid(self):
        return self.model.fut_secid

    def model_name(self):
        return self.name

    def ref_shared_array(self, OPMSharedArray shared_arr):
        self.shared_arr = shared_arr

    def spot(self):
        # return model.spot, which is the reference for black scholes vols
        return self.model.spot

    def roll(self):
        return self.model.roll

    def tdate(self):
        return self.model.tdate

    def put_to_call(self):
        return self.model.put_to_call

    def secid_to_strike(self):
        return self.model.secid_to_strike

    def secid_to_vega(self):
        return self.model.secid_to_vega

    def seqnum(self):
        return self.model.pub_count

    def secid_to_strike(self):
        return self.model.secid_to_strike

    def secid_to_moneyness(self):
        return self.model.secid_to_m

