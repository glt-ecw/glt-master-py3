from libc.stdint cimport int16_t, int32_t, int64_t, uint16_t, uint64_t
from glt.md.book_building cimport MDOrderBook

cdef extern from "match_simulation.h" nogil:
    cdef cppclass SimulatedOrderAck:
        const bint active
        const uint64_t orderid, client_orderid, origin_id, num_fills
        const int64_t remaining_qty, last_position
        SimulatedOrderAck()
    cdef cppclass OrderQueuer:
        OrderQueuer()
        uint64_t PrepareLimitOrder(int64_t secid, const char *strategy_name, uint64_t trigger_time,
                               uint64_t trigger_seqnum, int32_t price,
                               uint16_t side, int32_t qty, const char *context)
        uint64_t PrepareFAKOrder(int64_t secid, const char *strategy_name, uint64_t trigger_time,
                             uint64_t trigger_seqnum, int32_t price,
                             uint16_t side, int32_t qty, const char *context)
        uint64_t PrepareSprintOrder(int64_t secid, const char *strategy_name, uint64_t trigger_time,
                             uint64_t trigger_seqnum, int32_t price,
                             uint16_t side, int32_t qty, const char *context, double bypass_market_pct)
        uint64_t PrepareGhostFAKOrder(int64_t secid, const char *strategy_name, uint64_t trigger_time,
                                  uint64_t trigger_seqnum, int32_t price,
                                  uint16_t side, int32_t qty, const char *context)
        uint64_t PrepareGhostLimitOrder(int64_t secid, const char *strategy_name, uint64_t trigger_time,
                                         uint64_t trigger_seqnum, int32_t price,
                                         uint16_t side, int32_t qty, const char *context)
        uint64_t PrepareGhostSprintOrder(int64_t secid, const char *strategy_name, uint64_t trigger_time,
                                         uint64_t trigger_seqnum, int32_t price,
                                         uint16_t side, int32_t qty, const char *context, double bypass_market_pct)
        bint HasOrders() const
        void ClearOrders() const
        bint HasAcks() const
        SimulatedOrderAck TopAck()
        void RemoveAck()
        uint64_t CurrentClientOrderID() const
    cdef cppclass SimulatedMatchingEngine:
        SimulatedMatchingEngine()
        void Init(uint64_t me_id, uint64_t latency, bint use_exch_time, uint64_t evaluated_timedelta)
        void AddSecurity(uint64_t secid)
        void ReadMD(const MDOrderBook * const md_ptr)
        void EnqueueOrdersFrom(OrderQueuer *oq)
        bint ValidSecID(uint64_t secid) const
