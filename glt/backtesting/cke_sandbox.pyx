# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from cython.operator cimport dereference, preincrement
from libc.stdint cimport *
from libcpp.unordered_map cimport unordered_map as unordered_map_
from glt.md.standard cimport BookLevel, MDOrderBook, VolumeAccumulator
from glt.backtesting.base cimport StrategyInterface, SimulatedOrderAck


cdef uint16_t SIDE_BID = 1
cdef uint16_t SIDE_ASK = 2


cdef class OrderStatus:
    cdef readonly:
        uint64_t client_orderid
        uint64_t place_time
        uint16_t side
        int32_t price, remaining
        str context
        bint existing
        int16_t status

    def __init__(self, uint64_t client_orderid, uint64_t time, int32_t price, uint16_t side, int32_t qty, str context):
        self.client_orderid = client_orderid
        self.place_time = time
        self.price = price
        self.side = side
        self.remaining = qty
        self.context = context
        self.existing = True
        self.status = 1

    def __repr__(self):
        return (
            'OrderStatus{id=%d, time=%d, side=%d, %d @ %d, context=%s}'
        ) % (self.client_orderid, self.place_time, self.side, self.remaining, self.price, self.context)

    def filled_qty(self, new_qty):
        ## new_qty is ack.remaining_qty
        cdef int32_t filled_qty = self.remaining - new_qty
        self.remaining = new_qty
        self.existing = new_qty > 0
        return filled_qty


cdef class CKEKospiFuturesSweepChaser(StrategyInterface):
    cdef:
        readonly:
            str name
            uint64_t futures_secid
            int32_t ordersize
            int32_t min_tradevolume
            list original_orders
            dict cancels
            dict existing_orders
            int32_t last_bidprc, last_askprc, last_bidqty, last_askqty

    def __init__(self, uint64_t futures_secid, name, ordersize, min_tradevolume):
        self.futures_secid = futures_secid
        self.name = str(name)
        self.ordersize = ordersize
        self.min_tradevolume = min_tradevolume
        self.initialize([futures_secid])
        self.original_orders = []
        self.cancels = {}
        self.existing_orders = {}
        self.last_bidprc=0
        self.last_askprc=0
        self.last_bidqty=0 
        self.last_askqty=0

    def __repr__(self):
        return (
            '%s{%s, fut=%d, ordersize=%d, min_trade=%d, name=%s}'
        ) % (
            self.__class__.__name__, hex(id(self)), 
            self.futures_secid, self.ordersize, self.min_tradevolume, self.name
        )

    cdef uint64_t _place_new_order(self, MDOrderBook *md, uint64_t secid, 
            int32_t price, uint16_t side, int32_t qty, str context, 
            dict status_map):
        cdef:
            uint64_t client_orderid

        client_orderid = self.place_limit_order(
            md.time, md.seqnum, secid, price, side, qty, context
        )
        status_map[client_orderid] = OrderStatus(client_orderid, md.time, price, side, qty, context)

        return client_orderid


    cdef void _save_market(self, MDOrderBook *md):
        self.last_bidprc = md.bids[0].price
        self.last_askprc = md.asks[0].price
        self.last_bidqty = md.bids[0].qty
        self.last_askqty = md.asks[0].qty

 
    cdef uint64_t sim_cancel_order(self, uint64_t cancel_id, MDOrderBook *md, uint64_t secid,
            int32_t price, uint16_t side, int32_t qty, str context, dict status_map):

        cdef: 
            uint64_t client_orderid

        client_orderid = self.place_fak_order(
            md.time, md.seqnum, secid, price, side, qty, context
        )        

        #status_map[client_orderid] = OrderStatus(client_orderid, md.time, price, side, qty, context)

        return client_orderid

    def evaluate_md(self):
        cdef:
            MDOrderBook *md = self.md_ptr
            SimulatedOrderAck ack
            uint64_t client_orderid
            uint64_t time, place_time, seqnum, secid
            int32_t hedgeprice, hedgeqty, hedgeside, fillprice
            int32_t bidprc, bidqty, askprc, askqty
            #int32_t prev_bidprc=0, prev_bidqty=0, prev_askprc=0, prev_askqty=0
            int32_t buyprice, buyvolume, sellprice, sellvolume, tradeprice, tradeqty
            int32_t orderprice, orderqty
            int16_t sweepside
            int16_t ticksize = 500
            int16_t i = 0
            float tdiff = 0.0
            int32_t ordersize, status
            bint bidlevel_cleared=False, asklevel_cleared=False
            OrderStatus sweep_trade

        if not self.valid_md:
            return

        if md.bids[0].qty == 0 or md.asks[0].qty == 0:
            return
        
        time = md.time
        seqnum = md.seqnum
        secid = md.secid

        # let's evaluate some acks!
        while self._has_acks():
            ack = self._top_ack()
            #print 'remaining qty=%d for client id: %d' %(ack.remaining_qty, ack.client_orderid)
            #print ack.client_orderid, ack.remaining_qty, ack.client_orderid in self.existing_orders

            # TODO: After receiving an ack for your order, you will need to cancel it
            # one second after placing it. The OrderStatus object should be helpful
            # in determining whether an ack is an accept or fill using the
            # filled_qty(...) method

            if ack.client_orderid in self.existing_orders and self.existing_orders[ack.client_orderid].existing:
                place_time = self.existing_orders[ack.client_orderid].place_time
                status = self.existing_orders[ack.client_orderid].status
                tdiff = (time - place_time)/1.0e9
                fillqty = self.existing_orders[ack.client_orderid].filled_qty(ack.remaining_qty)
                
                if ack.client_orderid in self.cancels:
                    print "canceled client order id: %d, with orderid: %d" %(self.cancels[ack.client_orderid], ack.client_orderid)
                    self.existing_orders.pop(self.cancels[ack.client_orderid])

                elif fillqty == 0:
                    print 'this is an accept for order: %d, remaining_qty=%d, placed at=%d, seqnum=%d'%(ack.client_orderid, ack.remaining_qty, place_time, seqnum)

                else:
                    print 'this is a fill for order:%d, remaining_qty=%d, tdiff=%f, seqnum=%d'%(ack.client_orderid, ack.remaining_qty, tdiff, seqnum)
                    
                    ## only hedge original order
                    print self.original_orders
                    if ack.client_orderid in self.original_orders:
                        hedgeside = 1 if self.existing_orders[ack.client_orderid].side == 2 else 2
                        fillprice = self.existing_orders[ack.client_orderid].price
                        hedgeprice = fillprice - ticksize if hedgeside == 1 else fillprice + ticksize
                        hedgeqty = fillqty

                        context_str = ( 
                            'placed hedge order for orderid=%d at time: %s, seqnum: %s'
                            % (ack.client_orderid, time, seqnum)
                        )
                        
                        client_orderid = self._place_new_order(md, secid, hedgeprice, hedgeside, hedgeqty, context_str, self.existing_orders)                
                        print ('placed hedge order for orderid=%d for price:%d, qty:%d, side:%d, at time: %s, seqnum: %s' 
                                % (ack.client_orderid, hedgeprice, hedgeqty, hedgeside, time, seqnum)
                            )
                    else:
                        print "this id: %d, is a fill for a hedge. don't hedge again!"%ack.client_orderid

            self._remove_ack()
        
        ## cancel order after 1sec
        for client_orderid in self.existing_orders.keys():
            place_time = self.existing_orders[client_orderid].place_time
            if time - place_time > 1e9 and client_orderid in self.original_orders and self.existing_orders[client_orderid].existing == 1:
                print "1 sec has passed, canceling this order=%d at time=%d, placed at time=%d, seqnum=%d." %(client_orderid, time, place_time, seqnum)
                context_str = (
                    'placed cancel order for orderid=%d at time: %s, seqnum: %s'
                    %  (client_orderid, time, seqnum)

                )
                #cancel_id = self.sim_cancel_order(client_orderid, md, secid, orderprice, sweepside, orderqty, context_str, existing_orders)
                #self.cancels.append(cancel_id)
                #self.existing_orders[client_orderid].filled_qty(0)
                #print self.existing_orders[client_orderid].existing 
                #print "removed client order id: %d" %client_orderid

            

        # TODO: Now evaluate market data. Look for the MDOrderBook object in 
        # glt/md/book_building.pxd to see all the fields that exist.
        # After you have detected a sweep, you will want to place an order
        # to see if you get filled at a favorable price. After getting fills,
        # scalp the order. Otherwise, cancel the remaining quantity if you haven't
        # gotten filled in one second. FYI: one second := 1000000000 nanoseconds,
        # which is the units of md.time and md.exch_time
       
        bidprc = md.bids[0].price
        bidqty = md.bids[0].qty
        askprc = md.asks[0].price
        askqty = md.asks[0].qty
        
        buyprice = md.buyprice
        buyvolume = md.buyvolume
        sellprice = md.sellprice
        sellvolume = md.sellvolume
        
        bidlevel_cleared = (buyvolume > self.min_tradevolume) & (self.last_askqty==buyvolume) & (self.last_askprc == buyprice)        
        asklevel_cleared = (sellvolume > self.min_tradevolume) & (self.last_bidqty==sellvolume) & (self.last_bidprc == sellprice)
        
        if bidlevel_cleared or asklevel_cleared:
            sweepside = 1 if buyvolume else 2
            tradeprice = buyprice if buyvolume else sellprice
            tradeqty = buyvolume if buyvolume else sellvolume
            orderqty = self.ordersize
            orderprice = tradeprice + ticksize if buyvolume else tradeprice - ticksize

            context_str = (
                 'placed limit order for orderid=0 at time: %s, seqnum: %s'
                % (time, seqnum)
            )
            print 'placing limit order: price=%d, side=%d, placed at=%d, seqnum=%d, qty_traded=%d'%(orderprice, sweepside, time, seqnum, tradeqty)
            client_orderid = self._place_new_order(md, secid, orderprice, sweepside, orderqty, context_str, self.existing_orders)
            self.original_orders.append(client_orderid)
        self._save_market(md)
        return

