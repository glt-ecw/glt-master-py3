# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from cython.operator cimport dereference, preincrement
from libc.stdint cimport *
from libcpp.unordered_map cimport unordered_map as unordered_map_
from glt.md.standard cimport BookLevel, MDOrderBook, VolumeAccumulator
from glt.backtesting.base cimport StrategyInterface, SimulatedOrderAck

cdef uint16_t SIDE_BID = 1
cdef uint16_t SIDE_ASK = 2
cdef uint64_t MIN_VOLUME = 50
cdef uint16_t MIN_LEVELS = 1
cdef uint16_t MAX_LEVELS = 1
cdef uint16_t SNIPER_QTY = 20
cdef double MIN_LEAN_PCT = .35
cdef bint QTY_SNIPER = True
cdef bint SWEEP_PARAMS = True
cdef bint SWEEP_CHASER = False
cdef bint SWEEP_SNIPER = False

sweep_attributes = SweepDetector(MIN_VOLUME, MIN_LEVELS, MAX_LEVELS, SWEEP_PARAMS)
last_book = RetrieveLastBookUpdate()
manager = OrderManager()

cdef double book_size(double last_bid_qty, double last_offer_qty, double buyqty, double sellqty, short int side):
    if side == 1:
        new_offer_qty = last_offer_qty - buyqty
        new_book_qty = new_offer_qty + last_bid_qty
        return new_offer_qty / new_book_qty
    elif side == 2:
        new_bid_qty = last_bid_qty - sellqty
        new_book_qty = new_bid_qty + last_offer_qty
        return new_bid_qty / new_book_qty
    
cdef class RetrieveLastBookUpdate:
    cdef:
        list book_tracker
        list book

    def __init__(self):
        #ugly a.f - fix me
        self.book_tracker = [[0,0,0,0],[0,0,0,0]]

    def get(self, book):
        if not len(self.book_tracker):
            self.book_tracker.append(book)
            return self.book_tracker
        elif len(self.book_tracker):
            self.book_tracker.pop(0)
            self.book_tracker.append(book)
            return self.book_tracker[0]

cdef class SweepDetector:
    cdef:
        short int sweep_counter
        short int sweep_volume
        short int volume
        short int counter
        bint sweep_params
        bint sweep_toggle
        long seqnum 
        long last_seqnum
        readonly:
            short int min_volume
            short int min_levels
            short int max_levels

    def __init__(self, short int min_volume, short int min_levels, short int max_levels, bint sweep_params):        
        self.min_volume = min_volume
        self.min_levels = min_levels
        self.max_levels = max_levels
        self.sweep_params = sweep_params
        self.sweep_toggle = False
        self.sweep_counter = 0
        self.sweep_volume = 0
        self.last_seqnum = 0

    def add(self, counter, volume, update):
        if update == True:
            self.sweep_counter += counter
            self.sweep_volume += volume
        elif update == False:
            self.sweep_counter = 0
            self.sweep_volume = 0
            self.last_seqnum = 0
    
    def get_sweep(self):
        if self.sweep_params == True:
            if self.sweep_volume > self.min_volume:
                if abs(self.sweep_counter) >= self.min_levels:
                    if abs(self.sweep_counter) <= self.max_levels:
                        return self.last_seqnum, self.sweep_counter, self.sweep_volume
        else:
            return self.last_seqnum, self.sweep_counter, self.sweep_volume

    def return_toggle(self, toggle):
        if toggle == True:
            self.sweep_toggle = True
        elif toggle == False:
            self.sweep_toggle = False

    def update_recorder(self, volume_event, prev_book):
        buyqty, sellqty, seqnum = volume_event
        
        if self.sweep_toggle == True:
            self.add(0, 0, False)
            self.return_toggle(False)

        if buyqty >= prev_book[3] or sellqty >= prev_book[1]:             
            if self.sweep_counter == 0:
                if buyqty > 0:
                    self.last_seqnum = seqnum
                    self.add(1, buyqty, True)
                elif sellqty > 0:
                    self.last_seqnum = seqnum
                    self.add(-1, sellqty, True)
           
            elif self.sweep_counter > 0:
                if (seqnum - self.last_seqnum) == 1:
                    if buyqty > 0:
                        self.add(1, buyqty, True)
                        self.last_seqnum = seqnum
                    elif sellqty > 0:
                        self.return_toggle(True)
                        return self.get_sweep()
                else:
                    self.return_toggle(True)
                    return self.get_sweep()

            elif self.sweep_counter < 0:
                if (seqnum - self.last_seqnum) == 1:
                    if sellqty > 0:
                        self.add(-1, sellqty, True)
                        self.last_seqnum = seqnum
                    elif buyqty > 0:
                        self.return_toggle(True)
                        return self.get_sweep()
                else:
                    self.return_toggle(True)
                    return self.get_sweep()
        else:
            self.return_toggle(True)
            if self.sweep_counter > 0:
                return self.get_sweep()
 
cdef class OrderManager:
    cdef:
        dict open_orders    
        dict filled_orders
        dict canceled_orders
    
    def __init__(self):
        self.open_orders = {}
        self.filled_orders = {}
        self.canceled_orders = {}
        
    def add(self, client_orderid, order_info):
        if client_orderid in self.canceled_orders:
            print 'Ignoring order=%r, order was cancelled' % client_orderid
        else:
            self.open_orders[client_orderid] = order_info
            return
    
    def update_remqty(self, client_orderid, rem_qty):
        if client_orderid in self.canceled_orders:
            return
        else:
            self.open_orders[client_orderid]['rem_qty'] = rem_qty

    def remove(self, client_orderid):
        self.open_orders.pop(client_orderid, None)
        
    def add_to_filled_orders(self, client_orderid):
        self.filled_orders[client_orderid] = self.open_orders[client_orderid]
        print "Order=%r moved to filled orders" % client_orderid
        self.remove(client_orderid)
    
    def add_to_canceled_orders(self, client_orderid): 
        if client_orderid in self.canceled_orders:
            pass
        else: 
            self.canceled_orders[client_orderid] = self.open_orders[client_orderid]
            print "Order=%r moved to cancelled orders" % client_orderid
            self.remove(client_orderid)

    def get_status(self, client_orderid):
        if client_orderid in self.open_orders:
            return self.open_orders[client_orderid]    
        else:
            print 'Order=%d not found!' % client_orderid
    def order_exists(self, client_orderid):
        if client_orderid in self.filled_orders or client_orderid in self.canceled_orders:
            return True
        else:
            return False

cdef class ARSKospiFuturesSweepChaser(StrategyInterface):
    cdef:
        readonly:
            uint64_t futures_secid
            str name

    def __init__(self, uint64_t futures_secid, name):
        self.futures_secid = futures_secid
        self.name = str(name)
        self.initialize([futures_secid])
        
    def __repr__(self):
        return (
            '%s{%s, fut=%d, name=%s}'
        ) % (
            self.__class__.__name__, hex(id(self)), 
            self.futures_secid, self.name
        )

    cdef uint64_t _place_limit_order(self, long time, long seqnum, long secid, uint64_t side, str ordertype, long price, long qty):
        context_str = "Placing Limit Order: price=%r, side=%r, qty=%r, ordertype=%r" % (price, side, qty, ordertype)
        client_orderid = self.place_limit_order(time, seqnum, secid, price, side, qty, context_str)
        print context_str + " orderid=%r" % client_orderid
        manager.add(
            client_orderid, {
                'time':time,'seqnum':seqnum,'ordertype':ordertype,
                'secid':secid,'price':price,'side':side,'qty':qty,
                'context':context_str,'rem_qty':qty
                }
            )
        return client_orderid

    cdef uint64_t _place_fak_order(self, long time, long seqnum, long secid, uint64_t side, str ordertype, long price, long qty):
        context_str = "Placing FAK Order: price=%r, side=%r, qty=%r, ordertype=%r" % (price, side, qty, ordertype)
        client_orderid = self.place_fak_order(time, seqnum, secid, price, side, qty, context_str)
        print context_str + " orderid=%r" % client_orderid
        manager.add(
            client_orderid, {
                'time':time,'seqnum':seqnum,'ordertype':ordertype,
                'secid':secid,'price':price,'side':side,'qty':qty,
                'context':context_str,'rem_qty':qty
                }
            )
        return client_orderid

    cdef uint64_t _cancel_limit_order(self, long time, long seqnum, long secid, long qty, uint64_t cancel_id, short int side, long price):
        context_str = "Cancelling order=%d, price=%d, qty=%d, side=%d, secid=%d " % (cancel_id, price, qty, side, secid)
        print context_str
        client_orderid = self.place_fak_order(time, seqnum, seqnum, 0, 0, 0, context_str)
        manager.add_to_canceled_orders(cancel_id)
        return client_orderid     

    def evaluate_md(self):                
        cdef:
            MDOrderBook *md = self.md_ptr
            SimulatedOrderAck ack
            uint64_t client_orderid                      
                    
        while self._has_acks():
            ack = self._top_ack()
            print 'Recieved ack for client_orderid=%d, rem_qty=%d' % (ack.client_orderid, ack.remaining_qty)              
            if ack.remaining_qty == 0:
                if manager.order_exists(ack.client_orderid) == False: 
                    status = manager.get_status(ack.client_orderid)
                    if status['ordertype'] == 'trigger':
                        print "Order Filled: %s" % status
                        manager.update_remqty(ack.client_orderid, ack.remaining_qty)
                        # sweep trigger order was filled for full qty
                        if status['side'] == SIDE_BID:
                            self._place_limit_order(md.time, md.seqnum, self.futures_secid, SIDE_ASK, 'hedge', status['price']+500, status['qty'])
                            manager.add_to_filled_orders(ack.client_orderid)
                            self._remove_ack()

                        elif status['side'] == SIDE_ASK:
                            self._place_limit_order(md.time, md.seqnum, self.futures_secid, SIDE_BID, 'hedge', status['price']-500, status['qty'])
                            manager.add_to_filled_orders(ack.client_orderid)
                            self._remove_ack()

                    # hedge is fully filled if no rem_qty - remove from order manager
                    elif status['ordertype'] == 'hedge':
                        manager.update_remqty(ack.client_orderid, ack.remaining_qty)
                        print "Hedge Filled: %s" % status
                        manager.add_to_filled_orders(ack.client_orderid)
                        self._remove_ack()
                else:
                    self._remove_ack()
            # orders not fully filled handled here    
            elif ack.remaining_qty > 0:
                manager.update_remqty(ack.client_orderid, ack.remaining_qty)
                if manager.order_exists(ack.client_orderid) == False:
                    status = manager.get_status(ack.client_orderid)
                    # handles sweep trigger orders that are not fully filled
                    if status['ordertype'] == 'trigger':
                        if status['side'] == SIDE_ASK:
                            if status['price'] == md.asks[0].price:
                                print "Working order=%d @ best offer price, qty=%d" % (ack.client_orderid, ack.remaining_qty)
                                self._remove_ack()
                            else:
                                if ack.remaining_qty == SNIPER_QTY:
                                    self._cancel_limit_order(md.time, md.seqnum, self.futures_secid, ack.remaining_qty, ack.client_orderid, status['side'], status['price'])
                                    self._remove_ack()                                
                                elif ack.remaining_qty < SNIPER_QTY:       
                                    self._cancel_limit_order(md.time, md.seqnum, self.futures_secid, ack.remaining_qty, ack.client_orderid, status['side'], status['price'])
                                    self._place_limit_order(md.time, md.seqnum, self.futures_secid, SIDE_BID, 'hedge', status['price']-500, status['qty']-ack.remaining_qty)
                                    self._remove_ack()
                                 
                        elif status['side'] == SIDE_BID:
                            if status['price'] == md.bids[0].price:
                                print "Working order=%d @ best bid price, qty=%d" % (ack.client_orderid, ack.remaining_qty)
                                self._remove_ack()
                            else:    
                                if ack.remaining_qty == SNIPER_QTY:
                                    self._cancel_limit_order(md.time, md.seqnum, self.futures_secid, ack.remaining_qty, ack.client_orderid, status['side'], status['price'])
                                    self._remove_ack() 
                                elif ack.remaining_qty < SNIPER_QTY:
                                    self._cancel_limit_order(md.time, md.seqnum, self.futures_secid, ack.remaining_qty, ack.client_orderid, status['side'], status['price'])
                                    self._place_limit_order(md.time, md.seqnum, self.futures_secid, SIDE_ASK, 'hedge', status['price']+500, status['qty']-ack.remaining_qty)
                                    self._remove_ack()                              
                                    
                    # handles hedges that are partially filled
                    elif status['ordertype'] == 'hedge':
                        if status['side'] == SIDE_ASK:
                            if status['price'] == md.asks[0].price and (SNIPER_QTY / md.bids[0].qty) > MIN_LEAN_PCT:
                                print "Working hedge=%d @ best offer price, qty=%d" % (ack.client_orderid, ack.remaining_qty)
                                self._remove_ack()
                            elif status['price'] == md.asks[0].price and (SNIPER_QTY / md.bids[0].qty) < MIN_LEAN_PCT:
                                self._cancel_limit_order(md.time, md.seqnum, self.futures_secid, ack.remaining_qty, ack.client_orderid, status['side'], status['price'])
                                print "Placing order to scratch hedge=%d" % ack.client_orderid
                                self._place_limit_order(md.time, md.seqnum, self.futures_secid, SIDE_ASK, 'hedge', md.bids[0].price, status['qty'])
                                self._remove_ack()
                            else:
                                self._cancel_limit_order(md.time, md.seqnum, self.futures_secid, ack.remaining_qty, ack.client_orderid, status['side'], status['price'])
                                print "Placing order to bail on hedge=%d" % ack.client_orderid
                                self._place_limit_order(md.time, md.seqnum, self.futures_secid, SIDE_ASK, 'hedge', md.bids[0].price, status['qty'])
                                self._remove_ack()
                                
                        elif status['side'] == SIDE_BID:
                            if status['price'] == md.bids[0].price and (SNIPER_QTY / md.asks[0].qty) > MIN_LEAN_PCT:
                                print "Working hedge=%d @ best bid price, qty=%d" % (ack.client_orderid, ack.remaining_qty)
                                self._remove_ack()
                            elif status['price'] == md.bids[0].price and (SNIPER_QTY / md.asks[0].qty) < MIN_LEAN_PCT:
                                self._cancel_limit_order(md.time, md.seqnum, self.futures_secid, ack.remaining_qty, ack.client_orderid, status['side'], status['price'])
                                print "Placing order to scratch hedge=%d" % ack.client_orderid
                                self._place_limit_order(md.time, md.seqnum, self.futures_secid, SIDE_BID, 'hedge', md.asks[0].price, status['qty'])
                                self._remove_ack()
                            else:
                                self._cancel_limit_order(md.time, md.seqnum, self.futures_secid, ack.remaining_qty, ack.client_orderid, status['side'], status['price'])
                                print "Placing order to bail on hedge=%d" % ack.client_orderid
                                self._place_limit_order(md.time, md.seqnum, self.futures_secid, SIDE_BID, 'hedge', md.asks[0].price, status['qty'])
                                self._remove_ack()
                else:
                    self._remove_ack()
        
        # handle market data here
        if not self.valid_md:
            pass
        
        else:    
            prev_book = last_book.get([md.bids[0].price, md.bids[0].qty, md.asks[0].price, md.asks[0].qty])
            if md.buyvolume > 0 or md.sellvolume > 0:
                sweep = sweep_attributes.update_recorder([md.buyvolume, md.sellvolume, md.seqnum], prev_book)
                if sweep != None:
                    if SWEEP_SNIPER == True:
                        seq, levels, qty = sweep    
                        if levels > 0:
                            context_str = "Bull Sweep, levels=%r, qty=%r, last_seqnum=%r" % (levels, qty, seq)
                            print "Sweep Trigger: %s" % context_str
                            if SWEEP_CHASER == False: 
                                self._place_limit_order(md.time, md.seqnum, self.futures_secid, SIDE_ASK, 'trigger', md.asks[0].price, SNIPER_QTY)
                            elif SWEEP_CHASER == True:
                                self._place_limit_order(md.time, md.seqnum, self.futures_secid, SIDE_BID, 'trigger', md.asks[0].price, SNIPER_QTY)
                            else:
                                pass

                        elif levels < 0:
                            context_str = "Bear Sweep, levels=%r, qty=%r, last_seqnum=%r" % (levels, qty, seq)
                            print "Sweep Trigger: %s" % context_str
                            if SWEEP_CHASER == False:
                                self._place_limit_order(md.time, md.seqnum, self.futures_secid, SIDE_BID, 'trigger', md.bids[0].price, SNIPER_QTY)
                            elif SWEEP_CHASER == True:
                                self._place_limit_order(md.time, md.seqnum, self.futures_secid, SIDE_ASK, 'trigger', md.bids[0].price, SNIPER_QTY)
                            else:
                                pass
                    else:
                        pass

                elif sweep == None:
                    if QTY_SNIPER == True:
                        if md.buyvolume >= MIN_VOLUME:
                            ask_size = book_size(prev_book[1], prev_book[3], md.buyvolume, md.sellvolume, 1)
                            if ask_size > 0.001 and ask_size <= .30:
                                context_str = "Sniper Trigger: qty=%r, trigger_seqnum=%r, side=buy, ask_pct=%r" % (md.buyvolume, md.seqnum, ask_size)
                                print context_str
                                self._place_fak_order(md.time, md.seqnum, self.futures_secid, SIDE_BID, 'trigger', md.asks[0].price, SNIPER_QTY)
                        else:
                            pass
                        if md.sellvolume >= MIN_VOLUME:
                            bid_size = book_size(prev_book[1], prev_book[3], md.buyvolume, md.sellvolume, 2)
                            if bid_size > 0.001 and bid_size <= .30:
                                context_str = "Sniper Trigger: qty=%r, trigger_seqnum=%r, side=sell, bid_pct=%r" % (md.sellvolume, md.seqnum, bid_size)
                                print context_str
                                self._place_fak_order(md.time, md.seqnum, self.futures_secid, SIDE_ASK, 'trigger', md.bids[0].price, SNIPER_QTY)
                        else:
                            pass
            else:
                pass
        return

