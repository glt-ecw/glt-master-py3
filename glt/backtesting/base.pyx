# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from glt.md.standard cimport MDOrderBook, BookLevel
from glt.backtesting.match_simulation cimport SimulatedMatchingEngine


cdef int32_t find_order_sign(uint16_t side) nogil:
    if side == 1:
        return 1
    else:
        return -1


cdef class StrategyInterface(MDSource):
    def gc(self):
        print 'nothing to garbage collect for', self.__class__.__name__
        pass

    def initialize(self, list secids):
        super(StrategyInterface, self).initialize(secids)
        self.me_id = 0
        self.me_assigned = False

    def use_matching_engine(self, int64_t me_id):
        self.me_id = me_id
        self.me_assigned = True

    def matching_engine_id(self):
        return self.me_id

    def is_matching_engine_assigned(self):
        return self.me_assigned

    def has_orders(self):
        return self.oq.HasOrders()

    def clear_order_queue(self):
        return self.oq.ClearOrders()

    def place_limit_order(self, uint64_t time, uint64_t seqnum, uint64_t secid,
            int32_t price, uint16_t side, int32_t qty, str context):
        cdef uint64_t orderid
        orderid = self.oq.PrepareLimitOrder(secid, str.encode(self.__repr__()), time, seqnum, price, side, qty, str.encode(context))
        return orderid

    def place_fak_order(self, uint64_t time, uint64_t seqnum, uint64_t secid,
            int32_t price, uint16_t side, int32_t qty, str context):
        return self.oq.PrepareFAKOrder(secid, str.encode(self.__repr__()), time, seqnum, price, side, qty, str.encode(context))

    def place_sprint_order(self, uint64_t time, uint64_t seqnum, uint64_t secid,
            int32_t price, uint16_t side, int32_t qty, context, double bypass_market_by):
        return self.oq.PrepareSprintOrder(secid, str.encode(self.__repr__()), time, seqnum, price, side, qty, str.encode(context), bypass_market_by)

    def place_ghostfak_order(self, uint64_t time, uint64_t seqnum, uint64_t secid,
            int32_t price, uint16_t side, int32_t qty, str context):
        return self.oq.PrepareGhostFAKOrder(secid, str.encode(self.__repr__()), time, seqnum, price, side, qty, str.encode(context))

    def place_ghostlimit_order(self, uint64_t time, uint64_t seqnum, uint64_t secid,
            int32_t price, uint16_t side, int32_t qty, str context):
        raise NotImplemented('.place_ghostlimit_order(...) has bugs. do not use')
        #return self.oq.PrepareGhostLimitOrder(secid, str(self), time, seqnum, price, side, qty, context)

    def place_ghostsprint_order(self, uint64_t time, uint64_t seqnum, uint64_t secid,
            int32_t price, uint16_t side, int32_t qty, str context, double bypass_market_by):
        raise NotImplemented('.place_ghostsprint_order(...) has bugs. do not use')
        #return self.oq.PrepareGhostSprintOrder(secid, str(self), time, seqnum, price, side, qty, context, bypass_market_by)

    def cancel_order(self, uint64_t orderid):
        pass

    cdef bint _has_acks(self) nogil:
        return self.oq.HasAcks()

    cdef void _remove_ack(self) nogil:
        self.oq.RemoveAck()

    cdef SimulatedOrderAck _top_ack(self) nogil:
        cdef SimulatedOrderAck ack
        if not self._has_acks():
            return ack
        return self.oq.TopAck()

    def __repr__(self):
        return '%s{no info}' % self.__class__.__name__

    def transfer_orders_to(self, MatchingEngine me):
        me._enqueue_orders_from(&(self.oq))
        self.clear_order_queue()



cdef class MatchingEngine(MDSource):
    cdef:
        SimulatedMatchingEngine me
        readonly:
            bint use_exch_time
            uint64_t latency, time
            list strategies

    def __init__(self, list secids, uint64_t me_id, uint64_t latency=0, 
                 uint64_t look_forward_time=0, bint use_exch_time=True):
        cdef:
            size_t i, n_secids = len(secids)
        self.me.Init(me_id, latency, use_exch_time, look_forward_time)
        for 0 <= i < n_secids:
            self.me.AddSecurity(secids[i])
        self.initialize(secids)
        self.latency = latency
        self.use_exch_time = use_exch_time
        self.strategies = []

    cdef void _enqueue_orders_from(self, OrderQueuer *oq) nogil:
        self.me.EnqueueOrdersFrom(oq)

    def read_md(self):
        self.update_valid_md()
        if not self.valid_md:
            return
        self.me.ReadMD(self.md_ptr)
        # evaluate strategies
        for strategy in self.strategies:
            strategy.update_valid_md()
            strategy.evaluate_md()
            if strategy.has_orders():
                strategy.transfer_orders_to(self)

    def add_strategy(self, strategy):
        self.strategies.append(strategy)
