# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True

from cython.operator import dereference
from libc.stdint cimport *
from libcpp.unordered_map cimport unordered_map as unordered_map_
from glt.md.standard cimport BookLevel, MDOrderBook, VolumeAccumulator
from glt.backtesting.base cimport StrategyInterface, SimulatedOrderAck
import pandas as pd
import numpy as np
import time

cdef uint16_t SIDE_BID = 1
cdef uint16_t SIDE_ASK = 2
cdef double MIN_TURN_PCT = .2
cdef double MAX_TURN_PCT = .25
cdef uint16_t QUOTE_SIZE = 1
cdef uint16_t WAIT_TIME_S = 1
cdef double CANCEL_IMBAL = .15

turns = TurnFinder(MIN_TURN_PCT, MAX_TURN_PCT, QUOTE_SIZE)
timer = TurnTimer(WAIT_TIME_S)
manager = OrderManager()
monitor = CancelMonitor(CANCEL_IMBAL)

cdef class TurnFinder:
    cdef:
        bint bid_turn
        bint ask_turn
        uint16_t quote_size
        list previous_book
        list book
        readonly:
            double min_turn_pct
            double max_turn_pct
    
    def __init__(self, min_turn_pct, max_turn_pct, quote_size):
        self.bid_turn = False
        self.ask_turn = False
        self.min_turn_pct = min_turn_pct
        self.max_turn_pct = max_turn_pct
        self.quote_size = quote_size
        self.previous_book = []
        
    def get(self, book):
        bid, bidqty, ask, askqty = book
        # first book passed through the object 
        
        if not len(self.previous_book):
            self.previous_book = book
            return []
        else:
            if bid > self.previous_book[0] and ask > self.previous_book[2]:
                self.ask_turn = True
                self.previous_book = book
            elif bid < self.previous_book[0] and ask < self.previous_book[2]:
                self.bid_turn = True
                self.previous_book = book
            else:
                pass

            if self.ask_turn == True:
                bid_plus_us = float(bidqty + self.quote_size)
                divisor = float(bidqty + self.quote_size + askqty)
                turn_pct = bid_plus_us / divisor
                if turn_pct >= self.min_turn_pct and turn_pct <= self.max_turn_pct:
                    self.ask_turn = False
                    return [1, turn_pct]
                elif turn_pct > self.max_turn_pct:
                    self.ask_turn = False
                    return []

            elif self.bid_turn == True:
                ask_plus_us = float(askqty + self.quote_size)
                divisor = float(askqty + self.quote_size + bidqty)
                turn_pct = ask_plus_us / divisor
                if turn_pct >= self.min_turn_pct and turn_pct <= self.max_turn_pct:
                    self.bid_turn = False
                    return [2, turn_pct]
                elif turn_pct > self.max_turn_pct:
                    self.bid_turn = False
                    return []
            else:
                return []

cdef class TurnTimer:
    cdef:
        long long last_turn_time
        readonly:
            uint16_t wait_time_s
    
    def __init__(self, wait_time_s):
        self.last_turn_time = 0
        self.wait_time_s = wait_time_s
    
    def check(self, timestamp):
        if self.last_turn_time == 0:
            self.last_turn_time = timestamp
            return True
        else:
            if (timestamp - self.last_turn_time) >= (10**9)*self.wait_time_s:
                self.last_turn_time = timestamp
                return True
            else:
                return False 

cdef class CancelMonitor:
    cdef:
        list book
        readonly:
            double cancel_imbal
    
    def __init__(self, cancel_imbal):
        self.cancel_imbal = cancel_imbal

    def check(self, book):
        bid, bidqty, ask, askqty, side, quote_price = book
        
        if side == 1:
            if quote_price == bid:
                divisor = float(bidqty + askqty)
                turn_pct = float(bidqty) / divisor
                if turn_pct < self.cancel_imbal:
                    print turn_pct
                    return True
            else:
                return False
        elif side == 2:
            if quote_price == ask:
                divisor = float(bidqty + askqty)
                turn_pct = float(askqty) / divisor
                if turn_pct < self.cancel_imbal:
                    print turn_pct
                    return True
            else:
                return False   
                             
cdef class OrderManager:
    cdef:
        dict open_orders    
        dict filled_orders
        dict canceled_orders

    def __init__(self):
        self.open_orders = {}
        self.filled_orders = {}
        self.canceled_orders = {}
   
    def add(self, client_orderid, order_info):
        if client_orderid in self.canceled_orders:
            print 'Ignoring order=%r, order was cancelled' % client_orderid
        else:
            self.open_orders[client_orderid] = order_info
            return
    
    def update_remqty(self, client_orderid, rem_qty):
        if client_orderid in self.canceled_orders:
            return
        else:
            self.open_orders[client_orderid]['rem_qty'] = rem_qty

    def remove(self, client_orderid):
        self.open_orders.pop(client_orderid, None)
    
    def add_to_filled_orders(self, client_orderid):
        self.filled_orders[client_orderid] = self.open_orders[client_orderid]
        print "Order=%r moved to filled orders" % client_orderid
        self.remove(client_orderid)
    
    def add_to_canceled_orders(self, client_orderid): 
        if client_orderid in self.canceled_orders:
            pass
        else: 
            self.canceled_orders[client_orderid] = self.open_orders[client_orderid]
            print "Order=%r moved to cancelled orders" % client_orderid
            self.remove(client_orderid)

    def get_status(self, client_orderid):
        if client_orderid in self.open_orders:
            return self.open_orders[client_orderid]    
        else:
            print 'Order=%d not found!' % client_orderid
    def order_exists(self, client_orderid):
        if client_orderid in self.filled_orders or client_orderid in self.canceled_orders:
            return True
        else:
            return False
    
    def get_open_orders(self):
        return self.open_orders

cdef class ScraperScalpOrCancel(StrategyInterface):
    cdef:
        readonly:
            str trigger_name
            str hedge_name
            uint64_t trigger_secid
            uint64_t hedge_secid
            dict secid_map

    def __init__(self, uint64_t trigger_secid, trigger_name, uint64_t hedge_secid, hedge_name):
        self.trigger_secid = trigger_secid
        self.hedge_secid = hedge_secid
        self.trigger_name = str(trigger_name)
        self.hedge_name = str(hedge_name)
        self.initialize([trigger_secid, hedge_secid])
        self.secid_map = {self.trigger_secid:self.trigger_name, self.hedge_secid:self.hedge_name}

    def __repr__(self):
        return (
            '%s{%s, fut=%d, name=%s}'
        ) % ( 
            self.__class__.__name__, hex(id(self)), 
            self.trigger_secid, self.trigger_name
        )
    
    cdef uint64_t _place_limit_order(self, long time, long seqnum,
                                     long secid, uint64_t side, str ordertype, long price, long qty):
        context_str = "Placing Limit Order: price=%r, side=%r, qty=%r, ordertype=%r" % (price, side, qty, ordertype)
        client_orderid = self.place_limit_order(time, seqnum, secid, price, side, qty, context_str)
        print context_str + " orderid=%r" % client_orderid
        manager.add(
            client_orderid, {
                'time':time,'seqnum':seqnum,'ordertype':ordertype,
                'secid':secid,'price':price,'side':side,'qty':qty,
                'context':context_str,'rem_qty':qty
                }
            )
        return client_orderid

    cdef uint64_t _place_fak_order(self, long time, long seqnum,
                                  long secid, uint64_t side, str ordertype, long price, long qty):
        context_str = "Placing FAK Order: price=%r, side=%r, qty=%r, ordertype=%r" % (price, side, qty, ordertype)
        client_orderid = self.place_fak_order(time, seqnum, secid, price, side, qty, context_str)
        print context_str + " orderid=%r" % client_orderid
        manager.add(
            client_orderid, {
                'time':time,'seqnum':seqnum,'ordertype':ordertype,
                'secid':secid,'price':price,'side':side,'qty':qty,
                'context':context_str,'rem_qty':qty
                }
            )
        return client_orderid

    cdef uint64_t _cancel_limit_order(self, long time, long seqnum,
                                     long secid, long qty, uint64_t cancel_id, short int side, long price, bint place=False):
        context_str = "Cancelling order=%d, price=%d, qty=%d, side=%d, secid=%d" % (cancel_id, price, qty, side, secid)
        print context_str
        if place:
            client_orderid = self.place_fak_order(time, seqnum, secid, 1, 1, 1, context_str)
            manager.add(
                client_orderid, {
                    'time':time,'seqnum':seqnum,'ordertype':'fak',
                    'secid':secid,'price':price,'side':side,'qty':qty,
                    'context':context_str,'rem_qty':qty
                    }                
                )
            manager.add_to_canceled_orders(cancel_id)
            manager.add_to_canceled_orders(client_orderid)
            return client_orderid
        manager.add_to_canceled_orders(cancel_id)
        return cancel_id 


    def evaluate_md(self):
        cdef:
            MDOrderBook *md = self.md_ptr
            SimulatedOrderAck ack 
            uint64_t client_orderid

        # ack evaluation
        while self._has_acks():
            ack = self._top_ack()
            if not manager.order_exists(ack.client_orderid):
                print 'Received ack for client_orderid=%d, rem_qty=%d' % (ack.client_orderid, ack.remaining_qty)
            if ack.remaining_qty == 0:
                if not manager.order_exists(ack.client_orderid):
                    status = manager.get_status(ack.client_orderid)
                    if status['ordertype'] == 'scraper':
                        print "Scraper Filled: %s, order=%d" % (status, ack.client_orderid)
                        manager.update_remqty(ack.client_orderid, ack.remaining_qty)
                        if status['side'] == SIDE_BID:
                            hedgeid = self._place_limit_order(md.exch_time, md.seqnum, self.hedge_secid,
                                                              SIDE_ASK, 'hedge', status['price'], status['qty']*10)         
                            manager.add_to_filled_orders(ack.client_orderid)
                            print 'Hedging scraper_order=%r with hedge=%r' % (ack.client_orderid, hedgeid)
                            self._remove_ack()
                        elif status['side'] == SIDE_ASK:
                            hedgeid = self._place_limit_order(md.exch_time, md.seqnum, self.hedge_secid,
                                                              SIDE_BID, 'hedge', status['price'], status['qty']*10)
                            manager.add_to_filled_orders(ack.client_orderid)
                            print 'Hedging scraper_order=%r with hedge=%r' % (ack.client_orderid, hedgeid)
                            self._remove_ack()
                    elif status['ordertype'] == 'hedge':
                        manager.update_remqty(ack.client_orderid, ack.remaining_qty)
                        print "Hedge Filled: %s, order=%d " % (status, ack.client_orderid)
                        manager.add_to_filled_orders(ack.client_orderid)
                        self._remove_ack()

                else:
                    self._remove_ack()
            
            elif ack.remaining_qty > 0:
                manager.update_remqty(ack.client_orderid, ack.remaining_qty)
                if not manager.order_exists(ack.client_orderid):
                    status = manager.get_status(ack.client_orderid)
                    if status['ordertype'] == 'scraper':
                        if ack.remaining_qty == status['qty']:
                            if md.secid == self.trigger_secid:
                                if status['side'] == SIDE_ASK:
                                    if status['price'] == md.asks[0].price:
                                        order_info = (ack.client_orderid, ack.remaining_qty, md.bids[0].qty, md.asks[0].qty)
                                        print "Working scraper=%d @ best offer price, qty=%d, bidqty=%d, askqty=%d" % order_info
                                        self._remove_ack()
                                    else:
                                        print "Market has moved away from scraper, order=%d" % ack.client_orderid
                                        self._cancel_limit_order(md.exch_time, md.seqnum, self.trigger_secid,
                                                                 ack.remaining_qty, ack.client_orderid, SIDE_ASK, status['price'], False)
                                        self._remove_ack()
                                        
                                elif status['side'] == SIDE_BID:
                                    if status['price'] == md.bids[0].price:
                                        order_info = (ack.client_orderid, ack.remaining_qty, md.bids[0].qty, md.asks[0].qty)
                                        print "Working scraper=%d @ best bid price, qty=%d, bidqty=%d, askqty=%d" % order_info
                                        self._remove_ack()
                                    else:
                                        print "Market has moved away from scraper, order=%d" % ack.client_orderid
                                        self._cancel_limit_order(md.exch_time, md.seqnum, self.trigger_secid,
                                                                 ack.remaining_qty, ack.client_orderid, SIDE_BID, status['price'], False)
                                        self._remove_ack()
                                    
                        else:
                            self._remove_ack()
                            
                    elif status['ordertype'] == 'hedge':
                        if status['secid'] == self.hedge_secid:
                            if md.secid == self.hedge_secid:
                                if status['side'] == SIDE_ASK:
                                    if status['price'] == md.asks[0].price:
                                        order_info = (ack.client_orderid, md.asks[0].price, ack.remaining_qty, md.bids[0].qty, md.asks[0].qty)
                                        print "Working hedge=%d @ best offer price=%d, qty=%d, bidqty=%d, askqty=%d" % order_info
                                        self._remove_ack()
                                    else:
                                        print "We lost a tick on hedge=%d" % (ack.client_orderid)
                                        self._cancel_limit_order(md.exch_time, md.seqnum, self.hedge_secid,
                                                                 ack.remaining_qty, ack.client_orderid, SIDE_BID, status['price'], False)
                                        self._remove_ack()
                                elif status['side'] == SIDE_BID:
                                    if status['price'] == md.bids[0].price:
                                        order_info = (ack.client_orderid, md.bids[0].price, ack.remaining_qty, md.bids[0].qty, md.asks[0].qty)
                                        print "Working hedge=%d @ best bid price=%d, qty=%d, bidqty=%d, askqty=%d" % order_info
                                        self._remove_ack()
                                    else:
                                        print "We lost a tick on hedge=%d" % (ack.client_orderid) 
                                        self._cancel_limit_order(md.exch_time, md.seqnum, self.hedge_secid,
                                                             ack.remaining_qty, ack.client_orderid, SIDE_ASK, status['price'], False)
                                        self._remove_ack()
                else:
                    self._remove_ack()

        # cancel orders that are open and their acks have already been deleted
        # deletes hedge/scraper orders that are off of the market
        cancel_ids = []
        for orderid in manager.get_open_orders():
            order = manager.get_status(orderid)
            if order['ordertype'] == 'hedge':
                if md.secid == self.hedge_secid:
                    if order['side'] == 1:
                        if order['price'] != md.bids[0].price:
                            print "We lost a tick on hedge=%d" % orderid
                            cancel_ids.append((md.exch_time, md.seqnum, self.hedge_secid,
                                               order['rem_qty'], orderid, SIDE_BID, order['price'], False))
                        else:
                            order_info = (orderid, md.bids[0].price, order['rem_qty'], md.bids[0].qty, md.asks[0].qty)
                            print "Working hedge=%d @ best bid price=%d, qty=%d, bidqty=%d, askqty=%d" % order_info
                    elif order['side'] == 2:
                        if order['price'] != md.asks[0].price:
                            print "We lost a tick on hedge=%d" % orderid
                            cancel_ids.append((md.exch_time, md.seqnum, self.hedge_secid,
                                               order['rem_qty'], orderid, SIDE_ASK, order['price'], False))
                        else:
                            order_info = (orderid, md.asks[0].price, order['rem_qty'], md.bids[0].qty, md.asks[0].qty)
                            print "Working hedge=%d @ best offer price=%d, qty=%d, bidqty=%d, askqty=%d" % order_info
            elif order['ordertype'] == 'scraper':
                if md.secid == self.trigger_secid:
                    if not monitor.check([md.bids[0].price, md.bids[0].qty, md.asks[0].price, md.asks[0].qty, order['side'], order['price']]): 
                        if order['side'] == 1:
                            if order['price'] != md.bids[0].price:
                                if order['price'] < md.bids[0].price:
                                    print "Market has moved away from scraper, order=%d" % orderid
                                    cancel_ids.append((md.exch_time, md.seqnum, self.trigger_secid,
                                                       order['rem_qty'], orderid, SIDE_BID, order['price'], False))
                                else:
                                    pass
                            else:
                                order_info = (orderid, md.bids[0].price, order['rem_qty'], md.bids[0].qty, md.asks[0].qty)
                                print "Working scraper=%d @ best bid price=%d, qty=%d, bidqty=%d, askqty=%d" % order_info
                        elif order['side'] == 2:
                            if order['price'] != md.asks[0].price:
                                if order['price'] > md.asks[0].price:
                                    print "Market has moved away from scraper, order=%d" % orderid
                                    cancel_ids.append((md.exch_time, md.seqnum, self.trigger_secid,
                                                       order['rem_qty'], orderid, SIDE_ASK, order['price'], False))
                                else:
                                    pass
                            else:
                                order_info = (orderid, md.asks[0].price, order['rem_qty'], md.bids[0].qty, md.asks[0].qty)
                                print "Working scraper=%d @ best offer price=%d, qty=%d, bidqty=%d, askqty=%d" % order_info
                    else:
                        print 'Scraper cancel imbal triggered, order=%d' % orderid
                        cancel_ids.append((md.exch_time, md.seqnum, self.trigger_secid,
                                           order['rem_qty'], orderid, order['side'], order['price'], True))

        for cancel in cancel_ids:
            ts, seq, secid, rem_qty, oid, side, px, place = cancel 
            self._cancel_limit_order(ts, seq, secid, rem_qty, oid, side, px, place)

        cancel_ids = []
      
        if not self.valid_md:
            return

        else:
            if md.buyvolume == 0 and md.sellvolume == 0: 
                if md.secid == self.trigger_secid:
                    turn_direction = turns.get([md.bids[0].price, md.bids[0].qty, md.asks[0].price, md.asks[0].qty]) 
                    if turn_direction:
                        if timer.check(md.exch_time):
                            if len(turn_direction):
                                if turn_direction[0] == 1:
                                    context_str = 'Scraper turn: %r, side: %r' % (round(turn_direction[1], 3), turn_direction[0])    
                                    print(context_str)
                                    self._place_limit_order(md.exch_time, md.seqnum, self.trigger_secid, SIDE_BID,
                                                            'scraper', md.bids[0].price, QUOTE_SIZE)
                                elif turn_direction[0] == 2:
                                    context_str = 'Scraper turn: %r, side: %r' % (round(turn_direction[1], 3), turn_direction[0])
                                    print(context_str)           
                                    self._place_limit_order(md.exch_time, md.seqnum, self.trigger_secid, SIDE_ASK,
                                                            'scraper', md.asks[0].price, QUOTE_SIZE)
                                else:
                                    pass
