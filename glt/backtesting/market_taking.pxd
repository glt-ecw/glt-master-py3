from libc.stdint cimport int16_t, int32_t, int64_t, uint16_t, uint32_t, uint64_t
from glt.md.standard cimport MDSource
from glt.backtesting.matching cimport MockOrder


cdef class Taker(MDSource):
    cdef readonly list orders
