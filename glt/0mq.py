import zmq
import warnings


def mod_norm_addr(addr):
    if addr.startswith('norm://'):
        base_addr = addr[7:]
    else:
        base_addr = addr
    node_id = int(time.time() * 1e9) % (2**32-1)
    return 'norm://%d,%s' % (node_id, base_addr)


class NORMSocket:
    def __init__(self, addr, publish=False):
        self.addr = mod_norm_addr(addr)
        warn_msg = (
            'modifying address to comply with NORM: %s to %s'
        ) % (addr, self.addr)
        warnings.warn(warn_msg)
        context = zmq.Context()
        if publish:
            sock = context.socket(zmq.PUB)
            sock.connect(self.addr)
        else:
            sock = context.socket(zmq.SUB)
            sock.bind(self.addr)
            sock.setsockopt(zmq.SUBSCRIBE, '') 
        self.sock = sock
        self.publish = publish

    def send(self, data):
        return self.sock.send(data)

    def recv(self):
        return self.sock.recv()
