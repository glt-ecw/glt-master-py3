from libcpp.map cimport map as cppmap
from cython.operator cimport dereference as deref, preincrement as inc

cdef struct OrderBook:
    int depth
    cppmap[long, long] qtys
    #cppmap[long, long] pars
    cppmap[long, long] unresolved
    cppmap[long, long].iterator it
    cppmap[long, long].reverse_iterator rit

cdef OrderBook init_orderbook(int depth) nogil:
    cdef OrderBook ob
    ob.depth = depth
    ob.qtys = cppmap[long, long]()
    return ob

cdef void clear_orderbook(OrderBook ob):
    ob.qtys.clear()
    ob.unresolved.clear()
    return

cdef void add_qty_orderbook(OrderBook ob, long price, long qty):
    return

cdef void modify_qty_orderbook(OrderBook ob, long price, long qty):
    return

cdef void remove_price_orderbook(OrderBook ob, long price):
    return

cdef bint has_price_orderbook(OrderBook ob, long price):
    return ob.qtys.find(price) != ob.qtys.end()

cdef struct BBO:
    OrderBook bids
    OrderBook asks
    
cdef BBO init_bbo(int depth) nogil:
    cdef BBO bbo
    bbo.bids = init_orderbook(depth)
    bbo.asks = init_orderbook(depth)
    return bbo
    
cdef bint is_filled(BBO bbo) nogil:
    return bbo.bids.qtys.begin() != bbo.bids.qtys.end() and bbo.asks.qtys.begin() != bbo.asks.qtys.end()

cdef bint is_crossed(BBO bbo) nogil:
    if not is_filled(bbo):
        return False
    return deref(bbo.bids.qtys.rbegin()).first >= deref(bbo.asks.qtys.begin()).first

