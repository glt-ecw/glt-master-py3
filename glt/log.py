import logging
import datetime as dt

from logging import DEBUG, INFO


def create_logger(log_name, log_filename, debug=True, custom_formatting=None):
    logger = logging.getLogger(log_name)
    logger.setLevel(logging.DEBUG)

    log_file_handler = logging.FileHandler(log_filename)
    log_file_handler.setLevel(logging.DEBUG if debug else logging.INFO)
    log_term_handler = logging.StreamHandler()
    log_term_handler.setLevel(logging.DEBUG if debug else logging.INFO)

    if custom_formatting is None:
        custom_formatting = (
            '%(asctime)s|%(process)d'
            '|%(levelname)s|%(module)s::'
            '%(funcName)s - %(message)s'
        )
    # set formatter
    formatter = logging.Formatter(custom_formatting)
    log_file_handler.setFormatter(formatter)
    log_term_handler.setFormatter(formatter)

    logger.addHandler(log_file_handler)
    logger.addHandler(log_term_handler)
    return logger


def filter_file_handlers(logger):
    return [x for x in logger.handlers if isinstance(x, logging.FileHandler)]


def generate_log_filename(log_fmt):
    return log_fmt % dt.datetime.now().strftime('%Y%m%dT%H%M%S%f')
