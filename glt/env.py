import os

GLT_MASTER_PATH = os.environ['GLT_MASTER_PATH']
CFG_PATH = os.path.join(GLT_MASTER_PATH, 'cfg')
CONFIG_PATH = CFG_PATH

MYSQL_CFG = os.path.join(CFG_PATH, 'mysql.json')
MYSQL_CONFIG = MYSQL_CFG

HOSTS_CFG = os.path.join(CFG_PATH, 'hosts.json')
HOSTS_CONFIG = HOSTS_CFG
