cimport cython
from libcpp.map cimport map as cppmap
from libcpp.vector cimport vector
from cython.operator cimport dereference as deref, preincrement as inc

from numpy cimport ndarray
from numpy import zeros, float64, int64

cdef long MAXINT = 2147483647
cdef long MININT = -2147483648

cdef struct MarketLevel:
    long bidprc
    long bidqty
    long askprc
    long askqty

cdef long long_floor(long prc, long rval) nogil:
    cdef long adj = rval * (prc / rval)
    return adj

cdef long long_ceil(long prc, long rval) nogil:
    cdef long adj = rval * (prc / rval)
    if adj != prc:
        return adj + rval
    else:
        return adj
    
cdef long long_min(long a, long b) nogil:
    if a < b:
        return a
    else:
        return b
    
cdef long long_max(long a, long b) nogil:
    if a > b:
        return a
    else:
        return b
    
@cython.boundscheck(False)
cdef void aggregate_book_fast(long n, long my_depth, long mkt_depth, 
                              long tick_width, long total_syms, long[:] syms,
                              long[:, :] raw_bidprcs, long[:, :] raw_bidqtys, 
                              long[:, :] raw_askprcs, long[:, :] raw_askqtys, 
                              long[:, :, :] agg) nogil:
    cdef:
        long i, j, sym, bidprc, askprc, adj_bidprc, adj_askprc
        bint all_syms_accounted = False
        
        cppmap[long, vector[MarketLevel]] mkt
        cppmap[long, vector[MarketLevel]].iterator it
        
        cppmap[long, long] agg_bids, agg_asks
        cppmap[long, long].iterator agg_it
        cppmap[long, long].reverse_iterator agg_rit
        
    mkt = cppmap[long, vector[MarketLevel]]()
    agg_bids = cppmap[long, long]()
    agg_asks = cppmap[long, long]()
    
    for i in range(n):
        sym = syms[i]
        
        if mkt.find(sym) == mkt.end():
            mkt[sym] = vector[MarketLevel](mkt_depth)
            if mkt.size() >= total_syms:
                all_syms_accounted = True
            
        for j in range(mkt_depth):
            mkt[sym][j].bidprc = raw_bidprcs[i, j]
            mkt[sym][j].bidqty = raw_bidqtys[i, j]
            mkt[sym][j].askprc = raw_askprcs[i, j]
            mkt[sym][j].askqty = raw_askqtys[i, j]
        
        if all_syms_accounted:
            agg_bids.clear()
            agg_asks.clear()
            it = mkt.begin()
            while it != mkt.end():
                sym = deref(it).first
                for j in range(mkt_depth):
                    bidprc = mkt[sym][j].bidprc
                    askprc = mkt[sym][j].askprc
                    adj_bidprc = long_floor(bidprc, tick_width)
                    adj_askprc = long_ceil(askprc, tick_width)
                    if agg_bids.find(adj_bidprc) == agg_bids.end():
                        agg_bids[adj_bidprc] = mkt[sym][j].bidqty
                    else:
                        agg_bids[adj_bidprc] += mkt[sym][j].bidqty
                    if agg_asks.find(adj_askprc) == agg_asks.end():
                        agg_asks[adj_askprc] = mkt[sym][j].askqty
                    else:
                        agg_asks[adj_askprc] += mkt[sym][j].askqty
                inc(it)
                
            agg_rit = agg_bids.rbegin()
            j = 0
            while agg_rit != agg_bids.rend() and j < my_depth:
                agg[i, j, 0] = deref(agg_rit).first
                agg[i, j, 1] = deref(agg_rit).second
                inc(agg_rit)
                j += 1
                
            agg_it = agg_asks.begin()
            j = 0
            while agg_it != agg_asks.end() and j < my_depth:
                agg[i, j, 2] = deref(agg_it).first
                agg[i, j, 3] = deref(agg_it).second
                inc(agg_it)
                j += 1
    return

def aggregate_book(df, ndarray uniq_syms, long my_depth, long tick_width, long mkt_depth=10):
    cdef:
        long n = df.shape[0], total_syms = uniq_syms.shape[0]
        long[:] syms = df.sym_int.values
        long[:, :] raw_bidqtys, raw_bidprcs, raw_askprcs, raw_askqtys
        
        # output
        ndarray agg = zeros((n, my_depth, 4), dtype=int64)
    
    raw_bidprcs = df.loc[:, ['bidprc%d' % i for i in xrange(mkt_depth)]].values
    raw_bidqtys = df.loc[:, ['bidqty%d' % i for i in xrange(mkt_depth)]].values
    raw_askprcs = df.loc[:, ['askprc%d' % i for i in xrange(mkt_depth)]].values
    raw_askqtys = df.loc[:, ['askqty%d' % i for i in xrange(mkt_depth)]].values
    
    aggregate_book_fast(n, my_depth, mkt_depth, tick_width, total_syms, syms, 
                        raw_bidprcs, raw_bidqtys, raw_askprcs, raw_askqtys, agg)
    
    return agg

cdef void aggregate_even_book_fast(long n, long my_depth, long mkt_depth, 
                              long tick_width, long total_syms, long[:] syms,
                              long[:, :] raw_bidprcs, long[:, :] raw_bidqtys, 
                              long[:, :] raw_askprcs, long[:, :] raw_askqtys, 
                              long[:, :, :] agg) nogil:
    cdef:
        long i, j, sym, bidprc, askprc, adj_bidprc, adj_askprc
        bint all_syms_accounted = False
        
        cppmap[long, vector[MarketLevel]] mkt
        cppmap[long, vector[MarketLevel]].iterator it
        
        cppmap[long, long] agg_bids, agg_asks
        cppmap[long, long].iterator agg_it
        cppmap[long, long].reverse_iterator agg_rit
        
    mkt = cppmap[long, vector[MarketLevel]]()
    agg_bids = cppmap[long, long]()
    agg_asks = cppmap[long, long]()
    
    for i in range(n):
        sym = syms[i]
        
        if mkt.find(sym) == mkt.end():
            mkt[sym] = vector[MarketLevel](mkt_depth)
            if mkt.size() >= total_syms:
                all_syms_accounted = True
            
        for j in range(mkt_depth):
            mkt[sym][j].bidprc = raw_bidprcs[i, j]
            mkt[sym][j].bidqty = raw_bidqtys[i, j]
            mkt[sym][j].askprc = raw_askprcs[i, j]
            mkt[sym][j].askqty = raw_askqtys[i, j]
        
        if all_syms_accounted:
            agg_bids.clear()
            agg_asks.clear()
            it = mkt.begin()
            while it != mkt.end():
                sym = deref(it).first
                for j in range(mkt_depth):
                    bidprc = mkt[sym][j].bidprc
                    askprc = mkt[sym][j].askprc
                    if bidprc%tick_width == 0:
                        adj_bidprc = bidprc
                        if agg_bids.find(adj_bidprc) == agg_bids.end():
                            agg_bids[adj_bidprc] = mkt[sym][j].bidqty
                        else:
                            agg_bids[adj_bidprc] += mkt[sym][j].bidqty
                            
                    if askprc%tick_width == 0:
                        adj_askprc = askprc
                        if agg_asks.find(adj_askprc) == agg_asks.end():
                            agg_asks[adj_askprc] = mkt[sym][j].askqty
                        else:
                            agg_asks[adj_askprc] += mkt[sym][j].askqty
                inc(it)
                
            agg_rit = agg_bids.rbegin()
            j = 0
            while agg_rit != agg_bids.rend() and j < my_depth:
                agg[i, j, 0] = deref(agg_rit).first
                agg[i, j, 1] = deref(agg_rit).second
                inc(agg_rit)
                j += 1
                
            agg_it = agg_asks.begin()
            j = 0
            while agg_it != agg_asks.end() and j < my_depth:
                agg[i, j, 2] = deref(agg_it).first
                agg[i, j, 3] = deref(agg_it).second
                inc(agg_it)
                j += 1
    return

def aggregate_even_book(df, ndarray uniq_syms, long my_depth, long tick_width, long mkt_depth=10):
    cdef:
        long n = df.shape[0], total_syms = uniq_syms.shape[0]
        long[:] syms = df.sym_int.values
        long[:, :] raw_bidqtys, raw_bidprcs, raw_askprcs, raw_askqtys
        
        # output
        ndarray agg = zeros((n, my_depth, 4), dtype=int64)
    
    raw_bidprcs = df.loc[:, ['bidprc%d' % i for i in xrange(mkt_depth)]].values
    raw_bidqtys = df.loc[:, ['bidqty%d' % i for i in xrange(mkt_depth)]].values
    raw_askprcs = df.loc[:, ['askprc%d' % i for i in xrange(mkt_depth)]].values
    raw_askqtys = df.loc[:, ['askqty%d' % i for i in xrange(mkt_depth)]].values
    
    aggregate_even_book_fast(n, my_depth, mkt_depth, tick_width, total_syms, syms, 
                        raw_bidprcs, raw_bidqtys, raw_askprcs, raw_askqtys, agg)
    
    return agg


def time_to_first_turn(nkdf, relevant_turns):
    cdef:
        long n_nkdf = nkdf.shape[0], n_turns = relevant_turns.shape[0], i, j
        # nkdf arrays
        long[:] times = nkdf.time.values.astype(int64)
        long[:] uniq_ticks = nkdf.unique_tick.values
        # relvant_turns arrays
        long[:] turn_uniq_ticks = relevant_turns.prev_unique_tick.values
        long[:] turn_times = relevant_turns.prev_time.values.astype(int64)
        long turn_time, uniq_tick
        
        ndarray results = zeros(n_nkdf, dtype=int64)

    j = 0
    for i in range(n_turns):
        turn_time = turn_times[i]
        uniq_tick = turn_uniq_ticks[i] - 1
        # walk forward to find uniq_tick
        while j < n_nkdf and uniq_ticks[j] != uniq_tick:
            j += 1
        # now mark time to first tick
        while j < n_nkdf and uniq_ticks[j] == uniq_tick:
            results[j] = (turn_time - times[j]) / 1000
            j += 1
    
    return results

def sniper_streaker_sides(nkdf, long window_size, long sym_int, long tick_increment=1,
                          long ignore_sym=0, long ignore_qty=0):
    """
    streaker_sides(nkdf, double window_size, int sym_int)

    all: 0
    nk: 1
    mnk: 2
    snk: 3

    results:
        0 := buys_at_bid
        1 := buys_above_bid
        2 := sells_at_ask
        3 := sells_below_ask
        4 := buys_vs_ask
        5 := sells_vs_bid
        6 := vwap_buys
        7 := vwap_sells
    """
    cdef:
        long tlen = nkdf.shape[0], i, j, window_start
        long[:] times = nkdf.time.values.astype(int64)
        long[:] bidprc = nkdf.bidprc0.values
        long[:] askprc = nkdf.askprc0.values
        long[:] buyprices = nkdf.buyprice.values
        long[:] sellprices = nkdf.sellprice.values
        long[:] buyvolumes = nkdf.buyvolume.values
        long[:] sellvolumes = nkdf.sellvolume.values
        long[:] sym_ints = nkdf.sym_int.values

        long bid_ref, ask_ref, buys_vs_ask, sells_vs_bid
        long prcqty_buys, prcqty_sells, qty_buys, qty_sells, prcqty_tot, qty_tot
        long count_buys, count_sells
        ndarray[long, ndim=2] streaker_results = zeros((tlen, 4), dtype=int64)
        ndarray[double, ndim=2] vwap_results = zeros((tlen, 3), dtype=float64)
        bint valid_bid, valid_ask

    for i in range(tlen):
        j = i
        buys_vs_ask, sells_vs_bid = 0, 0
        count_buys, count_sells = 0, 0
        prcqty_buys, prcqty_sells, qty_buys, qty_sells = 0, 0, 0, 0
        prcqty_tot, qty_tot = 0, 0

        bid_ref = bidprc[i]
        ask_ref = askprc[i]
        # e.g. if tick_increment == 10 and bid_ref == 17555, will modify bid_ref to 17550
        if bid_ref%tick_increment > 0:
            bid_ref -= bid_ref%tick_increment
        if ask_ref%tick_increment > 0:
            ask_ref += ask_ref%tick_increment

        valid_bid, valid_ask = True, True
        # climb back to find the start of the window
        while j>=0 and times[i]-times[j]<=window_size:
            if (sym_int>0 and sym_int!=sym_ints[j]) or (ignore_sym==sym_ints[j] and ignore_qty>0 and (buyvolumes[j]>=ignore_qty or sellvolumes[j]>=ignore_qty)):
                j -= 1
                continue
            # keep summing trades? don't do it if market is worse in past
            if bidprc[j]<bid_ref:
                valid_bid = False
            if askprc[j]>ask_ref:
                valid_ask = False

            # buying streak against or through offer
            if valid_ask and buyprices[j]>=ask_ref:
                buys_vs_ask += buyvolumes[j]
                prcqty_buys += buyvolumes[j] * buyprices[j]
                qty_buys += buyvolumes[j]
                if buyvolumes[j]: #|||CHANGED THIS
                    count_buys += 1
            # selling streak against or through bid
            if valid_bid and sellprices[j]<=bid_ref:
                sells_vs_bid += sellvolumes[j]
                prcqty_sells += sellvolumes[j] * sellprices[j]
                qty_sells += sellvolumes[j]
                if sellvolumes[j]:
                    count_sells += 1
            j -= 1
        streaker_results[i, 0] = buys_vs_ask
        streaker_results[i, 1] = sells_vs_bid
        streaker_results[i, 2] = count_buys
        streaker_results[i, 3] = count_sells
        if qty_buys > 0:
            vwap_results[i, 0] = <double>prcqty_buys / <double>qty_buys
            prcqty_tot += prcqty_buys
            qty_tot += qty_buys
        if qty_sells > 0:
            vwap_results[i, 1] = <double>prcqty_sells / <double>qty_sells
            prcqty_tot += prcqty_sells
            qty_tot += qty_sells
        if qty_tot > 0:
            vwap_results[i, 2] = <double>prcqty_tot / <double>qty_tot
    return streaker_results, vwap_results

