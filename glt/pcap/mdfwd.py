import scapy.all

from collections import namedtuple
from glt.net import MulticastSocket
from struct import Struct
from time import sleep


FWD_EVENT_IDX_NUM_LEVELS = 8


FwdEvent = namedtuple(
    'FwdEvent', 
    [
        'serverid', 'secid', 
        'seqnum', 'time_sec', 'time_ns', 
        'event_type', 'status', 'last_tradeside', 'num_levels', 
        'last_tradeprc', 'last_tradeqty', 'volume',
        'bid_change', 'ask_change', 
        'indicative', 'replay', 'levels'
    ]
)


BookLevel = namedtuple('BookLevel', ['bidprc', 'bidqty', 'askprc', 'askqty', 'bidpar', 'askpar'])


class MDFWDReader:
    def __init__(self):
        self.header_struct = Struct('<iiQQQbbbb4xddddddB7x')
        self.level_struct = Struct('<ddddII')
        self.md = None
        
    def process(self, payload):
        header_struct = self.header_struct
        level_struct = self.level_struct
        hsize = header_struct.size
        lsize = level_struct.size
        header = header_struct.unpack(payload[:hsize])
        levels = []
        num_levels = header[FWD_EVENT_IDX_NUM_LEVELS]
        start = hsize
        for i in range(num_levels):
            levels.append(BookLevel(*level_struct.unpack(payload[start:start+lsize])))
            start += lsize
        self.md = FwdEvent(*(header + (levels, )))
        return self.md


class MDFWDReplay(MDFWDReader):
    def __init__(self, pcap_filename, iface, addr):
        self.pcap_filename = pcap_filename
        self.iface = iface
        self.addr = addr
        # open pcap
        self.pcap = scapy.all.rdpcap(pcap_filename)
        # init reader 
        self.reader = MDFWDReader()
        # init publisher
        self.publisher = MulticastSocket(iface, addr, publish=True)

    def replay_all(self, show_contents=False, speed=None):
        last_time = None
        if speed <= 0:
            speed = None
        for pkt in self.pcap:
            payload = pkt.load
            if show_contents:
                md = self.reader.process(payload)
                print(md)
            self.publisher.send(payload)
            if last_time is not None and speed is not None:
                dt = (pkt.time - last_time) / float(speed)
                sleep(dt)
            last_time = pkt.time
