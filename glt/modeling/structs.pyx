cimport cython
from cython.operator cimport dereference as deref
from numpy cimport ndarray
from libc.math cimport isnan
from glt.math.helpful_functions cimport *

from numpy import float64, int64

# global variables
cdef list tradedata_dtype = [
    ('time', int64), 
    ('side', int64), 
    ('price', float64), 
    ('qty', int64)
]

# MarketState functions
cdef ndarray initialized_market_states(df):
    cdef:
        long n = df.shape[0]
        list relevant_cols = [
            ('bidprc0', float64), 
            ('bidprc1', float64), 
            ('bidprc2', float64), 
            ('bidprc3', float64), 
            ('bidprc4', float64), 
            ('bidqty0', int64), 
            ('bidqty1', int64),
            ('bidqty2', int64),
            ('bidqty3', int64),
            ('bidqty4', int64),
            ('bidpar0', int64),
            ('bidpar1', int64),
            ('bidpar2', int64),
            ('bidpar3', int64),
            ('bidpar4', int64),
            ('askprc0', float64),
            ('askprc1', float64),
            ('askprc2', float64),
            ('askprc3', float64),
            ('askprc4', float64),
            ('askqty0', int64),
            ('askqty1', int64),
            ('askqty2', int64),
            ('askqty3', int64),
            ('askqty4', int64),
            ('askpar0', int64),
            ('askpar1', int64),
            ('askpar2', int64),
            ('askpar3', int64),
            ('askpar4', int64),
            ('buyprice', float64),
            ('buyvolume', int64),
            ('sellprice', float64),
            ('sellvolume', int64)
        ]
        str col
        ndarray mkt_structs = ndarray(n, relevant_cols)

    for col in map(lambda x: x[0], relevant_cols):
        if col in df:
            mkt_structs[col] = df[col]
        else:
            mkt_structs[col] = 0

    return mkt_structs

# we only need to zero out the qtys
cdef MarketState marketstate_zeroed() nogil:
    cdef MarketState mkt
    mkt.bidqty0 = 0
    mkt.bidqty1 = 0
    mkt.bidqty2 = 0
    mkt.bidqty3 = 0
    mkt.bidqty4 = 0
    mkt.askqty0 = 0
    mkt.askqty1 = 0
    mkt.askqty2 = 0
    mkt.askqty3 = 0
    mkt.askqty4 = 0
    return mkt

# EvaluatedMarket functions
cdef EvaluatedMarket new_evaluated_market(MarketState *mkt, long min_qty, long min_par, double vol) nogil:
    cdef EvaluatedMarket emkt

    if isnan(mkt.bidprc0):
        emkt.bidprc = 0.01
        emkt.good_bid = True
    elif mkt.bidpar0 >= min_par and mkt.bidqty0 >= min_qty:
        emkt.bidprc = mkt.bidprc0
        emkt.good_bid = True
    elif mkt.bidpar1 >= min_par and mkt.bidqty1 >= min_qty:
        emkt.bidprc = mkt.bidprc1
        emkt.good_bid = True
    elif mkt.bidpar2 >= min_par and mkt.bidqty2 >= min_qty:
        emkt.bidprc = mkt.bidprc2
        emkt.good_bid = True
    elif mkt.bidpar3 >= min_par and mkt.bidqty3 >= min_qty:
        emkt.bidprc = mkt.bidprc3
        emkt.good_bid = True
    elif mkt.bidpar4 >= min_par and mkt.bidqty4 >= min_qty:
        emkt.bidprc = mkt.bidprc4
        emkt.good_bid = True
    elif double_eq(vol, 0):
        emkt.bidprc = mkt.bidprc0
        emkt.good_bid = True
    else:
        emkt.good_bid = False

    if mkt.askpar0 >= min_par and mkt.askqty0 >= min_qty:
        emkt.askprc = mkt.askprc0
        emkt.good_ask = True
    elif mkt.askpar1 >= min_par and mkt.askqty1 >= min_qty:
        emkt.askprc = mkt.askprc1
        emkt.good_ask = True
    elif mkt.askpar2 >= min_par and mkt.askqty2 >= min_qty:
        emkt.askprc = mkt.askprc2
        emkt.good_ask = True
    elif mkt.askpar3 >= min_par and mkt.askqty3 >= min_qty:
        emkt.askprc = mkt.askprc3
        emkt.good_ask = True
    elif mkt.askpar4 >= min_par and mkt.askqty4 >= min_qty:
        emkt.askprc = mkt.askprc4
        emkt.good_ask = True
    elif double_eq(vol, 0):
        emkt.askprc = mkt.askprc0
        emkt.good_ask = True
    else:
        emkt.good_ask = False

    return emkt


# TradeData
cdef TradeData[:] new_tradedata_array(size_t alloced):
    cdef TradeData[:] tdata = ndarray(alloced, dtype=tradedata_dtype)
    return tdata


cdef class VolumeAccumulator:
    def __cinit__(self, MarketSide side, double price, long time_window, bint any_trade=False, size_t alloced=10):
        self.alloced = alloced
        self.any_trade = any_trade
        self.tdata = new_tradedata_array(self.alloced)
        self._init()
 
        self.side = side
        self.price = price
        self.time_window = time_window
        
    cdef void _init(self) nogil:
        self.head = 0
        self.length = 0
        self.ref_time = 0
        self.totalqty = 0
        self.priceqty = 0
        
    cdef bint _equals_price(self, double price) nogil:
        return double_eq(price, self.price)

    cdef bint _valid_trade(self, MarketSide side, double price) nogil:
        return self.any_trade or (self.side == Bid and double_gte(price, self.price)) or (self.side == Ask and double_lte(price, self.price))
    
    cdef long _calc_ref_time(self, long time) nogil:
        return time - self.time_window

    cdef bint _valid_time(self, long time) nogil:
        return time >= (self.ref_time + self.time_window)

    @cython.boundscheck(False)
    cdef void _update_price(self, double price, bint reset=True):
        cdef:
            size_t i, j, head = self.head, alloced = self.alloced, new_length = self.length
            long totalqty = self.totalqty
            TradeData tdat
            TradeData[:] tdata
            double old_price = self.price, priceqty = self.priceqty
        self.price = price
        if reset:
            self._init()
            return
        elif self.any_trade or (self.side == Bid and double_lte(price, old_price)) or (self.side == Ask and double_gte(price, old_price)):
            return
        tdata = self.tdata
        j = 0
        for 0 <= i < self.length:
            tdat = tdata[(head+i)%alloced]
            if self._valid_trade(<MarketSide>tdat.side, tdat.price):
                tdata[(head+j)%alloced] = tdat
                j += 1
            else:
                new_length -= 1
                totalqty -= tdat.qty
                priceqty -= tdat.price * tdat.qty
        self.length = new_length
        self.totalqty = totalqty
        self.priceqty = priceqty
        
    def update_price(self, double price):
        self._update_price(price)
        
    cdef void _update_side(self, MarketSide side) nogil:
        # reset everything since if the side changed
        if side != self.side:
            self.side = side
            self._init()
            
    def update_side(self, MarketSide side):
        self._update_side(side)
        
    @cython.boundscheck(False)
    cdef void _resize_tdata(self):
        cdef:
            size_t i, head = self.head, alloced = self.alloced, new_alloced
            TradeData[:] tdata = self.tdata, new_tdata
        new_alloced = 2 * alloced
        new_tdata = new_tradedata_array(new_alloced)
        for 0 <= i < alloced:
            new_tdata[i] = tdata[(head + i)%alloced]
        self.head = 0
        self.alloced = new_alloced
        self.tdata = new_tdata
        
    @cython.boundscheck(False)
    cdef void _push_tdata(self, TradeData *tdat):
        if self.length == self.alloced:
            self._resize_tdata()
        self.tdata[(self.head+self.length)%self.alloced] = deref(tdat)
        self.length += 1
        
    cdef void _add_trade(self, long time, MarketSide side, double price, long qty):
        cdef TradeData tdat
        if not self._valid_time(time) or not self._valid_trade(side, price):
            return
        self._remove_trades(time)
        tdat.time = time
        tdat.side = side
        tdat.price = price
        tdat.qty = qty
        self._push_tdata(&tdat)
        self.totalqty += qty
        self.priceqty += price * qty
        
    def add_trade(self, long time, MarketSide side, double price, long qty):
        self._add_trade(time, side, price, qty)
        
    @cython.boundscheck(False)
    cdef void _remove_trades(self, long time) nogil:
        cdef:
            size_t i, head = self.head, alloced = self.alloced, idx, length = self.length, new_length
            TradeData[:] tdata = self.tdata
            long ref_time, totalqty = self.totalqty, qty
            double priceqty = self.priceqty
        ref_time = self._calc_ref_time(time)
        new_length = length
        for 0 <= i < length:
            idx = (head+i)%alloced
            if tdata[idx].time >= ref_time:
                break
            else:
                new_length -= 1
                qty = tdata[idx].qty
                totalqty -= qty
                priceqty -= tdata[idx].price * qty
        if new_length == 0:
            self.head = 0
        else:
            self.head = idx
        self.length = new_length
        self.totalqty = totalqty
        self.priceqty = priceqty
        self.ref_time = ref_time

    def remove_trades(self, long time):
        self._remove_trades(time)
        
    def __len__(self):
        return self.length

    def __repr__(self):
        args = self.price, self.side, self.length, self.totalqty, self.ref_time
        return 'VolumeAccumulator(price=%0.5f, side=%d, length=%d, totalqty=%d, ref_time=%d)' % args
