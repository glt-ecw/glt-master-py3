# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
import datetime
from libc.stdint cimport *
from libc.math cimport NAN, ceil, floor, log, sqrt
from glt.modeling.filter_base cimport FilterModel, MDFilter
import glt.math.bs as bsm
from glt.math.bezier import BezierFit
from glt.md.standard cimport MDOrderBook
from glt.math.helpful_functions cimport double_lt, double_gt, double_eq, double_gte, double_lte
from numpy cimport ndarray
from numpy import isinf, isnan, zeros, ones, nan, mean, dot, sum, diag
import numpy as np
from numpy.linalg import LinAlgError, inv
from pandas import Timestamp, to_timedelta, to_datetime
from glt.math cimport kalman
import glt.db
import sys
import json
import os

EPSILON = 1e-7

#pythonize/uncythonize/demystify these functions for some flexibility
def less_than(a, b):
    return double_lt(a, b)
def greater_than(a, b):
    return double_gt(a, b)

#for testing new fitting
class WingModelFit:
    def __init__(self, tte=0):
        self.X = 0
        self.coefs = 0
        self.tte = tte
    
    def set_tte(self, tte):
        self.tte = tte

    def compute_x(self, xpts):
        n = len(xpts)
        X = zeros((n, 7))
        puts = xpts<=0
        calls = xpts>=0 #~puts
        
        X[calls, 0] = xpts[calls]
        X[calls, 1] = X[calls, 0]**2
        X[calls, 2] = np.log(X[calls, 0])
        X[puts, 3] = 0 - xpts[puts]
        X[puts, 4] = X[puts, 3]**2
        X[puts, 5] = np.log(X[puts, 3])
        X[puts, 6] = 1. / X[puts, 3]
        X[:, 2][isinf(X[:, 2])] = 0
        X[:, 5][isinf(X[:, 5])] = 0
        X[:, 6][isinf(X[:, 6])] = 0

        self.X = X
        return X
    
    def fit(self, k_points, spot, w=zeros(0)):
        weight_flag = len(w) > 1
        
        diffs = spot - k_points[:,0]
        atm, atm_vol = k_points[diffs==diffs[diffs>0].min()][0]
        
        points = k_points.copy()
        points[:,0] = np.log(points[:,0]/atm) / (points[:,1] * sqrt(self.tte))
        X = self.compute_x(points[:, 0])
        
        if weight_flag:
            W = diag(w)
            XTX = dot(X.T, dot(W, X))
            self.coef = dot(dot(inv(XTX), X.T), dot(W,points[:, [1]] - atm_vol)) 
        else:
            XTX = dot(X.T, X)
            self.coef = dot(dot(inv(XTX), X.T), points[:, [1]] - atm_vol)
        self.atm_vol = atm_vol

    def transform(self, k_points, spot):
        diffs = spot - k_points[:,0]
        atm, atm_vol = k_points[diffs==diffs[diffs>0].min()][0]
        
        points = k_points.copy()
        points[:,0] = np.log(points[:,0]/atm) / (points[:,1] * sqrt(self.tte))
        X = self.compute_x(points[:, 0])
        tmp = self.atm_vol + dot(X, self.coef)
        points[:,1] = tmp[:,0]
        return points


cdef class OptionMDFilter(MDFilter):

    def __init__(self, int32_t secid, int32_t min_qty, int32_t min_par, double min_bid,
                 double pause_window, double mkt_width, bint leader, bint weighted, 
                 double strike, int32_t cp, double ir, double tte, list px_tick_pair, 
                 double spot_tick_incr):

        MDFilter.__init__(self, secid, min_qty, min_par, pause_window, 
            mkt_width, leader, weighted)
        self.strike = strike
        self.cp = cp
        self.ir = ir
        self.tte = tte
        self.min_bid = min_bid
        for pair in px_tick_pair:
            if pair[0] == 0:
                self.tick_incr = pair[1]
            else:
                self.big_tick_incr = pair[1]
                self.big_tick_px = pair[0]
        self.spot_tick_incr = spot_tick_incr
        self.spot = 0.0
        self.vol = {'bid': nan, 'ask': nan, 'mid': nan, 'smooth': 0, 'dy' : 0}
        self.top_vol = {'bid' : nan, 'ask': nan, 'mid':nan}
        self.prev_vols = {'bid': [0], 'ask': [0]}
        self.m = 0
        self.delta = 0
        self.vega = 0
        self.otm = False

        self.MAX_PREV_VOLS_LEN = 3
  
    def update_parameters(self, opt_min_qty_par, mkt_width, pause_window, ir, min_bid):
        #create base parameter dict
        base_params = {
            'min_qty' : opt_min_qty_par[0],
            'min_par' : opt_min_qty_par[1],
            'mkt_width' : mkt_width,
            'pause_window' : pause_window
        }
        #pass to MDFilter.update_parameters()
        super(OptionMDFilter, self).update_parameters(**base_params)
        
        #set OptionMDFilter parameters one by one here
        self.ir = ir
        self.min_bid = min_bid

    cdef void read_dump(self, double vol, double spot):
        self.vol['smooth'] = vol
        self.spot = spot
        self.compute_greeks()
        self.otm_check()
 
    cdef void calculate_top_vol(self, MDFilter spot, double roll):
        cdef:
            double mkt_width, temp
            str side

        #filling out top of book vs taker side of futures market
        self.spot = spot.top['mid'] + roll
        mkt_width = (spot.top['ask'] - spot.top['bid']) * 1.0
        temp = self.spot_tick_incr
        if double_gt(mkt_width, self.spot_tick_incr):
            self.spot_tick_incr = 0

        #calc top vol
        for side in ['bid', 'ask']:
            self.top_vol[side] = self._get_vol(side, 'top')
        
        #set spot_tick_incr back 
        self.spot_tick_incr = temp 

    cdef void otm_check(self):
        #set otm flag
        if (double_lt(self.strike, self.spot) and self.cp == -1) or (
            double_gte(self.strike, self.spot) and self.cp == 1):
            self.otm = True
        else:
            self.otm = False

    cdef void calculate_vol(self, MDFilter spot, double roll):
        #spot must be a MIDPRICE
        cdef:
            int i, logsecid = 588014 
            double current_vol
       
        self.spot = spot.md['mid'] + roll

        self.otm_check()

        #if there is no bid, fill it with no_bid price
        if double_eq(self.md['bid'], 0):
            self.force_price(self.min_bid, 'bid')

        for side in ['bid', 'ask']:
        
            current_vol = self._get_vol(side, 'md')

            # Bail out: vol hasnt changed, vol calc is bad, md too wide (tight = False)
            if (double_eq(current_vol, self.vol[side]) or double_eq(current_vol, -1) or 
                    (not self.tight and not isnan(self.vol[side]))):
                if self.secid == logsecid:
                    print 'bail (vol same: %s, curr vol -1: %s, nottight notnan: %s' % (double_eq(current_vol, self.vol[side]), double_eq(current_vol, -1),(not self.tight and not isnan(self.vol[side])))
                    print side, current_vol, self.vol[side]
                break 

            # set comparison funcs to appropriate values for the side being checked
            # (ie a "worse" ask is higher whereas a "worse" bid is lower.  Pretty boss eh?)
            if side == 'bid':
                better_market = greater_than
                worse_market = less_than
                furthest_market = min
            else:
                better_market = less_than
                worse_market = greater_than
                furthest_market = max
            
            #case 1: new bid/ask vol is better existing vol bid/ask; update it & put old val
            #        in prev_vols
            #case 2: existing bid/ask vol explains new bid/ask price; do nothing
            #case 3: new bid/ask vol is worse than anyting in prev_vols; wipe out prev_vols 
            #        and update bid/ask vol
            #case 4: there might be a vol in prev_vols that explains price, if so update bid/ask 
            #        vol dict. if not, remove all values better than new bid/ask vol and set 
            #        vol dict to new bid/ask vol
            if better_market(current_vol, self.vol[side]) or isnan(self.vol[side]):
                if self.secid == logsecid:
                    print 'better market'
                self._update_prev_vols(self.vol[side], side)
                self.vol[side] = current_vol
            elif double_eq(self._get_price(self.vol[side], side), self.md[side], 0.001):
                if self.secid == logsecid:
                    print 'equal market'
                pass #do nothing
            elif worse_market(current_vol, furthest_market(self.prev_vols[side])):
                self.prev_vols[side] = [0]
                self.vol[side] = current_vol
                if self.secid == logsecid:
                    print 'worse market'
            else:
                i = 1 #used to slice list appropriately
                # This part may take some additional explanation...
                # Loop through all the values in the prev_vols list for the side,
                # if the price that vol would be equals the price in the market, use that vol
                # if the price that vol equals is WORSE, use the new vol and remove vols
                # in the list that are BETTER.
                if self.secid == logsecid:
                    print 'else'
                for pv in self.prev_vols[side]:
                    price = self._get_price(pv, side)
                    if double_eq(price, self.md[side], 0.001):
                        self.prev_vols[side] = self.prev_vols[side][i:]
                        self.vol[side] = pv
                        break
                    elif worse_market(price, self.md[side]):
                        self.prev_vols[side] = self.prev_vols[side][i-1:]
                        self.vol[side] = current_vol
                        break
                    i += 1 
                
                # cant have a len of zero or max/min functions break
                if len(self.prev_vols[side]) == 0:
                    self.prev_vols[side] = [0]

            if self.secid == logsecid:
                print 'current %s vol: %s' % (side, current_vol)
        #calc mid
        self.vol['mid'] = 0.5 * (self.vol['ask'] + self.vol['bid']) 
        self.compute_greeks()

        if self.secid == logsecid:
            print '>>>>>>>>>>>>>>>>'
            print 'OptionsMDFilter calculate_vol()'
            print 'mid vol: %s' % self.vol['mid']
            print 'spot mid: %s' % self.spot
            print 'top bid vol, ask vol: %s, %s' % (self.top_vol['bid'], self.top_vol['ask'])
            print 'bid vol, ask vol: %s, %s' % (self.vol['bid'], self.vol['ask'])
            print 'prev bid vols: %s' % self.prev_vols['bid']
            print 'prev ask vols: %s' % self.prev_vols['ask']
            print 'md ts: %s, seqnum: %s' % (self.md['ts'], self.md['seqnum'])
            print 'top md bid: %s, ask, %s' % (self.top['bid'], self.top['ask'])
            print 'md bid: %s, ask: %s' % (self.md['bid'], self.md['ask'])
            print 'vega: %s' % self.vega
            print '----------------'

    cdef void compute_greeks(self):
        if self.vol['mid'] == 0 or isnan(self.vol['mid']):
            vol = self.vol['smooth']
        else:
            vol = self.vol['mid']
            
        self.delta = bsm.delta(self.strike, self.spot, vol, self.tte, self.ir, self.cp)
        self.vega = bsm.vega(self.strike, self.spot, vol, self.tte, self.ir)
        self.m = log(self.strike / self.spot) / sqrt(self.tte)
               

    cdef void set_tte(self, double tte):
        self.tte = tte

    ##################
    # helper methods #
    ##################

    cdef void _update_prev_vols(self, double vol, str side):
        #nans will make your life hell
        if isnan(vol):
            return
        #make sure this vol doesnt exist in the lsit
        for pv in self.prev_vols[side]:
            if double_eq(pv, vol):
                return
        #put new vol value into front of the list
        if len(self.prev_vols[side]) == self.MAX_PREV_VOLS_LEN:
            self.prev_vols[side] = [vol] + self.prev_vols[side][:self.MAX_PREV_VOLS_LEN-1]
        elif self.prev_vols[side] == [0]:
            #first time through
            self.prev_vols[side] = [vol]
        else:
            self.prev_vols[side].insert(0, vol)

    cdef double _get_spot(self, str side):
        #get the appropriate side of spot
        if (self.cp == 1 and side == 'bid') or (self.cp == -1 and side == 'ask'):
            spot = self.spot - self.spot_tick_incr * 0.5
        elif (self.cp == 1 and side == 'ask') or (self.cp == -1 and side == 'bid'):
            spot = self.spot + self.spot_tick_incr * 0.5
        return spot 

    cdef double _get_vol(self, str side, str dict_type):
        cdef:
            double vol
        #get the implied vol for the price on side passed (MAKER vols)
        if dict_type == 'md':
            spot = self._get_spot(side)
            price = self.md[side]
        elif dict_type == 'top':
            #top vols are used to see what vol the TAKER would trade (ie buy call lift futs)
            opp_side = self._opp_side(side)
            spot = self._get_spot(opp_side)
            price = self.top[side]
            #to use MAKER vols
            #spot = self._get_spot(side)
            #price = self.top[side]

        if self.tight or dict_type == 'top':
            vol = bsm.implied_vol(spot, price, self.strike, self.tte, self.ir, self.cp)
        else:
            vol = -1
        return vol
        
    cdef double _get_price(self, double vol, str side):
        cdef:
            double spot, tv
        # calculate the price market the vol would be
        spot = self._get_spot(side)

        # calc tv, then round up/down depending on side to the nearest tick increment
        tv = bsm.bs_tv(spot, self.strike, self.tte, vol, self.ir, self.cp)
        
        #round the tv
        if side == 'bid':
            rounded_price = self.round_price_down(tv)
        else:
            rounded_price = self.round_price_up(tv)

        return rounded_price

    cdef double round_price_up(self, double price):
        cdef:
            double offer
        offer = ceil( (price / self.tick_incr) - EPSILON) * self.tick_incr
        if double_gt(offer, self.big_tick_px):
            offer = ceil( (price / self.big_tick_incr) - EPSILON) * self.big_tick_incr
        return offer
            
    cdef double round_price_down(self, double price):
        cdef:
            double bid
        bid = floor( (price / self.tick_incr) + EPSILON) * self.tick_incr
        if double_gt(bid, self.big_tick_px):
            bid = floor( (price / self.big_tick_incr) + EPSILON) * self.big_tick_incr
        return bid
            
    cdef str _opp_side(self, str side):
        cdef:
            str opp_side
        if side == 'bid':
            opp_side = 'ask'
        else:
            opp_side = 'bid'
        return opp_side

class EmptyLogger( object ):
    def __init__(self):
        pass
    def info(self, msg):
        pass
    def debug(self, msg):
        pass

cdef class OptionsFilterModel(object):
    # TODO:
            # - Curve Fitting:
            #                   - make sure no bad vols are going into the fitting
            #                   - ensure arb free curve (all call values increase, all flys >= 0)
            #                   - dynamic bezier degree
            #                   - pass model type from cfg
            #                   - cfg parameter if you dont want to smooth
            # - add __repr__ with output like options.pyx
            # - make np arrays in compute_model object attributes
            # - Roll shit:
            #               - handle when the roll has a market (over ride roll calc parameter)
            #               - store roll and read prev days roll for a seed value
            #               - get alpha/beta from cfg
            # - Read from cfg:
            #               - alpha, beta and prev days roll
    # TEST:
    #       - all Xs above
    #       - self.valid check for md being too wide in OptionMDFilter.calculate_vol
    def __init__(self, shortname, expiry, respline_interval, px_tick_pair, spot_tick_incr, max_opt_width, 
            max_fut_width, ir, opt_min_qty_par, fut_min_qty_par, pause_window, 
            t_offset, min_bid, use_roll, underlying_secid=None, exch_time=True, ts_start=None, name='None',
            logger=None):
        cdef:
            OptionMDFilter opt_md_filter

        self.name = str(name)
        if logger:
            self.logger = logger
        else:
            self.logger = EmptyLogger()
        start_time = datetime.datetime.now()
        self.respline_interval = respline_interval
        self.snapshot = False
        pause_window = float(pause_window)
        self.t_offset = str(t_offset)
        self.opt_contract = str(shortname) + str(expiry)
        self.ts = 0
        self.tte_ts = 0
        self.exch_time = exch_time
        self.ir = ir
        self.spot = 0
        self.pub_count = 0
        self.roll = 0
        #check if month is quarterly, if so you dont need a roll
        if str(expiry)[0] in ['H', 'M', 'U', 'Z']:
            self.use_roll = False
        else:
            self.use_roll = use_roll
        self.min_bid = min_bid
        
        # if ts_start has been passed get time and call _calc_tte()
        if ts_start is not None:
            time = Timestamp(ts_start)
            self._calc_tte(time)
        else:
            #this flag will be True after the first book is received
            self.tte_flag = False
            self.tte = 0

        #db hell: get all securites with shortname (ex: 'KOO') and expiry ('U17')
        # contractDefId 69 is old jpx exch, dont want those
        query = '''
            SELECT options.id as secid, shortName, expDate, strike, optionType, underlyingId
            FROM SECURITIES.securities
            LEFT JOIN SECURITIES.options on securities.id=options.id
            WHERE contractDefId != 69 and left(shortName, 6) = "%s";''' % (self.opt_contract)
        self.conn_defn = {'host' : '10.125.0.200', 'user' : 'rdeio', 'password' : 'fatmanblues'}
        conn = glt.db.MysqlConnection(**self.conn_defn)
        df_secids = conn.frame_query(query)
        conn.close()
        df_secids['cp'] = 3 - 2 * df_secids['optionType']
        #get the index of the strike
        df_secids.sort_values(['strike', 'optionType'], inplace=True)
        df_secids.loc[:, 'idx'] = nan
        df_secids.loc[df_secids['optionType']==1, 'idx'] = df_secids.loc[df_secids['optionType']==1, 'optionType'].expanding().sum()
        df_secids.loc[:, 'idx'] = df_secids['idx'].ffill()
        df_secids['idx'] = df_secids['idx'] - 1   #index starts at zero

        #maps/lists
        self.n_strikes = df_secids['idx'].max() + 1
        self.opt_secids = list(df_secids['secid'])
        self.call_secids = list(df_secids.loc[df_secids['cp']==1, 'secid'])
        self.put_to_call = {}
        for secid in self.call_secids:
            call_strike = df_secids.loc[df_secids['secid']==secid, 'strike'].values[0]
            put_mask = (df_secids['strike']==call_strike) & (df_secids['cp']==-1)
            put_secid = df_secids.loc[put_mask, 'secid'].values[0]
            self.put_to_call[put_secid] = secid

        #get the underlying of those options
        if underlying_secid:
            self.fut_secid = underlying_secid
        else:
            self.fut_secid = df_secids.iloc[0]['underlyingId']

        # Begin arduous process of building self.securities dict
        self.securities = {}

        # first, fill self.securities with OPTIONS
        opt_params = {
                'min_qty' : opt_min_qty_par[0], 'min_par' : opt_min_qty_par[1], 
                'pause_window' : 0, 'mkt_width' : max_opt_width,
                'leader' : False, 'weighted' : False, 'ir' : ir, 'tte' : self.tte,
                'px_tick_pair' : px_tick_pair, 'spot_tick_incr' : spot_tick_incr,
                'min_bid' : min_bid
                }
        #get each index and fill inthat way so the strikes are in order (for backtesting)
        for i in range(len(df_secids)):
            idx = df_secids.iloc[i]['idx']
            params = dict(df_secids.iloc[i][['strike', 'secid', 'cp']])
            params.update(opt_params)
            self.securities[params['secid']] = {
                'filter' : OptionMDFilter(**params),
                'type' : 'opt',
                'idx' : idx
                }

        # then, add FUTURE to self.securities
        fut_params = {
                'min_qty' : fut_min_qty_par[0], 'min_par' : fut_min_qty_par[1], 
                'pause_window' : pause_window, 'mkt_width' : max_fut_width, 
                'leader' : True, 'weighted' : False
                }
        self.securities[self.fut_secid] = {
                'filter' : MDFilter(secid=self.fut_secid, **fut_params),
                'type' : 'fut',
                'idx' : -1
                }

        #Roll setup
        self.roll_opts = {}
        self.init_roll()

        #init bezier objects with various degrees of freedom
        # bez_dict = { degree : { 'bez' : bezier object, 'sse' : sum squared error raw vs sm} }
        self.bez_dict = {}
        for i in range(2, 16):
            self.bez_dict[i] = {}
            self.bez_dict[i]['bez'] = BezierFit(i)
            self.bez_dict[i]['sse'] = -1
        #wing model init
        if self.tte_flag:
            self.wing = WingModelFit(self.tte)
        else:
            self.wing = WingModelFit()

        # load prev vols
        self.read_dump()

        #more maps
        self.secid_to_strike = {}
        self.secid_to_m = {}
        self.secid_to_vega = {}
        for secid in self.call_secids:
            opt_md_filter = self.securities[secid]['filter']
            self.secid_to_strike[secid] = opt_md_filter.strike
            self.secid_to_m[secid] = opt_md_filter.m
            self.secid_to_vega[secid] = opt_md_filter.vega

        end_time = datetime.datetime.now()

    def init_underlying(self, fut_min_qty_par, pause_window, max_fut_width, secid=None):
        #bail if secid is the same
        if secid == self.fut_secid:
            return
        elif secid != None:
            old_secid = self.fut_secid
            self.fut_secid = secid
            # rm old fut secid
            del self.securities[old_secid] 

        # add FUTURE to self.securities
        fut_params = {
                'min_qty' : fut_min_qty_par[0], 'min_par' : fut_min_qty_par[1], 
                'pause_window' : pause_window, 'mkt_width' : max_fut_width, 
                'leader' : True, 'weighted' : False
                }
        self.securities[self.fut_secid] = {
                'filter' : MDFilter(secid=self.fut_secid, **fut_params),
                'type' : 'fut',
                'idx' : -1
                }


    def init_roll(self, use_roll=None):
        #bail if roll is the same
        if use_roll == self.use_roll:
            return
        #set up roll situation
        if use_roll is not None:
            old_roll = self.use_roll
            self.use_roll = use_roll
            #check if prev roll value is in keys (ie if its a secid) and rm it
            if old_roll in self.securities.keys():
                del self.securities[old_roll]

        #if use_roll is a security id add it to the securities dict
        if self.use_roll and self.use_roll != True:
            # TODO unhash and test - and unhash read/dump data roll
            """
            query = '''SELECT id, expDate
                    FROM SECURITIES.futures
                    WHERE id={u}
                    UNION
                    SELECT id, expDate
                    FROM SECURITIES.futureSpreads
                    WHERE id={s}
                    ORDER BY expDate
                    '''.format(u=self.fut_secid, s=self.use_roll)
            conn = glt.db.MysqlConnection(**self.conn_defn)
            df_underlying = conn.frame_query(query)
            fut = df_underlying.loc[(df_underlying['id']==self.fut_secid), 'expDate'].values[0]
            spread = df_underlying.loc[(df_underlying['id']==self.use_roll), 'expDate'].values[0]
            if spread < fut:
                self.roll_mult = -1
            else:
                self.roll_mult = 1
            """
            self.roll_mult = -1
            roll_params = {
                    'min_qty' : 1, 'min_par' : 1, 
                    'pause_window' : 0, 'mkt_width' : 0, 
                    'leader' : False, 'weighted' : False
                    }
            self.securities[self.use_roll] = {
                'filter' : MDFilter(secid=self.use_roll, **roll_params),
                'type' : 'spread',
                'idx' : -2
                }
            #use prev days roll here and in else:
            #TODO is this right>??   can hash this, gets loaded in read_dump
            self.roll = -2 * self.roll_mult
        #use prev days last roll value here
        elif self.use_roll:
            # i dont know how to set this from prev values...
            self.kroll.value = 0
        else:
            self.roll = 0


    def update_parameters(self, respline_interval, max_opt_width, max_fut_width, ir, 
            opt_min_qty_par, fut_min_qty_par, pause_window, min_bid, use_roll, underlying_secid,
            shortname, expiry, spot_tick_incr, px_tick_pair, t_offset, exch_time, name):
        cdef:
            OptionMDFilter opt_md_filter
            MDFilter md_filter

        #handle self. updates
        self.respline_interval = respline_interval
        self.ir = ir
        #update with new secid if necessary (if not it bails)
        self.init_underlying(fut_min_qty_par, pause_window, max_fut_width, underlying_secid)
        self.logger.debug('fucking new secid is %s' % self.fut_secid)
        self.init_roll(use_roll)

        #handle MDFilter updates
        opt_params = {
            'opt_min_qty_par' : opt_min_qty_par,
            'mkt_width' : max_opt_width,
            'pause_window' : pause_window,
            'ir' : ir,
            'min_bid' : min_bid
        }
        fut_params = {
            'min_qty' : fut_min_qty_par[0],
            'min_par' : fut_min_qty_par[1],
            'mkt_width' : max_fut_width,
            'pause_window' : pause_window
        }
        for secid in self.securities.keys():
            sec_type = self.securities[secid]['type']
            if sec_type == 'opt':
                #do opt things
                opt_md_filter = self.securities[secid]['filter']
                opt_md_filter.update_parameters(**opt_params) 
            elif sec_type == 'fut':
                #do fut things
                md_filter = self.securities[secid]['filter']
                md_filter.update_parameters(**fut_params)


    cdef void set_tte(self, MDOrderBook *book):
        """Function to be called from wrapper class that takes a MDOrderBook and gets the timestamp
       then gets tte and fills that field in all the OptionFilters in the self.securities dict.
       Used for backtesting.  For production tte pass something that pd.Timetamp can read to
       the init var ts_start."""
        cdef:
            OptionMDFilter opt_md_filter
       
        #stupid flag but self.exch_time is true it uses book.exch_time (for backtest)
        #  if not it uses book.time (which is exch_time for jpa, pcap time for krx)
        if self.exch_time:
            time = Timestamp(Timestamp(book.exch_time) + to_timedelta(self.t_offset))
        else:
            time = Timestamp(Timestamp(book.time) + to_timedelta(self.t_offset))
       
        self._calc_tte(time)
        #loop through the dict and set tte in the MDFilter
        for val in self.securities.itervalues():
            if val['type'] == 'opt':
                opt_md_filter = val['filter']
                opt_md_filter.set_tte(self.tte)
        #set tte in wing model
        self.wing.set_tte(self.tte)

    cdef void _calc_tte(self, time):
        #get the tte
        self.tte_flag = True
        # set to prev date if in preopen
        if time.hour < 6:
            today = time.date() - to_timedelta('1D')
        else:
            today = time.date()
        self.tdate = str(today)
        self.tte = glt.db.getDTE(self.opt_contract, today) / 260.
        self.logger.info('%s TTE: %s' % (self.name, self.tte))


    cdef void calculate_top_vols(self):
        cdef:
            MDFilter md_filter
            OptionMDFilter opt_md_filter
        #this function just calculates whatever the top market vols are for backtesting purposes

        # set variables to filter objects so you can use them
        md_filter = self.securities[self.fut_secid]['filter']
        
        #loop through opts and calc top vol
        for secid in self.opt_secids:
            opt_md_filter = self.securities[secid]['filter']
            #opt_md_filter.calculate_vol(md_filter.md['mid'], self.roll)
            opt_md_filter.calculate_top_vol(md_filter, self.roll)


    cdef void evaluate_md(self, MDOrderBook *book):
        cdef:
            MDFilter md_filter, spread_filter
            OptionMDFilter opt_md_filter

        #sanity check
        if double_gt(book.bids[0].price, book.asks[0].price):
            return

        # poke the futures each book to check pause window
        #if book.secid == self.fut_secid:
        md_filter = self.securities[self.fut_secid]['filter']
        md_filter.evaluate_md(book)

        #if its an option pass it to an OptionMDFilter
        if self.securities[book.secid]['type'] == 'opt':
            opt_md_filter = self.securities[book.secid]['filter']
            opt_md_filter.evaluate_md(book)
        elif self.securities[book.secid]['type'] == 'spread':
            spread_filter = self.securities[book.secid]['filter']
            spread_filter.evaluate_md(book)
        
        # after each interval take a vol snapshot if the futures are valid 
        if self.exch_time:
            if (book.exch_time > self.ts + self.respline_interval and 
                    self.securities[self.fut_secid]['filter'].valid):
                self.ts = book.exch_time
                self.compute_model(book)
                self.snapshot = True
            else:
                self.snapshot = False
        else:
            if (book.time > self.ts + self.respline_interval and 
                    self.securities[self.fut_secid]['filter'].valid):
                self.ts = book.time
                self.compute_model(book)
                self.snapshot = True
            else:
                self.snapshot = False

    cdef void _build_roll_dict(self):
        cdef:
            OptionMDFilter opt_md_filter
            MDFilter md_filter = self.securities[self.fut_secid]['filter']
            dict roll_opts
            double spot, diff, furthest_diff
            str furthest_strike
       
        spot = md_filter.md['mid']
        roll_opts = {}
        furthest_diff = 0
        furthest_strike = ''
      
        #roll_opts = {str(strike) : {cp : OptionMDFilter, cp : OptionMDFilter, 
        #                           'diff' : abs(strike - spot)}
        for secid in self.opt_secids:
            opt_md_filter = self.securities[secid]['filter']
            diff = abs(opt_md_filter.strike - spot)
            #loop to find largest strike to spot difference
            furthest_diff = 0
            if roll_opts != {}:
                for k in roll_opts.iterkeys():
                    if roll_opts[k]['diff'] > furthest_diff:
                        furthest_diff = roll_opts[k]['diff']
                        furthest_strike = k

            #if strike in dict
            if roll_opts.has_key(str(opt_md_filter.strike)):
                roll_opts[str(opt_md_filter.strike)].update({opt_md_filter.cp : self.securities[secid]['filter']})
            # dict not full
            elif len(roll_opts.keys()) < 4:
                roll_opts[str(opt_md_filter.strike)] = {opt_md_filter.cp : self.securities[secid]['filter'], 'diff' : diff}
            # strike closer to ATM than furthest in dict
            elif double_lt(diff, furthest_diff):
                roll_opts.pop(furthest_strike)
                roll_opts[str(opt_md_filter.strike)] = {opt_md_filter.cp : self.securities[secid]['filter'], 'diff' : diff}
        self.roll_opts = roll_opts
    

    cdef void _update_kroll(self, double roll):
        if isnan(self.kroll.value):
            self.kroll.value = roll
        elif not isnan(roll):
            self.kroll = kalman.new_univariate_value(0.00005, 0.5, roll, self.kroll)
        else: #roll is nan
            self.kroll = kalman.new_univariate_value(0.00005, 0.5, 0, self.kroll)


    cdef void calc_roll_from_market(self):
        cdef:
            MDFilter roll_filter = self.securities[self.use_roll]['filter']

        if isnan(roll_filter.top['bid']) and isnan(roll_filter.top['ask']):
            return 

        self.roll = (roll_filter.top['bid'] + roll_filter.top['ask']) * 0.5 * self.roll_mult
        #self.logger.info('%s CALC_ROLL_FROM_MARKET ', (self.name, self.roll))


    cdef void calc_roll(self):
        cdef:
            MDFilter md_filter = self.securities[self.fut_secid]['filter']
            OptionMDFilter opt_md_filter

        #self.use_roll can be a bool or an int
        if self.use_roll != True:
            self.calc_roll_from_market()
            return

        spot = md_filter.md['mid']
        #check if 2 strikes <= spot, if not rebuild roll dict
        p = 0
        for key in self.roll_opts.iterkeys():
            if double_lte(float(key), spot):
                p += 1
        if p != 2:
            self._build_roll_dict()

        #go through roll dict and calulate roll at each strike
        rolls = []
        for key in self.roll_opts.iterkeys():
            k = float(key)
            k_roll = k
            for cp in [1, -1]:
                opt_md_filter = self.roll_opts[key][cp]
                k_roll += cp * opt_md_filter.top['mid']
            rolls.append(k_roll)
        #put the roll at each strike into kalman
        for r in rolls:
            self._update_kroll(r-spot)
        #set roll for model
        self.roll = self.kroll.value


    cdef void compute_model(self, MDOrderBook *book):
        cdef:
            MDFilter md_filter
            OptionMDFilter opt_md_filter
            #TODO: make this an attribute
            ndarray[double, ndim=2] points = zeros((self.n_strikes, 2), dtype='<f8')
            ndarray[double, ndim=2] transformed = zeros((self.n_strikes, 2), dtype='<f8')
            ndarray[double, ndim=1] smooth = zeros(self.n_strikes, dtype='<f8')
            ndarray[double, ndim=1] dy = zeros(self.n_strikes, dtype='<f8')
            ndarray[double, ndim=1] weights = zeros(self.n_strikes, dtype='<f8')
            ndarray[double, ndim=1] vega = zeros(self.n_strikes, dtype='<f8')
            ndarray[double, ndim=1] tick_incr = ones(self.n_strikes, dtype='<f8')
            ndarray[double, ndim=1] bids = zeros(self.n_strikes, dtype='<f8')
            ndarray[double, ndim=1] asks = zeros(self.n_strikes, dtype='<f8')
            int i=0

        self.new_curve = False

        # set variables to filter objects so you can use them
        md_filter = self.securities[self.fut_secid]['filter']

        #TODO: make sure there are no bad values going into spline
        #      - what are bad values?  sale vol mids?  markets too wide?
        #      - how many bad values keep you from resplining?
        #      and ensure an arb free curve

        # -- ROLL -- 
        #calculate if necessary
        if self.use_roll: 
            if self.use_roll == True:
                if len(self.roll_opts) == 0:
                    self._build_roll_dict()
            self.calc_roll()   

        # -- RAW VOLS --
        # What is going on here?
        #  Loop through the secids and calc vol on each option.
        #  if the option is OTM and the bid is better than the default bid from cfg
        #  put the strike and the vol in the points array (and calc the weight).
        #  In the bezier fitting, we filter the points that are zero and pass
        #  to the fit.  This creates an array with 0s where the option is basically 
        #  worthless and values where you need to spline it.
        for secid in self.opt_secids:
            opt_md_filter = self.securities[secid]['filter']
            #only necessary if the tte changes throughout the day
            #opt_md_filter.set_tte(self.tte)
            opt_md_filter.calculate_vol(md_filter, self.roll)
            opt_md_filter.calculate_top_vol(md_filter, self.roll)
            if opt_md_filter.otm:
                i = self.securities[secid]['idx']
                #prepare points & weights for bez launch
                if double_gt(opt_md_filter.md['bid'], self.min_bid):
                    smooth[i] = opt_md_filter.vol['smooth']
                    points[i, 0] = opt_md_filter.strike
                    points[i, 1] = opt_md_filter.vol['mid']
                    vega[i] = opt_md_filter.vega
                    if double_gt(opt_md_filter.top_vol['ask'], opt_md_filter.top_vol['bid']):
                        #use market taker vols for bids/asks
                        bids[i] = opt_md_filter.top_vol['bid']
                        asks[i] = opt_md_filter.top_vol['ask']
                        #use market maker vols for bids/asks -- if need be
                        #bids[i] = opt_md_filter.vol['bid']
                        #asks[i] = opt_md_filter.vol['ask']
                    if double_gte(opt_md_filter.top['ask'], 50):
                        tick_incr[i] = 5.0
                    # calc weights by raw/mid diff
                    w = 0.02 / ((opt_md_filter.vol['mid'] - (asks[i] + bids[i]) * 0.5) * vega[i] / tick_incr[i]) ** 2
                    if w > 1.0:
                        w = 1
                    weights[i] = w
                    
                    # calc weights by delta; peak at 20 and 50 delta
                    #if double_gt(abs(opt_md_filter.delta), 0.2):
                    #    weights[i] = abs(opt_md_filter.delta) / 0.5
                        #weights[i] = 0.2 / abs(opt_md_filter.delta)    
                    #else:
                    #    weights[i] = abs(opt_md_filter.delta) / 0.2
       
        # -- MASKS -- 
        #create masks to filter opts with no bid or nan vols (no md yet)
        no_bid_mask = points[:, 0]>0
        nan_vol_mask = ~isnan(points[:, 1])
        opt_mask = no_bid_mask & nan_vol_mask
        spread_mask = bids[opt_mask]>0

        #bail if you have no real vols yet
        if len(points[opt_mask]) == 0:
            return
       
        #check your current smoothed vols and bail if they are good
        e1 = bids[opt_mask][spread_mask] - smooth[opt_mask][spread_mask]
        e1[e1<0] = 0
        e2 = smooth[opt_mask][spread_mask] - asks[opt_mask][spread_mask]
        e2[e2<0] = 0
        #error is the smoothed vol outside the bid/ask spread in price (bc its * vega)
        error = (e1 + e2) * vega[opt_mask][spread_mask]
        #for best bid/ask vol bands
        #e3 = e1 + e2
        #error = e3[(e1>=0)&(e2>=0)] * vega[opt_mask][spread_mask][(e1>=0)&(e2>=0)]
        if max(error) == 0:
            self.logger.info('%s : Current smoothed vols still good (%s)' % (self.name, self.roll))
            return
        else:
            current_curve_error = max(error)
            k_off = (',').join(['%s' % x for x in points[:,0][opt_mask][spread_mask][error != 0]])
            self.logger.info('%s : These strikes are off (%s): %s' % (self.name, self.roll, k_off))

        #check to make sure np.dot(points.T, points) is not singluar, 
        #   if so return and dont update anything
        try:
            tmp = inv(dot(points[opt_mask].T, points[opt_mask]))
        except LinAlgError:
            self.logger.info('%s : Singular Matrix: points' % self.name)
            return
       
        #debug logging
        if False:
            print '\/\/\/\/\/\/\/\/\/'
            print Timestamp(book.exch_time)
            print '------'
            print 'k, v, w, vega'
            out = 'k = ['
            for i in range(points[opt_mask].shape[0]):
                out += '%s,' % points[opt_mask][i, 0]
            print out.rstrip(','), ']'
            out = 'v = ['
            for i in range(points[opt_mask].shape[0]):
                out += '%s,' % points[opt_mask][i, 1]
            print out.rstrip(','), ']'
            out = 'w = ['
            for i in range(weights[opt_mask].shape[0]):
                out += '%s,' % weights[opt_mask][i]
            print out.rstrip(','), ']'
            out = 'vega = ['
            for i in range(vega[opt_mask].shape[0]):
                out += '%s,' % vega[opt_mask][i]
            print out.rstrip(','), ']'
            out = 'tick = ['
            for i in range(tick_incr[opt_mask].shape[0]):
                out += '%s,' % tick_incr[opt_mask][i]
            print out.rstrip(','), ']'
          
            out = 'k_spread = ['
            for i in range(points[opt_mask][spread_mask].shape[0]):
                out += '%s,' % points[opt_mask][spread_mask][i,0]
            print out.rstrip(','), ']'
            out = 'bid_spread = ['
            for i in range(bids[opt_mask][spread_mask].shape[0]):
                out += '%s,' % bids[opt_mask][spread_mask][i] 
            print out.rstrip(','), ']'
            out = 'ask_spread = ['
            for i in range(asks[opt_mask][spread_mask].shape[0]):
                out += '%s,' % asks[opt_mask][spread_mask][i] 
            print out.rstrip(','), ']'

            print ''
            print '------'


        # -- SMOOTHED VOLS -- 
        #pump raw vols into each bezier fit and calc error, store the fit with max(error) <= 0
        #   into "good_fit"
        min_sse = current_curve_error #10000 #some arbitrarily high number
        best_fit = None
        good_fit = None
        for key in self.bez_dict.iterkeys():
            if key >= len(points[opt_mask]) - 1 and key != 2:
                break

            bez = self.bez_dict[key]['bez']
            try:
                bez.fit(points[opt_mask], weights[opt_mask])
            except LinAlgError:
                self.bez_dict[key]['sse'] = nan
                self.logger.info('%s : Singular matrix: bez.fit()' % self.name)
                continue
            #smooth (ie transform)
            tformed_vols = bez.transform(points[opt_mask])
            # finding all smoothed vols below bids (e1) and above offers (e2)
            e1 = bids[opt_mask][spread_mask] - tformed_vols[:,1][spread_mask]
            e1[e1<0] = 0
            e2 = tformed_vols[:,1][spread_mask] - asks[opt_mask][spread_mask]
            e2[e2<0] = 0
            #error is the smoothed vol outside the bid/ask spread in price (bc its * vega)
            error = (e1 + e2) * vega[opt_mask][spread_mask]
            #for best bid/ask vol bands
            #e3 = e1 + e2
            #error = e3[(e1>=0)&(e2>=0)] * vega[opt_mask][spread_mask][(e1>=0)&(e2>=0)]
            self.bez_dict[key]['sse'] = max(error)

            #see if the curve is a "good fit" (all smoothed vols are between bid/ask vols
            if self.bez_dict[key]['sse'] <= 0.0:
                good_fit = self.bez_dict[key]['bez']
                break
            # use the "best fit" if no curve meets the thresholds for max error
            if double_lt(self.bez_dict[key]['sse'], min_sse):
                min_sse = self.bez_dict[key]['sse']
                best_fit = self.bez_dict[key]['bez']
                
        if good_fit is not None:
            self.logger.info('%s : Bezier degree: %s (%s)' % (self.name, good_fit.degrees, self.roll))
            for key in self.bez_dict.iterkeys():
                #print key, self.bez_dict[key]['sse']
                if key == good_fit.degrees:
                    break
            #bez transform
            #keeping the nan vols here in order to get sm vols for those strikes too
            transformed[no_bid_mask] = good_fit.transform(points[no_bid_mask])
            dy[no_bid_mask] = good_fit.transform_deriv(points[no_bid_mask])[:,1]     
            
            #wing model fit and transform TODO remove this or set model dynamically
            #weights = (vega[opt_mask] / vega[opt_mask].max()) ** 2
            #self.wing.fit(points[opt_mask], self.spot, weights)
            #transformed[no_bid_mask] = self.wing.transform(points[no_bid_mask], self.spot)
        elif best_fit is not None:
            self.logger.info('%s : Bezier degree good fit NOT FOUND, using degree %s' % (
                self.name, best_fit.degrees))
            #for key in self.bez_dict.iterkeys():
                #print key, self.bez_dict[key]['sse']
            transformed[no_bid_mask] = best_fit.transform(points[no_bid_mask])
            dy[no_bid_mask] = best_fit.transform_deriv(points[no_bid_mask])[:,1]
        else:
            self.logger.info('%s : Current curve is best' % self.name)
            return
          
        # if you increase pub_count you will send out new vols (full_msgs)
        self.spot = md_filter.md['mid']
        self.pub_count += 1

        self.new_curve = True 
        #-- UPDATE OptionsMDFilter --
        #put smoothed vols and dy into OptionsMDFilter (or the sharred array in prod)
        for secid in self.opt_secids:
            opt_md_filter = self.securities[secid]['filter']
            
            i = self.securities[secid]['idx']
            # if there is a value in transformed put it in smoothed vol
            if double_gt(transformed[i,1], 0):
                sm_vol = transformed[i,1]
                vol_dy = dy[i]
            # transformed[i,1] would be zero if it was masked out above (no bid)
            # if it was masked out put the raw vol in there so you dont have nan vols anywhere.
            elif double_gt(opt_md_filter.vol['mid'], 0):
                sm_vol = opt_md_filter.vol['mid']
                vol_dy = 0 
            # by hashing this out you will either use raw vol or previous smoothed vol
            # if there is not md yet
            else:
                sm_vol = nan
                vol_dy = 0

            # if the vol at that strike is not nan set that to sm vol
            if not isnan(sm_vol):
               opt_md_filter.vol['smooth'] = sm_vol
            opt_md_filter.compute_greeks()
            #nko alpha beta = -0.002, -0.07 (alpha + beta * dy)
            #krx alpha beta = -0.00002, -0.4 (alpha + beta * dy)
            opt_md_filter.vol['dy'] = 0 #vol_dy * -0.4 + -0.00002
            
            #update maps... might not need it because you fill in data in options.pyx
            if opt_md_filter.cp == 1:
                self.secid_to_strike[secid] = opt_md_filter.strike
                self.secid_to_m[secid] = opt_md_filter.m
                self.secid_to_vega[secid] = opt_md_filter.vega

        if True:
            if best_fit is not None:
                deg = best_fit.degrees
            elif good_fit is not None:
                deg = good_fit.degrees
            else:
                deg = 'same'
            print 'Bezier degree : %s' % deg

    cdef void dump_data(self):
        cdef:
            OptionMDFilter opt_md_filter

        # store data model to dict
        curr_data = {}
        for secid in self.opt_secids:
            opt_md_filter = self.securities[secid]['filter']
            if opt_md_filter.vol['smooth'] != 0:
                curr_data[str(secid)] = {
                    'vol' : opt_md_filter.vol['smooth'], 'spot' : self.spot
                    } 
            
        # read json file 
        filename = '/tmp/%s_fugazy.json' % self.name
        # load existing file or create new dict
        try:
            with open(filename, 'r+') as f:
                prev_data = json.load(f)
        except (ValueError, IOError) as e:
            prev_data = {}
        
        # for newer data replace existing data, save old data without newer value
        for secid in curr_data.keys():
            if curr_data[secid]['vol'] != 0 and not isnan(curr_data[secid]['vol']):
                prev_data[secid] = curr_data[secid]
        #unhash to test new roll_init thing and on row 1175
        '''
        if self.use_roll:
            prev_data['roll'] = self.roll
        '''
        # write to file
        with open(filename, 'w+') as f:
            json.dump(prev_data, f, indent=4, separators=(',', ' : '))
        self.logger.info('%s : vols dumped to json'% self.name)


    cdef void read_dump(self):
        cdef:
            OptionMDFilter opt_md_filter

        # read json file
        filename = '/tmp/%s_fugazy.json' % self.name
        try:
            with open(filename, 'r') as f:
                #if the file exists load it
                self.logger.info('%s : reading json for prev vols'% self.name)
                data_dump = json.load(f)
        except IOError:
            self.logger.info('%s : no json file exists (%s), cannot load prev vols '% (self.name, filename))
            return

        #self.logger.info('%s : vol for secid %s, %s' % (self.name, 591964, data_dump[591964]['vol']) 

        # load data into OptionFilters 
        for sid in self.opt_secids:
            secid = str(int(sid))
            if secid in data_dump.keys():
                opt_md_filter = self.securities[sid]['filter']
                opt_md_filter.read_dump(data_dump[secid]['vol'], data_dump[secid]['spot'] ) 
        '''
        if 'roll' in data_dump.keys():
            self.roll = data_dump['roll']
        ''' 
        self.logger.info('%s : prev vols loaded from json'% self.name)

