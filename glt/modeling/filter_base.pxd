from libc.stdint cimport *
from glt.md.standard cimport MDOrderBook, MAX_DEPTH


cdef class FilterModel:
    cdef:
        readonly:
            dict sec_list
            list leaders, keys, samples, outliers
            int n, prev_outlier, last_z_dir
            double tv, last_val
        void evaluate_md(self, MDOrderBook *book)
        void compute_model(self, int secid)
        
cdef class MDFilter:
    cdef:
        public:
            dict md, md_staging, top
        readonly:
            bint valid, weighted, leader, tight
            int32_t secid, min_qty, min_par, bid_qty, ask_qty
            double tight_mkt, pause_window, bid, ask
        #void update_parameters(self, int32_t min_qty, int32_t min_par, double mkt_width, double pause_window)
        void evaluate_md(self, MDOrderBook *book)
        void get_market(self, MDOrderBook *book)
        void force_price(self, double price, str side)
        void _update_md(self, MDOrderBook *book)
        double _weighted_mid(self, MDOrderBook *book)
