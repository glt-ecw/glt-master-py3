from numpy cimport ndarray
from libc.math cimport round as fround, log as flog, sqrt as fsqrt, ceil as fceil, floor as ffloor, NAN, isnan
from glt.math cimport bs, kalman
from glt.math.helpful_functions cimport *
from glt.modeling.structs cimport *

from numpy import float64
from glt.db import getDTE

cdef int MAX_ITERATIONS = 100
cdef double TICK_PRECISION = 0.001

cdef class RawOption:
    """__init__(self, name, t=0.)
    parameters: name = exchange symbol, t = datetime trade date (slower b/c of db lookup) or tte"""
    cdef:
        public double hi_weight, mid_weight, lo_weight, q, r
        readonly double bid_round, ask_round, bid_px, ask_px, top_bid, top_ask, vol, weight, ir, cp, k, bid_weight, ask_weight, fut_tick
        readonly double s_round, l_round, large_tick
        public double bb_vol, bo_vol, t
        readonly str name, seq_num, opt_seq, fut_seq, bid_reason, ask_reason, reason #, exch
        readonly bint new_bid, new_ask, use_kalman
        public list prev_bid_vols, prev_ask_vols
        readonly kalman.UnivariateNode kalman_node
        double tv_precision
        long md_seqnum
        f
        public bint queue_by_tv
        VolSides curr_vols
        readonly double top_bid_vol, top_ask_vol, mt_bid_vol, mt_ask_vol

#    def __init__(self, name, t, bint queue_by_tv=False, f=None):
#        self.name = name
#        self.bb_vol = 0.0
#        self.bo_vol = 0.0
#        self.bid_round = 0.0
#        self.ask_round = 0.0
#        self.bid_px = 0.0
#        self.ask_px = 0.0
#        self.curr_vols.bid = 0.0
#        self.curr_vols.ask = 0.0
#
#        self.prev_bid_vols = []
#        self.prev_ask_vols = []
#
#        # kalman shizzle
#        self.q = 0.001
#        self.r = 1.0
#        self.reset_kalman()
#
#        self.vol = 0.
#        self.weight = 1.
#        self.hi_weight = 1.
#        self.mid_weight = .6
#        self.lo_weight = .25
#        self.bid_weight = 1.
#        self.ask_weight = 1.
#
#        self.exch = name[:2]
#
#        if not hasattr(t, "year"):
#            self.t = t
#        else:
#            self.t = getDTE('%sO%s1%s' % (name[:2], name[2], name[3]), fromDate=t ) / 260.
#
#        self.cp = 1 if name[5] == 'C' else -1
#        self.k = float64(name[6:])
#
#        #set exchange for tick size (.125 for ES, 2.5 for KO)
#        if self.exch == 'ES':
#            self.fut_tick = .125
#            self.ir = .0035
#            self.tv_precision = 0.1
#        elif self.exch == 'KO':
#            self.fut_tick = 0.025 #2.5
#            self.ir = .015
#            self.t = self.t - (1./260)
#            self.tv_precision = 0.001
#            self.k *= 0.01
#        
#        if self.cp < 0:
#            self.fut_tick *= -1
#
#        #testing/logging
#        self.seq_num = 'none'
#        self.opt_seq = 'none'
#        self.fut_seq = 'none'
#        self.bid_reason = 'none'
#        self.ask_reason = 'none'
#        self.reason = 'none'
#        
#        self.md_seqnum = 0
#        self.f = f
#        self.queue_by_tv = queue_by_tv

    def __init__(self, double cp, double k, double t, double ir, double fut_tick, 
                 double s_round, double l_round, double large_tick, double tv_precision, 
                 double kalman_q, double kalman_r, bint queue_by_tv=False, f=None):
        self.cp = cp
        self.k = k
        self.t = t
        self.ir = ir
        self.fut_tick = fut_tick
        self.s_round = s_round
        self.l_round = l_round
        self.large_tick = large_tick
        self.tv_precision = tv_precision
        self.q = kalman_q #0.001
        self.r = kalman_r #1.0

        if self.cp < 0:
            self.fut_tick *= -1
        
        self.bb_vol = 0.0
        self.bo_vol = 0.0
        self.bid_round = 0.0
        self.ask_round = 0.0
        self.bid_px = 0.0
        self.ask_px = 0.0
        self.curr_vols.bid = 0.0
        self.curr_vols.ask = 0.0
        self.top_bid_vol = 0.0
        self.top_ask_vol = 0.0
        self.mt_bid_vol = 0.0
        self.mt_ask_vol = 0.0

        self.prev_bid_vols = []
        self.prev_ask_vols = []

        # kalman shizzle
        self.reset_kalman()

        self.vol = 0.
        self.weight = 1.
        self.hi_weight = 1.
        self.mid_weight = .6
        self.lo_weight = .25
        self.bid_weight = 1.
        self.ask_weight = 1.

        #testing/logging
        self.seq_num = 'none'
        self.opt_seq = 'none'
        self.fut_seq = 'none'
        self.bid_reason = 'none'
        self.ask_reason = 'none'
        self.reason = 'none'
        
        self.md_seqnum = 0
        self.f = f
        self.queue_by_tv = queue_by_tv
        self.use_kalman = False

    def force_vol(self, double vol):
        self.vol = vol
        
    cdef bint _is_put_strike(self, double spot) nogil:
        return double_lt(self.k, spot)
    
    cdef bint _is_call_strike(self, double spot) nogil:
        return not self._is_put_strike(spot)
   
    cdef bint _is_otm(self, double spot) nogil:
        return (self.cp<0 and self.k<spot) or (self.cp>0 and self.k>=spot)
    
    cdef double _calc_moneyness(self, double spot) nogil:
        return flog(self.k / spot) / fsqrt(self.t)

    cdef double _calc_sd(self, double spot, double manual_vol=NAN) nogil:
        cdef double vol
        if isnan(manual_vol):
            if double_lte(self.vol, 0):
                return NAN
            else:
                vol = self.vol
        else:
            vol = manual_vol
        return self._calc_moneyness(spot) / vol

    cdef double _calc_tv(self, double spot, double v) nogil:
        return bs.tv(self.k, spot, v, self.t, self.ir, self.cp)

    cdef double _calc_delta(self, double spot, double v) nogil:
        return bs.delta(self.k, spot, v, self.t, self.ir, self.cp)

    cdef double _calc_vega(self, double spot, double v) nogil:
        return bs.vega(self.k, spot, v, self.t, self.ir)

    cdef double _calc_implied_vol(self, double spot, double prc, double seed_vol, double tte=NAN) nogil:
        cdef double seed = seed_vol
        if double_lte(seed, 0):
            seed = 1.0
        if isnan(tte):
            tte = self.t
        return bs.ivol(self.k, spot, tte, self.ir, self.cp, prc, seed, TICK_PRECISION, MAX_ITERATIONS)
   
    def is_put_strike(self, double spot):
        return self._is_put_strike(spot)
    
    def is_call_strike(self, double spot):
        return self._is_call_strike(spot)
 
    def is_otm(self, double spot):
        return self._is_otm(spot)
    
    def calc_moneyness(self, double spot):
        return self._calc_moneyness(spot)

    def calc_sd(self, double spot, double manual_vol=NAN):
        return self._calc_sd(spot, manual_vol)

    def calc_tv(self, double spot, double v):
        return self._calc_tv(spot, v)

    def calc_delta(self, double spot, double v):
        return self._calc_delta(spot, v)

    def calc_vega(self, double spot, double v):
        return self._calc_vega(spot, v)

    def calc_implied_vol(self, double spot, double prc, double seed_vol, double tte=NAN):
        return self._calc_implied_vol(spot, prc, seed_vol, tte=tte)
    
    cpdef set_weights(self, double hi, double mid, double lo):
        self.lo_weight = lo
        self.mid_weight = mid
        self.hi_weight = hi

    cpdef public book_update(self, double bid, double ask, double topbid, double topask, str seq_num):
        """book_update(self, double bid, double ask, str seq_num)
    parameters: bid, ask, seq num (str)
    State 1 of rawStrike object: sets bid and ask price and opt seq num.
    Must call calc_vol() to actually calculate vol."""
        self.bid_px = bid
        self.ask_px = ask
        self.top_bid = topbid
        self.top_ask = topask
        self.opt_seq = seq_num

    cpdef calc_raw_vol(self, WrappedMarketState w, double spot, long ts, long min_par, long min_qty, str opt_seqnum, str fut_seqnum, str timestr, bint debug=False, double tte=NAN):
        cdef:
            MarketState *mkt = w.mkt
            EvaluatedMarket evaluated_mkt
            bint use_kalman = False
        
        self.top_bid = mkt.bidprc0
        self.top_ask = mkt.askprc0

        evaluated_mkt = new_evaluated_market(mkt, min_qty, min_par, self.vol)

        # compute kalman on vol based on new market; might need it if either market is bad
        if evaluated_mkt.good_bid:
            self.bid_px = evaluated_mkt.bidprc
        else:
            use_kalman = True

        if evaluated_mkt.good_ask:
            self.ask_px = evaluated_mkt.askprc
        else:
            use_kalman = True

        if use_kalman:
            #pass
            self.update_kalman_node(spot, mkt.bidprc0, mkt.askprc0)
            self.vol = self.kalman_node.value
        else:
            self.calc_vol(spot, ts, fut_seqnum, timestr, debug)
            self.reset_kalman(self.vol)
        self.use_kalman = use_kalman

        # redundant
        self.top_bid_vol = self._calc_implied_vol(spot - self.fut_tick, mkt.bidprc0, self.bb_vol, tte=tte)
        self.top_ask_vol = self._calc_implied_vol(spot + self.fut_tick, mkt.askprc0, self.bo_vol, tte=tte)
        self.mt_bid_vol = self._calc_implied_vol(spot + self.fut_tick, mkt.bidprc0, self.bb_vol, tte=tte)
        self.mt_ask_vol = self._calc_implied_vol(spot - self.fut_tick, mkt.askprc0, self.bo_vol, tte=tte)

    cpdef calc_raw_vol_mdfwd(self, tuple deio_md, int n_levels, double spot, long ts, 
            long minpar, long minqty, long opt_seqnum, long fut_seqnum):
        cdef:
            WrappedMarketState w = WrappedMarketState()
            MarketState mkt = marketstate_zeroed()
            int nh = len(deio_md) - 6*n_levels

        mkt.bidprc0 = deio_md[nh]
        mkt.bidqty0 = <long>deio_md[nh+1]
        mkt.askprc0 = deio_md[nh+2]
        mkt.askqty0 = <long>deio_md[nh+3]
        mkt.bidpar0 = deio_md[nh+4]
        mkt.askpar0 = deio_md[nh+5]
        if n_levels > 1:
            mkt.bidprc1 = deio_md[nh+6]
            mkt.bidqty1 = <long>deio_md[nh+7]
            mkt.askprc1 = deio_md[nh+8]
            mkt.askqty1 = <long>deio_md[nh+9]
            mkt.bidpar1 = deio_md[nh+10]
            mkt.askpar1 = deio_md[nh+11]
        if n_levels > 2:
            mkt.bidprc2 = deio_md[nh+12]
            mkt.bidqty2 = <long>deio_md[nh+13]
            mkt.askprc2 = deio_md[nh+14]
            mkt.askqty2 = <long>deio_md[nh+15]
            mkt.bidpar2 = deio_md[nh+16]
            mkt.askpar2 = deio_md[nh+17]
        if n_levels > 3:
            mkt.bidprc3 = deio_md[nh+18]
            mkt.bidqty3 = <long>deio_md[nh+19]
            mkt.askprc3 = deio_md[nh+20]
            mkt.askqty3 = <long>deio_md[nh+21]
            mkt.bidpar3 = deio_md[nh+22]
            mkt.askpar3 = deio_md[nh+23]
        if n_levels > 4:
            mkt.bidprc4 = deio_md[nh+24]
            mkt.bidqty4 = <long>deio_md[nh+25]
            mkt.askprc4 = deio_md[nh+26]
            mkt.askqty4 = <long>deio_md[nh+27]
            mkt.bidpar4 = deio_md[nh+28]
            mkt.askpar4 = deio_md[nh+29]
        w.mkt = &mkt
        
        self.calc_raw_vol(w, spot, ts, minpar, minqty, str(opt_seqnum), str(fut_seqnum), 'nope')

    # let's deprecate this ASAP pls
    cpdef raw_vol_calc(self, list book, double spot, long ts, long min_par, long min_qty, str opt_seq_num, str fut_seq_num):
        cdef:
            WrappedMarketState w = WrappedMarketState()
            MarketState mkt

        mkt.bidpar2 = book[0]
        mkt.bidqty2 = book[1]
        mkt.bidprc2 = book[2]
        mkt.bidpar1 = book[3]
        mkt.bidqty1 = book[4]
        mkt.bidprc1 = book[5]
        mkt.bidpar0 = book[6]
        mkt.bidqty0 = book[7]
        mkt.bidprc0 = book[8]
        mkt.askprc0 = book[9]
        mkt.askqty0 = book[10]
        mkt.askpar0 = book[11]
        mkt.askprc1 = book[12]
        mkt.askqty1 = book[13]
        mkt.askpar1 = book[14]
        mkt.askprc2 = book[15]
        mkt.askqty2 = book[16]
        mkt.askpar2 = book[17]
        w.mkt = &mkt

        self.calc_raw_vol(w, spot, ts, min_par, min_qty, opt_seq_num, fut_seq_num, 'nope')
        
    cpdef raw_vol_calc_simple(self, double bidprc, double askprc, double topbid, double topask, double spot):
        cdef:
            WrappedMarketState w = WrappedMarketState()
            MarketState mkt
        
        if double_eq(bidprc, topbid):
            mkt.bidprc0 = bidprc
            mkt.bidqty0 = 101
            mkt.bidpar0 = 11
        else:
            mkt.bidprc0 = topbid
            mkt.bidqty0 = 1
            mkt.bidpar0 = 1
            mkt.bidprc1 = bidprc
            mkt.bidqty1 = 101
            mkt.bidpar1 = 11
            
        if double_eq(askprc, topask):
            mkt.askprc0 = askprc
            mkt.askqty0 = 101
            mkt.askpar0 = 11
        else:
            mkt.askprc0 = topask
            mkt.askqty0 = 1
            mkt.askpar0 = 1
            mkt.askprc1 = askprc
            mkt.askqty1 = 101
            mkt.askpar1 = 11
            
        w.mkt = &mkt
        
        self.calc_raw_vol(w, spot, 1, 10, 100, 'what', 'okay', 'nope', debug=True)
        

    cdef calc_vol(self, double spot, long ts, str seq_nums, str whenCalled, bint debug=False):
        """calc_vol(self, double spot, long ts, str seq_nums, str whenCalled)
    parameters: spot (as mid price), ts (as long), seq num (str), extra str for debugging
    Stage 2 of rawStrike object: calculates vol based on bid/ask prices you set in book_update()
    call (must have done this previously).
    Updates attributes: vol, bb_vol, bo_vol, reason and seq num of fut and opt"""

        #add fut seqnum to existing opt seq num
        self.seq_num = '%s %s %s' % (seq_nums, self.opt_seq, whenCalled)
        self.new_bid = False
        self.new_ask = False

        # find vol of bid and ask
        self.curr_vols.bid = self._calc_implied_vol(spot - self.fut_tick, self.bid_px, self.bb_vol)
        self.curr_vols.ask = self._calc_implied_vol(spot + self.fut_tick, self.ask_px, self.bo_vol)
        
        if double_lte(self.curr_vols.bid, 0) or double_lte(self.curr_vols.ask, 0):
            return

        #change for exch
        #if self.exch == 'ES':
        #    if double_lt(self.bid_px, 5):
        #        self.bid_round = 5.0
        #    else:
        #        self.bid_round = 25.0
        #    if double_lt(self.ask_px, 5):
        #        self.ask_round = 5.0
        #    else:
        #        self.ask_round = 25.0
        #elif self.exch == 'KO':
        #    self.bid_round = 0.01
        #    self.ask_round = 0.01

        if double_gt(self.bid_px, self.large_tick):
            self.bid_round = self.l_round
            self.ask_round = self.l_round
        elif double_lt(self.ask_px, self.large_tick):
            self.bid_round = self.s_round
            self.ask_round = self.s_round
        else:
            self.bid_round = self.s_round
            self.ask_round = self.l_round

        # put the current vol into the queue for later use
        self.queueVols(self.curr_vols.bid, spot, 'bid')
        self.queueVols(self.curr_vols.ask, spot, 'ask')

        # if current vol is "better" than existing best vol, reset best vol
        if double_lte(self.bb_vol, 0) or double_gt(self.curr_vols.bid, self.bb_vol):
            self.bb_vol = self.curr_vols.bid
            self.bid_reason = 'bImp'
            self.new_bid = True

        if double_lte(self.bo_vol, 0) or double_lt(self.curr_vols.ask, self.bo_vol):
            self.bo_vol = self.curr_vols.ask
            self.ask_reason = ':oImp'
            self.new_ask = True

        #check the other (or both sides if no improvement to best) side to see if it needs to be backed up
        if not self.new_ask:
            self.checkOppSide(spot, self.bid_px, self.ask_px, 'ask')
            
        if not self.new_bid:
            self.checkOppSide(spot, self.bid_px, self.ask_px, 'bid')

        self.reason = '%s%s %s%s' % (self.bid_reason, self.new_bid, self.ask_reason, self.new_ask)
        
        if self.new_bid or self.new_ask:
            self.vol = self.calcMid(self.bb_vol, self.bo_vol)
            if double_gt(self.bid_weight, self.ask_weight):
                self.weight = self.bid_weight
            else:
                self.weight = self.ask_weight

    cdef queueVols(self, double new_vol, double spot, str side):
        # create list of backup/previous vols for each side (max len: 3)
        cdef:
            long maxlen = 3, i
            list prev_vols
            double tv = 0, prev_tv, fut_tick = self.fut_tick
            bint should_reverse, exists = False
            
        if side == 'bid':
            should_reverse = True
            prev_vols = self.prev_bid_vols
            fut_tick *= -1
        else:
            should_reverse = False
            prev_vols = self.prev_ask_vols
            
        if self.queue_by_tv:
            tv = self._calc_tv(spot + fut_tick, new_vol)
        
        #if new_vol not in prev_vols:
        for i in range(len(prev_vols)):
            if self.queue_by_tv:
                prev_tv = self._calc_tv(spot + fut_tick, prev_vols[i])
                exists = double_eq(tv, prev_tv, epsilon=self.tv_precision)
            else:
                exists = double_eq(new_vol, prev_vols[i])
                
            if exists:
                break
                
        if not exists:
            prev_vols.append(new_vol)
            prev_vols.sort(reverse=should_reverse)
            if len(prev_vols) > maxlen:
                prev_vols.pop()

    cdef double calcMid(self, double bidvol, double askvol) nogil:
        cdef double midvol = 0.5 * (bidvol + askvol)
        if double_gt(midvol, 0):
            return midvol
        else:
            return 0
        
    cdef bint is_lt_top_ask(self, double tv, double tolerance=0.001) nogil:
        cdef double tv_mkt = fceil(tv / self.ask_round - tolerance) * self.ask_round
        return double_lt(tv_mkt, self.top_ask)

    cdef bint is_gt_top_bid(self, double tv, double tolerance=0.001) nogil:
        cdef double tv_mkt = ffloor(tv / self.bid_round + tolerance) * self.bid_round
        return double_gt(tv_mkt, self.top_bid)

    cdef checkOppSide(self, double spot, double bid, double ask, str side_to_check):
        cdef:
            double spot_for_bid, spot_for_ask, v
            long i, n

        spot_for_bid = spot - self.fut_tick
        spot_for_ask = spot + self.fut_tick
        
        if side_to_check == 'ask':
            # would the current best vol produce the current market side?
            if self.is_lt_top_ask(self._calc_tv(spot_for_ask, self.bo_vol)):
                # no?  churn through the array of prev_vols and see if any of those would produce the current market side
                n = len(self.prev_ask_vols)
                for i in range(n):
                    v = self.prev_ask_vols[i]
                    # when you find one, set that to current best vol and remove tighter vols from prev_vols
                    if not self.is_lt_top_ask(self._calc_tv(spot_for_ask, v)):
                        self.bo_vol = v
                        #remove vols that are too aggressive from list of previous
                        self.prev_ask_vols = self.prev_ask_vols[i:]
                        self.ask_reason = ':oBack'
                        self.new_ask = 1
                        break
                    elif i == n-1:
                        # if you dont find one take the "best" vol that would account for current side of market by adding 1 cent to 1 tick better
                        self.bo_vol = self._calc_implied_vol(spot_for_ask, ask - self.ask_round + 0.01, self.bo_vol)
                        self.prev_ask_vols = [self.bo_vol]
                        self.ask_reason = ':oNew'
                        self.new_ask = 1

        else:
            if self.is_gt_top_bid(self._calc_tv(spot_for_bid, self.bb_vol)):
                n = len(self.prev_bid_vols)
                for i in range(n):
                    v = self.prev_bid_vols[i]
                    if not self.is_gt_top_bid(self._calc_tv(spot_for_bid, v)):
                        self.bb_vol = v
                        self.prev_bid_vols = self.prev_bid_vols[i:]
                        self.bid_reason = 'bBack'
                        self.new_bid = 1
                        break
                    elif i == n-1:
                        self.bb_vol = self._calc_implied_vol(spot_for_bid, bid + self.bid_round - 0.01, self.bb_vol)
                        self.prev_bid_vols = [self.bb_vol]
                        self.bid_reason = 'bNew'
                        self.new_bid = 1

    cdef update_kalman_node(self, double spot, double bid, double ask):
        cdef double kalman_bid, kalman_ask, vol_midpoint
        
        kalman_bid = self._calc_implied_vol(spot - self.fut_tick, bid, self.bb_vol)
        kalman_ask = self._calc_implied_vol(spot + self.fut_tick, ask, self.bo_vol)
        vol_midpoint = self.calcMid(kalman_bid, kalman_ask)

        self.kalman_node = kalman.new_univariate_value(self.q, self.r, vol_midpoint, self.kalman_node)

    cdef void reset_kalman(self, double value=0) nogil:
        self.kalman_node.value = value
        self.kalman_node.K = 0
        self.kalman_node.P = 0


