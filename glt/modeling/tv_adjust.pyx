

import numpy as np
import datetime

cimport cython
cimport numpy as np
from glt.math.helpful_functions cimport *
from libc.math cimport sqrt, exp, pow as cpow, M_PI as pi, round as fround


cdef int SH_ARR_POS = 0
cdef int SH_ARR_TTE = 2
cdef int SH_ARR_MONEYNESS = 3
cdef int SH_ARR_VEGA = 4
cdef int SH_ARR_K = 6

cdef adjustPerOneLotVega(np.ndarray[double, ndim=1] moneynessPrime, np.ndarray[double, ndim=2] corrMatrix, np.ndarray[double, ndim=1] vegaArray, double maxAdjustment):
    cdef:
        int i=0, j=0, tlen = moneynessPrime.shape[0]
        double maxVega = max(vegaArray)
        np.ndarray[double, ndim=2] adjOneLotMatrix = np.zeros( (tlen,tlen) ) 

    for 0 <= i < tlen:
        adjOneLotMatrix[i] = corrMatrix[i] * maxAdjustment * (vegaArray[i] / maxVega )

    return adjOneLotMatrix

cdef normalDistributionPDF(double x, double mean, double standardDev):
    return 1 / (standardDev * sqrt(2*pi)) * exp( - cpow(x - mean, 2) / (2 * cpow(standardDev,2)  ) )

cdef logistic_func(double x, double a, double b):
    cdef:
        double y = a / (1 + np.e ** (-b * x)), return_value = 0
    
    if double_lt(x, 0):
        return_value = y
    else:
        return_value = a - y
    return return_value

cdef strikeCorrelation(np.ndarray[double, ndim=1] moneynessPrime, double widthFactor): #returns 2d matrix of correlations
    cdef:
        double a = sqrt(2 + cpow(widthFactor,2) ) - widthFactor
        #double denom = normalDistributionPDF(widthFactor, 0, a)
        double denom = logistic_func(0, 1, widthFactor)
        int i=0, j=0, tlen = moneynessPrime.shape[0]
        np.ndarray[double, ndim=2] corrMatrix = np.zeros( (tlen,tlen) )

    for 0 <= i < tlen:
        for 0 <= j < tlen:
            #corrMatrix[i,j] = normalDistributionPDF( cpow(moneynessPrime[j] - moneynessPrime[i],2) + widthFactor, 0, a) / denom
            corrMatrix[i,j] = logistic_func( moneynessPrime[j] - moneynessPrime[i], 1, widthFactor) / denom
    return corrMatrix

cdef moneynessPrime(moneynessList, double wingFactor):
    cdef:
        double b = (7.5*4.75**(-wingFactor/.4)+.8)
        np.ndarray[double, ndim=1] mP = np.zeros(moneynessList.shape[0])
    
    mP = wingFactor * np.arctan(b * moneynessList)
    
    return mP

cdef class TvAdjust:
    cdef:
        readonly double maxAdjust, capPct, wingFactor, widthFactor, timeFactor, strikeInc
        readonly np.ndarray moneyness_prime, strike_correlation, adjPerOneLot, calendar_correlation, position, vegas, ttes, dtes, spot, moneys, strikes
        readonly long tlen, ylen

    
    #def __init__(self, shared_array, maxAdjust, widthFactor, wingFactor, timeFactor, capPct):
    def __init__(self, double maxAdjust, double widthFactor, double wingFactor, double timeFactor, double capPct, double strikeInc):
        """Creates the Tv_Adjust object.  Use Tv_Adjust.create_adjustments(shared_array) to calculate adjustments
         each interval and after each fill.

        Parameters
        ----------
        position : ordered dict of 2d arrays
        maxAdjust : double
        widthFactor : double
        wingFactor : double
        timeFactor : double
        capPct : double
        strikeInc : double

        Useful Methods/Attributes
        -------------------------
        Tv_Adjust.create_adjustments( shared_array ) : inplace update of adjustments based on position
        Tv_Adjust.create_matrices() : updates the correlation matrices as moneyness/spot changes
        Tv_Adjust.update_parameters( maxAdjust, widthFactor, wingFactor, timeFactor, capPct) : updates and changes correlation matrices with new parameters (have to send all params if changing one for now)
        """

        self.ylen = 0
        self.tlen = 0
        self.maxAdjust = maxAdjust
        self.capPct = capPct
        self.wingFactor = wingFactor
        self.widthFactor = widthFactor
        self.timeFactor = timeFactor
        self.strikeInc = strikeInc        

        #self.reformat_data(shared_array)
        self._create_matrices()
        #self.calc_adjustments(shared_array)

        
    cdef roundToStrike(self, double x, double strikeIncrement):
        return strikeIncrement * fround(x/strikeIncrement)

    
    cdef moneynessToStrike(self, np.ndarray[double, ndim=1] moneynessArray, double tte, double spot, double strikeIncrement=5.):
        cdef:
            int ylen = moneynessArray.shape[0]
            np.ndarray[double, ndim=1] strikes = np.zeros(moneynessArray.shape[0])
            
        for 0 <= i < ylen:
            strikes[i] = self.roundToStrike( spot * exp(moneynessArray[i] * sqrt(tte)), strikeIncrement)
        return strikes
        
        
    cdef reformat_data(self, shared_array, fut_dict):
        cdef:
            int i=0, j=0, init=1
            
        if not self.tlen and not self.ylen:  
            self.ttes = np.array([shared_array[key][0,SH_ARR_TTE] for key in shared_array.keys()])
            self.dtes = self.ttes * 260.
            self.tlen = self.ttes.shape[0]
            self.ylen = max( [shared_array[key].shape[0] for key in shared_array.keys()] )
            self.position = np.zeros( (self.tlen, self.ylen) )
            self.moneys = np.zeros( (self.tlen, self.ylen) )
            self.vegas = np.zeros( (self.tlen, self.ylen) )
            self.strikes = np.zeros( (self.tlen, self.ylen) )
            self.spot = np.zeros(self.tlen)
            init = 0
        
        i = 0
        for key in shared_array.keys():
            j = shared_array[key].shape[0]
            self.position[i,:j] = shared_array[key][:j,SH_ARR_POS]
            self.moneys[i,:j] = shared_array[key][:j,SH_ARR_MONEYNESS]
            self.vegas[i,:j] = shared_array[key][:j,SH_ARR_VEGA]
            #A[np.isnan(A)] = 0
            self.vegas[i][np.isnan(self.vegas[i])] = 1
            self.strikes[i,:j] = shared_array[key][:j,SH_ARR_K]
            if len(fut_dict) == 0:
                self.spot[i] = self.strikes[i,0] / (np.exp(self.moneys[i,0] * np.sqrt(self.ttes[i])) )
            else:
                self.spot[i] = fut_dict[key]
            i += 1
           
        #if not init:
        if True:
           self._create_matrices()
            
    cdef _create_matrices(self):

        self.moneyness_prime = np.empty( (self.tlen, self.ylen) )
        self.strike_correlation = np.empty( (self.tlen, self.ylen, self.ylen) )
        self.adjPerOneLot = np.empty( (self.tlen, self.ylen, self.ylen) )
        self.calendar_correlation = np.zeros( (self.tlen, self.tlen) )
        s = self.moneyness_prime.shape

        for 0 <= i < self.tlen:
            self.moneyness_prime[i] = moneynessPrime(self.moneys[i], self.wingFactor)
            self.strike_correlation[i] = strikeCorrelation(self.moneyness_prime[i], self.widthFactor)
            self.adjPerOneLot[i] = adjustPerOneLotVega(self.moneys[i], self.strike_correlation[i], self.vegas[i], self.maxAdjust * sqrt(self.dtes[i] / 20.))
            for 0 <= j < self.tlen:
                self.calendar_correlation[i,j] = ( (min(self.dtes[i], self.dtes[j]) / max(self.dtes[i], self.dtes[j]))**self.timeFactor )

        #print 'MONEYNESS_PRIME'
        #print self.moneyness_prime 
        #print 'strike_correlation'
        #print self.strike_correlation
        #print 'adj / 1 lot'
        #print self.adjPerOneLot

    def create_matrices(self):
        self._create_matrices()
                
    cdef _create_adjustments(self, shared_array, fut_dict={}):
        self.reformat_data(shared_array, fut_dict)
        self.calc_adjustments(shared_array)
       
        
    def create_adjustments(self, shared_array, fut_dict={}):
        self._create_adjustments(shared_array, fut_dict)
        
    cdef calc_adjustments(self, shared_array):
        cdef:
            np.ndarray[double, ndim=2] final_adjustments = np.zeros( (self.tlen, self.ylen) )
        
        #step 1 is to spread each expirys position to all other expirys
        t_correlated_position = self.calendarCorrelation(self.timeFactor, self.position, self.moneys, self.strikes, self.dtes)

        #step 2 is to apply the result of your correlation matrices (adjPerOneLot) to the time correlated position
        for 0 <= i < len(self.dtes):
            final_adjustments[i,:] = self.adjustmentCompiler(t_correlated_position[i,:], self.adjPerOneLot[i,:,:], self.vegas[i,:])

        #step 3 is to put those adjustments into the shared_array
        i = 0
        for key in shared_array.keys():
            j = shared_array[key].shape[0]
            shared_array[key][:j,5] = final_adjustments[i,:j]
            i += 1
      
 
        #it may not be necessary to return the shared_array... well have to see how it works
        return shared_array
        
        
    def update_parameters(self, maxAdjust, widthFactor, wingFactor, timeFactor, capPct):
        self._update_parameters(maxAdjust, widthFactor, wingFactor, timeFactor, capPct)

    cdef _update_parameters(self, maxAdjust, widthFactor, wingFactor, timeFactor, capPct):
        #reset variables
        self.maxAdjust = maxAdjust
        self.capPct = capPct
        self.wingFactor = wingFactor
        self.widthFactor = widthFactor
        self.timeFactor = timeFactor
        
        self._create_matrices()


    cdef calendarCorrelation(self, double tFactor, np.ndarray[double, ndim=2] positionMatrix, np.ndarray[double, ndim=2] moneynessMatrix, np.ndarray[double, ndim=2] strikeMatrix, np.ndarray[double, ndim=1] dteArray): 
        #this part takes the longest amount of time...
        #try to use memory views instead of declaring numpy arrays each time
        cdef:
            int i=0, j=0, k=0, l=0, t=0   
            int ylen=positionMatrix.shape[1], expLen=dteArray.shape[0] 
            np.ndarray[double, ndim=2] calendarCorrelationMatrix = np.zeros( (expLen, expLen) )
            np.ndarray[double, ndim=3] unsummedPosition = np.zeros( (expLen, expLen, ylen) )
            np.ndarray[double, ndim=2] calendarCorrelatedPosition = np.zeros( (expLen, ylen) )
                        
        x0 = datetime.datetime.now()
        #step 1 : spread each expirys position to all the other expirys according to the calendar_correlation matrix
        for 0 <= i < expLen:
            for 0 <= j < expLen:
                unsummedPosition[i,j] = positionMatrix[j] * self.calendar_correlation[i,j]   

        #if you are looking at this... that sucks for you
        #unsummed_position is a 3d array where  [i,:,:] is the expiration where the positions came FROM
        #                                       [:,j,:] is the expiration that needs to be summed
        #                                       [:,:,k] is the position for a given moneyness
        #so what we are doing here is going through each [i,:,:] and summing all the [:,j,k] but we have to 
        #    do first is find all the strikes that result from the moneyness of expiry [i,:,:] (temp_strikes),
        #    then sum all occurences of that strike and puts it in calendarCorrelatedPosition.
        for 0 <= i < expLen:
            for 0 <= j < expLen:
                if i == j:
                    temp_strikes = strikeMatrix[i,:]
                else:
                    #this funtion is the bottleneck in calendar corrlation (makes up 80% of the time)
                    temp_strikes = self.moneynessToStrike(moneynessMatrix[i], dteArray[j]/260., self.spot[j], self.strikeInc )
                l=0
                k=0
                while k < ylen and l < ylen:
                    if double_eq(strikeMatrix[j,l], temp_strikes[k]):
                        calendarCorrelatedPosition[j,l] += unsummedPosition[j,i,k]
                        k += 1
                    elif double_lt(strikeMatrix[j,l], temp_strikes[k]):
                        l += 1
                    else:
                        k +=1

        return calendarCorrelatedPosition

    
    cdef adjustmentCompiler(self, np.ndarray[double, ndim=1] position, np.ndarray[double, ndim=2] adjOneLotMatrix, np.ndarray[double, ndim=1] vega):
        cdef:
            long xlen = position.shape[0], ylen = adjOneLotMatrix.shape[0], i = 0, j = 0
            np.ndarray[double, ndim=1] finalAdjustments = np.zeros(xlen)

        #multiply position by adjustments
        for 0 <= i < xlen:
            if position[i] == 0:
                continue
            for 0 <= j < xlen:
                finalAdjustments[j] += adjOneLotMatrix[i,j] * position[i]

        for 0 <= i < xlen:
            if finalAdjustments[i] < -self.capPct * vega[i] / 100.:
                finalAdjustments[i] = -self.capPct * vega[i] / 100.
            elif finalAdjustments[i] > self.capPct * vega[i] / 100.:
                finalAdjustments[i] = self.capPct * vega[i] / 100.

        return finalAdjustments * -1



