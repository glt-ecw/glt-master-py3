# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from libc.math cimport NAN
from glt.math.cpp cimport isnan

from glt.md.standard cimport (
    calc_wmid_from_mdorderbook,
    calc_mvr_entropy, copy_values_to_memoryview, calc_mvr_diff,
    calc_mvr_entropy_weighted_value
)

from numpy import empty, linspace
from numpy.polynomial.polynomial import polyfit


cdef double KOSPI_OPTION_MIN_TICK = 0.011
cdef double KOSPI_FUTURE_MIN_TICK = 0.051
cdef double COMPARISON_EPSILON = 1e-8

cdef uint8_t KOSPI_SPREAD_RESAMPLER_PRICE = 1
cdef uint8_t KOSPI_SPREAD_RESAMPLER_VOL = 2


cdef uint64_t uint64_max3(uint64_t va, uint64_t vb, uint64_t vc) nogil:
    cdef uint64_t maxv
    if va > vb:
        maxv = va
    else:
        maxv = vb
    if vc > maxv:
        maxv = vc
    return maxv


cdef double _weighted_average(double[:] values, double[:] w, uint64_t size) nogil:
    cdef:
        uint64_t i
        double w_total = 0, vw_total = 0
    for 0 <= i < size:
        vw_total += values[i] * w[i]
        w_total += w[i]
    return vw_total / w_total


cdef class KospiOptionSpreadResampler:
    def __init__(self, uint64_t interval, int16_t option_type, double tte, double interest_rate, 
                 double strike_over, double strike_under, double u_distance, 
                 double half_life, double price_multiplier):
        self.interval = interval
        self.option_type = option_type
        self.tte = tte
        self.interest_rate = interest_rate
        self.strike_over = strike_over
        self.strike_under = strike_under
        self.u_distance = u_distance
        self.epsilon = COMPARISON_EPSILON
        self.half_life = half_life
        self.price_multiplier = price_multiplier
        # init resamplers
        self.resampler_over.Init(self.option_type, self.strike_over, self.tte, self.interest_rate, self.interval)
        self.resampler_under.Init(self.option_type, self.strike_under, self.tte, self.interest_rate, self.interval)
        self.resampler_u.Init(self.u_distance + KOSPI_FUTURE_MIN_TICK, self.epsilon, self.interval)
        # init some prices
        self.price_u = NAN
        self.price_over = NAN
        self.price_under = NAN
        
    cdef void _record_over(self, MDOrderBook *md, bint use_exch_time=False) nogil:
        cdef double wmid = calc_wmid_from_mdorderbook(md, self.price_multiplier, KOSPI_OPTION_MIN_TICK)
        if isnan(wmid) or isnan(self.price_u) or self.price_over == wmid:
            return
        if use_exch_time:
            self.resampler_over.Record(md.exch_time, wmid, self.price_u)
        else:
            self.resampler_over.Record(md.time, wmid, self.price_u)
        self.price_over = wmid
        
    cdef void _record_under(self, MDOrderBook *md, bint use_exch_time=False) nogil:
        cdef double wmid = calc_wmid_from_mdorderbook(md, self.price_multiplier, KOSPI_OPTION_MIN_TICK)
        if isnan(wmid) or isnan(self.price_u) or self.price_under == wmid:
            return
        if use_exch_time:
            self.resampler_under.Record(md.exch_time, wmid, self.price_u)
        else:
            self.resampler_under.Record(md.time, wmid, self.price_u)
        self.price_under = wmid
        
    cdef void _record_u(self, MDOrderBook *md, bint use_exch_time=False) nogil:
        cdef double wmid = calc_wmid_from_mdorderbook(md, self.price_multiplier, KOSPI_FUTURE_MIN_TICK)
        if isnan(wmid) or self.price_u == wmid:
            return
        if use_exch_time:
            self.resampler_u.Record(md.exch_time, wmid)
        else:
            self.resampler_u.Record(md.time, wmid)
        self.price_u = wmid
        
    cdef void _export_to_ndarrays(self):
        cdef:
            MVRWithinBounds *resampler_u = &(self.resampler_u)
            OptionResampler *resampler_over = &(self.resampler_over)
            OptionResampler *resampler_under = &(self.resampler_under)
            # find max_st_time and max_et_time
            uint64_t max_st_time = uint64_max3(resampler_over.FirstSampleTime(), resampler_under.FirstSampleTime(), resampler_u.FirstSampleTime())
            uint64_t max_et_time = uint64_max3(resampler_over.LastSampleTime(), resampler_under.LastSampleTime(), resampler_u.LastSampleTime())
            uint64_t num_samples
        
        # force samples around max_st_time and max_et_time
        resampler_u.ForceSamplesAround(max_st_time, max_et_time, trim=False)
        resampler_over.ForceSamplesAround(max_st_time, max_et_time)
        resampler_under.ForceSamplesAround(max_st_time, max_et_time)
        
        if resampler_u.NumSamples() != resampler_over.NumSamples() or resampler_u.NumSamples() != resampler_under.NumSamples():
            self.futprcs = None
            self.spread_prices = None
            self.spread_vols = None
            self.w = None
            return
        
        num_samples = resampler_u.NumSamples()
        
        # x
        self.futprcs = empty(num_samples, dtype='<f8')
        copy_values_to_memoryview(resampler_u, self.futprcs, len(self.futprcs))

        # y (prices)
        self.spread_prices = empty(num_samples, dtype='<f8')
        calc_mvr_diff(resampler_over.PriceMVRPtr(), resampler_under.PriceMVRPtr(), self.spread_prices, len(self.spread_prices))

        # y (vols)
        self.spread_vols = empty(num_samples, dtype='<f8')
        calc_mvr_diff(resampler_over.VolMVRPtr(), resampler_under.VolMVRPtr(), self.spread_vols, len(self.spread_vols))

        # w
        self.w = empty(num_samples, dtype='<f8')
        calc_mvr_entropy(<MeanValueResampler*>resampler_u, self.half_life, self.w, len(self.w))
        
    cdef OptionImpliedValues* _implied_over_ptr(self) nogil:
        return &(self.resampler_over.implied)
    
    cdef OptionImpliedValues* _implied_under_ptr(self) nogil:
        return &(self.resampler_under.implied)
    
    cdef uint64_t _first_sample_time(self) nogil:
        return self.resampler_u.FirstSampleTime()
    
    cdef uint64_t _last_sample_time(self) nogil:
        return self.resampler_u.LastSampleTime()
    
    cdef double _min_u(self) nogil:
        return self.resampler_u.MinVal()
    
    cdef double _max_u(self) nogil:
        return self.resampler_u.MaxVal()
    
    cdef bint _use_black_scholes(self) nogil:
        return not self._has_u_range() or self._u_range() < self.u_distance

    cdef double _u_range(self) nogil:
        return self._max_u() - self._min_u()

    cdef uint64_t _num_samples(self) nogil:
        return self.resampler_u.NumSamples()

    cdef bint _has_u_range(self) nogil:
        return not isnan(self._u_range())

    cdef double _calc_tv(self, double strike, double vol, double u) nogil:
        return CalculateTV(self.option_type, strike, self.tte, self.interest_rate, u, vol)

    cdef double _calc_delta(self, double strike, double vol, double u) nogil:
        return CalculateDelta(self.option_type, strike, self.tte, self.interest_rate, u, vol)

    cdef double _calc_entropy_weighted_value(self, MeanValueResampler *mvr) nogil:
        return calc_mvr_entropy_weighted_value(mvr, self.half_life)

    cdef double _average_futprc(self) nogil:
        return self._calc_entropy_weighted_value(<MeanValueResampler*>&(self.resampler_u))

    cdef ndarray _futprcs_around_average(self, uint64_t num_points):
        return linspace(-self.u_distance, self.u_distance, num_points) + self._average_futprc()

    cdef double _average_over_spread_price(self) nogil:
        return self._calc_entropy_weighted_value(self.resampler_over.PriceMVRPtr())

    cdef double _average_under_spread_price(self) nogil:
        return self._calc_entropy_weighted_value(self.resampler_under.PriceMVRPtr())

    cdef double _average_spread_price(self) nogil:
        return self._average_over_spread_price() - self._average_under_spread_price()

    cdef double _average_over_vol(self) nogil:
        return self._calc_entropy_weighted_value(self.resampler_over.VolMVRPtr())

    cdef double _average_under_vol(self) nogil:
        return self._calc_entropy_weighted_value(self.resampler_under.VolMVRPtr())

    cdef ndarray _lsq_coef_prices(self, uint64_t n):
        return polyfit(self.futprcs, self.spread_prices, n, w=self.w)

    cdef double _implied_spread_delta(self) nogil:
        cdef:
            OptionImpliedValues *over = self._implied_over_ptr()
            OptionImpliedValues *under = self._implied_under_ptr()
        return over.delta - under.delta
