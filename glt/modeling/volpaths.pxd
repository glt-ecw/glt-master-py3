cdef double vp_for_moneyness(double m, double start_spot, double curr_spot, double[:, :] vp_curve) nogil

cdef double pt0func(double dte) nogil
cdef double pt1func(double dte) nogil
cdef double pt2func(double dte) nogil
cdef double pt3func(double dte) nogil
cdef double pt4func(double dte) nogil

cdef double pt0funcKO(double dte)
cdef double pt1funcKO(double dte)
cdef double pt2funcKO(double dte)
cdef double pt3funcKO(double dte)

cdef double[:, :] createVP(double d, str optMarket=*)
