import numpy as np
import pandas as pd
import bsm_implieds
import dbHelper as db
import dateutil.rrule as dr
import datetime


cimport cython
cimport numpy as np
from glt.math cimport kalman

from glt.db import getDTE

#only to get DTE from symbol name
epsilon = .0001
def GT_Ep(a, b):
    if a - b > epsilon:
        return True
    else:
        return False
    
def LT_Ep(a, b):
    return GT_Ep(b, a)

def EQ_Ep(a, b):
    if abs(a - b) <= epsilon:
        return True
    else:
        return False

def GTE_Ep(a, b):
    if GT_Ep(a,b) or EQ_Ep(a, b):
        return True
    else:
        return False

def LTE_Ep(a, b):
    if LT_Ep(a,b) or EQ_Ep(a,b):
        return True
    else:
        return False
  
#cdef class rawStrike:
cdef class RawStrike:
    """__init__(self, name, t=0.)
    parameters: name = exchange symbol, t = datetime trade date (slower b/c of db lookup) or tte"""
    cdef public double hi_weight, mid_weight, lo_weight, q, r
    cdef readonly double bb_vol, bo_vol, bid_round, ask_round, acc_bid_vol, acc_ask_vol, bid_px, ask_px, acc_window, curr_bid_vol, curr_ask_vol, vol, weight, t, ir, cp, k, temp_a, temp_b, vega, bid_weight, ask_weight, fut_tick, top_bid, top_ask, kalman_value
    cdef readonly str name, seq_num, opt_seq, fut_seq, bid_reason, ask_reason, reason, exch
    cdef int i, new_bid, new_ask
    cdef readonly list list_ts, prev_bid_vols, prev_ask_vols
    cdef readonly kalman.UnivariateNode kalman_node
    
    def __init__(self, name, t=0.):
    
        self.name = name
        self.bb_vol = 0.
        self.bo_vol = 0.
        self.bid_round = 0
        self.ask_round = 0
        self.bid_px = 0.
        self.ask_px = 0.
        
        self.prev_bid_vols = [0]
        self.prev_ask_vols = [0]
        
        #for accumulation (deprecated)
        self.list_ts = []
        self.acc_bid_vol = 0.
        self.acc_ask_vol = 0.
        self.acc_window = 5000000000.
        self.temp_a = 0.
        self.temp_b = 0.
        
        #kalman shizzle
        self.q = .001
        self.r = 1.0
        self.kalman_node.value = 0.
        self.kalman_value = 0.
        
        self.vol = 0.
        self.weight = 1.
        self.hi_weight = 1.
        self.mid_weight = .6
        self.lo_weight = .25
        self.bid_weight = 1.
        self.ask_weight = 1.
        self.vega = 0.
        
        self.exch = name[:2]
        
        if not hasattr(t, "year"):
            self.t = t
        else:
            self.t = getDTE('%sO%s1%s' % (name[:2], name[2], name[3]), fromDate=t ) / 260.
            
        #set exchange for tick size (.125 for ES, 2.5 for KO)
        if self.exch == 'ES':
            self.fut_tick = .125
            self.ir = .0035
        elif self.exch == 'KO':
            self.fut_tick = 2.5
            self.ir = .015
            self.t = self.t - (1./260)
            
        self.cp = 1 if name[5] == 'C' else -1
        self.k = float(name[6:])
        
        #testing/logging
        self.seq_num = 'none'
        self.opt_seq = 'none'
        self.fut_seq = 'none'
        self.bid_reason = 'none'
        self.ask_reason = 'none'
        self.reason = 'none'

    cpdef set_weights(self, double hi, double mid, double lo):
        self.lo_weight = lo
        self.mid_weight = mid
        self.hi_weight = hi

    cpdef public book_update(self, double bid, double ask, double topbid, double topask, str seq_num):
        """book_update(self, double bid, double ask, str seq_num)
    parameters: bid, ask, seq num (str) 
    State 1 of rawStrike object: sets bid and ask price and opt seq num.
    Must call calc_vol() to actually calculate vol."""
        self.bid_px = bid
        self.ask_px = ask
        self.top_bid = topbid
        self.top_ask = topask
        self.opt_seq = seq_num
    
    cpdef raw_vol_calc(self, list book, double spot, long ts, double min_par, double min_qty, str opt_seq_num, str fut_seq_num):
        bidpar2, bidqty2, bidprc2, bidpar1, bidqty1, bidprc1, bidpar0, bidqty0, bidprc0, askprc0, askqty0, askpar0, askprc1, askqty1, askpar1, askprc2, askqty2, askpar2 = book
        self.top_bid = bidprc0
        self.top_ask = askprc0
        
        found_good_bid = 0
        found_good_ask = 0
        if bidpar0 >= min_par and bidqty0 >= min_qty:
            self.bid_px = bidprc0
            found_good_bid = 1
        elif bidpar1 >= min_par and bidqty1 >= min_qty:
            self.bid_px = bidprc1
            found_good_bid = 1
        elif bidpar2 >= min_par and bidqty2 >= min_qty:
            self.bid_px = bidprc2
            found_good_bid = 1
        elif self.vol == 0:
            self.bid_px = bidprc0
            found_good_bid = 1
        if np.isnan(bidprc0):
            self.bid_px = .01
            found_good_bid = 1

        #find "good ask"
        if askpar0 >= min_par and askqty0 >= min_qty:
            self.ask_px = askprc0
            found_good_ask = 1
        elif askpar1 >= min_par and askqty1 >= min_qty:
            self.ask_px = askprc1
            found_good_ask = 1
        elif askpar2 >= min_par and askqty2 >= min_qty:
            self.ask_px = askprc2
            found_good_ask = 1
        elif self.vol == 0:
            self.ask_px = askprc0
            found_good_ask = 1
        
        #add a check for found_good_bid/ask and if it fails go to the kalman shit
        if found_good_ask and found_good_bid:
            self.calc_vol(spot, ts, fut_seq_num, 'fuckyou')
            #self.update_kalman_node(spot, bidprc0, askprc0)
            self.reset_kalman(self.vol)
        else:
            self.update_kalman_node(spot, bidprc0, askprc0)
            self.vol = self.kalman_node.value
            
        
    cpdef weighted_book_update(self, list book, double spot, str opt_seq_num, str fut_seq_num, int participantsFlag, double min_par, double min_qty, double min_par2, double min_qty2):
        """weighted_book_update(self, list book, str seq_num, bool participantsFlag=0, double min_par=0, double min_ratio=0, double cap_ratio=0)
    'book' parameter is all levels of the options book.  participantsFlag is if the exchange supports participants
    Must call calc_vol to actually calculate vol."""
        
        goodbid = 0
        goodask = 0
        bidpar2, bidqty2, bidprc2, bidpar1, bidqty1, bidprc1, bidpar0, bidqty0, bidprc0, askprc0, askqty0, askpar0, askprc1, askqty1, askpar1, askprc2, askqty2, askpar2 = book
        if participantsFlag:
            bid_ratio2 = bidqty2 / bidpar2
            bid_ratio1 = bidqty1 / bidpar1
            bid_ratio0 = bidqty0 / bidpar0
            ask_ratio0 = askqty0 / askpar0
            ask_ratio1 = askqty1 / askpar1
            ask_ratio2 = askqty2 / askpar2
            
            
            if EQ_Ep(bidprc0 - bidprc1, askprc1 - askprc0):
                self.bid_px = bidprc0
                self.ask_px = askprc0
                self.bid_weight = 1.
                self.ask_weight = 1.
                goodbid = 1
                goodask = 1
            
            if not goodbid:
                if bidqty0 >= min_qty and bidpar0 >= min_par:
                    self.bid_px = bidprc0
                    self.bid_weight = self.hi_weight
                    goodbid = 1
                elif bidqty1 >= min_qty and bidpar1 >= min_par:
                    self.bid_px = bidprc1
                    self.bid_weight = self.hi_weight
                    goodbid = 1
                elif bidqty2 >= min_qty and bidpar2 >= min_par:
                    self.bid_px = bidprc2
                    self.bid_weight = self.hi_weight
                    goodbid = 1
                elif bidqty0 >= min_qty2 and bidpar0 >= min_par2:
                    self.bid_px = bidprc0
                    self.bid_weight = self.mid_weight
                    goodbid = 1
                elif bidqty1 >= min_qty2 and bidpar1 >= min_par2:
                    self.bid_px = bidprc0
                    self.bid_weight = self.mid_weight
                    goodbid = 1
                elif bidqty2 >= min_qty2 and bidpar2 >= min_par2:
                    self.bid_px = bidprc0
                    self.bid_weight = self.mid_weight
                    goodbid = 1
                elif bidprc0 > 0:
                    self.bid_px = bidprc0
                    self.bid_weight = self.lo_weight
                    goodbid = 1
                    
                if askqty0 >= min_qty and askpar0 >= min_par:
                    self.ask_px = askprc0
                    self.ask_weight = self.hi_weight
                    goodask = 1
                elif askqty1 >= min_qty and askpar1 >= min_par:
                    self.ask_px = askprc1
                    self.ask_weight = self.hi_weight
                    goodask = 1
                elif askqty2 >= min_qty and askpar2 >= min_par:
                    self.ask_px = askprc2
                    self.ask_weight = self.hi_weight
                    goodask = 1
                elif askqty0 >= min_qty2 and askpar0 >= min_par2:
                    self.ask_px = askprc0
                    self.ask_weight = self.mid_weight
                    goodask = 1
                elif askqty1 >= min_qty2 and askpar1 >= min_par2:
                    self.ask_px = askprc0
                    self.ask_weight = self.mid_weight
                    goodask = 1
                elif askqty2 >= min_qty2 and askpar2 >= min_par2:
                    self.ask_px = askprc0
                    self.ask_weight = self.mid_weight
                    goodask = 1
                elif askprc0 > 0:
                    self.ask_px = askprc0
                    self.ask_weight = self.lo_weight
                    goodask = 1

            if goodbid and goodask:
                self.opt_seq = opt_seq_num
                self.calc_vol(spot, 1, fut_seq_num, 'wbu')

            
    cpdef public calc_vol(self, double spot, long ts, str seq_nums, str whenCalled):
        """calc_vol(self, double spot, long ts, str seq_nums, str whenCalled)
    parameters: spot (as mid price), ts (as long), seq num (str), extra str for debugging 
    Stage 2 of rawStrike object: calculates vol based on bid/ask prices you set in book_update()
    call (must have done this previously).
    Updates attributes: vol, bb_vol, bo_vol, reason and seq num of fut and opt"""
    
        #add fut seqnum to existing opt seq num
        self.seq_num = '%s %s %s' % (seq_nums, self.opt_seq, whenCalled)
        self.new_bid = 0
        self.new_ask = 0

        #find vol of bid and ask
        self.curr_bid_vol, self.curr_ask_vol = self.calcCurrVol(spot, self.bid_px, self.ask_px)

        #change for exch
        if self.exch == 'ES':
            #self.bid_round = 20. if self.bid_px < 5. else 4.
            #self.ask_round = 20. if self.ask_px < 5. else 4.
            self.bid_round = 5 if self.bid_px < 5. else 25
            self.ask_round = 5 if self.ask_px < 5. else 25
        elif self.exch == 'KO':
            self.bid_round = 1.
            self.ask_round = 1.
        
        #put the current vol into the queue for later use
        self.queueVols(self.curr_bid_vol, 'bid')
        self.queueVols(self.curr_ask_vol, 'ask')
        
        #if current vol is "better" than existing best vol, reset best vol
        #if self.curr_bid_vol > self.bb_vol:
        if GT_Ep(self.curr_bid_vol, self.bb_vol):
            self.bb_vol = self.curr_bid_vol
            #self.queueVols(self.curr_bid_vol, 'bid')
            self.bid_reason = 'bImp'
            self.new_bid = 1
        #if self.curr_ask_vol < self.bo_vol:
        if LT_Ep(self.curr_ask_vol, self.bo_vol):
            self.bo_vol = self.curr_ask_vol
            #self.queueVols(self.curr_ask_vol, 'ask')
            self.ask_reason = ':oImp'
            self.new_ask = 1

        #check the other (or both sides if no improvement to best) side to see if it needs to be backed up
        if self.new_bid and not self.new_ask:
            self.checkOppSide(spot, self.ask_round, self.bid_px, self.ask_px, 'ask')
        elif not self.new_bid and self.new_ask:
            self.checkOppSide(spot, self.bid_round, self.bid_px, self.ask_px, 'bid')
        elif not self.new_bid and not self.new_ask:
            self.checkOppSide(spot, self.ask_round, self.bid_px, self.ask_px, 'ask')
            self.checkOppSide(spot, self.bid_round, self.bid_px, self.ask_px, 'bid')
        
        self.reason = '%s%s %s%s' % (self.bid_reason, self.new_bid, self.ask_reason, self.new_ask)
        #update vol if necessary
        #self.kalman_top_book()
        
        if self.new_bid or self.new_ask:
            self.vol = self.calcMid(self.bb_vol, self.bo_vol)
            #self.vega = bsm_implieds.bs_vega(spot, self.k, self.t, self.vol, self.ir, self.cp)
            self.weight = min([self.bid_weight, self.ask_weight])
        
    cdef queueVols(self, new_vol, side):
        #create list of backup/previous vols for each side (max len: 3)
        max_len = 3
        if side == 'bid':
            if new_vol not in self.prev_bid_vols:
                self.prev_bid_vols.append(new_vol)
                self.prev_bid_vols.sort(reverse=True)
                if len(self.prev_bid_vols) > max_len:
                    self.prev_bid_vols = self.prev_bid_vols[:max_len]
        else:
            if new_vol not in self.prev_ask_vols:
                self.prev_ask_vols.append(new_vol)
                self.prev_ask_vols.sort()
                if len(self.prev_ask_vols) > max_len:
                    self.prev_ask_vols = self.prev_ask_vols[:max_len]
                
    cdef calcMid(self, bid_vol, ask_vol):       
        #self.vol = max( (bid_vol + ask_vol ) / 2., 0.)
        return max( (bid_vol + ask_vol ) / 2., 0.)
        
    cdef getMarket(self, tv, side):
        int_tv = int(round(tv * 100))
        #if side == 'bid':
            #int_tv = int(round(tv * 100))
        #elif side == 'ask':
            #int_tv = int(np.ceil(tv * 100))
        if side == 'bid':
            mod = int_tv % self.bid_round
            side = int_tv - mod
        elif side == 'ask':
            mod = int_tv % self.ask_round
            if mod != 0:
                side = int_tv + self.ask_round - mod
            else:
                side = int_tv
        return side / 100.
        
    cdef calcCurrVol(self, spot, bid, ask):
        if self.cp == 1:
            bid_vol = bsm_implieds.implied_vol(spot - self.fut_tick, bid, self.k, self.t, self.ir, self.cp)
            ask_vol = bsm_implieds.implied_vol(spot + self.fut_tick, ask, self.k, self.t, self.ir, self.cp)
        else:
            bid_vol = bsm_implieds.implied_vol(spot + self.fut_tick, bid, self.k, self.t, self.ir, self.cp)
            ask_vol = bsm_implieds.implied_vol(spot - self.fut_tick, ask, self.k, self.t, self.ir, self.cp)
        return (bid_vol, ask_vol)
        
    cdef checkOppSide(self, spot, roundTo, bid, ask, side_to_check):
        #find appropriate futures side (assuming crossing futures market for quote)
        if self.cp == 1:
            spot_for_ask = spot + self.fut_tick
            spot_for_bid = spot - self.fut_tick
        else:
            spot_for_ask = spot - self.fut_tick
            spot_for_bid = spot + self.fut_tick
            
        if side_to_check == 'ask':
            #would the current best vol produce the current market side?
            #if LT_Ep( self.getMarket(bsm_implieds.bs_tv(spot_for_ask, self.k, self.t, self.bo_vol, self.ir, self.cp), 'ask'), ask):   
            if LT_Ep( self.getMarket(bsm_implieds.bs_tv(spot_for_ask, self.k, self.t, self.bo_vol, self.ir, self.cp), 'ask'), self.top_ask):   
                #no?  churn through the array of prev_vols and see if any of those would produce the current market side
                for v in self.prev_ask_vols:
                    #when you find one, set that to current best vol and remove tighter vols from prev_vols
                    #if EQ_Ep( self.getMarket(bsm_implieds.bs_tv(spot_for_ask, self.k, self.t, v, self.ir, self.cp), 'ask'), ask):   
                    if EQ_Ep( self.getMarket(bsm_implieds.bs_tv(spot_for_ask, self.k, self.t, v, self.ir, self.cp), 'ask'), self.top_ask):   
                        self.bo_vol = v
                        #remove vols that are too aggressive from list of previous
                        self.prev_ask_vols = self.prev_ask_vols[self.prev_ask_vols.index(v):]
                        self.ask_reason = ':oBack'
                        self.new_ask = 1
                        break
                    elif v == self.prev_ask_vols[-1]:
                        #if you dont find one take the "best" vol that would account for current side of market by adding 1 cent to 1 tick better
                        self.bo_vol = bsm_implieds.implied_vol(spot_for_ask, ask - (self.ask_round / 100.)+.01, self.k, self.t, self.ir, self.cp)      
                        self.prev_ask_vols = [self.bo_vol]
                        self.ask_reason = ':oNew'
                        self.new_ask = 1

        else:
            #if GT_Ep( self.getMarket(bsm_implieds.bs_tv(spot_for_bid, self.k, self.t, self.bb_vol, self.ir, self.cp), 'bid'), bid):  
            if GT_Ep( self.getMarket(bsm_implieds.bs_tv(spot_for_bid, self.k, self.t, self.bb_vol, self.ir, self.cp), 'bid'), self.top_bid):  
                for v in self.prev_bid_vols:   
                    #if EQ_Ep( self.getMarket(bsm_implieds.bs_tv(spot_for_bid, self.k, self.t, v, self.ir, self.cp), 'bid'), bid):       
                    if EQ_Ep( self.getMarket(bsm_implieds.bs_tv(spot_for_bid, self.k, self.t, v, self.ir, self.cp), 'bid'), self.top_bid):       
                        self.bb_vol = v
                        self.prev_bid_vols = self.prev_bid_vols[self.prev_bid_vols.index(v):]
                        self.bid_reason = 'bBack'
                        self.new_bid = 1
                        break
                    elif v == self.prev_bid_vols[-1]:
                        self.bb_vol = bsm_implieds.implied_vol(spot_for_bid, bid + (self.bid_round / 100.) - .01 , self.k, self.t, self.ir, self.cp)     #
                        self.prev_bid_vols = [self.bb_vol]
                        self.bid_reason = 'bNew'
                        self.new_bid = 1

    cdef update_kalman_node(self, double spot, double bid, double ask):
        kalman_bid, kalman_ask = self.calcCurrVol(spot, bid, ask)
        vol_midpoint = self.calcMid(kalman_bid, kalman_ask)
        
        self.kalman_node = kalman.new_univariate_value(self.q, self.r, vol_midpoint, self.kalman_node)
        #self.kalman_value = kalman.univariate_array(self.q, self.r, self.kalman_array )[-1]        

    cdef void reset_kalman(self, double value=0) nogil:
        self.kalman_node.value = value
        self.kalman_node.K = 0
        self.kalman_node.P = 0   
 
    cdef accumulate(self, long ts):
        """deprecated """
        #append new vols
        self.list_ts.append(ts)
        self.prev_bid_vols.append(self.curr_bid_vol)
        self.prev_ask_vols.append(self.curr_ask_vol)
        
        if self.acc_bid_vol != 0: 
            #clean up stale vols                       
            i = 0
            while self.list_ts[i] < ts - self.acc_window: 
                i += 1
            self.list_ts = self.list_ts[i:]
            self.prev_bid_vols = self.prev_bid_vols[i:]
            self.prev_ask_vols = self.prev_ask_vols[i:]
            
            #accumulate weighted by % time of acc_window (new book value is weighted 25%)
            temp_b = self.acc_bid_vol * ( (self.list_ts[0] - (ts - self.acc_window) ) / self.acc_window )
            temp_a = self.acc_ask_vol * ( (self.list_ts[0] - (ts - self.acc_window) ) / self.acc_window )
            if len(self.list_ts) > 1:
                for i in range(0, len(self.list_ts)-1):
                    temp_b += self.prev_bid_vols[i] * ( (self.list_ts[i+1] - self.list_ts[i]) / self.acc_window ) 
                    temp_a += self.prev_ask_vols[i] * ( (self.list_ts[i+1] - self.list_ts[i]) / self.acc_window ) 
            temp_b = temp_b * .75 + self.curr_bid_vol * .25
            temp_a = temp_a * .75 + self.curr_ask_vol * .25
            #update acc*_vol
            self.acc_bid_vol = temp_b
            self.acc_ask_vol = temp_a
        else:
            self.acc_bid_vol = self.curr_bid_vol
            self.acc_ask_vol = self.curr_ask_vol   
