from libc.stdint cimport *
from libcpp.deque cimport deque as deque_
from libcpp.vector cimport vector as vector_
from glt.md.standard cimport MDOrderBook
from glt.listening.oe cimport DeioFill


cdef uint8_t SPREAD_TYPE_AB_SUBTRACT_U_LINEAR


cdef class SpreadPairModel:
    cdef:
        bint valid_md
        vector_[uint32_t] secids
        vector_[MDOrderBook] md_vec
        deque_[DeioFill] fill_queue
        MDOrderBook *md_ptr
        readonly:
            bint process_fills
            uint8_t spread_type
            uint32_t secid_a, secid_b, secid_u, u_idx
            double spread_value, ref_u, spread_value_du, improve_u_offset
            logger

        uint64_t key(self) nogil
        void _copy_md(self, MDOrderBook *src_md) nogil
        void _copy_fill(self, DeioFill *src_fill) nogil
        bint _all_valid_md(self) nogil
        uint64_t _num_fills(self) nogil
        bint _has_fills(self) nogil
        DeioFill* _next_fill(self) nogil
        void _pop_fill(self) nogil 
        MDOrderBook* _md_for(self, uint32_t secid) nogil
