# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True

from libc.math cimport NAN
from glt.md.standard cimport copy_mdorderbook


cdef uint8_t SPREAD_TYPE_AB_SUBTRACT_U_LINEAR = 1


cdef class SpreadPairModel:
    """ 
    SpreadPairModel is designed like an abstract class. The only method
    required is .evaluate_md(); if it is not implemented, Python will
    raise a NotImplemented error.

    A "spread pair" is defined however you want. It can be any function of
    the prices contracts a and b (e.g. a - b); you define how spread_value 
    is computed. This object is used merely as a vehicle to hold spread_value
    for use in a simulator, publisher or whatever else wants to point to
    a SpreadPairModel object.

    The purpose of u is to act as an underlying contract, which dictates how
    spread_value changes with respect to u using a reference price of u (ref_u).
    This relationship of u to a vs b is flexible; there can be specific model 
    types defined outside of this object which describe the behavior of how 
    spread_value changes with respect to u. Whoever consumes this object should
    know what sort of relationship u has to a vs b. In most cases, it will 
    change spread_value linearly with respect to a change in u (e.g. if
    spread_value_du = 0.01, spread_value = 0.9 at ref_u = 123.45, the
    extrapolated spread value at u = 123.25 will be 
    0.9 + 0.01*(123.25 - 123.45) = 0.898. KEEP IN MIND THIS RELATIONSHIP WILL
    NOT OVERWRITE spread_value; the consumer of this object will simply use
    this relationship to figure out what the spread value is at a specific
    price of u by using the reference spread_value at ref_u.

    spread_type defines how the spread and underlying relationships are computed.
    Use this to define the type of spread so whoever reads these values will
    understand how to interpret them.

    The object must be initialized with three variables: secid_a, secid_b and
    secid_u, which are int-based contract IDs. The rest is up to you to write.
    """
    def __init__(self, uint8_t spread_type, uint32_t secid_a, uint32_t secid_b, uint32_t secid_u):
        self.logger = None
        if spread_type == 0:
            raise ValueError('spread_type cannot be zero. use pre-defined uint8_t values')
        self.spread_type = spread_type
        self.secid_a = secid_a
        self.secid_b = secid_b
        self.secid_u = secid_u
        # set the node in self.md_vec for secid_u
        if secid_u == secid_a:
            self.u_idx = 0
        elif secid_u == secid_b:
            self.u_idx = 1
        else:
            self.u_idx = 2
        self.secids.push_back(secid_a)
        self.secids.push_back(secid_b)
        self.secids.push_back(secid_u)
        self.md_vec = vector_[MDOrderBook](3)
        self.valid_md = False
        self.md_ptr = NULL
        # fill handling: in your object, if you choose to listen to fills, set process_fills=True
        # and implement .evaluate_fill()
        self.process_fills = False
        # zero values to start
        self.spread_value = NAN
        self.ref_u = 0
        self.spread_value_du = 0
        self.improve_u_offset = 0

    def assign_logger(self, logger):
        self.logger = logger
        self.log_info('logger assigned: %s' % self.logger)

    cdef uint64_t key(self) nogil:
        if self.secid_a < self.secid_b:
            return ((<uint64_t>self.secid_a) << 32) + self.secid_b
        else:
            return ((<uint64_t>self.secid_b) << 32) + self.secid_a
        
    cdef void _copy_md(self, MDOrderBook *src_md) nogil:
        cdef MDOrderBook *md = self._md_for(src_md.secid)
        self.valid_md = (src_md != NULL and src_md.bids[0].qty > 0 and src_md.asks[0].qty > 0)
        if not self.valid_md:
            return
        self.md_ptr = md
        if md.bids.size() != src_md.bids.size():
            md.Init(src_md.bids.size())
            md.NullifyByteArray()
        copy_mdorderbook(src_md, md)

    cdef void _copy_fill(self, DeioFill *src_fill) nogil:
        cdef DeioFill fill
        if not (src_fill.secid == self.secid_a or src_fill.secid == self.secid_b or src_fill.secid == self.secid_u):
            return
        fill.deioid = src_fill.deioid
        fill.seqnum = src_fill.seqnum
        fill.fill_type = src_fill.fill_type
        fill.side = src_fill.side
        fill.secid = src_fill.secid
        fill.userid = src_fill.userid
        fill.serverid = src_fill.serverid
        fill.price = src_fill.price
        fill.qty = src_fill.qty
        fill.exch_trade_id = src_fill.exch_trade_id
        fill.fill_time = src_fill.fill_time
        fill.order_type = src_fill.order_type
        fill.assumed_fill_type = src_fill.assumed_fill_type
        self.fill_queue.push_back(fill)

    cdef uint64_t _num_fills(self) nogil:
        return self.fill_queue.size()

    cdef bint _has_fills(self) nogil:
        return not self.fill_queue.empty()

    cdef DeioFill* _next_fill(self) nogil:
        if self._has_fills():
            return &(self.fill_queue.front())
        else:
            return NULL

    cdef void _pop_fill(self) nogil:
        if self._has_fills():
            self.fill_queue.pop_front()

    cdef bint _all_valid_md(self) nogil:
        cdef bint valid = self.md_vec[0].seqnum > 0 and self.md_vec[1].seqnum > 0
        if self.secid_u != 0:
            return valid and self.md_vec[self.u_idx].seqnum > 0
        else:
            return valid

    cdef MDOrderBook* _md_for(self, uint32_t secid) nogil:
        cdef uint16_t i
        for 0 <= i < self.md_vec.size():
            if self.secids[i] == secid:
                return &(self.md_vec[i])
        return NULL

    def evaluate_md(self):
        raise NotImplemented('please implement .evaluate_md()')

    def evaluate_fills(self):
        raise NotImplemented('please implement .evaluate_fills() or set process_fills=False')

    def log_info(self, msg):
        if self.logger:
            self.logger.info('%s: %s' % (self, msg))

    def log_error(self, msg):
        if self.logger:
            self.logger.error('%s: %s' % (self, msg))

    def log_warning(self, msg):
        if self.logger:
            self.logger.warning('%s: %s' % (self, msg))

    def log_debug(self, msg):
        if self.logger:
            self.logger.debug('%s: %s' % (self, msg))


cdef class DumbRELFPairModel(SpreadPairModel):
    """
    DumbRELFPairModel is a demonstration of how to inherit SpreadPairModel. It
    resembles the basic Recursive Exponentially-weighted Linear Filter that Deio
    uses to "value" pairs of contracts. We do not recommend that such a filter
    be used for theoretical valuation; a univariate filter like this should be
    used to filter out noise.

    The way Deio computed RELF was taking the mid-price of contracts A and B and
    recursively calculating the spread between the two using some ratio of its
    previously-calculated value with this new value. There is no relationship
    to an underlying contract, so it is set to zero.
    """
    cdef:
        readonly:
            str name
            double gain
            bint computed

    def __init__(self, uint32_t secid_a, uint32_t secid_b, name=None, double gain=0.0):
        cdef uint32_t secid_u = 0
        SpreadPairModel.__init__(self, SPREAD_TYPE_AB_SUBTRACT_U_LINEAR, secid_a, secid_b, secid_u)
        if name is None:
            self.name = 'no name'
        else:
            self.name = str(name)
        self.gain = gain
        # no underlying necessary (see secid_u=0)
        self.ref_u = 0
        self.spread_value_du = 0
        # just take the first computed value if we haven't computed anything yet
        self.computed = False

    def __repr__(self):
        return 'DumbRELFPairModel{name="%s", gain=%0.4f}' % (self.name, self.gain)

    def evaluate_md(self):
        if not self.valid_md:
            return
        # we need for all of our known market data to be valid
        if not self._all_valid_md():
            return

        # let's compute a spread
        self._compute_dumb_value()

        if self.computed:
            self.log_debug('new spread_value=%0.4f at seqnum=%d' % (self.spread_value, self.md_ptr.seqnum))

    cdef void _compute_dumb_value(self) nogil:
        cdef:
            MDOrderBook *md_a = self._md_for(self.secid_a)
            MDOrderBook *md_b = self._md_for(self.secid_b)
            double mid_a, mid_b, a_vs_b
        if md_a == NULL or md_b == NULL:
            return
        mid_a = 0.5 * (md_a.bids[0].price + md_a.asks[0].price)
        mid_b = 0.5 * (md_b.bids[0].price + md_b.asks[0].price)
        a_vs_b = mid_a - mid_b
        if self.computed:
            self.spread_value = self.gain * a_vs_b + (1 - self.gain) * self.spread_value
        else:
            self.spread_value = a_vs_b
            self.computed = True
