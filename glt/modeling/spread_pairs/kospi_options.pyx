# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from cython.operator cimport dereference
from libc.math cimport fabs, NAN
from glt.md.standard cimport OptionImpliedValues, PyMDOrderBook
# TODO: make glt.oe.standard module and put DeioFill there instead...
from glt.oe.standard cimport DeioFill
from glt.modeling.spread_pairs.base cimport SPREAD_TYPE_AB_SUBTRACT_U_LINEAR

from numpy import ones, linspace
from glt.math.cvx import PolyFitOneFeature, is_bad_fit, calc_residual_error, SolverError
from collections import deque


cdef void copy_some_fill_info(DeioFill *src_fill, DeioFill *dst_fill) nogil:
    dst_fill.deioid = src_fill.deioid
    dst_fill.secid = src_fill.secid
    dst_fill.price = src_fill.price 
    dst_fill.side = src_fill.side
    dst_fill.qty = src_fill.qty
    dst_fill.remaining_qty = src_fill.remaining_qty
    dst_fill.order_type = src_fill.order_type


cdef void copy_fill_to_pairfill(DeioFill *fill, PairFill pair, bint quote_side) nogil:
    if quote_side:
        copy_some_fill_info(fill, pair._q_ptr())
    else:
        copy_some_fill_info(fill, pair._h_ptr())


cdef class PairFill:
    cdef:
        DeioFill q, h
        public:
            int32_t spread_qty
            double spread_value, ref_u

    def __init__(self):
        pass

    cdef DeioFill* _q_ptr(self) nogil:
        return &(self.q)

    cdef DeioFill* _h_ptr(self) nogil:
        return &(self.h)


cdef class AQPairFillStack:
    def __init__(self, uint64_t secid_q, uint64_t secid_h):
        self.secid_q = secid_q
        self.secid_h = secid_h
        self.position = 0
        self.ref_u = NAN
        self.calculated_ref_u = NAN
        self.calculated_spread_value = NAN
        self.aq_trades = deque()

    def __repr__(self):
        return (
            'AQPairFillStack{secid_q=%d, secid_h=%d, position=%d, num_trades=%d}'
        ) % (self.secid_q, self.secid_h, self.position, self._num_trades())

    cdef void _add_fill(self, DeioFill *fill):
        """
    if secid == qsecid:
        if side > 0:
            if not buy_quotes or (buy_quotes[-1] and buy_quotes[-1][0].price != price):
                buy_quotes.append(deque())
            buy_quotes[-1].append(fill)
        else:
            if not sell_quotes or (sell_quotes[-1] and sell_quotes[-1][0].price != price):
                sell_quotes.append(deque())
            sell_quotes[-1].append(fill)
    elif secid == hsecid:
        if side < 0 and buy_quotes:
            while fill.remqty > 0:
                qfill = buy_quotes[-1][0]
                if qfill.remqty > fill.remqty:
                    aq_trades.append((qfill, fill, fill.remqty))
                    qfill.remqty -= fill.remqty
                    fill.remqty = 0
                else:
                    aq_trades.append((qfill, fill, qfill.remqty))
                    fill.remqty -= qfill.remqty
                    buy_quotes[-1].popleft()
                print 'new pair!', aq_trades[-1]
                if not buy_quotes[-1]:
                    print 'popping buy_quotes...'
                    buy_quotes.pop()
        elif side > 0 and sell_quotes:
            while fill.remqty > 0:
                qfill = sell_quotes[-1][0]
                if qfill.remqty > fill.remqty:
                    aq_trades.append((qfill, fill, fill.remqty))
                    qfill.remqty -= fill.remqty
                    fill.remqty = 0
                else:
                    aq_trades.append((qfill, fill, qfill.remqty))
                    fill.remqty -= qfill.remqty
                    sell_quotes[-1].popleft()
                print 'new pair!', aq_trades[-1]
                if not sell_quotes[-1]:
                    print 'popping sell_quotes...'
                    sell_quotes.pop()
        """
        cdef:
            PairFill pair
            DeioFill *q
            DeioFill *h
            DeioFill *qfill
        if fill == NULL:
            return
        fill.remaining_qty = fill.qty
        pair = PairFill()
        q = pair._q_ptr()
        h = pair._h_ptr()
        if fill.secid == self.secid_q and fill.order_type == 5:
            if fill.side == 1:
                if self.buy_quotes.empty() or (not self.buy_quotes.back().empty() and self.buy_quotes.back().front().price == fill.price):
                    self.buy_quotes.push_back(deque_[DeioFill]())
                self.buy_quotes.back().push_back(dereference(fill))
            else:
                if self.sell_quotes.empty() or (not self.sell_quotes.back().empty() and self.sell_quotes.back().front().price == fill.price):
                    self.sell_quotes.push_back(deque_[DeioFill]())
                self.sell_quotes.back().push_back(dereference(fill))
        elif fill.secid == self.secid_h and fill.order_type == 4:
            if fill.side == 2 and not self.buy_quotes.empty():
                while fill.remaining_qty > 0:
                    qfill = &(self.buy_quotes.back().front())
                    copy_fill_to_pairfill(qfill, pair, True)
                    copy_fill_to_pairfill(fill, pair, False)
                    pair.spread_value = q.price - h.price
                    pair.ref_u = self.ref_u
                    if qfill.remaining_qty > fill.remaining_qty:
                        pair.spread_qty = fill.remaining_qty
                        print 'buy quote, qfill.remaining_qty > fill.remaining_qty', fill.qty, fill.remaining_qty, pair.spread_qty, 'before...', self.position
                        self.position += pair.spread_qty
                        self.aq_trades.append(pair)
                        qfill.remaining_qty -= fill.remaining_qty
                        fill.remaining_qty = 0
                    else:
                        pair.spread_qty = qfill.remaining_qty
                        print 'buy quote, qfill.remaining_qty <= fill.remaining_qty', fill.qty, fill.remaining_qty, pair.spread_qty, 'before...', self.position
                        self.position += pair.spread_qty
                        self.aq_trades.append(pair)
                        fill.remaining_qty -= qfill.remaining_qty
                        self.buy_quotes.back().pop_front()
                    if self.buy_quotes.back().empty():
                        self.buy_quotes.pop_back()
                    print 'bought pair!', self.secid_q, q.deioid, self.secid_h, h.deioid, pair.spread_qty
            elif fill.side == 1 and not self.sell_quotes.empty():
                while fill.remaining_qty > 0:
                    qfill = &(self.sell_quotes.back().front())
                    copy_fill_to_pairfill(qfill, pair, True)
                    copy_fill_to_pairfill(fill, pair, False)
                    pair.spread_value = q.price - h.price
                    pair.ref_u = self.ref_u
                    if qfill.remaining_qty > fill.remaining_qty:
                        print 'sell quote, qfill.remaining_qty > fill.remaining_qty', fill.qty, fill.remaining_qty, pair.spread_qty, 'before...', self.position
                        self.position += pair.spread_qty
                        pair.spread_qty = fill.remaining_qty
                        self.position -= pair.spread_qty
                        self.aq_trades.append(pair)
                        qfill.remaining_qty -= fill.remaining_qty
                        fill.remaining_qty = 0
                    else:
                        print 'sell quote, qfill.remaining_qty <= fill.remaining_qty', fill.qty, fill.remaining_qty, pair.spread_qty, 'before...', self.position
                        pair.spread_qty = qfill.remaining_qty
                        self.position -= pair.spread_qty
                        self.aq_trades.append(pair)
                        fill.remaining_qty -= qfill.remaining_qty
                        self.sell_quotes.back().pop_front()
                    if self.sell_quotes.back().empty():
                        self.sell_quotes.pop_back()
                    print 'sold pair!', self.secid_q, q.deioid, self.secid_h, h.deioid, pair.spread_qty
            else:
                return

    cdef void _update_ref_u(self, double ref_u) nogil:
        self.ref_u = ref_u

    cdef uint64_t _num_trades(self):
        return len(self.aq_trades) #.size()

    cdef int64_t _get_position(self) nogil:
        return self.position

    cdef void _calc_and_reset(self):
        cdef:
            PairFill pair
            uint64_t num_trades = self._num_trades()
            double total_ref_u = 0, total_spread_value = 0
        if num_trades == 0:
            return
        while self._num_trades() > 0:
            pair = self.aq_trades.popleft()
            total_ref_u += pair.ref_u
            total_spread_value += pair.spread_value
        self.calculated_ref_u = total_ref_u / <double>num_trades
        self.calculated_spread_value = total_spread_value / <double>num_trades


cdef class KospiOptionSpreadEvaluatorV1(SpreadPairModel):
    def __init__(self, uint64_t secid_a, uint64_t secid_b, uint64_t secid_u, 
                 uint64_t interval, int16_t option_type, double tte, double interest_rate, 
                 double strike_over, double strike_under,
                 double u_distance, double half_life, double price_multiplier,
                 uint64_t refit_interval, uint64_t poly_n, double residual_threshold,
                 pickle_filename,
                 double improve_u_offset, int64_t position_adjust_threshold):
        SpreadPairModel.__init__(self, SPREAD_TYPE_AB_SUBTRACT_U_LINEAR, secid_a, secid_b, secid_u)
        if improve_u_offset > 0.025:
            improve_u_offset = 0.025
        self.improve_u_offset = price_multiplier * improve_u_offset
        self.position_adjust_threshold = position_adjust_threshold
        self.process_fills = self.position_adjust_threshold > 0
        # init KospiOptionSpreadResampler
        self.interval = interval
        if option_type == 0:
            raise ValueError('option_type cannot equal 0. please use -1 or 1')
        self.option_type = option_type
        self.tte = tte
        self.interest_rate = interest_rate
        self.strike_over = strike_over
        self.strike_under = strike_under
        self.u_distance = u_distance
        self.half_life = half_life
        self.price_multiplier = price_multiplier
        self.resampler = KospiOptionSpreadResampler(self.interval, self.option_type, self.tte, self.interest_rate, 
                                                    self.strike_over, self.strike_under, self.u_distance,
                                                    self.half_life, self.price_multiplier)
        # refit interval
        self.refit_interval = refit_interval
        self.refit_time = 0
        # init PolyFitOneFeature
        self.poly_n = poly_n
        self.residual_threshold = residual_threshold
        self.model = PolyFitOneFeature(n=self.poly_n)
        if self.option_type > 0:
            self.model.add_constraint(self.model.x[1] >= 0)
        else:
            self.model.add_constraint(self.model.x[1] <= 0)
        self.failed_fit = True
        self.ratio = 0
        self.coef = None
        self.intercept = 0
        self.prev_rmean = 0
        self.prev_rerror = 0
        self.curr_rmean = 0
        self.curr_rerror = 0
        self.status = 'NULL'
        # for pickling
        self.pickle_filename = str(pickle_filename)
        self.read_pickle()
        # temporary variables for debugging
        self.pred_futprcs = None
        self.pred_spreads = None
        self.pred_spreads_prev = None
        # autoquoter handling
        self.aq_handler = AQPairFillStack(self.secid_a, self.secid_b)
        self.last_position = 0
        self.position_override = False

    def __repr__(self):
        return (
            'KospiOptionSpreadEvaluator{option_type=%d, strike_over=%0.4f, '
            'strike_under=%0.4f, u_range=%0.4f, status=%s, coef=%s, intercept=%0.4f, '
            'curr_fit: %0.4f +/- %0.4f}'
        ) % (
            self.option_type,
            self.strike_over, self.strike_under, 
            self.resampler._u_range(),
            self.status, self.coef, self.intercept,
            self.curr_rmean, self.curr_rerror
        )
        
    def read_pickle(self):
        pass
    
    def to_pickle(self):
        pass
    
    cdef void _update_refit_time(self) nogil:
        self.refit_time = (self.md_ptr.time / self.refit_interval) * self.refit_interval + self.refit_interval
    
    def evaluate_md(self):
        if not self.valid_md:
            return

        self._record_md()
        
        # do we refit?
        if self._time_to_evaluate():
            if self.refit_time == 0 or not self.resampler._has_u_range():
                self.log_debug(
                    (
                        'self.refit_time=%d, self.resampler._num_samples()=%d, self.resampler._u_range()=%0.4f'
                    ) % (self.refit_time, self.resampler._num_samples(), self.resampler._u_range())
                )
                self._update_refit_time()
                return
            
            # model stuff
            self._fit_model()
            
            # update time for next fitting
            self._update_refit_time()

            # save values?
            self._update_spread_pair_values()

    def evaluate_fills(self):
        cdef:
            DeioFill *fill
            int64_t position

        print 'why are we here?', self, self.process_fills
        while self._has_fills():
            fill = self._next_fill()
            self.aq_handler._add_fill(fill)
            self.log_warning(
                (
                    'yay fill! %d, %d, %0.4f, %d, %d, %d; aq_handler=%s' 
                ) % (
                    self._num_fills(), fill.secid, fill.price, fill.qty, fill.side, fill.order_type, self.aq_handler
                )
            )
            self._pop_fill()
            position = self.aq_handler._get_position()
            if self._position_past_threshold(position):
                self.log_warning('gotta update! last_position=%d, curr_position=%d' % (self.last_position, position))
                self.last_position = position
                self.aq_handler._calc_and_reset()
                self.position_override = True
           
    cdef bint _position_past_threshold(self, int64_t position) nogil:
        return fabs(self.last_position - position) >= self.position_adjust_threshold

    cdef void _update_spread_pair_values(self):
        # TODO: if self.position_override, use the current fills in aq_handler to determine fair value
        if self.position_override:
            self.log_warning('YEAH BUDDY! calculated_ref_u=%0.4f, calculated_spread_value=%0.4f' % (self.aq_handler.calculated_ref_u, self.aq_handler.calculated_spread_value))
            self.spread_value = self.price_multiplier * self.aq_handler.calculated_spread_value
            if self.last_position < 0:
                self.spread_value += 0.5
            else:
                self.spread_value -= 0.5
            self.ref_u = self.price_multiplier * self.aq_handler.calculated_ref_u
        else:
            self.spread_value = self.resampler._average_spread_price() * self.price_multiplier
            self.ref_u = self.resampler._average_futprc() * self.price_multiplier
        if self.coef is not None:
            self.spread_value_du = self.coef[0] * self.price_multiplier
        else:
            self.spread_value_du = 0.0

    cdef bint _time_to_evaluate(self) nogil:
        return self.md_ptr.time > self.refit_time
            
    cdef void _record_md(self):
        cdef MDOrderBook *md = self.md_ptr
        if md.secid == self.secid_a:
            self.resampler._record_over(md)
        elif md.secid == self.secid_b:
            self.resampler._record_under(md)
        elif md.secid == self.secid_u:
            self.resampler._record_u(md)
            self.aq_handler._update_ref_u(self.resampler.price_u)
            
    cdef void _fit_model(self):
        cdef:
            ndarray futprcs, spread_prices, w, du
            ndarray pred_spreads, pred_spreads_prev
            KospiOptionSpreadResampler resampler = self.resampler
            model = self.model
        # write to ndarrays
        resampler._export_to_ndarrays()
        # are there enough points?
        if resampler._use_black_scholes():
            du = linspace(-self.u_distance, self.u_distance, 100)
            futprcs = resampler._average_futprc() + du
            spread_prices = resampler._average_spread_price() + 0.85 * resampler._implied_spread_delta() * du
            w = ones(len(futprcs), dtype='<f8')
        else:
            futprcs = resampler.futprcs
            spread_prices = resampler.spread_prices
            w = resampler.w
        # attempt fit
        try:
            self.model.fit(futprcs, spread_prices, sample_weight=w)
            self.failed_fit = False
            self.status = self.model.prob.status
        except SolverError:
            self.log_warning('average_futprc=%0.4f' % resampler._average_futprc())
            self.log_warning('futprcs=%s, spread_prices=%s, w=%s' % (futprcs, spread_prices, w))
            self.log_warning('self.u_distance=%s' % self.u_distance)
            self.log_warning('and... average_spread_price=%0.4f, spread_delta=%0.4f, du=%s' % (resampler._average_spread_price(), resampler._implied_spread_delta(), du))
            self.log_warning('failed update... handle gracefully somehow. use_black_scholes=%s' % resampler._use_black_scholes())
            self.failed_fit = True
            self.status = 'FAILED'
        # evaluate
        if self.failed_fit:
            self.ratio = 0
            if self.coef is None:
                return
                #raise ValueError('failed_fit=True and coef is None. no good data?')
        elif self.coef is not None:
            pred_spreads_prev = model.predict(futprcs, coef=self.coef, intercept=self.intercept)
            # prev error calcs
            self.prev_rmean, self.prev_rerror = calc_residual_error(spread_prices, pred_spreads_prev, sample_weight=w)
            if fabs(self.prev_rmean) > self.residual_threshold:
                self.ratio = self.residual_threshold / fabs(self.prev_rmean)
                self.coef = self.ratio * self.coef + (1-self.ratio) * model.coef_
                self.intercept = self.ratio * self.intercept + (1-self.ratio) * model.intercept_
            else:
                self.ratio = 0
        else:
            pred_spreads_prev = None
            self.coef = model.coef_
            self.intercept = model.intercept_
        pred_spreads = model.predict(futprcs, coef=self.coef, intercept=self.intercept)
        self.curr_rmean, self.curr_rerror = calc_residual_error(spread_prices, pred_spreads, sample_weight=w)
        
        # remove later
        self.pred_futprcs = futprcs
        self.pred_spreads = pred_spreads
        self.pred_spreads_prev = pred_spreads_prev
