# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True

from libc.stdint cimport *
from libc.math cimport NAN
from glt.md.standard cimport MDOrderBook
from glt.modeling.spread_pairs.base cimport SpreadPairModel, SPREAD_TYPE_AB_SUBTRACT_U_LINEAR



cdef class DumbRELFPairModel(SpreadPairModel):
    """
    DumbRELFPairModel is a demonstration of how to inherit SpreadPairModel. It
    resembles the basic Recursive Exponentially-weighted Linear Filter that Deio
    uses to "value" pairs of contracts. We do not recommend that such a filter
    be used for theoretical valuation; a univariate filter like this should be
    used to filter out noise.

    The way Deio computed RELF was taking the mid-price of contracts A and B and
    recursively calculating the spread between the two using some ratio of its
    previously-calculated value with this new value. There is no relationship
    to an underlying contract, so it is set to zero.
    """
    cdef:
        readonly:
            str name
            double gain
            bint computed

    def __init__(self, uint32_t secid_a, uint32_t secid_b, name=None, double gain=0.0):
        cdef uint32_t secid_u = 0
        SpreadPairModel.__init__(self, SPREAD_TYPE_AB_SUBTRACT_U_LINEAR, secid_a, secid_b, secid_u)
        if name is None:
            self.name = 'no name'
        else:
            self.name = str(name)
        self.gain = gain
        # no underlying necessary (see secid_u=0)
        self.ref_u = 0
        self.spread_value_du = 0
        # just take the first computed value if we haven't computed anything yet
        self.computed = False

    def __repr__(self):
        return 'DumbRELFPairModel{name="%s", gain=%0.4f}' % (self.name, self.gain)

    def evaluate_md(self):
        if not self.valid_md:
            return
        # we need for all of our known market data to be valid
        if not self._all_valid_md():
            return

        # let's compute a spread
        self._compute_dumb_value()

        if self.computed:
            self.log_debug('new spread_value=%0.4f at seqnum=%d' % (self.spread_value, self.md_ptr.seqnum))

    cdef void _compute_dumb_value(self) nogil:
        cdef:
            MDOrderBook *md_a = self._md_for(self.secid_a)
            MDOrderBook *md_b = self._md_for(self.secid_b)
            double mid_a, mid_b, a_vs_b
        if md_a == NULL or md_b == NULL:
            return
        mid_a = 0.5 * (md_a.bids[0].price + md_a.asks[0].price)
        mid_b = 0.5 * (md_b.bids[0].price + md_b.asks[0].price)
        a_vs_b = mid_a - mid_b
        if self.computed:
            self.spread_value = self.gain * a_vs_b + (1 - self.gain) * self.spread_value
        else:
            self.spread_value = a_vs_b
            self.computed = True
