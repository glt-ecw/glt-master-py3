from libc.stdint cimport *
from libcpp.deque cimport deque as deque_
from libcpp.string cimport string as string_
from libcpp.vector cimport vector as vector_
from glt.md.standard cimport MDOrderBook
from glt.listening.oe cimport DeioFill
from glt.modeling.kospi_spreads cimport KospiOptionSpreadResampler
from glt.modeling.spread_pairs.base cimport SpreadPairModel
from numpy cimport ndarray



cdef class AQPairFillStack:
    cdef:
        uint64_t secid_q, secid_h
        vector_[deque_[DeioFill]] buy_quotes, sell_quotes
        #deque_[PairFill] aq_trades
        aq_trades
        int64_t position
        double ref_u
        readonly:
            double calculated_ref_u, calculated_spread_value

    cdef void _add_fill(self, DeioFill *fill)
    cdef void _update_ref_u(self, double ref_u) nogil
    cdef uint64_t _num_trades(self)
    cdef int64_t _get_position(self) nogil
    cdef void _calc_and_reset(self)



cdef class KospiOptionSpreadEvaluatorV1(SpreadPairModel):
    cdef:
        readonly:
            # resampler
            uint64_t interval
            int16_t option_type
            double tte, interest_rate, strike_over, strike_under, u_distance
            double half_life, price_multiplier
            KospiOptionSpreadResampler resampler
            # model
            uint64_t refit_interval, refit_time, poly_n
            double residual_threshold
            model
            bint failed_fit
            double ratio
            ndarray coef
            double intercept
            double prev_rmean, prev_rerror, curr_rmean, curr_rerror
            str status
            # pickling
            str pickle_filename
            # temp
            ndarray pred_futprcs, pred_spreads, pred_spreads_prev
            # fills
            AQPairFillStack aq_handler
            int64_t position_adjust_threshold, last_position
            bint position_override

        void _update_spread_pair_values(self)
        void _update_refit_time(self) nogil
        bint _time_to_evaluate(self) nogil
        void _record_md(self)
        void _fit_model(self)
        bint _position_past_threshold(self, int64_t position) nogil
    
