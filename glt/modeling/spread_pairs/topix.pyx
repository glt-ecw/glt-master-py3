# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True

from libc.stdint cimport *
from libc.math cimport NAN
from glt.md.standard cimport MDOrderBook
from glt.listening.oe cimport DeioFill
from glt.modeling.spread_pairs.base cimport SpreadPairModel, SPREAD_TYPE_AB_SUBTRACT_U_LINEAR
from glt.math.helpful_functions cimport double_eq, double_lt, double_gt, double_lte, double_gte
from glt.modeling.filter_base cimport FilterModel
import datetime
import pandas as pd
import numpy as np
cimport numpy as cnp

cdef class NKTP_FilterModel(FilterModel):
    cdef:
        readonly:
            int num_outliers, model_trigger_contract, secid_a, secid_b
            double z_outlier

    def __init__(self, int secid_a, int secid_b, int num_outliers, int model_trigger_contract, 
                double z_outlier, dict mdfilter_params):
        FilterModel.__init__(self, mdfilter_params)
        self.num_outliers = num_outliers
        self.model_trigger_contract = model_trigger_contract
        self.z_outlier = z_outlier
        self.secid_a = secid_a
        self.secid_b = secid_b

    def __repr__(self):
        return '''tv: %s, NK: %s (%s, %s), TP: %s (%s, %s)''' % (
                self.tv, self.sec_list[self.secid_a].md['mid'], self.sec_list[self.secid_a].md['seqnum'], 
                self.sec_list[self.secid_a].md['seqnum_valid'], self.sec_list[self.secid_b].md['mid'],
                self.sec_list[self.secid_b].md['seqnum'], self.sec_list[self.secid_b].md['seqnum_valid']
                )
       
    cdef void compute_model(self, int secid):
        #raise NotImplemented('please implement .compute_model()')
        #herein lies the rub
        cdef:
            double curr_val, temp_tv, std, avg
            int s_len = len(self.samples)
            bint add_curr = True
        curr_val = (self.sec_list[self.secid_a].md['mid'] - self.sec_list[self.secid_b].md['mid'] * 10) / 100.
        
        #Bail out if: 
        # any of the leg values are nan
        # OR current value is the same as previous value or the value before
        #   the previous (this forces at least a 3 price cycle, which
        # stretches your std a bit and makes the tv more stable).
        if np.isnan(self.sec_list[self.secid_a].md['mid']) or np.isnan(self.sec_list[self.secid_b].md['mid']):
            return
        if double_eq(curr_val, self.last_val):
            return
        if s_len>=2 and (double_eq(curr_val, self.samples[s_len-2]) or double_eq(curr_val, self.samples[s_len-1])):
            #reset outlier flag if your curr_val is back to a value in the samples list
            self.prev_outlier = 0
            self.outliers = []
            return 

        #we see if the z score of the current temp_val is >= 2 and if so set the outlier flag
        #   if the outlier flag is already set that means this is the second >= 2 outlier
        #   and is probably a signal, so we drop all the other samples and start over using
        #   using the previous 2 samples as a starting point.
        if len(self.samples) > 2:
            std = np.std(self.samples, ddof=1)  # denominator is (n-ddof) 
            avg = np.mean(self.samples)
            # set z score and handle zero division
            if double_eq(std, 0):
                z_score = 0
            else:
                z_score = (curr_val - avg) / std

            # check whether z score is "outlier" and is in the same direction
            # as last outlier (unless its the first outlier)
            if double_gte(np.abs(z_score), self.z_outlier):
                print '>>>>>>>  z score(#%s): %.2f, curr val: %s (nk: %s, tp: %s) ts: %s, secid: %s' % (
                            self.prev_outlier, z_score, curr_val, 
                            self.sec_list[self.secid_a].md['mid'], self.sec_list[self.secid_b].md['mid'], 
                            pd.Timestamp(self.sec_list[secid].md['ts']), secid
                            )
                # we want to reset the samples list to the outlier values but only
                # if weve had > num_outliers in a row.  We do this with the prev_outlier var.
                if self.prev_outlier == self.num_outliers:
                    self.samples = self.outliers
                    self.outliers = []
                    self.prev_outlier = 0 
                else:
                    #check that this is either the first outlier or that its the same direction
                    #  as the last outlier.  If its not, we reset the counter to 1, store value
                    if self.prev_outlier == 0 or np.sign(self.last_z_dir) == np.sign(z_score):
                        self.prev_outlier += 1
                        if curr_val not in self.outliers:
                            self.outliers.append(curr_val)
                    else:
                        self.prev_outlier = 1
                        self.outliers = [curr_val]
                    # store direction of outlier
                    if z_score < 0:
                        self.last_z_dir = -1
                    else:
                        self.last_z_dir = 1
            else:
                self.prev_outlier = 0
        
        #add curr value only if its not an outlier
        #if self.prev_outlier == 0 and add_curr:
        if self.prev_outlier == 0:
            self.samples.append(curr_val)
        self.last_val = curr_val
        # do not update the model if it has only moved "a little bit"
        self.round_tv()

    cdef round_tv(self):
        cdef:
            double avg = np.mean(self.samples)
            int i=1
        
        if len(self.samples) == 0:
            self.tv = 0
            return
        elif len(self.samples) == 1:
            self.tv = self.samples[0]
            return

        # if avg is a multiple of 5 remove last sample and try again
        while double_eq(avg % 5.0, 0):
            avg = np.mean(self.samples[:len(self.samples)-i])
            i+=1

        #checking which is the nearest 2.5 value
        if not double_eq(avg % 2.5, 0):
            if double_lt(avg % 5, 2.5):
                self.tv = avg - avg % 2.5 + 2.5
            else:
                self.tv = avg - avg % 2.5
        else:
            self.tv = avg
            

cdef class NK_TP(SpreadPairModel):
    
    cdef:
        readonly:
            str name
            double gain, last_a, last_b, spread_value2, tight_a, tight_b, pause_window, pos_adj, adjust
            long a_ts, b_ts, seqnum
            int position
            bint computed, a_chg, b_chg
            cnp.ndarray history
            NKTP_FilterModel test_filter
            f

    def __init__(self, uint32_t secid_a, uint32_t secid_b, name=None):
        cdef uint32_t secid_u = secid_b
        SpreadPairModel.__init__(self, SPREAD_TYPE_AB_SUBTRACT_U_LINEAR, secid_a, secid_b, secid_u)
        if name is None:
            self.name = 'no name'
        else:
            self.name = str(name)
        #set vars (some of these should be more elegantly set)
        self.ref_u = 0
        self.spread_value_du = 900
        self.last_a = 0
        self.last_b = 0
        self.spread_value = 0
        self.spread_value2 = 0
        self.tight_a = 1000
        self.tight_b = 50
        self.a_ts = 0
        self.b_ts = 0
        self.pause_window = 5e7 # in nanos
        self.seqnum = 0
        self.history = np.zeros(2)
        self.a_chg = False
        self.b_chg = False
        # just take the first computed value if we haven't computed anything yet
        self.computed = False

        #adjustment stuff
        self.process_fills=True
        self.position = 0 
        self.pos_adj = 2.5
        self.adjust = 0 
        
        #init MDFilter stuff
        # TODO: do this as parameters from __init__
        test_dict = {self.secid_a : {'min_qty' : 50, 'min_par' : 0, 'pause_window' : 5e9, 
                                'mkt_width' : 1000, 'leader' : True, 'weighted' : False
                        }, 
                    self.secid_b : {'min_qty' : 20, 'min_par' : 0, 'pause_window' : 5e9, 
                                'mkt_width' : 50, 'leader' : True, 'weighted' : False
                        } 
                    }
        num_outliers = 1
        z_outlier = 2.0
        model_trigger_contract = self.secid_a
        
        #def __init__(self, int secid_a, int secid_b, int num_outliers, int model_trigger_contract, 
        #        double z_outlier, dict mdfilter_params):
        self.test_filter = NKTP_FilterModel(self.secid_a, self.secid_b, num_outliers, 
                                        model_trigger_contract, z_outlier, test_dict)
        
    def __repr__(self):
        return '<<< NK_TP >>> tv: %s (%s/%s), deio_tv: %s, ref_u: %s (nk mid: %s, tp mid: %s) [%s]' % (
                self.spread_value2/100., self.adjust, self.position, self.spread_value/100., self.ref_u/100., 
                self.test_filter.sec_list[self.secid_a].md['mid']/100.,
                self.test_filter.sec_list[self.secid_b].md['mid']/100., len(self.test_filter.samples)
                )

    def evaluate_fills(self):
        cdef:
            DeioFill *fill
            int fill_threshold = 1

        while self._has_fills():
            fill = self._next_fill()
            #TODO: these values shouldnt be hardcoded
            if fill.serverid == 97 and fill.assumed_fill_type == 1 and fill.secid == self.secid_a:
                print "FILL! servid: %s, side: %s, type: %s, secid: %s" % (
                        fill.serverid, fill.side, fill.assumed_fill_type, fill.secid
                        )
                self.position += fill.qty * (3-2*fill.side)
                if self.position >= fill_threshold:
                    self.adjust = -self.pos_adj
                elif self.position <= -fill_threshold:
                    self.adjust = self.pos_adj
                else:
                    self.adjust = 0
            self._pop_fill()

    def evaluate_md(self):
        if not self.valid_md:
            return
        # we need for all of our known market data to be valid
        if not self._all_valid_md():
            return

        # let's compute a spread
        self._compute_spread()

        if self.computed:
            self.log_debug('NK_TP: new spread_value=%0.2f (%0.2f) at seqnum=%d' % (
                self.spread_value, self.spread_value2, self.md_ptr.seqnum))

    cdef void _compute_spread(self):
        cdef:
            MDOrderBook *md_a = self._md_for(self.secid_a)
            MDOrderBook *md_b = self._md_for(self.secid_b)
            double a_vs_b #,mid_a , mid_b
            double mid_a = 0.5 * (md_a.bids[0].price + md_a.asks[0].price)
            double mid_b = 0.5 * (md_b.bids[0].price + md_b.asks[0].price)

        # bail out scenarios:
        if md_a == NULL or md_b == NULL:
            return
        
        self.test_filter.evaluate_md(md_a)
        self.test_filter.evaluate_md(md_b)
        
        #for logging changing md (not required to function correctly)
        curr_a = self.test_filter.sec_list[self.secid_a].md['mid'] 
        stage_a =  self.test_filter.sec_list[self.secid_a].md_staging['mid']
        seq_a = self.test_filter.sec_list[self.secid_a].md_staging['seqnum']
        valid_a = self.test_filter.sec_list[self.secid_a].md['seqnum_valid']
        curr_b = self.test_filter.sec_list[self.secid_b].md['mid'] 
        stage_b =  self.test_filter.sec_list[self.secid_b].md_staging['mid']
        seq_b = self.test_filter.sec_list[self.secid_b].md_staging['seqnum']
        valid_b = self.test_filter.sec_list[self.secid_b].md['seqnum_valid']

        if not double_eq(curr_a, stage_a) and not self.a_chg:
            self.a_chg=True
            print 'NK change: %s to %s (seq: %s)' % (curr_a/100, stage_a/100, seq_a) 
            #bad use of old var:
            self.tight_a = stage_a
        elif double_eq(curr_a, stage_a) and self.a_chg and double_eq(self.tight_a, curr_a):
            self.a_chg=False
            print 'NK now valid: %s (seq: %s, valid: %s)' % (curr_a/100, seq_a, valid_a)
            if len(self.test_filter.samples) > 0:
                avg = np.mean(self.test_filter.samples)
            else:
                avg = 'nan'
            print '  Stored Values: %s, mean: %s' % (
                    self.test_filter.samples, avg
                    )

        if not double_eq(curr_b, stage_b) and not self.b_chg:
            self.b_chg=True
            print 'TP change: %s to %s (seq: %s)' % (curr_b/100, stage_b/100, seq_b) 
            #bad use of old var:
            self.tight_b = stage_b
        elif double_eq(curr_b, stage_b) and self.b_chg and double_eq(self.tight_b, curr_b):
            self.b_chg=False
            print 'TP now valid: %s (seq: %s, valid: %s)' % (curr_b/100, seq_b, valid_b) 
            if len(self.test_filter.samples) > 0:
                avg = np.mean(self.test_filter.samples)
            else:
                avg = 'nan'
            print '  Stored Values: %s, mean: %s' % (
                    self.test_filter.samples, avg
                    )
        #end logging

        if not double_eq(self.spread_value2 / 100., self.test_filter.tv + self.adjust):
            if len(self.test_filter.samples) > 0:
                avg = np.mean(self.test_filter.samples)
            else:
                avg = 'nan'
            print '  Stored Values: %s, mean: %s' % (
                    self.test_filter.samples, avg
                    )

            #hey ape man... this is nk - 9tp
            self.spread_value2 = (self.test_filter.tv + self.adjust) * 100
            mid_b = self.test_filter.sec_list[self.secid_b].md['mid']
            mid_a = self.spread_value2 + mid_b * 10
            self.spread_value = mid_a - mid_b

            self.ref_u = mid_b
            self.computed = True

        else:
            self.computed = False

