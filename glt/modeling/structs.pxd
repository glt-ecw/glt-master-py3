from numpy cimport ndarray


cdef enum MarketSide:
    Bid = 1
    Ask

cdef struct MarketState:
    double bidprc0
    double bidprc1
    double bidprc2
    double bidprc3
    double bidprc4
    long bidqty0
    long bidqty1
    long bidqty2
    long bidqty3
    long bidqty4
    long bidpar0
    long bidpar1
    long bidpar2
    long bidpar3
    long bidpar4
    double askprc0
    double askprc1
    double askprc2
    double askprc3
    double askprc4
    long askqty0
    long askqty1
    long askqty2
    long askqty3
    long askqty4
    long askpar0
    long askpar1
    long askpar2
    long askpar3
    long askpar4
    double buyprice
    long buyvolume
    double sellprice
    long sellvolume
    #double tradeprc
    #long tradeqty


cdef struct EvaluatedMarket:
    double bidprc
    double askprc
    bint good_bid
    bint good_ask


cdef struct VolSides:
    double bid
    double ask


cdef struct TradeData:
    long time
    long side
    double price
    long qty


cdef class WrappedMarketState:
    cdef MarketState *mkt


cdef class VolumeAccumulator:
    cdef:
        TradeData[:] tdata
        size_t head, alloced
        readonly size_t length
        readonly long totalqty, ref_time, time_window
        readonly MarketSide side
        readonly double price, priceqty
        readonly bint any_trade
   
        void _init(self) nogil
        bint _equals_price(self, double price) nogil 
        bint _valid_trade(self, MarketSide side, double price) nogil
        long _calc_ref_time(self, long time) nogil
        bint _valid_time(self, long time) nogil
        void _update_price(self, double price, bint reset=*)
        void _update_side(self, MarketSide side) nogil
        void _resize_tdata(self)
        void _push_tdata(self, TradeData *tdat)
        void _add_trade(self, long time, MarketSide side, double price, long qty)
        void _remove_trades(self, long time) nogil


cdef ndarray initialized_market_states(df)
cdef MarketState marketstate_zeroed() nogil
cdef EvaluatedMarket new_evaluated_market(MarketState *mkt, long min_qty, long min_par, double vol) nogil

