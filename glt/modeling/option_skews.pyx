import datetime

cimport cython

from glt.modeling cimport volpaths
from glt.math import bezier

from glt.math cimport bs

from numpy cimport ndarray
from libc.math cimport log as flog, sqrt as fsqrt, fabs, round as fround, NAN, isnan

from numpy import array, empty, zeros, ones, float64, int64, where

from glt.math.helpful_functions cimport *
from glt.modeling.structs cimport *


cdef enum WeightType:
    NoWeight = 0
    DeltaWeight
    RatioWeight
    MarketWidthPutWeight

cdef enum StrikeType:
    Price = 0
    Moneyness
    SD


def generate_rawstrike_maps(raw_options):
    calls = filter(lambda x: x[1].cp > 0, raw_options)
    rs_calls = dict(map(lambda x: (x[1].k, x[1]), calls))
    symbol_calls = dict(map(lambda x: (x[1].k, x[0]), calls))

    puts = filter(lambda x: x[1].cp < 0, raw_options)
    rs_puts = dict(map(lambda x: (x[1].k, x[1]), puts))

    strikes = list(set(map(lambda x: x[1].k, raw_options)))
    strikes.sort()

    raw_calls = map(lambda k: rs_calls.get(k), strikes)
    raw_puts = map(lambda k: rs_puts.get(k), strikes)
    symbols = map(lambda k: symbol_calls.get(k) or 'UNKNOWN:%0.3f' % k, strikes)
    symbols_map = dict(zip(strikes, symbols))

    return array(strikes), array(raw_calls), array(raw_puts), symbols_map


cdef class BezierModel:
    cdef:
        public dict raw_option_dict
        readonly dict smoothed_vol, smoothed_vol_vp, moneyness, volpath, tvs, vega, adjust, rs_map, symbols_map
        readonly dict latest_curve, latest_tick_devs
        readonly cycles_until_update, violation_count
        readonly double band, band_pct, put_delta, call_delta, t, spot
        readonly double[:, :] vp
        readonly str reason
        readonly WeightType weight_type
        readonly ndarray strikes, rs_calls, rs_puts, rs_otm, points
        readonly ndarray input_mask, output_mask, delta_mask, weights
        readonly double minpar, minqty, gain
        readonly bint updated
        readonly list update_data
        readonly size_t degree
        readonly fitter, tmp_fitter
        readonly StrikeType strike_type
        readonly double sd_zero_weights_within

    def __init__(self, list raw_options, double option_minqty, double option_minpar,
            double min_pdelta, double min_cdelta, size_t degree,
            double band, double band_pct, str weight_type=None,
            long cycles_until_update=0, str exch=None, bint use_volpath=False, str strike_type=None,
            double sd_zero_weights_within=NAN, *args, **kwargs):
        cdef:
            size_t n
            double t
        self.raw_option_dict = dict(raw_options)

        self.strikes, self.rs_calls, self.rs_puts, self.symbols_map = generate_rawstrike_maps(raw_options)
        n = len(self.strikes)

        self.input_mask = zeros(n, dtype=bool)
        self.output_mask = zeros(n, dtype=bool)
        self.delta_mask = zeros(n, dtype=bool)
        self.weights = ones(n, dtype=float64)

        self.smoothed_vol = {}
        self.smoothed_vol_vp = {}
        self.moneyness = {}
        self.volpath = {}
        self.tvs = {}
        self.vega = {}
        self.cycles_until_update = cycles_until_update
        self.violation_count = 0
        self.latest_curve = {}
        self.latest_tick_devs = {}

        self.spot = 0
        self.put_delta = min_pdelta
        self.call_delta = min_cdelta
        self.degree = degree
        self.minqty = option_minqty
        self.minpar = option_minpar
        self.updated = False

        self.band = band
        self.band_pct = band_pct
        self.reason = ''
        if weight_type is None:
            self.weight_type = NoWeight
        elif weight_type.lower() == 'delta':
            self.weight_type = DeltaWeight
        elif weight_type.lower() == 'ratio':
            self.weight_type = RatioWeight
        else:
            self.weight_type = NoWeight

        if strike_type is None:
            self.strike_type = Price
        elif strike_type.lower() == 'moneyness':
            self.strike_type = Moneyness
        elif strike_type.lower() == 'sd':
            self.strike_type = SD
        else:
            self.strike_type = Price

        # set sd weighting to zero (only if we're using that strike_type)
        if self.strike_type == SD:
            self.sd_zero_weights_within = sd_zero_weights_within
        else:
            self.sd_zero_weights_within = NAN

        for rs in self.rs_calls:
            if rs is not None:
                t = rs.t
                break
        self.t = t

        if use_volpath and exch is not None:
            self.vp = volpaths.createVP(t * 260, exch)
        else:
            self.vp = zeros((0, 0))

        self.update_data = []
        self.gain = 0.5

        # yeah buddy
        self.points = empty((n, 2), dtype=float64)
        self.points[:] = NAN
        self.fitter = bezier.BezierFit(degree)
        self.tmp_fitter = bezier.BezierFit(degree)

    def force_values(self, double spot, dict smoothed_vol):
        cdef:
            ndarray rs_otm
            dict rs_map

        rs_otm, rs_map = self.build_rs_maps(spot)
        self.commit_new_curve(smoothed_vol, rs_otm, rs_map, spot)

    def force_rawvol(self, long secid, double vol):
        self.raw_option_dict[secid].force_vol(vol)

    def update_vol_mdfwd(self, long ts, long secid, tuple deio_md, int n_levels,
                         double spot, long opt_seqnum, long fut_seqnum):
        if secid in self.raw_option_dict:
            self.raw_option_dict[secid].calc_raw_vol_mdfwd(
                deio_md, n_levels, spot, ts, self.minpar, self.minqty, opt_seqnum, fut_seqnum
            )

    def update_gain(self, double gain):
        self.gain = gain

    cdef commit_new_curve(self, dict latest_curve, ndarray rs_otm, dict rs_map, double spot):
        cdef:
            double v, gain = self.gain
            list del_keys
            ndarray old_points, new_points

        # work in progress
        if self.fitter.coef is None:
            self.fitter.copyfrom(self.tmp_fitter)
        else:
            old_points = self.fitter.transform(self.points[self.output_mask])
            new_points = self.tmp_fitter.transform(self.points[self.output_mask])
        # end
            
        if not self.smoothed_vol:
            self.smoothed_vol = latest_curve
        else:
            del_keys = filter(lambda k: k not in latest_curve, self.smoothed_vol)
            for key in del_keys:
                del self.smoothed_vol[key]
            for key in latest_curve:
                if key not in self.smoothed_vol:
                    self.smoothed_vol[key] = latest_curve[key]
                else:
                    self.smoothed_vol[key] = gain * latest_curve[key] + (1-gain) * self.smoothed_vol[key]

        self.smoothed_vol_vp = self.smoothed_vol.copy()

        self.tvs = {k: rs_map[k].calc_tv(spot, v) for k, v in self.smoothed_vol.iteritems()}
        self.vega = {k: rs_map[k].calc_vega(spot, v) for k, v in self.smoothed_vol.iteritems()}
        self.moneyness = {k: rs_map[k].calc_moneyness(spot) for k in self.smoothed_vol}
        self.volpath = {k: volpaths.vp_for_moneyness(m, spot, spot+1, self.vp) for k, m in self.moneyness.iteritems()}
        self.spot = spot

        self.rs_otm = rs_otm
        self.rs_map = rs_map
        self.updated = True

    def reset_violation_count(self):
        self.violation_count = 0

    def check_bands(self, double current_spot, bint check_using_skew=True, force_update=False):
        cdef:
            dict outside_bands = {}
            dict latest_curve, rs_map, latest_tick_devs
            ndarray rs_otm
            double vega, dev, band, vol, sm_vol
            bint use_volpath = (len(self.vp)>0), respline
            list update_data = [], reasons = []
            double tick_dev, n_outside, outside_pct, n_keys = 0

        latest_curve, rs_otm, rs_map = self.build_curve(current_spot)
        self.updated = False

        latest_tick_devs = {}
        for key in latest_curve:
            sm_vol = latest_curve[key]
            rs = rs_map[key]
            vega = rs.calc_vega(current_spot, sm_vol)
            latest_tick_devs[key] = vega * (rs.vol - sm_vol)

        self.latest_curve = latest_curve
        self.latest_tick_devs = latest_tick_devs

        if not self.vega or len(latest_curve) - len(self.vega) >= 2:
            self.reason = 'updated. len(latest_curve)=%d, len(self.vega)=%d' % (len(latest_curve), len(self.vega))
            self.commit_new_curve(latest_curve, rs_otm, rs_map, current_spot)
        else:
            for key, vega in self.vega.iteritems():
                if check_using_skew and key not in latest_curve:
                    continue

                if double_eq(self.spot, current_spot) or not use_volpath:
                    dvp = 0
                else:
                    dvp = self.volpath[key] * (current_spot - self.spot)

                #this check is essentially meaningless since check_using_skew is defaulted to True
                if check_using_skew:
                    vol = latest_curve[key]
                else:
                    vol = rs_map[key].vol
                sm_vol = self.smoothed_vol[key]
                dev = (vol - (sm_vol + dvp))
                tick_dev = vega * dev
                n_keys += 1

                # does the latest curve vol deviate from the upper bound of the bandwidth?
                if double_gt(tick_dev, self.band) or double_lt(tick_dev, -1 * self.band):
                    outside_bands[key] = vol, sm_vol, dvp, vega, tick_dev
                self.smoothed_vol_vp[key] = self.smoothed_vol[key] + dvp

            #find strikes outside of band
            if force_update:
                outside_pct = 1.0
            else:
                outside_pct = len(outside_bands) / n_keys

            if double_gte(outside_pct, self.band_pct):
                update_data.append(('force_update=%s; need to update curve' % force_update, outside_bands, n_keys))

                self.violation_count += 1
                if force_update or self.violation_count >= self.cycles_until_update:
                    self.commit_new_curve(latest_curve, rs_otm, rs_map, current_spot)
                    self.reset_violation_count()
                    self.reason = 'updated. see update_data for more info'
                else:
                    self.reason = 'tried to update ( %d / %d )' % (self.violation_count, self.cycles_until_update)
            else:
                if self.violation_count > 0:
                    self.reason = 'resetting violation_count %d back to zero' % self.violation_count
                    self.reset_violation_count()
                else:
                    self.reason = ''
            self.update_data = update_data


    cdef tuple build_rs_maps(self, double spot):
        cdef ndarray rs_otm = where(self.strikes<spot, self.rs_puts, self.rs_calls)
        return rs_otm, {self.symbols_map[k]: rs for k, rs in zip(self.strikes, rs_otm)}

    cdef tuple build_curve(self, double spot):
        cdef:
            long i
            ndarray input_mask, output_mask, delta_mask, moneynesses, weights, rs_otm, points
            bint use_input, use_volpath = (len(self.vp)>0), change_degrees
            double moneyness, corr_delta, delta_adj, v, abs_delta, degree = self.degree, xpt
            long good_count = 0
            dict rs_map

        # split all possible syms into puts and calls based on spot, sort them and find relevant syms and floats
        delta_mask = self.delta_mask
        input_mask = self.input_mask
        output_mask = self.output_mask
        weights = self.weights
        points = self.points

        delta_mask[:] = False
        input_mask[:] = False
        output_mask[:] = False

        weights[:] = 1

        if not double_eq(spot, self.spot):
            rs_otm, rs_map = self.build_rs_maps(spot)
        else:
            rs_otm = self.rs_otm
            rs_map = self.rs_map

        for i, rs in enumerate(rs_otm):
            if rs is None:
                continue

            v = rs.vol
            use_input = double_gt(v, 0)

            if use_input:
                if use_volpath:
                    moneyness = rs.calc_moneyness(spot)
                    delta_adj = volpaths.vp_for_moneyness(moneyness, spot, spot+1, self.vp) * rs.calc_vega(spot, v)
                else:
                    delta_adj = 0
                corr_delta = rs.calc_delta(spot, v) + delta_adj
                delta_mask[i] = (rs.cp > 0 and double_gte(corr_delta, self.call_delta)) or (rs.cp < 0 and double_lte(corr_delta, self.put_delta))

                if self.strike_type == Price:
                    xpt = rs.k
                elif self.strike_type == Moneyness:
                    xpt = rs.calc_moneyness(spot)
                elif self.strike_type == SD:
                    xpt = rs.calc_sd(spot)

                points[i, 0] = xpt
                points[i, 1] = v

                abs_delta = fabs(corr_delta)

                # figure out weights
                if not isnan(self.sd_zero_weights_within):
                    if double_lte(fabs(xpt), self.sd_zero_weights_within):
                        weights[i] = 1e-10
                elif self.weight_type == DeltaWeight:
                    if double_lt(abs_delta, 0.2):
                        weights[i] = abs_delta / 0.2
                    else:
                        weights[i] = 0.2 / abs_delta
                elif self.weight_type == RatioWeight:
                    weights[i] = rs.weight
                good_count += delta_mask[i] # & double_gte(weights[i], 0)

            input_mask[i] = use_input
            output_mask[i] = True

        input_mask &= delta_mask
        output_mask &= delta_mask

        change_degrees = (good_count <= (degree + 1))
        if change_degrees:
            degree = good_count - 1
            self.tmp_fitter.update_degrees(degree)

        self.tmp_fitter.fit(points[input_mask], weights[input_mask])
#        print self.tmp_fitter.coef
#        print points[input_mask], weights[input_mask]
        curve = self.tmp_fitter.transform(points[output_mask])

        if change_degrees:
            self.tmp_fitter.update_degrees(self.degree)

        for v in curve[:, 1]:
            if double_lte(v, 0):
                raise ValueError('smoothed vols <= 0', curve, points[input_mask], weights[input_mask], self.tmp_fitter.coef, degree, good_count)

        return {self.symbols_map[k]: v for k, v in zip(self.strikes[output_mask], curve[:, 1])}, rs_otm, rs_map


cdef class OldBezierModel:
    cdef:
        public dict raw_option_dict
        readonly set put_keys, atm_keys, call_keys
        readonly dict smoothed_vol, smoothed_vol_vp, moneyness, volpath, tvs, vega, adjust, rs_map, symbols_map
        readonly long degree, sections, cycles_until_update, violation_count
        readonly double band, band_pct, put_delta, call_delta, t, spot
        readonly double[:, :] vp
        readonly str reason
        readonly WeightType weight_type
        readonly ndarray strikes, rs_calls, rs_puts, rs_otm
        readonly ndarray tmp_input_mask, tmp_output_mask, tmp_delta_mask, tmp_vols, tmp_weights
        readonly double minpar, minqty
        readonly bint updated
        readonly list update_data

    def __cinit__(self, list raw_options, str exch, double option_minqty, double option_minpar,
            double min_pdelta, double min_cdelta, long degree,
            double band, double band_pct, str weight_type,
            long cycles_until_update=0, long sections=2, bint use_volpath=False):
        cdef:
            long n
            double t
        self.raw_option_dict = dict(raw_options)

        self.strikes, self.rs_calls, self.rs_puts, self.symbols_map = generate_rawstrike_maps(raw_options)
        n = len(self.strikes)

        self.tmp_input_mask = zeros(n, dtype=bool)
        self.tmp_output_mask = zeros(n, dtype=bool)
        self.tmp_delta_mask = zeros(n, dtype=bool)
        self.tmp_vols = zeros(n, dtype=float64)
        self.tmp_weights = ones(n, dtype=float64)

        self.smoothed_vol = {}
        self.smoothed_vol_vp = {}
        self.moneyness = {}
        self.volpath = {}
        self.tvs = {}
        self.vega = {}
        self.sections = sections
        self.cycles_until_update = cycles_until_update
        self.violation_count = 0

        self.spot = 0
        self.put_delta = min_pdelta
        self.call_delta = min_cdelta
        self.degree = degree
        self.minqty = option_minqty
        self.minpar = option_minpar
        self.updated = False

        self.band = band
        self.band_pct = band_pct
        self.put_keys = set()
        self.atm_keys = set()
        self.call_keys = set()
        self.reason = ''
        if weight_type == 'delta':
            self.weight_type = DeltaWeight
        elif weight_type == 'mktWidthPut':
            self.weight_type = MarketWidthPutWeight
        elif weight_type == 'ratio':
            self.weight_type = RatioWeight
        else:
            self.weight_type = NoWeight

        for rs in self.rs_calls:
            if rs is not None:
                t = rs.t
                break
        if use_volpath:
            self.vp = volpaths.createVP(t * 260, exch)
        else:
            self.vp = zeros((0, 0))
        self.update_data = []

    def force_values(self, double spot, dict smoothed_vol):
        cdef:
            ndarray rs_otm
            dict rs_map

        rs_otm, rs_map = self.build_rs_maps(spot)
        self.commit_new_curve(smoothed_vol, rs_otm, rs_map, spot)

    def force_rawvol(self, long secid, double vol):
        self.raw_option_dict[secid].force_vol(vol)

    def update_vol_mdfwd(self, long ts, long secid, tuple deio_md, int n_levels,
                         double spot, long opt_seqnum, long fut_seqnum):
        if secid in self.raw_option_dict:
            self.raw_option_dict[secid].calc_raw_vol_mdfwd(
                deio_md, n_levels, spot, ts, self.minpar, self.minqty, opt_seqnum, fut_seqnum
            )

    def update_vol_marketstate(self, WrappedMarketState w):
        pass

    cdef void commit_new_curve(self, dict latest_curve, ndarray rs_otm, dict rs_map, double spot):
        cdef double v

        self.smoothed_vol = latest_curve
        self.smoothed_vol_vp = latest_curve.copy()

        self.tvs = {k: rs_map[k].calc_tv(spot, v) for k, v in self.smoothed_vol.iteritems()}
        self.vega = {k: rs_map[k].calc_vega(spot, v) for k, v in self.smoothed_vol.iteritems()}
        self.moneyness = {k: rs_map[k].calc_moneyness(spot) for k in self.smoothed_vol}
        self.volpath = {k: volpaths.vp_for_moneyness(m, spot, spot+1, self.vp) for k, m in self.moneyness.iteritems()}
        self.spot = spot

        self.create_bands(rs_map)

        self.rs_otm = rs_otm
        self.rs_map = rs_map
        self.updated = True

    cdef create_bands(self, dict rs_map):
        cdef:
            set relevant_keys = set(self.tvs)
            set atm_keys, put_keys

            double wide_prc = 5.0

        if self.sections == 3:
            atm_keys = set(filter(lambda x: x[1] > wide_prc, self.tvs.iteritems()))
            self.atm_keys = relevant_keys.intersection(atm_keys)
            put_keys = set(filter(lambda x: x[1] <= wide_prc and rs_map[x[0]].cp < 0, self.tvs.iteritems())) - self.atm_keys
            self.put_keys = relevant_keys.intersection(put_keys)
        elif self.sections == 2:
            put_keys = set(filter(lambda x: rs_map[x].cp < 0, self.tvs))
            self.put_keys = relevant_keys.intersection(put_keys)
        self.call_keys = relevant_keys - self.atm_keys - self.put_keys

    def reset_violation_count(self):
        self.violation_count = 0

    def check_bands(self, double current_spot, bint check_using_skew=False, force_update=False):
        cdef:
            dict hi_calls = {}, hi_atm = {}, hi_puts = {}
            dict lo_calls = {}, lo_atm = {}, lo_puts = {}
            dict latest_curve, rs_map
            ndarray rs_otm
            double vega, dev, band, vol, sm_vol
            double hi_puts_pct, hi_calls_pct, hi_atm_pct, lo_puts_pct, lo_calls_pct, lo_atm_pct
            bint raise_calls, raise_puts, raise_atm, lower_calls, lower_puts, lower_atm
            bint use_volpath = (len(self.vp)>0)
            list update_data = [], reasons = []
            double tick_dev, n_puts, n_calls, n_atm

        latest_curve, rs_otm, rs_map = self.build_curve(current_spot)
        self.updated = False

        if not self.vega or len(latest_curve) - len(self.vega) >= 2:
            self.reason = 'updated. len(latest_curve)=%d, len(self.vega)=%d' % (len(latest_curve), len(self.vega))
            self.commit_new_curve(latest_curve, rs_otm, rs_map, current_spot)
        else:
            for key, vega in self.vega.iteritems():
                if check_using_skew and key not in latest_curve:
                    continue

                if double_eq(self.spot, current_spot) or not use_volpath:
                    dvp = 0
                else:
                    #dvp = volpaths.vp_for_moneyness(self.moneyness[key], self.spot, current_spot, self.vp)
                    dvp = self.volpath[key] * (current_spot - self.spot)

                #band = self.band / vega

                if check_using_skew:
                    vol = latest_curve[key]
                else:
                    vol = rs_map[key].vol
                sm_vol = self.smoothed_vol[key]
                dev = (vol - (sm_vol + dvp))
                tick_dev = vega * dev

                # does the latest curve vol deviate from the upper bound of the bandwidth?
                if double_gt(tick_dev, self.band) or (force_update and double_gt(tick_dev, 0)):
                    if key in self.call_keys:
                        hi_calls[key] = (vol, sm_vol, dvp, vega, tick_dev)
                    elif key in self.put_keys:
                        hi_puts[key] = (vol, sm_vol, dvp, vega, tick_dev)
                    elif key in self.atm_keys:
                        hi_atm[key] = (vol, sm_vol, dvp, vega, tick_dev)
                # how about the lower bound?
                elif double_lt(tick_dev, -1*self.band) or (force_update and double_lt(tick_dev, 0)):
                    if key in self.call_keys:
                        lo_calls[key] = (vol, sm_vol, dvp, vega, tick_dev)
                    elif key in self.put_keys:
                        lo_puts[key] = (vol, sm_vol, dvp, vega, tick_dev)
                    elif key in self.atm_keys:
                        lo_atm[key] = (vol, sm_vol, dvp, vega, tick_dev)

                self.smoothed_vol_vp[key] = self.smoothed_vol[key] + dvp

            n_puts = len(self.put_keys) or 1
            n_calls = len(self.call_keys) or 1
            n_atm = len(self.atm_keys) or 1

            hi_puts_pct = len(hi_puts) / n_puts
            hi_calls_pct = len(hi_calls) / n_calls
            hi_atm_pct = len(hi_atm) / n_atm

            lo_puts_pct = len(lo_puts) / n_puts
            lo_calls_pct = len(lo_calls) / n_calls
            lo_atm_pct = len(lo_atm) / n_atm

            # update self.reason for debugging (lists strikes, pct and # of sections)
            raise_calls = double_gte(hi_calls_pct, self.band_pct)
            if raise_calls:
                update_data.append(('hi_calls', hi_calls, n_calls))

            lower_calls = double_gte(lo_calls_pct, self.band_pct)
            if lower_calls:
                update_data.append(('lo_calls', lo_calls, n_calls))

            raise_puts = double_gte(hi_puts_pct, self.band_pct)
            if raise_puts:
                update_data.append(('hi_puts', hi_puts, n_puts))

            lower_puts = double_gte(lo_puts_pct, self.band_pct)
            if lower_puts:
                update_data.append(('lo_puts', lo_puts, n_puts))

            raise_atm = double_gte(hi_atm_pct, self.band_pct)
            if raise_atm:
                update_data.append(('hi_atm', hi_atm, n_atm))

            lower_atm = double_gte(lo_atm_pct, self.band_pct)
            if lower_atm:
                update_data.append(('lo_atm', lo_atm, n_atm))

            if raise_calls or lower_calls or raise_puts or lower_puts or raise_atm or lower_atm:
                self.violation_count += 1
                if self.violation_count >= self.cycles_until_update:
                    self.commit_new_curve(latest_curve, rs_otm, rs_map, current_spot)
                    self.reset_violation_count()
                    self.reason = 'updated. see update_data for more info'
                else:
                    self.reason = 'tried to update ( %d / %d )' % (self.violation_count, self.cycles_until_update)
            else:
                if self.violation_count > 0:
                    self.reason = 'resetting violation_count %d back to zero' % self.violation_count
                    self.reset_violation_count()
                else:
                    self.reason = ''
            self.update_data = update_data

    cdef tuple build_rs_maps(self, double spot):
        cdef ndarray rs_otm = where(self.strikes<spot, self.rs_puts, self.rs_calls)
        return rs_otm, {self.symbols_map[k]: rs for k, rs in zip(self.strikes, rs_otm)}

    cdef tuple build_curve(self, double spot):
        cdef:
            long i
            ndarray input_mask, output_mask, delta_mask, moneynesses, vols, weights, rs_otm
            bint use_input, use_volpath = (len(self.vp)>0)
            double moneyness, corr_delta, delta_adj, v, abs_delta, degree = self.degree
            long good_count = 0
            dict rs_map

        # split all possible syms into puts and calls based on spot, sort them and find relevant syms and floats
        delta_mask = self.tmp_delta_mask
        input_mask = self.tmp_input_mask
        output_mask = self.tmp_output_mask
        vols = self.tmp_vols
        weights = self.tmp_weights

        delta_mask[:] = False
        input_mask[:] = False
        output_mask[:] = False

        vols[:] = 0
        weights[:] = 1

        if not double_eq(spot, self.spot):
            rs_otm, rs_map = self.build_rs_maps(spot)
        else:
            rs_otm = self.rs_otm
            rs_map = self.rs_map

        for i, rs in enumerate(rs_otm):
            if rs is None:
                continue

            v = rs.vol
            use_input = double_gt(v, 0)

            if use_input:
                moneyness = rs.calc_moneyness(spot)
                if use_volpath:
                    delta_adj = volpaths.vp_for_moneyness(moneyness, spot, spot+1, self.vp) * rs.calc_vega(spot, v)
                else:
                    delta_adj = 0
                corr_delta = rs.calc_delta(spot, v) + delta_adj
                delta_mask[i] = (rs.cp > 0 and double_gte(corr_delta, self.call_delta)) or (rs.cp < 0 and double_lte(corr_delta, self.put_delta))

                vols[i] = v

                abs_delta = fabs(corr_delta)

                # figure out weights
                if self.weight_type == DeltaWeight:
                    if double_lt(abs_delta, 0.2):
                        weights[i] = abs_delta / 0.2
                    else:
                        weights[i] = 0.2 / abs_delta
                elif self.weight_type == MarketWidthPutWeight:
                    if rs.cp < 0:
                        weights[i] = rs.weight
                    elif double_lt(abs_delta, 0.2):
                        weights[i] = abs_delta / 0.2
                    else:
                        weights[i] = 0.2 / abs_delta
                elif self.weight_type == RatioWeight:
                    weights[i] = rs.weight
                good_count += delta_mask[i]

            input_mask[i] = use_input
            output_mask[i] = True

        input_mask &= delta_mask
        output_mask &= delta_mask

        if good_count <= (degree + 1):
            #raise ValueError('not enough vol points', good_count)
            degree = good_count - 1

        ctrl_points = bezier.find_control_pts(self.strikes[input_mask], vols[input_mask], degree, weights[input_mask])
        curve = bezier.bezier_curve(ctrl_points, self.strikes[output_mask])

        for v in curve[1]:
            if double_lte(v, 0):
                raise ValueError('smoothed vols <= 0', curve, self.strikes[input_mask], self.strikes[output_mask], vols[input_mask], weights[input_mask], ctrl_points, degree, good_count)

        return {self.symbols_map[k]: v for k, v in zip(self.strikes[output_mask], curve[1])}, rs_otm, rs_map
