# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from libc.stdint cimport int64_t, uint64_t, int32_t, uint16_t
from libc.math cimport NAN, INFINITY, llround, log as log_, fmin as fmin_, fmax as fmax_, sqrt as sqrt_
from libcpp.unordered_map cimport unordered_map as unordered_map_
from numpy cimport ndarray
from glt.math cimport bs
from glt.md.standard cimport MovingAverageWithEntropy, MAWEManagerINT64

from numpy import zeros, float64, int64
from pandas import DataFrame, to_datetime


cdef extern from "<cmath>" namespace "std": 
    bint isnan(double x) nogil 


cdef struct OptionContractPrices:
    double strike, call, put, cvol, pvol, cdelta, pdelta


cdef double calc_wmid(double bidprc, double askprc, int64_t bidqty, int64_t askqty) nogil:
    cdef int64_t total_qty = bidqty + askqty
    if total_qty == 0:
        return NAN
    return (bidprc * <double>askqty + askprc * <double>bidqty) / <double>total_qty


cdef double calc_mid(double bidprc, double askprc) nogil:
    return 0.5 * (bidprc + askprc)


def option_calc(md, bint sweep_reset=False, double spread_min=0.001, double spread_max=2.499, ir=0.0):
    cdef:
        int64_t[:] skeys = md['skey'].values
        double[:] bidprcs = md['bidprc0'].values
        double[:] askprcs = md['askprc0'].values
        int64_t[:] bidqtys = md['bidqty0'].values
        int64_t[:] askqtys = md['askqty0'].values
        int64_t[:] bidpars = md['bidpar0'].values
        int64_t[:] askpars = md['askpar0'].values
        int64_t[:] msgs = md['msg_int'].values
        # options related stuff
        int64_t[:] ops = md['optiontype'].values
        double[:] strikes = md['strike'].values
        double[:] ytes = md['yte'].values
        # iter
        uint64_t i, n = len(md)
        double wmid, spread, spread_delta
        # yeup
        double futprc = NAN, strike_diff
        int64_t skey, adj_skey
        OptionContractPrices *option
        OptionContractPrices *adj_option
        unordered_map_[int64_t, OptionContractPrices] mids
        list my_dtype
        ndarray output
    
    my_dtype = [
        ('futprc', float64),
        ('call', float64),
        ('put', float64),
        ('option', float64),
        ('adj_option', float64),
        ('vol', float64),
        ('adj_vol', float64),
        ('spread', float64),
        ('spread_delta', float64),
        ('strike_diff', float64),
        ('moneyness', float64),
        ('delta', float64),
        ('adj_delta', float64)
    ]
    output = zeros(n, dtype=my_dtype)
    for col, dtype in my_dtype:
        if dtype == float64:
            output[col][:] = NAN
    
    for 0 <= i < n:
        # ignore sweeps and super thin markets
        if sweep_reset and (msgs[i] == 3 or bidpars[i] < 2 or askpars[i] < 2):
            if ops[i] == 0:
                futprc = NAN
            continue
        if ops[i] != 0 and isnan(futprc):
            # clear the known options prices?
            mids.clear()
            continue
        if askprcs[i] - bidprcs[i] < 0.015:
            wmid = calc_wmid(bidprcs[i], askprcs[i], bidqtys[i], askqtys[i])
        else:
            wmid = calc_mid(bidprcs[i], askprcs[i])
        if ops[i] == 0:
            futprc = wmid
        skey = skeys[i]
        if mids.find(skey) == mids.end():
            mids[skey].strike = strikes[i]
            mids[skey].call = NAN
            mids[skey].put = NAN
            mids[skey].cvol = NAN
            mids[skey].pvol = NAN
            mids[skey].cdelta = NAN
            mids[skey].pdelta = NAN
        option = &(mids[skey])
        if ops[i] > 0:
            option.call = wmid
            option.cvol = bs.ivol(option.strike, futprc, ytes[i], ir, 1.0, option.call, 1.0, 0.001, 100)
            option.cdelta = bs.delta(option.strike, futprc, option.cvol, ytes[i], ir, 1.0)
            adj_skey = skey + 250
            strike_diff = strikes[i] - futprc
            output[i]['call'] = option.call
        elif ops[i] < 0:
            option.put = wmid
            option.pvol = bs.ivol(option.strike, futprc, ytes[i], ir, -1.0, option.put, 1.0, 0.001, 100)
            option.pdelta = bs.delta(option.strike, futprc, option.pvol, ytes[i], ir, -1.0)
            adj_skey = skey - 250
            strike_diff = strikes[i] - futprc
            output[i]['put'] = option.put
        else:
            strike_diff = NAN
            adj_skey = -1
        if mids.find(adj_skey) == mids.end():
            continue
        adj_option = &(mids[adj_skey])
        # compute spreads
        if ops[i] > 0 and strike_diff > 0 and not isnan(adj_option.call):
            # use OTM call
            output[i]['option'] = option.call
            output[i]['adj_option'] = adj_option.call
            output[i]['vol'] = option.cvol
            output[i]['adj_vol'] = adj_option.cvol
            output[i]['delta'] = option.cdelta
            output[i]['adj_delta'] = adj_option.cdelta
            # spread
            spread = fmax_(spread_min, option.call - adj_option.call)
            spread_delta = fmax_(0, option.cdelta - adj_option.cdelta)
        elif ops[i] < 0 and strike_diff < 0 and not isnan(adj_option.put):
            # use OTM put
            output[i]['option'] = option.put
            output[i]['adj_option'] = adj_option.put
            output[i]['vol'] = option.pvol
            output[i]['adj_vol'] = adj_option.pvol
            output[i]['delta'] = option.pdelta
            output[i]['adj_delta'] = adj_option.pdelta
            # spread
            spread = fmax_(spread_min, option.put - adj_option.put)
            spread_delta = fmin_(0, option.pdelta - adj_option.pdelta)
        else:
            spread = NAN
            spread_delta = NAN
        # save values
        output[i]['futprc'] = futprc
        output[i]['strike_diff'] = strike_diff
        output[i]['moneyness'] = log_(strikes[i] / futprc) / sqrt_(ytes[i])
        output[i]['spread'] = spread
        output[i]['spread_delta'] = spread_delta
    return output


cdef void _reverse_expanding_range(double[:] values, uint64_t n, double[:] output) nogil:
    cdef:
        double minval = NAN, maxval = NAN
        uint64_t i, j
        
    for 0 <= j < n:
        i = n-1-j
        if isnan(minval):
            minval = values[i]
            maxval = values[i]
        elif values[i] < minval:
            minval = values[i]
        elif values[i] > maxval:
            maxval = values[i]
        output[i] = maxval - minval


def reverse_expanding_range(ndarray values):
    cdef:
        uint64_t n = len(values)
        ndarray output = zeros(n, dtype=float64)
    _reverse_expanding_range(values, n, output)
    return output


def max_value_by_key(ndarray keys, ndarray values):
    cdef:
        set unique_keys = set(keys)
        uint64_t i, j, n = len(values)
        dict max_map = {}
        
    for 0 <= j < n:
        i = n-1-j
        if keys[i] in unique_keys:
            max_map[keys[i]] = values[i]
            unique_keys.remove(keys[i])
        if not unique_keys:
            break
            
    return max_map
