from numpy cimport ndarray
from libc.math cimport isnan
from glt.math.helpful_functions cimport *
from glt.modeling.structs cimport MarketState, WrappedMarketState, initialized_market_states
from glt.tools cimport SpotManager, SuperSpotManager

from numpy import int64
from pandas import to_timedelta

def create_symbol_dict(list symbols):
    return {k: [] for k in symbols}

"""
def mock_vol_publisher_front_month(df, start_time, double roll, long futures_secid, 
                                   double futures_tick_width, long futures_min_size, long pause_window,
                                   long minqty, long minpar): 
    cdef:
        long n = df.shape[0], i
        
        MarketState[:] mkts = initialized_market_states(df)
        
        long[:] ts = df['time'].values.astype(int64)
        long[:] secids = df['secid'].values
        
        # paramters that should probably be passed to the function
        long start_offset = 0, count = 1, st
        
        SuperSpotManager spot = SuperSpotManager(futures_tick_width, futures_min_size, pause_window)
        WrappedMarketState w = WrappedMarketState()

    st = start_time.value
    
    # figure out what to do at each new piece of market data
    for 0 <= i < n:
        w.mkt = &(mkts[i])
        if secids[i] == futures_secid:
            spot._update_marketstate(ts[i], seqnum[i], w.mkt)
            yield secids[i], ts[i], spot.prc
        else:
            spot._update_usability(ts[i])
            if not isnan(spot.prc):
                rs = model.raw_option_dict[sym[i]]
                rs.calc_raw_vol(w, spot.prc + roll, ts[i], minpar, minqty, 'o', 'f', '')
                yield secids[i], ts[i], rs.vol
"""         
            

def evaluate_bezier_model(model, df, start_time, model_interval, 
        double roll, long minQty, long minPar, str futContract, 
        double futWidth, long minFutDepth, fut_pause_window,
        double kalman_q=0, bint check_using_skew=False, use_reset_violation_count=False):
    """Back-testing function that replicates BezierModel fitting on an options market.

Parameters
----------
model : BezierModel
    Model being evaluated
df : DataFrame
    Input time series of options and futures data.
start_time : datetime-like object
    Time to start simulating model fitting
model_interval : string or int
    Interval of time used to determine when a model update occurs.
    If string, specify units (e.g. '1s' for one second, '100ms' for 
    100 millisecond); if int, specify in nanoseconds.
roll : float
    Fixed roll to adjust futures to actual underlying price
min_qty : int
    RawStrike parameter; used to determine whether a market is good,
    where the qty in the market must be >= min_qty
min_par : int
    RawStrike parameter; used to determine whether a market is good,
    where the number of participants in the market must be >= min_par
fut_contract : string
    Symbol of the market's future contract
fut_width : double
    The minimum futures market width to determine whether that market
    is good.
fut_depth : int
    The minimum quantity on either side of the futures market to determine
    whether that market is good.
pause_window : string or int
    Window of time used to pause updating after when a futures
    market is deemed good. Only after determining the market is good will
    we wait for the amount of time set by pause_window.    
    If string, specify units (e.g. '1s' for one second, '100ms' for 
    100 millisecond); if int, specify in nanoseconds.
kalman_q : float
    RawStrike parameter; q parameter of univariate kalman filter to determine
    smoothing of thin markets.
check_using_skew : bool
    Model parameter determining whether bezier band violations are checked versus
    newly fitted skew (True) or raw vols (False)
    
Returns
-------
raw_vols : dict
acc_bids : dict
acc_asks : dict
best_bid : dict
best_ask : dict
seq_nums : dict
reason : dict
sm_vols : dict
sm_vp : dict
weight : dict


    """

    cdef:
        long n = df.shape[0], i
        
        MarketState[:] mkts = initialized_market_states(df)
        
        long[:] ts = df['time'].values.astype(int64)
        long[:] seqnum = df['seqnum'].values
        
        ndarray[str, ndim=1] sym = df['symbol'].values
        
        list uniq_symbols = model.raw_option_dict.keys()
        
        # paramters that should probably be passed to the function
        long count = 1, st = start_time.value
        
        # return values
        dict raw_vols = create_symbol_dict(uniq_symbols)
        list sm_vols = [], sm_vp = []
        
        # return values for testing... not needed
        dict acc_bids = create_symbol_dict(uniq_symbols)
        dict acc_asks = create_symbol_dict(uniq_symbols)
        dict best_bid = create_symbol_dict(uniq_symbols)
        dict best_ask = create_symbol_dict(uniq_symbols)
        dict seq_nums = create_symbol_dict(uniq_symbols)
        dict reason = { 'reason' : [], 'futures_seqnum': [] }
        
        WrappedMarketState w = WrappedMarketState()
        
        SuperSpotManager spot_manager
        
        str key
        
        # return values
        long cycle, pauseWindow, last_good_time = 0
        
        bint has_check_skew_method, override_kalman_q, should_check_bands, has_violation_count
    
    # hack to check if BezierModel has check_skew function; newer versions do not
    has_check_skew_method = hasattr(model, 'check_skews')
    has_violation_count = hasattr(model, 'violation_count')
        
    override_kalman_q = double_gt(kalman_q, 0)
    
    # check types for model_interval and fut_pause_window
    if type(model_interval) in (str, unicode):
        cycle = to_timedelta(model_interval).value
    else:
        cycle = model_interval
        
    if type(fut_pause_window) in (str, unicode):
        pauseWindow = to_timedelta(fut_pause_window).value
    else:
        pauseWindow = fut_pause_window
        
    spot_manager = SuperSpotManager(futWidth, minFutDepth, pauseWindow)
    
    print '------------------------------------------------------------------------'
    print 'bezier model:                             ', model.__class__
    print '    check_skews method exists:            ', has_check_skew_method
    print '    violation_count field exists:         ', has_violation_count
    print '    use reset_violation_count:            ', use_reset_violation_count
    print '    update curve after this many cycles   ', model.cycles_until_update if hasattr(model, 'cycles_until_update') else 0
    print '    do we want to check bands using skew? ', check_using_skew
    print '    kalman q parameter:                   ', kalman_q
    print '    override kalman q?                    ', override_kalman_q
    print 'futures parameters:'
    print '    futures minimum qty:                  ', minFutDepth
    print '    futures minimum width:                ', futWidth
    print '    roll:                                 ', roll
    print '    pause window (in nanoseconds):        ', pauseWindow
    print 'simulation parameters:'
    print '    start time:                           ', start_time
    print '    cycle (in nanoseconds):               ', cycle
    print '-----------------------------------------------------------------------'
    
    raw_vols['ts'] = []
    
    if override_kalman_q:
        for key in uniq_symbols:
            model.raw_option_dict[key].q = kalman_q
    
    # figure out what to do at each new piece of market data
    for i in range(n):
        w.mkt = &(mkts[i])
        if sym[i] == futContract:
            # if the market moved, it's not good anymore; we need to initialize a new time
            # after this point for a new pause_window evaluation
            spot_manager._update_marketstate(ts[i], seqnum[i], w.mkt)
            if use_reset_violation_count and isnan(spot_manager.prc) and has_violation_count and model.violation_count > 0:
                model.reset_violation_count()
        else:
            spot_manager._update_usability(ts[i])
            if not isnan(spot_manager.prc):
            # spot is "good" so let's update our vol for whichever option we're at in the time series
                try:
                    model.raw_option_dict[sym[i]].calc_raw_vol(w, spot_manager.prc + roll, ts[i], minPar, minQty, '%s' % spot_manager.seqnum, '%s' % seqnum[i], 'FUCK YOU')
                except ValueError as inst:
                    print 'ValueError...', i, ts[i], sym[i], 'seqnum', seqnum[i], spot_manager.as_dict(), inst
                    break
            
        # snapshot of vols at each cycle
        if ts[i] > st + cycle * count:
            should_check_bands = not isnan(spot_manager.prc)

            # check bands
            if should_check_bands:
                if not has_check_skew_method:
                    model.check_bands(spot_manager.prc, check_using_skew=check_using_skew)
                elif check_using_skew:
                    model.check_skews(spot_manager.prc)
                else:
                    model.check_bands(spot_manager.prc)
            
            raw_vols['ts'].append(st + cycle * count)
            
            sm_vols.append(model.smoothed_vol.copy())
            sm_vp.append(model.smoothed_vol_vp.copy())
            
            for key in uniq_symbols:
                # snapshot of smoothed vols, volpath vols, weights, etc
                raw_vols[key].append(model.raw_option_dict[key].vol)
                
                if not model.raw_option_dict[key].prev_bid_vols:
                    acc_bids[key].append(0)
                elif len(model.raw_option_dict[key].prev_bid_vols) > 1:
                    acc_bids[key].append(model.raw_option_dict[key].prev_bid_vols[1])
                else:
                    acc_bids[key].append(model.raw_option_dict[key].prev_bid_vols[0])
                
                if not model.raw_option_dict[key].prev_ask_vols:
                    acc_asks[key].append(0)
                elif len(model.raw_option_dict[key].prev_ask_vols) > 1:
                    acc_asks[key].append(model.raw_option_dict[key].prev_ask_vols[1])
                else:
                    acc_asks[key].append(model.raw_option_dict[key].prev_ask_vols[0])
                    
                best_bid[key].append(model.raw_option_dict[key].bb_vol)
                best_ask[key].append(model.raw_option_dict[key].bo_vol)
                seq_nums[key].append(model.raw_option_dict[key].seq_num)
            
            if should_check_bands:
                reason['reason'].append(model.reason)
            else:
                reason['reason'].append("did not check bands: spot['good'] == %s, %0.1f / %0.1f ( %d x %d ); ts[i]: %d, time_diff: %d, after_pause_window: %d" % (
                        spot_manager.good, spot_manager.bid, spot_manager.ask, 
                        spot_manager.bidqty, spot_manager.askqty, ts[i], ts[i]-spot_manager.last_good_ts, 
                        ts[i]-spot_manager.last_good_ts-pauseWindow)
                                       )
            reason['futures_seqnum'].append(spot_manager.seqnum)
            
            count += 1

    return raw_vols, acc_bids, acc_asks, best_bid, best_ask, seq_nums, reason, sm_vols, sm_vp


def evaluate_bezier_model_old(model, df, start_time, model_interval, double roll, long minQty, long minPar, str futContract, double futWidth, long minFutDepth, fut_pause_window, 
                          double kalman_q=0, bint check_using_skew=False):
    """Back-testing function that replicates BezierModel fitting on an options market.

Parameters
----------
model : BezierModel
    Model being evaluated
df : DataFrame
    Input time series of options and futures data.
start_time : datetime-like object
    Time to start simulating model fitting
model_interval : string or int
    Interval of time used to determine when a model update occurs.
    If string, specify units (e.g. '1s' for one second, '100ms' for 
    100 millisecond); if int, specify in nanoseconds.
roll : float
    Fixed roll to adjust futures to actual underlying price
min_qty : int
    RawStrike parameter; used to determine whether a market is good,
    where the qty in the market must be >= min_qty
min_par : int
    RawStrike parameter; used to determine whether a market is good,
    where the number of participants in the market must be >= min_par
fut_contract : string
    Symbol of the market's future contract
fut_width : double
    The minimum futures market width to determine whether that market
    is good.
fut_depth : int
    The minimum quantity on either side of the futures market to determine
    whether that market is good.
pause_window : string or int
    Window of time used to pause updating after when a futures
    market is deemed good. Only after determining the market is good will
    we wait for the amount of time set by pause_window.    
    If string, specify units (e.g. '1s' for one second, '100ms' for 
    100 millisecond); if int, specify in nanoseconds.
kalman_q : float
    RawStrike parameter; q parameter of univariate kalman filter to determine
    smoothing of thin markets.
check_using_skew : bool
    Model parameter determining whether bezier band violations are checked versus
    newly fitted skew (True) or raw vols (False)
    
Returns
-------
raw_vols : dict
acc_bids : dict
acc_asks : dict
best_bid : dict
best_ask : dict
seq_nums : dict
reason : dict
sm_vols : dict
sm_vp : dict
weight : dict


    """

    cdef:
        long n = df.shape[0], i
        
        MarketState[:] mkts = initialized_market_states(df)
        
        long[:] ts = df['time'].values.astype(int64)
        long[:] seqnum = df['seqnum'].values
        
        ndarray[str, ndim=1] sym = df['symbol'].values
        
        list timestrs = map(str, df['time'])
        
        list uniq_symbols = model.raw_option_dict.keys()
        
        # paramters that should probably be passed to the function
        long start_offset = 0, count = 1, st
        #double minOptWidth = 150000.75, roll = -33
        
        # return values
        dict raw_vols = create_symbol_dict(uniq_symbols)
        dict sm_vols = create_symbol_dict(uniq_symbols)
        dict sm_vp = create_symbol_dict(uniq_symbols)
        dict weight = create_symbol_dict(uniq_symbols)
        
        # return values for testing... not needed
        dict acc_bids = create_symbol_dict(uniq_symbols)
        dict acc_asks = create_symbol_dict(uniq_symbols)
        dict best_bid = create_symbol_dict(uniq_symbols)
        dict best_ask = create_symbol_dict(uniq_symbols)
        dict seq_nums = create_symbol_dict(uniq_symbols)
        dict reason = { 'reason' : [], 'futures_goodness': [], 'futures_seqnum': [] } #, 'min_strike': [], 'max_strike': [] }
        
        
        WrappedMarketState w = WrappedMarketState()
        
        str key
        
        # return values
        dict spot = {
            'bid': 0, 
            'ask': 0, 
            'bidqty': 0,
            'askqty': 0,
            'mid': 0, 
            'wide': False, 
            'ts': 0, 
            'seqnum': 0, 
            'good': False 
        }
        
        long goodness, cycle, pauseWindow, last_good_time = 0
        
        bint has_check_skew_method, override_kalman_q, should_check_bands, has_violation_count

    st = start_time.value
    
    # hack to check if BezierModel has check_skew function; newer versions do not
    has_check_skew_method = hasattr(model, 'check_skews')
    has_violation_count = hasattr(model, 'violation_count')
        
    override_kalman_q = double_gt(kalman_q, 0)
    
    # check types for model_interval and fut_pause_window
    if type(model_interval) in (str, unicode):
        cycle = to_timedelta(model_interval).value
    else:
        cycle = model_interval
        
    if type(fut_pause_window) in (str, unicode):
        pauseWindow = to_timedelta(fut_pause_window).value
    else:
        pauseWindow = fut_pause_window
    
    print '------------------------------------------------------------------------'
    print 'bezier model:                             ', model.__class__
    print '    check_skews method exists:            ', has_check_skew_method
    print '    violation_count field exists:         ', has_violation_count
    print '    update curve after this many cycles   ', model.cycles_until_update if hasattr(model, 'cycles_until_update') else 0
    print '    do we want to check bands using skew? ', check_using_skew
    print '    kalman q parameter:                   ', kalman_q
    print '    override kalman q?                    ', override_kalman_q
    print 'futures parameters:'
    print '    futures minimum qty:                  ', minFutDepth
    print '    futures minimum width:                ', futWidth
    print '    roll:                                 ', roll
    print '    pause window (in nanoseconds):        ', pauseWindow
    print 'simulation parameters:'
    print '    start time:                           ', start_time
    print '    cycle (in nanoseconds):               ', cycle
    print '-----------------------------------------------------------------------'
    
    raw_vols['ts'] = []
    
    if override_kalman_q:
        for key in uniq_symbols:
            model.raw_option_dict[key].q = kalman_q
    
    # figure out what to do at each new piece of market data
    for i in range(n):
        if sym[i] == futContract:
            # if the market moved, it's not good anymore; we need to initialize a new time
            # after this point for a new pause_window evaluation
            if not double_eq(mkts[i].bidprc0, spot['bid']) or not double_eq(mkts[i].askprc0, spot['ask']):
                spot['good'] = False
                if has_violation_count and model.violation_count > 0:
                    model.reset_violation_count()

            # fill in spot with new data
            spot['seqnum'] = seqnum[i]
            spot['bid'] = mkts[i].bidprc0
            spot['ask'] = mkts[i].askprc0
            spot['bidqty'] = mkts[i].bidqty0
            spot['askqty'] = mkts[i].askqty0
            spot['mid'] = 0.5 * (spot['bid'] + spot['ask'])
            
            # evaluate whether the futures market is good
            if spot['bidqty'] >= minFutDepth and spot['askqty'] >= minFutDepth and double_lte(spot['ask'] - spot['bid'], futWidth):
                spot['wide'] = False
                spot['ts'] = ts[i]
                if not spot['good']:
                    last_good_time = spot['ts']
                spot['good'] = True
                goodness = 1
            elif double_gt(spot['ask'] - spot['bid'], futWidth):
                spot['wide'] = True
                spot['good'] = False
                goodness = -1
            else:
                spot['wide'] = False
                spot['good'] = False
                goodness = 0
                
        elif spot['good'] and ts[i] - last_good_time > pauseWindow:
            # spot is "good" so let's update our vol for whichever option we're at in the time series
            w.mkt = &(mkts[i])
            try:
                model.raw_option_dict[sym[i]].calc_raw_vol(w, spot['mid'] + roll, ts[i], minPar, minQty, '%s' % spot['seqnum'], '%s' % seqnum[i], timestrs[i])
            except ValueError as inst:
                print 'ValueError...', i, ts[i], sym[i], 'seqnum', seqnum[i], spot, inst
                break
                
            #jff  TEMP!!
            #if ts[i] > pd.Timestamp('2016-02-01 12:01').value and ts[i] < pd.Timestamp('2016-02-01 12:02').value:
            #    print pd.Timestamp(ts[i])
            #    print '   fut: %s  %s' % (spot['bid'], spot['ask'])
            #    print '   good_bid: %s   top_bid: %s   top_ask: %s    good_ask: %s' % (model.raw_option_dict['KOG6 P22000'].bid_px, 
            #                    model.raw_option_dict['KOG6 P22000'].top_bid, model.raw_option_dict['KOG6 P22000'].top_ask, model.raw_option_dict['KOG6 P22000'].ask_px)
            #    print '   vol: %s' % model.raw_option_dict['KOG6 P22000'].vol
            #    print '   bbv: %s  bov: %s' % (model.raw_option_dict['KOG6 P22000'].bb_vol, model.raw_option_dict['KOG6 P22000'].bo_vol)
            #    print '   cbv: %s  cov: %s' % (model.raw_option_dict['KOG6 P22000'].curr_bid_vol, model.raw_option_dict['KOG6 P22000'].curr_ask_vol)
            #    print '   prev_bids: %s' % model.raw_option_dict['KOG6 P22000'].prev_bid_vols
            #    print '   prev_asks: %s' % model.raw_option_dict['KOG6 P22000'].prev_ask_vols
            
        # snapshot of vols at each cycle
        if ts[i] > st + start_offset + cycle * count:
            should_check_bands = spot['good'] and ts[i] - last_good_time > pauseWindow

            # check bands
            if should_check_bands:
                if not has_check_skew_method:
                    model.check_bands(spot['mid'], check_using_skew=check_using_skew)
                elif check_using_skew:
                    model.check_skews(spot['mid'])
                else:
                    model.check_bands(spot['mid'])
            
            raw_vols['ts'].append(st + start_offset + cycle * count)
            for key in uniq_symbols:
                # snapshot of smoothed vols, volpath vols, weights, etc
                if key in model.smoothed_vol.keys():
                    sm_vols[key].append(model.smoothed_vol[key])
                    sm_vp[key].append(model.smoothed_vol_vp[key])
                    weight[key].append(model.raw_option_dict[key].weight)
                else:
                    sm_vols[key].append(0)
                    sm_vp[key].append(0)
                    weight[key].append(0)
                raw_vols[key].append(model.raw_option_dict[key].vol)
                
                if not model.raw_option_dict[key].prev_bid_vols:
                    acc_bids[key].append(0)
                elif len(model.raw_option_dict[key].prev_bid_vols) > 1:
                    acc_bids[key].append(model.raw_option_dict[key].prev_bid_vols[1])
                else:
                    acc_bids[key].append(model.raw_option_dict[key].prev_bid_vols[0])
                
                if not model.raw_option_dict[key].prev_ask_vols:
                    acc_asks[key].append(0)
                elif len(model.raw_option_dict[key].prev_ask_vols) > 1:
                    acc_asks[key].append(model.raw_option_dict[key].prev_ask_vols[1])
                else:
                    acc_asks[key].append(model.raw_option_dict[key].prev_ask_vols[0])
                    
                best_bid[key].append(model.raw_option_dict[key].bb_vol)
                best_ask[key].append(model.raw_option_dict[key].bo_vol)
                seq_nums[key].append(model.raw_option_dict[key].seq_num)
            
            if should_check_bands:
                reason['reason'].append(model.reason)
            else:
                reason['reason'].append("did not check bands: spot['good'] == %s, %0.1f / %0.1f ( %d x %d ); ts[i]: %d, time_diff: %d, after_pause_window: %d" % (spot['good'], spot['bid'], spot['ask'], spot['bidqty'], spot['askqty'], ts[i], ts[i]-last_good_time, ts[i]-last_good_time-pauseWindow))
            reason['futures_goodness'].append(goodness)
            reason['futures_seqnum'].append(spot['seqnum'])
                
            count += 1

    return raw_vols, acc_bids, acc_asks, best_bid, best_ask, seq_nums, reason, sm_vols, sm_vp, weight
