cimport cython
from libc.math cimport exp as fexp
from numpy cimport ndarray

from numpy import array, arange
from glt.math import bezier

@cython.boundscheck(False)
cdef double vp_for_moneyness(double m, double start_spot, double curr_spot, double[:, :] vp_curve) nogil:
    cdef:
        long i = 0, j = 0, prev_val = 0, next_val = 0
        double chg = curr_spot - start_spot, vp

    if m <= vp_curve[0, 0]:
        vp = vp_curve[1, 0] * chg
    elif m >= vp_curve[0, -1]:
        vp = vp_curve[1, -1] * chg
    else:
        while vp_curve[0, j] <= m:
            j += 1
        prev_val = j - 1
        next_val = j
        vp = ((m - vp_curve[0, prev_val]) / (vp_curve[0, next_val] - vp_curve[0, prev_val]) * (vp_curve[1, next_val] - vp_curve[1, prev_val]) + vp_curve[1, prev_val]) * chg

    return vp

# spx
cdef double pt0func(double dte) nogil:
    return .0012 * fexp(-.22 * dte ) + -.00015

cdef double pt1func(double dte) nogil:
    return .0025 * fexp(-.4 * dte ) + -.00015

cdef double pt2func(double dte) nogil:
    return -.0011 * fexp(-.045 * dte ) + -.00017

cdef double pt3func(double dte) nogil:
    return .0006 * fexp(-.05 * dte) + .00008

cdef double pt4func(double dte) nogil:
    return -.0008 * fexp(-.1 * dte) + -.0003


# ko
cdef double pt0funcKO(double dte):
    return .009 * fexp(-.22 * dte)

cdef double pt1funcKO(double dte):
    return 0

cdef double pt2funcKO(double dte):
    return .0035 * fexp(-.22 * dte) + .0005

cdef double pt3funcKO(double dte):
    return -.005 * fexp(-.125 * dte) + -.00075


cdef double[:, :] createVP(double d, str optMarket='ES'):
    cdef double[:, :] vol_path
    """def createVP(d, optMarket='ES'):
d is dte returns volpath curve"""
    if optMarket == 'ES':
        pt0 = (-.4, pt0func(d))
        pt1 = (-.26, pt1func(d))
        pt2 = (-.125, pt2func(d))
        pt3 = (.01, pt3func(d))
        pt4 = (.15, pt4func(d))
    
        vol_path = bezier.bezier_curve(array([pt0, pt1, pt2, pt3, pt4]), arange(-.45, .15, .01))
    elif optMarket == 'KO':
        #pt0 = (-.35, pt0funcKRX(d))
        #pt1 = (-.183, pt1funcKRX(d))
        #pt2 = (-.0125, pt2funcKRX(d))
        #pt3 = (.15, pt3funcKRX(d))
        pt0 = (-.35, 0.)
        pt1 = (-.183, 0.)
        pt2 = (-.0125, 0.)
        pt3 = (.15, 0.)

        vol_path = bezier.bezier_curve(array([pt0, pt1, pt2, pt3]), arange(-.45, .15, .01))

    return vol_path
