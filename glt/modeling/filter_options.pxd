from libc.stdint cimport *
from libc.math cimport NAN, ceil, floor
from glt.modeling.filter_base cimport FilterModel, MDFilter
from glt.md.standard cimport MDOrderBook
from glt.math.kalman cimport UnivariateNode

cdef class OptionMDFilter(MDFilter):
    cdef:
        readonly:
            double spot, strike, ir, m, delta, vega, spot_tick_incr, tte, tick_incr, big_tick_incr, big_tick_px, min_bid
            int cp, MAX_PREV_VOLS_LEN
            dict vol, prev_vols, top_vol
            bint otm
        #void update_parameters(self, int32_t min_qty, int32_t min_par, double mkt_width, double pause_window, double ir, double min_bid)
        void read_dump(self, double vol, double spot)
        void calculate_vol(self, MDFilter spot, double roll)
        void calculate_top_vol(self, MDFilter spot, double roll)
        void otm_check(self)
        void compute_greeks(self)
        void set_tte(self, double tte)
        void _update_prev_vols(self, double vol, str side)
        double _get_spot(self, str side)
        double _get_vol(self, str side, str dict_type)
        double _get_price(self, double vol, str side)
        double round_price_up(self, double price)
        double round_price_down(self, double price)
        str _opp_side(self, str side)

cdef class OptionsFilterModel:
    cdef:
        readonly:
            dict fut_params, opt_params, securities, roll_opts, put_to_call
            dict secid_to_strike, secid_to_m, secid_to_vega, bez_dict, conn_defn
            list opt_secids, call_secids
            int fut_secid, n_strikes, pub_count, roll_mult
            double respline_interval, ts, ir, tte, spot, roll, min_bid, tte_ts
            bint snapshot, tte_flag, exch_time, new_curve
            str opt_contract, t_offset, tdate, name
            UnivariateNode kroll
            #python objects
            wing, use_roll, logger
        #void update_parameters(self, respline_interval, max_opt_width, max_fut_width, ir, opt_min_qty_par,fut_min_qty_par, pause_window, min_bid, use_roll, underlying_secid)
        void  set_tte(self, MDOrderBook *book)
        void _calc_tte(self, time)
        void evaluate_md(self, MDOrderBook *book)
        void compute_model(self, MDOrderBook *book)
        void calculate_top_vols(self)
        void calc_roll_from_market(self)
        void calc_roll(self)
        void _build_roll_dict(self)
        void _update_kroll(self, double roll)
        void dump_data(self)
        void read_dump(self)
