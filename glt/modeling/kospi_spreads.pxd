# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from libc.stdint cimport *

from glt.md.standard cimport (
    MDOrderBook, MeanValueResampler, MVRWithinBounds, 
    OptionResampler, OptionImpliedValues,
    CalculateTV, CalculateDelta, CalculateGamma 
)
from numpy cimport ndarray 


cdef class KospiOptionSpreadResampler:
    cdef:
        MVRWithinBounds resampler_u
        OptionResampler resampler_over, resampler_under
        readonly:
            uint64_t interval
            int16_t option_type
            double price_u, price_over, price_under
            double tte, interest_rate, strike_over, strike_under
            double u_distance, epsilon
            double half_life
            double price_multiplier
            ndarray futprcs, spread_prices, spread_vols, w
            
        void _record_over(self, MDOrderBook *md, bint use_exch_time=*) nogil

        void _record_under(self, MDOrderBook *md, bint use_exch_time=*) nogil
        
        void _record_u(self, MDOrderBook *md, bint use_exch_time=*) nogil
        
        void _export_to_ndarrays(self)
        
        OptionImpliedValues* _implied_over_ptr(self) nogil
    
        OptionImpliedValues* _implied_under_ptr(self) nogil
    
        uint64_t _first_sample_time(self) nogil
    
        uint64_t _last_sample_time(self) nogil
    
        double _min_u(self) nogil
    
        double _max_u(self) nogil
    
        bint _use_black_scholes(self) nogil

        double _u_range(self) nogil

        bint _has_u_range(self) nogil

        uint64_t _num_samples(self) nogil

        double _calc_tv(self, double strike, double vol, double u) nogil

        double _calc_delta(self, double strike, double vol, double u) nogil

        double _calc_entropy_weighted_value(self, MeanValueResampler *mvr) nogil

        double _average_futprc(self) nogil

        ndarray _futprcs_around_average(self, uint64_t num_points)

        double _average_over_spread_price(self) nogil

        double _average_under_spread_price(self) nogil

        double _average_spread_price(self) nogil

        double _average_over_vol(self) nogil

        double _average_under_vol(self) nogil

        ndarray _lsq_coef_prices(self, uint64_t n)

        double _implied_spread_delta(self) nogil
