# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True

from libc.math cimport NAN
from glt.math.helpful_functions cimport double_eq, double_lt, double_gt, double_lte, double_gte
from numpy import isnan
from pandas import Timestamp
#MAX_DEPTH is from book_building.h (imported through the .pxd of this file)

#im not super sure where this number comes from...
PRICE_MULTIPLIER = 10000.

cdef class FilterModel(object):
    """__init__(self, double z_outlier, int num_outliers, int trigger_contract, dict mdfilter_params):

    This is the piece that does the modeling for the individual MDFilter objects.  The idea is that you
    initiate a FilterModel and it creates the necessary MDFilter objects which filters the individual
    securities.  The FilterModel then uses the data from the MDFilters to give you a theoretical value
    (or values plural, its up to you).  You will need to create the "compute_model" function and you'll 
    probably also want to create a __repr__ function.  

    Parameters
    ----------
    mdfilter_params : dict of dicts
        The keys of the main dict are the deio security ids you want to model.  The values of this
        dict are dicts of MDFilter parameters (see that code for parameters specifics).  

    Returns
    -------
    Whatever you decide to return!  Or not return!  So many possibilities... only limited by your
    imagi:ation!
    """ 
    
    # make sure you actually need the variable trigger contract, i think this was a hack to fix a 
    # problem that likely has a better solution
    def __init__(self, dict mdfilter_params):
        cdef int i
        #TODO which of these attributes are necessary for the base class? rm extras.
        self.sec_list = {}
        self.n = len(mdfilter_params)
        self.leaders = []
        self.samples = []
        self.outliers = []
        self.prev_outlier = 0
        self.last_val = 0
        self.tv = 0

        # unpack parameters for each secid and pass to MDFilter
            #def __init__(self, int32_t secid, int32_t min_qty, int32_t depth, double pause_window, 
            #    double mkt_width, bint leader, bint weighted):
        for secid, params in mdfilter_params.iteritems():
            self.sec_list[secid] = MDFilter(secid=secid, **params)
            if params['leader']:
                self.leaders.append(secid)


    cdef void evaluate_md(self, MDOrderBook *book):
        cdef:
            bint all_valid = True
            int i
            MDFilter md_filter

        if book.secid in self.sec_list.keys():
            md_filter = self.sec_list[book.secid]
            md_filter.evaluate_md(book)

        all_valid = True
        for secid in self.leaders:    #THIS IS WHY IT COMPUTES TWICE for tp/nk spread pricing!!!!!
            self.compute_model(book.secid)

    cdef void compute_model(self, int secid):
        raise NotImplemented('please implement .compute_model()')
        #herein lies the rub


cdef class MDFilter(object):
    """__init__(self, int32_t secid, int32_t min_qty, int32_t depth, double pause_window,
                 double mkt_width, bint leader, bint weighted)

        Welcome to the wonderful world of market data filtering.  MDFilter is a
        container that reads MDOrderBooks and applies the filters based on your
        parameters (described below).  WARNING!!! The weighted mid functionality hasn't
        been thoroughly tested!  If you use this while still able to read this warning,
        please test then remove the warning.

        Parameters
        ----------
        secid : int32_t
            deio security id
        min_qty : int32_t
            Minimum qty required before a level is use.  Books without
            this quantity are discarded.
        min_par : int32_t
            Minimum participant required before a level is taken.  Books that
            do  not meet the criteria are discarded. 
        pause_window : double
            Used in conjunction with leader.  This sets the time interval to
            wait until it updates the MDFilter objects values with new market data.
            Only leader = True instances use this feature.  The idea being that 
            leader contracts have to wait for 'follower' contracts to catch up
            so reading 'leader' md in real time will not represent the true state
            of the market.
        mkt_width : double
            This is the max width between bid and ask values.  Books wider than this will
            be discarded.  Note: for futures this value should probalby be one tick. 
            A value of -1 "turns off" this feature and any width is evaluated.
        leader : bint
            Setting this to true forces you to wait the pause_window length of time to 
            update the market.  Should be used on contracts that lead and should not lag.
        weighted : bint
            If you want the mid point generated to be weighted, set to True.

        Returns
        -------
        A MDFilter object of your very own.  This object is essentially a dict named
        self.md with the following keys:
        ['bid', 'ask', 'bid_qty', 'ask_qty', 'seqnum', 'mid', 'ts', 'seqnum_valid']
        Most of these are self explanatory, but 
        'seqnum' is the sequence number of the book that first met min_qty and 
             and made the book valid.  
        'mid' is continually updated according to qty if weighted is True.
        Another important attribute is self.valid.  This determines if the book has passed
        the pause_window time requirement.

        Notes ("Its dangerous to go alone.  Take this." - Old man from Legend of Zelda)
        -----
        MDOrderBook is in the file ~/glt-master/glt/md/book_building.pxd, which references the
        header file ~/glt-master/projects/cpp/md_helpers/book_building.h.  They key to
        understanding how to use these objects is understanding how the MDOrderBook works, so
        take a peek at those files.
    """

    def __init__(self, int32_t secid, int32_t min_qty, int32_t min_par,  
                double pause_window, double mkt_width, bint leader, bint weighted):

        self.secid = secid
        self.tight_mkt = mkt_width
        self.leader = leader
        self.weighted = weighted
        self.pause_window = pause_window
        self.min_qty = min_qty
        self.min_par = min_par
        self.valid = False
        self.tight = False
        self.top = {'bid' : NAN, 'ask' : NAN, 'mid' : NAN}
        self.md = {}
        dict_keys = ['bid', 'ask', 'bid_qty', 'ask_qty', 'seqnum', 'mid', 'ts', 'seqnum_valid']
        for key in dict_keys:
            self.md[key] = NAN
        #leaders have separate dicts for md and md_staging, otherwise its the same dict
        if self.leader:
            self.md_staging = self.md.copy()
        else:
            self.md_staging = self.md

    def update_parameters(self, min_qty, min_par, mkt_width, pause_window):
        self.tight_mkt = mkt_width
        self.pause_window = pause_window
        self.min_qty = min_qty
        self.min_par = min_par

    cdef void evaluate_md(self, MDOrderBook *book):
        cdef:
            int logsecid = 000000
        #this is the function called from the outside to pass in the MDOrderBook
 
        if not double_eq(book.secid, self.secid):
            self.check_pause_window(book.time, book.seqnum)
            return

        # set top of book only by par
        temp_qty = self.min_qty
        self.min_qty = 0
        self.get_market(book)
        self.top['bid'], self.top['ask'] = self.bid, self.ask
        self.top['mid'] = (self.top['bid'] + self.top['ask']) * 0.5
        self.min_qty = temp_qty
        
        #set book with min qty/par
        self.get_market(book)

        # only proceed if market is both tight (if enabled) and different
        # for readability, set tight variable here
        if double_gt(self.tight_mkt, 0):
            self.tight = double_lte(self.ask - self.bid, self.tight_mkt)
            # tight: only market width component
            # valid: tight AND paused window has passed (only matters for leaders)
            if not self.tight:
                self.valid = False
        else:
            self.tight = True

        #case 1: if current market != staging market: then call _update_md
        #case 2: for weighted mid
        #case 3: if new book == md but != staging, then its a flicker and reset staging
        #        (only applies to leaders)
        #case 4: md_staging bid/ask is nan, should only happen on first book update of day
        case = 'no case'
        #tight and different
        if self.tight and (self.bid != self.md_staging['bid'] or self.ask != self.md_staging['ask']):
            self._update_md(book)
            case = '1'
        #tight and weighted
        elif self.tight and self.weighted and self.md['bid'] == self.md_staging['bid']:
            self.md['mid'] = self._weighted_mid(book)
            case = '2'
        #i dont think you can actually hit case 3... confirm
        elif self.tight and self.bid == self.md['bid'] and self.md['bid'] != self.md_staging['bid']:
            self.md_staging = self.md.copy()
            case = '3'
        # if nothing in obj, put something there
        elif (isnan(self.md_staging['bid']) or isnan(self.md_staging['ask'])):
            self._update_md(book)
            case = '4'

        #logging for testing
        if book.secid in [logsecid]:
            print '>>>>>>>>>>>>>'
            print 'MDFilter.evaluate_md() ', book.secid
            print Timestamp(book.time)
            print Timestamp(self.md_staging['ts'])
            print 'seq, bid, ask:', book.seqnum, book.bids[0].price, book.asks[0].price
            print 'book bids:', book.bids[0].price, book.bids[1].price, book.bids[2].price, book.bids[3].price, book.bids[4].price
            print 'book asks:', book.asks[0].price, book.asks[1].price, book.asks[2].price, book.asks[3].price, book.asks[4].price
            print 'self.bid/ask:', self.bid, self.ask
            print "md[]:", self.md['bid'], self.md['ask'], self.md['mid']
            print "stage md[]:", self.md_staging['bid'], self.md_staging['ask'], self.md_staging['mid']
            print "top[]:", self.top['bid'], self.top['ask'], self.top['mid']
            print 'tight, valid:', self.tight, self.valid, self.tight_mkt,  double_lte(self.ask - self.bid, self.tight_mkt)
            print 'case: ', case
            print '^^^^^^^^^^^^^'

    cdef void get_market(self, MDOrderBook *book):
        #Find the appropriate level in the book that meets min qty/par
        # sum across levels.  If no min par/qty, set to top level.
        if self.min_par == 0 and self.min_qty == 0 or isnan(self.md['mid']):
            self.bid = book.bids[0].price / PRICE_MULTIPLIER
            self.ask = book.asks[0].price / PRICE_MULTIPLIER
        else:
            #bids
            i, qty, par = 0, 0, 0
            while True:
                par += book.bids[i].participants
                qty += book.bids[i].qty
                if (qty >= self.min_qty and par >= self.min_par):
                    break
                elif i == MAX_DEPTH-1:
                    i = 0
                    break
                i += 1
            self.bid = book.bids[i].price / PRICE_MULTIPLIER
            self.bid_qty = qty

            #asks
            i, qty, par = 0, 0, 0
            while i < MAX_DEPTH:
                par += book.asks[i].participants
                qty += book.asks[i].qty
                if (qty >= self.min_qty and par >= self.min_par):
                    break
                elif i == MAX_DEPTH-1:
                    i = 0
                    break
                i += 1
            self.ask = book.asks[i].price / PRICE_MULTIPLIER
            self.ask_qty = qty 


    def check_pause_window(self, ts, seqnum=0):
        # This makes the values in md valid if they have existed longer than
        # the pause window.  Only necessary if the instance is a leader.

        #to make sure this isnt being called before its received a book
        if self.md_staging['ts'] == 0:
            return

        now = ts
        # valid: if tight and pause window has passed (only for leaders)
        if not self.valid and self.leader and now - self.md_staging['ts'] > self.pause_window:
            #move staging md into md dict
            self.md = self.md_staging.copy()
            self.md['seqnum_valid'] = seqnum
            self.valid = True
        elif not self.tight and self.leader:
            self.valid = False

    cdef void force_price(self, double price, str side):
        #intended use for options/securities that have no bid and you want to force a price
        self.md[side] = price

    cdef void _update_md(self, MDOrderBook *book):
        #updates the staging_md if the instance is a leader, else
        #  updates the md

        #if this contract is a leader (and not nan), push the data to the md_staging until 
        #   enough time has passed (pause window time)
        if self.leader and not isnan(self.md['mid']):
            d = self.md_staging
            d['seqnum_valid'] = 0
            self.valid = False
        else:
            d = self.md
            d['seqnum_valid'] = book.seqnum
            # data that is not the init data is always valid for non leaders
            self.valid = True


        d['bid'] = self.bid
        d['ask'] = self.ask
        d['bid_qty'] = self.bid_qty 
        d['ask_qty'] = self.ask_qty
        d['seqnum'] = book.seqnum
        d['ts'] = book.time
        if self.weighted:
            d['mid'] = self._weighted_mid(book)
        else:
            d['mid'] = 0.5 * (d['bid'] + d['ask'])

    cdef double _weighted_mid(self, MDOrderBook *book):
        #untested.  use at your peril.
        # returns the value to reverse weighted mid

        #calculate reverse weighted mid
        ttl_qty = book.asks[0].qty + book.bids[0].qty
        return 1./ttl_qty * (book.asks[0].qty * book.bids[0].price + book.bids[0].qty * book.asks[0].price)


