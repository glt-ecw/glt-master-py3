from libc.stdint cimport *
from libcpp.string cimport string


cdef extern from "order_entry.h" nogil:
    cdef cppclass DeioFill:
        uint64_t deioid, fwd_seqnum, seqnum
        uint16_t fill_type, side
        uint64_t secid
        uint16_t userid, serverid
        double price
        int32_t qty, remaining_qty
        string exch_trade_id, fill_time
        uint16_t order_type
        uint16_t assumed_fill_type
