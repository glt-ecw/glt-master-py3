# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from libc.math cimport NAN
from numpy cimport ndarray

import os

from collections import namedtuple
from glob import glob
from numpy import zeros, int64, float64, fromfile, concatenate


def what():
    pass
