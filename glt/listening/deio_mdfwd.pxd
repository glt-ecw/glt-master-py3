from libc.stdint cimport int8_t, int16_t, int32_t, int64_t, uint8_t, uint16_t, uint32_t, uint64_t
from libcpp.vector cimport vector

from glt.md.standard cimport MDOrderBook


cdef extern from "deio_mdfwd.h" nogil:
    cdef cppclass DeioMDFWDHeader:
        uint32_t server_id, security_id
        uint64_t seqnum, time_sec, time_nanos
        uint8_t event_type, status, trade_direction, num_levels
        double trade_price
        int64_t trade_qty, total_volume
        double bid_price_change, ask_price_change
        double indicative
        bint replay
    cdef cppclass DeioMDFWDLevel:
        double bid_price, ask_price
        int64_t bid_qty, ask_qty
        uint32_t bid_participants, ask_participants
    cdef cppclass DeioMDFWDReader:
        int32_t indicative
        MDOrderBook md
        DeioMDFWDHeader header
        vector[DeioMDFWDLevel] levels
        DeioMDFWDReader()
        void Process(char *bytes, double price_multiplier)
        bint UseMD() const
