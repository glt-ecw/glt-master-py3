from libc.stdint cimport *


cdef class DeioMulticastReceiverInterface:
    cdef:
        readonly:
            str iface, addr
            pending_msgs
            sock
            uint64_t msg_count
            thread

        bint _has_msgs(self) nogil
