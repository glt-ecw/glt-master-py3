from libc.stdint cimport *
from libcpp.string cimport string
from glt.oe.standard cimport DeioFill


cdef extern from "deio_fillfwd.h" nogil:
    cdef cppclass DeioFillFWDReader:
        DeioFill fill
        DeioFillFWDReader()
        void AddSecID(uint64_t secid, uint16_t order_type) 
        bint IgnoreFill() const
        void Process(const char *str, uint64_t length)
