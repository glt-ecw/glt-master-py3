# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from cython.operator cimport preincrement, predecrement

from collections import deque
from threading import Thread
from time import sleep
from glt.net import MulticastSocket


cdef class DeioMulticastReceiverInterface:
    def __init__(self, iface, addr):
        self.iface = str(iface)
        self.addr = str(addr)
        self.pending_msgs = deque()
        self.sock = MulticastSocket(self.iface, self.addr, publish=False)
        self.msg_count = 0 
        self.thread = None
    
    def run(self):
        while True:
            payload = self.sock.recv(1024)
            self.pending_msgs.append(payload)
            preincrement(self.msg_count)

    def listen(self, double sleep_by=0.5):
        self.thread = Thread(target=self.run)
        self.thread.daemon = True
        self.thread.start()
        sleep(sleep_by)

    def close(self):
        pass

    def stop(self):
        self.close()

    cdef bint _has_msgs(self) nogil:
        return self.msg_count > 0 

    def pop_msg(self):
        cdef msg 
        if not self._has_msgs():
            return None
        msg = self.pending_msgs.popleft()
        predecrement(self.msg_count)
        return msg 

    def process_next_msg(self):
        raise NotImplemented('please implement .process_next_msg()')
