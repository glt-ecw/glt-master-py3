# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
from glt.md.standard cimport PyMDOrderBook


cdef class DeioMDReceiver(DeioMulticastReceiverInterface):
    def __init__(self, iface, addr, double price_multiplier=1.0):
        DeioMulticastReceiverInterface.__init__(self, iface, addr)
        self.price_multiplier = price_multiplier

    def process_next_msg(self):
        cdef msg = self.pop_msg()
        if msg is None:
            return False
        self.reader.Process(msg, self.price_multiplier)
        return True

    cdef bint _use_md(self) nogil:
        return self.reader.UseMD()

    def latest_msgs(self):
        cdef PyMDOrderBook pymd
        while self._has_msgs():
            pymd = PyMDOrderBook()
            self.process_next_msg()
            if self._use_md():
                pymd.copy(&(self.reader.md), parse_topic=False)
                yield pymd
