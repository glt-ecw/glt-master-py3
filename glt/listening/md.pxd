from glt.listening.deio_mdfwd cimport *
from glt.listening.base cimport DeioMulticastReceiverInterface


cdef class DeioMDReceiver(DeioMulticastReceiverInterface):
    cdef:
        double price_multiplier
        DeioMDFWDReader reader

        bint _use_md(self) nogil
