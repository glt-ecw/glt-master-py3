# distutils: language=c++
# distutils: extra_compile_args=-std=c++11
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
cdef class DeioFillReceiver(DeioMulticastReceiverInterface):
    def __init__(self, iface, addr):
        DeioMulticastReceiverInterface.__init__(self, iface, addr)

    def process_next_msg(self):
        cdef msg = self.pop_msg()
        if msg is None:
            return False
        self.reader.Process(msg, len(msg))
        return True

    cdef bint _ignore_fill(self) nogil:
        return self.reader.IgnoreFill()
