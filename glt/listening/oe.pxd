from glt.listening.deio_fillfwd cimport *
from glt.listening.base cimport DeioMulticastReceiverInterface


cdef class DeioFillReceiver(DeioMulticastReceiverInterface):
    cdef:
        DeioFillFWDReader reader

        bint _ignore_fill(self) nogil
