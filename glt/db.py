import json
import mysql.connector
import pandas as pd
import datetime as dt
import dateutil.rrule as dr
import glt.env

from collections import OrderedDict


WEEKDAYS = tuple(range(5))

CONFIG_BASENAME = 'mysql.json'

get_type = mysql.connector.FieldType.get_info


def read_mysql_config(json_filename=None):
    if json_filename is None:
        json_filename = glt.env.MYSQL_CONFIG
    cfg = None
    with open(json_filename, 'r') as f:
        cfg = json.load(f)
    return cfg


def autoclose_query(cmd, user='rdeio'):
    """autoclose_query(cmd, user='rdeio')

Automatically open connection to a mysql server, query, then close connection.

Parameters
----------
cmd : str
    Your MySQL query text. See MySQL documentation for syntax.
user : str
    User for GLT MySQL servers. Allowed users are: rdeio, deio, and bk_rdeio.
    Default is rdeio.
"""

    cfg = read_mysql_config()
    if user not in cfg:
        raise ValueError('user not recognized. allowed users are rdeio, bk_rdeio, and deio')
    conn = None
    try:
        conn = glt.db.MysqlConnection(**cfg[user])
        return conn.frame_query(cmd)
    finally:
        if conn is not None:
            conn.close()


def query_deio_smoothed_vols(conn_defn, term, start_time, column_convert_func, model, server=None):
    """
Parameters
----------
conn_defn : dict
    Keys required: host, user, pw (password), db (database)
term : string
    Option term (e.g. ESOJ16)
start_time : datetime object
    When to take the last known smoothed vols.
column_convert_func : func
    Columns of queried table are stored as string-formatted
    strikes (e.g. S5000), which need to be converted to a
    meaningful column name (e.g. ESJ6 C500, or relevant security
    identifier). Convention should use call option identifiers.
model : BezierModel object
    Used to extract put and call delta boundaries. Also used
    to compute deltas using raw_option_dict. Keys of this
    dictionary must be the same as the result of converting
    the vol columns using column_convert_func.

Returns
-------
vols : dict if queries are successful, None otherwise
    Keys are float64 format; values are vols

    """
    evt_time = start_time.strftime('%Y-%m-%d %H:%M:%S')
    datestr = start_time.strftime('%Y%m%d')
    try:
        myconn = conn(**conn_defn)
        if server is None or len(server) == 0:
            cmd = """
                SHOW TABLES LIKE 'opm_%s_%s_smoothedVols'
            """ % (datestr, term)
        else:
            cmd = """
                SHOW TABLES LIKE 'opm_%s_%s_%s_smoothedVols'
            """ % (datestr, server, term)
        vol_table = myconn.frame_query(cmd).values
        if len(vol_table) == 0:
            raise ValueError('no vol tables found; mysql cmd: %s' % cmd)
        vol_table = vol_table[0, 0]
        cmd = """
            SELECT * FROM %s
            WHERE eventtime<='%s'
            ORDER BY eventtime DESC, useventtime DESC
            LIMIT 1
        """ % (vol_table, evt_time)
        df = myconn.frame_query(cmd)
        if not df.shape[0]:
            raise ValueError('no vols found in %s', vol_table)
        vols = df.iloc[0].dropna()
        evt_time = vols['eventTime'].to_pydatetime()
        cmd = """
            SELECT futuremidprice + synthetic as spot
            FROM %s
            WHERE eventtime<='%s'
            ORDER BY eventtime DESC, useventtime DESC
            LIMIT 1
        """ % (vol_table.replace('smoothed', 'raw'), evt_time)
        spot = myconn.frame_query(cmd).values
        if not spot.shape[0]:
            raise ValueError('no spot found in rawVols')
        spot = spot[0, 0]
        vols = vols[[x for x in vols.index if 'Time' not in x]]
        vols.index = list(map(column_convert_func, vols.index))
        vols = vols.to_dict()
        remove_keys = []
        for k, v in vols.items():
            if v < 0:
                raise ValueError('smoothed vol < 0')
            if k not in model.raw_option_dict:
                remove_keys.append(k)
                continue
            rs = model.raw_option_dict[k]
            delta = rs.calc_delta(spot, v)
            if not rs.is_otm(spot):
                delta = rs.cp * (1.0 - abs(delta))
            if (rs.cp > 0 and delta < model.call_delta) or (rs.cp < 0 and delta > model.put_delta):
                remove_keys.append(k)
        for k in remove_keys:
            del vols[k]
        return spot, vols
    finally:
        myconn.close()


def get_expiry(today, shortname):
    conn_defn = {
        #'host': '10.1.31.200',
        'host': '10.125.0.200',
        'user': 'rdeio',
        'pw': 'fatmanblues',
        'db': 'SECURITIES'
    }
    try:
        #conn = glt.db.MysqlConnection(**cfg[user])
        myconn = conn(**conn_defn)
        query = """
        SELECT
            shortNameTbl.name as shortName,
            options.expDate as expdate
        FROM (
            SELECT
                securities.id,
                LEFT(securities.shortName, LOCATE('_', securities.shortName)-1) AS name
            FROM securities
            WHERE shortname like '%s_%%'
        ) AS shortNameTbl
        JOIN options
        ON options.id=shortNameTbl.id
        WHERE expdate>='%s'
        GROUP BY shortNameTbl.name
        """ % (shortname, today)
        df = myconn.frame_query(query)
    finally:
        myconn.close()
    return df.expdate.values[0]


def get_dte(name, from_date=None):
    if from_date is None:
        today = dt.datetime.today().date()
    else:
        today = from_date
    shortname = '%sO%s1%s' % (name[:2], name[2], name[3])
    expDate = get_expiry(today, shortname)
    expDate = pd.Timestamp(expDate).date()   
    #jff: im not sure why it added one day... if its still here after a while i guess we can del it (12/14/17)
    #return dr.rrule(dr.DAILY, dtstart=today, until=expDate+dt.timedelta(days=1), byweekday=WEEKDAYS).count()
    return dr.rrule(dr.DAILY, dtstart=today, until=expDate, byweekday=WEEKDAYS).count()


def getDTE(contractShortName, fromDate=None):
    if fromDate is None:
        today = dt.datetime.today().date()
    else:
        today = fromDate
    expDate = get_expiry(today, contractShortName)
    expDate = pd.Timestamp(expDate).date()    
    #jff: im not sure why it added one day... if its still here after a while i guess we can del it (12/14/17)
    #return dr.rrule(dr.DAILY, dtstart=today, until=expDate+dt.timedelta(days=1), byweekday=WEEKDAYS).count()
    return dr.rrule(dr.DAILY, dtstart=today, until=expDate, byweekday=WEEKDAYS).count()


def conn(host, user, pw, db=None):
    param = {
        'host': host,
        'user': user,
        'password': pw
    }
    if db is not None:
        param['database'] = db
    return MysqlConnection(**param)


class MysqlConnection:
    conn = None
    cursor = None

    def __init__(self, **kwargs):
        param = dict(kwargs)
        param['raw'] = True
        param['raise_on_warnings'] = True
        param['use_pure'] = False
        self.param = param
        self.open()

    def open(self):
        if self.conn is None:
            self.conn = mysql.connector.connect(**self.param)
            self.cursor = self.conn.cursor(buffered=True)
        
    def close(self):
        if self.cursor is not None:
            self.cursor.close()
            self.cursor = None
        if self.conn is not None:
            self.conn.close()
            self.conn = None

    def cursor_exists(self):
        return self.cursor is not None

    def execute(self, instruction):
        if not self.cursor_exists():
            raise AssertionError('cursor does not exist')
        self.cursor.execute(instruction)

    def commit(self):
        if not self.cursor_exists():
            raise AssertionError('cursor does not exist')
        self.conn.commit()

    def fetchall(self):
        if not self.cursor_exists():
            raise AssertionError('cursor does not exist')
        return self.cursor.fetchall()

    def frame_query(self, instruction):
        if not self.cursor_exists():
            raise AssertionError('cursor does not exist')

        self.execute(instruction)
        desc = self.cursor.description
        columns = OrderedDict([(r[0], get_type(r[1])) for r in desc])

        data = pd.DataFrame(self.cursor.fetchall(), columns=list(columns.keys()))
        for col, mysql_type in columns.items():
            if mysql_type in ('INT', 'LONG', 'LONGLONG'):
                try:
                    data[col] = data[col].astype(int)
                except TypeError:
                    import warnings
                    warnings.warn(
                        (
                            'column "%s" is of type %s but has NULL. Filling with zeros.'
                        ) % (col, mysql_type), Warning)
                    data[col] = data[col].fillna(0).astype(int)
            elif mysql_type in ('FLOAT', 'DOUBLE', 'DECIMAL', 'NEWDECIMAL'):
                data[col] = data[col].astype(float)
            elif mysql_type in ('VAR_STRING', 'STRING', 'TIME'):
                data[col] = data[col].astype(str)
            elif mysql_type in ('DATE', 'DATETIME', 'TIMESTAMP'):
                timecol = data[col].astype(str)
                timecol[timecol=='0000-00-00 00:00:00'] = 'NaT'
                data[col] = pd.to_datetime(timecol)
            else:
                raise ValueError('do not recognize %s type' % mysql_type)
        return data

    # deprecated
    def executeQuery(self, instruction):
        return self.frame_query(instruction)


#class conn:
#    def __init__(self, host, user, pw, db=None):
#        self.host = host
#        self.user = user
#        self.pw = pw
#        self.db = db
#        self.connection = self.open()
#        self.curs = self.connection.cursor()
#
#    def fetchall(self): #, cursorObj):
#        #rows = cursorObj.fetchall()
#        rows = self.curs.fetchall()
#        dat = OrderedDict() #{}
#        if rows != ():
#            desc = self.curs.description #cursorObj.description
#            data = [item for item in rows]
#            fieldnames = [item[0] for item in desc]
#            for i, fieldname in enumerate(fieldnames): # in xrange(len(fieldNames)):
#                dat[fieldname] = [row[i] for row in data]
#        return dat
#        #else:
#        #    return pd.DataFrame()
#
#    def open(self):
#        mysql_cn = sql.connect(host=self.host, port=3306,
#                                user=self.user, passwd=self.pw,
#                                db=self.db if self.db else '')
#        #mysql_cn.autocommit(True)
#        return mysql_cn
#
#    def frame_query(self, query):
#        #self.curs.execute(query)
#        self.execute(query)
#        return pd.DataFrame(self.fetchall()) #create_dict(self.curs))
#
#    def execute(self, query):
#        self.curs.execute(query)
#        return
#
#    def executeQuery(self, command):
#        return self.frame_query(command)
#
#    def close(self):
#        self.connection.close()
#
