#cimport cython
# cython: boundscheck=False
# cython: cdivision=True
# cython: wraparound=False
from cython.operator cimport preincrement as inc

from glt.modeling.structs cimport MarketState
from glt.math.kalman cimport UnivariateNode

cdef inline str get_monthcode(long month):
    cdef str monthcodes
    if month > 0 and month < 13:
        monthcodes = '0FGHJKMNQUVXZ'
        return monthcodes[month]
    else:
        return 'A'

#@cython.boundscheck(False)
#@cython.wraparound(False)
cdef inline void fast_unique_ticks(long tlen, long[:] bidprc, long[:] askprc, long[:] results) nogil:
    cdef long i = 0, tick = 0
    for i in range(tlen):
        if i == 0:
            continue
        if bidprc[i] != bidprc[i-1] or askprc[i] != askprc[i-1]:
            #tick += 1
            inc(tick)
        results[i] = tick
    return


cdef class SpotManager:
    cdef:
        readonly bint wide, good, usable, new_tick
        readonly double tick_width, bid, ask, prc
        readonly long min_size, ts, pause_window, bidqty, askqty, seqnum, last_good_ts
    
    cdef void update_md(self, long ts, long seqnum, double bidprc, long bidqty, double askprc, long askqty) nogil
    cdef bint _is_usable(self, long ts) nogil
    cdef void _update_marketstate(self, long ts, long seqnum, MarketState *mkt) nogil


cdef class SuperSpotManager:
    cdef:
        readonly bint good
        readonly double tick_width, bid, ask, prc
        readonly long min_size, pause_window, bidqty, askqty, seqnum, last_good_ts, bidpar, askpar
        readonly double last_turn_tradeprc, last_turn_ts
    
        void update_md(self, long ts, long seqnum, double bidprc, long bidqty, double askprc, long askqty,
            long bidpar, long askpar,
            double buyprice, long buyvolume, double sellprice, long sellvolume) nogil
        void _update_usability(self, long ts) nogil
        void _update_marketstate(self, long ts, long seqnum, MarketState *mkt) nogil


cdef class SyntheticRollManager:
    cdef:
        readonly double futprc, last_futprc, raw_roll, roll, upper, lower, strike_boundary
        readonly double q, r, band, small_band, large_band
        readonly dict option_prices
        readonly set sampled_strikes
        UnivariateNode kroll
        readonly double[:] prev_movement
        readonly size_t prev_head, prev_n
        readonly double prev_threshold, prev_roll_diff
        readonly bint has_new_data

        double _get_roll(self) nogil
        double _get_kroll(self) nogil
        double _get_last_futprc(self) nogil
        double _get_last_synthetic(self) nogil
        void _update_option(self, double cp, double k, double bid, double ask)
        void _update_roll(self, double k, double[:] prcs) nogil
        double fix_band(self) nogil
        void _update_kroll(self, double roll) nogil



cdef class OrderBook:
    cdef:
        dict bookqtys
        public list prices

    cdef void clear(self)
    cdef void modify(self, long price, long qty)
    cdef void remove(self, long price)
    cdef bint has(self, long price)
    cdef long at(self, long price)

cdef class BBO:
    cdef:
        public long[:] bidprices, askprices, bidqtys, askqtys
        readonly int levels
        readonly OrderBook bid_ob, ask_ob
        readonly long[:] best
    cdef void clear_book(self)
    cdef void update_book_array(self, long side, long[:] prices, long[:] qtys)
    cdef void remove_irrelevant(self, long side, long price, OrderBook ob)
    cdef long update_book_by(self, long side, long price, long qty)
    cdef long update_book_by_nocheck(self, long side, long price, long qty)
    cdef bint has(self, long side, long price)
    cdef long at(self, long side, long price)
    cdef void rebuild_price_arrays(self)
    cdef void rebuild_qty_arrays(self)
    cdef bint is_crossed(self) nogil
    cdef list update_assumed_tradeqty(self, long side, long price, long tradeqty, long itemqty)
    cdef tuple reverse_assumed_tradeqty(self, long side, list tradeqtys, long reversed_tradeqty)

