from __future__ import print_function
cimport cython
from numpy cimport ndarray
from numpy import zeros, float64, int64
from pandas import DataFrame

from glt.structures cimport *

@cython.boundscheck(False)
cdef long peek_time(ArrayQueue *queue) nogil:
    if queue.length <= 0:
        return 0
    return queue.data[queue.head].time

@cython.boundscheck(False)
cdef long purge_trades(ArrayQueue *trades, long time, long window, bint checked_first=False) nogil:
    cdef:
        Data[:] data = trades.data
        Data popped
        long remove_qty = 0
    if not checked_first:
        popped = pop_arrayqueue(trades)
        remove_qty += popped.qty
    while trades.length > 0 and time - peek_time(trades) > window:
        popped = pop_arrayqueue(trades)
        remove_qty += popped.qty
    return remove_qty

cdef class VolumeAccumulator:
    cdef:
        long window, tradesum
        ArrayQueue trades

    def __cinit__(self, long window):
        self.window = window
        self.tradesum = 0
        self.trades = new_arrayqueue(0)

    cdef long add(self, long time, long price, long qty):
        cdef:
            long window = self.window, remove_qty
            ArrayQueue trades = self.trades
        if trades.length > 0 and time - peek_time(&trades) > window:
            remove_qty = purge_trades(&(self.trades), time, window, checked_first=True)
            self.tradesum += qty - remove_qty
        else:
            self.tradesum += qty
        push_arrayqueue(&(self.trades), time, 0, price, qty)
        return self.tradesum
    
    @cython.boundscheck(False)
    cdef void clear(self) nogil:
        while self.trades.length > 0:
            pop_arrayqueue(&(self.trades))
        self.tradesum = 0
        return

# analysis
def regex_search_rawmdtxt(series, bo2_seqnum, bd1_seqnum, bd1_counter):
    # zgrep -E '(BD1,.*96,30,11,0,50,13516.* seqNum: 524,.*numItems|BO2,.*96,30,11,0,50,13516.* seqNum: 28587,)' *20150331*.txt.gz
    items = ['(BD1,.*', series, '.* seqNum: ', str(bd1_seqnum), ',.*numItems.*counter: ', str(bd1_counter), 
             '|BO2,.*', series, '.* seqNum: ', str(bo2_seqnum), ',)']
    return ''.join(items)

def regex_search_mdmsglog(series, bo2_seqnum, bd1_seqnum, bd1_counter):
    # (BD1: .*96, 30, 11, 0, 50, 13516, 0.* seqNum: 729\W|BO2: .*96, 30, 11, 0, 50, 13516, 0.* seqNum: 36512\W)
    series_refmt = series.replace(',', ', ')
    items = ['(BD1: .*', series_refmt, '.* seqNum: ', str(bd1_seqnum), '\W|BO2: .*', series_refmt, '.* seqNum: ', str(bo2_seqnum), '\W)']
    return ''.join(items)

def compute_streaker_mkt(df, long window_size, long sym_int, bint compute_at_mkt=True):
    cdef:
        long tlen = df.shape[0], i, j, qty_buys, qty_sells, count_buys, count_sells, bid, ask, buyvolume, sellvolume, buyprice, sellprice
        long[:] times = df.timestamp.values
        long[:] sym_ints = df.sym_int.values
        long[:] bidprcs = df.bidprc0.values
        long[:] askprcs = df.askprc0.values
        long[:] buyprices = df.buyprice.values
        long[:] sellprices = df.sellprice.values
        long[:] buyvolumes = df.buyvolume.values
        long[:] sellvolumes = df.sellvolume.values
        long[:] rbuyprices, rsellprices, rbuyvolumes, rsellvolumes
        ndarray streaker_results = zeros((tlen, 4), dtype=long)
        tuple acols = ('rbuyprice', 'rsellprice', 'rbuyvolume', 'rsellvolume')
        bint assumed = False

    if acols[0] in df.columns:
        rbuyprices = df.rbuyprice.values
        rsellprices = df.rsellprice.values
        rbuyvolumes = df.rbuyvolume.values
        rsellvolumes = df.rsellvolume.values
        assumed = True

    for i in range(tlen):
        j = i
        
        qty_buys, qty_sells = 0, 0
        count_buys, count_sells = 0, 0

        bid, ask = bidprcs[i], askprcs[i]
        buyprice, sellprice = buyprices[i], sellprices[i]

        while j >= 0 and times[i] - times[j] <= window_size:
            if sym_int > 0 and sym_int != sym_ints[j]:
                j -= 1
                continue
            buyvolume, sellvolume = buyvolumes[j], sellvolumes[j]
            if ((compute_at_mkt and ask == buyprices[j]) or buyprice == buyprices[j]) and buyvolume > 0:
                qty_buys += buyvolume
                count_buys += 1
                if assumed and rbuyprices[j] == buyprices[j]:
                    qty_buys += rbuyvolumes[j]
            if ((compute_at_mkt and bid == sellprices[j]) or sellprice == sellprices[j]) and sellvolume > 0:
                qty_sells += sellvolume
                count_sells += 1
                if assumed and rsellprices[j] == sellprices[j]:
                    qty_buys += rsellvolumes[j]
            j -= 1
        streaker_results[i, 0] = qty_buys
        streaker_results[i, 1] = qty_sells
        streaker_results[i, 2] = count_buys
        streaker_results[i, 3] = count_sells

    return DataFrame(streaker_results, index=df.index, columns=('acc_buyqty', 'acc_sellqty', 'acc_buycnt', 'acc_sellcnt'))

def compute_streaker(df, long window_size, long sym_int):
    """
    streaker_sides(nkdf, double window_size, int sym_int)

    all: 0
    nk: 1
    mnk: 2
    snk: 3

    results:
        0 := buys_at_bid
        1 := buys_above_bid
        2 := sells_at_ask
        3 := sells_below_ask
        4 := buys_vs_ask
        5 := sells_vs_bid
        6 := vwap_buys
        7 := vwap_sells
    """
    cdef:
        long tlen = df.shape[0], i = 0, j = 0
        long[:] times = df.timestamp.values
        long[:] buyprices = df.buyprice.values
        long[:] sellprices = df.sellprice.values
        long[:] buyvolumes = df.buyvolume.values
        long[:] sellvolumes = df.sellvolume.values
        long[:] sym_ints = df.sym_int.values

        long buyprice, sellprice
        long prcqty_buys, prcqty_sells, qty_buys, qty_sells, prcqty_tot, qty_tot
        long count_buys, count_sells
        ndarray[long, ndim=2] streaker_results = zeros((tlen, 4), dtype=long)
        ndarray[double, ndim=2] vwap_results = zeros((tlen, 3), dtype=float64)

    for i in range(tlen):
        j = i
        
        prcqty_buys, prcqty_sells, qty_buys, qty_sells = 0, 0, 0, 0
        prcqty_tot, qty_tot = 0, 0
        count_buys, count_sells = 0, 0
        
        if buyvolumes[i] > 0 or sellvolumes[i] > 0:
            buyprice, sellprice = buyprices[i], sellprices[i]
            # climb back to find the start of the window
            while j >= 0 and times[i] - times[j] <= window_size:
                if sym_int > 0 and sym_int != sym_ints[j]:
                    j -= 1
                    continue
                # keep summing trades?
                if buyvolumes[j] > 0 and buyprices[j] == buyprice:
                    qty_buys += buyvolumes[j]
                    prcqty_buys += buyvolumes[j] * buyprices[j]
                    count_buys += 1
                if sellvolumes[j] > 0 and sellprices[j] == sellprice:
                    prcqty_sells += sellvolumes[j] * sellprices[j]
                    qty_sells += sellvolumes[j]
                    count_sells += 1
                j -= 1
            streaker_results[i, 0] = qty_buys
            streaker_results[i, 1] = qty_sells
            streaker_results[i, 2] = count_buys
            streaker_results[i, 3] = count_sells
            if qty_buys > 0:
                vwap_results[i, 0] = <double>prcqty_buys / <double>qty_buys
                prcqty_tot += prcqty_buys
                qty_tot += qty_buys
            if qty_sells > 0:
                vwap_results[i, 1] = <double>prcqty_sells / <double>qty_sells
                prcqty_tot += prcqty_sells
                qty_tot += qty_sells
            if qty_tot > 0:
                vwap_results[i, 2] = <double>prcqty_tot / <double>qty_tot
    return streaker_results, vwap_results

def compute_accqtys(df, long window_size):
    """
    streaker_sides(nkdf, double window_size)

    results:
        0 := buys_at_bid
        1 := buys_above_bid
        2 := sells_at_ask
        3 := sells_below_ask
        4 := buys_vs_ask
        5 := sells_vs_bid
    """
    cdef:
        long tlen = df.shape[0], i = 0, j = 0
        long[:] times = df.time.astype(int64).values
        long[:] buyprices = df.buyprice.values
        long[:] sellprices = df.sellprice.values
        long[:] buyvolumes = df.buyvolume.values
        long[:] sellvolumes = df.sellvolume.values

        long buyprice, sellprice
        long prcqty_buys, prcqty_sells, qty_buys, qty_sells, prcqty_tot, qty_tot
        long count_buys, count_sells
        ndarray[long, ndim=2] streaker_results = zeros((tlen, 4), dtype=long)
        ndarray[double, ndim=2] vwap_results = zeros((tlen, 3), dtype=float64)

    for i in range(tlen):
        j = i
        
        prcqty_buys, prcqty_sells, qty_buys, qty_sells = 0, 0, 0, 0
        prcqty_tot, qty_tot = 0, 0
        count_buys, count_sells = 0, 0
        
        if buyvolumes[i] > 0 or sellvolumes[i] > 0:
            buyprice, sellprice = buyprices[i], sellprices[i]
            # climb back to find the start of the window
            while j >= 0 and times[i] - times[j] <= window_size:
                # keep summing trades?
                if buyvolumes[j] > 0 and buyprices[j] == buyprice:
                    qty_buys += buyvolumes[j]
                    prcqty_buys += buyvolumes[j] * buyprices[j]
                    count_buys += 1
                if sellvolumes[j] > 0 and sellprices[j] == sellprice:
                    prcqty_sells += sellvolumes[j] * sellprices[j]
                    qty_sells += sellvolumes[j]
                    count_sells += 1
                j -= 1
            streaker_results[i, 0] = qty_buys
            streaker_results[i, 1] = count_buys
            streaker_results[i, 2] = qty_sells
            streaker_results[i, 3] = count_sells
            if qty_buys > 0:
                vwap_results[i, 0] = <double>prcqty_buys / <double>qty_buys
                prcqty_tot += prcqty_buys
                qty_tot += qty_buys
            if qty_sells > 0:
                vwap_results[i, 1] = <double>prcqty_sells / <double>qty_sells
                prcqty_tot += prcqty_sells
                qty_tot += qty_sells
            if qty_tot > 0:
                vwap_results[i, 2] = <double>prcqty_tot / <double>qty_tot
    streaker_df = DataFrame(
        streaker_results, 
        index=df.index, 
        columns=['acc_buyqty', 'acc_buycnt', 'acc_sellqty', 'acc_sellcnt']
    )
    vwap_df = DataFrame(
        vwap_results,
        index=df.index,
        columns=['buyvwap', 'sellvwap', 'vwap']
    )
    return streaker_df, vwap_df
